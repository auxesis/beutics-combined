<?php
	return [
	    'review_questions_arr' => [
	        '1' => ['name'=>'Do you believe this store/individual offer Value for Money services?','value'=>'2','key_value'=>'value_for_money'],

	        '2' => ['name'=>'How satisfied are you with the Staff Expertise?','value'=>'2','key_value'=>'staff_expertise'],

	        '3' => ['name'=>'How do you find the Cleanliness maintained?','value'=>'1','key_value'=>'ambience_comfort'],

	        '4' => ['name'=>'How convenient was the Location, in terms of accessibility, parkings etc?','value'=>'1.5','key_value'=>'location'],

	        '5' => ['name'=>'How satisfied are you with the Care and Attention provided?','value'=>'1.5','key_value'=>'hospitality'],
	        	
	        '6' => ['name'=>'What do you think of the Facilities, Products or Equipments used?','value'=>'2','key_value'=>'products_equipments'],
	    ],

	    'rating_arr' => [
	    	'1' => ['name'=>'poor','value'=>'2.5'],
	    	'2' => ['name'=>'average','value'=>'5'],
	    	'3' => ['name'=>'good','value'=>'7.5'],
	    	'4' => ['name'=>'superb','value'=>'10'],
	    ],
	];
?>