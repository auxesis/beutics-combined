<?php
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Str;
use App\Model\User;
use App\Model\Admin;
use App\Model\SpUser;
use App\Model\Chat;
//use Mail;
 use App\Mail\TestEmail;

if (!function_exists('send')) {

    function send($data) {
        $from_mail = 'contact@beutics.com';
        $site_title = 'Beutics';
        // Send email
        Mail::send('email.'.$data['templete'], ['data' => $data], function ($m) use ($data, $from_mail, $site_title) {
          $m->from($from_mail, $site_title);
          $m->to($data['email'])->subject($data['subject']);
        });

    }
}
if (!function_exists('changeImageUrl')) {


    function changeImageUrl($path) {

        $url = str_replace('v2/','',$path);
        return $url;
      
    }

}
if (!function_exists('changeImageUrlForFileExist')) {


    function changeImageUrlForFileExist($path) {
        $check = "";
        if (strpos($path, 'v2/') == false) { 
            $check = "web";
        } 
        else { 
            $check = "app";
        } 

        if($check == "app"){
          $headers = get_headers($path);
    
          
           if(stripos($headers[0], "200 OK")){
             
            return $path;
            
          }else{
            $url = str_replace('v2/','',$path);
            return $url;
          }
        }else{
          $headers = get_headers($path);
          if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
                $link = "https"; 
          else
              $link = "http";
          
           if(stripos($headers[0], "200 OK")){
             
            return $path;
            
          }else{
            if($link == 'https'){
               $url = substr_replace($path,'v2/', 24, 0);
             }else{
               $url = substr_replace($path,'v2/', 23, 0);
             }
           
            // $url = str_replace('v1/','',$path);
            return $url;
          }
        }
        
        
      
    }

}
if (!function_exists('buttonHtml')) {

    function buttonHtml($key, $link, $asso=[]) {
        $array = [
            "edit" => "<span class='f-left margin-r-5'><a data-toggle='tooltip'  class='btn btn-primary btn-xs' title='Edit' target='_blank' href='" . $link . "'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a></span>",
             "confirm" => "<span class='f-left margin-r-5'><a data-toggle='tooltip'  class='btn btn-primary btn-xs' title='Confirm Order' target='_blank' href='" . $link . "'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a></span>",
            "Active" => '<span class="f-left margin-r-5"> <a data-toggle="tooltip" class="btn btn-success btn-xs" title="Active" href="' . $link . '"><i class="fa fa-check" aria-hidden="true"></i></a></span>',
            "Inactive" => '<span class="f-left margin-r-5"> <a data-toggle="tooltip" class="btn btn-warning btn-xs" title="Inactive" href="' . $link . '"><i class="fa fa-times" aria-hidden="true"></i></a></span>',
           "Complete" => '<span class="f-left margin-r-5"> <a data-toggle="tooltip" class="btn btn-warning btn-xs" title="Complete Booking" target="_blank" href="' . $link . '"><i class="fa fa-check" aria-hidden="true"></i></a></span>',
           "leaves" => '<span class="f-left margin-r-5"> <a data-toggle="tooltip" class="btn btn-warning btn-xs" title="View Leaves" target="_blank" href="' . $link . '"><i class="fa fa-clock-o" aria-hidden="true"></i></a></span>',
           "Reschedule" => '<span class="f-left margin-r-5"> <a data-toggle="tooltip" class="btn btn-danger btn-xs" title="Reschedule Booking" target="_blank" href="' . $link . '"><i class="fa fa-calendar" aria-hidden="true"></i></a></span>',
            "delete" => '<form method="post" action="' . $link . '" accept-charset="UTF-8" style="display:inline" onsubmit="return deleteRow(this)"><input name="_method" value="DELETE" type="hidden">
            ' . csrf_field() . '<span><button data-toggle="tooltip"  title="Delete" type="submit" class="btn btn-danger btn-xs delete-action"><i class="fa fa-trash-o" aria-hidden="true"></i></button></span></form>',

            "delete_assoc" => '<form method="post" action="' . $link . '" accept-charset="UTF-8" style="display:inline" onsubmit=deleteRow(this,"'.$asso.'")><input name="_method" value="DELETE" type="hidden">
            ' . csrf_field() . '<span><button data-toggle="tooltip"  title="Delete" type="submit" class="btn btn-danger btn-xs delete-action"><i class="fa fa-trash-o" aria-hidden="true"></i></button></span></form>',


            "change-password" => '<span class="f-left margin-r-5"><a data-toggle="tooltip"  class="btn btn-danger btn-xs" title="Change Password" target="_blank" href="' . $link . '"><i class="fa fa-key" aria-hidden="true"></i></a></span>',
            "view" => '<span class="f-left margin-r-5"><a data-toggle="tooltip"  class="btn btn-info btn-xs" title="View Profile" target="_blank" href="' . $link . '"><i class="fa fa-eye" aria-hidden="true"></i></a></span>',

            "staff" => '<span class="f-left margin-r-5"><a data-toggle="tooltip"  class="btn btn-info btn-xs" title="View Staff" target="_blank" href="' . $link . '"><i class="fa fa-users" aria-hidden="true"></i></a></span>',

            "product" => '<span class="f-left margin-r-5"><a data-toggle="tooltip"  class="btn btn-info btn-xs" title="View Product" target="_blank" href="' . $link . '"><i class="fa fa-product-hunt" aria-hidden="true"></i></a></span>',

            "nominate" => '<span class="f-left margin-r-5"><a data-toggle="tooltip"  class="btn btn-info btn-xs" title="Nominate" target="_blank" href="' . $link . '"><i class="fa fa-trophy" aria-hidden="true"></i></a></span>',

            "services" => '<span class="f-left margin-r-5"><a data-toggle="tooltip"  class="btn btn-info btn-xs" title="Services" target="_blank" href="' . $link . '"><i class="fa fa-cogs" aria-hidden="true"></i></a></span>',
            "banner" => '<span class="f-left margin-r-5"><a data-toggle="tooltip"  class="btn btn-info btn-xs" title="Banner Images" target="_blank" href="' . $link . '"><i class="fa fa-file-image-o" aria-hidden="true"></i></a></span>',
            "chat" => '<span class="f-left margin-r-5"><a data-toggle="tooltip"  class="btn btn-info btn-xs" title="chat" target="_blank" href="' . $link . '"><i class="fa fa-comments-o" aria-hidden="true"></i></a></span>',

            "cancel" => '<span class="f-left margin-r-5"><a data-toggle="tooltip"  class="btn btn-danger btn-xs" title="Cancel Booking" target="_blank" href="' . $link . '"><i class="fa fa-window-close" aria-hidden="true"></i></a></span>',

            "reviews" => '<span class="f-left margin-r-5"><a data-toggle="tooltip"  class="btn btn-danger btn-xs" title="Reviews" target="_blank" href="' . $link . '"><i class="fa fa-star" aria-hidden="true"></i></a></span>',

            "view_settlement" => '<span class="f-left margin-r-5"><a data-toggle="tooltip"  class="btn btn-info btn-xs" title="View Settlement" target="_blank" href="' . $link . '"><i class="fa fa-eye" aria-hidden="true"></i></a></span>',
            "view_wallet" => '<span class="f-left margin-r-5"><a data-toggle="tooltip"  class="btn btn-info btn-xs" title="View Wallet" target="_blank" href="' . $link . '"><i class="fa fa-gift" aria-hidden="true"></i></a></span>',
            "view_promocode" => '<span class="f-left margin-r-5"><a data-toggle="tooltip"  class="btn btn-info btn-xs" title="View Code Uses" target="_blank" href="' . $link . '"><i class="fa fa-gift" aria-hidden="true"></i></a></span>',
            "view_lifetime" => '<span class="f-left margin-r-5"><a data-toggle="tooltip"  class="btn btn-info btn-xs" title=" View Life Time Earning " target="_blank" href="' . $link . '"><i class="fa fa-gift" aria-hidden="true"></i></a></span>',
        ];

        if (isset($array[$key])) {
            return $array[$key];
        }
        return '';
    }
}

/*
 * * Button With Html
 */
if (!function_exists('getButtons')) {


    function getButtons($array = []) {
        $html = '';
        foreach($array as $arr)
        {
          $offer_asso = isset($arr['asso']) ? $arr['asso'] : '';

            $html  .= buttonHtml($arr['key'],$arr['link'], $offer_asso);
        }
        return $html;
      
    }

}

if (!function_exists('loginData')) {


    function loginData() {
        if(Session::get('AdminLoggedIn')['user_id']){            
            $user_id = Session::get('AdminLoggedIn')['user_id'];
        }
        $row =  Admin::whereId($user_id)->first();
        return $row;
      
    }

}

if (!function_exists('spLoginData')) {


    function spLoginData() {
        if(Session::get('SpAdminLoggedIn')['user_id']){            
            $user_id = Session::get('SpAdminLoggedIn')['user_id'];
        }
        $row =  SpUser::whereId($user_id)->first();
        return $row;
      
    }

}

if (!function_exists('getStatusText')) {

    function getStatusText($current_status) {
      $html = '';
      switch ($current_status) {
          case '1':
              $html =  '<span class="f-left margin-r-5"><a data-toggle="tooltip"  class="btn btn-success btn-xs" title="Active">Approved</a></span>';
          break;
          case '0':
              $html =  '<span class="f-left margin-r-5"><a data-toggle="tooltip"  class="btn btn-info btn-xs" title="Inactive">Pending</a></span>';
          break;
          case '2':
              $html =  '<span class="f-left margin-r-5"><a data-toggle="tooltip"  class="btn btn-danger btn-xs" title="Rejected">Rejected</a></span>';
          break;
          default:
          break;
      }
      return $html;  
    }
}

if (!function_exists('getStatus')) {

    function getStatus($current_status,$id) {
       $html = '';
      switch ($current_status) {
          case '1':
                $html =  '<span class="f-left margin-r-5" id = "status_'.$id.'"><a data-toggle="tooltip"  class="btn btn-success btn-xs" title="Active" onClick="changeStatus('.$id.')" >Active</a></span>';
         
             
              break;
               case '0':
                $html =  '<span class="f-left margin-r-5" id = "status_'.$id.'"><a data-toggle="tooltip"  class="btn btn-danger btn-xs" title="Inactive" onClick="changeStatus('.$id.')" >Inactive</a></span>';

             
              break;
          
          default:
            
              break;
      }

      return $html;
      
    }

}

if (!function_exists('getApprovalStatus')) {

    function getApprovalStatus($current_status,$id) {
       $html = '';
      switch ($current_status) {
          case '1':
                $html =  '<span class="f-left margin-r-5" id = "approvalStatus_'.$id.'"><a data-toggle="tooltip"  class="btn btn-success btn-xs" title="Active" onClick="changeApproval('.$id.')" >Yes</a></span>';
         
             
              break;
               case '0':
                $html =  '<span class="f-left margin-r-5" id = "approvalStatus_'.$id.'"><a data-toggle="tooltip"  class="btn btn-danger btn-xs" title="Inactive" onClick="changeApproval('.$id.')" >No</a></span>';

             
              break;
          
          default:
            
              break;
      }
      return $html;  
    }
}

if (!function_exists('getFeaturedStatus')) {

    function getFeaturedStatus($current_status,$id) {
       $html = '';
      switch ($current_status) {
          case '1':
                $html =  '<span class="f-left margin-r-5" id = "featuredstatus_'.$id.'"><a data-toggle="tooltip"  class="btn btn-success btn-xs" title="Featured" onClick="changeFeaturedStatus('.$id.')" >Featured</a></span>';
         
             
              break;
               case '0':
                $html =  '<span class="f-left margin-r-5" id = "featuredstatus_'.$id.'"><a data-toggle="tooltip"  class="btn btn-danger btn-xs" title="Make Featured" onClick="changeFeaturedStatus('.$id.')" >Make Featured</a></span>';

             
              break;
          
          default:
            
              break;
      }

      return $html;
      
    }

}

if (!function_exists('returnColorCode')) {


    function returnColorCode($rating)
    {
       $color_arr = array();

        if($rating > 2.3 && $rating < 5){

            $color_arr = array("Needs Improvement","#f3001c");//red

        }if($rating > 4.9 && $rating <= 6){

            $color_arr = array("Average","#f7aa25");//orange

        }if($rating > 6 && $rating <= 7){

             $color_arr = array("Good","#60db57");//light green

        }if($rating > 7 && $rating <= 8){

            $color_arr = array("Very Good","#078229");//dark green

        }if($rating > 8 && $rating <= 9){

            $color_arr = array("Superb","#8bb9ff");//blue green

        }if($rating > 9 && $rating <= 10){
            $color_arr = array("Exceptional","#800080");//bright purple
        }
        return $color_arr;
    }

}

if (!function_exists('sendNotifications')) {


    function sendNotifications($rating)
    {
       
    }

}


// get unread chat

if (! function_exists('unreadChat')) {
  function unreadChat($userId, $table, $other_table)
  {
      return Chat::where('other_user_id',$userId)->where('other_user_id_tbl',$table)->where('user_id_tbl',$other_table)->where(function($q) {
          $q->where('read_status','0')->orWhere('read_status','-1');
      })->count();
  }
}

if (! function_exists('isValidYoutubeUrl')) {
  function isValidYoutubeUrl($url)
  {
      //return preg_match("/^(https?\:\/\/)?(www\.)?(youtube\.com|youtu\.be)\/watch\?v\=\w+$/", $url);
      return preg_match("/^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(\?\S*)?$/", $url);
  }
}

if (!function_exists('unique_slug')) {

    function unique_slug($name) {

      $slug = str_slug($name);

      if (empty($slug)){
          $string = mb_strtolower($name, "UTF-8");;
          $string = preg_replace("/[\/\.]/", " ", $string);
          $string = preg_replace("/[\s-]+/", " ", $string);
          $slug = preg_replace("/[\s_]/", '-', $string);
      }

      return $slug;
    }
}

if (! function_exists('str_slug')) {
    /**
     * Generate a URL friendly "slug" from a given string.
     *
     * @param  string  $title
     * @param  string  $separator
     * @param  string  $language
     * @return string
     */
    function str_slug($title, $separator = '-', $language = 'en')
    {
        return Str::slug($title, $separator, $language);
    }
}

// if ( ! function_exists('uploadwithresize'))
// {
//     function uploadwithresize($file,$path)
//     { 
//         $h=522;
//         $w= 522;
//         $orig_h = 630;
//         $orig_w = 1242;
//         $fileName = time().rand(111111111,9999999999).'.'.$file->getClientOriginalExtension();
//         $destinationPath    = 'public/sp_uploads/'.$path.'/';
//         $image_resize = Image::make($file->getRealPath())
//         ->resize($orig_w, $orig_h)
//         // original
//         ->save($destinationPath.$fileName)

//         // thumbnail
//         ->resize($w, $h)
//         ->save($destinationPath.'thumb/'.$fileName)
//         ->destroy();
//         return $fileName;
//     }
// }