<?php
namespace App\Http\Middleware;

use Closure;
use Session;

class SpAdminLoggedIn{
    public function handle($request, Closure $next){
        //dd(Session::get('LoggedIn'));
       
        if(Session::has('SpAdminLoggedIn'))
        {

            return $next($request);
        }
        else
        {
            return redirect()->route('spadmin.login');
        }

        
    }
}
