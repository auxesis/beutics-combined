<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
class Authentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{
            $token = JWTAuth::getToken();
            $user = JWTAuth::toUser($token);
            if(empty($user)){
                return response()->json(['loginstatus'=>false,'message'=>'Your account is deleted by admin']);
            }elseif(empty($user->status)){
                return response()->json(['loginstatus'=>false,'message'=>'Your account is deactivated by admin']);
            }
        }catch (JWTException $e) {
            if($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
                return response()->json(['status'=>false,'message'=>'token_expired']);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                return response()->json(['status'=>false,'message'=>'token_invalid']);
            }else{
                return response()->json(['status'=>false,'message'=>'Token is required']);
            }
        }
        
       return $next($request);
    }
}
