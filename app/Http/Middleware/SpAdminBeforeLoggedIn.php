<?php
namespace App\Http\Middleware;

use Closure;
use Session;

class SpAdminBeforeLoggedIn{
    public function handle($request, Closure $next){
        //dd(Session::get('LoggedIn'));
       
        if(!Session::has('SpAdminLoggedIn'))
        {

            return $next($request);
        }
        else
        {
            
            Session::flash('warning','Invalid request');
            return redirect()->route('spadmin.dashboard');
        }

        
    }
}
