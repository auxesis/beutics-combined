<?php

namespace App\Http\Controllers\Admin;
use App\Model\SpUser;
use App\Model\User;
use App\Model\WalletCashHistory;
use datatables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use DB;
use Illuminate\Support\Facades\Input;   
use Illuminate\Support\Facades\Hash;
use Session;

use App\Model\Conversation;
use App\Model\Chat;
use App\Model\Booking;
use App\Model\ECard;

class UserController extends Controller
{
    protected $title;
    protected $model;

    public function __construct()
    {
        $this->model = 'users';
        $this->title = 'Users';
        $this->module = 'users';
    } 


    public function index()
    {       
        $title = 'Customers';
        $module = 'users';
        $model = $this->model;
        $breadcum = [$title=>route($model.'.index')];
        return view('admin.users.index',compact('title','module','breadcum'));
    }

    public function getData(Request $request)
    {

        $columns = ['name','email','country_code','mobile_no','registration_type','created_at','status','action'];
        $totalData = User::count();
        
        $limit = $request->input('length');
        $start = $request->input('start');
        
        // $order = ($request->input('order.0.column')==7)?'unread_count':$columns[$request->input('order.0.column')];

        $unread = unreadChat(1,'admins','users');

        $order = ($request->input('order.0.column')==7 || $unread>0)?'unread_count':$columns[$request->input('order.0.column')];
        $dir = ($unread>0)?'desc':$request->input('order.0.dir');

        $AdminLoggedIn = session('AdminLoggedIn');
        $userId = $AdminLoggedIn['user_id'];
        $booking_id=0;

        $users = User::select(['*',DB::raw("(SELECT count(*) as total FROM chat WHERE ( ( chat.other_user_id ='".$userId."' AND chat.user_id =users.id AND chat.other_user_id_tbl='admins' AND chat.user_id_tbl='users') AND ( chat.read_status='0' OR chat.read_status='-1' )) AND (chat.booking_id='0')) as unread_count")]);
        // $totalFiltered = User::count();

        if (!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $users = $users->where(function($query) use ($search) {
                $query->where('name', 'LIKE', "%{$search}%")
                        ->orWhere('email', 'LIKE', "%{$search}%")
                        ->orWhere('updated_at', 'LIKE', "%{$search}%")
                        ->orWhere('mobile_no', 'LIKE', "%{$search}%")
                        ->orWhere('created_at', 'LIKE', "%{$search}%");
            });
        }

        if (!empty($request->input('verfication_filter'))) 
        {
            $filter_val = $request->input('verfication_filter');
           
            $users = $users->where(function($query) use ($filter_val) 
            {
                if($filter_val == 'verified'){
                    $query->whereNull('verifyToken')->whereNotNull('email');
                }else{
                    $query->whereNotNull('verifyToken')->whereNotNull('email');
                }
                
            });
        }

        $data_query_count = $users;
        $totalFiltered = $data_query_count->count();
        $users = $users->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        // print_r($users->toArray());die;

        $data = array();
        if (!empty($users)) {
            foreach ($users as $key => $row) {
                $verification_status = '-';
                if($row->email!='' && $row->email!=null){
                     if($row->verifyToken!='' && $row->verifyToken!=null){
                        $verification_status = '<b style="color:red;">(Unverified)</b>';
                    }else{
                        $verification_status = '<b style="color:green;">(Verified)</b>';
                    }
                }
               
                $nestedData['name'] = $row->name;
                $nestedData['email'] = $row->email.'<br/>'.$verification_status;
                $nestedData['country_code'] = $row->country_code;
                $nestedData['mobile_no'] = $row->mobile_no;
                $nestedData['registration_type'] = $row->registration_type; 
                $nestedData['status'] = getStatus($row->status,$row->id);
                $nestedData['created_at'] = date('d-M-y h:i:s',strtotime($row->created_at));
                $nestedData['action'] =  getButtons([
                                //['key'=>'edit','link'=>route('users.edit',$row->id)],
                                ['key'=>'view','link'=>route('users.view',$row->id)],
                                ['key'=>'change-password','link'=>route('users.change-password',$row->slug)],
                                 ['key'=>'view_wallet','link'=>route('users.view_wallet_transactions',$row->id)]
                            ]);

                $ccount="";

                if($row->unread_count>0){
                        $ccount='<span style="position: absolute;right: -10px;top: -16px;background-color: #449d44;width: 18px;color: #fff;border-radius: 50%;">'.$row->unread_count.'</span>';
                    }

                $nestedData['action'] .='<span class="f-left margin-r-5"  style="position:relative"><a data-toggle="tooltip"  class="btn btn-info btn-xs" title="chat" target="_blank" href="' . route('users.userChatHistory',$row->id) . '"><i class="fa fa-comments-o" aria-hidden="true"></i></a>'.$ccount.'</span>';

                $nestedData['referral'] = $referral = '-';
                if($row->referrer_id){
                    if($row->referrer_type == '0'){
                        $user_dt = User::where('id', $row->referrer_id)->first();
                        $referral = 'Customer: <b>'.ucfirst($user_dt['name']).'</b><br>Share Code: '.$user_dt['share_code'];
                    } elseif($row->referrer_type == '1'){
                        $user_dt = SpUser::where('id', $row->referrer_id)->first();
                        $referral = 'Service Provider: <b>'.ucfirst($user_dt['store_name']).'</b><br>Share Code: '.$user_dt['share_code'];

                    }
                }
                $nestedData['referral'] = $referral;
                //echo '<pre>'; print_r($nestedData);die;
                $data[] = $nestedData;
            }

        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function StatusUpdate(Request $request)
    {
        $user_id = $request->user_id;
        $row  = User::whereId($user_id)->first();
        $row->status =  $row->status=='1'?'0':'1';
        $row->save();
        $html = '';
        switch ($row->status) {
          case '1':
               $html =  '<a data-toggle="tooltip"  class="btn btn-success btn-xs" title="Active" onClick="changeStatus('.$user_id.')" >Active</a>';
              break;
               case '0':
               $html =  '<a data-toggle="tooltip"  class="btn btn-danger btn-xs" title="Inactive" onClick="changeStatus('.$user_id.')" >InActive</a>';
              break;
          
          default:
            
              break;
      }
      return $html;


    }

    public function arabicStatusUpdate(Request $request)
    {
        $user_id = $request->user_id;
        $row  = User::whereId($user_id)->first();
        $row->is_arabic_selected =  $row->is_arabic_selected=='1'?'0':'1';
        $row->save();
        $html = '';
        switch ($row->is_arabic_selected) {
          case '1':
               $html =  '<a data-toggle="tooltip"  class="btn btn-success btn-xs" title="Active" onClick="changeArabicStatus('.$user_id.')" >Active</a>';
              break;
               case '0':
               $html =  '<a data-toggle="tooltip"  class="btn btn-danger btn-xs" title="Inactive" onClick="changeArabicStatus('.$user_id.')" >InActive</a>';
              break;
          
          default:
            
              break;
      }
      return $html;


    }
    
    public function changePassword($slug, Request $request)
    {
        $title = 'Change Password';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        return view('admin.users.changePassword',compact('title','slug','model','breadcum'));
    }

    public function doUpdatePassword(Request $request)
    {
        $validation = array(
            //'old_password' => 'required',
            'new_password' => 'required|min:8',
            'confirm_password' => 'required|same:new_password',
        );
        $validator = Validator::make(Input::all(), $validation);
        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator->errors());
        }
        else
        {
            $user =  User::whereSlug($request->slug)->first();
            if ($user)
            {
                $user->password = Hash::make(Input::get('new_password'));
                $user->save();
                Session::flash('success', __('Password updated successfully.'));
                return redirect()->route('users.index');
            } 
            else
            {
                Session::flash('warning', __('User not found'));
                return redirect()->back();
            }
        }

    }

    public function delete($id) {
        $user = User::where('id', $id)->first();
        if ($user) {
            $user->delete();
             Session::flash('success', __('User deleted successfully.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('Invalid request.'));
            return redirect()->back();
        }
    }

    public function userChatHistory($id)
    {
        
        $user = User::where('id', $id)->first();

        if ($user) {

            $udata = $user->toArray();

            $title = $udata['name'];

            $AdminLoggedIn = session('AdminLoggedIn');
            $userId = $AdminLoggedIn['user_id'];
            $otherId = $udata['id'];
            $booking_id=0;
            
            $user_id_tbl = 'admins';
            $other_user_id_tbl = 'users';

            $groupId = ($userId>$otherId)?$userId."".$otherId."".$booking_id:$otherId."".$userId."".$booking_id;
            $groupId = 'AU-'.$groupId;
            //echo $groupId; die;
            Chat::where("group_id",$groupId)
            ->where("other_user_id_tbl",'admins')
            ->where("user_id_tbl",'users')
            ->update(['read_status'=>'1']);  

            $chats = Chat::where("group_id",$groupId)->where(function($q){
                $q->where(function($q){
                    $q->where("other_user_id_tbl",'admins')->where("user_id_tbl",'users');
                })->orWhere(function($q){
                    $q->where("other_user_id_tbl",'users')->where("user_id_tbl",'admins');
                });
            })
            //->toSql();
            ->get()
            ->toArray();  

            //print_r($chats); die;



            $other['id']          = $otherId;
            $other['image']       = (!empty($udata['image']))?changeImageUrlForFileExist(url('/public/'.$udata['image'])):'https://ptetutorials.com/images/user-profile.png';;
            $other['full_name']   = $udata['name'];
            // dd($udata);

            $module = 'users';
            $model = $this->model;
            $breadcum = [$title=>route($model.'.index')];
            
            $otherId = isset($otherId) ? $otherId : '';
            $userId = isset($userId) ? $userId : '';
            $user_id_tbl = isset($user_id_tbl) ? $user_id_tbl : '';
            $other_user_id_tbl = isset($other_user_id_tbl) ? $other_user_id_tbl : '';

            return view('admin.users.chat',compact('title','model','other','breadcum','chats', 'otherId', 'userId', 'user_id_tbl', 'other_user_id_tbl')); 

        }else{

            Session::flash('warning', __('Invalid request.'));
            return redirect()->route('users.index');

        }

    }

    public function view($id)
    {
        $title = 'View Profile';
        $module = $this->module;
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        $row = User::where('id',$id)->first();     
        return view('admin.users.view',compact('title','module','breadcum','row','tiers'));
    }


    public function view_wallet_transactions($id)
    {       
        $title = 'View Wallet Transactions';
        $module = 'users';
        $model = $this->model;
        $breadcum = ['Customers'=>route($model.'.index'),$title =>''];
        $row = User::where('id', '=', $id)->first();
        return view('admin.users.viewWalletTransactions',compact('title','module','breadcum', 'id','row'));
    }

    public function getWalletData(Request $request)
    {

        $columns = ['name','email','country_code','mobile_no','registration_type','created_at','status','action'];
                
        $limit = $request->input('length');
        $start = $request->input('start');
        
         if (!empty($request->input('id'))) {
            $id = $request->input('id');
            $rows = WalletCashHistory::where('user_id', $id);
        }

        $data_query_count = $rows;
        $totalFiltered = $data_query_count->count();
        $rows = $rows->offset($start)
                ->limit($limit)
                ->orderBy('id','DESC')
                ->get();
        // print_r($users->toArray());die;

        $data = array();
        if (!empty($rows)) {
            foreach ($rows as $key => $val) {
                $nestedData['s_no'] = $key+1;
                $nestedData['id'] = $val['id'];
                $nestedData['amount'] = round($val['amount'],2).' AED';
                $nestedData['transaction_type'] = $val['transaction_type']=='0'?'Credit':'Debit';
                $nestedData['description'] = ucfirst($val['description']);
                $nestedData['transaction_date_time'] = ($val->transaction_date_time!='' && $val->transaction_date_time!=null)? date('Y-m-d h:i A', strtotime($val->transaction_date_time)):'';

                // if(($val['description'] == "Refund" && $val['transaction_type'] == "0") || ($val['transaction_type'] == "1" && $val['description'] != "e-giftcard" )){

                //     // $nestedData['transaction_id'] = $val['trans_ref_id'];
                //     $nestedData['booking_unique_id'] = isset($val->getAssociatedBookingDetails->booking_unique_id)?$val->getAssociatedBookingDetails->booking_unique_id:'';
                //     // $nestedData['sp_id'] = isset($val->getAssociatedBookingDetails->sp_id)?$val->getAssociatedBookingDetails->sp_id:'';
                //     // $nestedData['sp_name'] = isset($val->getAssociatedBookingDetails->getAssociatedSpInformation->store_name)?$val->getAssociatedBookingDetails->getAssociatedSpInformation->store_name:'';
                    
                // }

                // elseif($val['description'] == "e-giftcard") {

                //     // $nestedData['transaction_id'] = $val['trans_ref_id'];
                //     $nestedData['booking_unique_id'] = $val->getAssociatedECardDetails->card_unique_id;
                //     if($val['transaction_type'] == "0"){
                //         $nestedData['user_name'] = $val->getAssociatedECardDetails->getAssociatedUserInfo->name;
                //     }
                //     // else{
                        
                //     //     if($val->getAssociatedECardDetails->receiver_id != "" && $val->getAssociatedECardDetails->receiver_id != null){

                //     //         $nestedData['user_name'] = $val->getAssociatedECardDetails->getAssociatedReceiverInfo->name;
                //     //     }else{
                //     //         $nestedData['user_name'] = $val->getAssociatedECardDetails->receiver_name;
                //     //     }
                        
                //     // }

                //     // $nestedData['gift_theme'] = ($val->getAssociatedECardDetails->getAssociatedOccasionThemeInfo->image!='' && $val->getAssociatedECardDetails->getAssociatedOccasionThemeInfo->image!=null)? asset('public/eCardThemes/'.$val->getAssociatedECardDetails->getAssociatedOccasionThemeInfo->image):'';
                //     // $nestedData['gift_message'] = $val->getAssociatedECardDetails->receiver_message;
                //     // $nestedData['gift_occasion'] = $val->getAssociatedECardDetails->getAssociatedOccasionInfo->name;
                    
                // }
                // else{
                //     // $nestedData['transaction_id'] = $val['trans_ref_id'];
                //     $nestedData['booking_unique_id'] = $val['booking_unique_id'];
                // }
                    $nestedData['booking_unique_id'] = "";
                if(($val['description'] == null || $val['description'] == '') && $val['transaction_type']!='0'){

                    $nestedData['booking_unique_id'] = "<a title='View' target='_blank' href='" . route('orders.view',[$val['trans_ref_id'],'']) . "'>".$val->getAssociatedBookingDetails->booking_unique_id."</a>";
          
                }
                elseif ($val['description'] == "e-giftcard" ) {

                   $nestedData['booking_unique_id'] = "<a title='View' target='_blank' href='" . route('e-card.show',[$val['trans_ref_id'],'']) . "'>".$val->getAssociatedECardDetails->card_unique_id."</a>";
                }else{
                    if($val['description'] == "Refund"){
                        $booking_row = Booking::where('id',$val['trans_ref_id'])->where('user_id',$val['user_id'])->where('status','2')->count();

                        $card_row = ECard::where('id',$val['trans_ref_id'])->where('user_id',$val['user_id'])->where('status','2')->count();

                        if($card_row>0){

                            // $nestedData['booking_unique_id'] = $val->getAssociatedECardDetails->card_unique_id;
                            $nestedData['booking_unique_id'] = "<a title='View' target='_blank' href='" . route('e-card.show',[$val['trans_ref_id'],'']) . "'>".$val->getAssociatedECardDetails->card_unique_id."</a>";
                        }else{
                            // $nestedData['booking_unique_id'] = $val->getAssociatedBookingDetails->booking_unique_id;
                            $nestedData['booking_unique_id'] = "<a title='View' target='_blank' href='" . route('orders.view',[$val['trans_ref_id'],'']) . "'>".$val->getAssociatedBookingDetails->booking_unique_id."</a>";
                        }
                    }
                }


                if($nestedData['booking_unique_id'] == ""){
                    $nestedData['booking_unique_id'] = 'NA';
                    if($val['transaction_type'] == "0" && $val['description'] == ""){
                       $nestedData['description'] = 'Top-up'; 
                    }
                }
                // if($nestedData['booking_unique_id']){
                //     if($val['description'] == "e-giftcard"){
                //         $nestedData['booking_unique_id'] = "<a title='View' target='_blank' href='" . route('e-card.show',[$nestedData['transaction_id'],'']) . "'>".$nestedData['booking_unique_id']."</a>";
                //     } else {
                //         $nestedData['booking_unique_id'] = "<a title='View' target='_blank' href='" . route('orders.view',[$nestedData['transaction_id'],'']) . "'>".$nestedData['booking_unique_id']."</a>";
                //     }
                   
                // } else {
                //     $nestedData['booking_unique_id'] = 'NA';
                //     if($val['transaction_type'] == "0" && $val['description'] == ""){
                //        $nestedData['description'] = 'Top-up'; 
                //     }
                // }

                $data[] = $nestedData;
            }

        }
        
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalFiltered),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }
    public function download_file(){

        $delimiter = ",";
        $filename = "customers_" . date('Y-m-d-H-i-s') . ".csv";
        
        //create a file pointer
        $f = fopen('php://memory', 'w');
        
        //set column headers
        $fields = array('ID', 'Name', 'Email', 'Country Code', 'Mobile', 'Gender', 'DOB', 'Registration Type (manual/facebook)', 'Refer Code Used', 'My Refer Code', 'Current Earning (AED)', 'Current Redemption (AED)', 'Current Wallet Amount (AED)', 'Notification Alert Status', 'Verification Status', 'Sign Up Date/Time');
        fputcsv($f, $fields, $delimiter);
        
        $users = User::where('status', '1')->get()->toArray();
       
        foreach($users as $row){
            $verification_status = '-';
                if($row['email']!='' && $row['email']!=null){
                     if($row['verifyToken']!='' && $row['verifyToken']!=null){
                        $verification_status = 'Unverified';
                    }else{
                        $verification_status = 'Verified';
                    }
                }

            $lineData = array(
                    $row['id'],
                    $row['name'], 
                    $row['email'], 
                    $row['country_code'], 
                    $row['mobile_no'], 
                    $row['gender'], 
                    $row['dob'], 
                    ucfirst($row['registration_type']), 
                    $row['refer_code'], 
                    $row['share_code'], 
                    round($row['earning'],2), 
                    round($row['redemption'],2), 
                    round($row['wallet_amount'],2), 
                    ($row['notification_alert_status'])?'Yes':'No', 
                    $verification_status, 
                    date('Y-m-d h:i A', strtotime($row['created_at']))
                );
            fputcsv($f, $lineData, $delimiter);
        }
        
        fseek($f, 0);
        
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="' . $filename . '";');
        
        //output all remaining data on a file pointer
        fpassthru($f);
        exit();
    }
}


