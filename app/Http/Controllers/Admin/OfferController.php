<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;   

use App\Model\Offer;
use App\Model\SpOffer;
use datatables;
use App;
use Session;

class OfferController extends Controller
{
    protected $title;
    protected $model;
    protected $module;

    public function __construct()
    {
        $this->title = 'Offers';
        $this->model = 'offer';
        $this->module = 'offer';
    } 

    public function index()
    {
        $title = 'Offers';
        $module = $this->module;
        $model = $this->model;
        $breadcum = [$title=>route($model.'.index')];
        return view('admin.offer.index',compact('title','model','module','breadcum'));
    }

    public function getData(Request $request)
    {

        $columns = ['title','title_ar','created_at','action'];
        $totalData = Offer::count();
        
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        $row = new Offer();
        // $totalFiltered = User::count();

        if (!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = $row->where(function($query) use ($search) {
                $query->where('title', 'LIKE', "%{$search}%")
                        ->orWhere('created_at', 'LIKE', "%{$search}%");
            });
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        // print_r($users);die;
        $data = array();
        if (!empty($row)) {
            foreach ($row as $key => $rows) {
                $nestedData['title'] = $rows->title;
                $nestedData['title_ar'] = $rows->title_ar;
                $associatedTables = SpOffer::where('offer_id',$rows->id)->count();
                $nestedData['created_at'] = date('d-M-y h:i:s',strtotime($rows->created_at));
                $nestedData['action'] =  getButtons([
                                ['key'=>'edit','link'=>route('offer.edit',$rows->id)],
                                ['key'=>'delete_assoc','link'=>route('offer.destroy',$rows->id), 'asso'=>$associatedTables],
                            ]);
                
                $data[] = $nestedData;
            }

        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

  
    public function create()
    {
        $action = "create";
        $title = 'Create Offer';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];

        return view('admin.offer.create',compact('action','title','breadcum','model','module'));
    }

    public function store(Request $request)
    {
        try{
            $validation = array(
                'title' => 'required',
                'title_ar' => 'required',
            );

            $validator = Validator::make(Input::all(), $validation);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
            } 
            else {
                $row = new Offer();
                
                $row->title = $request->title;
                $row->title_ar = $request->title_ar;

                $row->save();
                
                Session::flash('success', __('Offer added successfully.'));
                return redirect()->route('offer.index');
            }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

   
    public function show($id)
    {
        //
    }

 
    public function edit($id)
    {
        $rows = Offer::where('id',$id)->first();
        $title = 'Edit Offer';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        return view('admin.offer.edit',compact('rows','title','breadcum'));
    }

  
    public function update(Request $request, $id)
    {
        try{
            $validation = array(
                'title' => 'required',
                'title_ar' => 'required',
            );

            $validator = Validator::make(Input::all(), $validation);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
            } 
            else {
                $row = Offer::where('id',$id)->first();
                $row->title = $request->title;
                $row->title_ar = $request->title_ar;
                $row->save();
                
                Session::flash('success', __('Offer Updated successfully.'));
                return redirect()->route('offer.index');
            }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

    public function destroy($id)
    {
        $row = Offer::where('id', $id)->first();
        if ($row) {
            $row->delete();
             Session::flash('success', __('Offer Type deleted successfully.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('Invalid request.'));
            return redirect()->back();
        }
    }
}
