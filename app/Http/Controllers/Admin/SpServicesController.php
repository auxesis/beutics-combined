<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;   
use App\Model\SpService;
use App\Model\SpUser;
use datatables;
use App;
use Session;

class SpServicesController extends Controller
{
    protected $title;
    protected $model;

    public function __construct()
    {
        $this->title = 'Services';
        $this->model = 'sp';
    } 

    public function index($spid)
    {
        $title = 'Services';
        $model = $this->model;
        $user = SpUser::where('id',$spid)->select('name')->first();
           
        $breadcum = ['Service Provider' =>route($model.'.index'),$user->name =>''];
        return view('admin.spServices.index',compact('title','model','breadcum','spid'));
    }

    public function getData(Request $request)
    {
        $spid = $request->spid;
        $columns = ['service_id','service_id','created_at','action'];
        $totalData = SpService::where('user_id',$spid)->where('is_deleted','!=','1')->count();
        
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        $row = SpService::where('user_id',$spid)->where('is_deleted','!=','1')->select("sp_services.*");

        if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = $row->leftJoin('services', 'services.id', '=', 'sp_services.service_id')
                    ->where(function($query) use ($search) {
                        $query->Where('services.name', 'LIKE', "%{$search}%");
                        
                    });
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        // print_r($users);die;
        $data = array();
        if (!empty($row)) {
            foreach ($row as $key => $rows) {
                
                $nestedData['service_id'] = $rows->getAssociatedService->name;
                $nestedData['subcat_name'] = $rows->getServiceSubcategory->getSubcat->category_name;
                $nestedData['created_at'] = date('d-M-y h:i:s',strtotime($rows->created_at));
                $nestedData['action'] =  getButtons([
                                ['key'=>'view','link'=>route('sp.services.view',$rows->id)],
                            ]);
                
                $data[] = $nestedData;
            }

        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function view($id)
    {
        $title = 'View Services';
        $model = $this->model;

        $rows = SpService::where('id',$id)->first();
        $breadcum = ['Service Provider' =>route($model.'.index'),$rows->getAssociatedUsername->name =>route($model.'.service.index',$rows->user_id),$rows->getAssociatedService->name  =>''];
        
        return view('admin.spServices.view',compact('title','model','breadcum','rows'));
    }
}   
