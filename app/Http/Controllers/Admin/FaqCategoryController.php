<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;   

use App\Model\FaqCategory;
use App\Model\Faq;
use datatables;
use App;
use Session;

class FaqCategoryController extends Controller
{
    protected $title;
    protected $model;
    protected $module;

    public function __construct()
    {
        $this->title = 'Faq Category';
        $this->model = 'faq-category';
        $this->module = 'faq-category';
    } 

    public function index()
    {
        $title = 'Faq Category';
        $module = $this->module;
        $model = $this->model;
        $breadcum = [$title=>'Faq Categories'];        
        return view('admin.faq_category.index',compact('title','model','module','breadcum'));
    }

    public function getData(Request $request)
    {
        
        $columns = ['name','name_ar','created'];
        $totalData = FaqCategory::count();
        
        
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        $row = FaqCategory::select("faq_categories.*");
        
        if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = $row->Where('name', 'LIKE', "%{$search}%");
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('faq_categories.id', 'ASC')
                ->get();
      
        $data = array();
        if (!empty($row)) {
            foreach ($row as $key => $rows) {
                $nestedData['name'] = $rows->name;
                $nestedData['name_ar'] = $rows->name_ar;
                $nestedData['created'] = date('Y-m-d h:i A', strtotime($rows->created_at));
                $associatedTables = Faq::where('faq_category_id',$rows->id)->count();
                $nestedData['action'] =  getButtons([
                            ['key'=>'edit','link'=>route('faq-category.edit',$rows->id)],
                            ['key'=>'delete_assoc','link'=>route('faq-category.destroy',$rows->id), 'asso'=>$associatedTables],
                        ]);
                
                $data[] = $nestedData;
            }

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }
  
    public function create()
    {
        $action = "create";
        $title = 'Create Faq Category';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        return view('admin.faq_category.create',compact('action','title','breadcum','model'));
    }

    
    public function store(Request $request)
    {
        try{
             if (Input::isMethod('post')) {
                $validation = array(
                    'name'=>'required',
                    'name_ar'=>'required',
                );
                $validator = Validator::make(Input::all(), $validation);

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {
                    $row = new FaqCategory();
                    $row['name'] = $request->name;
                    $row['name_ar'] = $request->name_ar;
                    if($row->save())
                    {
                        Session::flash('success', __('Faq category added successfully.'));
                        return redirect()->route('faq-category.index');
                    }                    
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

     
    public function edit($id)
    {
        $rows = FaqCategory::where('id',$id)->first();
        $title = 'Edit Faq Category';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        return view('admin.faq_category.edit',compact('rows','title','breadcum'));
    }

   
    public function update(Request $request, $id)
    {
        try{
             if (Input::isMethod('PATCH')) {
                $validation = array(
                    'name'=>'required',
                    'name_ar'=>'required',
                );

                $validator = Validator::make(Input::all(), $validation);

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {
                    $row = FaqCategory::where('id',$id)->first();
                    $row['name'] = $request->name;
                    $row['name_ar'] = $request->name_ar;

                    if($row->save())
                    {
                        Session::flash('success', __('Faq category updated successfully.'));
                        return redirect()->route('faq-category.index');
                    }
                    
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

  
    public function destroy($id)
    {
        $row = FaqCategory::where('id', $id)->first();
        if ($row) {
            $row->delete();
             Session::flash('success', __('Faq category deleted successfully.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('Invalid request.'));
            return redirect()->back();
        }
    }
}
