<?php

namespace App\Http\Controllers\Admin;
use App\Model\User;
use datatables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use DB;
use Illuminate\Support\Facades\Input;   
use Illuminate\Support\Facades\Hash;
use Session;

use App\Model\Conversation;
use App\Model\Chat;

class UserController extends Controller
{
    protected $title;
    protected $model;

    public function __construct()
    {
        $this->model = 'users';
        $this->title = 'Users';
        $this->module = 'users';
    } 


    public function index()
    {   	
        $title = 'Customers';
        $module = 'users';
        $model = $this->model;
        $breadcum = [$title=>route($model.'.index')];
    	return view('admin.users.index',compact('title','module','breadcum'));
    }

    public function getData(Request $request)
    {

        $columns = ['name','email','country_code','mobile_no','registration_type','created_at','status','action'];
        $totalData = User::count();
        
        $limit = $request->input('length');
        $start = $request->input('start');
        
        // $order = ($request->input('order.0.column')==7)?'unread_count':$columns[$request->input('order.0.column')];

        $unread = unreadChat(1,'admins','users');

        $order = ($request->input('order.0.column')==7 || $unread>0)?'unread_count':$columns[$request->input('order.0.column')];
        $dir = ($unread>0)?'desc':$request->input('order.0.dir');

        $AdminLoggedIn = session('AdminLoggedIn');
        $userId = $AdminLoggedIn['user_id'];
        $booking_id=0;

        $users = User::select(['*',DB::raw("(SELECT count(*) as total FROM chat WHERE ( ( chat.other_user_id ='".$userId."' AND chat.user_id =users.id AND chat.other_user_id_tbl='admins' AND chat.user_id_tbl='users') AND ( chat.read_status='0' OR chat.read_status='-1' )) AND (chat.booking_id='0')) as unread_count")]);
        // $totalFiltered = User::count();

        if (!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $users = $users->where(function($query) use ($search) {
                $query->where('name', 'LIKE', "%{$search}%")
                        ->orWhere('email', 'LIKE', "%{$search}%")
                        ->orWhere('mobile_no', 'LIKE', "%{$search}%")
                        ->orWhere('updated_at', 'LIKE', "%{$search}%")
                        ->orWhere('created_at', 'LIKE', "%{$search}%");
            });
        }

        if (!empty($request->input('verfication_filter'))) 
        {
            $filter_val = $request->input('verfication_filter');
           
            $users = $users->where(function($query) use ($filter_val) 
            {
                if($filter_val == 'verified'){
                    $query->whereNull('verifyToken');
                }else{
                    $query->whereNotNull('verifyToken');
                }
                
            });
        }

        $data_query_count = $users;
        $totalFiltered = $data_query_count->count();
        $users = $users->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        // print_r($users->toArray());die;

        $data = array();
        if (!empty($users)) {
            foreach ($users as $key => $row) {

                if($row->verifyToken!='' && $row->verifyToken!=null){
                    $verification_status = '<b style="color:red;">(Unverified)</b>';
                }else{
                    $verification_status = '<b style="color:green;">(Verified)</b>';
                }

                $nestedData['name'] = $row->name;
                $nestedData['email'] = $row->email.'<br/>'.$verification_status;
                $nestedData['country_code'] = $row->country_code;
                $nestedData['mobile_no'] = $row->mobile_no;
                $nestedData['registration_type'] = $row->registration_type;
                $nestedData['status'] = getStatus($row->status,$row->id);
                $nestedData['created_at'] = date('d-M-y h:i:s',strtotime($row->created_at));
                $nestedData['action'] =  getButtons([
                                //['key'=>'edit','link'=>route('users.edit',$row->id)],
                                ['key'=>'view','link'=>route('users.view',$row->id)],
                                ['key'=>'change-password','link'=>route('users.change-password',$row->slug)],
                                // ['key'=>'chat','link'=>route('users.userChatHistory',$row->id)]
                            ]);

                $ccount="";

                if($row->unread_count>0){
                        $ccount='<span style="position: absolute;right: -10px;top: -16px;background-color: #449d44;width: 18px;color: #fff;border-radius: 50%;">'.$row->unread_count.'</span>';
                    }

                $nestedData['action'] .='<span class="f-left margin-r-5"  style="position:relative"><a data-toggle="tooltip"  class="btn btn-info btn-xs" title="chat" href="' . route('users.userChatHistory',$row->id) . '"><i class="fa fa-comments-o" aria-hidden="true"></i></a>'.$ccount.'</span>';
                
                $data[] = $nestedData;
            }

        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function StatusUpdate(Request $request)
    {
        $user_id = $request->user_id;
        $row  = User::whereId($user_id)->first();
        $row->status =  $row->status=='1'?'0':'1';
        $row->save();
        $html = '';
        switch ($row->status) {
          case '1':
               $html =  '<a data-toggle="tooltip"  class="btn btn-success btn-xs" title="Active" onClick="changeStatus('.$user_id.')" >Active</a>';
              break;
               case '0':
               $html =  '<a data-toggle="tooltip"  class="btn btn-danger btn-xs" title="Inactive" onClick="changeStatus('.$user_id.')" >InActive</a>';
              break;
          
          default:
            
              break;
      }
      return $html;


    }

    public function changePassword($slug, Request $request)
    {
        $title = 'Change Password';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        return view('admin.users.changePassword',compact('title','slug','model','breadcum'));
    }

    public function doUpdatePassword(Request $request)
    {
         $validation = array(
            //'old_password' => 'required',
            'new_password' => 'required|min:8',
            'confirm_password' => 'required|same:new_password',
        );
        $validator = Validator::make(Input::all(), $validation);
        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator->errors());
        }
        else
        {
            $user =  User::whereSlug($request->slug)->first();
            if ($user)
            {
                $user->password = Hash::make(Input::get('new_password'));
                $user->save();
                Session::flash('success', __('Password updated successfully.'));
                return redirect()->route('users.index');
            } 
            else
            {
                Session::flash('warning', __('User not found'));
                return redirect()->back();
            }
        }
    }

    public function delete($id) {
        $user = User::where('id', $id)->first();
        if ($user) {
            $user->delete();
             Session::flash('success', __('User deleted successfully.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('Invalid request.'));
            return redirect()->back();
        }
    }

    public function userChatHistory($id)
    {
        
        $user = User::where('id', $id)->first();

        if ($user) {

            $udata = $user->toArray();

            $title = $udata['name'];

            $AdminLoggedIn = session('AdminLoggedIn');
            $userId = $AdminLoggedIn['user_id'];
            $otherId = $udata['id'];
            $booking_id=0;

            $groupId   = ($userId>$otherId)?$userId."".$otherId."".$booking_id:$otherId."".$userId."".$booking_id;

            Chat::where("group_id",$groupId)->where("other_user_id_tbl",'admins')->where("user_id_tbl",'users')->update(['read_status'=>'1']);  

            $chats = Chat::where("group_id",$groupId)->get()->toArray();  

            $other['id']          = $otherId;
            $other['image']       = (!empty($udata['image']))?url('/public/'.$udata['image']):'https://ptetutorials.com/images/user-profile.png';;
            $other['full_name']   = $udata['name'];
            // dd($udata);

            $module = 'users';
            $model = $this->model;
            $breadcum = [$title=>route($model.'.index')];
            return view('admin.users.chat',compact('title','model','other','breadcum','chats')); 

        }else{

            Session::flash('warning', __('Invalid request.'));
            return redirect()->route('users.index');
        }
    }

    public function view($id)
    {
        $title = 'View Profile';
        $module = $this->module;
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        $row = User::where('id',$id)->first();     
        return view('admin.users.view',compact('title','module','breadcum','row','tiers'));
    }
}
