<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;   

use App\Model\Banner;
use datatables;
use App;
use Session;
use DB;

class BannersController extends Controller
{
    public function __construct()
    {
        $this->title = 'Banners';
        $this->model = 'banners';
        $this->module = 'banners';
    } 

    public function index()
    {
        $title = __('messages.View Banners');
        $module = $this->module;
        $model = $this->model;
        $breadcum = [$title=>route($model.'.index')];
        return view('admin.banner.index',compact('title','model','module','breadcum'));
    }

    public function getData(Request $request)
    {
        $columns = ['page_name','page_section_name','title','sub_title','image'];
        $totalData = Banner::where('banners.status','1')->count();

        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');
        $row = Banner::select('banners.*')->where('banners.status','1');

        if (!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = $row->where(function($query) use ($search) {
                    $query->Where('title', 'LIKE', "%{$search}%")
                    ->orWhere('page_name', 'LIKE', "%{$search}%")
                    ->orWhere('page_section_name', 'LIKE', "%{$search}%");
            });
        }
        //echo $row->toSql(); exit;
        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('id', 'desc')
                ->get();

        $data = array();
        if (!empty($row)) {
            foreach ($row as $key => $rows) 
            {
                $nestedData['page_name'] = ucwords(str_replace('-',' ',$rows->page_name));
                $nestedData['page_section_name'] = $rows->page_section_name;
                $nestedData['title'] = $rows->title;
                $nestedData['sub_title'] = $rows->sub_title;
                $nestedData['image'] = ($rows->image !='') ? '<img src="'.changeImageUrlForFileExist(asset("sp_uploads/banner_images/".$rows->page_name."/".$rows->image)).'" width="60" height="50"/>' : '<img src="'.asset("images/defualt.jpeg").'" alt="..."" height="50" width="60">';
                
                $nestedData['sort_order'] = $rows->sort_order;              
                $nestedData['status'] = $nestedData['status'] = getStatus($rows->status,$rows->id);             
                $nestedData['action'] =  getButtons([
                                ['key'=>'edit','link'=>route('banners.edit',[$rows->id])],
                                ['key'=>'delete','link'=>route('banners.destroy',$rows->id)],
                            ]);
                
                $data[] = $nestedData;
            }

        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }
  
    public function create()
    {
        $action = "create";
        $title = __('messages.Create Banner');
        $model = $this->model;
        $breadcum = [__('messages.Banner')=>route($model.'.index'),$title =>''];
        $all_page_sections  = Banner::orderby('page_section_name', 'asc')->pluck('page_section_name', 'page_section_name')->toArray();
        $page_name_array = array(
        	"home"=>"Home",
        	"landing-beauty" => "Landing Beauty",
        	"landing-fitness" => "Landing Fitness",
        	"landing-wellness" => "Landing Wellness",
        	"listing-beauty" => "Listing Beauty",
        	"listing-fitness" => "Listing Fitness",
        	"listing-wellness" => "Listing Wellness"
        );
        return view('admin.banner.create',compact('action','title','breadcum','model','all_page_sections','page_name_array'));
    }
    
    public function store(Request $request)
    {
        try{
             if (Input::isMethod('post')) {
                $validation = array(
                    'page_name' => 'required',
                    'page_section_name'=>'required',
                    'title' => 'required',
                    'image' => 'required',
                );

                $desc = explode(' ',$request->description);
                $validator = Validator::make(Input::all(), $validation);

                if(count($desc)>500){
                     $validator -> errors() -> add('description', 'Description cannot be more than 500 words.');
                     return redirect()->back()
                    -> withErrors($validator)
                    -> withInput(Input::all());
                }

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {
                   
                    $row = new Banner();
                    
                    if ($request->hasFile("image"))
                    {
                    	$file = $request->image;
                        $name = time().str_random(2).'.'.$file->getClientOriginalExtension();
                        $destinationPath = public_path('/sp_uploads/banner_images/'.$request->page_name.'/');
                        $img = $file->move($destinationPath, $name);
                        $row->image = $name;
                    }
                    
                    $row->page_name = $request->page_name;
                    $row->page_section_name = ucwords($request->page_section_name);
                    $row->title = ucfirst($request->title);
                    $row->sub_title = ucfirst($request->sub_title);
                    $row->link = $request->link;
                    $row->description = $request->description;
                    $row->sub_description = $request->sub_description;
                    $row->sort_order = $request->sort_order;
                    $row->save();

                    if($row->save())
                    {
                        Session::flash('success', __('messages.Banner added successfully.'));
                        return redirect()->route('banners.index');
                    }
                    
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

   
    public function show($id)
    {
        //
    }

   	public function StatusUpdate(Request $request)
    {
        $banner_id = $request->banner_id;
        $row  = Banner::whereId($banner_id)->first();
        $row->status =  $row->status=='1'?'0':'1';
        $row->save();
        $html = '';
        switch ($row->status) {
          case '1':
               $html =  '<a data-toggle="tooltip"  class="btn btn-success btn-xs" title="Active" onClick="changeStatus('.$banner_id.')" >Active</a>';
              break;
               case '0':
               $html =  '<a data-toggle="tooltip"  class="btn btn-danger btn-xs" title="Inactive" onClick="changeStatus('.$banner_id.')" >InActive</a>';
              break;
          
          default:
            
              break;
      }
      return $html;


    }

    public function edit($id)
    {
        $rows = Banner::where('id',$id)->first();
        $title = __('messages.Edit Banner');
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        $all_page_sections  = Banner::orderby('page_section_name', 'asc')->pluck('page_section_name', 'page_section_name')->toArray();
        $page_name_array = array(
        	"home"=>"Home",
        	"landing-beauty" => "Landing Beauty",
        	"landing-fitness" => "Landing Fitness",
        	"landing-wellness" => "Landing Wellness",
        	"listing-beauty" => "Listing Beauty",
        	"listing-fitness" => "Listing Fitness",
        	"listing-wellness" => "Listing Wellness"
        );
        return view('admin.banner.edit',compact('rows','title','breadcum','all_page_sections','page_name_array'));
    }

   
    public function update(Request $request, $id)
    {   //dd($request); exit;
        try{
             if (Input::isMethod('PATCH')) {
                $validation = array(
                    'page_name' => 'required',
                    'page_section_name'=>'required',
                    'title' => 'required',
                    //'image' => 'required',
                );
                
                $desc = explode(' ',$request->description);
                $validator = Validator::make(Input::all(), $validation);

                if(count($desc)>500){
                     $validator -> errors() -> add('description', 'Description cannot be more than 500 words.');
                     return redirect()->back()
                    -> withErrors($validator)
                    -> withInput(Input::all());
                }

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {
                    $row = Banner::where('id',$id)->first();
                    
                    if ($request->hasFile("image"))
                    {
                    	$file = $request->image;
                        $name = time().str_random(2).'.'.$file->getClientOriginalExtension();
                        $destinationPath = public_path('/sp_uploads/banner_images/'.$request->page_name.'/');
                        $img = $file->move($destinationPath, $name);
                        $row->image = $name;
                    }
                    
                    $row->page_name = $request->page_name;
                    $row->page_section_name = ucwords($request->page_section_name);
                    $row->title = ucfirst($request->title);
                    $row->sub_title = ucfirst($request->sub_title);
                    $row->link = $request->link;
                    $row->description = $request->description;
                    $row->sub_description = $request->sub_description;
                    $row->sort_order = $request->sort_order;
                    $row->save();
                   
                    if($row->save())
                    {
                        Session::flash('success', __('messages.Banner updated successfully.'));
                        return redirect()->route('banners.index');
                    }
                    
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

  
    public function destroy($id)
    {
        $row = Banner::where('id', $id)->first();
        if ($row) {
            $row->delete();
             Session::flash('success', __('messages.Banner deleted successfully.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('messages.Invalid request.'));
            return redirect()->back();
        }
    }
}
