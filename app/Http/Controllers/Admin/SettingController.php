<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;   

use App\Model\Setting;
use datatables;
use App;
use Session;

class SettingController extends Controller
{
    
    protected $title;
    protected $model;
    protected $module;

    public function __construct()
    {
        $this->title = 'Settings';
        $this->model = 'setting';
        $this->module = 'setting';
    } 

    public function index()
    {
        $title = $this->title;
        $module = $this->module;
        $model = $this->model;
        $breadcum = [$title=>route($model.'.index')];
        $row = Setting::first();
        return view('admin.setting.index',compact('title','model','module','breadcum','row'));
    }

    public function edit($id)
    {
        $row = Setting::where('id',$id)->first();
        $title = 'Edit Setting';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        return view('admin.setting.edit',compact('row','title','breadcum'));
    }

  
    public function update(Request $request,$id)
    {
        try{
             
            $validation = array(
                'percentage' => 'required',
                'referred_cashback' => 'required',
                'contact_sync_cashback' => 'required',
                'appointment_date_selection' => 'required',
                'gift_cancellation_days' => 'required',
                'order_expiration_days' => 'required',
                'customer_android_app_version' => 'required',
                'customer_ios_app_version' => 'required',
                'customer_android_app_force' => 'required',
                'customer_ios_app_force' => 'required',
                'sp_android_app_version' => 'required',
                'sp_ios_app_version' => 'required',
                'sp_android_app_force' => 'required',
                'sp_ios_app_force' => 'required',
            );
        
            $validator = Validator::make(Input::all(), $validation);
           
            if ($validator->fails()) {

                return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
            } 
            else {
                $row = Setting::where('id', $id)->first();
                $row->percentage = $request->percentage;
                $row->referred_cashback = $request->referred_cashback;
                $row->contact_sync_cashback = $request->contact_sync_cashback;
                $row->appointment_date_select = $request->appointment_date_selection;
                $row->gift_cancellation_days = $request->gift_cancellation_days;
                $row->order_expiration_days = $request->order_expiration_days;
                $row->customer_android_app_version = $request->customer_android_app_version;
                $row->customer_ios_app_version = $request->customer_ios_app_version;
                $row->customer_android_app_force = $request->customer_android_app_force;
                $row->customer_ios_app_force = $request->customer_ios_app_force;
                $row->sp_android_app_version = $request->sp_android_app_version;
                $row->sp_ios_app_version = $request->sp_ios_app_version;
                $row->sp_android_app_force = $request->sp_android_app_force;
                $row->sp_ios_app_force = $request->sp_ios_app_force;
                
                $row->save();
                
                Session::flash('success', __('Setting Updated successfully.'));
                return redirect()->route('setting.index');
            }
             
        }
        catch( \Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

}
