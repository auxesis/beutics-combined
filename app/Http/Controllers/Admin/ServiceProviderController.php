<?php

namespace App\Http\Controllers\Admin;
use datatables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use DB;
use Illuminate\Support\Facades\Input;   
use Illuminate\Support\Facades\Hash;
use Intervention\Image\ImageManagerStatic as Image;
use Session;
use App\Model\SpUser;
use App\Model\Tier;
use App\Model\Category;
use App\Model\SpAmenity;
use App\Model\Amenity;
use App\Model\SpBannerImages;
use App\Model\Conversation;
use App\Model\Chat;
use App\Model\Tag;
use App\Model\Booking;
use App\Model\TrailSession;
use App\Model\SpCountry;


class ServiceProviderController extends Controller
{
    protected $title;
    protected $model;

    public function __construct()
    {
        $this->model = 'sp';
        $this->title = 'Service Provider';
        $this->module = 'service-provider';
    } 

    public function index()
    {       
        $title = $this->title;
        $module = $this->module;
        $model = $this->model;
        $breadcum = [$title=>route($model.'.index')];
        return view('admin.serviceProvider.index',compact('title','module','breadcum'));
    }
 
    public function getData(Request $request)
    {
        $columns = ['name','email','store_name','mobile_no','created_at','status','is_tax_applied','is_approved','action','is_featured'];
        $totalData = SpUser::count();
        $limit = $request->input('length');
        $start = $request->input('start');
        // $order = $columns[$request->input('order.0.column')];

        $unread = unreadChat(1,'admins','sp_users');

        $order = ($request->input('order.0.column')==7 || $unread>0)?'unread_count':'id';
        $dir = ($request->input('order.0.column')==7 || $unread>0)?'desc':$request->input('order.0.dir');
        // $users = new SpUser;

        $AdminLoggedIn = session('AdminLoggedIn');
        $userId = $AdminLoggedIn['user_id'];
        $booking_id=0;

        $users = SpUser::select(['*',DB::raw("(SELECT count(*) as total FROM chat WHERE ( ( chat.other_user_id ='".$userId."' AND chat.user_id =sp_users.id AND chat.other_user_id_tbl='admins' AND chat.user_id_tbl='sp_users') AND ( chat.read_status='0' OR chat.read_status='-1' )) AND (chat.booking_id='0')) as unread_count")]);

        // $totalFiltered = User::count();
        if (!empty($request->input('search.value'))) 
        {
            $search = $request->input('search.value');
            $users = $users->where(function($query) use ($search) {
            $query->where('name', 'LIKE', "%{$search}%")
                  ->orWhere('email', 'LIKE', "%{$search}%")
                  ->orWhere('mobile_no', 'LIKE', "%{$search}%")
                  ->orWhere('store_name', 'LIKE', "%{$search}%")
                  ->orWhere('updated_at', 'LIKE', "%{$search}%")
                  ->orWhere('created_at', 'LIKE', "%{$search}%");
            }); 
        }

        if (!empty($request->input('status_filter'))) 
        {
            $is_approvedsearch = $request->input('status_filter');
            $is_approvedsearch_1 = '';
            if($is_approvedsearch == 'pending')
            {
                $is_approvedsearch_1 = '0';
            }
            if($is_approvedsearch == 'approved')
            {
                $is_approvedsearch_1 = '1';
            }
            $users = $users->where(function($query) use ($is_approvedsearch_1) 
            {
                $query->where('is_approved', $is_approvedsearch_1);
            });
        }
        if (!empty($request->input('visibility_filter'))) 
        {
            $is_approvedsearch = $request->input('visibility_filter');
            $is_approvedsearch_1 = '';
            if($is_approvedsearch == 'inactive')
            {
                $is_approvedsearch_1 = '0';
            }
            if($is_approvedsearch == 'active')
            {
                $is_approvedsearch_1 = '1';
            }
            $users = $users->where(function($query) use ($is_approvedsearch_1) 
            {
                $query->where('status', $is_approvedsearch_1);
            });
        }

        if (!empty($request->input('verfication_filter'))) 
        {
            $filter_val = $request->input('verfication_filter');
           
            $users = $users->where(function($query) use ($filter_val) 
            {
                if($filter_val == 'verified'){
                    $query->whereNull('verifyToken');
                }else{
                    $query->whereNotNull('verifyToken');
                }
                
            });
        }
        
        $data_query_count = $users;
        $totalFiltered = $data_query_count->count();
        $users = $users->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        // print_r($users);die;
        $data = array();
        if (!empty($users)) {
            foreach ($users as $key => $row) {
                if($row->verifyToken!='' && $row->verifyToken!=null){
                    $verification_status = '<b style="color:red;">(Unverified)</b>';
                }else{
                    $verification_status = '<b style="color:green;">(Verified)</b>';
                }
                
                $nestedData['name'] = $row->name;
                $nestedData['email'] = $row->email.'<br/>'.$verification_status;
                $nestedData['store_name'] = $row->store_name;
                $nestedData['mobile_no'] = $row->mobile_no;
                $nestedData['status'] = getStatus($row->status,$row->id);
                $nestedData['is_featured'] = getFeaturedStatus($row->is_featured,$row->id);
                $nestedData['is_tax_applied'] = getApprovalStatus($row->is_tax_applied,$row->id);
                $nestedData['created_at'] = date('d-M-y h:i:s',strtotime($row->created_at));
                $nestedData['action'] =  getButtons([
                                // ['key'=>'chat','link'=>route('sp.spuserChatHistory',$row->id)],
                                ['key'=>'view','link'=>route('sp.view',$row->id)],
                                ['key'=>'change-password','link'=>route('sp.change-password',$row->slug)],
                                ['key'=>'edit','link'=>route('sp.edit',$row->id)],
                                ['key'=>'staff','link'=>route('staff.index',['spid'=>$row->id])],
                                ['key'=>'product','link'=>route('product.index',['spid'=>$row->id])],
                                ['key'=>'services','link'=>route('sp.service.index',['spid'=>$row->id])],
                                ['key'=>'banner','link'=>route('sp.service.banner',['spid'=>$row->id])],
                                ['key'=>'reviews','link'=>route('sp.overall.reviews',[$row->id])],
                                ['key'=>'view_lifetime','link'=>route('sp.lifetime',[$row->id])],

                            ]);

                $ccount="";

                if($row->unread_count>0){
                        $ccount='<span style="position: absolute;right: -10px;top: -16px;background-color: #449d44;width: 18px;color: #fff;border-radius: 50%;">'.$row->unread_count.'</span>';
                    }

                $nestedData['action'] .='<span class="f-left margin-r-5"  style="position:relative"><a data-toggle="tooltip"  class="btn btn-info btn-xs" title="chat" target="_blank" href="' . route('sp.spuserChatHistory',$row->id) . '"><i class="fa fa-comments-o" aria-hidden="true"></i></a>'.$ccount.'</span>';

                $data[] = $nestedData;
            }
        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function FeaturedStatusUpdate(Request $request)
    {
        $user_id = $request->user_id;
        $row  = SpUser::whereId($user_id)->first();
        $row->is_featured =  $row->is_featured=='1'?'0':'1';
        $row->save();
        $html = '';
        switch ($row->is_featured) {
          case '1':
               $html =  '<a data-toggle="tooltip"  class="btn btn-success btn-xs" title="Featured" onClick="changeFeaturedStatus('.$user_id.')" >Featured</a>';
              break;
               case '0':
               $html =  '<a data-toggle="tooltip"  class="btn btn-danger btn-xs" title="Make Featured" onClick="changeFeaturedStatus('.$user_id.')" >Make Featured</a>';
              break;
          
          default:
            
              break;
      }
      return $html;
    }

    public function showLifeTime($id)
    {
        $title = 'View Life Time Earning';
        $module = $this->module;
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        $total_lt_online_sales = Booking::where('sp_id',$id)
                ->where('status','5')
                ->where('payment_type','0')
                ->where('payment_settlement_status','paid')
                ->select(DB::raw('sum(CASE 
                        WHEN IFNULL(final_settlement_amount,0) = 0 THEN total_item_amount 
                        ELSE final_settlement_amount
                        END) AS total_online_sale'))
                ->first();
                // echo $total_online_sales;die;

          $total__lt_offline_sales = Booking::where('sp_id',$id)
                ->where('status','5')
                ->where('payment_settlement_status','paid')
                ->where('payment_type','1')
                ->select(DB::raw('sum(CASE 
                        WHEN IFNULL(final_settlement_amount,0) = 0 THEN total_item_amount 
                        ELSE final_settlement_amount
                        END) AS total_offline_sale'))
                ->first();
          $life_time_earning = $total_lt_online_sales['total_online_sale'] + $total__lt_offline_sales['total_offline_sale'];   
          $total_online = ($total_lt_online_sales['total_online_sale'])?$total_lt_online_sales['total_online_sale']:'0'; 
          $total_offline = ($total__lt_offline_sales['total_offline_sale'])?$total__lt_offline_sales['total_offline_sale']:'0'; 
        return view('admin.serviceProvider.showLifeTime',compact('title','module','breadcum','total_online','total_offline', 'life_time_earning'));
    }

    public function view($id)
    {
        $title = 'View Profile';
        $module = $this->module;
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        $row = SpUser::leftJoin('trail_sessions', function($join) {
                  $join->on('sp_users.trail_session_id', '=', 'trail_sessions.id');
                })->leftJoin('sp_countries', function($join) {
                  $join->on('sp_countries.user_id', '=', 'sp_users.id');
                })->where('sp_users.id',$id)->first();
        $tiers = $this->getTierName();       
        return view('admin.serviceProvider.view',compact('title','module','breadcum','row','tiers'));
    }

    public function edit($id)
    {
        $row = SpUser::where('id',$id)->first();
        $title = 'Edit Service Provider';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        $categories = $this->getCategoryName();
        $tiers = $this->getTierName();    
        $user_amenities = SpAmenity::where('user_id',$id)->get();
        $usr_amen = $session_list = array();
        foreach($user_amenities as $usr_am)
        {
            $usr_amen[] = $usr_am['amenity_id'];
        }
        $amenities = $this->getAmenities();
        $banner_images = SpBannerImages::where('user_id',$id)->get();
        $trail_sessions = TrailSession::select('id','name')->get();
        $tags_arr = Tag::orderby('tag_name','Asc')->pluck('tag_name','id')->toArray();
        $selected_tags = explode(',',$row->tag_id);
        foreach($trail_sessions as $rows){
            $session_list[$rows['id']] = $rows['name'];
        }
        $country_list = $this->getmapcountry();
        $user_countries = SpCountry::where('user_id',$id)->first();
        return view('admin.serviceProvider.edit',compact('row','title','breadcum','categories','tiers','amenities','usr_amen','banner_images','session_list','country_list','user_countries','tags_arr','selected_tags'));
    }

    public function update(Request $request)
    {
        try{
            $validation = array(
              'is_individual' => 'required',
              'category_id' => 'required',
              'store_name' => 'required|max:50',
              'store_number' => 'required|max:50',
              'name' => 'required|max:50',
              'address' => 'required',
              'latitude' => 'required',
              'longitude' => 'required',
              'email' => 'email|unique:sp_users,email,'.$request->id,
              'country_code' => 'required|exists:countries,phonecode',
              'mobile_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:16|min:8|unique:sp_users,mobile_no,'.$request->id,
              'whatsapp_no' => 'regex:/^([0-9\s\-\+\(\)]*)$/|max:16|min:8',
              'agreement' => 'mimes:pdf',

              // 'landmark' => 'max:50',
              'service_criteria' => 'required',
              'amenity' => 'required',
              'profile_image' => 'mimes:jpeg,jpg,png,gif',
              'store_image' => 'mimes:jpeg,jpg,png,gif',
              'country'=>'required',
              'delivery_mode' => 'required'
            );

            $validator = Validator::make(Input::all(), $validation);

            if ($validator->fails()) 
            {
                return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
            } 
            else 
            {
                $user_id = $request->id;
                $user = SpUser::where('id', $request->id)->first();
                $user->name = $request->name;
                $user->is_individual = $request->is_individual;
                $user->is_verified = $request->is_verified;
                $user->is_virtual_tour = $request->is_virtual_tour;
                $user->is_free_trial = $request->is_free_trial;
                $user->service_criteria = $request->service_criteria;
                $user->category_id = $request->category_id;
                $user->tier_id = $request->tier_id;
                $user->store_name = $request->store_name;
                $user->store_url = unique_slug($request->store_name);
                $user->store_number = $request->store_number;
                $user->country_code = $request->country_code;
                $user->email = $request->email;  
                $user->mobile_no = $request->mobile_no;
                $user->whatsapp_no = $request->whatsapp_no;
                $user->address = $request->address;
                $user->landmark = $request->landmark;
                $user->latitude = $request->latitude;
                $user->longitude = $request->longitude;
                $user->description = $request->description;
                $user->session_description = $request->session_description;
                $user->session_sub_description = $request->session_sub_description;
                $user->service_criteria = implode(',',$request->service_criteria);
                $user->trail_session_id = $request->trail_session;
                $user->delivery_mode = implode(',',$request->delivery_mode);
                $user->sub_locality = $request->sub_locality;
                $user->tag_id = ($request->tags) ? implode(',',$request->tags) : '';
                $user->price = $request->price;

                if ($request->hasFile('agreement') && $request->file('agreement'))
                {
                    $file = $request->file('agreement');
                    $name = time().'.'.$file->getClientOriginalExtension();
                    $destinationPath = public_path('/sp_uploads/approve_pdf/');
                    $img = $file->move($destinationPath, $name);
                    $user->agreement = $name;
                } 
                
                if ($request->hasFile("store_image"))
                {
                    $file = $request->store_image;
                    $name = time().str_random(2).'.'.$file->getClientOriginalExtension().'.png';
                    $destinationPath = public_path('/sp_uploads/store/');
                    $img = $file->move($destinationPath, $name);
                    $user->store_image = $name;
                }

                if ($request->hasFile('store_image') && $request->file('store_image'))
                {
                    $image = $request->image_cr;
                    list($type, $image) = explode(';', $image);
                    list(, $image)      = explode(',', $image);
                    $image = base64_decode($image);
                    $image_name= time().'.png';
                    $path = public_path('/sp_uploads/store/'.$image_name);
                    file_put_contents($path, $image);
                    $user->store_image = $image_name;
                } 

                $amenities = $request->amenity;
                foreach($amenities as $am){
                    SpAmenity::manageAmenities($user_id, $am,'remove');
                }
                foreach($amenities as $am){
                    SpAmenity::manageAmenities($user_id, $am,'add');
                }

                if($request->banner_path){

                    $count = count($request->banner_path);
                
                    for($i=0;$i< $count;$i++){
           
                        $img = $request->banner_path[$i];
                        if ($img)
                        {
                            $file = $img;
                            $name = time().str_random(2).'.'.$file->getClientOriginalExtension();
                            $destinationPath = public_path('sp_uploads/banner_images/');
                            $img = $file->move($destinationPath, $name);
                            $row = new SpBannerImages();
                            $row->user_id = $user_id;
                            $row->banner_path = $name;
                            $row->save();
                        }
                    }
                } 

                if($request->country)
                { 
                    $country_code = $country_name = $state_code = $state_name = $city_name = ''; 
                    $city_code = 0;       
                    $get_country = explode('/',$request->country);
                    $country_code = $get_country[0];
                    $country_name = $get_country[1];

                    if(isset($request->state) && $request->state !='' && $request->state !=null)
                    {
                        $get_state = explode('/',$request->state);
                        $state_code = $get_state[0];
                        $state_name = $get_state[1];
                    }
                    
                    if(isset($request->city) && $request->city !='' && $request->city !=null)
                    {
                        $get_city = explode('/',$request->city);
                        $city_code = $get_city[0];
                        $city_name = $get_city[1];
                    }
                    
                    $attribute = ['user_id' => $user_id];
                    $values = ['country_isocode' => $country_code,'country_name'=> $country_name, 'state_isocode' => $state_code,'state_name' => $state_name,'city_isocode' => $city_code,'city_name' =>$city_name];

                    $maprow = SpCountry::where('user_id', $user_id);
                    if($maprow->count() !=0)
                    {
                        $maprow->update($values);
                    }
                    else
                    {
                        SpCountry::updateOrCreate($attribute,$values);
                    }
                }

                $user->account_title = $request->account_title;
                $user->bank = $request->bank;
                $user->account_iban = $request->account_iban;
                $user->branch_name = $request->branch_name;
                $user->branch_address = $request->branch_address;
                $user->save();
                Session::flash('success', __('Service Provider Updated successfully.'));
                return redirect()->route('sp.index');
            }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

    public function banner($id)
    {
        $title = 'View Banner Images';
        $model = $this->model;
        $userId=$id;
        $breadcum = ['Service Provider' =>route($model.'.index'),'Banner Images'=>''];
        $banner_images = SpBannerImages::where('user_id',$id)->get();
        return view('admin.serviceProvider.banner',compact('title','model','banner_images','userId','breadcum'));
    }

    public function updateGallery(Request $request)
    {
        $validation = array('banner_image' => 'required|mimes:jpeg,jpg,png,gif|dimensions:min_width=1300,min_height=800',);
        $validator = Validator::make(Input::all(), $validation);
        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator->errors());
        }
        else
        {
            $limit=10;
            $userId=$request->userId;
            $banner_images = SpBannerImages::where('user_id',$userId)->count();
            if($limit > $banner_images)
            {
                $row = new SpBannerImages();
                $row->user_id = $userId;

                if ($request->hasFile('banner_image') && $request->file('banner_image'))
                {
                    $file = $request->file('banner_image');

                    $name = time().'.'.$file->getClientOriginalExtension();

                    $image_resize = Image::make($file->getRealPath());
                    $image_resize->resize(1300, 800);    
                    // $destinationPath = public_path('/sp_uploads/banner_images/');
                    $image_resize->save(public_path('/sp_uploads/banner_images/' .$name));
                    // $img = $file->move($destinationPath, $name);

                    $row->banner_path = $name;
                }
                $row->save();
                return redirect()->route('sp.service.banner',[$userId]);
            }
            else
            {
                return redirect()->back()->withErrors('Maximum 10 banners allowed');
            }
        }
        
    }


    public function deleteBannerImage(Request $request)
    {

        $id = $request->banner_id;

        $row  = SpBannerImages::whereId($id)->first();
        if($row){
           unlink('public/sp_uploads/banner_images/'.$row->banner_path);
           $row->delete();
           return 'success';
        }
    }


      
    public function StatusUpdate(Request $request)
    {
        $user_id = $request->user_id;
        $row  = SpUser::whereId($user_id)->first();
        $row->status =  $row->status=='1'?'0':'1';
        $row->save();
        $html = '';
        switch ($row->status) {
          case '1':
               $html =  '<a data-toggle="tooltip"  class="btn btn-success btn-xs" title="Active" onClick="changeStatus('.$user_id.')" >Active</a>';
              break;
               case '0':
               $html =  '<a data-toggle="tooltip"  class="btn btn-danger btn-xs" title="Inactive" onClick="changeStatus('.$user_id.')" >InActive</a>';
              break;
          
          default:
            
              break;
      }
      return $html;


    }

    public function updateTaxVisibility(Request $request)
    {
        $user_id = $request->user_id;
        $row  = SpUser::whereId($user_id)->first();
        $row->is_tax_applied =  $row->is_tax_applied=='1'?'0':'1';
        $row->save();
        $html = '';
        switch ($row->is_tax_applied) {
          case '1':
               $html =  '<a data-toggle="tooltip"  class="btn btn-success btn-xs" title="Active" onClick="changeApproval('.$user_id.')" >Yes</a>';
              break;
               case '0':
               $html =  '<a data-toggle="tooltip"  class="btn btn-danger btn-xs" title="Inactive" onClick="changeApproval('.$user_id.')" >No</a>';
              break;
          
          default:
            
              break;
      }
      return $html;


    }


    public function approveUser(Request $request)
    {
        $validation = array(
            'tier' => 'required',
            'agreement' => 'required|mimes:pdf',
        );
        $validator = Validator::make(Input::all(), $validation);
        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator->errors());
        }
        else
        {
            $row =  SpUser::whereSlug($request->slug)->first();
            $row->tier_id = $request->tier;

            if ($request->hasFile('agreement') && $request->file('agreement'))
            {
                $file = $request->file('agreement');

                $name = time().'.'.$file->getClientOriginalExtension();
                $destinationPath = public_path('/sp_uploads/approve_pdf/');
                $img = $file->move($destinationPath, $name);

                ///$row->agreement = '/sp_uploads/approve_pdf/'.$name;
                $row->agreement = $name;
            } 

            $row->is_approved = '1';
            $row->status = '1';
                        

            $row->save();
            if($row->save()){
                // $data['templete'] = "sp_approve_email";
                // $data['name'] = $row->name;
                // $data['email'] = $row->email;
                // $data['subject'] = "Approval Email";
                // send($data);

                $data1['templete'] = "welcome_sp_mail";
                $data1['name'] = $row->name;
                $data1['store_name'] = $row->store_name;
                $data1['email'] = $row->email;
                $data1['subject'] = "Welcome onboard Beutics";
                send($data1);

                            
                Session::flash('success', __('User Approved successfully.'));
                return redirect()->route('sp.index');
            }else{
                Session::flash('warning', __('Something Wrong'));
                return redirect()->back();
            }
        }
    }

    public function declineUser(Request $request)
    {
        $row =  SpUser::whereSlug($request->slug)->first();
        
        $data['templete'] = "sp_decline_email";
        $data['name'] = $row->name;
        $data['email'] = $row->email;
        $data['decline_reason'] = $request->description;
        $data['subject'] = "Decline Email";
        send($data);
        $row->delete();

        $success = 'User Declined successfully';
        return $success;
    }

    public function changePassword($slug, Request $request)
    {
        $title = 'Change Password';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        return view('admin.serviceProvider.changePassword',compact('title','slug','model','breadcum'));
    }

    public function doUpdatePassword(Request $request)
    {
         $validation = array(
            //'old_password' => 'required',
            'new_password' => 'required|min:8',
            'confirm_password' => 'required|same:new_password',
        );
        $validator = Validator::make(Input::all(), $validation);
        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator->errors());
        }
        else
        {
            $user =  SpUser::whereSlug($request->slug)->first();
            if ($user)
            {
                $user->password = Hash::make(Input::get('new_password'));
                $user->save();
                Session::flash('success', __('Password updated successfully.'));
                return redirect()->route('sp.index');
            } 
            else
            {
                Session::flash('warning', __('User not found'));
                return redirect()->back();
            }
        }
    }

    public function getTierName()
    {

        $tier_data = Tier::get();
        $tier_arr = array();

        foreach($tier_data as $data)
        {
            $tier_arr[$data['id']] = $data['title'].'(Commission - '.$data['commission'].',redeem - '.$data['redeem'].',earning - '.$data['earning'].')'; 
        }
         
        return $tier_arr;
    }

    public function getCategoryName()
    {
        $cat_data = Category::where('parent_id','0')->get();
        $cat_arr = array();

        foreach($cat_data as $data)
        {
            $cat_arr[$data['id']] = $data['category_name']; 
        }
         
        return $cat_arr;
    }

    public function getAmenities()
    {
        $amenity_data = Amenity::get();
        $amenity_arr = array();

        foreach($amenity_data as $data)
        {
            $amenity_arr[$data['id']] = $data['name']; 
        }
         
        return $amenity_arr;
    }

    // public function delete($id) {
    //     $user = SpUser::where('id', $id)->first();
    //     if ($user) {
    //         $user->delete();
    //          Session::flash('success', __('User deleted successfully.'));
    //         return redirect()->back();
    //     } else {
    //         Session::flash('warning', __('Invalid request.'));
    //         return redirect()->back();
    //     }
    // }

    public function spuserChatHistory($id)
    {
        
        $user = SpUser::where('id', $id)->first();

        if ($user) {

            $udata = $user->toArray();

            $title = $udata['name'];

            $AdminLoggedIn = session('AdminLoggedIn');
            $userId = $AdminLoggedIn['user_id'];
            $otherId = $udata['id'];
            $user_id_tbl = 'admins';
            $other_user_id_tbl = 'sp_users';

            $booking_id=0;

            $groupId   = ($userId>$otherId)?$userId."".$otherId."".$booking_id:$otherId."".$userId."".$booking_id;

            $groupId = 'AS-'.$groupId;

            Chat::where("group_id",$groupId)->where("other_user_id_tbl",'admins')->where("user_id_tbl",'sp_users')->update(['read_status'=>'1']);

            $chats = Chat::where("group_id",$groupId)->where(function($q){
                $q->where(function($q){
                    $q->where("other_user_id_tbl",'admins')->where("user_id_tbl",'sp_users');
                })->orWhere(function($q){
                    $q->where("other_user_id_tbl",'sp_users')->where("user_id_tbl",'admins');
                });
            })->get()->toArray();  

            $other['id']          = $otherId;
            $other['image']       = (!empty($udata['profile_image']))?changeImageUrlForFileExist(url('/public/sp_uploads/profile/'.$udata['profile_image'])):'https://ptetutorials.com/images/user-profile.png';;
            $other['full_name']   = $udata['store_name'];
            // dd($udata);

            $model = $this->model;
            $breadcum = [$title=>route($model.'.index')];
            return view('admin.serviceProvider.chat',compact('title','model','other','breadcum','chats',  'otherId', 'userId', 'user_id_tbl', 'other_user_id_tbl')); 

        }else{

            Session::flash('warning', __('Invalid request.'));
            return redirect()->route('users.index');
        }
    }

    public function getStateList(Request $request)
    {
        $inputtype = 'state';
        $data = $this->getmapcountry([$request->country_id,'states']);

        if(empty($data))
        {
            $inputtype = 'city';
            $data = $this->getmapcountry([$request->country_id,'cities']);
        }

        if(empty($data))
        {
            $inputtype = 'country';
            $data = $this->getmapcountry();
        }

        $json_data = array(
            "inputtype" => $inputtype,
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function getCityList(Request $request)
    {
        $inputtype = 'city';
        $data = $this->getmapcountry([$request->country_id,'states',$request->state_id,'cities']);

        if(empty($data))
        {
            $data = $this->getmapcountry([$request->country_id,'cities']);
        }
        
        $json_data = array(
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function getmapcountry($parami=[])
    {
        $ponmo = implode('/',$parami);
        $url = "https://api.countrystatecity.in/v1/countries/$ponmo"; //echo $url;
        $res = $this->get_content($url);
        return json_decode($res);
    }

    function get_content($URL)
    {
        $headers = ['X-CSCAPI-KEY: MDI5WjFSYjVZWTdlemJ5WlJrZWlMSjV5VERaM3BlcmVseUV3THRqag=='];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL,$URL);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
}
