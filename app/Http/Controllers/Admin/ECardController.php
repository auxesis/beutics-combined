<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;   

use App\Model\ECard;
use App\Model\Occasion;
use App\Model\GiftCategory;
use App\Model\User;
use App\Model\WalletCashHistory;
use datatables;
use App;
use Session;
use App\Model\Notification;

class ECardController extends Controller
{
    protected $title;
    protected $model;
    protected $module;

    public function __construct()
    {
        $this->title = 'E-Card';
        $this->model = 'e-card';
        $this->module = 'e-card';
    } 

    public function index()
    {
        $title = 'E-Cards';
        $module = $this->module;
        $model = $this->model;
        $breadcum = [$title=>'E-Card'];
        $occasions  = Occasion::orderby('name', 'asc')->pluck('name', 'id')->toArray();
        return view('admin.e_card.index',compact('title','model','module','breadcum', 'occasions'));
    }

    public function getData(Request $request)
    {
   
        $columns = ['name','question'];
        
        $totalData = ECard::count();
                       
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');      
     
        $row = ECard::select("e_cards.*");
      

        if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = ECard::leftJoin('users as au', 'e_cards.user_id', '=', 'au.id')
                ->leftJoin('users as cu', 'e_cards.receiver_id', '=', 'cu.id')
                ->where(function($query) use ($search) {
                    $query->Where('au.name', 'LIKE', "%{$search}%")
                    ->orWhere('cu.name', 'LIKE', "%{$search}%")
                    ->OrWhere('au.mobile_no', 'LIKE', "%{$search}%")
                    ->OrWhere('cu.mobile_no', 'LIKE', "%{$search}%")
                    ->OrWhere('e_cards.receiver_name', 'LIKE', "%{$search}%")
                    ->OrWhere('e_cards.receiver_mobile_no', 'LIKE', "%{$search}%")
                    ->OrWhere('e_cards.card_unique_id', 'LIKE', "%{$search}%");
                })
            ->select('e_cards.*');
        }

        if (!empty($request->input('category_filter'))) {
            $occasion_id = $request->input('category_filter');
             $row = $row->where(function($query) use ($occasion_id) 
            {
                $query->where('e_cards.occasion_id', '=', $occasion_id);
            });

            /*$row = ECard::Where('e_cards.occasion_id', '=', $occasion_id)
            ->select('e_cards.*');*/
        }
        if (!empty($request->input('status_filter'))) {
            $status = $request->input('status_filter');
            if($status == 'received'){
                $status = "1";
            }elseif($status == 'cancelled'){
                $status = "2";
            } else {
                $status = "0";
            }
             $row = $row->where(function($query) use ($status) 
            {
                $query->where('e_cards.status', '=', $status);
            });
             
             
            /*$row = ECard::Where('e_cards.status', '=', $status)
            ->select('e_cards.*');*/
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('e_cards.id', 'desc')
                ->get();
       
        $data = array();
        if (!empty($row)) {
            foreach ($row as $key => $rows) {
                $nestedData['id'] = $rows->id;
                $nestedData['card_unique_id'] = $rows->card_unique_id;
                $nestedData['occasion'] = $rows->getAssociatedOccasionInfo->name;
                $nestedData['sender'] = $rows->getAssociatedUserInfo->name.'</br><b>('.$rows->getAssociatedUserInfo->mobile_no.')</b>';
                if($rows->receiver_id){
                    $nestedData['receiver'] = $rows->getAssociatedReceiverInfo->name.'</br><b>('.$rows->getAssociatedReceiverInfo->mobile_no.')</b>';
                } else {
                    $nestedData['receiver'] = $rows->receiver_name.'</br><b>('.$rows->receiver_mobile_no.')</b>';
                }
                $nestedData['amount'] = round($rows->amount,2).' AED';
                $status = array('0' => 'Pending', '1' => 'Received', '2' => 'Cancelled');
                $nestedData['status'] = $status[$rows->status];
                $nestedData['created_at'] = date('Y-m-d h:i:A',strtotime($rows->created_at));
                $nestedData['card_send_date'] = ($rows->card_send_date)?date('Y-m-d h:i:A',strtotime($rows->card_send_date)):date('Y-m-d h:i:A',strtotime($rows->created_at));
                if($nestedData['status'] == 'Cancelled' || $nestedData['status'] == 'Received'){
                    $nestedData['action'] =  getButtons([
                        ['key'=>'view','link'=>route('e-card.show',$rows->id)],
                    ]);
                } else {
                    $nestedData['action'] =  getButtons([
                        ['key'=>'view','link'=>route('e-card.show',$rows->id)],
                        ['key'=>'cancel','link'=>route('e-card.cancelled_card_view',$rows->id)],
                    ]);
                }
                
                
                $data[] = $nestedData;
            }

        }
        //echo '<pre>'; print_r($data);
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }
  
    public function show($id)
    {
        $title = 'E-Card Details';
        $rows = ECard::select('e_cards.*')->where('id', $id)->first();
        
        if($rows){
            $data['id'] = $rows->id;
            $data['card_unique_id'] = $rows->card_unique_id;
            $data['occasion'] = $rows->getAssociatedOccasionInfo->name;
            $data['theme'] = '<img src="'.asset("/public/eCardThemes/".$rows->getAssociatedOccasionThemeInfo->image).'" width="120" height="75"/>';
            $data['sender'] = $rows->getAssociatedUserInfo->name.'</br><b>('.$rows->getAssociatedUserInfo->mobile_no.')</b>';
            if($rows->receiver_id){
                $data['receiver'] = $rows->getAssociatedReceiverInfo->name.'</br><b>('.$rows->getAssociatedReceiverInfo->mobile_no.')</b>';
            } else {
                $data['receiver'] = $rows->receiver_name.'</br><b>('.$rows->receiver_mobile_no.'<br>'.$rows->receiver_email.')</b>';
            }
            $data['receiver_message'] = $rows->receiver_message;
            $data['amount'] = round($rows->amount,2).' AED';
            $status = array('0' => 'Pending', '1' => 'Received', '2' => 'Cancelled');
            $data['status'] = $status[$rows->status];
            // $data['card_send_date'] = ($rows->card_send_date)?date('Y-m-d',strtotime($rows->card_send_date)):date('Y-m-d',strtotime($rows->created_at));
            if($rows->status!='0'){
                 $data['card_send_date'] = ($rows->card_send_date!=null && $rows->card_send_date!='')?date('Y-m-d h:i:A',strtotime($rows->card_send_date)):date('Y-m-d h:i:A',strtotime($rows->created_at));
             }else{
                 $data['card_send_date'] = "";
             }
           
            $data['created_at'] = date('Y-m-d h:i:A',strtotime($rows->created_at)); 
            // $data['scheduled_date'] = ($rows->card_send_date!=null && $rows->card_send_date!='')?date('Y-m-d h:i:A',strtotime($rows->card_send_date)):'-'; 
            
            if($rows->cancellation_refunded_amount){
                $data['cancellation_reason'] = $rows->cancellation_reason;
                $data['cancellation_refunded_amount'] = round($rows->cancellation_refunded_amount,2).' AED';
                $data['cancelled_date_time'] = date('Y-m-d h:i:A',strtotime($rows->cancelled_date_time));  
            }
            $payment_details = '';
            if($rows->getAssociatedBookingPaymentDetail){
                foreach($rows->getAssociatedBookingPaymentDetail as $d_val){
                    $payment_details .= ucfirst($d_val->payment_option).'('.round($d_val->amount,2).' AED)'.', ';
                }
            }
            $data['payment_details'] = rtrim($payment_details, ' ,');
        }
        //echo '<pre>'; print_r($data);die;
        $module = $this->module;
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>'E-Card Detail'];     
        return view('admin.e_card.view',compact('title','data','module','breadcum'));
    }
    

    public function cancelled_card_view($id){
      $title = 'Cancel E-Card';
      $model = 'e-card';
      
      $breadcum = ['E-Cards'=>route($model.'.index'),$title =>''];
      
      $result = ECard::where('id',$id)->first()->toArray();
     return view('admin.e_card.CancelledCardView',compact('title','result','breadcum'));
    }
     
  public function cancel(Request $request)
  {
    try {
        $data = $request->all();       

        //echo '<pre>'; print_r($data);die;
        $validator = Validator::make($data, 
            [
              'id' => 'required',
            ]);
          if ($validator->fails()) 
          {
            
            $error = $this->validationHandle($validator->messages());
            Session::flash('danger', $error);
            return redirect()->back()->withInput();
          } 
          else 
          {
            $rows = ECard::where('id',$request->id)->first();
            if($rows){
                if($request->cancellation_refunded_amount && ($request->cancellation_refunded_amount > $rows->amount)){
                  Session::flash('danger', 'Please enter a valid refund amount, the entered refund amount should not exceed the total e-card amount.');
                  return redirect()->back()->withInput();
                }

                $rows->cancellation_refunded_amount = $request->cancellation_refunded_amount;          
                $rows->cancellation_reason = $request->cancellation_reason;
                $rows->status = '2';
                $rows->cancelled_date_time = date('Y-m-d H:i:s');

                if($rows->save()){
                    $rewarded_user_id = $rows->user_id;
                    
                    $user = User::where('id',$rewarded_user_id)->first();                  
                    $user->wallet_amount = ($user->wallet_amount+$request->cancellation_refunded_amount);
                    $user->save();

                    $wallet_row = new WalletCashHistory();
                   
                    $wallet_row->user_id = $rewarded_user_id;
                    $wallet_row->trans_ref_id = $request->id;
                    $wallet_row->amount = $request->cancellation_refunded_amount;
                    $wallet_row->transaction_type = '0';
                    $wallet_row->description = $request->description;
                    $wallet_row->transaction_date_time = date('Y-m-d H:i:s');
                    $wallet_row->save();  

                    //notification for customer
                   if($rows->user_id!='' && $rows->user_id!=null && $user->notification_alert_status == '1'){

                    // $message_title = 'e-Gift Card Cancelled!';
                    // $message = 'Your e-Gift Card with card number ('.$rows->card_unique_id.') has been cancelled as per your request. We have initiated the refund to your wallet and the same will reflect asap.';

                    $message_title = 'Refund Notification!';
                    $message = 'Hey, we have processed your refund('.$rows->card_unique_id.') of AED '.$request->cancellation_refunded_amount.' and credited your wallet. Enjoy Shopping with Beutics!';
                    
                    Notification::saveNotification($rows->user_id,$rows->receiver_id,$rows->id,'E_CARD_CANCELLED',$message,$message_title,'customer');

                  }

                 

                  Session::flash('success', __('E-card has been cancelled Successfully.'));
                  return redirect()->route('e-card.index');
                }
              }else{
                Session::flash('danger', __('OrderId is not valid.'));
                 return redirect()->back()->withInput();
              }
              
          }
            
     }
     catch(\Exception $e){
       $msg = $e->getMessage();
       Session::flash('danger', $msg);
       return redirect()->back()->withInput();
     }
  }

}
