<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;   

use App\Model\GiftTheme;
use App\Model\ECard;
use App\Model\Occasion;
use datatables;
use App;
use Session;

class GiftThemeController extends Controller
{
    protected $title;
    protected $model;
    protected $module;

    public function __construct()
    {
        $this->title = 'Gift Theme';
        $this->model = 'gift-theme';
        $this->module = 'gift-theme';
    } 

    public function index()
    {
        $title = 'Gift Theme';
        $module = $this->module;
        $model = $this->model;
        $breadcum = [$title=>'Gift Theme'];
        return view('admin.gift_theme.index',compact('title','model','module','breadcum'));
    }

    public function getData(Request $request)
    {
   
        $columns = ['name','image'];
        
        $totalData = GiftTheme::count();
                       
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');      
     
        $row = GiftTheme::select("gift_themes.*");
      

        if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = GiftTheme::leftJoin('occasions', function($join) {
                  $join->on('gift_themes.occasion_id', '=', 'occasions.id');
                }) 
            ->where(function($query) use ($search) {
                $query->Where('name', 'LIKE', "%{$search}%"); 
            })
          ->select('gift_themes.*', 'occasions.name');
 
        } else {
            $row = GiftTheme::leftJoin('occasions', function($join) {
                  $join->on('gift_themes.occasion_id', '=', 'occasions.id');
                }) 
          ->select('gift_themes.*', 'occasions.name');
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('gift_themes.id', 'desc')
                ->get();
       
        $data = array();
        if (!empty($row)) {
            foreach ($row as $key => $rows) {
                $checked = ($rows->is_publish == 1) ? 'checked' : '';
                $nestedData['checkbox'] = '<input type="checkbox" name="pdr_checkbox" class="pdr_checkbox" onClick="makePublish('.$rows->id.')"  '.$checked.' />';
                $nestedData['id'] = $rows->id;
                $nestedData['name'] = $rows->name;
                $nestedData['image'] = '<img src="'.asset("/public/eCardThemes/".$rows->image).'" width="120" height="75"/>';
                $associatedTables = ECard::where('theme_id',$rows->id)->count();
                $nestedData['action'] =  getButtons([
                                ['key'=>'edit','link'=>route('gift-theme.edit',$rows->id)],
                                ['key'=>'delete_assoc','link'=>route('gift-theme.destroy',$rows->id), 'asso'=>$associatedTables],
                            ]);
                
                $data[] = $nestedData;
            }

        }
        // echo '<pre>'; print_r($data);
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }
  
    public function create()
    {
        $action = "create";
        $title = 'Create Theme';
        $model = $this->model;
        $occasions = Occasion::select('id','name')->get();
        $occasion_list = array();
        foreach($occasions as $row){
            $occasion_list[$row['id']] = $row['name'];
        }
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        return view('admin.gift_theme.create',compact('action','title','breadcum','model','occasion_list'));
    }

    
    public function store(Request $request)
    {
        try{ //dd($request);
             if (Input::isMethod('post')) { 
                $validation = array(
                    'occasion_id'=>'required',
                    'image'=>'required|mimes:jpeg,jpg,png,gif',
                );

                $validator = Validator::make(Input::all(), $validation);

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {
                    $row = new GiftTheme();
                    $row['occasion_id'] = $request->occasion_id;
                    
                    if ($request->hasFile('image') && $request->file('image'))
                    {
                        $image = $request->image_cr;
                        list($type, $image) = explode(';', $image);
                        list(, $image)      = explode(',', $image);
                        $image = base64_decode($image);
                        $image_name= time().'.png';
                        $path = public_path('/eCardThemes/'.$image_name);
                        file_put_contents($path, $image);
                        $row->image = $image_name;
                    }

                    if($row->save())
                    {
                        Session::flash('success', __('Gift Theme added successfully.'));
                        return redirect()->route('gift-theme.index');
                    }
                    
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }
   
    public function edit($id)
    {
        $rows = GiftTheme::where('id',$id)->first();
        $title = 'Edit Theme';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>'Gift Theme'];        
        $occasions = Occasion::select('id','name')->get();
        $occasion_list = array();
        foreach($occasions as $row){
            $occasion_list[$row['id']] = $row['name'];
        }

        return view('admin.gift_theme.edit',compact('rows','title','breadcum','occasion_list'));
    }

   
    public function update(Request $request, $id)
    {
        try{
             if (Input::isMethod('PATCH')) {
                $validation = array(
                    'occasion_id'=>'required',
                    'image'=>'mimes:jpeg,jpg,png,gif',
                );

                $validator = Validator::make(Input::all(), $validation);

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {
                    $row = GiftTheme::where('id',$id)->first();
                    $row['occasion_id'] = $request->occasion_id;
                   
                    if ($request->hasFile('image') && $request->file('image'))
                    {

                        $image = $request->image_cr;
                        list($type, $image) = explode(';', $image);
                        list(, $image)      = explode(',', $image);
                        $image = base64_decode($image);
                        $image_name= time().'.png';
                        $path = public_path('/eCardThemes/'.$image_name);
                        file_put_contents($path, $image);
                        $row->image = $image_name;
                    }

                    if($row->save())
                    {
                        Session::flash('success', __('Gift Theme updated successfully.'));
                        return redirect()->route('gift-theme.index');
                    }
                    
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

  
    public function destroy($id)
    {
        $row = GiftTheme::where('id', $id)->first();
        if ($row) {
            $row->delete();
             Session::flash('success', __('Gift Theme deleted successfully.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('Invalid request.'));
            return redirect()->back();
        }
    }

    public function PublishUpdate(Request $request)
    {
        $ecard_id = $request->ecard_id;
        $row  = GiftTheme::whereId($ecard_id)->first();
        $row->is_publish =  $row->is_publish=='1'?'0':'1';
        $row->save();
        $html = '';
        switch ($row->is_publish) {
          case '1':
               $html =  '<input type="checkbox" name="pdr_checkbox" class="pdr_checkbox" onClick="makePublish('.$ecard_id.')" checked/>';
              break;
               case '0':
               $html =  '<input type="checkbox" name="pdr_checkbox" class="pdr_checkbox" onClick="makePublish('.$ecard_id.')" />';
              break;
          
          default:
            
              break;
        }
      return $html;
    }
}