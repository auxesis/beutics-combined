<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;   

use App\Model\Product;
use App\Model\SpUser;
use datatables;
use App;
use Session;

class ProductController extends Controller
{
    protected $title;
    protected $model;
    protected $module;

    public function __construct()
    {
        $this->title = 'Product';
        $this->model = 'product';
        $this->module = 'product';
    } 

    public function index()
    {
        $title = 'Product';
        $module = $this->module;
        $model = $this->model;
        $spid = isset($_GET['spid']) ? $_GET['spid'] : '';
        if($spid){
            $sp_row = SpUser::where('id',$spid)->select('name')->first();

            $breadcum = [$sp_row->name => route('sp.index'),$title=>''];
        } else{
            $breadcum = [$title=>''];
        }
        return view('admin.product.index',compact('title','model','module','breadcum','spid'));
    }

    public function getData(Request $request)
    {
        $spid = $request->input('spid');
        $columns = ['sp_id','product_name','product_image'];
        if($request->input('spid')){
            $totalData = Product::where('sp_id',$spid)->count();
        }
        else{
            $totalData = Product::count();
        }
        
        
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        
        if($request->input('spid')){
            $row = Product::where('sp_id',$spid)->select("products.*");
        }
        else{
            $row = Product::select("products.*");
        }
        // $totalFiltered = User::count();

        if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = $row->leftJoin('sp_users', 'sp_users.id', '=', 'products.sp_id')
                    ->where(function($query) use ($search) {
                        $query->Where('product_name', 'LIKE', "%{$search}%")
                        ->orWhere('sp_users.store_name', 'LIKE', "%{$search}%");
                        
                    });
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('products.id', 'desc')
                ->get();
        // print_r($users);die;
        $data = array();
        if (!empty($row)) {
            foreach ($row as $key => $rows) {
                $nestedData['sp_id'] = $rows->getServiceProInfo->store_name;
                $nestedData['product_name'] = $rows->product_name;
                $nestedData['product_image'] = '<img src="'.changeImageUrlForFileExist(asset("sp_uploads/products/".$rows->product_image)).'" width="60" height="50"/>';

                $edit_link = ($spid) ? route('product.edit',[$rows->id, 'spid'=>$spid]) : route('product.edit',[$rows->id]);   

                $nestedData['action'] =  getButtons([
                                ['key'=>'edit','link'=>$edit_link],
                                ['key'=>'delete','link'=>route('product.destroy',$rows->id)],
                            ]);
                
                $data[] = $nestedData;
            }

        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }
  
    public function create()
    {
        $action = "create";
        $title = 'Create Product';
        $model = $this->model;
        $sp_row = SpUser::select('id','store_name')->get();
        $service_provider_arr = array();
        foreach($sp_row as $row){
            $service_provider_arr[$row['id']] = $row['store_name'];
        }
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        return view('admin.product.create',compact('action','title','breadcum','model','module','service_provider_arr'));
    }

    
    public function store(Request $request)
    {
        try{
             if (Input::isMethod('post')) {
                $validation = array(
                    'sp_id'=>'required',
                    'product_name'=>'required|max:50',
                    'product_image'=>'required|mimes:jpeg,jpg,png,gif',
                );

                $validator = Validator::make(Input::all(), $validation);

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {
                    $row = new Product();
                    $row['sp_id'] = $request->sp_id;
                    $row['product_name'] = $request->product_name;
                   
                    if ($request->hasFile('product_image') && $request->file('product_image'))
                    {

                        $image = $request->image_cr;
                        list($type, $image) = explode(';', $image);
                        list(, $image)      = explode(',', $image);
                        $image = base64_decode($image);
                        $image_name= time().'.png';
                        $path = public_path('/sp_uploads/products/'.$image_name);
                        file_put_contents($path, $image);
                        $row->product_image = $image_name;
                    }

                    $row->save();
                    if($row->save())
                    {
                        Session::flash('success', __('Product added successfully.'));
                        return redirect()->route('product.index');
                    }
                    
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

   
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        $rows = Product::where('id',$id)->first();
        $title = 'Edit Product';
        $model = $this->model;
        
        if(isset($_GET['spid'])){
            $sp_row = SpUser::where('id',$_GET['spid'])->select('name')->first();

            $breadcum = [$sp_row->name => route('sp.index'),'Product'=>route('product.index', ['spid'=>$_GET['spid']]),$title=>''];
        }else{
            $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        }
        $sp_row = SpUser::select('id','store_name')->get();
        $service_provider_arr = array();
        foreach($sp_row as $row){
            $service_provider_arr[$row['id']] = $row['store_name'];
        }

        return view('admin.product.edit',compact('rows','title','breadcum','service_provider_arr'));
    }

   
    public function update(Request $request, $id)
    {
        try{
             if (Input::isMethod('PATCH')) {
                $validation = array(
                    'sp_id'=>'required',
                    'product_name'=>'required|max:50',
                    'product_image'=>'mimes:jpeg,jpg,png,gif',
                );

                $validator = Validator::make(Input::all(), $validation);

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {
                    $row = Product::where('id',$id)->first();
                    $row['sp_id'] = $request->sp_id;
                    $row['product_name'] = $request->product_name;
                   
                    if ($request->hasFile('product_image') && $request->file('product_image'))
                    {

                        $image = $request->image_cr;
                        list($type, $image) = explode(';', $image);
                        list(, $image)      = explode(',', $image);
                        $image = base64_decode($image);
                        $image_name= time().'.png';
                        $path = public_path('/sp_uploads/products/'.$image_name);
                        file_put_contents($path, $image);
                        $row->product_image = $image_name;
                    }

                    $row->save();
                    if($row->save())
                    {
                        Session::flash('success', __('Product updated successfully.'));
                        return redirect()->route('product.index');
                    }
                    
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

  
    public function destroy($id)
    {
        $row = Product::where('id', $id)->first();
        if ($row) {
            $row->delete();
             Session::flash('success', __('Product deleted successfully.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('Invalid request.'));
            return redirect()->back();
        }
    }
}
