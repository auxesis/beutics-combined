<?php

namespace App\Http\Controllers\Admin;
use datatables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use DB;
use Illuminate\Support\Facades\Input;   
use Illuminate\Support\Facades\Hash;
use Session;
use App\Model\User;
use App\Model\CashbackHistory;
use App\Model\Booking;
use App\Model\ECard;
use App\Model\ForumComment;
use App\Model\Review;
use App\Model\SpUser;
use App\Model\BookingPaymentDetail;
use App\Model\Forum;
use App\Model\SpService;
use App\Model\SpOffer;
use App\Model\Tier;
use Excel;

class ReportController extends Controller
{

    private $excel;

    public function __construct(Excel $excel)
    {
        $this->excel = $excel;
    }

    public function customer_report()
    {
    	$title = 'Customer Report';
    	return view('admin.reports.customer_report',compact('title'));
    }

    public function exportCustomer(Request $request){
            
        try { 
                $curr_time = date('Y-m-d H:i:s');
                $rptname =  'customer_report('.$curr_time.')';
                $users= User::
                
                select('users.id','users.name','users.country_code','users.referrer_id','users.mobile_no','users.email','users.created_at',
                    'users.referrer_type','users.wallet_amount','users.redemption','users.share_code');
                 
                 if($request->get('reg_sdate') && $request->get('reg_sdate')){
                    $start_date = $request->get('reg_sdate');
                    $end_date = $request->get('reg_edate');
                    $users= $users->whereDate('users.created_at','>=',$request->input('reg_sdate'))
                    ->whereDate('users.created_at','<=',$request->input('reg_edate'));
                }
                
                $users = $users
                ->groupBy('users.id')
                ->get();
                // print_r($users);die;
                $user_arr = array();

                if(count($users)>0){
                    $i=1;
                   foreach($users as $row){
                    $ref_type = '';
                    $ref_name = '';
                    //ecard count
                    $ecount = ECard::Where('user_id',$row->id)->count();
                    $forum_comments_count = ForumComment::Where('user_id',$row->id)->count();
                    $reviews_count = Review::Where('user_id',$row->id)->count();
                    $gifts = Booking::Where('gifted_by',$row->id)->count();

                    //referral count
                    $referrals_count = User::where('refer_code',$row->share_code)->count();

                     //total refer and earn
                    $total_refer_earning = CashbackHistory::where('user_id',$row->id)->whereIn('type',array('1','2'))
                    ->select(DB::raw('sum(amount) as amount'))->groupBy('user_id')->first();

                    $total_cashback = CashbackHistory::where('user_id',$row->id)
                    ->where(function ($query){

                        $query->where('type','0')
                        ->orWhere('type','4');
                    })
                    ->select(DB::raw('sum(amount) as amount'))
                    ->groupBy('user_id')->first();

                     if($row->referrer_type!=null && $row->referrer_type!=''){
                        if($row->referrer_type=="0"){
                            $ref_type = 'Customer';
                            $ref_name = User::where('id',$row->referrer_id)->pluck('name')->first();

                        }else{
                            $ref_type = 'Store';
                            $ref_name = SpUser::where('id',$row->referrer_id)->select('store_name')->first();
                        }

                    }

                        $user_arr[] = [
                            "SNo" => $i,
                            "Name" => $row->name,
                            "Country Code" => $row->country_code,
                            "Contact Number" => $row->mobile_no,
                            "Email" => $row->email,
                            "Date of Signup" => date('d-M-Y',strtotime($row->created_at)),
                            "Time of Signup" => date('H:i:s',strtotime($row->created_at)),
                            "Referral Count" => $referrals_count,

                            "Referral Type"=>$ref_type,
                            "Referral Name"=>$ref_name,

                            "Refer Earning" =>$total_refer_earning ? $total_refer_earning['amount']:'0',
                            "Total Cashback" =>$total_cashback ? $total_cashback['amount']:'0',
                            "Total Redemption" => round($row->redemption,2),
                            "My Cash" => round($row->wallet_amount,2),
                            "Forum Comments" => $forum_comments_count,
                            "Ecard Purchased" => $ecount,
                            "Gift Purchased" => $gifts,
                            "Reviews" => $reviews_count,

                        ];
                        $i++;
                   }
                    
                }


                if(count($user_arr)>0){  
                    Excel::create($rptname, function($excel) use($user_arr) {
                        $excel->sheet('ExportFile', function($sheet) use($user_arr) {
                            $sheet->fromArray($user_arr);
                              $sheet->row(1, function($row) {  
                                    $row->setBackground('#FFFF00');  
                                });
                        });
                    })->export('xlsx');
                    Session::flash('success',"Excel generated successfully."); 
                    return back();
                }
                else{
                    Session::flash('danger','No Record Found.'); 
                    return back();
                }
            } catch (\Exception $e) {
                // Session::flash('danger',$e->getMessage());
                // return back();
                print_r($e->getMessage());die;
            }         
    }

    public function customersGetData(Request $request)
    {

        $columns = ['name','mobile_no','email','created_at','redemption','my_cash'];

        if($request->get('reg_sdate') && $request->get('reg_sdate')){
            $start_date = $request->get('reg_sdate');
            $end_date = $request->get('reg_edate');
            $totalData= User::whereDate('users.created_at','>=',$request->input('reg_sdate'))
            ->whereDate('users.created_at','<=',$request->input('reg_edate'))->count();
        }else{
            $totalData= User::count();
        }
        
        $limit = $request->input('length');
        $start = $request->input('start');
        $sno =  $request->input('start')+1;
        // echo $request->input('order.0.column');die;
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        // $row = new User();
        $row= User::
         
            select('users.id','users.name','users.country_code','users.referrer_id','users.mobile_no','users.email','users.created_at',
                'users.referrer_type','users.wallet_amount','users.redemption','users.share_code');
             
             if($request->get('reg_sdate') && $request->get('reg_sdate')){
                $start_date = $request->get('reg_sdate');
                $end_date = $request->get('reg_edate');
                $row= $row->whereDate('users.created_at','>=',$request->input('reg_sdate'))
                ->whereDate('users.created_at','<=',$request->input('reg_edate'));
            }
                        

// $row = new User();
        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                // ->orderBy('id', 'desc')
                ->orderBy($order, $dir)
                // ->withTrashed()
                ->get();
        // print_r($users);die;
        $data = array();
        if (!empty($row)) {
            foreach ($row as $key => $rows) {
                $ref_type ='';
                $ref_name='';
                 $referrals_count = User::where('refer_code',$rows->share_code)->count();

                     //total refer and earn
                $total_refer_earning = CashbackHistory::where('user_id',$rows->id)->whereIn('type',array('1','2'))
                ->select(DB::raw('sum(amount) as amount'))->groupBy('user_id')->first();

                $total_cashback = CashbackHistory::where('user_id',$rows->id)
                ->where(function ($query){

                    $query->where('type','0')
                    ->orWhere('type','4');
                })
                ->select(DB::raw('sum(amount) as amount'))
                ->groupBy('user_id')->first();

                //ecard count
                $ecount = ECard::Where('user_id',$rows->id)->count();
                $forum_comments_count = ForumComment::Where('user_id',$rows->id)->count();
                $reviews_count = Review::Where('user_id',$rows->id)->count();
                $gifts = Booking::Where('gifted_by',$rows->id)->count();
                if($rows->referrer_type!=null && $rows->referrer_type!=''){
                        if($rows->referrer_type=="0"){
                            $ref_type = 'Customer';
                            $ref_name = User::where('id',$rows->referrer_id)->pluck('name')->first();

                        }else{
                            $ref_type = 'Store';
                            $ref_name = SpUser::where('id',$rows->referrer_id)->pluck('store_name')->first();
                        }

                    }

                $nestedData['sno'] = $sno++;
                $nestedData['name'] = $rows->name;
                $nestedData['email'] = $rows->email;
                $nestedData['country_code'] = $rows->country_code;
                $nestedData['mobile_no'] = $rows->mobile_no;
                $nestedData['created_at'] = date('d-M-Y',strtotime($rows->created_at));
                $nestedData['created_time'] = date('H:i:s',strtotime($rows->created_at));
                $nestedData['ecount'] = $ecount;
                $nestedData['total_forum_comments'] = $forum_comments_count;
                $nestedData['reviewsCount'] = $reviews_count;
                $nestedData['gifts'] = $gifts;
                $nestedData['referral_type'] = $ref_type.'<br><b>'.$ref_name.'</br></b>';
                $nestedData['referral_count'] = $referrals_count;
                $nestedData['my_cash'] = round($rows->wallet_amount,2);
                $nestedData['redemption'] = round($rows->redemption,2);
                $nestedData['total_refer_earning'] = $total_refer_earning ? $total_refer_earning['amount']:'0';
                $nestedData['total_cashback'] = $total_cashback ? $total_cashback['amount']:'0';
                // $nestedData['referral_count'] = $rows->referral_count;
                $data[] = $nestedData;
            }

        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function order_report()
    {
        $title = 'Order Report';
        return view('admin.reports.order_report',compact('title'));
    }

    public function orderGetData(Request $request)
    {

        $columns = ['name','email','mobile_no','created_at','ecount','total_forum_comments'];

        if($request->get('reg_sdate') && $request->get('reg_sdate')){
            $start_date = $request->get('reg_sdate');
            $end_date = $request->get('reg_edate');
            $totalData= Booking::whereDate('bookings.created_at','>=',$request->input('reg_sdate'))
            ->whereDate('bookings.created_at','<=',$request->input('reg_edate'))->count();
        }else{
            $totalData= Booking::count();
        }
        
        $limit = $request->input('length');
        $start = $request->input('start');
        $sno =  $request->input('start')+1;
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        // $row = new User();
        $row= Booking::
         
            select('bookings.id','bookings.booking_unique_id','bookings.appointment_date','bookings.appointment_time','bookings.sp_id','bookings.created_at',
                'bookings.user_id','bookings.completed_date_time','bookings.status','bookings.completed_date_time','bookings.is_gift','bookings.total_item_amount','bookings.paid_amount');
             
             if($request->get('reg_sdate') && $request->get('reg_sdate')){
                $start_date = $request->get('reg_sdate');
                $end_date = $request->get('reg_edate');
                $row= $row->whereDate('bookings.created_at','>=',$request->input('reg_sdate'))
                ->whereDate('bookings.created_at','<=',$request->input('reg_edate'));
            }
                        

// $row = new User();
        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('id', 'desc')
                // ->withTrashed()
                ->get();
        // print_r($users);die;
        $data = array();
        if (!empty($row)) {
        
            foreach ($row as $key => $rows) {
                $status_arr = array('0'=>'Awaiting','1'=>'Confirmed','2'=>'Cancelled','3'=>'Refunded','4'=>'Expired','5'=>'Completed');
                //tier
                $tier = SpUser::Where('id',$rows->id)->select('tier_id')->first();
                $item_name_arr1 = array();
                foreach($rows->getAssociatedBookingItems as $bkitems){
                    if($bkitems['is_service'] == '1')
                    {
                        if(isset($bkitems->getAssociatedServiceDetail->getAssociatedService->name)){
                            $item_name_arr1[] = $bkitems->getAssociatedServiceDetail->getAssociatedService->name;
                        }
                        
                    }
                    else{
                        if(isset($bkitems->getAssociatedOfferDetail->getAssociatedOfferName->title)){
                            $item_name_arr1[] = $bkitems->getAssociatedOfferDetail->getAssociatedOfferName->title;
                        }
                        
                    }

                }
                $nestedData['sno'] = $sno++;
                $nestedData['order_id'] = $rows->booking_unique_id;
                $nestedData['created_at'] = date('d-M-Y H:i',strtotime($rows->created_at));
                $nestedData['appointment_date'] = date('d-M-Y',strtotime($rows->appointment_date)).' '.date('H:i',strtotime($rows->appointment_time));
                $nestedData['user_id'] = isset($rows->getAssociatedUserInfo->name)?$rows->getAssociatedUserInfo->name:'';
                $nestedData['sp_id'] = isset($rows->getAssociatedSpInformation->store_name)?$rows->getAssociatedSpInformation->store_name:'';
                $nestedData['tier_id'] = isset($tier->getAssociatedTierName->commission)?$tier->getAssociatedTierName->commission.'%':"";
                $nestedData['items'] = implode(',',$item_name_arr1);
                $nestedData['original_price'] = $rows->total_item_amount;
                $nestedData['best_price'] = $rows->paid_amount;
                $nestedData['status'] = $status_arr[$rows->status];
                $nestedData['gift_status'] = $rows->is_gift =='1'?'Gift':'Self';
                $nestedData['gift_user_id'] =  $rows->is_gift =='1'?$rows->getAssociatedUserInfo->name:'-';
                $nestedData['gift_mobile_no'] = $rows->is_gift =='1'?$rows->getAssociatedUserInfo->mobile_no:'-';
                $nestedData['completed_date'] = $rows->completed_date_time!='' ? date('d-M-Y H:i',strtotime($rows->completed_date_time)):"";
                $data[] = $nestedData;
            }

        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function exportOrder(Request $request){
            
        try { 
                $curr_time = date('Y-m-d H:i');
                $rptname =  'order_report('.$curr_time.')';
                $orders= Booking::
         
                select('bookings.id','bookings.booking_unique_id','bookings.appointment_date','bookings.appointment_time','bookings.sp_id','bookings.created_at',
                    'bookings.user_id','bookings.completed_date_time','bookings.status','bookings.completed_date_time','bookings.is_gift','bookings.total_item_amount','bookings.paid_amount');
                 
                 if($request->get('reg_sdate') && $request->get('reg_sdate')){
                    $start_date = $request->get('reg_sdate');
                    $end_date = $request->get('reg_edate');
                    $orders= $orders->whereDate('bookings.created_at','>=',$request->input('reg_sdate'))
                    ->whereDate('bookings.created_at','<=',$request->input('reg_edate'));
                }
                $orders= $orders->get();

                $order_arr = array();
                // print_r($orders);die;
                if(count($orders)>0){
                    $i=1;
                   foreach($orders as $rows){


                    $status_arr = array('0'=>'Awaiting','1'=>'Confirmed','2'=>'Cancelled','3'=>'Refunded','4'=>'Expired','5'=>'Completed');
                    //tier
                    $tier = SpUser::Where('id',$rows->id)->select('tier_id')->first();

                    foreach($rows->getAssociatedBookingItems as $bkitems){
                        if($bkitems['is_service'] == '1')
                        {
                            if(isset($bkitems->getAssociatedServiceDetail->getAssociatedService->name)){
                                $item_name_arr1[] = $bkitems->getAssociatedServiceDetail->getAssociatedService->name;
                            }
                            
                        }
                        else{
                            if(isset($bkitems->getAssociatedOfferDetail->getAssociatedOfferName->title)){
                                $item_name_arr1[] = $bkitems->getAssociatedOfferDetail->getAssociatedOfferName->title;
                            }
                            
                        }

                    }

                   
                    $order_arr[] = [
                        'S.No' => $i,
                        'Order ID' => $rows->booking_unique_id,
                        'Date' => date('d-M-Y H:i',strtotime($rows->created_at)),
                        'Appointment Date' => isset($rows->appointment_date)? date('d-M-Y',strtotime($rows->appointment_date)).' '.date('H:i',strtotime($rows->appointment_time)):"",
                        'Customer' => isset($rows->getAssociatedUserInfo->name)? $rows->getAssociatedUserInfo->name:"-",
                        'Store' => $rows->getAssociatedSpInformation->store_name,
                        'Tier of Store' => isset($tier->getAssociatedTierName->commission)?$tier->getAssociatedTierName->commission.'%':"",
                        'Item Purchased' => implode(',',$item_name_arr1),
                        'Buy Price' => $rows->total_item_amount,
                        'DP' => $rows->paid_amount,
                        'Status' => $status_arr[$rows->status],
                        'Gift Service' => $rows->is_gift =='1'?'Gift':'Self',
                        'Gifted To - Customer Name' =>  $rows->is_gift =='1'?$rows->getAssociatedUserInfo->name:'-',
                        'Gifted To - Mobile no' => $rows->is_gift =='1'?$rows->getAssociatedUserInfo->mobile_no:'-',
                        'Order Closed Date' => $rows->completed_date_time!='' ? date('d-M-Y H:i',strtotime($rows->completed_date_time)):"",
                    ];
                    $i++;
                   }
                   // print_r($order_arr);die;
                    
                }
                
                if(count($order_arr)>0){  
                    Excel::create($rptname, function($excel) use($order_arr) {
                        $excel->sheet('ExportFile', function($sheet) use($order_arr) {
                            $sheet->fromArray($order_arr);
                              $sheet->row(1, function($row) {  
                                    $row->setBackground('#FFFF00');  
                                });
                        });
                    })->export('xlsx');
                    Session::flash('success',"Excel generated successfully."); 
                    return back();
                }
                else{
                    Session::flash('danger','No Record Found.'); 
                    return back();
                }
            } catch (\Exception $e) {
                // Session::flash('danger',$e->getMessage());
                // return back();
                print_r($e->getMessage());die;
            }         
    }

    public function ecard_report()
    {
        $title = 'E-Card Report';
        return view('admin.reports.ecard_report',compact('title'));
    }

    public function exportEcard(Request $request){
            
        try { 
                $curr_time = date('Y-m-d H:i:s');
                $rptname =  'ecard_report('.$curr_time.')';
                $ecards= ECard::
                
                select("*");
                 
                 if($request->get('reg_sdate') && $request->get('reg_sdate')){
                    $start_date = $request->get('reg_sdate');
                    $end_date = $request->get('reg_edate');

                    $ecards= $ecards->
                         whereRaw('IF (`card_send_date` != null, `card_send_date` >= "'.$request->input('reg_sdate').'" and `card_send_date` <= "'.$request->input('reg_edate').'",`created_at` >= "'.$request->input('reg_sdate').'" AND `created_at` <= "'.$request->input('reg_edate').'")');

                }
                
                $ecards = $ecards
                ->get();
                // print_r($users);die;
                $ecard_arr = array();

                if(count($ecards)>0){
                    $i=1;
                    $status = array('0'=>'pending','1'=>'received','2'=>'cancelled');
                   foreach($ecards as $row){
                    $pay_mode = BookingPaymentDetail::where('booking_id',$row->id)
                            ->where('booking_type','ecard')
                            ->first();

                    if($row->receiver_id!=null){
                        $rec_name = $row->getAssociatedReceiverInfo->name;
                        $rec_email = $row->getAssociatedReceiverInfo->email;
                        $rec_mobile_no = $row->getAssociatedReceiverInfo->mobile_no;
                    }else{
                        $rec_name = $row->receiver_name;
                        $rec_email = $row->receiver_email;
                        $rec_mobile_no = $row->receiver_mobile_no;
                    }

                        $ecard_arr[] = [
                            "SNo" => $i,
                            "Card unique Id" => $row->card_unique_id,
                            "Ocassion Name" => $row->getAssociatedOccasionInfo->name,
                            "Sender Name" => $row->getAssociatedUserInfo->name,
                            "Sender Email" => $row->getAssociatedUserInfo->email,
                            "Sender Mobile Number" => $row->getAssociatedUserInfo->mobile_no,
                            "Receiver Name" => $rec_name,
                            "Receiver Email" => $rec_email,
                            "Receiver Mobile Number" => $rec_mobile_no,
                            "Amount" => $row->amount,
                            "Status" => $status[$row->status],
                            "Send Date" => $row->card_send_date!=null ? date('d-M-Y',strtotime($row->card_send_date)):date('d-M-Y',strtotime($row->created_at)),
                            "Received Date" => $row->card_received_date!=null ? date('d-M-Y H:i',strtotime($row->card_received_date)):date('d-M-Y',strtotime($row->card_received_date)),
                            "Payment Mode" => isset($pay_mode)? $pay_mode->payment_option:"",

                        ];
                        $i++;
                   }
                    
                }


                if(count($ecard_arr)>0){  
                    Excel::create($rptname, function($excel) use($ecard_arr) {
                        $excel->sheet('ExportFile', function($sheet) use($ecard_arr) {
                            $sheet->fromArray($ecard_arr);
                              $sheet->row(1, function($row) {  
                                    $row->setBackground('#FFFF00');  
                                });
                        });
                    })->export('xlsx');
                    Session::flash('success',"Excel generated successfully."); 
                    return back();
                }
                else{
                    Session::flash('danger','No Record Found.'); 
                    return back();
                }
            } catch (\Exception $e) {
                // Session::flash('danger',$e->getMessage());
                // return back();
                print_r($e->getMessage());die;
            }         
    }

    public function ecardGetData(Request $request)
    {
        $columns = ['card_unique_id','occasion_id','sender_detail','receiver_detail','amount','status','send_date','received_date','payment_mode'];

        if($request->get('reg_sdate') && $request->get('reg_sdate')){
            $start_date = $request->get('reg_sdate');
            $end_date = $request->get('reg_edate');

            $totalData= ECard::
                    // where(function ($query) use ($request){
                    //     $query->where(function ($query) use ($request){
                    //         $query->whereNotNull('card_send_date')
                    //             ->whereDate('e_cards.card_send_date','>=',$request->input('reg_sdate'))
                    //             ->whereDate('e_cards.card_send_date','<=',$request->input('reg_edate'));

                    //     })
                    //     ->orWhere(function ($query) use ($current_date) {
                    //         $query
                    //             ->whereDate('e_cards.created_at','>=',$request->input('reg_sdate'))
                    //             ->whereDate('e_cards.created_at','<=',$request->input('reg_edate'));
                    //     });
                        
                    // })->count();

            whereRaw('IF (`card_send_date` != null, `card_send_date` >= "'.$request->input('reg_sdate').'" and `card_send_date` <= "'.$request->input('reg_edate').'",`created_at` >= "'.$request->input('reg_sdate').'" AND `created_at` <= "'.$request->input('reg_edate').'")')->count();


        }else{
            $totalData= ECard::count();
        }
        
        $limit = $request->input('length');
        $start = $request->input('start');
        $sno =  $request->input('start')+1;
        // echo $request->input('order.0.column');die;
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        // $row = new User();
        $row= ECard::select('*');
                      
             if($request->get('reg_sdate') && $request->get('reg_sdate')){
                $start_date = $request->get('reg_sdate');
                $end_date = $request->get('reg_edate');

                $row= $row
                    // ->where(function ($query) use ($request){
                    //     $query->where(function ($query) use ($request){
                    //         $query->whereNotNull('card_send_date')
                    //             ->whereDate('e_cards.card_send_date','>=',$request->input('reg_sdate'))
                    //             ->whereDate('e_cards.card_send_date','<=',$request->input('reg_edate'));

                    //     })
                    //     ->orWhere(function ($query) use ($current_date) {
                    //         $query
                    //             ->whereDate('e_cards.created_at','>=',$request->input('reg_sdate'))
                    //             ->whereDate('e_cards.created_at','<=',$request->input('reg_edate'));
                    //     });
                        
                    // });
                ->whereRaw('IF (`card_send_date` != null, `card_send_date` >= "'.$request->input('reg_sdate').'" and `card_send_date` <= "'.$request->input('reg_edate').'",`created_at` >= "'.$request->input('reg_sdate').'" AND `created_at` <= "'.$request->input('reg_edate').'")');
            }         

// $row = new User();
        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                // ->orderBy('id', 'desc')
                ->orderBy($order, $dir)
                // ->withTrashed()
                ->get();
        // print_r($users);die;
        $data = array();
        if (!empty($row)) {
            $status = array('0'=>'pending','1'=>'received','2'=>'cancelled');
            foreach ($row as $key => $rows) {

                $pay_mode = BookingPaymentDetail::where('booking_id',$rows->id)
                            ->where('booking_type','ecard')
                            ->first();

                $nestedData['sno'] = $sno++;
                $nestedData['card_unique_id'] = $rows->card_unique_id;
                $nestedData['occasion_id'] = $rows->getAssociatedOccasionInfo->name;
                $nestedData['sender_detail'] = $rows->getAssociatedUserInfo->name.'<br><b>('.$rows->getAssociatedUserInfo->email.')<br/></b>'.$rows->getAssociatedUserInfo->mobile_no;

                if($rows->receiver_id!=null){
                    $nestedData['receiver_detail'] = $rows->getAssociatedReceiverInfo->name.'<br><b>('.$rows->getAssociatedReceiverInfo->email.')<br/></b>'.$rows->getAssociatedReceiverInfo->mobile_no;
                }else{
                    $nestedData['receiver_detail'] = $rows->receiver_name.'<br><b>('.$rows->receiver_email.')<br/></b>'.$rows->receiver_mobile_no;
                }
                $nestedData['amount'] = $rows->amount;
                $nestedData['status'] = $status[$rows->status];
                $nestedData['send_date'] = $rows->card_send_date!=null ? date('d-M-Y',strtotime($rows->card_send_date)):date('d-M-Y',strtotime($rows->created_at));
                $nestedData['received_date'] = date('d-M-Y H:i',strtotime($rows->card_received_date));
                $nestedData['payment_mode'] = isset($pay_mode)? $pay_mode->payment_option:"";
                $data[] = $nestedData;
            }

        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function sp_report()
    {
        $title = 'Service Provider Report';
        return view('admin.reports.sp_report',compact('title'));
    }

    public function exportSp(Request $request){
            
        try { 
                $curr_time = date('Y-m-d H:i:s');
                $rptname =  'sp_report('.$curr_time.')';
                $sp_users= SpUser::
                
                select("*");
                 
                 if($request->get('reg_sdate') && $request->get('reg_sdate')){
                    $start_date = $request->get('reg_sdate');
                    $end_date = $request->get('reg_edate');
                    $sp_users= $sp_users->whereDate('sp_users.created_at','>=',$request->input('reg_sdate'))
                    ->whereDate('sp_users.created_at','<=',$request->input('reg_edate'));
                }
                
                $sp_users = $sp_users
                ->get();
                // print_r($users);die;
                $sp_arr = array();

                if(count($sp_users)>0){
                    foreach ($sp_users as $rows) {

                        $no_ref = User::where('refer_code',$rows->share_code)->count();
                        $forum_count = Forum::where('user_id',$rows->id)->count();
                        $store_sr_count = SpService::where('user_id',$rows->id)
                        ->leftJoin('sp_service_prices', function($join) {
                          $join->on('sp_services.id', '=', 'sp_service_prices.sp_service_id');
                        })
                        ->where('field_key', 'like', '%_ORIGINALPRICE%')
                        ->count();

                        $hs_sr_count = SpService::where('user_id',$rows->id)
                        ->leftJoin('sp_service_prices', function($join) {
                          $join->on('sp_services.id', '=', 'sp_service_prices.sp_service_id');
                        })
                        ->where('field_key', 'like', '%_HOME_ORIGINALPRICE%')
                        ->count();
                        $store_offers = SpOffer::where('service_type','shop')->where('user_id',$rows->id)->pluck('id')->toArray();
                        $home_offers = SpOffer::where('service_type','home')->where('user_id',$rows->id)->pluck('id')->toArray();

                       
                        $reviews_count = Review::where('sp_id',$rows->id)->count();
                        $i=1;
                    // echo $rows->getAssociatedTierName->earning;die;
                        // $tier = Tier::where('id',$rows->tier_id)->first()->title;
                        // echo $tier;die;
                        $sp_arr[] = [
                            "SNo" => $i,
                            "Category Name" => $rows->getAssociatedCategoryName->category_name,
                            "Store Name" => $rows->store_name,
                            "Tier Name" =>  isset($rows->getAssociatedTierName->earning)?$rows->getAssociatedTierName->earning:"",
                            "Location" => $rows->address.','.$rows->landmark,
                            "Contact Name" => $rows->name,
                            "Store No" => $rows->store_number,
                            "Mobile No" => $rows->mobile_no,
                            "Whatsapp No" => $rows->whatsapp_no,
                            "Verified Status" => $rows->is_verified=='1'?'Verified':'Not Verified',
                            "Referral Code" =>$rows->share_code,
                            "Referral Count" => $no_ref,
                            "HS" => $rows->is_provide_home_service == '1'?'Yes':'No',
                            "MOV" => $rows->mov,
                            "Location Serve" => $rows->area_description,
                            "Store Active Status" => $rows->status=='1'?'Active':'InActive',
                            "Review Reviewed" => $reviews_count,
                            "VAT" =>$rows->is_tax_applied=='1'?'Yes':'No',
                            "Forum Activity" =>$forum_count,
                            "Store Service Count" =>$store_sr_count,
                            "Home Service Count" =>$hs_sr_count,
                            "Store Offers" => implode(',',$store_offers),
                            "Home Offers" => implode(',',$home_offers),
                            "Signup Date & Time" =>date('d-M-Y H:s',strtotime($rows->created_at)),

                        ];
                        $i++;
                   }
                   // print_r($sp_arr);die;
                    
                }


                if(count($sp_arr)>0){  
                    Excel::create($rptname, function($excel) use($sp_arr) {
                        $excel->sheet('ExportFile', function($sheet) use($sp_arr) {
                            $sheet->fromArray($sp_arr);
                              $sheet->row(1, function($row) {  
                                    $row->setBackground('#FFFF00');  
                                });
                        });
                    })->export('xlsx');
                    Session::flash('success',"Excel generated successfully."); 
                    return back();
                }
                else{
                    Session::flash('danger','No Record Found.'); 
                    return back();
                }
            } catch (\Exception $e) {
                // Session::flash('danger',$e->getMessage());
                // return back();
                print_r($e->getMessage().$e->getLine());die;
            }         
    }

    public function spGetData(Request $request)
    {
        $columns = ['category_id','store_name','tier','location','contact_name','store_no','mobile_no','whatsapp_no','verified_status','referral_code','referral_count','hs','mov','location_serve','store_active_status','review_reviewd','vat','forum_activity','store_service_count','hs_service_count','sp_date_time'];

        if($request->get('reg_sdate') && $request->get('reg_sdate')){
            $start_date = $request->get('reg_sdate');
            $end_date = $request->get('reg_edate');
            $totalData= SpUser::whereDate('created_at','>=',$request->input('reg_sdate'))
            ->whereDate('created_at','<=',$request->input('reg_edate'))->count();
        }else{
            $totalData= SpUser::count();
        }        


        $limit = $request->input('length');
        $start = $request->input('start');
        $sno =  $request->input('start')+1;
        // echo $request->input('order.0.column');die;
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        // $row = new User();
        $row= SpUser::
         
            select('*');
             
             if($request->get('reg_sdate') && $request->get('reg_sdate')){
                $start_date = $request->get('reg_sdate');
                $end_date = $request->get('reg_edate');
                $row= $row->whereDate('sp_users.created_at','>=',$request->input('reg_sdate'))
                ->whereDate('sp_users.created_at','<=',$request->input('reg_edate'));
            }
// $row = new User();
        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                // ->orderBy('id', 'desc')
                ->orderBy($order, $dir)
                // ->withTrashed()
                ->get();
        // print_r($users);die;
        $data = array();
        if (!empty($row)) {
            foreach ($row as $key => $rows) {

                $no_ref = User::where('refer_code',$rows->share_code)->count();
                $forum_count = Forum::where('user_id',$rows->id)->count();
                $store_sr_count = SpService::where('user_id',$rows->id)
                ->leftJoin('sp_service_prices', function($join) {
                  $join->on('sp_services.id', '=', 'sp_service_prices.sp_service_id');
                })
                ->where('field_key', 'like', '%_ORIGINALPRICE%')
                ->count();

                $hs_sr_count = SpService::where('user_id',$rows->id)
                ->leftJoin('sp_service_prices', function($join) {
                  $join->on('sp_services.id', '=', 'sp_service_prices.sp_service_id');
                })
                ->where('field_key', 'like', '%_HOME_ORIGINALPRICE%')
                ->count();

                $store_offers = SpOffer::where('service_type','shop')->where('user_id',$rows->id)->pluck('id')->toArray();
                $home_offers = SpOffer::where('service_type','home')->where('user_id',$rows->id)->pluck('id')->toArray();
               
                $reviews_count = Review::where('sp_id',$rows->id)->count();
                // $tier = Tier::where('id',$rows->tier_id)->first()->title;

                $nestedData['sno'] = $sno++;
                $nestedData['category_id'] = $rows->getAssociatedCategoryName->category_name;
                $nestedData['store_name'] = $rows->store_name;
                $nestedData['tier'] = isset($rows->getAssociatedTierName->earning)?$rows->getAssociatedTierName->earning:"";
                $nestedData['location'] = $rows->address.','.$rows->landmark;
                $nestedData['contact_name'] = $rows->name;
                $nestedData['store_no'] = $rows->store_number;
                $nestedData['mobile_no'] = $rows->mobile_no;
                $nestedData['whatsapp_no'] = $rows->whatsapp_no;
                $nestedData['verified_status'] = $rows->is_verified=='1'?'Verified':'Not Verified';
                $nestedData['referral_code'] = $rows->share_code;
                $nestedData['referral_count'] = $no_ref;
                $nestedData['hs'] = $rows->is_provide_home_service == '1'?'Yes':'No';
                $nestedData['mov'] = $rows->mov;
                $nestedData['location_serve'] = $rows->area_description;
                $nestedData['store_active_status'] = $rows->status=='1'?'Active':'InActive';
                $nestedData['review_reviewd'] =  $reviews_count;
                $nestedData['vat'] = $rows->is_tax_applied=='1'?'Yes':'No';
                $nestedData['forum_activity'] = $forum_count;
                $nestedData['store_service_count'] = $store_sr_count;          
                $nestedData['hs_service_count'] = $hs_sr_count;
                $nestedData['store_offers'] = implode(',',$store_offers);
                $nestedData['home_offers'] = implode(',',$home_offers);
                $nestedData['sp_date_time'] = date('d-M-Y H:s',strtotime($rows->created_at));
                // $nestedData['approval_date_time'] = ;

                
                $data[] = $nestedData;
            }

        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    
}
