<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;   
use App\Model\Category;
use App\Model\Service;
use App\Model\SpService;
use App\Model\PromoCodeLinkedService;
use App\Model\QuickFact;
use App\Model\AddOnService;
use datatables;
use App;
use Session;

class NewServiceController extends Controller
{
    protected $title;
    protected $model;
    protected $module;

    public function __construct()
    {
        $this->title = 'Services';
        $this->model = 'service';
        $this->module = 'service';
    } 

    public function index()
    {
        $title = 'Services';
        $module = $this->module;
        $model = $this->model;
        $breadcum = [$title=>route($model.'.index')];
        return view('admin.service.new.index',compact('title','model','module','breadcum'));
    }

    public function getData(Request $request)
    {

        $columns = ['name','name_ar','cat_id','subcat_id','created_at','action'];
        $totalData = Service::count();
        
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        $row = Service::select('services.*');
        // $totalFiltered = User::count();

        if (!empty($request->input('search.value'))) {
            $search = $request->input('search.value');

            $row = $row->leftJoin('categories', 'categories.id', '=', 'services.subcat_id')
                ->where(function($query) use ($search) {
                    $query->Where('categories.category_name', 'LIKE', "%{$search}%")
                    ->orWhere('services.name', 'LIKE', "%{$search}%");
                    
                });
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        // print_r($users);die;
        $data = array();
        if (!empty($row)) {
            foreach ($row as $key => $rows) {
                $nestedData['name'] = $rows->name;
                $nestedData['name_ar'] = $rows->name_ar;
                $nestedData['cat_id'] = $rows->getAssociatedcategoryName->category_name;
                $nestedData['subcat_id'] = isset($rows->getSubcat->category_name)?$rows->getSubcat->category_name:"";
                $associatedTables = $this->checkAssociatedTables($rows->id);
                $nestedData['created_at'] = date('d-M-y h:i:s',strtotime($rows->created_at));
                $nestedData['action'] =  getButtons([
                                ['key'=>'edit','link'=>route('service.edit',$rows->id)],
                                ['key'=>'delete_assoc','link'=>route('service.destroy',$rows->id), 'asso'=>$associatedTables],
                            ]);
                
                $data[] = $nestedData;
            }

        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function checkAssociatedTables($id)
    {
        $tables = '';
        $service_count = SpService::where('service_id',$id)->count();
        $qf_count = PromoCodeLinkedService::where('service_id',$id)->count();
        $promo_count = QuickFact::where('service_id',$id)->count();

        if($service_count > 0){
            $tables = $tables.'SpServices__';
        }
        if($qf_count > 0){
            $tables = $tables.'QuickFacts__';
        }
        if($promo_count > 0){
            $tables = $tables.'Promocodes';
        }
        return $tables;
        //return explode(',',$tables);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action = "create";
        $title = 'Create Service';
        $model = $this->model;
        $breadcum = [$this->title=>route('new-'.$model.'.index'),$title =>''];
        $categories = $this->getCategoryName();
        $services = $this->getServiceName();
        return view('admin.service.new.create',compact('action','title','breadcum','model','module','categories','services'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $validation = array(
                'cat_id' => 'required',
                'subcat_id' => 'required',
                'name' => 'required',
                'name_ar' => 'required',
                // 'price' => 'required',
                'description' => 'required',
                'description_ar' => 'required',
                'serviceTimeHour' => 'required',
                'serviceTimeMin' => 'required',
            );

            $validator = Validator::make(Input::all(), $validation);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
            } 
            else {

                $row = new Service();
                $time=$request->serviceTimeHour.':'.$request->serviceTimeMin.':'.'00';
                $row->cat_id = $request->cat_id;
                $row->subcat_id = $request->subcat_id;
                $row->name = $request->name;
                $row->name_ar = $request->name_ar;
                $row->price = $request->price;
                $row->time = $time;
                $row->description = $request->description;
                $row->description_ar = $request->description_ar;
                $row->save();
                if($row->save()){
                    if($request->addon){
                        foreach($request->addon as $vals){
                            $addon_row = new AddOnService();
                            $addon_row['service_id'] = $row->id;
                            $addon_row['addon_service_id'] = $vals;
                            $addon_row->save();
                        }
                    }
                }
                
                Session::flash('success', __('Service added successfully.'));
                return redirect()->route('new-service.index');
            }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $row = Service::where('id',$id)->first();
        $title = 'Edit Service';
        $model = $this->model;
        $breadcum = [$this->title=>route('new-'.$model.'.index'),$title =>''];
        $categories = $this->getCategoryName();
        $services = $this->getServiceName();
        $services_1 = AddOnService::where('service_id',$id)->pluck('addon_service_id');
        $cat_id = Service::where('id',$id)->pluck('cat_id');
        $sub_cat_arr = Category::where('parent_id',$cat_id)->get();
        foreach($sub_cat_arr as $data)
        {
            $subcat_arr[$data['id']] = $data['category_name']; 
        }

        return view('admin.service.new.edit',compact('row','title','breadcum','categories','services','services_1','subcat_arr'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $validation = array(
                'cat_id' => 'required',
                'subcat_id' => 'required',
                'name' => 'required',
                'name_ar' => 'required',
                // 'price' => 'required',
                'description' => 'required',
                'description_ar' => 'required',
                'serviceTimeHour' => 'required',
                'serviceTimeMin' => 'required',
            );

            $validator = Validator::make(Input::all(), $validation);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
            } 
            else {

                $row = Service::where('id',$id)->first();
                $time=$request->serviceTimeHour.':'.$request->serviceTimeMin.':'.'00';
                $row->cat_id = $request->cat_id;
                $row->subcat_id = $request->subcat_id;
                $row->name = $request->name;
                $row->name_ar = $request->name_ar;
                $row->price = $request->price;
                $row->time = $time;
                $row->description = $request->description;
                $row->description_ar = $request->description_ar;
                $row->save();
                if($row->save()){
                    if($request->addon){
                        foreach($request->addon as $vals){
                            AddOnService::where('service_id',$row->id)->delete();
                            AddOnService::manageAddOnServices($row->id, $vals);
                        }
                    }
                }
                
                Session::flash('success', __('Service Updated successfully.'));
                return redirect()->route('new-service.index');
            }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $row = Service::where('id', $id)->first();
        if ($row) {
            // $row->delete();
             Session::flash('success', __('Service deleted successfully.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('Invalid request.'));
            return redirect()->back();
        }
    }

    public function getCategoryName()
    {
        $cat_data = Category::where('parent_id','0')->get();
        $cat_arr = array();

        foreach($cat_data as $data)
        {
            $cat_arr[$data['id']] = $data['category_name']; 
        }
         
        return $cat_arr;
    }

    public function getServiceName()
    {
        $service_data = Service::get();
        $service_arr = array();

        foreach($service_data as $data)
        {
            $service_arr[$data['id']] = $data['name']; 
        }
         
        return $service_arr;
    }

    public function ajaxSubcategory(Request $request)
    {
        $cat_id = $request->cat_id;

        $cat_data = Category::where('parent_id',$cat_id)->get();
        $cat_arr = array();

        foreach($cat_data as $data)
        {
            $cat_arr[$data['id']] = $data['category_name']; 
        }
         
        return view('admin.subcategory.new.ajaxSubcategory',compact('cat_arr'));

    }
}
