<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;   

use App\Model\ServiceAttribute;
use App\Model\Category;
use datatables;
use App;
use Session;

class ServiceAttributeController extends Controller
{
    protected $title;
    protected $model;
    protected $module;

    public function __construct()
    {
        $this->title = 'Service Attribute';
        $this->model = 'service-attribute';
        $this->module = 'service-attribute';
    } 

    public function index()
    {
        $title = 'Service Attribute';
        $module = $this->module;
        $model = $this->model;
        $breadcum = [$title=>'Service Attributes'];        
        return view('admin.service_attribute.index',compact('title','model','module','breadcum'));
    }

    public function getData(Request $request)
    {
        
        $columns = ['name','name_ar','cat_id','created'];
        $totalData = ServiceAttribute::count();
        
        
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        $row = ServiceAttribute::select("service_attributes.*");
        
        if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = $row->Where('name', 'LIKE', "%{$search}%");
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('service_attributes.id', 'ASC')
                ->get();
        
        $category_arr = $this->getCategoryName();
        $data = array();
        if (!empty($row)) {
            foreach ($row as $key => $rows) {
                $nestedData['name'] = $rows->name;
                $nestedData['name_ar'] = $rows->name_ar;
                $nestedData['cat_id'] = $category_arr[$rows->cat_id];
                $nestedData['created'] = date('Y-m-d h:i A', strtotime($rows->created_at));
                $nestedData['action'] =  getButtons([
                            ['key'=>'edit','link'=>route('service-attribute.edit',$rows->id)],
                            ['key'=>'delete_assoc','link'=>route('service-attribute.destroy',$rows->id), 'asso'=>''],
                        ]);
                
                $data[] = $nestedData;
            }

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }
  
    public function create()
    {
        $action = "create";
        $title = 'Create Service Attribute';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        $categories = $this->getCategoryName();
        return view('admin.service_attribute.create',compact('action','title','breadcum','model','categories'));
    }

    
    public function store(Request $request)
    {
        try{
             if (Input::isMethod('post')) {
                $validation = array(
                    'cat_id' => 'required',
                    'name'=>'required',
                    'name_ar'=>'required',
                );
                $validator = Validator::make(Input::all(), $validation);

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {
                    $row = new ServiceAttribute();
                    $row['cat_id'] = $request->cat_id;
                    $row['name'] = $request->name;
                    $row['name_ar'] = $request->name_ar;
                    $row['url'] = str_replace(' ','_',strtolower($request->name));
                    $row['description'] = $request->description;
                    if($row->save())
                    {
                        Session::flash('success', __('Service Attribute added successfully.'));
                        return redirect()->route('service-attribute.index');
                    }                    
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

     
    public function edit($id)
    {
        $rows = ServiceAttribute::where('id',$id)->first();
        $title = 'Edit Service Attribute';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        $categories = $this->getCategoryName();
        return view('admin.service_attribute.edit',compact('rows','title','breadcum','categories'));
    }

   
    public function update(Request $request, $id)
    {
        try{
             if (Input::isMethod('PATCH')) {
                $validation = array(
                    'cat_id' => 'required',
                    'name'=>'required',
                    'name_ar'=>'required',
                );

                $validator = Validator::make(Input::all(), $validation);

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {
                    $row = ServiceAttribute::where('id',$id)->first();
                    $row['cat_id'] = $request->cat_id;
                    $row['name'] = $request->name;
                    $row['name_ar'] = $request->name_ar;
                    $row['url'] = str_replace(' ','_',strtolower($request->name));
                    $row['description'] = $request->description;
                    if($row->save())
                    {
                        Session::flash('success', __('Service Attribute updated successfully.'));
                        return redirect()->route('service-attribute.index');
                    }
                    
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

  
    public function destroy($id)
    {
        $row = ServiceAttribute::where('id', $id)->first();
        if ($row) {
            $row->delete();
             Session::flash('success', __('Service Attribute deleted successfully.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('Invalid request.'));
            return redirect()->back();
        }
    }

    public function getCategoryName()
    {
        $cat_data = Category::where('parent_id','0')->get();
        $cat_arr = array();

        foreach($cat_data as $data)
        {
            $cat_arr[$data['id']] = $data['category_name']; 
        }
         
        return $cat_arr;
    }
}
