<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;   

use App\Model\Review;
use App\Model\Booking;
use App\Model\ReviewQuestionRating;
use App\Model\ReviewSpService;
use App\Model\SpUser;
use App\Model\Staff;
use App\Model\SpService;
use App\Model\RatingReview;
use datatables;
use App;
use Session;    
use DB;
class ReviewsController extends Controller
{
    
    protected $title;
    protected $model;
    protected $module;

    public function __construct()
    {
        $this->title = 'Reviews';
        $this->model = 'reviews';
        $this->module = 'reviews';
    } 

    public function index()
    {
        $title = $this->title;
        $module = $this->module;
        $model = $this->model;
        $breadcum = [$title=>route($model.'.index')];
        return view('admin.reviews.index',compact('title','model','module','breadcum'));
    }

    public function getData(Request $request)
    {

        $columns = ['sp_id','customer_name','beutics_rating','is_approved','action'];
        $totalData = Review::count();
        
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        $row = Review::select("reviews.*");
        // $totalFiltered = User::count();

        // if (!empty($request->input('search.value'))) {
        //     $search = $request->input('search.value');
        //     $row = $row->where(function($query) use ($search) {
        //         $query->where('customer_name', 'LIKE', "%{$search}%");
        //     });
        // }

        if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = $row->leftJoin('users', 'users.id', '=', 'reviews.user_id')
                    ->leftJoin('sp_users', 'sp_users.id', '=', 'reviews.sp_id')

                ->where(function($query) use ($search) {
                    $query->Where('users.name', 'LIKE', "%{$search}%")
                    ->orWhere('sp_users.store_name', 'LIKE', "%{$search}%")
                    ->orWhere('sp_users.name', 'LIKE', "%{$search}%");
                });
        }


        if (!empty($request->input('status_filter'))) 
        {
            $is_approvedsearch = $request->input('status_filter');
            $is_approvedsearch_1 = '';
            if($is_approvedsearch == 'pending')
            {
                $is_approvedsearch_1 = '0';
            }
            if($is_approvedsearch == 'approved')
            {
                $is_approvedsearch_1 = '1';
            }
            $row = $row->where(function($query) use ($is_approvedsearch_1) {
                $query->where('reviews.is_approved', $is_approvedsearch_1);
            });
        }

        if (!empty($request->input('review_type'))) 
        {
            //echo $request->input('review_type');die;
            $is_approvedsearch = $request->input('review_type');
            if($is_approvedsearch == 'customer')
            {

                    $row = $row->where('reviews.user_id','!=', '');
               
            }
            if($is_approvedsearch == 'admin')
            {
                
                    $row = $row->whereNull('reviews.user_id')->orWhere('reviews.user_id', '');
                
            }
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        // print_r($users);die;
        $data = array();
        if (!empty($row)) {
            foreach ($row as $key => $rows) {
                $nestedData['sp_id'] = $rows->getAssociatedSpName->store_name;
                $nestedData['customer_name'] = $rows->customer_name;
                $nestedData['description'] = $rows->description;
                if($rows->user_id){
                    $nestedData['customer_name'] = $rows->getAssociatedUser->name;
                }
                if($rows->rate_beutics){
                    $rating_arr = \Config::get('constants.rating_arr');
                    $nestedData['beutics_rating'] = $rating_arr[$rows->rate_beutics]['value'];
                } else {
                    $nestedData['beutics_rating'] = '-';
                }
                
                $nestedData['is_approved'] = getApprovalStatus($rows->is_approved,$rows->id);
                $nestedData['action'] =  getButtons([
                                ['key'=>'edit','link'=>route('reviews.edit',$rows->id)],
                                ['key'=>'delete','link'=>route('reviews.destroy',$rows->id)],
                                ['key'=>'view','link'=>route('reviews.show',$rows->id)],
                            ]);
                $nestedData['description'] = $rows->description;
                $avg_rat = $this->getIndividualReview($rows->id);

                $nestedData['overall_review'] = '<b>'.$avg_rat['average_rating'].'</b>';
                //echo '<pre>'; print_r($nestedData);die;
                $data[] = $nestedData;
            }

        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function create()
    {
        $action = "create";
        $title = 'Add Review';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        $service_providers = $this->getSpName();
        $reviews_ques_arr = \Config::get('constants.review_questions_arr');
        $rating_arr = \Config::get('constants.rating_arr');

        return view('admin.reviews.create',compact('action','title','breadcum','model','module','service_providers','reviews_ques_arr','rating_arr'));
    }

    public function getSpName()
    {
        $sp_data = SpUser::get();
        $sp_arr = array();

        foreach($sp_data as $data)
        {
            $sp_arr[$data['id']] = $data['store_name']; 
        }
         
        return $sp_arr;
    }
    //get staff and service info
    public function getSpStaffServices(Request $request)
    {
        $sp_id = $request->sp_id;



        $staff_data = Staff::where('sp_id',$sp_id)->select('staff_name','id')->get();
        $staff_arr = array();
        foreach($staff_data as $staff_row){
            $staff_arr[$staff_row['id']] = $staff_row['staff_name'];
        }

        $service_arr = array();
        $service_data = SpService::where('user_id',$sp_id)->select('service_id','id')->get();
        foreach($service_data as $service_row){
            $service_arr[$service_row['id']] = $service_row->getAssociatedService->name;
        }

        $final_arr = array();
        $final_arr['staff_data'] = $staff_arr;
        $final_arr['service_data'] = $service_arr;

        if(isset($request->staff_id))
        $staff_id = $request->staff_id;
        else $staff_id = '';
        
        if(isset($request->sp_sevices_data))
        $sp_sevices_data = $request->sp_sevices_data;
        else $sp_sevices_data = '';

        if(isset($request->selected_staff_like_dislike))
        $selected_staff_like_dislike = $request->selected_staff_like_dislike;
        else $selected_staff_like_dislike = '';
               
        return view('admin.reviews.ajaxSpStaffServices',compact('final_arr','staff_id','sp_sevices_data','selected_staff_like_dislike'));
        
    }
    //ajax for searching
    public function ajaxGetSpSearchedStaffServices(Request $request)
    {

        $sp_id = $request->sp_id;
        //print_r($request->all());die;
        if($request->search_name == "staff"){
            $staff_data = Staff::where('sp_id',$sp_id)->select('staff_name','id')->where('staff_name', 'LIKE', '%' . $request->search_keyword . '%')->get();

            $staff_arr = array();
            foreach($staff_data as $staff_row){
                $staff_arr[$staff_row['id']] = $staff_row['staff_name'];
            }
            
            $final_arr = array();
            $final_arr['staff_data'] = $staff_arr;
            $final_arr['search_name'] = $request->search_name;
            $final_arr['selected_staff_id'] = $request->selected_staff_id;
            //return $final_arr;

        }else{
            $service_arr = array();
            $service_data = SpService::leftJoin('services', function($join) {
                              $join->on('sp_services.service_id', '=', 'services.id');
                            })
            ->where('user_id',$sp_id)
            ->select('sp_services.service_id','sp_services.id')
            ->where('services.name', 'LIKE', '%' . $request->search_keyword . '%')
            ->get();

            foreach($service_data as $service_row){
                $service_arr[$service_row['id']] = $service_row->getAssociatedService->name;
            }

             $final_arr = array();
             $final_arr['search_name'] = $request->search_name;
             $final_arr['service_data'] = $service_arr;
             $final_arr['service_ids_check'] = explode(',', $request->service_id_check);
             //return $final_arr;
        }
        //dd($final_arr);
        return view('admin.reviews.ajaxSpSearchedStaffServices',compact('final_arr'));
        
    }


  
    public function store(Request $request)
    {
        try{ //echo '<pre>'; print_r($request->all());die;
             if (Input::isMethod('post')) {
                $validation = array(
                    'sp_id' => 'required',
                    'customer_name' => 'required',
                    'description' => 'required',
                    'staff_id' => 'nullable',
                    'staff_like_dislike' => 'nullable',
                    'service_id' => 'nullable',
                    'question1' => 'required',
                    'question2' => 'required',
                    'question3' => 'required',
                    'question4' => 'required',
                    'question5' => 'required',
                    'question6' => 'required',

                );

                $validator = Validator::make(Input::all(), $validation);

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {

                    $row = new Review();
                    $row->sp_id = $request->sp_id;
                    $row->customer_name = $request->customer_name;
                    $row->description = $request->description;
                    $row->staff_id = $request->staff_id;
                    $row->staff_like_dislike = $request->staff_like_dislike;
                    $row->is_approved = '1';
                    
                    $row->save();
                    if($row->save()){
                        
                       for($i=1;$i<=6;$i++){
                           
                           $rating_row = new ReviewQuestionRating();
                            $rating_row['review_id'] = $row->id;
                            $rating_row['ques_id'] = $i;
                            $rating_row['rating'] = $request['question'.$i];
                            $rating_row->save();
                            
                        }
                        if($request->service_id){
                            foreach($request->service_id as $srow)
                            {
                                $like_check = 'service_like_dislike'.$srow;
                                $service_row = new ReviewSpService();
                                $service_row['review_id'] = $row->id;
                                $service_row['service_id'] = $srow;
                                $service_row['like_dislike'] = $request->$like_check;
                                $service_row->save();
                                
                            }
                        }

                    }   
                    
                    Session::flash('success', __('Review added successfully.'));
                    return redirect()->route('reviews.index');
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

  
    public function show($id)
    {
        $title = '';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];

        $reviews_ques_arr = \Config::get('constants.review_questions_arr');
        $rating_arr = \Config::get('constants.rating_arr');

        $customer_reviews = $this->getIndividualReview($id);
        
        $re_ques_rating = Review::where('id',$id)->first();
        if($re_ques_rating['rate_beutics']){
            $rating_arr = \Config::get('constants.rating_arr');
            $re_ques_rating['rate_beutics'] = $rating_arr[$re_ques_rating['rate_beutics']]['value'];
        } else {
            $re_ques_rating['rate_beutics'] = '-';
        }

        return view('admin.reviews.view',compact('title','model','breadcum','customer_reviews','reviews_ques_arr','rating_arr','re_ques_rating'));

    }

    public function showOverallSpReviews($sp_id)
    {
        $sp_row = SpUser::where('id',$sp_id)->select('store_name')->first();
        $title = 'View Reviews';
        $model = 'sp';
        $breadcum = ['Service Provider'=>route($model.'.index'),$title =>''];

        $customer_reviews = $this->getCustomerReviews($sp_id);
        $customer_combined_reviews = $this->getCombinedReviews($sp_id);

        // echo "<pre>";print_r($this->getCombinedReviews());die;
        return view('admin.reviews.overall_review_view',compact('title','model','breadcum','customer_reviews','customer_combined_reviews','sp_id'));

    }

    public function getIndividualReview($id)
    {
        $reviews_ques_arr = \Config::get('constants.review_questions_arr');
        $rating_arr = \Config::get('constants.rating_arr');
        

        $review = Review::where('id',$id)->first();
        if($review){

            $rating_calc = array();
            foreach($review->getAssociatedReviewQuestions as $review_ques)
            {
                $ques_value =  $reviews_ques_arr[$review_ques['ques_id']]['value'];
                $ans =  $rating_arr[$review_ques['rating']]['value'];

                $rating_calc[] = $ques_value*$ans;
            }

            $final_rating = (array_sum($rating_calc))/10;
            $result = array();

            $result['average_rating'] = round($final_rating,2);
            return $result;
        }
       
    }
   
    public function edit($id)
    {
        $rows = Review::where('id',$id)->get();
        if($rows[0]->user_id){
            $customer_name = $rows[0]->getAssociatedUser->name;
        } else {
           $customer_name = $rows[0]->customer_name;
        }

        $row = Review::where('id',$id)->first();
        $booking_data = array();
        if($row['booking_id']){
            $booking_data = Booking::where('id',$row['booking_id'])->select('booking_unique_id')->first()->toArray();
        }
        
        
        if($customer_name){
            $row['customer_name'] = $customer_name;
        } 
        $rating_row = ReviewQuestionRating::where('review_id',$id)->orderBy('review_question_ratings.ques_id','ASC')->get();

        $title = 'Edit Review';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        $service_providers = $this->getSpName();
        $reviews_ques_arr = \Config::get('constants.review_questions_arr');
        $rating_arr = \Config::get('constants.rating_arr');

        $review_sp_services_data = ReviewSpService::select('service_id','like_dislike')->where('review_id',$id)->get();



        return view('admin.reviews.edit',compact('row','title','breadcum','model','service_providers','reviews_ques_arr','rating_arr', 'rating_row','review_sp_services_data', 'booking_data'));
    }

  
    public function update(Request $request,$id)
    {
        
        try{

            if (Input::isMethod('PATCH')) {

                $data = $request->all();

                //dd($data);

                $validation = array(
                    'sp_id' => 'required',
                    'customer_name' => 'required',
                    'description' => 'required',
                    'staff_id' => 'nullable',
                    'staff_like_dislike' => 'nullable',
                    'service_id' => 'nullable',
                    'question1' => 'required',
                    'question2' => 'required',
                    'question3' => 'required',
                    'question4' => 'required',
                    'question5' => 'required',
                    'question6' => 'required',

                );

                $validator = Validator::make(Input::all(), $validation,[
                        'service_id.required' => 'Please choose service',
                        'staff_id.required' => 'Please choose staff',
                        'staff_like_dislike.required' => 'Please like or dislike the staff',
                    ]);

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {

                    $row = Review::where('id',$id)->first();
                    //dd($row);
                    $row->sp_id = $request->sp_id;
                    $row->customer_name = $request->customer_name;
                    $row->description = $request->description;
                    $row->staff_id = $request->staff_id;
                    $row->staff_like_dislike = $request->staff_like_dislike;
                    
                    $row->save();

                    //dd($row->save());

                    if($row->save()){
                       
                       ReviewQuestionRating::where('review_id',$row->id)->delete(); 
                       for($i=1;$i<=6;$i++){
                           
                           $rating_row = new ReviewQuestionRating();
                            $rating_row['review_id'] = $row->id;
                            $rating_row['ques_id'] = $i;
                            $rating_row['rating'] = $request['question'.$i];
                            $rating_row->save();
                            
                        }

                        ReviewSpService::where('review_id',$row->id)->delete();
                        if($request->service_id){
                            foreach($request->service_id as $srow)
                            {
                                $like_check = 'service_like_dislike'.$srow;
                                $service_row = new ReviewSpService();
                                $service_row['review_id'] = $row->id;
                                $service_row['service_id'] = $srow;
                                $service_row['like_dislike'] = $request->$like_check;
                                $service_row->save();
                                
                            }
                        }
                        
                    }   
                    
                    Session::flash('success', __('Review is updated successfully.'));
                    return redirect()->route('reviews.index');
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

    public function getCustomerReviews($sp_id)
    {
       
        $user_id = $sp_id;

        $results = RatingReview::select('rating_reviews.id','reviews.customer_name','reviews.description','reviews.created_at',DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1) as rating"))
            ->leftJoin('reviews', function($join) {
                  $join->on('rating_reviews.id', '=', 'reviews.id');
                })
            ->where('rating_reviews.sp_id',$user_id);            
            
            $results = $results->groupBy('rating_reviews.id')->paginate(10);
            // $rows_arr = $results->toArray();
            return $results;
    }

    public function getCustomerReviewsFilter(Request $request)
    {
       
        $user_id = $request->sp_id;

        $results = RatingReview::select('rating_reviews.id','reviews.customer_name','reviews.user_id','reviews.description','reviews.created_at',DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1) as rating"))
            ->leftJoin('reviews', function($join) {
                  $join->on('rating_reviews.id', '=', 'reviews.id');
                })
            ->where('rating_reviews.sp_id',$user_id);


            if($request->filter == 'needs_improvement'){
                 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'>',2.3);
                 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'<',5);                         
            }
            elseif($request->filter == 'average'){
                 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'>',4.9);
                 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'<=',6);                            
            }
            elseif($request->filter == 'good'){
                 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'>',6);
                 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'<=',7);                            
            }
            elseif($request->filter == 'very_good'){
                 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'>',7);
                 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'<=',8);                            
            }
            elseif($request->filter == 'superb'){
                 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'>',8);
                 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'<=',9);                            
            }elseif($request->filter == 'exceptional'){
                 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'>',9);
                 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'<=',10);                           
            }               
            
            $results = $results->groupBy('rating_reviews.id')->paginate(10);
            // print_r($results);die;
            // $rows_arr = $results->toArray();
            return view('admin.reviews.customerReviewsFilter',compact('results'));
    }

    public function getCombinedReviews($sp_id)
    {
        $reviews_ques_arr = \Config::get('constants.review_questions_arr');
        $rating_arr = \Config::get('constants.rating_arr');

        $reviews = Review::where('sp_id',$sp_id)->get();

        if(count($reviews)>0){
            $rows_arr = $reviews->toArray();

            $review_arr = array();
        
            $service_wise_arr = array();
            foreach($reviews as $rows)
            {
                $rating_calc = array();
                foreach($rows->getAssociatedReviewQuestions as $review_ques)
                {
                    $ques_value =  $reviews_ques_arr[$review_ques['ques_id']]['value'];
                    $ans =  $rating_arr[$review_ques['rating']]['value'];

                    $rating_calc[] = $ques_value*$ans;
                }

                foreach($rows->getAssociatedReviewServices as $review_sr)
                {
                    if(!isset($service_wise_arr[$review_sr['service_id']]['dislike_count'])) {
                        $service_wise_arr[$review_sr['service_id']]['dislike_count'] = 0;
                    }

                    if($review_sr['like_dislike'] == 0){
                        $service_wise_arr[$review_sr['service_id']]['dislike_count'] += (integer)1;
                        
                    }
                    
                    if(isset($service_wise_arr[$review_sr['service_id']]['like_count'])){
                        $service_wise_arr[$review_sr['service_id']]['like_count'] += (integer)$review_sr['like_dislike'];
                        
                    } else{
                        $service_wise_arr[$review_sr['service_id']]['like_count'] = 0 + (integer)$review_sr['like_dislike'];
                        
                    }
                    //$service_wise_arr[$review_sr['service_id']]['likes_dislikes'][] =  $review_sr['like_dislike'];
                    if(!isset($service_wise_arr[$review_sr['service_id']]['service_id'])) {
                        $service_wise_arr[$review_sr['service_id']]['service_id'] =  $review_sr['service_id'];
                        $service_wise_arr[$review_sr['service_id']]['service_name'] =  $review_sr->getAssociatedReviewService->getAssociatedService->name;
                    }
                }


                $final_rating = (array_sum($rating_calc))/10;

                $review_arr[$rows['id']] = $final_rating;

            }
             array_multisort(array_map(function($element) {
                  return $element['like_count'];
              }, $service_wise_arr), SORT_DESC, $service_wise_arr);
             $top_ten = array_slice($service_wise_arr, 0,10);
             
            $total_reviews = array_sum($review_arr)/count($review_arr);
        
            $question_wise_arr = array();
            foreach($reviews as $rows)
            {
                foreach($rows->getAssociatedReviewQuestions as $review_ques)
                {
                    $question_wise_arr[$review_ques['ques_id']][] = $rating_arr[$review_ques['rating']]['value'];
                }

            }
            
            $ques_final_calc = array();

            foreach($question_wise_arr as $key => $val){
                $ques_final_calc[$reviews_ques_arr[$key]['key_value']] = round((array_sum($val))/count($val),0);
            }

            $finale_data = $ques_final_calc;
            $finale_data['average_rating'] = round($total_reviews,1);
            $finale_data['total_reviewers'] = count($review_arr);
            $finale_data['services'] = $service_wise_arr;
            
            return  $finale_data;
        }
    }
  
    public function destroy($id)
    {
        $row = Review::where('id', $id)->first();
        if ($row) {
            $row->delete();
             Session::flash('success', __('Review deleted successfully.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('Invalid request.'));
            return redirect()->back();
        }
    }

    public function StatusUpdate(Request $request)
    {
        $review_id = $request->review_id;
        $row  = Review::whereId($review_id)->first();
        $row->is_approved =  $row->is_approved=='1'?'0':'1';
        $row->save();
        $html = '';

       switch ($row->is_approved) {
          case '1':
               $html =  '<a data-toggle="tooltip"  class="btn btn-success btn-xs" title="Active" onClick="changeApproval('.$review_id.')" >Yes</a>';
              break;
               case '0':
               $html =  '<a data-toggle="tooltip"  class="btn btn-danger btn-xs" title="Inactive" onClick="changeApproval('.$review_id.')" >No</a>';
              break;
          
          default:
            
              break;
      }
      return $html;

    }


}
