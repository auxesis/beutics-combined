<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;   

use App\Model\PromoCode;
use App\Model\Booking;
use App\Model\SpUser;
use App\Model\User;
use App\Model\Service;
use App\Model\Category;
use App\Model\PromoCodeLinkedSubcategory;
use App\Model\PromoCodeLinkedService;
use App\Model\PromoCodeLinkedCustomer;
use App\Model\PromoCodeLinkedStore;
use App\Model\PromoCodeLinkedCategory;
use App\Model\Notification;

use datatables;
use App;
use Session;    

class PromoCodeController extends Controller
{
    
    protected $title;
    protected $model;
    protected $module;

    public function __construct()
    {
        $this->title = 'Promo Code';
        $this->model = 'promocode';
        $this->module = 'promocode';
    } 

    public function index()
    {
        $title = $this->title;
        $module = $this->module;
        $model = $this->model;
        $breadcum = [$title=>route($model.'.index')];
        return view('admin.promoCodes.index',compact('title','model','module','breadcum'));
    }

    public function getData(Request $request)
    {

        $columns = ['promo_code_title','promo_code_title_ar','from_date','to_date','minimum_order_amount','code_limit','status','action'];
        $totalData = PromoCode::count();
        
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        $row = new PromoCode();
        // $totalFiltered = User::count();

        if (!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = $row->where(function($query) use ($search) {
                $query->where('promo_code_title', 'LIKE', "%{$search}%");
            });
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('id', 'DESC')
                ->get();
        // print_r($users);die;
        $data = array();
        if (!empty($row)) {
            foreach ($row as $key => $rows) {
                $nestedData['promo_code_title'] = $rows->promo_code_title;
                $nestedData['promo_code_title_ar'] = $rows->promo_code_title_ar;
                $nestedData['from_date'] = date('d-M-Y',strtotime($rows->from_date));
                $nestedData['to_date'] = date('d-M-Y',strtotime($rows->to_date));
                $nestedData['minimum_order_amount'] = $rows->minimum_order_amount;
                $nestedData['code_limit'] = $rows->code_limit;
                $nestedData['status'] = getStatus($rows->status,$rows->id);
                $nestedData['action'] =  getButtons([
                                ['key'=>'edit','link'=>route('promocode.edit',$rows->id)],
                                ['key'=>'delete','link'=>route('promocode.destroy',$rows->id)],
                                ['key'=>'view','link'=>route('promocode.show',$rows->id)],
                                ['key'=>'view_promocode','link'=>route('promocode.promo-log',$rows->id)],
                            ]);
                if($rows->linked_store){ $str = 'Selected Store->'; } else { $str = 'All Store->'; }
                if($rows->linked_customer){ $str .= 'Selected Customer->'; } else { $str .= 'All Customer->'; }
                if($rows->linked_level == '1'){
                    $str .= 'Subcategory'; 
                } elseif($rows->linked_level == '2'){
                    $str .= 'Category'; 
                } else {
                    $str .= 'Service'; 
                }
                $nestedData['flow'] = $str;
                $data[] = $nestedData;
            }

        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

   
    public function create()
    {
        $title = 'Create';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        $store_names = $this->getStoreNames();
        $customer_names = $this->getCustomerNames();
        $service_names = $this->getServiceNames();
        $category_names = $this->getCategoryNames();
        $subcategory_names = $this->getSubCategoryNames();

        return view('admin.promoCodes.create',compact('title','breadcum','model','module','store_names','customer_names','service_names','category_names','subcategory_names'));
    }

  
    public function store(Request $request)
    {
        try{
             if (Input::isMethod('post')) {
                $validation = array(
                    'promo_code_title' => 'required|unique:promo_codes',
                    'promo_code_title_ar' => 'required|unique:promo_codes',
                    'description' => 'required',
                    'description_ar' => 'required',
                    'from_date' => 'required',
                    'to_date' => 'required',
                    'minimum_order_amount' => 'required',
                    'code_limit' => 'required',
                    'promo_code_amount' => 'required',
                    'linked_level' => 'required',
                    'linked_customer' => 'required',
                    'linked_store' => 'required',
                );

                $desc = explode(' ',$request->description);
                $validator = Validator::make(Input::all(), $validation);

                if(count($desc)>500){
                     $validator -> errors() -> add('description', 'Description cannot be more than 500 words.');
                     return redirect()->back()
                    -> withErrors($validator)
                    -> withInput(Input::all());
                }

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {
                   // echo "<pre/>"; print_r($request->all());die;
                    $row = new PromoCode();

                    $row->promo_code_title = strtoupper($request->promo_code_title);
                    $row->promo_code_title_ar = strtoupper($request->promo_code_title_ar);
                    $row->description = $request->description;
                    $row->description_ar = $request->description_ar;
                    $row->from_date = $request->from_date;
                    $row->to_date = $request->to_date;
                    $row->minimum_order_amount = $request->minimum_order_amount;
                    $row->code_limit = $request->code_limit;
                    $row->promo_code_type = $request->promo_code_type;
                    $row->promo_code_amount = $request->promo_code_amount;
                    $row->linked_store = $request->linked_store;
                    $row->linked_customer = $request->linked_customer;
                    $row->linked_level = $request->linked_level;
                    $row->cron_status = '1';
                    if($request->linked_customer){
                        $reminder_date_fifteen = date('Y-m-d', strtotime($request->from_date.' +15days'));
                        $reminder_date_three = date('Y-m-d', strtotime($request->to_date.' -3days'));
                       
                        if($reminder_date_fifteen > $reminder_date_three){
                            $row->reminder_date = $reminder_date_three;
                        } else {
                            $row->reminder_date = $reminder_date_fifteen;
                        }
                    }
                    //echo '<pre>'; print_r($row);die;
                    if($row->save()){

                        if($request->linked_store == '1'){

                            foreach($request->sp_id as $s_row){
                                $rows = new PromoCodeLinkedStore();
                                $rows->promo_code_id = $row->id;
                                $rows->sp_id = $s_row;
                                $rows->save(); 
                           
                            }
                        }

                         if($request->linked_customer == '1'){
                            foreach($request->user_id as $c_row){
                                $rows = new PromoCodeLinkedCustomer();
                                $rows->promo_code_id = $row->id;
                                $rows->user_id = $c_row;
                                $rows->save(); 
                            }
                        }

                        if($request->linked_level == '0'){
                            foreach($request->service_id as $sr_row){
                                $rows = new PromoCodeLinkedService();
                                $rows->promo_code_id = $row->id;
                                $rows->service_id = $sr_row;
                                $rows->save(); 
                            }
                        }

                        if($request->linked_level == '1'){
                            foreach($request->subcategory_id as $sub_row){
                                $rows = new PromoCodeLinkedSubcategory();
                                $rows->promo_code_id = $row->id;
                                $rows->subcategory_id = $sub_row;
                                $rows->save(); 
                            }
                        }

                        if($request->linked_level == '2'){
                            foreach($request->category_id as $cat_row){
                                $rows = new PromoCodeLinkedCategory();
                                $rows->promo_code_id = $row->id;
                                $rows->category_id = $cat_row;
                                $rows->save(); 
                            }
                        }

                        }


                        Session::flash('success', __('Promo Code Created successfully.'));
                        return redirect()->route('promocode.index');
                    }                    
                }
             }
        
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

  
    public function show($id)
    {
        $row = PromoCode::where('id',$id)->first();
        $title = "View";
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        return view('admin.promoCodes.view',compact('row','title','model','breadcum'));

    }

    
    public function StatusUpdate(Request $request)
    {
        $promo_id = $request->promo_id;
        $row  = PromoCode::whereId($promo_id)->first();
        $row->status =  $row->status=='1'?'0':'1';
        $row->save();
        $html = '';
        switch ($row->status) {
          case '1':
               $html =  '<a data-toggle="tooltip"  class="btn btn-success btn-xs" title="Active" onClick="changeStatus('.$promo_id.')" >Active</a>';
              break;
               case '0':
               $html =  '<a data-toggle="tooltip"  class="btn btn-danger btn-xs" title="Inactive" onClick="changeStatus('.$promo_id.')" >InActive</a>';
              break;
          
          default:
            
              break;
      }
      return $html;


    }

    public function edit($id)
    {
        $row = PromoCode::where('id',$id)->first();
        $row->promo_code_amount = round($row->promo_code_amount,2);
        
        // $row = PromoCode::where('id',$id)->with('getAssociatedServiceProviders')->with('getAssociatedCustomers')->with('getAssociatedSPromoSubcategories')->with('getAssociatedPromoServices')->with('getAssociatedPromoCategories')->first()->toArray();
        $title = 'Edit Promo Code';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        $selected_stores = PromoCodeLinkedStore::where('promo_code_id',$id)->pluck('sp_id')->toArray();
        $selected_customers = PromoCodeLinkedCustomer::where('promo_code_id',$id)->pluck('user_id')->toArray();

        $selected_services = PromoCodeLinkedService::where('promo_code_id',$id)->pluck('service_id')->toArray();

        $selected_category = PromoCodeLinkedCategory::where('promo_code_id',$id)->pluck('category_id')->toArray();

        $selected_subcategory = PromoCodeLinkedSubcategory::where('promo_code_id',$id)->pluck('subcategory_id')->toArray();

        $store_names = $this->getStoreNames();
        $customer_names = $this->getCustomerNames();
        $service_names = $this->getServiceNames();
        $category_names = $this->getCategoryNames();
        $subcategory_names = $this->getSubCategoryNames();

        return view('admin.promoCodes.edit',compact('row','title','breadcum','store_names','customer_names','service_names','category_names','subcategory_names','selected_customers','selected_stores','selected_services','selected_category','selected_subcategory'));
    }

  
    public function update(Request $request,$id)
    {
      
        try{
            $validation = array(
                'promo_code_title' => 'required|unique:promo_codes,promo_code_title,'.$id,
                'promo_code_title_ar' => 'required|unique:promo_codes,promo_code_title_ar,'.$id,
                'description' => 'required',
                'description_ar' => 'required',
                'from_date' => 'required',
                'to_date' => 'required',
                'minimum_order_amount' => 'required',
                'code_limit' => 'required',
                'promo_code_amount' => 'required',
                'linked_level' => 'required',
                'linked_customer' => 'required',
                'linked_store' => 'required',
            );

            $desc = explode(' ',$request->description);
            $validator = Validator::make(Input::all(), $validation);

            if(count($desc)>500){
                 $validator -> errors() -> add('description', 'Description cannot be more than 500 words.');
                 return redirect()->back()
                -> withErrors($validator)
                -> withInput(Input::all());
            }

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
            } 
            else {
              
                $row = PromoCode::where('id',$id)->first();

                
                $row->promo_code_title = strtoupper($request->promo_code_title);
                $row->promo_code_title_ar = $request->promo_code_title_ar;
                $row->description = $request->description;
                $row->description_ar = $request->description_ar;
                $row->from_date = $request->from_date;
                $row->to_date = $request->to_date;
                $row->minimum_order_amount = $request->minimum_order_amount;
                $row->code_limit = $request->code_limit;
                $row->promo_code_type = $request->promo_code_type;
                $row->promo_code_amount = $request->promo_code_amount;
                $row->linked_store = $request->linked_store;
                $row->linked_customer = $request->linked_customer;
                $row->linked_level = $request->linked_level;
                
                if($row->save()){

                    if($request->linked_store == 1){
                        PromoCodeLinkedStore::where('promo_code_id',$row->id)->delete();
                        foreach($request->sp_id as $s_row){
                            $rows = new PromoCodeLinkedStore();
                            $rows->promo_code_id = $row->id;
                            $rows->sp_id = $s_row;
                            $rows->save(); 
                       
                        }
                    }
                    $new_arr = array();
                    $already = PromoCodeLinkedCustomer::where('promo_code_id',$row->id)->pluck('user_id')->toArray();
                    if(count($already)>0){
                        // $new_arr = array_merge(array_diff($already, $request->user_id), array_diff($request->user_id, $already));

                        $new_arr =  array_diff($request->user_id, $already);
                    }
                    

                     if($request->linked_customer == 1){
                        PromoCodeLinkedCustomer::where('promo_code_id',$row->id)->delete();
                        foreach($request->user_id as $c_row){
                            $rows = new PromoCodeLinkedCustomer();
                            $rows->promo_code_id = $row->id;
                            $rows->user_id = $c_row;
                            $rows->save(); 
                        }
                    }

                    if($request->linked_level == 0){
                         PromoCodeLinkedService::where('promo_code_id',$row->id)->delete();

                        foreach($request->service_id as $sr_row){
                            $rows = new PromoCodeLinkedService();
                            $rows->promo_code_id = $row->id;
                            $rows->service_id = $sr_row;
                            $rows->save(); 
                        }
                    }

                    if($request->linked_level == 1){
                        PromoCodeLinkedSubcategory::where('promo_code_id',$row->id)->delete();

                        foreach($request->subcategory_id as $sub_row){
                            $rows = new PromoCodeLinkedSubcategory();
                            $rows->promo_code_id = $row->id;
                            $rows->subcategory_id = $sub_row;
                            $rows->save(); 
                        }
                    }

                    if($request->linked_level == 2){
                        PromoCodeLinkedCategory::where('promo_code_id',$row->id)->delete();

                        foreach($request->category_id as $cat_row){
                            $rows = new PromoCodeLinkedCategory();
                            $rows->promo_code_id = $row->id;
                            $rows->category_id = $cat_row;
                            $rows->save(); 
                        }
                    }

                    //send notification to selected customers
                    if($request->linked_customer == '1' && count($new_arr)>0){


                        $dis_label = '';
                        if($row->promo_code_type == '0'){
                            $dis_label = 'flat';
                        }else{
                            $dis_label = '%';
                        }
                        foreach($new_arr as $c_row){

                            $u_info = User::where('id',$c_row)->select('name','notification_alert_status','email','id')->first();
                            if($u_info->notification_alert_status == '1'){
                                // $message = 'Thanks for your patience '.$u_info->name.', the discounted vouchers are ready for use. You have successfully attained '.$row->code_limit.' vouchers each with '.round($row->promo_code_amount,2).' '.$dis_label.' OFF. Start shopping, apply promo code and save.';

                                $message_main = __('messages.Vouchers Credited',['name' => $u_info->name,'limit'=>$row->code_limit,'amt'=>round($row->promo_code_amount,2),'label'=>$dis_label]);


                                    $message_title_main = __('messages.Vouchers Credited!');

                                    app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

                                    $message_other = __('messages.Vouchers Credited',['name' => $u_info->name,'limit'=>$row->code_limit,'amt'=>round($row->promo_code_amount,2),'label'=>$dis_label]);
                                    
                                    $message_title_other = __('messages.Vouchers Credited!');

                            
                                    Notification::saveNotification($u_info->id,'',$row->id,'VOUCHER_CREDITED',$message_main,$message_other,$message_title_main,$message_title_other,'customer');

                                    app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');
                                
                                }

                                if($u_info->email != ''){

                                    $promo_created_date = strtotime($row->from_date); // or your date as well
                                    $expiry_date = strtotime($row->to_date);
                                    $datediff = $expiry_date - $promo_created_date ;

                                    $total_days = round($datediff / (60 * 60 * 24));

                                    $data['templete'] = "redeem_discount_voucher_mail";
                                    $data['customer_name'] = $u_info->name;
                                    $data['email'] = $u_info->email;
                                    $data['discount_label'] = $dis_label;
                                    $data['discount'] = round($row->promo_code_amount,2);
                                    $data['code_limit'] = $row->code_limit;
                                    $data['total_days'] = $total_days;
                                    $data['subject'] = "Redeem your discount vouchers";
                                    send($data);
                                }
                            }
                        }



                    Session::flash('success', __('Promo Code Updated successfully.'));
                    return redirect()->route('promocode.index');
                }                    
            }
             
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

  
    public function destroy($id)
    {
        $row = PromoCode::where('id', $id)->first();
        if ($row) {
            $row->delete();
             Session::flash('success', __('Promo Code deleted successfully.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('Invalid request.'));
            return redirect()->back();
        }
    }
    
    public function getCategoryName()
    {
        $cat_data = Category::where('category_name','!=','bridal')->where('parent_id','0')->get();
        $cat_arr = array();

        foreach($cat_data as $data)
        {
            $cat_arr[$data['id']] = $data['category_name']; 
        }
         
        return $cat_arr;
    }

    public function getStoreNames()
    {
        $spuser_data = SpUser::select('id','store_name')->get();
        $spuser_arr = array();

        foreach($spuser_data as $data)
        {
            $spuser_arr[$data['id']] = $data['store_name']; 
        }
         
        return $spuser_arr;
    }

    public function getCustomerNames()
    {
        $user_data  = User::select('id','name','mobile_no')->get();
        $user_arr = array();

        foreach($user_data as $data)
        {
            if($data['mobile_no']==null && $data['mobile_no']==''){
                $user_arr[$data['id']] = 'Customer_'.$data['id']; 
            }else{
                $user_arr[$data['id']] = $data['name'].' ('.$data['mobile_no'].')'; 
            }
            
        }
         
        return $user_arr;
    }

    public function getServiceNames()
    {
        $sr_data  = Service::select('id','name')->get();
        $sr_arr = array();

        foreach($sr_data as $data)
        {
            $sr_arr[$data['id']] = $data['name']; 
        }
         
        return $sr_arr;
    }

    public function getCategoryNames()
    {
        $rows = Category::where('category_name','!=','bridal')->where('parent_id','0')->get();
        $cat_arr = array();

        foreach($rows as $data)
        {
            $cat_arr[$data['id']] = $data['category_name']; 
        }
         
        return $cat_arr;
    }

    public function getSubCategoryNames()
    {
        $rows = Category::where('parent_id','!=','0')->get();
        $subcat_arr = array();

        foreach($rows as $data)
        {
            $subcat_arr[$data['id']] = $data['category_name']; 
        }
         
        return $subcat_arr;
    }

    public function promo_log($id){
        $title = $this->title;
        $module = $this->module;
        $model = $this->model;
        $breadcum = ['Promo Codes'=>route($model.'.index'),'Promo Code Uses' =>''];
        $row = Booking::leftJoin('users', function($join) {
              $join->on('bookings.user_id', '=', 'users.id');
            })
            ->leftJoin('sp_users', function($join) {
              $join->on('bookings.sp_id', '=', 'sp_users.id');
            })            
            ->select('bookings.*', 'users.name', 'sp_users.store_name as sp_name')
            ->where('bookings.promo_code_id',$id)
            //->where('bookings.status','5')
            ->orderBy('bookings.id','desc')->get();

        
        $data = array();
        if (!empty($row)) {
            foreach($row as $val){
                $row_data = array();
                $row_data['id'] = $val->id;
                $row_data['booking_unique_id'] = $val->booking_unique_id;
                $row_data['name'] = $val->name;
                $row_data['sp_name'] = $val->sp_name;
                $row_data['created_at'] = date('Y-m-d', strtotime($val->created_at));
                $data[] = $row_data;
            }
           
        }
        
        return view('admin.promoCodes.promoLog',compact('title','model','module','breadcum','data'));
    }
}
