<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;   

use App\Model\FitnessType;
use App\Model\FitnessService;
use datatables;
use App;
use Session;

class FitnessTypeController extends Controller
{
    protected $title;
    protected $model;
    protected $module;

    public function __construct()
    {
        $this->title = 'Fitness Type';
        $this->model = 'fitness-type';
        $this->module = 'fitness-type';
    } 

    public function index()
    {
        $title = 'Fitness Type';
        $module = $this->module;
        $model = $this->model;
        $breadcum = [$title=>'Fitness Types'];        
        return view('admin.fitness_type.index',compact('title','model','module','breadcum'));
    }

    public function getData(Request $request)
    {
        
        $columns = ['name','title','created'];
        $totalData = FitnessService::count();
        
        
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        $row = FitnessService::select("fitness_services.*");
        
        if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = $row->Where('name', 'LIKE', "%{$search}%");
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('fitness_services.id', 'ASC')
                ->get();
        
        $data = array();
        if (!empty($row)) {
            foreach ($row as $key => $rows) {
                $nestedData['title'] = $rows->title;
                $nestedData['name'] = $rows->getAssociatedFitnessType->name;
                $nestedData['created'] = date('Y-m-d h:i A', strtotime($rows->created_at));
                $nestedData['action'] =  getButtons([
                            ['key'=>'edit','link'=>route('fitness-type.edit',$rows->id)],
                            ['key'=>'delete_assoc','link'=>route('fitness-type.destroy',$rows->id), 'asso'=>''],
                        ]);
                
                $data[] = $nestedData;
            }

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }
  
    public function create()
    {
        $action = "create";
        $title = 'Create Fitness Type';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        $typesArr = FitnessType::orderby('name', 'asc')->pluck('name', 'id')->toArray();
        return view('admin.fitness_type.create',compact('action','title','breadcum','model','typesArr'));
    }

    
    public function store(Request $request)
    {
        try{
             if (Input::isMethod('post')) {
                $validation = array(
                    'fitness_type_id' => 'required',
                    'title'=>'required',
                );
                $validator = Validator::make(Input::all(), $validation);

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {
                    $row = new FitnessService();
                    $row['fitness_type_id'] = $request->fitness_type_id;
                    $row['title'] = $request->title;
                    if($row->save())
                    {
                        Session::flash('success', __('Fitness Type added successfully.'));
                        return redirect()->route('fitness-type.index');
                    }                    
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

     
    public function edit($id)
    {
        $rows = FitnessService::where('id',$id)->first();
        $title = 'Edit Fitness Type';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        $typesArr = FitnessType::orderby('name', 'asc')->pluck('name', 'id')->toArray();
        return view('admin.fitness_type.edit',compact('rows','title','breadcum','typesArr'));
    }

   
    public function update(Request $request, $id)
    {
        try{
             if (Input::isMethod('PATCH')) {
                $validation = array(
                    'fitness_type_id' => 'required',
                    'title'=>'required',
                );

                $validator = Validator::make(Input::all(), $validation);

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {
                    $row = FitnessService::where('id',$id)->first();
                    $row['fitness_type_id'] = $request->fitness_type_id;
                    $row['title'] = $request->title;
                    if($row->save())
                    {
                        Session::flash('success', __('Fitness Type updated successfully.'));
                        return redirect()->route('fitness-type.index');
                    }
                    
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

  
    public function destroy($id)
    {
        $row = FitnessService::where('id', $id)->first();
        if ($row) {
            $row->delete();
             Session::flash('success', __('Fitness Type deleted successfully.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('Invalid request.'));
            return redirect()->back();
        }
    }
}


     