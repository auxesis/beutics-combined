<?php
namespace App\Http\Controllers\Admin;
use datatables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use DB;
use Illuminate\Support\Facades\Input;   
use Illuminate\Support\Facades\Hash;
use Session;
use App\Model\Admin;


class AdminController extends Controller
{
    protected $title;

    public function __construct()
    {
        $this->title = 'Admin';
    } 

    public function login()
    {
    	$title = 'Login';
    	return view('admin.admin.login',compact('title'));
    }
    public function checklogin(Request $request)
    {
    	 try {
                $validator = Validator::make($request->all(), [
                            'username' => 'required',
                            'password' => 'required|max:50|min:8',
                ]);

                if ($validator->fails()) 
                {
                    return redirect()->back()->withInput()->withErrors($validator->errors());
                   
                }
                else 
                {
                   $username = $request->username;
                   $password = bcrypt($request->password);	
                
				   $user = Admin::where('email',$username)->first();
                   
                    if($user && Hash::check($request->password, $user->password))
                   {
                        Session::put('AdminLoggedIn', ['user_id'=>$user->id,'userData'=> $user]);
                        Session::save();
                        return redirect()->route('admin.dashboard');
                   }
                   else
                   {
                        //Session::flash('invalid', 'Invalid username or password');
                        return redirect()->back()->with('message','Invalid username or password');
                   }
                }
            } 
            catch (\Exception $e) 
            {
                $msg = $e->getMessage();
                Session::flash('invalid',$msg);
                return redirect()->back()->withInput();
            }
    }

    public function logout()
    {
        Session::forget('AdminLoggedIn');
        return redirect()->route('admin.login');
    }

    public function changeAdminPassword()
    {
        $title = 'Password Setting';
        // $model = $this->model;
        // $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        return view('admin.admin.changeAdminPassword',compact('title'));
    }

    public function updateAdminPassword()
    {
        $validation = array(
            'old_password' => 'required',
            'new_password' => 'required|min:8',
            'confirm_password' => 'required|same:new_password',
        );
        $validator = Validator::make(Input::all(), $validation);
        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator->errors());
        }
        else
        {
            $user_data = loginData();//helper function
            $user =  Admin::where('id',$user_data['id'])->first();
           
            if (Hash::check(Input::get('old_password'), $user->password))
            {
                $user->password = Hash::make(Input::get('new_password'));
                $user->save();
                Session::flash('success', __('Password updated successfully.'));
                return redirect()->route('admin.dashboard');
            } 
            else
            {
                Session::flash('warning', __('Wrong old password'));
                return redirect()->back();
            }
        }
    }

    
}
