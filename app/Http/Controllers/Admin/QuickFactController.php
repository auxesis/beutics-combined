<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;   

use App\Model\QuickFact;
use App\Model\QuickfactBannerImage;
use App\Model\Category;
use App\Model\Service;
use datatables;
use App;
use Session;    

class QuickFactController extends Controller
{
    
    protected $title;
    protected $model;
    protected $module;

    public function __construct()
    {
        $this->title = 'Quick Facts';
        $this->model = 'quickFact';
        $this->module = 'quickfact';
    } 

    public function index()
    {
        $title = $this->title;
        $module = $this->module;
        $model = $this->model;
        $breadcum = [$title=>route($model.'.index')];
        return view('admin.quickFact.index',compact('title','model','module','breadcum'));
    }

    public function getData(Request $request)
    {

        $columns = ['title','title_ar','category_id','subcategory_id','service_id','action'];
        $totalData = QuickFact::count();
        
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        $row = new QuickFact();
        // $totalFiltered = User::count();

        if (!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = $row->where(function($query) use ($search) {
                $query->where('title', 'LIKE', "%{$search}%");
            });
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('id', 'DESC')
                ->get();
        // print_r($users);die;
        $data = array();
        if (!empty($row)) {
            foreach ($row as $key => $rows) {
                $nestedData['title'] = $rows->title;
                $nestedData['title_ar'] = $rows->title_ar;
                $nestedData['category_id'] = $rows->getAssociatedcategoryName->category_name;
                $nestedData['subcategory_id'] = $rows->getAssociatedSubcategoryName->category_name;
                $nestedData['service_id'] = $rows->getAssociatedServiceName->name;
                $nestedData['action'] =  getButtons([
                                ['key'=>'edit','link'=>route('quickFact.edit',$rows->id)],
                                ['key'=>'delete','link'=>route('quickFact.destroy',$rows->id)],
                            ]);
                
                $data[] = $nestedData;
            }

        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

   
    public function create()
    {
        $action = "create";
        $title = 'Create Facts';
        $model = $this->model;
        $categories = $this->getCategoryName();
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];

        return view('admin.quickFact.create',compact('action','title','breadcum','model','module','categories'));
    }

  
    public function store(Request $request)
    {
        try{
             if (Input::isMethod('post')) {
                $validation = array(
                    'category_id' => 'required',
                    'subcategory_id' => 'required',
                    'service_id' => 'required',
                    'title' => 'required|max:50',
                    'title_ar' => 'required|max:50',
                    // 'description' => 'required|max:500',
                    'image' => 'required|mimes:jpeg,jpg,png,gif',
                );

                $desc = explode(' ',$request->description);
                $validator = Validator::make(Input::all(), $validation);

                if(count($desc)>500){
                     $validator -> errors() -> add('description', 'Description cannot be more than 500 words.');
                     return redirect()->back()
                    -> withErrors($validator)
                    -> withInput(Input::all());
                }

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {

                    $row = new QuickFact();
                    $row->category_id = $request->category_id;
                    $row->subcategory_id = $request->subcategory_id;
                    $row->service_id = $request->service_id;
                    $row->subservice_id = $request->subservice_id;
                    $row->title = $request->title;
                    $row->title_ar = $request->title_ar;
                    $row->description = $request->description;
                    $row->description_ar = $request->description_ar;

                    if ($request->hasFile('image') && $request->file('image'))
                    {
                        $file = $request->file('image');

                        $name = time().'.'.$file->getClientOriginalExtension();
                        $destinationPath = public_path('/sp_uploads/quick_facts/');
                        $img = $file->move($destinationPath, $name);

                        $row->image = $name;
                    }

                    $row->save();
                    if($row->save()){
                        if($request->banner_path){

                            $count = count($request->banner_path);
                        
                            foreach($request->banner_path as $key => $val){
                                if ($request->banner_path[$key])
                                {
                                    $img = $request->banner_path[$key];
                                    $file = $img;
                                    $name = time().str_random(2).'.'.$file->getClientOriginalExtension();
                                    $destinationPath = public_path('sp_uploads/quick_facts/');
                                    $img = $file->move($destinationPath, $name);

                                    $rows = new QuickfactBannerImage();

                                    $rows->quick_fact_id = $row->id;
                                    $rows->banner_path = $name;
                                    
                                    $rows->save();
                                }
                            }
                        } 
                    }
                    
                    Session::flash('success', __('Quick facts added successfully.'));
                    return redirect()->route('quickFact.index');
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

  
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        $row = QuickFact::where('id',$id)->first();
        $title = 'Edit Quick Fact';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        $categories = $this->getCategoryName();

        $subcategories = Category::where('id',$row->subcategory_id)->get();
        $subcat_arr = array();
        foreach($subcategories as $sub_rows){
            $subcat_arr[$sub_rows['id']]  = $sub_rows['category_name'];
        }

        $services = Service::where('id',$row->service_id)->where('parent_service_id','0')->get();
        $service_arr = $sub_service_arr = array();
        foreach($services as $service_rows){
            $service_arr[$service_rows['id']]  = $service_rows['name'];
        }

        $sub_services = Service::where('id',$row->subservice_id)->get();
        foreach($sub_services as $service_rows){
            $sub_service_arr[$service_rows['id']]  = $service_rows['name'];
        }

        $banner_images = QuickfactBannerImage::where('quick_fact_id',$row->id)->get();
        return view('admin.quickFact.edit',compact('row','title','breadcum','categories','banner_images','subcat_arr','service_arr','sub_service_arr'));
    }

  
    public function update(Request $request,$id)
    {
        try{
             
            $validation = array(
                'category_id' => 'required',
                'subcategory_id' => 'required',
                'service_id' => 'required',
                'title' => 'required|max:50',
                'title_ar' => 'required|max:50',
                // 'description' => 'required|max:500',
                // 'image' => 'required|mimes:jpeg,jpg,png,gif',
            );
        
            $desc = explode(' ',$request->description);
            $validator = Validator::make(Input::all(), $validation);

            if(count($desc)>500){
                 $validator -> errors() -> add('description', 'Description cannot be more than 500 words.');
                 return redirect()->back()
                -> withErrors($validator)
                -> withInput(Input::all());
            }
           
            if ($validator->fails()) {

                return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
            } 
            else {
                    $row = QuickFact::where('id',$id)->first();
                    $row->category_id = $request->category_id;
                    $row->subcategory_id = $request->subcategory_id;
                    $row->service_id = $request->service_id;
                    $row->subservice_id = $request->subservice_id;
                    $row->title = $request->title;
                    $row->title_ar = $request->title_ar;
                    $row->description = $request->description;
                    $row->description_ar = $request->description_ar;

                    if ($request->hasFile('image') && $request->file('image'))
                    {
                        $file = $request->file('image');

                        $name = time().'.'.$file->getClientOriginalExtension();
                        $destinationPath = public_path('/sp_uploads/quick_facts/');
                        $img = $file->move($destinationPath, $name);

                        $row->image = $name;
                    }

                    $row->save();
                    if($row->save()){
                        if($request->banner_path){

                            $count = count($request->banner_path);
                        
                            foreach($request->banner_path as $key => $val){
                                if ($request->banner_path[$key])
                                {
                                    $img = $request->banner_path[$key];
                                    $file = $img;
                                    $name = time().str_random(2).'.'.$file->getClientOriginalExtension();
                                    $destinationPath = public_path('sp_uploads/quick_facts/');
                                    $img = $file->move($destinationPath, $name);

                                    $rows = new QuickfactBannerImage();

                                    $rows->quick_fact_id = $row->id;
                                    $rows->banner_path = $name;
                                    
                                    $rows->save();
                                }
                            }
                        } 
                    }
                    
                    Session::flash('success', __('Quick facts Updated successfully.'));
                    return redirect()->route('quickFact.index');
            }
             
        }
        catch( \Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

  
    public function destroy($id)
    {
        $row = QuickFact::where('id', $id)->first();
        if ($row) {
            $row->delete();
             Session::flash('success', __('Quick Fact deleted successfully.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('Invalid request.'));
            return redirect()->back();
        }
    }

    public function getCategoryName()
    {
        $cat_data = Category::where('category_name','!=','bridal')->where('parent_id','0')->get();
        $cat_arr = array();

        foreach($cat_data as $data)
        {
            $cat_arr[$data['id']] = $data['category_name']; 
        }
         
        return $cat_arr;
    }

    public function ajaxSubcategory(Request $request)
    {
        $cat_id = $request->cat_id;
        $cat_data = Category::where('parent_id',$cat_id)->get();
        $cat_arr = array();

        foreach($cat_data as $data)
        {
            $cat_arr[$data['id']] = $data['category_name']; 
        }
         
        return view('admin.quickFact.ajaxSubcategory',compact('cat_arr'));

    }

    public function ajaxGetSubcatServices(Request $request)
    {
        $subcat_id = $request->subcat_id;
        $service_data = Service::where('subcat_id',$subcat_id)->where('parent_service_id','0')->get();
        $service_arr = array();

        foreach($service_data as $data)
        {
            $service_arr[$data['id']] = $data['name']; 
        }

        return view('admin.quickFact.ajaxGetSubcatServices',compact('service_arr'));

    }

    public function ajaxGetSubcatSubServices(Request $request)
    {
        $service_id = $request->service_id;
        $service_data = Service::where('parent_service_id',$service_id)->get();
        $sub_service_arr = array();

        foreach($service_data as $data)
        {
            $sub_service_arr[$data['id']] = $data['name']; 
        }

        return view('admin.quickFact.ajaxGetSubcatSubServices',compact('sub_service_arr'));

    }

    public function deleteBannerImage(Request $request)
    {
        $id = $request->banner_id;
        $row  = QuickfactBannerImage::whereId($id)->first();

        if($row){
           unlink('public/sp_uploads/quick_facts/'.$row->banner_path);
           $row->delete();
           return 'success';
        }
    }
}
