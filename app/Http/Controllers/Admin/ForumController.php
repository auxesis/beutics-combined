<?php

namespace App\Http\Controllers\Admin;
use datatables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use DB;
use Illuminate\Support\Facades\Input;   
use Illuminate\Support\Facades\Hash;
use Session;
use App\Model\Forum;
use App\Model\ForumCategory;
use App\Model\ForumBannerImage;
use App\Model\ForumComment;
use App\Model\ForumReplyBannerImage;
use App\Model\ForumCommentLikeDislike;
use App\Model\User;


class ForumController extends Controller
{
    protected $title;
    protected $model;
    protected $module;

    public function __construct()
    {
        $this->title = 'Forums';
        $this->model = 'forum';
        $this->module = 'forum';
    } 

    public function index()
    {

        $title = $this->title;
        $breadcum = [$title =>''];

        return view('admin.forums.queries',compact('title','model','breadcum')); 
    }

    public function getData(Request $request)
    {

        
        $columns = ['subject','message','date','username','status'];
        
        $totalData = Forum::select('forums.id','forums.message','forums.subject','forums.user_id','users.name as username','forums.created_at as date')
                ->leftJoin('users', function($join) {
                  $join->on('forums.user_id', '=', 'users.id');
                })
                //->groupBy('forums.id')
                ->count();
               

        //dd($totalData);
               
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        $row = Forum::select('forums.id','forums.message','forums.subject','forums.user_id','users.name as username','forums.created_at as date','forums.status')
                ->leftJoin('users', function($join) {
                  $join->on('forums.user_id', '=', 'users.id');
                });
        
      
        if (!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = $row->where(function($query) use ($search) {
                $query
                //where('forums.message', 'LIKE', "%{$search}%")
                        ->where('forums.subject', 'LIKE', "%{$search}%")
                        ->orWhere('users.name', 'LIKE', "%{$search}%");
                        
            });
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('id', 'DESC')
                ->get();
        
        $data = array();
        if (!empty($row)) {

            foreach ($row as $key => $rows) {

                $nestedData['subject'] = $rows->subject;
                $nestedData['username'] = $rows->username;
                $nestedData['date'] = $rows->date;
                $nestedData['status'] = getStatusText($rows->status);
                $nestedData['action'] = '';
                if($rows->status == 0)
                {
                $nestedData['action'] .= '<form method="post" action="'.route("forum.queries.approve-status", [$rows->id]).'" accept-charset="UTF-8" style="display:inline" onsubmit="return deleteRow(this)">'.csrf_field().' '.method_field("PUT").'<span><button data-toggle="tooltip" title="Approve" type="submit" class="btn btn-success btn-xs approve-action"><i class="fa fa-check"></i></button></span></form>';
                $nestedData['action'] .= '<form method="post" action="'.route("forum.queries.reject-status", [$rows->id]).'" accept-charset="UTF-8" style="display:inline" onsubmit="return deleteRow(this)">'.csrf_field().' '.method_field("PUT").'<span><button data-toggle="tooltip" title="Reject" type="submit" class="btn btn-warning btn-xs rejected-action"><i class="fa fa-times"></i></button></span></form>'; 
                }     
                $nestedData['action'] .=  getButtons([
                                ['key'=>'view','link'=>route('forum.queries.view',[$rows->id])],
                                ['key'=>'delete','link'=>route('forum.queries.delete',[$rows->id])]
                            ]);
                
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }


    public function viewQuery(Request $request ,$id){
        $title = 'View';
        $breadcum = [$this->title=>route($this->model.'.index'),$title =>''];

        $forum = Forum::where('id',$id)->first();
        
        $banners = ForumBannerImage::where('forum_id',$id)->get();
        
        $responses = ForumComment::where('forum_id',$id)->count();

        $user = User::select('name')->where('id',$forum->user_id)->first();
        $username = $user->name;

        $posted_on = $forum->created_at;

        $categories =  ForumCategory::leftJoin('categories', function($join) {
                  $join->on('forum_categories.category_id', '=', 'categories.id');
                })->where('forum_id',$id)->select('categories.category_name')->get()->toArray();
       // echo '<pre>'; print_r($categories);
        
        $responses_list = ForumComment::select('sp_users.name as sp_username','users.name as username','forum_comments.message','users.image as user_image','sp_users.profile_image as sp_image','sp_users.store_name','sp_users.store_type', 'forum_comments.created_at as date','forum_comments.id')->where('forum_id',$id)
        ->with('getAssociatedCommentBannerImages')
        ->with('getAssociatedLikeDislike')
        ->leftJoin('users', function($join) {
                  $join->on('forum_comments.user_id', '=', 'users.id');
                })
        ->leftJoin('sp_users', function($join) {
                  $join->on('forum_comments.sp_id', '=', 'sp_users.id');
                })
        ->get();

        $response_check = ForumComment::where('forum_id',$id)->count();
        
        return view('admin.forums.view',compact('title','model','breadcum','forum','banners','username','responses','responses_list','response_check','posted_on', 'categories'));
    }

    public function deleteForumComment($id)
    {
        $comment = ForumComment::where('id', $id)->first();
        if ($comment) {
            $comment->delete();
             Session::flash('success', __('Comment deleted successfully.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('Invalid request.'));
            return redirect()->back();
        }
    }

    public function deleteForumQueries($id)
    {
        $forum = Forum::where('id', $id)->first();
      
        if ($forum) {
            $forum->delete();
             Session::flash('success', __('Forum deleted successfully.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('Invalid request.'));
            return redirect()->back();
        }
    }

    public function approveForumStatus($id)
    {
        $forum = Forum::where('id', $id)->first();
      
        if ($forum) {
            $forum->status = "1";
            $forum->save();
             Session::flash('success', __('Forum Status successfully Updated.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('Invalid request.'));
            return redirect()->back();
        }
    }

    public function rejectForumStatus($id)
    {
        $forum = Forum::where('id', $id)->first();
      
        if ($forum) {
            $forum->status = "2";
            $forum->save();
             Session::flash('success', __('Forum Status successfully Updated.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('Invalid request.'));
            return redirect()->back();
        }
    }
}
