<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;   

use App\Model\Faq;
use App\Model\FaqCategory;
use datatables;
use App;
use Session;

class FaqController extends Controller
{
    protected $title;
    protected $model;
    protected $module;

    public function __construct()
    {
        $this->title = 'Faq';
        $this->model = 'faq';
        $this->module = 'faq';
    } 

    public function index()
    {
        $title = 'FAQ';
        $module = $this->module;
        $model = $this->model;
        $breadcum = [$title=>'FAQ'];
        $categories  = FaqCategory::orderby('name', 'asc')->pluck('name', 'id')->toArray();
        return view('admin.faq.index',compact('title','model','module','breadcum', 'categories'));
    }

    public function getData(Request $request)
    {
   
        $columns = ['name','question','question_ar'];
        
        $totalData = Faq::count();
                       
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');      
     
        $row = Faq::select("faqs.*");
      

        if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = Faq::leftJoin('faq_categories', function($join) {
                  $join->on('faqs.faq_category_id', '=', 'faq_categories.id');
                }) 
            ->where(function($query) use ($search) {
                $query->Where('faq_categories.name', 'LIKE', "%{$search}%")
                ->orWhere('faqs.question', 'LIKE', "%{$search}%"); 
            })
          ->select('faqs.*', 'faq_categories.name');
        } else if (!empty($request->input('category_filter'))) {
            $faq_category_id = $request->input('category_filter');
            $row = Faq::leftJoin('faq_categories', function($join) {
                  $join->on('faqs.faq_category_id', '=', 'faq_categories.id');
                }) 
            ->where(function($query) use ($faq_category_id) {
                $query->orWhere('faqs.faq_category_id', $faq_category_id);
            })
          ->select('faqs.*', 'faq_categories.name');      
        } else {
            $row = Faq::leftJoin('faq_categories', function($join) {
                  $join->on('faqs.faq_category_id', '=', 'faq_categories.id');
                }) 
          ->select('faqs.*', 'faq_categories.name');
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('faqs.id', 'desc')
                ->get();
       
        $data = array();
        if (!empty($row)) {
            foreach ($row as $key => $rows) {
                $nestedData['id'] = $rows->id;
                $nestedData['name'] = $rows->name;
                $nestedData['question'] = $rows->question;
                $nestedData['question_ar'] = $rows->question_ar;
                $nestedData['action'] =  getButtons([
                                ['key'=>'edit','link'=>route('faq.edit',$rows->id)],
                                ['key'=>'delete','link'=>route('faq.destroy',$rows->id)],
                                ['key'=>'view','link'=>route('faq.show',$rows->id)],
                            ]);
                
                $data[] = $nestedData;
            }

        }
        // echo '<pre>'; print_r($data);
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }
  
    public function create()
    {
        $action = "create";
        $title = 'Create FAQ';
        $model = $this->model;
        $faq_categories = FaqCategory::select('id','name')->get();
        $category_list = array();
        foreach($faq_categories as $row){
            $category_list[$row['id']] = $row['name'];
        }
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        return view('admin.faq.create',compact('action','title','breadcum','model','category_list'));
    }

    
    public function store(Request $request)
    {
        try{ //dd($request);
             if (Input::isMethod('post')) { 
                $validation = array(
                    'faq_category_id'=>'required',
                    'question'=>'required',
                    'question_ar'=>'required',
                    'answer'=>'required',
                    'answer_ar'=>'required',
                );

                $validator = Validator::make(Input::all(), $validation);

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {
                    $row = new Faq();
                    $row['faq_category_id'] = $request->faq_category_id;
                    $row['question'] = $request->question;
                    $row['question_ar'] = $request->question_ar;
                    $row['answer'] = $request->answer;
                    $row['answer_ar'] = $request->answer_ar;
                    if($row->save())
                    {
                        Session::flash('success', __('FAQ added successfully.'));
                        return redirect()->route('faq.index');
                    }
                    
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }
   
    public function edit($id)
    {
        $rows = Faq::where('id',$id)->first();
        $title = 'Edit FAQ';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>'FAQ'];        
        $faq_categories = FaqCategory::select('id','name')->get();
        $category_list = array();
        foreach($faq_categories as $row){
            $category_list[$row['id']] = $row['name'];
        }

        return view('admin.faq.edit',compact('rows','title','breadcum','category_list'));
    }

   
    public function update(Request $request, $id)
    {
        try{
             if (Input::isMethod('PATCH')) {
                $validation = array(
                    'faq_category_id'=>'required',
                    'question'=>'required',
                    'question_ar'=>'required',
                    'answer'=>'required',
                    'answer_ar'=>'required',
                );

                $validator = Validator::make(Input::all(), $validation);

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {
                    $row = Faq::where('id',$id)->first();
                    $row['faq_category_id'] = $request->faq_category_id;
                    $row['question'] = $request->question;
                    $row['question_ar'] = $request->question_ar;
                    $row['answer'] = $request->answer;
                    $row['answer_ar'] = $request->answer_ar;
                    if($row->save())
                    {
                        Session::flash('success', __('FAQ updated successfully.'));
                        return redirect()->route('faq.index');
                    }
                    
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

    public function show($id)
    {
        $title = 'FAQ Details';
        $row = Faq::leftJoin('faq_categories', function($join) {
              $join->on('faqs.faq_category_id', '=', 'faq_categories.id');
            }) 
        ->select('faqs.*', 'faq_categories.name')
        ->where('faqs.id', $id)->first();
        //echo '<pre>'; print_r($row);die;
        $module = $this->module;
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>'FAQ Detail'];     
        return view('admin.faq.view',compact('title','row','module','breadcum'));
    }
  
    public function destroy($id)
    {
        $row = Faq::where('id', $id)->first();
        if ($row) {
            $row->delete();
             Session::flash('success', __('FAQ deleted successfully.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('Invalid request.'));
            return redirect()->back();
        }
    }
}
