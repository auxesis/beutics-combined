<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;   

use App\Model\TrailSession;
use App\Model\TrailSessionQuestion;
use App\Model\TrailSessionQuestionsFaqs;
use App\Model\User;
use datatables;
use App;
use Session;

class TrailSessionFaqController extends Controller
{
    protected $title;
    protected $model;
    protected $module;

    public function __construct()
    {
        $this->title = 'Trail Sessions Faq';
        $this->model = 'trail-session';
        $this->module = 'trail-session-faq';
    } 

    public function index()
    {
        $title = __('messages.Session Responded Faqs');
        $module = $this->module;
        $model = $this->model;
        $breadcum = [$title=>'Trail Sessions'];
        $trail_sessions = TrailSession::select('id','name')->get();
        $session_list = array();
        foreach($trail_sessions as $row){
            $session_list[$row['id']] = $row['name'];
        }
        return view('admin.trail_session_faq.index',compact('title','model','module','breadcum','session_list'));
    }

    public function getData(Request $request)
    {
        $columns = ['store_name','username','session_name','email','mobile_no'];
        
        $total = TrailSessionQuestionsFaqs::groupBy('user_id')->get();
        $totalData = $total->count();
       
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');      
     
        $row = TrailSessionQuestionsFaqs::select("trail_session_questions_faqs.*");
      

        if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = TrailSessionQuestionsFaqs::leftJoin('trail_session_questions', function($join) {
                  $join->on('trail_session_questions_faqs.question_id', '=', 'trail_session_questions.id');
                })
                ->leftJoin('sp_users', function($join) {
                  $join->on('trail_session_questions_faqs.sp_id', '=', 'sp_users.id');
                })
                ->leftJoin('users', function($join) {
                  $join->on('trail_session_questions_faqs.user_id', '=', 'users.id');
                })
                ->leftJoin('trail_sessions', function($join) {
                  $join->on('trail_session_questions.trail_session_id', '=', 'trail_sessions.id');
                }) 
            ->where(function($query) use ($search) {
                $query->Where('trail_sessions.name', 'LIKE', "%{$search}%")
                ->orWhere('trail_session_questions.question', 'LIKE', "%{$search}%")
                ->orWhere('sp_users.store_name', 'LIKE', "%{$search}%")
                ->orWhere('users.name', 'LIKE', "%{$search}%")
                ->orWhere('users.email', 'LIKE', "%{$search}%")
                ->orWhere('users.mobile_no', 'LIKE', "%{$search}%");
            })
          ->select('trail_session_questions_faqs.*', 'users.name as username', 'trail_sessions.name as session_name','trail_session_questions.question','users.email','users.mobile_no','sp_users.store_name','users.id as user_id','sp_users.id as sp_id','trail_sessions.id as session_id'); 
        } else if (!empty($request->input('session_filter'))) {
            $session_id = $request->input('session_filter');
            $row = TrailSessionQuestionsFaqs::leftJoin('trail_session_questions', function($join) {
                  $join->on('trail_session_questions_faqs.question_id', '=', 'trail_session_questions.id');
                })
                ->leftJoin('sp_users', function($join) {
                  $join->on('trail_session_questions_faqs.sp_id', '=', 'sp_users.id');
                })
                ->leftJoin('users', function($join) {
                  $join->on('trail_session_questions_faqs.user_id', '=', 'users.id');
                })
                ->leftJoin('trail_sessions', function($join) {
                  $join->on('trail_session_questions.trail_session_id', '=', 'trail_sessions.id');
                }) 
            ->where(function($query) use ($session_id) {
                $query->orWhere('trail_session_questions.trail_session_id', $session_id);
            })
          ->select('trail_session_questions_faqs.*', 'users.name as username', 'trail_sessions.name as session_name','trail_session_questions.question','users.email','users.mobile_no','sp_users.store_name','users.id as user_id','sp_users.id as sp_id','trail_sessions.id as session_id');      
        } else {
        $row = TrailSessionQuestionsFaqs::leftJoin('trail_session_questions', function($join) {
                  $join->on('trail_session_questions_faqs.question_id', '=', 'trail_session_questions.id');
                })
                ->leftJoin('sp_users', function($join) {
                  $join->on('trail_session_questions_faqs.sp_id', '=', 'sp_users.id');
                })
                ->leftJoin('users', function($join) {
                  $join->on('trail_session_questions_faqs.user_id', '=', 'users.id');
                })
                ->leftJoin('trail_sessions', function($join) {
                  $join->on('trail_session_questions.trail_session_id', '=', 'trail_sessions.id');
                }) 
          ->select('trail_session_questions_faqs.*', 'users.name as username', 'trail_sessions.name as session_name','trail_session_questions.question','users.email','users.mobile_no','sp_users.store_name','users.id as user_id','sp_users.id as sp_id','trail_sessions.id as session_id'); 
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->groupBy('trail_session_questions_faqs.user_id')->groupBy('trail_session_questions_faqs.sp_id')->groupBy('trail_session_questions.trail_session_id')->offset($start)
                ->limit($limit)
                ->orderBy('trail_session_questions_faqs.id', 'desc')
                ->get();
       
        $data = array();
        if (!empty($row)) {
            foreach ($row as $key => $rows) {
                $nestedData['store_name'] = $rows->store_name;
                $nestedData['session_name'] = $rows->session_name;
                $nestedData['username'] = ucwords($rows->username);
                $nestedData['email'] = $rows->email;
                $nestedData['mobile_no'] = $rows->mobile_no;
                $nestedData['action'] =  getButtons([
                                ['key'=>'view','link'=>route('trail-session-faq.view',['sp_id'=>$rows->sp_id,'user_id'=>$rows->user_id,'session_id'=>$rows->session_id])],
                                ['key'=>'delete','link'=>route('trail-session-faq.destroy',$rows->user_id)],
                            ]);
                $data[] = $nestedData;
            }

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalFiltered),
            "recordsFiltered" => intval($totalData),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function create()
    {
        
    }

    
    public function store(Request $request)
    {
       
    }
   
    public function edit($id)
    {
       
    }

   
    public function update(Request $request, $id)
    {
      
    }

    public function view(Request $request ,$sp_id, $user_id, $session_id)
    {
        $title = __('messages.Session Responded Faqs');
        $model = $this->model;
        $breadcum = [$this->title=>route('trail-session-faq.index'),$title =>'Session'];
        
        $user_data = User::where('id',$user_id)->first();
        $faq_responses = TrailSessionQuestionsFaqs::select('question','answer','trail_session_questions_faqs.created_at')
        ->leftJoin('trail_session_questions', function($join) {
                  $join->on('trail_session_questions_faqs.question_id', '=', 'trail_session_questions.id');
                })->where('user_id',$user_id)->where('sp_id',$sp_id)->where('trail_session_questions.trail_session_id',$session_id)->orderBy('trail_session_questions_faqs.created_at','desc')->get();
        return view('admin.trail_session_faq.view',compact('title','model','breadcum','faq_responses','user_data'));

    }

    public function destroy($id)
    {
        $rows = TrailSessionQuestionsFaqs::where('sp_id', $id)->first();//->pluck('question_id')->toArray();
        if ($rows) {
            //TrailSessionQuestion::whereIn('id',$rows)->delete();
            TrailSessionQuestionsFaqs::where('sp_id',$id)->delete();
            Session::flash('success', __('Faqs deleted successfully.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('Invalid request.'));
            return redirect()->back();
        }
    }
}
