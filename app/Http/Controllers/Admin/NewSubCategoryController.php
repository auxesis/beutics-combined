<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;   

use App\Model\Category;
use App\Model\Service;
use App\Model\PromoCodeLinkedSubcategory;
use App\Model\QuickFact;
use App\Model\CategoryType;
use datatables;
use App;
use Session;

class NewSubCategoryController extends Controller
{
    protected $title;
    protected $model;
    protected $module;

    public function __construct()
    {
        $this->title = 'Sub Categories';
        $this->model = 'subcategory';
        $this->module = 'new-subcategory';
    } 

    public function index()
    {
        $title = 'Sub Categories';
        $module = $this->module;
        $model = $this->model;
        $breadcum = [$title=>route($model.'.index')];
        return view('admin.subcategory.new.index',compact('title','model','module','breadcum'));
    }

    public function getData(Request $request)
    {
        $columns = ['category_name','category_name_ar','parent_id','created_at','action'];
        $totalData = Category::where('parent_id','!=','0')->count();
        
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        $row = Category::where('parent_id','!=','0');
        // $totalFiltered = User::count();

        if (!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = $row->where(function($query) use ($search) {
                $query->where('category_name', 'LIKE', "%{$search}%")
                        ->orWhere('created_at', 'LIKE', "%{$search}%");
            });
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        // print_r($users);die;
        $data = array();
        $category_arr = $this->getCategoryName();
        if (!empty($row)) {
            foreach ($row as $key => $rows) {

                $nestedData['category_name'] = $rows->category_name;
                $nestedData['category_name_ar'] = $rows->category_name_ar;
                $nestedData['parent_id'] = $category_arr[$rows->parent_id];
                $associatedTables = $this->checkAssociatedTables($rows->id);
                //$associatedTables = json_encode($associatedTables);
                $nestedData['created_at'] = date('d-M-y h:i:s',strtotime($rows->created_at));

                $nestedData['action'] =  getButtons([
                                ['key'=>'edit','link'=>route('new-subcategory.edit',$rows->id)],
                                ['key'=>'delete_assoc','link'=>route('new-subcategory.destroy',$rows->id), 'asso'=>$associatedTables],
                            ]);
                
                $data[] = $nestedData;
            }

        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function checkAssociatedTables($id)
    {
        $tables = '';
        $service_count = Service::where('subcat_id',$id)->count();
        $qf_count = QuickFact::where('subcategory_id',$id)->count();
        $promo_count = PromoCodeLinkedSubcategory::where('subcategory_id',$id)->count();

        if($service_count > 0){
            $tables = $tables.'Services__';
        }
        if($qf_count > 0){
            $tables = $tables.'QuickFacts__';
        }
        if($promo_count > 0){
            $tables = $tables.'Promocodes';
        }
        return $tables;
        //return explode(',',$tables);

    }

    public function create()
    {
        $action = "create";
        $title = 'Create Sub Category';
        $model = $this->model;
        $breadcum = [$this->title=>route('new-'.$model.'.index'),$title =>''];
        $categories = $this->getCategoryName();
        return view('admin.subcategory.new.create',compact('action','title','breadcum','model','module','categories'));
    }

   
    public function store(Request $request)
    {
        try{
             if (Input::isMethod('post')) {
                $validation = array(
                    'parent_id' => 'required',
                    'category_name' => 'required|max:50',
                    'category_name_ar' => 'required|max:50',
                    'icon' => 'required|mimes:png,jpeg,jpg',
                    // 'black_image' => 'required|mimes:jpeg,jpg,png',
                );

                $validator = Validator::make(Input::all(), $validation);

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {
                    $getType = Category::where('id',$request->parent_id)->select('type_id')->first();
                    $row = new Category();

                    $row->parent_id = $request->parent_id;
                    $row->type_id = $getType->type_id;
                    $row->category_name = $request->category_name;
                    $row->category_name_ar = $request->category_name_ar;
                    // if ($request->hasFile('coloured_image') && $request->file('coloured_image'))
                    // {
                    //     $file = $request->file('coloured_image');

                    //     $name = time().'._c'.$file->getClientOriginalExtension();
                    //     $destinationPath = public_path('/sp_uploads/sub_categories/');
                    //     $img = $file->move($destinationPath, $name);

                    //     $row->coloured_image = $name;
                    // } 

                    if ($request->hasFile('icon') && $request->file('icon'))
                    {
                        $file = $request->file('icon');

                        $name = time().'.'.$file->getClientOriginalExtension();
                        $destinationPath = public_path('/sp_uploads/sub_categories/');
                        $img = $file->move($destinationPath, $name);

                        $row->icon = $name;
                    } 

                    $row->save();
                    
                    Session::flash('success', __('Sub Category created successfully.'));
                    return redirect()->route('new-subcategory.index');
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

    
    public function show($id)
    {
        //
    }

 
    public function edit($id)
    {
        $row = Category::where('id',$id)->first();
        $title = 'Edit Sub Category';
        $model = $this->model;
        $breadcum = [$this->title=>route('new-'.$model.'.index'),$title =>''];
        $categories = $this->getCategoryName();
        return view('admin.subcategory.new.edit',compact('row','title','breadcum','categories'));
    }

  
    public function update(Request $request, $id)
    {
        try{
            $validation = array(
                'parent_id' => 'required',
                'category_name' => 'required|max:50',
                'category_name_ar' => 'required|max:50',
                // 'coloured_image' => 'mimes:jpeg,jpg,png',
                'icon' => 'mimes:png,jpeg,jpg',
            );

            $validator = Validator::make(Input::all(), $validation);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
            } 
            else {
                $row = Category::where('id',$id)->first();
                
                $row->parent_id = $request->parent_id;
                $row->category_name = $request->category_name;
                $row->category_name_ar = $request->category_name_ar;
                // if ($request->hasFile('coloured_image') && $request->file('coloured_image'))
                // {
                //     $file = $request->file('coloured_image');

                //     $name = time().'_c.'.$file->getClientOriginalExtension();
                //     $destinationPath = public_path('/sp_uploads/sub_categories/');
                //     $img = $file->move($destinationPath, $name);

                //     $row->coloured_image = $name;
                // } 

                if ($request->hasFile('icon') && $request->file('icon'))
                {
                    $file = $request->file('icon');

                    $name = time().'.'.$file->getClientOriginalExtension();
                    $destinationPath = public_path('/sp_uploads/sub_categories/');
                    $img = $file->move($destinationPath, $name);

                    $row->icon = $name;
                } 

                $row->save();
                
                Session::flash('success', __('Sub Category Updated successfully.'));
                return redirect()->route('new-subcategory.index');
            }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

    public function destroy($id)
    {
        $row = Category::where('id', $id)->first();
        if ($row) {
             $row->delete();
             Session::flash('success', __('Sub Category deleted successfully.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('Invalid request.'));
            return redirect()->back();
        }
    }

    public function getCategoryName()
    {
        $cat_data = Category::where('parent_id','0')->get();
        $cat_arr = array();

        foreach($cat_data as $data)
        {
            $cat_arr[$data['id']] = $data['category_name']; 
        }
         
        return $cat_arr;
    }
}
