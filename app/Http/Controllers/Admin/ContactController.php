<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;   

use App\Model\Contact;
use datatables;
use App;
use Session;

class ContactController extends Controller
{
    protected $title;
    protected $model;
    protected $module;

    public function __construct()
    {
        $this->title = 'Contact';
        $this->model = 'contact';
        $this->module = 'contact';
    } 

    public function index()
    {
        $title = 'Contact';
        $module = $this->module;
        $model = $this->model;
        $breadcum = [$title=>'Contact'];        
        return view('admin.contact.index',compact('title','model','module','breadcum'));
    }

    public function getData(Request $request)
    {
        
        $columns = ['name','email', 'message', 'created'];
        $totalData = Contact::count();
        
        
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        $row = Contact::select("contacts.*");
        
        if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = $row->Where('name', 'LIKE', "%{$search}%")
                     ->orWhere('email', 'LIKE', "%{$search}%")
                     ->orWhere('message', 'LIKE', "%{$search}%");
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('contacts.id', 'desc')
                ->get();
      
        $data = array();
        if (!empty($row)) {
            foreach ($row as $key => $rows) {
                $nestedData['name'] = $rows->name;
                $nestedData['email'] = $rows->email;
                $nestedData['message'] = substr($rows->message,0,100).'...';
                $nestedData['status'] = ($rows->responded_at)?'Responded':'Pending';
                $nestedData['responded_at'] = ($rows->responded_at)?date('Y-m-d', strtotime($rows->responded_at)):'-';
                $nestedData['created'] = date('Y-m-d h:i A', strtotime($rows->created_at));
                $nestedData['action'] =  getButtons([
                    ['key'=>'view','link'=>route('contact.view',[$rows->id])],
                  ]); 
                $data[] = $nestedData;
            }

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function view($id){
      $title = 'View Contact Details';
      $model = 'contact';
      $breadcum = ['Contact'=>route($model.'.index'),$title =>''];     
      $result = Contact::where('id',$id)->first()->toArray();
     // echo '<pre>'; print_r($result);die;
     return view('admin.contact.view',compact('title','result','breadcum'));
    }


  public function save_contact(Request $request)
  {
    try {
      $data = $request->all();       

     // echo '<pre>'; print_r($data);die;
      $validator = Validator::make($data, 
          [
            'responded_at' => 'required',
            'responded_name' => 'required',
            'responded_email' => 'required',
          ]);
        if ($validator->fails()) 
        {            
          $error = $this->validationHandle($validator->messages());
          Session::flash('danger', $error);
          return redirect()->back()->withInput();
        } 
        else {
            // add data in payment_settlement data.
            $row = Contact::where('id',$data['id'])->first();
            $row->id = $data['id'];
            $row->responded_at = $data['responded_at'];
            $row->responded_name = $data['responded_name'];
            $row->responded_email = $data['responded_email'];
        
            //echo '<pre>'; print_r($row);die;
            if($row->save()){ 
               Session::flash('success', __('Contact has been successfully marked as Responded.'));
                return redirect()->route('contact.index');
            } else {
                Session::flash('danger', __('There is some issue to mark the contact as responded.'));
                return redirect()->back()->withInput();
            }
            
        }     
         
     }
     catch(\Exception $e){
       $msg = $e->getMessage();
       Session::flash('danger', $msg);
       return redirect()->back()->withInput();
     }
  }

}
