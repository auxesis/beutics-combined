<?php

namespace App\Http\Controllers\Admin;
use datatables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;   

use App\Model\CustomPage;
use DB;
use App;
use Session;
class CustomPagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $model;
    protected $title;
    protected $module;

    public function __construct()
    {
        $this->model = 'custompages';
        $this->title = 'Pages Manager';
        $this->module = 'custompages';
    }

    public function index()
    {
        $title = $this->title;
        $model = $this->model;
        $module = $this->module;
        $breadcum = [$title=>route($model.'.index')];
        return view('admin.custom_pages.index',compact('title','module','breadcum')); 
    }

    public function getData(Request $request)
    {
        $columns = ['slug','title','updated_at','created_at','content','action'];
        $totalData = CustomPage::count();
       
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        $pages = CustomPage::select('*');

        // $totalFiltered = User::count();

        if (!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $pages = $pages->where(function($query) use ($search) {
                $query->where('title', 'LIKE', "%{$search}%")
                        ->orWhere('slug', 'LIKE', "%{$search}%")
                        ->orWhere('updated_at', 'LIKE', "%{$search}%")
                        ->orWhere('created_at', 'LIKE', "%{$search}%");
            });
        }

        $data_query_count = $pages;
        $totalFiltered = $data_query_count->count();
        $pages = $pages->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        // print_r($users);die;
        $data = array();
        if (!empty($pages)) {
            foreach ($pages as $key => $row) {
                $nestedData['title'] = $row->title;
                $nestedData['created_at'] = date('d-M-y h:i:s',strtotime($row->created_at));
                $nestedData['action'] =  getButtons([
                                ['key'=>'edit','link'=>route('custompages.edit',$row->slug)],
                            ]);
                
                $data[] = $nestedData;
            }

        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    
    public function edit($slug)
    {
        $row = CustomPage::where('slug',$slug)->first();
        $title = 'Edit Custom Page';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        return view('admin.custom_pages.edit',compact('row','title','breadcum'));
    }

    public function update(Request $request)
    {
        try{
            
             if (Input::isMethod('post')) {
               
                $validation = array(
                    'title' => 'required',
                    'content' => 'required',                    
                    'content_ar' => 'required',                    
                );
            
                $validator = Validator::make(Input::all(), $validation);
               
                if ($validator->fails()) {

                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {
                    $row = CustomPage::where('slug', $request->slug)->first();
                    
                    $row->title = $request->title;
                    $row->content = $request->content;         
                    $row->content_ar = $request->content_ar;         
                    
                    $row->save();
                    
                    Session::flash('success', __('Custom Pages Updated successfully.'));
                   // return redirect('/custompages');
                    return redirect()->route('custompages.index');
                }
             }
        }
        catch( \Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

}
