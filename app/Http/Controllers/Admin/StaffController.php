<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;   

use App\Model\Staff;
use App\Model\SpUser;
use App\Model\StaffLeave;
use App\Model\StaffImagesVideos;
use App\Model\Booking;
use App\Model\Review;
use App\Model\Category;
use App\Model\Country;
use datatables;
use App;
use Session;
use DB;

class StaffController extends Controller
{

    protected $title;
    protected $model;
    protected $module;

    public function __construct()
    {
        $this->title = 'Staff';
        $this->model = 'staff';
        $this->module = 'staff';
    } 
   
    public function index()
    {
        $title = 'Staff';
        $module = $this->module;
        $model = $this->model;
        $spid = isset($_GET['spid']) ? $_GET['spid'] : '';
        if($spid){
            $sp_row = SpUser::where('id',$spid)->select('name')->first();

            $breadcum = [$sp_row->name => route('sp.index'),$title=>''];
        } else{
            $breadcum = [$title=>''];
        }
        
        return view('admin.staff.index',compact('title','model','module','breadcum', 'spid'));
    }

    public function getData(Request $request)
    {
        $spid = $request->input('spid');
        $columns = ['sp_id','staff_name','nationality','experience','speciality','is_provide_home_service'];
        if($request->input('spid')){
            $totalData = Staff::where('sp_id',$spid)->where('is_deleted','!=','1')->count();
        }
        else{
            $totalData = Staff::where('is_deleted','!=','1')->count();
        }
        
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');
        if($request->input('spid')){
             $row = Staff::where('sp_id',$spid)->select('staff.*')->where('is_deleted','!=','1');
        }
        else{
             $row = Staff::select('staff.*')->where('is_deleted','!=','1');
        }
        
        if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = $row->leftJoin('sp_users', 'sp_users.id', '=', 'staff.sp_id')
                    ->where(function($query) use ($search) {
                        $query->Where('staff_name', 'LIKE', "%{$search}%")
                        ->orWhere('sp_users.store_name', 'LIKE', "%{$search}%");
                        
                    });
        }


        $categories  = Category::where('category_name','!=','bridal')->where('parent_id','!=','0')->orderby('category_name', 'asc')->pluck('category_name', 'id')->toArray();
        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('staff.id', 'desc')
                ->get();
        // print_r($users);die;
        $data = array();
        if (!empty($row)) {
            foreach ($row as $key => $rows) {
                $speciality_arr = array();
                $arr = explode(",",$rows->speciality);
                foreach($arr as $cat_name_id)
                {
                    if($cat_name_id === '0')
                    {
                        $cat_name_id = 'All Rounders';
                    }
                    if(array_key_exists($cat_name_id,$categories))
                    {
                        $speciality_arr[] = $categories[$cat_name_id];
                    }
                    else
                    {
                        array_push($speciality_arr,$cat_name_id);
                    }
                }

                $nestedData['sp_id'] = $rows->getServiceProInfo->store_name;
                $nestedData['staff_name'] = $rows->staff_name;
                $nestedData['nationality'] = ($rows->getNationalityInfo['nationality']) ? $rows->getNationalityInfo['nationality'] : $rows->nationality;
                $nestedData['experience'] = $rows->experience;
                $nestedData['speciality'] = implode(',',$speciality_arr);
                if($rows->is_provide_home_service == 1){
                    $nestedData['is_provide_home_service'] = 'Yes';
                }else{
                    $nestedData['is_provide_home_service'] = 'No';
                }
                
                $edit_link = ($spid) ? route('staff.edit',[$rows->id, 'spid'=>$spid]) : route('staff.edit',[$rows->id]);
                $associatedTables = $this->checkAssociatedTables($rows->id);
                $nestedData['action'] =  getButtons([
                                ['key'=>'edit','link'=>$edit_link],
                                ['key'=>'delete_assoc','link'=>route('staff.destroy',$rows->id), 'asso'=>$associatedTables],
                            ]);
                
                $data[] = $nestedData;
            }

        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function checkAssociatedTables($id)
    {
        $tables = '';
        $service_count = StaffLeave::where('staff_id',$id)->count();
        $qf_count = Booking::where('staff_id',$id)->count();
        $promo_count = Review::where('staff_id',$id)->count();

        if($service_count > 0){
            $tables = $tables.'StaffLeave__';
        }
        if($qf_count > 0){
            $tables = $tables.'Bookings__';
        }
        if($promo_count > 0){
            $tables = $tables.'Reviews';
        }
        return $tables;
        //return explode(',',$tables);

    }

    public function create()
    {
        $action = "create";
        $title = 'Create Staff';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        $sp_row = SpUser::select('id','store_name')->get();
        $service_provider_arr = array();
        foreach($sp_row as $row){
            $service_provider_arr[$row['id']] = $row['store_name'];
        }

        $categories  = Category::where('category_name','!=','bridal')->where('parent_id','!=','0')->orderby('category_name', 'asc')->pluck('category_name', 'id')->toArray();
        $nationality_arr = Country::whereNotNull('nationality')->pluck('nationality', 'id')->toArray();
        return view('admin.staff.create',compact('action','title','breadcum','model','module','service_provider_arr','categories','nationality_arr'));
    }

   
    public function store(Request $request)
    {
        try{
             if (Input::isMethod('post')) {
                $validation = array(
                    /*'sp_id' => 'required',
                    'staff_name'=>'required|max:50',
                    'experience'=>'required',
                    'speciality'=>'required',
                    'staff_image'=>'mimes:jpeg,jpg,png,gif'*/
                );

                $desc = explode(' ',$request->description);
                $validator = Validator::make(Input::all(), $validation, array(
                    'sp_id.required' => 'Service provider name is required.',
                ));

                if(count($desc)>500){
                     $validator -> errors() -> add('description', 'Description cannot be more than 500 words.');
                     return redirect()->back()
                    -> withErrors($validator)
                    -> withInput(Input::all());
                }

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {
                     
                    $row = new Staff();
                    $row['sp_id'] = $request->sp_id;
                    $row['staff_name'] = $request->staff_name;
                    $row['nationality'] = $request->nationality;
                    $row['experience'] = $request->experience;
                    $row['speciality'] = implode(',',$request->speciality);
                    $row['certificates'] = $request->certificates;
                    $row['description'] = $request->description;
                    $row['description_ar'] = $request->description_ar;
                    $row['video_url'] = $request->video_url;

                    if(!$request->is_provide_home_service){
                        $row['is_provide_home_service'] = '0';
                    }else{
                        $row['is_provide_home_service'] = $request->is_provide_home_service;
                    }

                    if ($request->hasFile('staff_image') && $request->file('staff_image'))
                    {
                        $image = $request->image_cr;
                        list($type, $image) = explode(';', $image);
                        list(, $image)      = explode(',', $image);
                        $image = base64_decode($image);
                        $image_name= time().'.png';
                        $path = public_path('/sp_uploads/staff/'.$image_name);
                        file_put_contents($path, $image);
                        $row->staff_image = $image_name;
                    }
                    $row->save();
                    if($row->save()){

                        //upload image
                        if($request->staff_images){

                            $count = count($request->staff_images);
                        
                            foreach($request->staff_images as $key => $val){
                                if ($request->staff_images[$key])
                                {
                                    $img = $request->staff_images[$key];
                                    $file = $img;
                                    $getMimeType = $file->getClientMimeType();
                                    $name = time().str_random(2).'.'.$file->getClientOriginalExtension();
                                    $destinationPath = public_path('/sp_uploads/staff/');
                                    $img = $file->move($destinationPath, $name);
                                    $rowImage = new StaffImagesVideos();
                                    $rowImage->staff_id = $row->id;
                                    $rowImage->staff_image = $name;
                                    $rowImage->content_type = substr($getMimeType, 0, 5);
                                    $rowImage->save();
                                }
                            }
                        } 

                        //upload video
                        if($request->staff_videos){

                            $count = count($request->staff_videos);
                        
                            foreach($request->staff_videos as $key => $val){
                                if ($request->staff_videos[$key])
                                {
                                    $img = $request->staff_videos[$key];
                                    $file = $img;
                                    $getMimeType = $file->getClientMimeType();
                                    $name = time().str_random(2).'.'.$file->getClientOriginalExtension();
                                    $destinationPath = public_path('/sp_uploads/staff/');
                                    $img = $file->move($destinationPath, $name);
                                    $rowImage = new StaffImagesVideos();
                                    $rowImage->staff_id = $row->id;
                                    $rowImage->staff_video = $name;
                                    $rowImage->content_type = substr($getMimeType, 0, 5);
                                    $rowImage->save();
                                }
                            }
                        }
                    }

                    Session::flash('success', __('Staff member added successfully.'));
                    return redirect()->route('staff.index');
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

   
    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $rows = Staff::where('id',$id)->first();
        $title = 'Edit Staff';
        $model = $this->model;
        if(isset($_GET['spid'])){
            $sp_row = SpUser::where('id',$_GET['spid'])->select('name')->first();

            $breadcum = [$sp_row->name => route('sp.index'),'staff'=>route('staff.index', ['spid'=>$_GET['spid']]),$title=>''];
        }else{
            $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        }
        

        $sp_row = SpUser::select('id','store_name')->get();
        $service_provider_arr = $staff_images = $staff_videos = array();
        foreach($sp_row as $row){
            $service_provider_arr[$row['id']] = $row['store_name'];
        }
        
        $staff_images_videos = StaffImagesVideos::where('staff_id',$rows->id)->get();
       // echo "<pre>"; print_r($staff_images_videos); exit;
        foreach($staff_images_videos as $files)
        {
            $staff_images[$files['id']] = $files['staff_image'];
            $staff_videos[$files['id']] = $files['staff_video'];
        }

        $categories  = Category::where('category_name','!=','bridal')->where('parent_id','!=','0')->orderby('category_name', 'asc')->pluck('category_name', 'id')->toArray();
        $nationality_arr = Country::whereNotNull('nationality')->pluck('nationality', 'id')->toArray();
        return view('admin.staff.edit',compact('rows','title','breadcum','service_provider_arr','staff_images','staff_videos','categories','nationality_arr'));
    }

   
    public function update(Request $request, $id)
    {
        try{
            $validation = array(
                'sp_id' => 'required',
                'staff_name'=>'required|max:50',
                'experience'=>'required',
                'speciality'=>'required',
                'staff_image'=>'mimes:jpeg,jpg,png,gif',
            );
            $desc = explode(' ',$request->description);
            $validator = Validator::make(Input::all(), $validation, array(
                'sp_id.required' => 'Service provider name is required.'
            ));

            if(count($desc)>500){
                 $validator -> errors() -> add('description', 'Description cannot be more than 500 words.');
                 return redirect()->back()
                -> withErrors($validator)
                -> withInput(Input::all());
            }

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
            } 
            else {
              //  print_r($request->all());die;
                $row = Staff::where('id',$id)->first();
                $row['sp_id'] = $request->sp_id;
                $row['staff_name'] = $request->staff_name;
                $row['nationality'] = $request->nationality;
                $row['experience'] = $request->experience;
                $row['speciality'] = implode(',',$request->speciality);
                $row['certificates'] = $request->certificates;
                $row['description'] = $request->description;
                $row['description_ar'] = $request->description_ar;
                $row['video_url'] = $request->video_url;

                if(!$request->is_provide_home_service){
                    $row['is_provide_home_service'] = '0';
                }else{
                    $row['is_provide_home_service'] = $request->is_provide_home_service;
                }

                if ($request->hasFile('staff_image') && $request->file('staff_image'))
                {
                    $image = $request->image_cr;
                    list($type, $image) = explode(';', $image);
                    list(, $image)      = explode(',', $image);
                    $image = base64_decode($image);
                    $image_name= time().'.png';
                    $path = public_path('/sp_uploads/staff/'.$image_name);
                    file_put_contents($path, $image);
                    $row->staff_image = $image_name;
                }
                $row->save();
                if($row->save()){

                    //upload image
                    if($request->staff_images){

                        $count = count($request->staff_images);
                    
                        foreach($request->staff_images as $key => $val){
                            if ($request->staff_images[$key])
                            {
                                $img = $request->staff_images[$key];
                                $file = $img;
                                $getMimeType = $file->getClientMimeType();
                                $name = time().str_random(2).'.'.$file->getClientOriginalExtension();
                                $destinationPath = public_path('/sp_uploads/staff/');
                                $img = $file->move($destinationPath, $name);
                                $rowImage = new StaffImagesVideos();
                                $rowImage->staff_id = $row->id;
                                $rowImage->staff_image = $name;
                                $rowImage->content_type = substr($getMimeType, 0, 5);
                                $rowImage->save();
                            }
                        }
                    } 

                    //upload video
                    if($request->staff_videos){

                        $count = count($request->staff_videos);
                    
                        foreach($request->staff_videos as $key => $val){
                            if ($request->staff_videos[$key])
                            {
                                $img = $request->staff_videos[$key];
                                $file = $img;
                                $getMimeType = $file->getClientMimeType();
                                $name = time().str_random(2).'.'.$file->getClientOriginalExtension();
                                $destinationPath = public_path('/sp_uploads/staff/');
                                $img = $file->move($destinationPath, $name);
                                $rowImage = new StaffImagesVideos();
                                $rowImage->staff_id = $row->id;
                                $rowImage->staff_video = $name;
                                $rowImage->content_type = substr($getMimeType, 0, 5);
                                $rowImage->save();
                            }
                        }
                    }
                }

                Session::flash('success', __('Staff member updated successfully.'));
                return redirect()->route('staff.index');
            }

        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

    public function deleteImageVideos(Request $request)
    {
        $id = $request->staff_id;
        $row  = StaffImagesVideos::whereId($id)->first();
        if($row){
            if($row->content_type == 'image')
                unlink('public/sp_uploads/staff/'.$row->staff_image);
            else
                unlink('public/sp_uploads/staff/'.$row->staff_video);

           $row->delete();
           return 'success';
        }
    }
  
    public function destroy($id)
    {
        $row = Staff::where('id', $id)->first();
        if ($row) {
            // $row->delete();
             Session::flash('success', __('Staff deleted successfully.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('Invalid request.'));
            return redirect()->back();
        }
    }
}
