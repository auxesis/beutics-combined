<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input; 


use datatables;
use App;
use Session;
use App\Model\SpUser;
use App\Model\StaffLeave;
use App\Model\User;
use App\Model\Booking;
use App\Model\BookingItem;
use App\Model\PromoCodeLinkedCategory;
use App\Model\PromoCodeLinkedCustomer;
use App\Model\PromoCodeLinkedService;
use App\Model\PromoCodeLinkedStore;
use App\Model\PromoCodeLinkedSubcategory;
use App\Model\CashbackHistory;
use App\Model\RescheduleOrderHistory;
use App\Model\ShopTiming;
use App\Model\Staff;
use App\Model\WalletCashHistory;
use App\Model\PaymentSettlement;
use App\Model\PaymentSettlementOrder;
use App\Model\Setting;
use App\Model\Notification;
use App\Model\Chat;
use DB;
use Config;

class OrdersController extends Controller
{
    
    public function newOrders()
    {
        $title = 'All New Orders';
        $breadcum = [$title =>''];
        return view('admin.orders.newOrders',compact('title','row','breadcum'));
    }

    public function getData(Request $request)
    {
        $columns = ['name','sp_id', 'appointment_date','payment_type','created_date','action'];
        $order = $columns[$request->input('order.0.column')];

        $totalData = Booking::count();    
        $limit = $request->input('length');
        $start = $request->input('start');     

        $status_arr = array('0'=>'Awaiting','1'=>'Confirmed','2'=>'Cancelled','3'=>'Refunded','4'=>'Expired','5'=>'Completed');
        
        if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                })
                ->leftJoin('sp_users', function($join) {
                  $join->on('bookings.sp_id', '=', 'sp_users.id');
                })
                ->where(function ($query) use ($search){
                        $query->orWhere('users.name', 'like', '%' .$search . '%')
                        ->orWhere('sp_users.store_name', 'like', '%' .$search . '%')
                        ->orWhere('booking_unique_id', '=', $search)
                        ->orWhere('users.mobile_no', 'like', '%' .$search . '%');
                    })
                
                ->select('bookings.*', 'users.name', 'users.mobile_no')
                ->where('bookings.status','0')
                ->where('bookings.user_id','!=','')
                ->orderBy('bookings.id','desc');
        } else {
          $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                }) 
          ->select('bookings.*', 'users.name', 'users.mobile_no')
            ->where('bookings.user_id','!=','')
            ->where('bookings.status','0');
        }
       
        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('bookings.id', 'desc')
                ->get();
      
        $data = array();

        if (!empty($row)) {
            foreach($row as $val){
              $date = $val['appointment_date'];
              $time = $val['appointment_time'];
              $combinedDT = '';
              if($date!='' && $date!=null && $time!=null && $time!=''){
                 $combinedDT = date('M d, Y h:i A', strtotime("$date $time"));
               }else{
                $combinedDT = 'NA';
               }
               $contact_no = '';
               if($val['mobile_no']!='' && $val['mobile_no']!=null){
                $contact_no = '</br><b>('.$val['mobile_no'].')</b>';
               }
            $nestedData = [
              "id" => $val['id'],
              "booking_unique_id" => $val['booking_unique_id'],
              "name" => ucfirst($val['name']).$contact_no,
              "appointment_date" => $combinedDT,
              "payment_type" => $val['payment_type']=='0'?'Paid':'To Pay at Store',
              
              "tax_amount" => round($val['tax_amount'],2),
              "total_amount" => round($val['booking_total_amount'],2),
              "paid_amount" => round($val['paid_amount'],2),
              "created_date" => date('Y-m-d h:i:A',strtotime($val['created_at'])),
              "status" => $status_arr[$val['status']],
            ];
       
             if(!$val['user_id'] && !$val['is_gift'] && ($val['db_user_name'] || $val['db_user_mobile_no'])){
              $db_contact_no = '';
               if($val['db_user_mobile_no']!='' && $val['db_user_mobile_no']!=null){
                $db_contact_no = '</br><b>('.$val['db_user_mobile_no'].')</b>';
               }

              $nestedData['name'] = ucfirst($val['db_user_name']).$db_contact_no;
             
            }
            
            if(!$val['user_id'] && $val['is_gift'] && ($val['receiver_name'] || $val['receiver_mobile_no'])){
              $re_contact_no = '';
               if($val['receiver_mobile_no']!='' && $val['receiver_mobile_no']!=null){
                $re_contact_no = '</br><b>('.$val['receiver_mobile_no'].')</b>';
               }
               
              $nestedData['name'] = ucfirst($val['receiver_name']).$re_contact_no;
            
            }

            $nestedData['sp_id'] = ucfirst($val->getAssociatedSpInformation->store_name).'</br><b>('.$val->getAssociatedSpInformation->mobile_no.')</b>';
           
            $nestedData['action'] =  getButtons([
                            ['key'=>'view','link'=>route('orders.view',[$val->id,''])],
                            ['key'=>'chat','link'=>route('orders.orderChatHistory',[$val->id,''])],
                            ['key'=>'cancel','link'=>route('orders.cancelled_order_view',[$val->id, ''])]
                                                                                    
                        ]); 
            $data[] = $nestedData;
          }

        }
        
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }


    public function confirmOrders()
    {
        $title = 'All Confirmed Orders';
        $breadcum = [$title =>''];
        return view('admin.orders.confirmOrders',compact('title','row','breadcum'));
    }

    public function getDataConfirm(Request $request)
    {
       
        $columns = ['id','tax_amount', 'total_amount','paid_amount','created_date','db_user_name','db_user_mobile_no','booking_unique_id','appointment_date','appointment_time','status','service_end_time','Action'];
        $order = $columns[$request->input('order.0.column')];

        $totalData = Booking::count();    
        $limit = $request->input('length');
        $start = $request->input('start');     

        $status_arr = array('0'=>'Awaiting','1'=>'Confirmed','2'=>'Cancelled','3'=>'Refunded','4'=>'Expired','5'=>'Completed');
        
        if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                })
            ->leftJoin('sp_users', function($join) {
                  $join->on('bookings.sp_id', '=', 'sp_users.id');
                })
             
                ->where(function ($query) use ($search){
                        $query->orWhere('users.name', 'like', '%' .$search . '%')
                         ->orWhere('sp_users.store_name', 'like', '%' .$search . '%')
                        ->orWhere('booking_unique_id', '=', $search)
                        ->orWhere('users.mobile_no', 'like', '%' .$search . '%');
                    })
                
                ->select('bookings.*', 'users.name', 'users.mobile_no')
                ->where('bookings.status','1')
                ->orderBy('bookings.id','desc');
        } else {
         
          $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                }) 
          ->select('bookings.*', 'users.name', 'users.mobile_no')
            ->where('bookings.status','1');
        }

        if (!empty($request->input('from_appointment_date')) && !empty($request->input('to_appointment_date'))) {
               
                $from_app_date = $request->input('from_appointment_date');
                $to_app_date = $request->input('to_appointment_date');

                $row = $row->where(function($query) use ($from_app_date,$to_app_date) {
                $query->where('appointment_date','>=', $from_app_date)
                ->where('appointment_date','<=', $to_app_date);
            });             
        }


        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('bookings.id', 'desc')
                ->get();
        
        $data = array();

        if (!empty($row)) {
            foreach($row as $val){
              $date = $val['appointment_date'];
              $time = $val['appointment_time'];
              $combinedDT = '';
              if($date!='' && $date!=null && $time!=null && $time!=''){
                 $combinedDT = date('M d, Y h:i A', strtotime("$date $time"));
               }else{
                $combinedDT = 'NA';
               }

               $contact_no = '';
               if($val['mobile_no']!='' && $val['mobile_no']!=null){
                $contact_no = '</br><b>('.$val['mobile_no'].')</b>';
               }
// if($row['staff_id']!='' && $row['staff_id']!=null){
//       $data['staff_name'] = $row->getAssociatedStaffInformation->staff_name;
//     }else{
      
            $nestedData = [
              "id" => $val['id'],
              "booking_unique_id" => $val['booking_unique_id'],
              "name" => ucfirst($val['name']).$contact_no,
              "appointment_date" => $combinedDT,
              "service_end_time" => $date.' '.date('h:i A', strtotime($val['service_end_time'])),
              "staff_name" => ($val['staff_id']!='' && $val['staff_id']!=null)? $val->getAssociatedStaffInformation->staff_name:'',
              "payment_type" => $val['payment_type']=='0'?'Paid':'To Pay at Store',
              "tax_amount" => round($val['tax_amount'],2),
              "total_amount" => round($val['booking_total_amount'],2),
              "paid_amount" => round($val['paid_amount'],2),
              "created_date" => date('Y-m-d h:i:A',strtotime($val['created_at'])),
              "confirmed_date_time" => ($val['confirmed_date_time']!='' && $val['confirmed_date_time']!=null) ? date('Y-m-d h:i A',strtotime($val['confirmed_date_time'])):'-',
              "status" => $status_arr[$val['status']],
            ];
            

             if(!$val['user_id'] && !$val['is_gift'] && ($val['db_user_name'] || $val['db_user_mobile_no'])){
              $db_contact_no = '';
               if($val['db_user_mobile_no']!='' && $val['db_user_mobile_no']!=null){
                $db_contact_no = '</br><b>('.$val['db_user_mobile_no'].')</b>';
               }

              $nestedData['name'] = ucfirst($val['db_user_name']).$db_contact_no;
             
            }
            
            if(!$val['user_id'] && $val['is_gift'] && ($val['receiver_name'] || $val['receiver_mobile_no'])){
              $re_contact_no = '';
               if($val['receiver_mobile_no']!='' && $val['receiver_mobile_no']!=null){
                $re_contact_no = '</br><b>('.$val['receiver_mobile_no'].')</b>';
               }

              $nestedData['name'] = ucfirst($val['receiver_name']).$re_contact_no;
            
            }
            $nestedData['sp_id'] = ucfirst($val->getAssociatedSpInformation->store_name).'</br><b>('.$val->getAssociatedSpInformation->mobile_no.')</b>';
            $nestedData['action'] =  getButtons([
                            ['key'=>'view','link'=>route('orders.view',[$val->id,1])],
                            ['key'=>'chat','link'=>route('orders.orderChatHistory',[$val->id,1])],
                            ['key'=>'cancel','link'=>route('orders.cancelled_order_view',[$val->id,1])]                                                  
                        ]); 
            $data[] = $nestedData;
          }

        }
        
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }



    public function completeOrders()
    {
        $title = 'All Completed Orders';
        $breadcum = [$title =>''];
        return view('admin.orders.completeOrders',compact('title','row','breadcum'));
    }

    public function getDataComplete(Request $request)
    {
       
        $columns = ['id','tax_amount', 'total_amount','paid_amount','created_date','db_user_name','db_user_mobile_no','booking_unique_id','appointment_date','appointment_time','status','service_end_time','Action'];
        $order = $columns[$request->input('order.0.column')];

        $totalData = Booking::count();    
        $limit = $request->input('length');
        $start = $request->input('start');     

        $status_arr = array('0'=>'Awaiting','1'=>'Confirmed','2'=>'Cancelled','3'=>'Refunded','4'=>'Expired','5'=>'Completed');
        
         if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                })
            ->leftJoin('sp_users', function($join) {
                  $join->on('bookings.sp_id', '=', 'sp_users.id');
                })
             
                ->where(function ($query) use ($search){
                        $query->orWhere('users.name', 'like', '%' .$search . '%')
                         ->orWhere('sp_users.store_name', 'like', '%' .$search . '%')
                        ->orWhere('booking_unique_id', '=', $search)
                        ->orWhere('users.mobile_no', 'like', '%' .$search . '%');
                    })
                
                ->select('bookings.*', 'users.name', 'users.mobile_no')
                ->where('bookings.status','5')
                ->orderBy('bookings.id','desc');
        } else {
          
          $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                }) 
          ->select('bookings.*', 'users.name', 'users.mobile_no')
            ->where('bookings.status','5');
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('bookings.id', 'desc')
                ->get();
        
        $data = array();

        if (!empty($row)) {
            foreach($row as $val){
              $date = $val['appointment_date'];
              $time = $val['appointment_time'];
              $combinedDT = '';
              if($date!='' && $date!=null && $time!=null && $time!=''){
                 $combinedDT = date('M d, Y h:i A', strtotime("$date $time"));
               }else{
                $combinedDT = 'NA';
               }

               $contact_no = '';
               if($val['mobile_no']!='' && $val['mobile_no']!=null){
                $contact_no = '</br><b>('.$val['mobile_no'].')</b>';
               }

            $nestedData = [
              "id" => $val['id'],
              "booking_unique_id" => $val['booking_unique_id'],
              "name" => ucfirst($val['name']).$contact_no,
              "appointment_date" => $combinedDT,
              "service_end_time" => $date.' '.date('h:i A', strtotime($val['service_end_time'])),
              "payment_type" => $val['payment_type']=='0'?'Paid':'To Pay at Store',
              "tax_amount" => round($val['tax_amount'],2),
              "total_amount" => round($val['booking_total_amount'],2),
              "paid_amount" => round($val['paid_amount'],2),
              "created_date" => date('Y-m-d h:i:A',strtotime($val['created_at'])),
              "completed_date_time" => $val['completed_date_time']!='' ? date('Y-m-d h:i:A',strtotime($val['completed_date_time'])):'',
              "status" => $status_arr[$val['status']],
            ];
             if(!$val['user_id'] && !$val['is_gift'] && ($val['db_user_name'] || $val['db_user_mobile_no'])){
              $db_contact_no = '';
               if($val['db_user_mobile_no']!='' && $val['db_user_mobile_no']!=null){
                $db_contact_no = '</br><b>('.$val['db_user_mobile_no'].')</b>';
               }

              $nestedData['name'] = ucfirst($val['db_user_name']).$db_contact_no;
             
            }
            
            if(!$val['user_id'] && $val['is_gift'] && ($val['receiver_name'] || $val['receiver_mobile_no'])){
              $re_contact_no = '';
               if($val['receiver_mobile_no']!='' && $val['receiver_mobile_no']!=null){
                $re_contact_no = '</br><b>('.$val['receiver_mobile_no'].')</b>';
               }
               
              $nestedData['name'] = ucfirst($val['receiver_name']).$re_contact_no;
            
            }
            $nestedData['sp_id'] = ucfirst($val->getAssociatedSpInformation->store_name).'</br><b>('.$val->getAssociatedSpInformation->mobile_no.')</b>';
            $nestedData['action'] =  getButtons([
                            ['key'=>'view','link'=>route('orders.view',[$val->id,2])],
                            ['key'=>'chat','link'=>route('orders.orderChatHistory',[$val->id,2])]                                                        
                        ]); 
            $data[] = $nestedData;
          }

        }
        
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function expiredOrders()
    {
        $title = 'All Expired Orders';
        $breadcum = [$title =>''];
        return view('admin.orders.expiredOrder',compact('title','breadcum'));
    }

    public function getExpired(Request $request)
    {
        
        $columns = ['id','tax_amount', 'total_amount','paid_amount','created_date','db_user_name','db_user_mobile_no','booking_unique_id','appointment_date','appointment_time','status','service_end_time','Action'];
        $order = $columns[$request->input('order.0.column')];

        $totalData = Booking::count();    
        $limit = $request->input('length');
        $start = $request->input('start');     

        $status_arr = array('0'=>'Awaiting','1'=>'Confirmed','2'=>'Cancelled','3'=>'Refunded','4'=>'Expired','5'=>'Completed');
        
         if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                })
            ->leftJoin('sp_users', function($join) {
                  $join->on('bookings.sp_id', '=', 'sp_users.id');
                })
             
                ->where(function ($query) use ($search){
                        $query->orWhere('users.name', 'like', '%' .$search . '%')
                         ->orWhere('sp_users.store_name', 'like', '%' .$search . '%')
                        ->orWhere('booking_unique_id', '=', $search)
                        ->orWhere('users.mobile_no', 'like', '%' .$search . '%');
                    })
                
                ->select('bookings.*', 'users.name', 'users.mobile_no')
                ->where('bookings.status','4')
                ->orderBy('bookings.id','desc');
        } else {
          
          $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                }) 
          ->select('bookings.*', 'users.name', 'users.mobile_no')
            ->where('bookings.status','4');
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('bookings.id', 'desc')
                ->get();
        
        $data = array();

        if (!empty($row)) {
            foreach($row as $val){
            $nestedData = [
              "id" => $val['id'],
              "booking_unique_id" => $val['booking_unique_id'],
              "name" => ucfirst($val['name']).'</br><b>('.$val['mobile_no'].')</b>',
              "appointment_time" => date('h:i A', strtotime($val['appointment_time'])),
              "appointment_date" => $val['appointment_date']!='' && $val['appointment_date']!=null ? date('Y-m-d',strtotime($val['appointment_date'])):"",
              "service_end_time" => $val['appointment_date'].' '.date('h:i A', strtotime($val['service_end_time'])),
              "tax_amount" => round($val['tax_amount'],2),
              "total_amount" => round($val['booking_total_amount'],2),
              "paid_amount" => round($val['paid_amount'],2),
              "created_date" => date('Y-m-d h:i:A',strtotime($val['created_at'])),
              "status" => $status_arr[$val['status']],
            ];
             if(!$val['user_id'] && !$val['is_gift'] && ($val['db_user_name'] || $val['db_user_mobile_no'])){
              $nestedData['name'] = ucfirst($val['db_user_name']).'</br><b>('.$val['db_user_mobile_no'].')</b>';
             
            }
            if(!$val['user_id'] && $val['is_gift'] && ($val['receiver_name'] || $val['receiver_mobile_no'])){
              $nestedData['name'] = ucfirst($val['receiver_name']).'</br><b>('.$val['receiver_mobile_no'].')</b>';
            
            }
            $nestedData['sp_id'] = ucfirst($val->getAssociatedSpInformation->store_name).'</br><b>('.$val->getAssociatedSpInformation->mobile_no.')</b>';
            $nestedData['action'] =  getButtons([
                            ['key'=>'view','link'=>route('orders.view',[$val->id,3])], 
                            // ['key'=>'chat','link'=>route('orders.orderChatHistory',[$val->id,3])]                                                       
                        ]); 
            $data[] = $nestedData;
          }

        }
        
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }
    public function showBookingDetails($row)
  {

    $data = array();
    $data['id'] = $row['id'];
    $data['booking_unique_id'] = $row['booking_unique_id'];
    $data['booking_service_type'] = $row['booking_service_type'] == 'shop'?'Instore':'Home';
    $data['sp_id'] = $row['sp_id'];
    $data['db_user_name'] = $row['db_user_name'];
    $data['db_user_mobile_no'] = $row['db_user_mobile_no'];
    if(!$row['user_id'] && !$row['is_gift'] && ($row['db_user_name'] || $row['db_user_mobile_no'])){
      $data['user_name'] = $row['db_user_name'];                          
    }elseif(!$row['user_id'] && $row['is_gift'] && ($row['receiver_name'] || $row['receiver_mobile_no'])){
      $data['user_name'] = $row['receiver_name'];                         
    } else {
      $data['user_name'] = $row->getAssociatedUserInfo->name;
      $data['user_profile_image'] = $row->getAssociatedUserInfo->image!='' ? asset("uploads/".$row->getAssociatedUserInfo->image):'';
    }
    
    $data['user_id'] = $row['user_id'];
    $data['staff_id'] = $row['staff_id'];
    if($row['staff_id']!='' && $row['staff_id']!=null){
      $data['staff_name'] = $row->getAssociatedStaffInformation->staff_name;
    }else{
      $data['staff_name'] = "";
    }
    
    $data['confirmed_date_time'] = $row['confirmed_date_time'];
    $data['cancelled_date_time'] = $row['cancelled_date_time'];
    $data['completed_date_time'] = $row['completed_date_time'];
    $data['cancellation_reason'] = $row['cancellation_reason'];
    $data['cancellation_refunded_amount'] = $row['cancellation_refunded_amount'];
    $data['final_settlement_amount'] = round($row['final_settlement_amount'],2);
    $data['final_settlement_tax'] = round($row['final_settlement_tax'],2);
    $data['final_settlement_cashback'] = round($row['final_settlement_cashback'],2);
   
    $data['is_gift'] = $row['is_gift'];
    $data['status'] = $row['status'];
    $data['appointment_time'] = $row['appointment_time'];
    $data['invoice'] = $row['invoice'];
    $data['service_end_time'] = $row['service_end_time'];
    $data['appointment_date'] = $row['appointment_date']!='' && $row['appointment_date']!= null ? $row['appointment_date']:"";
    $data['redeemed_reward_points'] = $row['redeemed_reward_points'];
    $data['payment_type'] = $row['payment_type'];
    if($row->getAssociatedPromoCodeInformation){
      $data['promo_code_title'] = $row->getAssociatedPromoCodeInformation->promo_code_title;
    } else {
      $data['promo_code_title'] = '';
    }
    
    $data['promo_code_amount'] = round($row['promo_code_amount'],2);
    $disc = ($data['redeemed_reward_points']+$data['promo_code_amount']);
    $data['total_final_settlement_amount'] = (($data['final_settlement_amount']-$disc)+$data['final_settlement_tax']);
    $data['sp_tax'] = $data['tax_per'] = 0;
    if($row->getAssociatedSpInformation->is_tax_applied){
       $tax = Setting::select('percentage')->first();
       $data['tax_per'] = $tax['percentage'];
      if($data['final_settlement_amount']){
        $data['sp_tax'] = round((($data['final_settlement_amount'] * $tax['percentage'])/100),2);
        
      } else {
        $data['sp_tax'] = round((($row['total_item_amount'] * $tax['percentage'])/100),2);
        
      }
    }

    $tier = SpUser::leftJoin('tiers', function($join) {
              $join->on('sp_users.tier_id', '=', 'tiers.id');
            })
        ->where('sp_users.id',$row['sp_id'])
        ->select('tiers.commission')
        ->first();

    $data['beutics_comm_per'] = round($tier->commission,0).'%';
    if($data['final_settlement_amount']){
      $data['beutics_comm'] = round((($data['final_settlement_amount'] * $tier->commission)/100),2);
      $amt = $data['final_settlement_amount'];
      $paid_amt = $data['total_final_settlement_amount'];
    } else {
      $data['beutics_comm'] = round((($row['total_item_amount'] * $tier->commission)/100),2);
      $amt = $row['total_item_amount'];
      $paid_amt = $row['paid_amount'];
    }
    //$data['gateway_charges'] = round((($paid_amt * 2.7)/100),2)+1;
    //$data['total_to_pay'] = ($amt-$data['beutics_comm']);
    $data['tax_amount'] = round($row['tax_amount'],2);
    $data['total_amount'] = round($row['booking_total_amount'],2);
    $data['paid_amount'] = round($row['paid_amount'],2);
    $data['total_item_amount'] = round($row['total_item_amount'],2);
    $data['total_cashback'] = round($row['total_cashback'],2);
    $data['created_date'] = date('Y-m-d h:i:A',strtotime($row['created_at']));
    $data['gift_send_date'] = ($row['gift_send_date'])?date('Y-m-d',strtotime($row['gift_send_date'])):date('Y-m-d',strtotime($row['created_at']));
    $data['receiver_name'] = $row['receiver_name'];
    $data['receiver_mobile_no'] = $row['receiver_mobile_no'];
    $data['receiver_email'] = $row['receiver_email'];
    $data['receiver_message'] = $row['receiver_message'];
    $data['gifted_by'] = $row['gifted_by'];
    if($row['gifted_by']){
    $data['gifted_by_name'] = ucfirst($row->getAssociatedGiftByUserInfo->name).'</br><b>('.$row->getAssociatedGiftByUserInfo->mobile_no.')</b>';
    }
    
    $data['sp_store_name'] = ucfirst($row->getAssociatedSpInformation->store_name);
    $data['sp_name'] = ucfirst($row->getAssociatedSpInformation->name).'</br><b>('.$row->getAssociatedSpInformation->mobile_no.')</b>';
    $item_name_arr = array();
    foreach($row->getAssociatedBookingItems as $bkitems){

      if($bkitems['is_service'] == '1')
      {
        $item_name_arr[] = [
          'service_id' => $bkitems['booking_item_id'],
          'name' => $bkitems->getAssociatedServiceDetail->getAssociatedService->name,
          'quantity' => $bkitems['quantity'],
          'original_price' => round($bkitems['original_price'],2),
          'best_price' => round($bkitems['best_price'],2),
          'cashback' => round($bkitems['cashback'],2),
          'gender' => $bkitems['gender'],
        ];
      }
      else{
        $offer_services = array();

        foreach($bkitems->getAssociatedOfferDetail->getAssociatedOfferServices as $os){

          $offer_services[] = [
            'service_id' => $os['sp_service_id'],
            'quantity' => $os['quantity'],
            'name' => $os->getAssociatedOfferServicesName->getAssociatedService->name,
          ];
        }

        $item_name_arr[] = [
          'offer_id' => $bkitems['booking_item_id'],
          'name' => $bkitems->getAssociatedOfferDetail->getAssociatedOfferName->title,
          'offer_terms' => $bkitems->getAssociatedOfferDetail->offer_terms,
          'offer_details' => $bkitems->getAssociatedOfferDetail->offer_details,
          'gender' => $bkitems['gender'],
          'quantity' => $bkitems['quantity'],
          'original_price' => round($bkitems['original_price'],2),
          'best_price' => round($bkitems['best_price'],2),
          'cashback' => round($bkitems['cashback'],2),
          'services'=>$offer_services,
        ];
      }

    }

    $reschedule_arr = array();
    foreach($row->getAssociatedRescheduleOrderHistory as $reschedule){

      $reschedule_arr[] = [
        'appointment_date' => date('Y-m-d',strtotime($reschedule['appointment_date'])),
        'appointment_time' => date('h:i A',strtotime($reschedule['appointment_time'])),
        'service_end_time' => date('h:i A',strtotime($reschedule['service_end_time'])),
        'created_at' => date('Y-m-d h:i A',strtotime($reschedule['created_at']))
      ];  
    }

    $data['booking_items'] = $item_name_arr;
    $data['reschedule_history'] = $reschedule_arr;
    
    $data['user_refer_code'] = isset($row->getAssociatedUserInfo->refer_code)?$row->getAssociatedUserInfo->refer_code:'';
    $data['sp_share_code'] = isset($row->getAssociatedSpInformation->share_code)?$row->getAssociatedSpInformation->share_code:'';

    $payment_details = '';
    $data['pt_transaction_id'] = '';
    if($row->getAssociatedBookingPaymentDetailAll){
        foreach($row->getAssociatedBookingPaymentDetailAll as $d_val){
            if($d_val->transaction_id){
              $data['pt_transaction_id'] = $d_val->transaction_id;
            }
            $payment_details .= ucfirst($d_val->payment_option).'('.round($d_val->amount,2).' AED)'.', ';
        }
    }
    $data['payment_details'] = rtrim($payment_details, ' ,');
    //echo '<pre>'; print_r($data);die;
    return $data;
        
  }

    public function view($id, $flag = ''){
      $title = 'Order Details';
      $model = 'orders';
      if($flag && $flag == 1){
        $breadcum = ['Orders'=>route($model.'.confirm'),$title =>''];
      } elseif($flag && $flag == 2){
        $breadcum = ['Orders'=>route($model.'.complete'),$title =>''];
      } elseif($flag && $flag == 3){
        $breadcum = ['Orders'=>route($model.'.cancelled'),$title =>''];
      } elseif($flag && $flag == 4){
        $breadcum = ['Orders'=>route($model.'.unpaid_settlements'),$title =>''];
      } elseif($flag && $flag == 5){
        $breadcum = ['Orders'=>route($model.'.paid_settlements'),$title =>''];
      } else {
        $breadcum = ['Orders'=>route($model.'.new'),$title =>''];
      }
      
      $order = Booking::where('id',$id)->first();
      $result = $this->showBookingDetails($order);  
      //echo '<pre>'; print_r($result);die;
     return view('admin.orders.View',compact('title','result','breadcum'));
    }


    public function cancelled_order_view($id, $flag=''){
      $title = 'Cancel Order';
      $model = 'orders';
      if($flag && $flag == 1){
        $breadcum = ['Orders'=>route($model.'.confirm'),$title =>''];
      } else {
        $breadcum = ['Orders'=>route($model.'.new'),$title =>''];
      }
      $order = Booking::where('id',$id)->first();
      $result = $this->showBookingDetails($order);  
     if($order->status!='2')  {
        return view('admin.orders.CancelledOrderView',compact('title','result','breadcum'));
      }else{
        return view('errors.404');
      }
    }
/*
    public function cancel($id){
      $rows = Booking::where('id',$id)->first();
      //print_r($rows);die;
      if($rows){
          $rows->status = '2';
          if($rows->save()){
            Session::flash('success', __('Order has been cancelled.'));
            return redirect()->back();
          }
      }
    }*/


  public function cancel(Request $request)
  {
    try {
        $data = $request->all();       

        //echo '<pre>'; print_r($data);die;
        $validator = Validator::make($data, 
            [
              'order_id' => 'required',
            ]);
          if ($validator->fails()) 
          {
            
            $error = $this->validationHandle($validator->messages());
            Session::flash('danger', $error);
            return redirect()->back()->withInput();
          } 
          else 
          {
            $rows = Booking::where('id',$request->order_id)->first();
            

            if($rows){
                
                if($request->cancellation_refunded_amount && ($request->cancellation_refunded_amount > $rows->paid_amount)){
                  Session::flash('danger', 'Please enter a valid refund amount, the entered refund amount should not exceed the total paid amount.');
                  return redirect()->back()->withInput();
                }  
                
                $rows->cancellation_refunded_amount = $request->cancellation_refunded_amount;          
                $rows->cancellation_reason = $request->cancellation_reason;
                $rows->status = '2';
                $rows->cancelled_date_time = date('Y-m-d H:i:s');

                if($rows->save()){

                  //notification for sp
                  //check cashback already exists or not

                  $check_wallet = WalletCashHistory::where('trans_ref_id',$rows->id)->where('description',$request->description)->first();

                  if($rows->is_gift == '' && $rows->is_gift == null){
                      $rewarded_user_id = $rows->user_id;
                    }else{
                      $rewarded_user_id = $rows->gifted_by;
                    }
                    
                    $user = User::where('id',$rewarded_user_id)->first();  
                    
                  if($rows->db_user_name == '' && $rows->db_user_name == null && !$check_wallet){  
                
                    $user->wallet_amount = ($user->wallet_amount+$request->cancellation_refunded_amount);
                    $user->save();

                    //return redeemed reward points
                    if($rows->redeemed_reward_points!=null && $rows->redeemed_reward_points!=''){
                     
                      $earning = $user->earning + $rows->redeemed_reward_points ;
                      $user->earning = $earning;
                      $user->save();

                      $cashback = new CashbackHistory();
                      $cashback->user_id = $rewarded_user_id;
                      $cashback->booking_id = $rows->id;
                      $cashback->type = '4';
                      $cashback->amount =round($rows->redeemed_reward_points,2);
                      $cashback->save();

                    }


                    $wallet_row = new WalletCashHistory();
                   
                    $wallet_row->user_id = $rewarded_user_id;
                    $wallet_row->trans_ref_id = $request->order_id;
                    $wallet_row->amount = $request->cancellation_refunded_amount;
                    $wallet_row->transaction_type = '0';
                    $wallet_row->description = $request->description;
                    $wallet_row->transaction_date_time = date('Y-m-d H:i:s');
                    $wallet_row->save();                    
                  }

                  
                  if(!$check_wallet){
                     //notification for customer
                     if($rows->user_id!='' && $rows->user_id!=null && $user->notification_alert_status == '1'){
                   
                      // $message_title = 'Order Cancelled!';
                      // $message = 'Your booking with order number ('.$rows->booking_unique_id.') with '.$rows->getAssociatedSpInformation->store_name.' has been cancelled as per your request. We have initiated the refund to your wallet and the same will reflect asap.';

                      if($rows->payment_type!='1'){
                        $message_title_main = __('messages.Refund Credit!');
                        // $message_main = 'Hey, we have processed your cancellation and refunded your wallet with AED '.$request->cancellation_refunded_amount.'. Enjoy shopping with Beutics!';

                        $message_main =  __('messages.cancellation',['refund_amount' => $request->cancellation_refunded_amount]);

                        app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');


                        $message_title_other = __('messages.Refund Credit!');

                        $message_other =  __('messages.cancellation',['refund_amount' => $request->cancellation_refunded_amount]);
                     
                        Notification::saveNotification($rewarded_user_id,$rows->sp_id,$rows->id,'ORDER_CANCELLED',$message_main,$message_other,$message_title_main,$message_title_other,'customer');
                        app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

                        
                      }else{
                        $message_title_main = __('messages.Order Cancelled!');
                        // $message = 'Your booking with order number ('.$rows->booking_unique_id.') with '.$rows->getAssociatedSpInformation->store_name.' has been cancelled as per your request.';

                        $message_main =  __('messages.cancellation_1',['order_id' => $rows->booking_unique_id,'store_name'=>$rows->getAssociatedSpInformation->store_name]);

                        app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');


                        $message_title_other = __('messages.Order Cancelled!');
                        $message_other = __('messages.cancellation_1',['order_id' => $rows->booking_unique_id,'store_name'=>$rows->getAssociatedSpInformation->store_name]);
                     
                        Notification::saveNotification($rewarded_user_id,$rows->sp_id,$rows->id,'ORDER_CANCELLED',$message_main,$message_other,$message_title_main,$message_title_other,'customer');
                        app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

                      }
                      

                    }

                    if($rows->getAssociatedSpInformation->notification_alert_status == '1'){
                      $name = "";
                      if(!isset($rows->getAssociatedUserInfo->name)){
                        $name = $rows->receiver_name;
                        
                      }else{
                          $name = $rows->getAssociatedUserInfo->name;
                      }
                      $message_title_main = __('messages.Order Cancellation!');
                      // $message_main = 'Due to certain unforeseen circumstances Beutics user '.$name.' had to cancel his order. We regret the inconvenience but stay tuned for more orders.';

                      $message_main = __('messages.cancel msg',['name' => $name]);

                      app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');


                      $message_title_other = __('messages.Order Cancellation!');
                      $message_other = __('messages.cancel msg',['name' => $name]);

                      Notification::saveNotification($rows->sp_id,$rewarded_user_id,$rows->id,'ORDER_CANCELLED',$message_main,$message_other,$message_title_main,$message_title_other,'sp');
                      app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

                    }


                   if($rewarded_user_id!='' &&$rewarded_user_id!=null){
                      $data['templete'] = "order_cancellation";
                      $data['name'] = $rows->getAssociatedUserInfo->name;
                      $data['email'] = $rows->getAssociatedUserInfo->email;
                      $data['order_no'] = $rows->booking_unique_id;
                      $data['store_name'] = $rows->getAssociatedSpInformation->store_name;
                      $data['payment_method'] = $rows->payment_type == '0'?'Online':'Offline';
                      $data['payment_amount'] = $rows->cancellation_refunded_amount;
                      $data['cancel_date'] = $rows->cancelled_date_time;
                      $data['subject'] = "Order cancellation request successful.";
                      send($data);
                    }
                  }
                 

                  Session::flash('success', __('Order has been cancelled Successfully.'));
                  return redirect()->route('orders.cancelled');
                }
              }else{
                Session::flash('danger', __('OrderId is not valid.'));
                 return redirect()->back()->withInput();
              }
              
          }
            
     }
     catch(\Exception $e){
       $msg = $e->getMessage();
       Session::flash('danger', $msg);
       return redirect()->back()->withInput();
     }
  }

    public function cancelledOrders()
    {
        $title = 'All Cancelled Orders';
        $breadcum = [$title =>''];
        return view('admin.orders.cancelledOrders',compact('title','row','breadcum'));
    }

    public function getCancelled(Request $request)
    {
        
        $columns = ['id','tax_amount', 'total_amount','paid_amount','created_date','db_user_name','db_user_mobile_no','booking_unique_id','appointment_date','appointment_time','status','service_end_time','Action'];
        $order = $columns[$request->input('order.0.column')];

        $totalData = Booking::count();    
        $limit = $request->input('length');
        $start = $request->input('start');     

        $status_arr = array('0'=>'Awaiting','1'=>'Confirmed','2'=>'Cancelled','3'=>'Refunded','4'=>'Expired','5'=>'Completed');
        
         if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                })
            ->leftJoin('sp_users', function($join) {
                  $join->on('bookings.sp_id', '=', 'sp_users.id');
                })
             
                ->where(function ($query) use ($search){
                        $query->orWhere('users.name', 'like', '%' .$search . '%')
                         ->orWhere('sp_users.store_name', 'like', '%' .$search . '%')
                        ->orWhere('booking_unique_id', '=', $search)
                        ->orWhere('users.mobile_no', 'like', '%' .$search . '%');
                    })
                
                ->select('bookings.*', 'users.name', 'users.mobile_no')
                ->where('bookings.status','2')
                ->orderBy('bookings.id','desc');
        } else {
          
          $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                }) 
          ->select('bookings.*', 'users.name', 'users.mobile_no')
            ->where('bookings.status','2');
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('bookings.id', 'desc')
                ->get();
        
        $data = array();

        if (!empty($row)) {
            foreach($row as $val){
            $nestedData = [
              "id" => $val['id'],
              "booking_unique_id" => $val['booking_unique_id'],
              "name" => ucfirst($val['name']).'</br><b>('.$val['mobile_no'].')</b>',
              "appointment_time" => date('h:i A', strtotime($val['appointment_time'])),
              "appointment_date" => $val['appointment_date']!='' && $val['appointment_date']!=null ? date('Y-m-d',strtotime($val['appointment_date'])):"",
              "service_end_time" => $val['appointment_date'].' '.date('h:i A', strtotime($val['service_end_time'])),
              "tax_amount" => round($val['tax_amount'],2),
              "total_amount" => round($val['booking_total_amount'],2),
              "paid_amount" => round($val['paid_amount'],2),
              "created_date" => date('Y-m-d h:i:A',strtotime($val['created_at'])),
              "status" => $status_arr[$val['status']],
            ];
             if(!$val['user_id'] && !$val['is_gift'] && ($val['db_user_name'] || $val['db_user_mobile_no'])){
              $nestedData['name'] = ucfirst($val['db_user_name']).'</br><b>('.$val['db_user_mobile_no'].')</b>';
             
            }
            if(!$val['user_id'] && $val['is_gift'] && ($val['receiver_name'] || $val['receiver_mobile_no'])){
              $nestedData['name'] = ucfirst($val['receiver_name']).'</br><b>('.$val['receiver_mobile_no'].')</b>';
            
            }
            $nestedData['sp_id'] = ucfirst($val->getAssociatedSpInformation->store_name).'</br><b>('.$val->getAssociatedSpInformation->mobile_no.')</b>';
            $nestedData['action'] =  getButtons([
                            ['key'=>'view','link'=>route('orders.view',[$val->id,3])], 
                            ['key'=>'chat','link'=>route('orders.orderChatHistory',[$val->id,3])]                                                       
                        ]); 
            $data[] = $nestedData;
          }

        }
        
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function sp_unpaid_settlements()
    { 
        $title = 'All SP Unpaid Settlements';
        $breadcum = [$title =>''];
        return view('admin.orders.spUnpaidSettlements',compact('title','breadcum'));
    }

    public function getDataSpUnpaid(Request $request)
    {
       
        $columns = ['id','store_name','Action'];
        $order = $columns[$request->input('order.0.column')];

        $totalData = SpUser::count();    
        $limit = $request->input('length');
        $start = $request->input('start');     
        

        if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $rows = SpUser::where(function ($query) use ($search){
                        $query->Where('store_name', 'like', '%' .$search . '%');
                    })
                ->orderby('store_name', 'asc')->pluck('store_name', 'id')->toArray();
        } else {
           $rows  = SpUser::orderby('store_name', 'asc')->pluck('store_name', 'id')->toArray();
        }

        //echo '<pre>'; print_r($row);die;
        $data = array();
        if(!empty($rows)){
          $s_no = 1;
          foreach($rows as $key => $val){
            $nested_data  =array();
            $unpaid_bookings = 0;
            $unpaid_bookings = Booking::where('sp_id', $key)->where('payment_settlement_status', 'unpaid')->where('status', '5')->count();
            if($unpaid_bookings){
              $nested_data['s_no'] = $s_no;
              $nested_data['name'] = $val;
              $nested_data['total_pending'] = $unpaid_bookings;
              $nested_data['action'] = getButtons([
                          ['key'=>'view','link'=>route('orders.unpaid_settlements',[$key])]
                      ]); 
              $s_no++;
              $data[] = $nested_data;
            }
          }
        }
        //echo '<pre>'; print_r($data);die;        
        $totalFiltered = count($data);
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }


    public function unpaid_settlements($sp_id =null)
    { //echo $sp_id;die;
        $title = 'All Unpaid Settlements';
        $breadcum = ['All SP Unpaid Settlements'=>route('orders.sp_unpaid_settlements'),$title =>''];
        $sp_list  = SpUser::orderby('store_name', 'asc')->pluck('store_name', 'id')->toArray();
        return view('admin.orders.unpaidSettlements',compact('title','row','breadcum', 'sp_list', 'sp_id'));
    }

    public function getDataUnpaid(Request $request)
    {
        $columns = ['id','tax_amount', 'total_amount','paid_amount','created_date','db_user_name','db_user_mobile_no','booking_unique_id','appointment_date','appointment_time','status','service_end_time','Action'];
        $order = $columns[$request->input('order.0.column')];

        $totalData = Booking::count();    
        $limit = $request->input('length');
        $start = $request->input('start');     
         $sp_ref = Booking::leftJoin('sp_users', function($join) {
                  $join->on('bookings.sp_id', '=', 'sp_users.id');
                })
                      ->leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                })
                ->whereNotNull('user_id')
                ->where('bookings.status','5')
                ->where('payment_type','1')
                ->where('bookings.payment_settlement_status','unpaid')
                ->whereRaw('sp_users.share_code = users.refer_code')
                //->where('bookings.sp_id',$user_id)
                ->distinct('bookings.user_id')
                    ->pluck('bookings.user_id');

         if(!empty($request->input('sp_id'))) {
            $sp_id = $request->input('sp_id');
            $settlement_option = SpUser::where('id',$sp_id)->select('settlement_option')->first();

            $sp_ref = Booking::leftJoin('sp_users', function($join) {
                  $join->on('bookings.sp_id', '=', 'sp_users.id');
                })
                      ->leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                })
                ->whereNotNull('user_id')
                ->where('bookings.status','5')
                ->where('payment_type','1')
                ->where('bookings.payment_settlement_status','unpaid')
                ->whereRaw('sp_users.share_code = users.refer_code')
                ->where('bookings.sp_id',$sp_id)
                ->distinct('bookings.user_id')
                    ->pluck('bookings.user_id');

            $payment_type = ($request->input('payment_type'))?$request->input('payment_type'):'0';
            if($payment_type == 0){
                $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                })
                ->leftJoin('sp_users', function($join) {
                  $join->on('bookings.sp_id', '=', 'sp_users.id');
                })             
                ->where('bookings.sp_id',$sp_id)   
                ->where('bookings.status','5')
                // if($settlement_option['settlement_option']!='billable'){
                //   $row = $row->where('bookings.payment_type','!=','1');
                // }
                
                ->where('bookings.payment_settlement_status','unpaid')
                ->whereNotIn('bookings.user_id',$sp_ref) 
                ->whereNull('bookings.db_user_name')
                ->where('bookings.settlement_option','billable')      
                ->orderBy('bookings.id','desc')
                ->select('bookings.*', 'users.name', 'users.mobile_no');

            } else {
              $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                })
                ->leftJoin('sp_users', function($join) {
                  $join->on('bookings.sp_id', '=', 'sp_users.id');
                })    

                ->where('bookings.sp_id',$sp_id)   
                ->where('bookings.status','5')
                ->where('bookings.payment_settlement_status','unpaid')

                // if($settlement_option['settlement_option']!='billable'){
                //   $row = $row->where(function ($query) use($sp_ref){     
                //     $query->where(function ($query) use ($sp_ref){  
                //       $query->whereIn('bookings.user_id',$sp_ref) 
                //           ->orWhereNotNull('bookings.db_user_name');   
                //      }) 
                //       ->orWhere(function ($query){ 
                //          $query->where('bookings.payment_type','1');
                //       });          
                                   
                //   });   
                // }else{
                  ->where(function ($query) use($sp_ref){                  
                    $query->whereIn('bookings.user_id',$sp_ref) 
                          ->orWhereNotNull('bookings.db_user_name')
                          ->orWhere('bookings.payment_type','1');                 
                  })
                  ->where('bookings.settlement_option','nonbillable') 
                  
                // }                      
                  ->orderBy('bookings.id','desc')
                  ->select('bookings.*', 'users.name', 'users.mobile_no');
            }
          
        } 
        $totalFiltered = 0;
        if (!empty($row)) {
          $data_query_count = $row;
          $totalFiltered = $data_query_count->count();
          $row = $row->offset($start)
                  ->limit($limit)
                  ->orderBy('bookings.id', 'desc')
                  ->get();
        }

        $data = array();

        if (!empty($row)) {
            foreach($row as $val){
              $date = $val['appointment_date'];
              $time = $val['appointment_time'];
              $combinedDT = '';
              if($date!='' && $date!=null && $time!=null && $time!=''){
                 $combinedDT = date('M d, Y h:i A', strtotime("$date $time"));
               }else{
                $combinedDT = 'NA';
               }

               $contact_no = '';
               if($val['mobile_no']!='' && $val['mobile_no']!=null){
                $contact_no = '</br><b>('.$val['mobile_no'].')</b>';
               }

            $nestedData = [
              "id" => $val['id'],
              "input_el" => '<input type="checkbox" name="ids[]" value="'.$val['id'].'" /> ',
              "booking_unique_id" => $val['booking_unique_id'],
              "name" => ucfirst($val['name']).$contact_no,
              "appointment_date" => $combinedDT,
              "service_end_time" => date('h:i A', strtotime($val['service_end_time'])),
              //"payment_type" => $val['payment_type']=='0'?'Paid':'To Pay at Store',
              "tax_amount" => round($val['tax_amount'],2),
              "total_amount" => round($val['booking_total_amount'],2),
              "paid_amount" => round($val['paid_amount'],2).' AED',
              "created_date" => date('Y-m-d h:i:A',strtotime($val['created_at'])),
              "completed_date_time" => $val['completed_date_time']!='' ? date('Y-m-d h:i:A',strtotime($val['completed_date_time'])):'',
            ];
            if(empty($request->input('sp_id'))){
              $nestedData['input_el'] = ' ';
            }
             if(!$val['user_id'] && !$val['is_gift'] && ($val['db_user_name'] || $val['db_user_mobile_no'])){
              $db_contact_no = '';
               if($val['db_user_mobile_no']!='' && $val['db_user_mobile_no']!=null){
                $db_contact_no = '</br><b>('.$val['db_user_mobile_no'].')</b>';
               }

              $nestedData['name'] = ucfirst($val['db_user_name']).$db_contact_no;
             
            }
            
            if(!$val['user_id'] && $val['is_gift'] && ($val['receiver_name'] || $val['receiver_mobile_no'])){
              $re_contact_no = '';
               if($val['receiver_mobile_no']!='' && $val['receiver_mobile_no']!=null){
                $re_contact_no = '</br><b>('.$val['receiver_mobile_no'].')</b>';
               }
               
              $nestedData['name'] = ucfirst($val['receiver_name']).$re_contact_no;
            
            }
            $nestedData['sp_id'] = ucfirst($val->getAssociatedSpInformation->store_name).'</br><b>('.$val->getAssociatedSpInformation->mobile_no.')</b>';
            $nestedData['action'] =  getButtons([
                ['key'=>'view_settlement','link'=>route('orders.view-settlement',[$val->booking_unique_id,1])],
              ]); 
            
            $tier = SpUser::leftJoin('tiers', function($join) {
                  $join->on('sp_users.tier_id', '=', 'tiers.id');
                })
            ->where('sp_users.id',$val['sp_id'])
            ->select('tiers.commission')
            ->first();

            $beutics_comm = 0;
            if($val['final_settlement_amount']){
              $disc = ($val['redeemed_reward_points']+$val['promo_code_amount']);
              $nestedData['paid_amount'] = (($val['final_settlement_amount']-$disc)+$val['final_settlement_tax']).' AED';
              $sp_cost = $val['final_settlement_amount'];
              $beutics_comm = round((($val['final_settlement_amount'] * $tier->commission)/100),2);
            } else {
              $sp_cost = $val['total_item_amount'];              
              $beutics_comm = round((($val['total_item_amount'] * $tier->commission)/100),2);
            }

            $sp_before_tax = ($sp_cost-$beutics_comm);
            $sp_tax_pay = 0;
            if($val->getAssociatedSpInformation->is_tax_applied){
              $tax = Setting::select('percentage')->first();
              $sp_tax_pay = round((($sp_before_tax * $tax['percentage'])/100),2);
            }

            $nestedData['beutics_comm'] = round($beutics_comm,2).' AED';
            $nestedData['sp_cost'] = round($sp_cost,2).' AED';
            $nestedData['sp_total_to_pay'] = round($sp_before_tax+$sp_tax_pay, 2).' AED';

            if($val['payment_type'] == '0'){
              $nestedData['beutics_comm'] = '0 AED';
            } else {
              $nestedData['sp_total_to_pay'] = '0 AED';
            }
            $payment_type = ($request->input('payment_type'))?$request->input('payment_type'):'0';
            if($payment_type){
              $nestedData['beutics_comm'] = 'NA';
              $nestedData['sp_total_to_pay'] = 'NA';
            }
            $data[] = $nestedData;
          }
        }
        
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function confirm_settlements(Request $request)
    {
        $data = $request->all();
        if(!empty($data['ids'])){
          $title = 'Confirm Settlement';
          $breadcum = [$title =>''];
          $sp_id = DB::table('bookings')->whereIn('bookings.id',$data['ids'])->first()->sp_id;
          
          $total_amt = DB::table('bookings')->whereIn('bookings.id',$data['ids'])->sum('bookings.booking_total_amount');
          $total_amount = number_format($total_amt, 2).' AED';
          $ids = serialize($data['ids']);    
          $total_sp_cost = $sp_total_to_pay = $total_beutics_commission = $total_sp_total_to_pay = 0;
          if (!empty($ids)) {
            foreach($data['ids'] as $b_id){
              $val = Booking::where('bookings.id',$b_id)->first();

              $tier = SpUser::leftJoin('tiers', function($join) {
                    $join->on('sp_users.tier_id', '=', 'tiers.id');
                  })
              ->where('sp_users.id',$val->sp_id)
              ->select('tiers.commission')
              ->first();

              $beutics_comm = 0;
              if($val['final_settlement_amount']){
                $disc = ($val['redeemed_reward_points']+$val['promo_code_amount']);
                $sp_cost = $val['final_settlement_amount'];
                $beutics_comm = round((($val['final_settlement_amount'] * $tier->commission)/100),2);
              } else {
                $sp_cost = $val['total_item_amount'];              
                $beutics_comm = round((($val['total_item_amount'] * $tier->commission)/100),2);
              }

              $sp_before_tax = ($sp_cost-$beutics_comm);
              $sp_tax_pay = 0;
              if($val->getAssociatedSpInformation->is_tax_applied){
                $tax = Setting::select('percentage')->first();
                $sp_tax_pay = round((($sp_before_tax * $tax['percentage'])/100),2);
              }
              
              $sp_total_to_pay = round($sp_before_tax+$sp_tax_pay, 2);

              if($val['payment_type'] == '0'){
                $beutics_comm = '0';
              } else {
                $sp_total_to_pay = '0';
              }
              
              $total_beutics_commission += round($beutics_comm,2);
              $total_sp_total_to_pay += round($sp_total_to_pay,2);
            }
          }
          $total_sp_cost = ($total_sp_total_to_pay-$total_beutics_commission);
          return view('admin.orders.confirmSettlements',compact('title','row','breadcum', 'ids', 'total_amount', 'total_amt', 'sp_id', 'total_sp_cost', 'sp_total_to_pay'));
        } else {
          Session::flash('danger', 'Please select the orders which you want to mark as Paid.');
          return redirect()->back()->withInput();
        }
    }

  public function save_settlements(Request $request)
  {
    try {
      $data = $request->all();       

      //echo '<pre>'; print_r($data);die;
      $validator = Validator::make($data, 
          [
            'order_ids' => 'required',
            'paid_by' => 'required',
          ]);
        if ($validator->fails()) 
        {            
          $error = $this->validationHandle($validator->messages());
          Session::flash('danger', $error);
          return redirect()->back()->withInput();
        } 
        else 
        { 
          if($data['order_ids']){

            // add data in payment_settlement data.
            $row = new PaymentSettlement();
            $row->sp_id = $data['sp_id'];
            $row->total_amount = $data['total_amount'];
            $row->paid_by = $data['paid_by'];
            $row->payment_mode = $data['payment_mode'];
            $row->payment_date = $data['payment_date'];
            $row->notes = $data['notes'];
            $row->payment_transaction_id = $data['payment_transaction_id'];

            if ($request->hasFile('receipt') && $request->file('receipt'))
            {
                $file = $request->file('receipt');
                $name = time().'.'.$file->getClientOriginalExtension();
                $destinationPath = public_path('/receipts/');
                $img = $file->move($destinationPath, $name);
                $row->receipt = $name;
            } 
           // echo '<pre>'; print_r($row);die;
            $order_ids = unserialize($data['order_ids']);
            if($row->save()){ 
              // add data in payment_Settilement_order data.
              $payment_settlement_id = $row->id;
              foreach($order_ids as $order_id){
                $order_row = '';
                $order_row = new PaymentSettlementOrder();
                $order_row->payment_settlement_id = $payment_settlement_id;
                $order_row->booking_id = $order_id;
                $order_row->save();
              }
              // Update booking table 
              $update_booking = DB::table('bookings')->whereIn('bookings.id', $order_ids)->update(array('bookings.payment_settlement_status' => 'Paid'));

              if($update_booking){
                Session::flash('success', __('Order has been successfully marked as Paid.'));
                return redirect()->route('orders.sp_unpaid_settlements');
              } else {
                Session::flash('danger', __('There is some issue to mark the orders as paid.'));
                return redirect()->back()->withInput();
              }
            }
          }            
        }
     }
     catch(\Exception $e){
       $msg = $e->getMessage();
       Session::flash('danger', $msg);
       return redirect()->back()->withInput();
     }
  }

  public function offline_settlements(Request $request)
  {
    try {
      $data = $request->all();       

      //echo '<pre>'; print_r($data);die;
      $validator = Validator::make($data, 
          [
            'ids' => 'required',
          ]);
        if ($validator->fails()) 
        {            
          $error = $this->validationHandle($validator->messages());
          Session::flash('danger', $error);
          return redirect()->back()->withInput();
        } 
        else 
        { 
          if($data['ids']){
              $order_ids = $data['ids'];
              // Update booking table 
              $update_booking = DB::table('bookings')->whereIn('bookings.id', $order_ids)->update(array('bookings.payment_settlement_status' => 'Paid'));

              if($update_booking){
                Session::flash('success', __('Order has been successfully marked as Paid.'));
                return redirect()->route('orders.sp_unpaid_settlements');
              } else {
                Session::flash('danger', __('There is some issue to mark the orders as paid.'));
                return redirect()->back()->withInput();
              }
            }
          }            
        }
     catch(\Exception $e){
       $msg = $e->getMessage();
       Session::flash('danger', $msg);
       return redirect()->back()->withInput();
     }
  }


    public function view_settlement($id, $flag = ''){
      $title = 'Settlement Details';
      $model = 'orders';
      $order = Booking::where('booking_unique_id',$id)->first();
      if($flag && $flag == 1){
        $breadcum = ['Unpaid Settlements'=>route($model.'.unpaid_settlements', $order['sp_id']),$title =>''];
      } elseif($flag && $flag == 2){
        $breadcum = ['Paid Settlements'=>route($model.'.paid_settlements'),$title =>''];
      }      
      $result = $this->showBookingDetails($order);  
      $settlements = PaymentSettlementOrder::leftJoin('payment_settlements', function($join) {
              $join->on('payment_settlements.id', '=', 'payment_settlement_orders.payment_settlement_id');
            })->where('booking_id',$order['id'])->select('payment_settlement_orders.booking_id','payment_settlement_orders.payment_settlement_id', 'payment_settlements.*')->first();
      if(!empty($settlements)){
        $settlements = $settlements->toArray();
      }
      //echo '<pre>'; print_r($settlements);die;
     return view('admin.orders.viewSettlement',compact('title','result','breadcum', 'settlements'));
    }

  public function paid_settlements()
    {
        $title = 'All Paid Settlements';
        $breadcum = [$title =>''];
        $sp_list  = SpUser::orderby('store_name', 'asc')->pluck('store_name', 'id')->toArray();
        return view('admin.orders.paidSettlements',compact('title','row','breadcum', 'sp_list'));
    }

    public function getDataPaid(Request $request)
    {
       
        $columns = ['id','tax_amount', 'total_amount','paid_amount','created_date','db_user_name','db_user_mobile_no','booking_unique_id','appointment_date','appointment_time','status','service_end_time','Action'];
        $order = $columns[$request->input('order.0.column')];

        $totalData = Booking::count();    
        $limit = $request->input('length');
        $start = $request->input('start');     
        
        $sp_ref = Booking::leftJoin('sp_users', function($join) {
                  $join->on('bookings.sp_id', '=', 'sp_users.id');
                })
                      ->leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                })
                ->whereNotNull('user_id')
                ->where('bookings.status','5')
                ->where('payment_type','1')
                ->where('bookings.payment_settlement_status','unpaid')
                ->whereRaw('sp_users.share_code = users.refer_code')
                //->where('bookings.sp_id',$user_id)
                ->distinct('bookings.user_id')
                    ->pluck('bookings.user_id');

         if(!empty($request->input('sp_id'))) {
            $sp_id = $request->input('sp_id');
            // $settlement_option = SpUser::where('id',$sp_id)->select('settlement_option')->first();

            $sp_ref = Booking::leftJoin('sp_users', function($join) {
                  $join->on('bookings.sp_id', '=', 'sp_users.id');
                })
                      ->leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                })
                ->whereNotNull('user_id')
                ->where('bookings.status','5')
                ->where('payment_type','1')
                ->where('bookings.payment_settlement_status','paid')
                ->whereRaw('sp_users.share_code = users.refer_code')
                ->where('bookings.sp_id',$sp_id)
                ->distinct('bookings.user_id')
                    ->pluck('bookings.user_id');

            $payment_type = ($request->input('payment_type'))?$request->input('payment_type'):'0';
            if($payment_type == 0){
                $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                })
                ->leftJoin('sp_users', function($join) {
                  $join->on('bookings.sp_id', '=', 'sp_users.id');
                })  
                ->leftJoin('payment_settlement_orders', function($join) {
                  $join->on('bookings.id', '=', 'payment_settlement_orders.booking_id');
                })
                ->leftJoin('payment_settlements', function($join) {
                  $join->on('payment_settlements.id', '=', 'payment_settlement_orders.payment_settlement_id');
                })            
                ->where('bookings.sp_id',$sp_id)   
                ->where('bookings.status','5')
                // if($settlement_option['settlement_option']!='billable'){
                //   $row = $row->where('bookings.payment_type','!=','1');
                // }
                ->where('bookings.settlement_option','billable') 
                ->where('bookings.payment_settlement_status','paid')
                ->whereNotIn('bookings.user_id',$sp_ref) 
                ->whereNull('bookings.db_user_name')         
                ->orderBy('bookings.id','desc')
                ->select('bookings.*', 'payment_settlement_orders.*', 'payment_settlements.payment_transaction_id','payment_settlements.paid_by','payment_settlements.payment_date', 'users.name', 'users.mobile_no');

            } else {
              $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                })
                ->leftJoin('sp_users', function($join) {
                  $join->on('bookings.sp_id', '=', 'sp_users.id');
                })
                ->leftJoin('payment_settlement_orders', function($join) {
                  $join->on('bookings.id', '=', 'payment_settlement_orders.booking_id');
                })
                ->leftJoin('payment_settlements', function($join) {
                  $join->on('payment_settlements.id', '=', 'payment_settlement_orders.payment_settlement_id');
                })              
                ->where('bookings.sp_id',$sp_id)   
                ->where('bookings.status','5')
                ->where('bookings.payment_settlement_status','paid')
                
                // if($settlement_option['settlement_option']!='billable'){
                //   $row = $row->where(function ($query) use($sp_ref){     
                //     $query->where(function ($query) use ($sp_ref){  
                //       $query->whereIn('bookings.user_id',$sp_ref) 
                //           ->orWhereNotNull('bookings.db_user_name');   
                //      }) 
                //       ->orWhere(function ($query){ 
                //          $query->where('bookings.payment_type','1');
                //       });          
                                   
                //   });   
                // }else{
                  ->where(function ($query) use($sp_ref){                  
                    $query->whereIn('bookings.user_id',$sp_ref) 
                          ->orWhereNotNull('bookings.db_user_name')
                          ->orWhere('bookings.payment_type','1');                   
                  })
                // }     
                 ->where('bookings.settlement_option','nonbillable')    
                 ->orderBy('bookings.id','desc')
                ->select('bookings.*', 'payment_settlement_orders.*', 'payment_settlements.payment_transaction_id','payment_settlements.paid_by','payment_settlements.payment_date', 'users.name', 'users.mobile_no');
            }
          
        }

        $totalFiltered = 0;
        if (!empty($row)) {
          $data_query_count = $row;
          $totalFiltered = $data_query_count->count();
          $row = $row->offset($start)
                  ->limit($limit)
                  ->orderBy('bookings.id', 'desc')
                  ->get();
        }

        $data = array();
        //echo '<pre>'; print_r($row);die;
        if (!empty($row)) {
            foreach($row as $val){
              $date = $val['appointment_date'];
              $time = $val['appointment_time'];
              $combinedDT = '';
              if($date!='' && $date!=null && $time!=null && $time!=''){
                 $combinedDT = date('M d, Y h:i A', strtotime("$date $time"));
               }else{
                $combinedDT = 'NA';
               }

               $contact_no = '';
               if($val['mobile_no']!='' && $val['mobile_no']!=null){
                $contact_no = '</br><b>('.$val['mobile_no'].')</b>';
               }

            $nestedData = [
              "id" => $val['id'],
              "payment_transaction_id" => $val['payment_transaction_id'],
              "paid_by" => $val['paid_by'],
              "booking_unique_id" => $val['booking_unique_id'],
              "name" => ucfirst($val['name']).$contact_no,
              "appointment_date" => $combinedDT,
              "service_end_time" => date('h:i A', strtotime($val['service_end_time'])),
              //"payment_type" => $val['payment_type']=='0'?'Paid':'To Pay at Store',
              "tax_amount" => round($val['tax_amount'],2),
              "total_amount" => round($val['booking_total_amount'],2),
              "paid_amount" => round($val['paid_amount'],2).' AED',
              "created_date" => date('Y-m-d h:i:A',strtotime($val['created_at'])),
              "payment_date" => ($val['payment_date'])?$val['payment_date']:'',
              "completed_date_time" => $val['completed_date_time']!='' ? date('Y-m-d h:i:A',strtotime($val['completed_date_time'])):'',
            ];
             if(!$val['user_id'] && !$val['is_gift'] && ($val['db_user_name'] || $val['db_user_mobile_no'])){
              $db_contact_no = '';
               if($val['db_user_mobile_no']!='' && $val['db_user_mobile_no']!=null){
                $db_contact_no = '</br><b>('.$val['db_user_mobile_no'].')</b>';
               }

              $nestedData['name'] = ucfirst($val['db_user_name']).$db_contact_no;
             
            }
            
            if(!$val['user_id'] && $val['is_gift'] && ($val['receiver_name'] || $val['receiver_mobile_no'])){
              $re_contact_no = '';
               if($val['receiver_mobile_no']!='' && $val['receiver_mobile_no']!=null){
                $re_contact_no = '</br><b>('.$val['receiver_mobile_no'].')</b>';
               }
               
              $nestedData['name'] = ucfirst($val['receiver_name']).$re_contact_no;
            
            }
            $nestedData['sp_id'] = ucfirst($val->getAssociatedSpInformation->store_name).'</br><b>('.$val->getAssociatedSpInformation->mobile_no.')</b>';
            $nestedData['action'] =  getButtons([
                ['key'=>'view_settlement','link'=>route('orders.view-settlement',[$val['booking_unique_id'],2])],
              ]); 
           
            if($val['final_settlement_amount']){
              $disc = ($val['redeemed_reward_points']+$val['promo_code_amount']);
              $nestedData['paid_amount'] = (($val['final_settlement_amount']-$disc)+$val['final_settlement_tax']).' AED';
            }

            $tier = SpUser::leftJoin('tiers', function($join) {
                    $join->on('sp_users.tier_id', '=', 'tiers.id');
                  })
              ->where('sp_users.id',$val->sp_id)
              ->select('tiers.commission')
              ->first();

             $beutics_comm = 0;

              if($val['final_settlement_amount']){
                $disc = ($val['redeemed_reward_points']+$val['promo_code_amount']);
                $sp_cost = $val['final_settlement_amount'];
                $beutics_comm = round((($val['final_settlement_amount'] * $tier->commission)/100),2);
              } else {
                $sp_cost = $val['total_item_amount'];              
                $beutics_comm = round((($val['total_item_amount'] * $tier->commission)/100),2);
              }

              $sp_before_tax = ($sp_cost-$beutics_comm);
              $sp_tax_pay = 0;
              if($val->getAssociatedSpInformation->is_tax_applied){
                $tax = Setting::select('percentage')->first();
                $sp_tax_pay = round((($sp_before_tax * $tax['percentage'])/100),2);
              }
              
              $sp_total_to_pay = round($sp_before_tax+$sp_tax_pay, 2);

              if($val['payment_type'] == '0'){
                $beutics_comm = '0';
              } else {
                $sp_total_to_pay = '0';
              }
              if($val['payment_type'] == '1'){
                $nestedData['total_sp_cost'] = abs($sp_cost).' AED';
              }else{
                $nestedData['total_sp_cost'] = abs($sp_total_to_pay-$beutics_comm).' AED';
              }      
              

            $data[] = $nestedData;
          }
        }
        
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

  public function transaction_history()
    {
        $title = 'Transaction History';
        $breadcum = [$title =>''];
        $sp_list  = SpUser::orderby('store_name', 'asc')->pluck('store_name', 'id')->toArray();
        return view('admin.orders.transactionHistory',compact('title','row','breadcum', 'sp_list'));
    }

    public function getDataHistory(Request $request)
    {
       
        $columns = ['id','tax_amount', 'total_amount','paid_amount','created_date','db_user_name','db_user_mobile_no','booking_unique_id','appointment_date','appointment_time','status','service_end_time','Action'];
        $order = $columns[$request->input('order.0.column')];

        $totalData = PaymentSettlement::count();    
        $limit = $request->input('length');
        $start = $request->input('start');     
        $row = array();
        $row = PaymentSettlement::leftJoin('sp_users', function($join) {
                  $join->on('payment_settlements.sp_id', '=', 'sp_users.id');
                })
                ->orderBy('payment_settlements.id','desc')
                ->select('payment_settlements.*', 'sp_users.store_name', 'sp_users.mobile_no');
         if(!empty($request->input('sp_id'))) {
            $sp_id = $request->input('sp_id');
            $row = PaymentSettlement::leftJoin('sp_users', function($join) {
                  $join->on('payment_settlements.sp_id', '=', 'sp_users.id');
                })
                ->where('payment_settlements.sp_id',$sp_id)
                ->orderBy('payment_settlements.id','desc')
                ->select('payment_settlements.*', 'sp_users.store_name', 'sp_users.mobile_no');
        }

        if (!empty($request->input('from_appointment_date')) && !empty($request->input('to_appointment_date'))) {
               
            $from_app_date = $request->input('from_appointment_date');
            $to_app_date = $request->input('to_appointment_date');

            $row = $row->where(function($query) use ($from_app_date,$to_app_date) {
                $query->where('payment_settlements.payment_date','>=', $from_app_date)
                ->where('payment_settlements.payment_date','<=', $to_app_date);
            });             
        }

        $totalFiltered = 0;
        if (!empty($row)) {
          $data_query_count = $row;
          $totalFiltered = $data_query_count->count();
          $row = $row->offset($start)
                  ->limit($limit)
                  ->orderBy('payment_settlements.id', 'desc')
                  ->get();
        }

        $data = array();
        //echo '<pre>'; print_r($row);die;
        if (!empty($row)) {
            foreach($row as $val){
            $nestedData = [
              "id" => $val['id'],
              "payment_transaction_id" => $val['payment_transaction_id'],
              "paid_by" => $val['paid_by'],
              "payment_mode" => $val['payment_mode'],
              "total_amount" => number_format($val['total_amount'], 2).' AED',
              "store_name" => $val['store_name'].'</br><b>('.$val['mobile_no'].')</b>',
              "created_at" => $val['created_at']!='' ? date('Y-m-d h:i:A',strtotime($val['created_at'])):'',
              "payment_date" => ($val['payment_date']) ? $val['payment_date']:'',
            ];
            
            $nestedData['action'] =  getButtons([
                ['key'=>'view','link'=>route('orders.view_history',[$val->id])],
              ]); 
            $data[] = $nestedData;
          }
        }
        
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function view_history($id){
      $title = 'Transaction Details';
      $model = 'orders';
      $breadcum = ['Transaction History'=>route($model.'.transaction_history'),$title =>''];
            
      $row = PaymentSettlementOrder::leftJoin('payment_settlements', function($join) {
              $join->on('payment_settlements.id', '=', 'payment_settlement_orders.payment_settlement_id');
            })
            ->leftJoin('bookings', function($join) {
                  $join->on('payment_settlement_orders.booking_id', '=', 'bookings.id');
               })
            ->leftJoin('sp_users', function($join) {
                  $join->on('payment_settlements.sp_id', '=', 'sp_users.id');
                })
             ->leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
              })
            ->where('payment_settlement_orders.payment_settlement_id',$id)
            ->select('users.name','bookings.appointment_date','bookings.appointment_time','bookings.service_end_time','bookings.created_at as b_created_at','bookings.completed_date_time','sp_users.store_name','sp_users.mobile_no','payment_settlement_orders.booking_id','payment_settlement_orders.payment_settlement_id', 'payment_settlements.*')
            ->get()->toArray();
      //echo '<pre>'; print_r($row);
      if(!empty($row)){
        foreach ($row as $key => $value) {
          $date = $value['appointment_date'];
          $time = $value['appointment_time'];
          $combinedDT = '';
          if($date!='' && $date!=null && $time!=null && $time!=''){
             $combinedDT = date('M d, Y h:i A', strtotime("$date $time"));
           }else{
            $combinedDT = 'NA';
           }
          $result['store_name'] = ucfirst($value['store_name']).'</br><b>('.$value['mobile_no'].')</b>';
          $result['payment_transaction_id'] = $value['payment_transaction_id'];
          $result['paid_by'] = $value['paid_by'];
          $result['payment_mode'] = $value['payment_mode'];
          $result['total_amount'] = number_format($value['total_amount'], 2).' AED';
          $result['transaction_date_time'] = $value['created_at'];
          $result['receipt'] = $value['receipt'];
          $result['notes'] = ($value['notes']) ? $value['notes']:' NA';
          $result['payment_date'] = ($value['payment_date']) ? $value['payment_date']:'';
          $result['PaymentSettlementOrder'][$key]['name'] = $value['name'];
          $result['PaymentSettlementOrder'][$key]['appointment_date'] = $combinedDT;
          $result['PaymentSettlementOrder'][$key]['service_end_time'] = $value['service_end_time'];
          $result['PaymentSettlementOrder'][$key]['b_created_at'] = $value['b_created_at']!='' ? date('Y-m-d h:i:A',strtotime($value['b_created_at'])):'';
          $result['PaymentSettlementOrder'][$key]['completed_date_time'] = $value['completed_date_time']!='' ? date('Y-m-d h:i:A',strtotime($value['completed_date_time'])):'';
          $result['PaymentSettlementOrder'][$key]['action'] =  getButtons([
                ['key'=>'view','link'=>route('orders.view',[$value['booking_id'],3])],
              ]); 
        }
      }
      //echo '<pre>'; print_r($result);die;
     return view('admin.orders.viewHistory',compact('title','result','breadcum'));
    }


    public function aesthetic_orders()
    {
        $title = 'All Aesthetic Orders';
        $breadcum = [$title =>''];
        $sp_list  = SpUser::where('category_id', '4')->orderby('store_name', 'asc')->pluck('store_name', 'id')->toArray();
        return view('admin.orders.aestheticOrders',compact('title','row','breadcum', 'sp_list'));
    }

    public function getAestheticData(Request $request)
    {
       
        $columns = ['id','tax_amount', 'total_amount','paid_amount','created_date','db_user_name','db_user_mobile_no','booking_unique_id','appointment_date','appointment_time','status','service_end_time','Action'];
        $order = $columns[$request->input('order.0.column')];

        $totalData = Booking::leftJoin('sp_users', function($join) {
                  $join->on('bookings.sp_id', '=', 'sp_users.id');
                })->where('sp_users.category_id','4')->count();    
        $limit = $request->input('length');
        $start = $request->input('start');     
        
         if(!empty($request->input('sp_id'))) {
            $sp_id = $request->input('sp_id');
            $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                })
                ->leftJoin('sp_users', function($join) {
                  $join->on('bookings.sp_id', '=', 'sp_users.id');
                })             
                ->where('bookings.sp_id',$sp_id)
                ->where('sp_users.category_id','4')
                ->where('bookings.status','5')
                ->where('bookings.payment_settlement_status',null)
                ->orderBy('bookings.id','desc')
                ->select('bookings.*', 'users.name', 'users.mobile_no');
        } else {
          $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                })
                ->leftJoin('sp_users', function($join) {
                  $join->on('bookings.sp_id', '=', 'sp_users.id');
                })             
                ->where('bookings.status','5')
                ->where('sp_users.category_id','4')
                ->where('bookings.payment_settlement_status',null)
                ->orderBy('bookings.id','desc')
                ->select('bookings.*', 'users.name', 'users.mobile_no');
        }
        $totalFiltered = 0;
        if (!empty($row)) {
          $data_query_count = $row;
          $totalFiltered = $data_query_count->count();
          $row = $row->offset($start)
                  ->limit($limit)
                  ->orderBy('bookings.id', 'desc')
                  ->get();
        }

        $data = array();

        if (!empty($row)) {
            foreach($row as $val){
              $date = $val['appointment_date'];
              $time = $val['appointment_time'];
              $combinedDT = '';
              if($date!='' && $date!=null && $time!=null && $time!=''){
                 $combinedDT = date('M d, Y h:i A', strtotime("$date $time"));
               }else{
                $combinedDT = 'NA';
               }

               $contact_no = '';
               if($val['mobile_no']!='' && $val['mobile_no']!=null){
                $contact_no = '</br><b>('.$val['mobile_no'].')</b>';
               }

            $nestedData = [
              "id" => $val['id'],
              "booking_unique_id" => $val['booking_unique_id'],
              "name" => ucfirst($val['name']).$contact_no,
              "appointment_date" => $combinedDT,
              "service_end_time" => date('h:i A', strtotime($val['service_end_time'])),
              "tax_amount" => round($val['tax_amount'],2),
              "total_amount" => round($val['booking_total_amount'],2),
              "paid_amount" => round($val['paid_amount'],2),
              "created_date" => date('Y-m-d h:i:A',strtotime($val['created_at'])),
              "completed_date_time" => $val['completed_date_time']!='' ? date('Y-m-d h:i:A',strtotime($val['completed_date_time'])):'',
            ];
             if(!$val['user_id'] && !$val['is_gift'] && ($val['db_user_name'] || $val['db_user_mobile_no'])){
              $db_contact_no = '';
               if($val['db_user_mobile_no']!='' && $val['db_user_mobile_no']!=null){
                $db_contact_no = '</br><b>('.$val['db_user_mobile_no'].')</b>';
               }

              $nestedData['name'] = ucfirst($val['db_user_name']).$db_contact_no;
             
            }
            
            if(!$val['user_id'] && $val['is_gift'] && ($val['receiver_name'] || $val['receiver_mobile_no'])){
              $re_contact_no = '';
               if($val['receiver_mobile_no']!='' && $val['receiver_mobile_no']!=null){
                $re_contact_no = '</br><b>('.$val['receiver_mobile_no'].')</b>';
               }
               
              $nestedData['name'] = ucfirst($val['receiver_name']).$re_contact_no;
            
            }
            $nestedData['sp_id'] = ucfirst($val->getAssociatedSpInformation->store_name).'</br><b>('.$val->getAssociatedSpInformation->mobile_no.')</b>';
            $nestedData['action'] =  getButtons([
                ['key'=>'view','link'=>route('orders.view_aesthetic',[$val->id])],
              ]); 
            $data[] = $nestedData;
          }
        }
        
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }


    public function view_aesthetic($id){
      $title = 'Aesthetic Order Details';
      $model = 'orders';
      $breadcum = ['Orders'=>route($model.'.aesthetic_orders'),$title =>''];
      
      $order = Booking::where('id',$id)->first();
      $result = $this->showBookingDetails($order);  
      //echo '<pre>'; print_r($result);die;
     return view('admin.orders.viewAesthetic',compact('title','result','breadcum'));
    }


  public function save_aesthetic(Request $request)
  {
    try {
      $data = $request->all();      

     // echo '<pre>'; print_r($data);//die;
      $success = 0;
      if(!empty($data['final_amount'])){
        // add data in payment_settlement data.
        $rows = Booking::where('id',$data['id'])->first();  

        $final_amount = $data['final_amount'];

        if($rows->redeemed_reward_points){
          $final_amount = ($final_amount-$rows->redeemed_reward_points);
        }       
        if($rows->promo_code_amount){
          $final_amount = ($final_amount-$rows->promo_code_amount);
        }

        $tax = Setting::select('percentage')->first();
        $tax_amount = ($final_amount * $tax['percentage'])/100;

        $tier = SpUser::leftJoin('tiers', function($join) {
                  $join->on('sp_users.tier_id', '=', 'tiers.id');
                })
            ->where('sp_users.id',$data['sp_id'])
            ->select('tiers.earning')
            ->first();

        $total_cashback = round(($final_amount*$tier->earning)/100,0);   

        $rows->final_settlement_amount = $data['final_amount'];
        $rows->final_settlement_tax = $tax_amount;
        $rows->final_settlement_cashback = $total_cashback;
        $rows->payment_settlement_status = 'unpaid';
        //echo '<pre>'; print_r($rows);die;
        if($rows->save()){
          $user = User::where('id',$rows->user_id)->first();
          if($user->earning!=0 && $user->earning!=null){
            $total_earning = $total_cashback+$user->earning;
          }else{
            $total_earning = $total_cashback;
          }

          $user->earning = round($total_earning,2);
          $user->save();

          $cashback = new CashbackHistory();
          $cashback->user_id = $rows->user_id;
          $cashback->booking_id = $rows->id;
          $cashback->type = '0';
          $cashback->amount =round($total_cashback,2);
          if($cashback->save()){
            if($user->notification_alert_status == '1'){

              // $message = 'Hey! Just to let you know, I have credited your cashback earning of AED '.round($total_cashback,2).' onto your wallet for your recent purchase {Order ID: '.$rows->booking_unique_id.'} .';

              $message_main = __('messages.Cashback Credited',['cashback' => round($total_cashback,2),'order_id'=>$rows->booking_unique_id]);
              $message_title_main = __('messages.Cashback Credited!');

              app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');


              $message_other = __('messages.Cashback Credited',['cashback' => round($total_cashback,2),'order_id'=>$rows->booking_unique_id]);
              $message_title_other = __('messages.Cashback Credited!');

              
              Notification::saveNotification($rows->user_id,$rows->sp_id,$rows->id,'CASHBACK_CREDITED',$message_main,$message_other,$message_title_main,$message_title_other,'customer');
              app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

              
            }
          } 
          $success = 1;
        }
      } else {
        //echo '<pre>'; print_r($data);die;
        // Update booking table 
        $update_booking = DB::table('bookings')->where('bookings.id', $data['id'])->update(array('bookings.payment_settlement_status' => 'unpaid'));
        if($update_booking){
          $success = 1;
        }
      }

      if($success){
        Session::flash('success', __('Order has been successfully updated.'));
        return redirect()->route('orders.aesthetic_orders');
      } else {
        Session::flash('danger', __('There is some issue to update the order.'));
        return redirect()->back()->withInput();
      }

     }
     catch(\Exception $e){
       $msg = $e->getMessage();
       Session::flash('danger', $msg);
       return redirect()->back()->withInput();
     }
  }



    public function orderChatHistory($id, $flag = '')
    {
        $title = 'Order Chat History';
        $model = 'orders';
        $data = array();
        if($flag && $flag == 1){
          $breadcum = ['Orders'=>route($model.'.confirm'),$title =>''];
        } elseif($flag && $flag == 2){
          $breadcum = ['Orders'=>route($model.'.complete'),$title =>''];
        } elseif($flag && $flag == 3){
          $breadcum = ['Orders'=>route($model.'.cancelled'),$title =>''];
        } elseif($flag && $flag == 4){
          $breadcum = ['Orders'=>route($model.'.unpaid_settlements'),$title =>''];
        } elseif($flag && $flag == 5){
          $breadcum = ['Orders'=>route($model.'.paid_settlements'),$title =>''];
        } else {
          $breadcum = ['Orders'=>route($model.'.new'),$title =>''];
        }      

        $chats = Chat::where("booking_id",$id)->get()->toArray(); 
        //echo '<pre>'; print_r($chats); 
        if($chats){
          foreach($chats as $chat){
            $nestedData = array();
            if($chat['user_id_tbl'] == 'users'){
              $user = User::where('id', $chat['user_id'])->first();
              $nestedData['name'] = $user['name'].'<br><small>(Customer)</small>';
            } else if($chat['user_id_tbl'] == 'sp_users'){
              $spuser = SpUser::where('id', $chat['user_id'])->first();
              $nestedData['name'] = $spuser['store_name'];
            }

            $nestedData['user_id_tbl'] = $chat['user_id_tbl'];
            $nestedData['message'] = $chat['message'];
            $nestedData['user_time'] = $chat['user_time'];
            $data[] = $nestedData;
          }
        }
        //echo '<pre>'; print_r($data);die; 
        return view('admin.orders.orderChatHistory',compact('title','model','breadcum','data')); 
    }


    public function giftOrders()
    {
        $title = 'All Pending Gifted Orders';
        $breadcum = [$title =>''];
        return view('admin.orders.giftOrders',compact('title','row','breadcum'));
    }

    public function getGiftData(Request $request)
    {
        $columns = ['name','sp_id', 'appointment_date','payment_type','created_date','action'];
        $order = $columns[$request->input('order.0.column')];

        $totalData = Booking::count();    
        $limit = $request->input('length');
        $start = $request->input('start');     

        $status_arr = array('0'=>'Awaiting','1'=>'Confirmed','2'=>'Cancelled','3'=>'Refunded','4'=>'Expired','5'=>'Completed');
        
        if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                })
                ->leftJoin('sp_users', function($join) {
                  $join->on('bookings.sp_id', '=', 'sp_users.id');
                })
                ->where(function ($query) use ($search){
                        $query->orWhere('users.name', 'like', '%' .$search . '%')
                        ->orWhere('sp_users.store_name', 'like', '%' .$search . '%')
                        ->orWhere('booking_unique_id', '=', $search)
                        ->orWhere('bookings.receiver_name', '=', $search)
                        ->orWhere('users.mobile_no', 'like', '%' .$search . '%');
                    })
                
                ->select('bookings.*', 'users.name', 'users.mobile_no')
                ->where('bookings.status','0')
                ->where('bookings.is_gift','1')
                ->whereNull('bookings.user_id')
                ->orderBy('bookings.id','desc');
        } else {
          $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                }) 
          ->select('bookings.*', 'users.name', 'users.mobile_no')
            ->whereNull('bookings.user_id')
            ->where('bookings.is_gift','1')
            ->where('bookings.status','0');
        }
       
        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('bookings.id', 'desc')
                ->get();
      
        $data = array();

        if (!empty($row)) {
            foreach($row as $val){
               $contact_no = '';
               if($val['mobile_no']!='' && $val['mobile_no']!=null){
                $contact_no = '</br><b>('.$val['mobile_no'].')</b>';
               }
            $nestedData = [
              "id" => $val['id'],
              "booking_unique_id" => $val['booking_unique_id'],
              "name" => ucfirst($val['name']).$contact_no,
              "payment_type" => $val['payment_type']=='0'?'Paid':'To Pay at Store',
              
              "tax_amount" => round($val['tax_amount'],2),
              "total_amount" => round($val['booking_total_amount'],2),
              "paid_amount" => round($val['paid_amount'],2),
              "created_date" => date('Y-m-d h:i:A',strtotime($val['created_at'])),
              "status" => $status_arr[$val['status']],
            ];
       
             if(!$val['user_id'] && !$val['is_gift'] && ($val['db_user_name'] || $val['db_user_mobile_no'])){
              $db_contact_no = '';
               if($val['db_user_mobile_no']!='' && $val['db_user_mobile_no']!=null){
                $db_contact_no = '</br><b>('.$val['db_user_mobile_no'].')</b>';
               }

              $nestedData['name'] = ucfirst($val['db_user_name']).$db_contact_no;
             
            }
            
            if(!$val['user_id'] && $val['is_gift'] && ($val['receiver_name'] || $val['receiver_mobile_no'])){
              $re_contact_no = '';
               if($val['receiver_mobile_no']!='' && $val['receiver_mobile_no']!=null){
                $re_contact_no = '</br><b>('.$val['receiver_mobile_no'].')</b>';
               }
               
              $nestedData['name'] = ucfirst($val['receiver_name']).$re_contact_no;
            
            }

            $nestedData['sp_id'] = ucfirst($val->getAssociatedSpInformation->store_name).'</br><b>('.$val->getAssociatedSpInformation->mobile_no.')</b>';
           
            $nestedData['action'] =  getButtons([
                            ['key'=>'view','link'=>route('orders.view',[$val->id,''])],
                            ['key'=>'cancel','link'=>route('orders.cancelled_order_view',[$val->id, ''])]
                                                                                    
                        ]); 
            $data[] = $nestedData;
          }

        }
        
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

}
