<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;   

use App\Model\Tag;
use datatables;
use App;
use Session;

class TagController extends Controller
{
    protected $title;
    protected $model;
    protected $module;

    public function __construct()
    {
        $this->title = 'Tag';
        $this->model = 'tag';
        $this->module = 'tag';
    } 

    public function index()
    {
        $title = 'Tag';
        $module = $this->module;
        $model = $this->model;
        $breadcum = [$title=>'Tag'];        
        return view('admin.tag.index',compact('title','model','module','breadcum'));
    }

    public function getData(Request $request)
    {
        
        $columns = ['tag_name','tag_name_ar','created'];
        $totalData = Tag::count();
        
        
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        $row = Tag::select("tags.*");
        
        if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = $row->Where('name', 'LIKE', "%{$search}%");
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('tags.id', 'ASC')
                ->get();
      
        $data = array();
        if (!empty($row)) {
            foreach ($row as $key => $rows) {
                $nestedData['tag_name'] = $rows->tag_name;
                $nestedData['tag_name_ar'] = $rows->tag_name_ar;
                $nestedData['created'] = date('Y-m-d h:i A', strtotime($rows->created_at));
                $nestedData['action'] =  getButtons([
                            ['key'=>'edit','link'=>route('tag.edit',$rows->id)],
                            ['key'=>'delete_assoc','link'=>route('tag.destroy',$rows->id), 'asso'=>''],
                        ]);
                
                $data[] = $nestedData;
            }

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }
  
    public function create()
    {
        $action = "create";
        $title = 'Create Tag';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        return view('admin.tag.create',compact('action','title','breadcum','model'));
    }

    
    public function store(Request $request)
    {
        try{
             if (Input::isMethod('post')) {
                $validation = array(
                    'tag_name'=>'required',
                    'tag_name_ar'=>'required',
                );
                $validator = Validator::make(Input::all(), $validation);

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {
                    $row = new Tag();
                    $row['tag_name'] = $request->tag_name;
                    $row['tag_name_ar'] = $request->tag_name_ar;
                    if($row->save())
                    {
                        Session::flash('success', __('Tag added successfully.'));
                        return redirect()->route('tag.index');
                    }                    
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

     
    public function edit($id)
    {
        $rows = Tag::where('id',$id)->first();
        $title = 'Edit Tag';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        return view('admin.tag.edit',compact('rows','title','breadcum'));
    }

   
    public function update(Request $request, $id)
    {
        try{
             if (Input::isMethod('PATCH')) {
                $validation = array(
                    'tag_name'=>'required',
                    'tag_name_ar'=>'required',
                );

                $validator = Validator::make(Input::all(), $validation);

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {
                    $row = Tag::where('id',$id)->first();
                    $row['tag_name'] = $request->tag_name;
                    $row['tag_name_ar'] = $request->tag_name_ar;

                    if($row->save())
                    {
                        Session::flash('success', __('Tag updated successfully.'));
                        return redirect()->route('tag.index');
                    }
                    
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

  
    public function destroy($id)
    {
        $row = Tag::where('id', $id)->first();
        if ($row) {
            $row->delete();
             Session::flash('success', __('Tag deleted successfully.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('Invalid request.'));
            return redirect()->back();
        }
    }
}
