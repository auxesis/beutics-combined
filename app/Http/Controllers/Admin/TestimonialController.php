<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;   

use App\Model\Testimonial;
use datatables;
use App;
use Session;
use DB;

class TestimonialController extends Controller
{
    protected $title;
    protected $model;
    protected $module;

    public function __construct()
    {
        $this->title = 'Testimonial';
        $this->model = 'testimonial';
        $this->module = 'testimonial';
    } 

    public function index()
    {
        $title = __('messages.View Testimonial');
        $module = $this->module;
        $model = $this->model;

        $breadcum = [$title=> $model.'.index'];
        return view('admin.testimonial.index',compact('title','model','module','breadcum'));
    }

    public function getData(Request $request)
    {
        $spid = $request->input('spid');//helper function
        $columns = ['name','designation','email','image'];
        
        $totalData = Testimonial::where('testimonials.status','1')->count();
            
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        $row = Testimonial::select('testimonials.*')->where('testimonials.status','1');

        if (!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = $row->where(function($query) use ($search) {
                    $query->Where('name', 'LIKE', "%{$search}%")
                    ->orWhere('designation', 'LIKE', "%{$search}%");
            });
        }
        //echo $row->toSql(); exit;
        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('id', 'desc')
                ->get();

        $data = array();
        if (!empty($row)) {
            foreach ($row as $key => $rows) 
            {
                $nestedData['name'] = $rows->name;
                $nestedData['designation'] = $rows->designation;
                $nestedData['email'] = $rows->email;
                $nestedData['image'] = '<img src="'.changeImageUrlForFileExist(asset("sp_uploads/testimonial".'/'.$rows->image)).'" width="60" height="50"/>';               
                $nestedData['action'] =  getButtons([
                                ['key'=>'edit','link'=>route('testimonial.edit',[$rows->id])],
                                ['key'=>'delete','link'=>route('testimonial.destroy',$rows->id)],
                            ]);
                
                $data[] = $nestedData;
            }

        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }
  
    public function create()
    {
        $action = "create";
        $title = __('messages.Create Testimonial');
        $model = $this->model;
        $breadcum = [__('messages.Testimonial')=>route($model.'.index'),$title =>''];
        return view('admin.testimonial.create',compact('action','title','breadcum','model'));
    }
    
    public function store(Request $request)
    {
        try{
             if (Input::isMethod('post')) {
                $validation = array(
                    'name' => 'required',
                    'designation'=>'required',
                    'email' => 'required',
                    'image' => 'required',
                );

                $desc = explode(' ',$request->description);
                $validator = Validator::make(Input::all(), $validation);

                if(count($desc)>500){
                     $validator -> errors() -> add('description', 'Description cannot be more than 500 words.');
                     return redirect()->back()
                    -> withErrors($validator)
                    -> withInput(Input::all());
                }

                /*if(!isValidYoutubeUrl($request->email))
                {
                    $validator -> errors() -> add('email', 'Please Enter Valid Youtube Url.');
                     return redirect()->back()
                    -> withErrors($validator)
                    -> withInput(Input::all());
                }*/

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {
                    
                    $row = new Testimonial();
                    if ($request->hasFile("image"))
                    {
                        $file = $request->image;
                        $name = time().str_random(2).'.'.$file->getClientOriginalExtension().'.png';
                        $destinationPath = public_path('/sp_uploads/testimonial/');
                        $img = $file->move($destinationPath, $name);
                        $row->image = $name;
                    }

                    $row->name = ucfirst($request->name);
                    $row->designation = ucfirst($request->designation);
                    $row->description = ucfirst($request->description);
                    $row->email = $request->email;
                    $row->save();

                    if($row->save())
                    {
                        Session::flash('success', __('messages.Testimonial added successfully.'));
                        return redirect()->route('testimonial.index');
                    }
                    
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

   
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        $rows = Testimonial::where('id',$id)->first();
        $title = __('messages.Edit Testimonial');
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        return view('admin.testimonial.edit',compact('rows','title','breadcum'));
    }

   
    public function update(Request $request, $id)
    {   //dd($request); exit;
        try{
             if (Input::isMethod('PATCH')) {
                $validation = array(
                    'name' => 'required',
                    'designation'=>'required',
                    'email' => 'required',
                );

                $desc = explode(' ',$request->description);
                $validator = Validator::make(Input::all(), $validation);

                if(count($desc)>500){
                     $validator -> errors() -> add('description', 'Description cannot be more than 500 words.');
                     return redirect()->back()
                    -> withErrors($validator)
                    -> withInput(Input::all());
                }

                /*if(!isValidYoutubeUrl($request->email))
                {
                    $validator -> errors() -> add('email', 'Please Enter Valid Youtube Url.');
                     return redirect()->back()
                    -> withErrors($validator)
                    -> withInput(Input::all());
                }*/

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {
                    $row = Testimonial::where('id',$id)->first();
                    
                    if ($request->hasFile("image"))
                    {
                        $file = $request->image;
                        $name = time().str_random(2).'.'.$file->getClientOriginalExtension().'.png';
                        $destinationPath = public_path('/sp_uploads/testimonial/');
                        $img = $file->move($destinationPath, $name);
                        $row->image = $name;
                    }

                    $row->name = ucfirst($request->name);
                    $row->designation = ucfirst($request->designation);
                    $row->description = ucfirst($request->description);
                    $row->email = $request->email;
                    $row->save();
                   
                    if($row->save())
                    {
                        Session::flash('success', __('messages.Testimonial updated successfully.'));
                        return redirect()->route('testimonial.index');
                    }
                    
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

  
    public function destroy($id)
    {
        $row = Testimonial::where('id', $id)->first();
        if ($row) {
            $row->delete();
             Session::flash('success', __('messages.Testimonial deleted successfully.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('messages.Invalid request.'));
            return redirect()->back();
        }
    }
}
