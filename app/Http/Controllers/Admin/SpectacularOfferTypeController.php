<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;   

use App\Model\SpectacularOfferType;
use App\Model\SpOffer;
use datatables;
use App;
use Session;

class SpectacularOfferTypeController extends Controller
{
    protected $title;
    protected $model;
    protected $module;

    public function __construct()
    {
        $this->title = 'Spectacular Offers Type';
        $this->model = 'spectacularOffer';
        $this->module = 'spectacularOfferType';
    } 

    public function index()
    {
        $title = $this->title;
        $module = $this->module;
        $model = $this->model;
        $breadcum = [$title=>route($model.'.index')];
       
        return view('admin.spectacularOfferType.index',compact('title','model','module','breadcum'));
    }

    public function getData(Request $request)
    {

        $columns = ['title','title_ar','created_at','action'];
        $totalData = SpectacularOfferType::count();
        
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        $row = new SpectacularOfferType();
        // $totalFiltered = User::count();

        if (!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = $row->where(function($query) use ($search) {
                $query->where('title', 'LIKE', "%{$search}%")
                        ->orWhere('created_at', 'LIKE', "%{$search}%");
            });
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('id', 'DESC')
                ->get();
        // print_r($users);die;
        $data = array();
        if (!empty($row)) {
            foreach ($row as $key => $rows) {
                $nestedData['title'] = $rows->title;
                $nestedData['title_ar'] = $rows->title_ar;
                $nestedData['created_at'] = date('d-M-y h:i:s',strtotime($rows->created_at));
                $associatedTables = SpOffer::where('offer_id',$rows->id)->count();
                $nestedData['action'] =  getButtons([
                                ['key'=>'edit','link'=>route('spectacularOffer.edit',$rows->id)],
                                ['key'=>'delete_assoc','link'=>route('spectacularOffer.destroy',$rows->id), 'asso'=>$associatedTables],
                            ]);
                
                $data[] = $nestedData;
            }

        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

  
    public function create()
    {
        $action = "create";
        $title = 'Create Offer Type';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];

        return view('admin.spectacularOfferType.create',compact('action','title','breadcum','model','module'));
    }

    public function store(Request $request)
    {
        try{
            $validation = array(
                'title' => 'required',
                'title_ar' => 'required',
            );

            $validator = Validator::make(Input::all(), $validation);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
            } 
            else {
                $row = new SpectacularOfferType();
                
                $row->title = $request->title;
                $row->title_ar = $request->title_ar;

                $row->save();
                
                Session::flash('success', __('Spectacular Offer Type added successfully.'));
                return redirect()->route('spectacularOffer.index');
            }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

   
    public function show($id)
    {
        //
    }

 
    public function edit($id)
    {
        $rows = SpectacularOfferType::where('id',$id)->first();
        $title = 'Edit Offer Type';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        return view('admin.spectacularOfferType.edit',compact('rows','title','breadcum'));
    }

  
    public function update(Request $request, $id)
    {
        try{
            $validation = array(
                'title' => 'required',
                'title_ar' => 'required',
            );

            $validator = Validator::make(Input::all(), $validation);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
            } 
            else {
                $row = SpectacularOfferType::where('id',$id)->first();
                $row->title = $request->title;
                $row->title_ar = $request->title_ar;
                $row->save();
                
                Session::flash('success', __('Spectacular Offer Type Updated successfully.'));
                return redirect()->route('spectacularOffer.index');
            }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

    public function destroy($id)
    {
        $row  = SpectacularOfferType::whereId($id)->first();

        if($row){
           $row->delete();
           Session::flash('success', __('Spectacular Offer Type Deleted successfully.'));
           return redirect()->route('spectacularOffer.index');
        }
    }
}
