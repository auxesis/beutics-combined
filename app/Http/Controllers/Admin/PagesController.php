<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use App\Model\User;
use App\Model\SpUser;
use App\Model\Staff;
use App\Model\SpOffer;
use App\Model\Booking;
use App\Model\Review;
use App\Model\ECard;
use DB;
class PagesController extends Controller
{
    public function dashboard()
    {           
        $title = 'Dashboard';
        $user_count = User::count();
        $sp_count = SpUser::where('status','1')->count();
        $sp_staff = Staff::count();
        $sp_pending_req = SpUser::where('is_approved','0')->count();
        $sp_pending_offer_req =  SpOffer::where('status','0')->where('is_deleted','!=','1')->where('expiry_date','>=',date('Y-m-d'))->count();
        $new_bookings = Booking::where('status','0')->where('user_id','!=','')->count();//new orders
        $confirmed_bookings = Booking::where('status','1')->count();//confirmed orders
        $completed_bookings = Booking::where('status','5')->count();//completed orders
        $expired_bookings = Booking::where('status','4')->count();//expired orders
        $cancelled_bookings = Booking::where('status','2')->count();//cancelled orders

        $review_count = Review::where('rate_beutics','!=','')->count(); // Reviews rows count
        //$review_sum = Review::sum('rate_beutics'); // Reviews rows count
        $review_sum = \DB::table("reviews")->selectRaw(
                    'SUM(CASE 
                        WHEN reviews.rate_beutics = "1" THEN "2.5" 
                        WHEN reviews.rate_beutics = "2" THEN "5" 
                        WHEN reviews.rate_beutics = "3" THEN "7.5" 
                        WHEN reviews.rate_beutics = "4" THEN "10" 
                        ELSE "0" 
                        END) AS status_lable')->first();
        // echo '<pre>'; print_r($review_sum);die;
        if($review_sum->status_lable!=0){
            $avg_review = round(($review_sum->status_lable/$review_count),1);
        }else{
            $avg_review = 0;
        }
        
        $total_ecards = ECard::count();//confirmed orders

        $user_detail = loginData();
        return view('admin.pages.dashboard',compact('user_detail','title','user_count','sp_count','sp_staff','sp_pending_req','sp_pending_offer_req','sp_pending_offer_req','new_bookings','confirmed_bookings','completed_bookings','expired_bookings','cancelled_bookings', 'avg_review','total_ecards'));
    }
}
