<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;   

use App\Model\Amenity;
use App\Model\SpAmenity;
use App\Model\SpOffer;
use App\Model\SpOfferService;
use App\Model\SpService;
use App\Model\SpServicePrice;
use App\Model\Booking;
use App\Model\User;
use App\Model\SpUser;
use datatables;
use App;
use Session;

class AmenityController extends Controller
{
    
    protected $title;
    protected $model;
    protected $module;

    public function __construct()
    {
        $this->title = 'Amenities';
        $this->model = 'amenity';
        $this->module = 'amenity';
    } 

    public function index()
    {
//         $rows = Booking::get();
// // print_r($rows);die;
//         $aa = array();
//         foreach($rows as $row){

//             $sp_referred_customer = 0;
//             $user_info = User::where('id',$row->user_id)->first();          
//             $settlement_option = SpUser::where('id',$row->sp_id)->first(); 

//             if($row->user_id!=null && $row->user_id!=""){
//                 // echo $user_info->refer_code.'------';
//                 // echo $settlement_option->share_code.'<br/>';
//                 if($user_info->refer_code == $settlement_option->share_code && $user_info->refer_code !=null && $settlement_option->share_code !=null){
//                     $sp_referred_customer = 1;
//                     // $aa[] = $row->id;
//                 }
//             }
             
            

//             if($row->payment_type == '1'){
//                 $row->settlement_option = $settlement_option['settlement_option'];
//             }

//             if($sp_referred_customer == '1' || $row->db_user_name != null){
//                 $row->settlement_option = 'nonbillable';
//             }
//             $row->save();

//         }
//        dd('success');
        // $rows = SpServicePrice::where('field_key', 'like', '%_ORIGINALPRICE')->where('price',0.00)->pluck('sp_service_id');
        // print_r($rows);die;
       
//        SpService::whereIn('id',$rows)->update(array('is_deleted'=>'1'));
//        $roww = SpService::where('is_deleted','1')->get()->toArray();
// print_r($roww);die;
       
// print_r($roww);die;
//         $not_avai_service_offer = $delete_sevice_offer = [];

//         $sp_offers = SpOffer::where('is_deleted','!=','1')
//         //->where('id',44)  
//         ->get();
//         foreach($sp_offers as $offer_key => $offer_row) {
//             $gender_upp = strtoupper($offer_row->gender);
//             $service_type = $offer_row->service_type;
//             if($service_type == 'home'){
//                 $find_string = $gender_upp.'_HOME_ORIGINALPRICE';
//             } else {
//                 $find_string = $gender_upp.'_ORIGINALPRICE';
//             }
//             $offer_id = $offer_row->id;
//             $offer_services = SpOfferService::where('sp_offer_id', $offer_id)->get();
//             if(!empty($offer_services)) {

//                 foreach($offer_services as $offer_service_key => $offer_service_row) {
//                     //print_r($offer_service_row); die;
//                     $service_id = $offer_service_row->sp_service_id;
//                     $no_sr = SpService::where('id',$service_id)->where('is_deleted','1')->first();
//                     if(empty($no_sr)){
//                         $sp_service_prices = SpServicePrice::where('sp_service_id', $service_id)
//                         ->where('field_key', '=', $find_string)->where('price','!=',0.00)
//                         //->toSql();
//                         ->first();
//                         if(empty($sp_service_prices)) {
//                             $not_avai_service_offer[$offer_id][] = $service_id;
//                         }
//                     }else{
//                         //$delete_sevice_offer[$offer_id] 
//                         $not_avai_service_offer[$offer_id][] = $service_id;
//                     }
                    

                    
//                 }
//             } else {
//                 $not_avai_service_offer[$offer_id] = '';
//             }
//         }
        
// print_r($not_avai_service_offer); die;
 // SpOffer::whereIn('id',array_keys($not_avai_service_offer))->update(array('is_deleted'=>'1'));
 //        print_r(array_keys($not_avai_service_offer)); 
 //        die;

//         $data1['templete'] = "admin_mail";
//         $data1['email'] = 'contact@beutics.com';
//         $data1['subject'] = "Test Email- 2";
//         $data1['message'] = "A new Service Provider  has requested to join the platform. Kindly review and action the registration.";
//         send($data1);
// die("done");
        $title = 'Amenities';
        $module = $this->module;
        $model = $this->model;
        $breadcum = [$title=>route($model.'.index')];
        return view('admin.amenity.index',compact('title','model','module','breadcum'));
    }

    public function getData(Request $request)
    {

        $columns = ['name','name_ar','created_at','action'];
        $totalData = Amenity::count();
        
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        $row = new Amenity();
        // $totalFiltered = User::count();

        if (!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = $row->where(function($query) use ($search) {
                $query->where('name', 'LIKE', "%{$search}%")
                        ->orWhere('created_at', 'LIKE', "%{$search}%");
            });
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                // ->withTrashed()
                ->get();
        // print_r($users);die;
        $data = array();
        if (!empty($row)) {
            foreach ($row as $key => $rows) {

                $nestedData['name'] = $rows->name;
                $nestedData['name_ar'] = $rows->name_ar;
                $associatedAmenity = SpAmenity::where('amenity_id',$rows->id)->count();
                $nestedData['created_at'] = date('d-M-y h:i:s',strtotime($rows->created_at));
                $nestedData['action'] =  getButtons([
                                ['key'=>'edit','link'=>route('amenity.edit',$rows->id)],
                                ['key'=>'delete_assoc','link'=>route('amenity.destroy',$rows->id), 'asso'=>$associatedAmenity],
                            ]);
                
                $data[] = $nestedData;
            }

        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

   
    public function create()
    {
        $action = "create";
        $title = 'Create Amenity';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];

        return view('admin.amenity.create',compact('action','title','breadcum','model','module'));
    }

  
    public function store(Request $request)
    {
        try{
             if (Input::isMethod('post')) {
                $validation = array(
                    'name' => 'required',
                    'name_ar' => 'required',
                    'image' => 'required|mimes:png',
                );

                $validator = Validator::make(Input::all(), $validation);

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {
                    $row = new Amenity();
                    
                    $row->name = $request->name;
                    $row->name_ar = $request->name_ar;

                    if ($request->hasFile('image') && $request->file('image'))
                    {
                        $file = $request->file('image');

                        $name = time().'.'.$file->getClientOriginalExtension();
                        $destinationPath = public_path('/sp_uploads/amenities/');
                        $img = $file->move($destinationPath, $name);

                        $row->image = $name;
                    } 

                    $row->save();
                    
                    Session::flash('success', __('Amenity added successfully.'));
                    return redirect()->route('amenity.index');
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

  
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        $row = Amenity::where('id',$id)->first();
        $title = 'Edit Amenity';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        return view('admin.amenity.edit',compact('row','title','breadcum'));
    }

  
    public function update(Request $request,$id)
    {
        try{
             
            $validation = array(
                'name' => 'required',
                'name_ar' => 'required',
                'image' => 'mimes:png',
            );
        
            $validator = Validator::make(Input::all(), $validation);
           
            if ($validator->fails()) {

                return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
            } 
            else {
                $row = Amenity::where('id', $id)->first();
                $row->name = $request->name;
                $row->name_ar = $request->name_ar;
                if ($request->hasFile('image') && $request->file('image'))
                {
                    $file = $request->file('image');

                    $name = time().'.'.$file->getClientOriginalExtension();
                    $destinationPath = public_path('/sp_uploads/amenities/');
                    $img = $file->move($destinationPath, $name);

                    $row->image = $name;
                } 
                $row->save();
                
                Session::flash('success', __('Amenity Updated successfully.'));
                return redirect()->route('amenity.index');
            }
             
        }
        catch( \Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

  
    public function destroy($id)
    {
        $row = Amenity::where('id', $id)->first();
        if ($row) {
            $row->delete();
             Session::flash('success', __('Amenity deleted successfully.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('Invalid request.'));
            return redirect()->back();
        }
    }
}
