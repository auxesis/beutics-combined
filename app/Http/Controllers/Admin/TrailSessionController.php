<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;   

use App\Model\TrailSession;
use App\Model\TrailSessionQuestion;
use App\Model\TrailSessionQuestionsFaqs;
use App\Model\SpUser;
use datatables;
use App;
use Session;

class TrailSessionController extends Controller
{
    protected $title;
    protected $model;
    protected $module;

    public function __construct()
    {
        $this->title = 'Trail Sessions';
        $this->model = 'trail-session';
        $this->module = 'trail-session';
    } 

    public function index()
    {
        $title = 'Trail Sessions';
        $module = $this->module;
        $model = $this->model;
        $breadcum = [$title=>'Trail Sessions'];
        $trail_sessions = TrailSession::select('id','name')->get();
        $session_list = array();
        foreach($trail_sessions as $row){
            $session_list[$row['id']] = $row['name'];
        }
        return view('admin.trail_session.index',compact('title','model','module','breadcum','session_list'));
    }

    public function getData(Request $request)
    {
   
        $columns = ['name'];
        
        $totalData = TrailSession::count();
                       
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');      
     
        $row = TrailSession::select("trail_sessions.*");
      

        if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = $row->Where('name', 'LIKE', "%{$search}%");
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('trail_sessions.id', 'desc')
                ->get();
       
        $data = array();
        if (!empty($row)) {
            foreach ($row as $key => $rows) {
                $nestedData['id'] = $key+1;
                $nestedData['name'] = $rows->name;
                $nestedData['action'] =  getButtons([
                                ['key'=>'edit','link'=>route('trail-session.edit',$rows->id)],
                                ['key'=>'delete','link'=>route('trail-session.destroy',$rows->id)],
                            ]);
                
                $data[] = $nestedData;
            }

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function create()
    {
        $action = "create";
        $title = 'Create Session';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        return view('admin.trail_session.create',compact('action','title','breadcum','model'));
    }

    
    public function store(Request $request)
    {
        try{
             if (Input::isMethod('post')) { 
                $validation = array(
                    'name'=>'required|string',
                    //'question'=>'required|array|min:10',
                    "question.*"  => "required|string|distinct",
                );

                $validator = Validator::make(Input::all(), $validation, array(
                    'name.required' => 'Session name field is required.',
                ));

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {
                    $sessionrow = new TrailSession();
                    $sessionrow['name'] = ucwords($request->name);
                    if($sessionrow->save())
                    {
                        foreach($request->question as $arr)
                        {
                            $row = new TrailSessionQuestion();
                            $row['trail_session_id'] = $sessionrow->id;
                            $row['question'] = ucfirst($arr);
                            $row->save();
                        }
                    
                        if($row->save())
                        {
                            Session::flash('success', __('Session added successfully.'));
                            return redirect()->route('trail-session.index');
                        }
                    } 
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }
   
    public function edit($id)
    {
        $rows = TrailSession::where('id',$id)->first();
        $title = 'Edit Session';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>'Session'];
        $trail_sessions = TrailSession::select('id','name')->get();
        $session_list = $questionList = array();
        foreach($trail_sessions as $row){
            $session_list[$row['id']] = $row['name'];
        }
        $questionArr = TrailSessionQuestion::where('trail_session_id',$rows['id'])->pluck('question','id');
        foreach($questionArr as $key => $value)
        {
            $associatedTables = TrailSessionQuestionsFaqs::where('question_id',$key)->count();
            $questionList[$key] = $value;
            //$questionList['count'] = $associatedTables;
        }
        //dd($questionList); exit;
        return view('admin.trail_session.edit',compact('rows','title','breadcum','session_list','questionList'));
    }

   
    public function update(Request $request, $id)
    {
        try{
             if (Input::isMethod('PATCH')) {
                $validation = array(
                    'trail_session_id'=>'required|string',
                    "question.*"  => "required|string|distinct",
                );

                $validator = Validator::make(Input::all(), $validation, array(
                    'trail_session_id.required' => 'Session name field is required.',
                ));

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {

                    $questionArr = array_filter($request->question);
                    foreach($request->question as $id => $arr)
                    {
                        $row = TrailSessionQuestion::where('id',$id)->first();
                        if(strpos($id, 'new') !== false)
                            $row = new TrailSessionQuestion();

                        $row['trail_session_id'] = $request->trail_session_id;
                        $row['question'] = ucfirst($arr);
                        $row->save();
                    }
                    
                    if($row->save())
                    {
                        Session::flash('success', __('Session updated successfully.'));
                        return redirect()->route('trail-session.index');
                    }
                    
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

  
    public function destroy($id)
    {
        $row = TrailSession::where('id', $id)->first();
        if ($row) {
            $row->delete();
             Session::flash('success', __('Session deleted successfully.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('Invalid request.'));
            return redirect()->back();
        }
    }

    public function deleteSessionQuestion(Request $request)
    {
        $row = TrailSessionQuestion::where('id', $request->id)->first();
        if ($row) {
            $row->delete();
            return response()->json(['message'=>"Session Question deleted successfully.", 'status' => 'success']);
        } else {
            return response()->json(['message'=>__('Invalid request.'), 'status' => 'warning']);
        }
    }
}
