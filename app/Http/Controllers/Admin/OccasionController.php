<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;   

use App\Model\Occasion;
use App\Model\GiftTheme;
use App\Model\ECard;
use datatables;
use App;
use Session;

class OccasionController extends Controller
{
    protected $title;
    protected $model;
    protected $module;

    public function __construct()
    {
        $this->title = 'Occasion';
        $this->model = 'occasion';
        $this->module = 'occasion';
    } 

    public function index()
    {
        $title = 'Occasion';
        $module = $this->module;
        $model = $this->model;
        $breadcum = [$title=>'Occasions'];        
        return view('admin.occasion.index',compact('title','model','module','breadcum'));
    }

    public function getData(Request $request)
    {
        
        $columns = ['name','name_ar','created'];
        $totalData = Occasion::count();
        
        
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        $row = Occasion::select("occasions.*");
        
        if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = $row->Where('name', 'LIKE', "%{$search}%");
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('occasions.id', 'desc')
                ->get();
      
        $data = array();
        if (!empty($row)) {
            foreach ($row as $key => $rows) {
                $nestedData['name'] = $rows->name;
                $nestedData['name_ar'] = $rows->name_ar;
                $nestedData['created'] = date('Y-m-d h:i A', strtotime($rows->created_at));
                $associatedTables = $this->checkAssociatedTables($rows->id);
                $nestedData['action'] =  getButtons([
                            ['key'=>'edit','link'=>route('occasion.edit',$rows->id)],
                            ['key'=>'delete_assoc','link'=>route('occasion.destroy',$rows->id), 'asso'=>$associatedTables],
                        ]);
                
                $data[] = $nestedData;
            }

        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function checkAssociatedTables($id)
    {
        $tables = '';
        $service_count = GiftTheme::where('occasion_id',$id)->count();
        $qf_count = ECard::where('occasion_id',$id)->count();

        if($service_count > 0){
            $tables = $tables.'Themes__';
        }
        if($qf_count > 0){
            $tables = $tables.'Ecards__';
        }
        
        return $tables;
        //return explode(',',$tables);

    }
  
    public function create()
    {
        $action = "create";
        $title = 'Create Occasion';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        return view('admin.occasion.create',compact('action','title','breadcum','model'));
    }

    
    public function store(Request $request)
    {
        try{
             if (Input::isMethod('post')) {
                $validation = array(
                    'name'=>'required',
                    'name_ar'=>'required',
                );
                $validator = Validator::make(Input::all(), $validation);

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {
                    $row = new Occasion();
                    $row['name'] = $request->name;
                    $row['name_ar'] = $request->name_ar;
                    if($row->save())
                    {
                        Session::flash('success', __('Occasion added successfully.'));
                        return redirect()->route('occasion.index');
                    }                    
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

     
    public function edit($id)
    {
        $rows = Occasion::where('id',$id)->first();
        $title = 'Edit Occasion';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        return view('admin.occasion.edit',compact('rows','title','breadcum'));
    }

   
    public function update(Request $request, $id)
    {
        try{
             if (Input::isMethod('PATCH')) {
                $validation = array(
                    'name'=>'required',
                    'name_ar'=>'required',
                );

                $validator = Validator::make(Input::all(), $validation);

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {
                    $row = Occasion::where('id',$id)->first();
                    $row['name'] = $request->name;
                    $row['name_ar'] = $request->name_ar;
                   
                    if($row->save())
                    {
                        Session::flash('success', __('Occasion updated successfully.'));
                        return redirect()->route('occasion.index');
                    }
                    
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

  
    public function destroy($id)
    {
        $row = Occasion::where('id', $id)->first();
        if ($row) {
            $row->delete();
             Session::flash('success', __('Occasion deleted successfully.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('Invalid request.'));
            return redirect()->back();
        }
    }
}
