<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;   

use App\Model\Tier;
use datatables;
use App;
use Session;

class TierController extends Controller
{
    protected $title;
    protected $model;
    protected $module;

    public function __construct()
    {
        $this->model = 'tier';
        $this->module = 'tier';
        $this->title = 'tiers';
    } 

    public function index()
    {
        $title = 'View Tier';
        $module = $this->module;
        $model = $this->model;
        $breadcum = [$title=>route($model.'.index')];
        return view('admin.tier.index',compact('title','module','breadcum'));
    }

   public function getData(Request $request)
   {
        $columns = ['title','commission','redeem','earning','action'];
       
        $totalData = Tier::count();
       
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        $row = new Tier();
        // $totalFiltered = User::count();

        if (!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = $row->where(function($query) use ($search) {
                $query->where('title', 'LIKE', "%{$search}%")
                        ->orWhere('created_at', 'LIKE', "%{$search}%");
            });
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        // print_r($users);die;
        $data = array();
        if (!empty($row)) {
            foreach ($row as $key => $rows) {
                $nestedData['title'] = $rows->title;
                $nestedData['commission'] = $rows->commission.'%';
                $nestedData['redeem'] = $rows->redeem.'%';
                $nestedData['earning'] = $rows->earning.'%';
                $nestedData['action'] =  getButtons([
                                ['key'=>'edit','link'=>route('tier.edit',$rows->id)],
                                // ['key'=>'delete','link'=>route('tier.destroy',$rows->id)],
                            ]);
                
                $data[] = $nestedData;
            }

        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function create()
    {
        $action = "create";
        $title = 'Create Tier';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
 
        return view('admin.tier.create',compact('action','title','breadcum','model','module'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
             if(Input::isMethod('post')) {
                $validation = array(
                    'title' => 'required',
                    'commission' => 'required|numeric|between:0,100',
                    'redeem' => 'required|numeric|between:0,100',
                    'earning' => 'required|numeric|between:0,100',
                );

                $validator = Validator::make(Input::all(), $validation);

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors());
                } 
                else {
                    $row = new Tier();
                    
                    $row->title = $request->title;
                    $row->commission = $request->commission;
                    $row->redeem = $request->redeem;
                    $row->earning = $request->earning;
                    $row->save();
                    
                    Session::flash('success', __('Tier added successfully.'));
                    return redirect()->route('tier.index');
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        $row = Tier::where('id',$id)->first();
        $title = 'Edit Tier';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.index'),$title =>''];
        return view('admin.tier.edit',compact('row','title','breadcum'));
    }

   
    public function update(Request $request, $id)
    {
        try{
             if(Input::isMethod('PUT')) {
                $validation = array(
                    'title' => 'required',
                    'commission' => 'required|numeric|between:0,100',
                    'redeem' => 'required|numeric|between:0,100',
                    'earning' => 'required|numeric|between:0,100',
                );

                $validator = Validator::make(Input::all(), $validation);

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors());
                } 
                else {
                    $row = Tier::where('id', $id)->first();
                    
                    $row->title = $request->title;
                    $row->commission = $request->commission;
                    $row->redeem = $request->redeem;
                    $row->earning = $request->earning;
                    $row->save();
                    
                    Session::flash('success', __('Tier Updated successfully.'));
                    return redirect()->route('tier.index');
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

  
    public function destroy($id)
    {
        $row = Tier::where('id', $id)->first();
        if ($row) {
            $row->delete();
             Session::flash('success', __('Tier deleted successfully.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('Invalid request.'));
            return redirect()->back();
        }
    }
}
