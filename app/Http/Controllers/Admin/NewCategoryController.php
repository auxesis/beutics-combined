<?php

namespace App\Http\Controllers\Admin;
use datatables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use DB;
use Illuminate\Support\Facades\Input;   
use Illuminate\Support\Facades\Hash;
use Session;
use App\Model\CategoryType;


class NewCategoryController extends Controller
{
    protected $title;
    protected $model;
    protected $module;

    public function __construct()
    {
        $this->title = 'Category';
        $this->model = 'category';
        $this->module = 'new-category';
    } 

    public function index()
    {
        $title = 'Categories';
        $module = $this->module;
        $model = $this->model;
        $breadcum = [$title=>route($model.'.index')];
        $rows = CategoryType::paginate(10);
        return view('admin.category.new.index',compact('title','model','module','breadcum','rows'));
    }

    public function getData(Request $request)
    {
        $columns = ['type_name','type_name_ar','created_at'];
        $totalData = CategoryType::where('status','1')->count();
        
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        $row = CategoryType::where('status','1');
        
        if (!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = $row->where(function($query) use ($search) {
                $query->where('type_name', 'LIKE', "%{$search}%")
                        ->orWhere('created_at', 'LIKE', "%{$search}%");
            });
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        // print_r($users);die;
        $data = array();
        if (!empty($row)) {
            foreach ($row as $key => $rows) {

                $nestedData['category_name'] = $rows->type_name;
                $nestedData['category_name_ar'] = $rows->type_name_ar;
                $nestedData['created_at'] = date('d-M-y h:i:s',strtotime($rows->created_at));
                
                $data[] = $nestedData;
            }

        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }  
}
