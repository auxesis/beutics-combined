<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;   
use App\Model\SpOffer;
use App\Model\SpUser;
use App\Model\Offer;
use App\Model\Category;
use App\Model\SpectacularOfferType;
use Intervention\Image\ImageManagerStatic as Image;
use datatables;
use App;
use Session;

class SpOffersController extends Controller
{
    protected $title;
    protected $model;
    protected $module;

    public function __construct()
    {
        $this->title = 'Offers';
        $this->model = 'sp';
        $this->module = 'spOffers';
    } 

    public function pendingOffers()
    {
        $title = 'Pending Offers';
        $module = 'offerPending';
        $model = $this->model;
        $breadcum = [$title=>route($model.'.pending.offer')];
        $p_categories  = Category::where('id', '!=', '5')->where('parent_id', '0')->orderby('category_name', 'asc')->pluck('category_name', 'id')->toArray();
        return view('admin.spOffers.pendingOffers',compact('title','model','module','breadcum','p_categories'));
    }  


    public function approvedOffers()
    {
        $title = 'Approved Offers';
        $module = 'offerApproved';
        $model = $this->model;
        $breadcum = [$title=>route($model.'.approved.offer')];
        $p_categories  = Category::where('id', '!=', '5')->where('parent_id', '0')->orderby('category_name', 'asc')->pluck('category_name', 'id')->toArray();
        return view('admin.spOffers.approvedOffers',compact('title','model','module','breadcum','p_categories'));
    }

    public function expiredOffers()
    {
        $title = 'Expired Offers';
        $module = 'offerExpired';
        $model = $this->model;
        $breadcum = [$title=>route($model.'.expired.offer')];
        return view('admin.spOffers.expiredOffers',compact('title','model','module','breadcum'));
    }

    public function spectacularOffers()
    {
        $title = 'Spectacular Offers';
        $module = 'offerSpectacular';
        $model = $this->model;
        $breadcum = [$title=>route($model.'.spectacular.offer')];
        $p_categories  = Category::where('id', '!=', '5')->where('parent_id', '0')->orderby('category_name', 'asc')->pluck('category_name', 'id')->toArray();
        //echo '<pre>' ;print_r($p_categories);die;
        return view('admin.spOffers.spectacularOffers',compact('title','model','module','breadcum', 'p_categories'));
    }


    public function getData(Request $request)
    {

        $columns = ['offer_id','user_id','spectacular_offer','gender','service_type','created_at','expiry_date','is_featured','status','Action'];
        $current_date = date('Y-m-d');

        if($request->status == 'pending'){
            $totalData = SpOffer::where('status','0')->where('expiry_date','>=',$current_date)->where('is_deleted','!=','1')->count();
        }elseif($request->status == 'expired'){
            $totalData = SpOffer::where('expiry_date','<',$current_date)->where('is_deleted','!=','1')->count();
        }elseif($request->status == 'spectacular'){
            $totalData = SpOffer::where('is_nominated','1')->where('spectacular_offer','1')->where('expiry_date','>=',$current_date)->where('is_deleted','!=','1')->count();
        }else{
            $totalData = SpOffer::where('status','1')->where('is_nominated','0')->where('is_deleted','!=','1')->where('expiry_date','>=',$current_date)->count();
        } 
        
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        if($request->status == 'pending'){
           $row = SpOffer::select("sp_offers.*")->where('sp_offers.status','0')->where('is_deleted','!=','1')->where('expiry_date','>=',$current_date);
        }elseif($request->status == 'expired'){
            $row = SpOffer::select("sp_offers.*")->where('is_deleted','!=','1')->where('expiry_date','<',$current_date);
        }elseif($request->status == 'spectacular'){
            $row = SpOffer::select("sp_offers.*")->where('is_deleted','!=','1')->where('spectacular_offer','1')->where('is_nominated','1')->where('expiry_date','>=',$current_date);
        }else{
            $row = SpOffer::select("sp_offers.*")->where('is_deleted','!=','1')->where('sp_offers.status','1')->where('is_nominated','0')->where('expiry_date','>=',$current_date);
        }  

        $row = $row->leftJoin('sp_users', 'sp_users.id', '=', 'sp_offers.user_id');

        if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = $row->leftJoin('offers', 'offers.id', '=', 'sp_offers.offer_id')
                        // ->leftJoin('sp_users', 'sp_users.id', '=', 'sp_offers.user_id')
                        ->leftJoin('spectacular_offer_types', 'spectacular_offer_types.id', '=', 'sp_offers.spectacular_offer_type_id')

                    ->where(function($query) use ($search) {
                        $query->Where('offers.title', 'LIKE', "%{$search}%")
                        ->orWhere('sp_offers.id', '=', "{$search}")
                        ->orWhere('sp_users.store_name', 'LIKE', "%{$search}%")
                        ->orWhere('spectacular_offer_types.title', 'LIKE', "%{$search}%");
                    });
        }

        if (!empty($request->input('category_filter'))) {
         
            $category_id = $request->input('category_filter');
            $is_approvedsearch_1 = '';

            $row = $row
                ->where(function($query) use ($category_id) {
                    $query->orWhere('sp_users.category_id', $category_id);
                });         
        }

         if (!empty($request->input('expiry_filter'))) {
               
                $date = $request->input('expiry_filter');

                $row = $row->where(function($query) use ($date) {
                $query->where('sp_offers.expiry_date','<=', $date);
            });             
        }

         if (!empty($request->input('status_filter'))) {
         
            $is_approvedsearch = $request->input('status_filter');
            $is_approvedsearch_1 = '';

            if($is_approvedsearch == 'No'){
                $is_approvedsearch_1 = '0';
            }
            if($is_approvedsearch == 'Yes'){
                $is_approvedsearch_1 = '1';
            }
            $row = $row->where(function($query) use ($is_approvedsearch_1) {
            $query->where('sp_offers.spectacular_offer', $is_approvedsearch_1);
            });
            
             
        }

        if (!empty($request->input('discount_filter'))) {
         
            $discount_order = $request->input('discount_filter');
            $order = '';

            if($discount_order == 'high'){
                $order = 'DESC';
            }
            if($discount_order == 'low'){
                $order = 'ASC';
            }
            
            $row = $row->orderBy('sp_offers.discount', $order);
            
            
             
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('id', 'desc')
                ->get();
        //echo "<pre>";print_r($row);die;
        $data = array();
        if (!empty($row)) {
            foreach ($row as $key => $rows) {
                $nestedData['id'] = $rows->id;
                $nestedData['offer_id'] = $rows->getAssociatedOfferName->title;
                $nestedData['user_id'] = $rows->getAssociatedOfferProviderName->store_name;
                if($request->status !='spectacular'){
                    if($rows->spectacular_offer == 1){
                        $nestedData['spectacular_offer'] = 'Yes';
                    }else{
                        $nestedData['spectacular_offer'] = 'No';
                    }
                }else{
                    $nestedData['spectacular_offer_type_id'] = ($rows->getAssociatedSpectOfferName) ? $rows->getAssociatedSpectOfferName->title : '';
                }
                
                
                $nestedData['discount'] = number_format($rows->discount, 2);
                $nestedData['gender'] = $rows->gender;
                $nestedData['service_type'] = $rows->service_type;
                $nestedData['created_at'] = date('d-M-y h:i:s',strtotime($rows->created_at));
                $nestedData['expiry_date'] = date('d-M-y',strtotime($rows->expiry_date));
                $nestedData['is_featured'] = getFeaturedStatus($rows->is_featured,$rows->id);

                if($request->status =='pending'){
                    $nestedData['action'] =  getButtons([
                                ['key'=>'edit','link'=>route('sp.offer.edit',[$rows->id,'pending'])],                                                                
                            ]);
                }elseif($request->status =='approved'){
                    if($rows->spectacular_offer ==1){
                        $nestedData['action'] =  getButtons([
                            ['key'=>'edit','link'=>route('sp.offer.edit',[$rows->id,'approved'])],
                            ['key'=>'view','link'=>route('sp.offer.view',[$rows->id,'approved'])],
                            ['key'=>'nominate','link'=>route('sp.offer.nominate',[$rows->id,'approved'])],
                            ['key'=>'delete','link'=>route('sp.offer.destroy',[$rows->id])],                                                        
                        ]);
                    }else{
                        $nestedData['action'] =  getButtons([
                            ['key'=>'edit','link'=>route('sp.offer.edit',[$rows->id,'approved'])],
                            ['key'=>'view','link'=>route('sp.offer.view',[$rows->id,'approved'])],
                            ['key'=>'delete','link'=>route('sp.offer.destroy',[$rows->id])], 
                        ]);
                    }
                    
                }elseif($request->status =='spectacular'){
                    $nestedData['action'] =  getButtons([
                        ['key'=>'edit','link'=>route('sp.offer.nominate',[$rows->id,'spectacular'])],
                        ['key'=>'view','link'=>route('sp.offer.view',[$rows->id,'spectacular'])],
                        ['key'=>'delete','link'=>route('sp.offer.destroy',[$rows->id])],
                    ]);
                }else{
                    $nestedData['action'] =  getButtons([
                        ['key'=>'view','link'=>route('sp.offer.view',[$rows->id,'expired'])],                                                        
                    ]);
                }
                
                $nestedData['spectacular_status'] = getStatus($rows->spectacular_status,$rows->id);

                $data[] = $nestedData;
            }

        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }


    public function StatusUpdate(Request $request)
    {
        $offer_id = $request->offer_id;
        $row  = SpOffer::whereId($offer_id)->first();
        $row->spectacular_status =  $row->spectacular_status=='1'?'0':'1';
        $row->save();
        $html = '';
        switch ($row->spectacular_status) {
          case '1':
               $html =  '<a data-toggle="tooltip"  class="btn btn-success btn-xs" title="Active" onClick="changeStatus('.$offer_id.')" >Active</a>';
              break;
               case '0':
               $html =  '<a data-toggle="tooltip"  class="btn btn-danger btn-xs" title="Inactive" onClick="changeStatus('.$offer_id.')" >InActive</a>';
              break;
          
          default:
            
              break;
      }
      return $html;
    }

    public function FeaturedStatusUpdate(Request $request)
    {
        $offer_id = $request->offer_id;
        $row  = SpOffer::whereId($offer_id)->first();
        $row->is_featured =  $row->is_featured=='1'?'0':'1';
        $row->save();
        $html = '';
        switch ($row->is_featured) {
          case '1':
               $html =  '<a data-toggle="tooltip"  class="btn btn-success btn-xs" title="Featured" onClick="changeFeaturedStatus('.$offer_id.')" >Featured</a>';
              break;
               case '0':
               $html =  '<a data-toggle="tooltip"  class="btn btn-danger btn-xs" title="Make Featured" onClick="changeFeaturedStatus('.$offer_id.')" >Make Featured</a>';
              break;
          
          default:
            
              break;
      }
      return $html;
    }

    public function edit($id,$name)
    {
        $rows = SpOffer::where('id',$id)->first();
        $title = ucfirst($name).' Offer';
        $model = $this->model;
        $breadcum = [$this->title=>route($model.'.'.$name.'.offer'),$title =>''];

        $rows->user_id = $rows->getAssociatedOfferProviderName->store_name;
        $offer_name_rows = Offer::select('id','title')->get();
        

        return view('admin.spOffers.edit',compact('rows','title','sp_name','offer_name_rows','model','breadcum','name'));
    }

    public function nominateOffer($id,$status_name)
    {
        $rows = SpOffer::where('id',$id)->first();
        $title = 'Nominate Offer';
        $model = $this->model;
        $breadcum = [$status_name.' Offer'=>route($model.'.'.$status_name.'.offer'),$title =>''];

        $rows->user_id = $rows->getAssociatedOfferProviderName->store_name;
        $offer_name_rows = Offer::select('id','title')->get();
        $spect_offer_name_rows = SpectacularOfferType::select('id','title')->get();

        return view('admin.spOffers.nominateOffer',compact('rows','title','sp_name','offer_name_rows','breadcum','spect_offer_name_rows','status_name'));
    }

    public function updateOffer(Request $request,$id)
    {
        try{
             
            $validation = array(
              'offer_id' => 'required',
             // 'spectacular_offer' => 'required',
              'offer_details' => 'required',
              'discount' => 'required',
              'best_price' => 'required',
              'expiry_date' => 'required',
              'offer_terms' => 'required',
            );
        
            $validator = Validator::make(Input::all(), $validation);
           
            if ($validator->fails()) {

                return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
            } 
            else {
                  
                $row = SpOffer::where('id',$id)->first();
            
                $row->offer_id =$request->offer_id;
                if($request->spectacular_offer){
                    $row->spectacular_offer = $request->spectacular_offer;
                }else{
                    $row->spectacular_offer = '0';
                }
                
                $row->offer_details = $request->offer_details;
                $row->discount = $request->discount;
                $row->best_price = $request->best_price;
                $row->expiry_date = date('Y-m-d',strtotime($request->expiry_date));
                $row->offer_terms = $request->offer_terms;
                $row->remarks = $request->remarks;
                $row->status = '1';
              

                $row->save();
                     
                Session::flash('success', __('Offer Updated successfully.'));
                return redirect()->route('sp.'.$request->status_name.'.offer');
            }
             
        }
        catch( \Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

    public function declineOffer($id)
    {
        $row  = SpOffer::whereId($id)->first();
        $row->delete();
        Session::flash('success', __('Offer Declined successfully.'));
        return redirect()->route('sp.pending.offer');
    }

    public function updateNominateOffer(Request $request,$id)
    {
        try{
             
            $validation = array(
              'offer_id' => 'required',
              'offer_details' => 'required',
              'discount' => 'required',
              'best_price' => 'required',
              'expiry_date' => 'required',
              'offer_terms' => 'required',
              //'remarks' => 'required',
              'nominate_thumb_image' => 'mimes:jpeg,jpg,png|dimensions:min_width=300,min_height=350',
              'nominate_banner_image' => 'mimes:jpeg,jpg,png|dimensions:min_width=1242,min_height=630',
            );
        
            $validator = Validator::make(Input::all(), $validation);
           
            if ($validator->fails()) {

                return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
            } 
            else {
                 
                $row = SpOffer::where('id',$id)->first();
            
                $row->offer_id =$request->offer_id;
                if($request->spectacular_offer){
                    $row->spectacular_offer = $request->spectacular_offer;
                    $row->is_nominated = '1';
                }else{
                    $row->spectacular_offer = '0';
                    $row->is_nominated = '0';
                }
                
                $row->offer_details = $request->offer_details;
                $row->discount = $request->discount;
                $row->best_price = $request->best_price;
                $row->expiry_date = date('Y-m-d',strtotime($request->expiry_date));
                $row->offer_terms = $request->offer_terms;
                $row->remarks = $request->remarks;
                $row->spectacular_offer_type_id = $request->spectacular_offer_type_id;
              
                if ($request->hasFile('nominate_thumb_image') && $request->file('nominate_thumb_image'))
                {

                    $file = $request->file('nominate_thumb_image');

                    $name = time().rand(111111111,9999999999).'.'.$file->getClientOriginalExtension();

                    $destinationPath = public_path('/sp_uploads/nominated_offers/thumb');
                    $img = $file->move($destinationPath, $name);

                    // $image_resize = Image::make($file->getRealPath());
                    // $image_resize->resize(300, 522);    
                    // $destinationPath = public_path('/sp_uploads/banner_images/');
                    // $image_resize->save(public_path('/sp_uploads/nominated_offers/thumb' .$name));
                    // $image = uploadwithresize($file,'nominated_offers');

                    $row->nominate_thumb_image = $name;
                }

                if ($request->hasFile('nominate_banner_image') && $request->file('nominate_banner_image'))
                {

                    $file = $request->file('nominate_banner_image');

                    $name = time().rand(111111111,9999999999).'.'.$file->getClientOriginalExtension();

                    // $image_resize = Image::make($file->getRealPath());
                    // $image_resize->resize(1242, 630);    
                    // // $destinationPath = public_path('/sp_uploads/banner_images/');
                    // $image_resize->save(public_path('/sp_uploads/nominated_offers/' .$name));
                    // $image = uploadwithresize($file,'nominated_offers');

                    $destinationPath = public_path('/sp_uploads/nominated_offers');
                    $img = $file->move($destinationPath, $name);

                    $row->nominate_banner_image = $name;
                }
              
                
                // echo "<pre/>";print_r($row);die;
                $row->save();
                     
                Session::flash('success', __('Offer Nominated successfully.'));
                return redirect()->route('sp.'.$request->status_name.'.offer');
            }
             
        }
        catch( \Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

    public function viewOffer($id,$offer_status)
    {
        $title = 'View';
        $title_1 = ucfirst($offer_status).' Offers';
        $model = $this->model;
        $rows = SpOffer::where('id',$id)->first();
        $breadcum = [$title_1=>route($model.'.'.$offer_status.'.offer'),$title=>''];
        return view('admin.spOffers.view',compact('title','model','module','breadcum','rows'));
    }


    public function destroy($id)
    {
        $row = SpOffer::where('id', $id)->first();
        if ($row) {
            // $row->delete();
            $row->is_deleted = '1';
            $row->save();

             Session::flash('success', __('messages.Offer deleted successfully.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('messages.Invalid request.'));
            return redirect()->back();
        }
    }
}
