<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\SpUser;

class DetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Show store details
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function storeDetail($store_slug)
    {
        $storeDetail = SpUser::where('store_url',$store_slug)->first();
        if (!$storeDetail){
            abort(404);
        }
        $storeOffers = $storeDetail->getSpOffers;
        $storeInstructors = $storeDetail->getAssociatedStaff;

        return view('details/activities',compact('storeOffers','storeInstructors'));
    }

    public function bdetailsindex()
    {
        return view('details/activities');
    }

    public function idetailsindex()
    {
        return view('details/instructor');
    }

    public function adetailsindex()
    {
        return view('details/about');
    }

    public function rdetailsindex()
    {
        return view('details/reviews');
    }

    public function beautyproindex()
    {
        return view('details/products');
    }

    public function beautystoreindex()
    {
        return view('details/atstore');
    }
}
