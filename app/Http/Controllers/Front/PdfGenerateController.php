<?php 
namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Model\Booking;
use App\Model\SpUser;
use App\Model\User;
use App\Model\Review;
use DB;
use PDF;
use App;


class PdfGenerateController extends Controller
{
    // public function __construct(Request $request)
    // {
    //      parent::__construct($request);

    // }
    public function orderInvoice(Request $request,$lang)
    {
        App::setLocale($request->lang);
        $row = Booking::where('id',$request->orderid)->first();
        $final_data = $this->orderDetail($row,$request->lang);
        // echo "<pre/>";print_r($final_data);die;
        return view('front.pdf.orderInvoice',compact('final_data'));

    }

    public function generate()
    {

         $conv = new \Anam\PhantomMagick\Converter();
         $aa = $conv->source('http://192.168.0.67/beutics/generate-pdf/1')
            ->toPng()
            ->save(public_path('/sp_uploads/google.png'));

    }

    public function orderDetail($row,$lang)
    {

        $col = '';
        if($lang!= 'en'){
             $col = '_ar';
        }
        $name = 'name'.$col;
        $title = 'title'.$col;
        $data = array();

        $tier = SpUser::leftJoin('tiers', function($join) {
                  $join->on('sp_users.tier_id', '=', 'tiers.id');
                })
            ->where('sp_users.id',$row['sp_id'])
            ->select('tiers.earning')
            ->first();

        $data['id'] = $row['id'];
        $data['booking_unique_id'] = $row['booking_unique_id'];
        $data['sp_id'] = $row['sp_id'];
        $data['sp_store_name'] = $row->getAssociatedSpInformation->store_name;
        $data['sp_address'] = $row->getAssociatedSpInformation->address;
        $data['sp_landmark'] = $row->getAssociatedSpInformation->landmark;
        $data['sp_mobile_no'] = $row->getAssociatedSpInformation->store_number;
        $data['sp_profile_image'] = $row->getAssociatedSpInformation->profile_image!='' ? asset('sp_uploads/profile/'.$row->getAssociatedSpInformation->profile_image):'';
        // $data['sp_mobile_no'] = $row->getAssociatedSpInformation->mobile_no;
        $data['sp_country_code'] = $row->getAssociatedSpInformation->country_code;
        $data['user_id'] = $row['user_id'];
        $data['staff_id'] = $row['staff_id'];
        if($row['staff_id']!='' && $row['staff_id']!=null){
            $data['staff_name'] = $row->getAssociatedStaffInformation->staff_name;
        }else{
            $data['staff_name'] = "";
        }
        $data['is_gift'] = $row['is_gift'];
        $data['gifted_by'] = $row['gifted_by'];
        $data['sender_name'] = $row['gifted_by']!='' && $row['gifted_by']!=null ? $row->getAssociatedGiftByUserInfo->name:'';
        $data['receiver_name'] = $row['receiver_name'];
        $data['receiver_mobile_no'] = $row['receiver_mobile_no'];

        $data['appointment_time'] = $row['appointment_time'];
        $data['appointment_date'] = $row['appointment_date']!='' && $row['appointment_date']!= null ? $row['appointment_date']:"";
        $data['redeemed_reward_points'] = $row['redeemed_reward_points'];
        $data['payment_type'] = $row['payment_type'];
        $data['promo_code_amount'] = round($row['promo_code_amount'],2);
        $data['tax_amount'] = round($row['tax_amount'],2);
        $data['total_amount'] = round($row['booking_total_amount'],2);
        $data['paid_amount'] = round($row['paid_amount'],2);
        $data['booking_service_type'] = $row['booking_service_type'];
        $data['total_item_amount'] = round($row['total_item_amount'],2);

        if($row['db_user_name'] == '' || $row['db_user_name'] == null){
            $data['total_cashback'] = round(($row['booking_total_amount']*$tier->earning)/100,0);
        }else{
            $data['total_cashback'] = 0;
        }
        
        $data['status'] = $row['status'];
        $data['created_date'] = date('Y-m-d',strtotime($row['created_at']));

        $review = Review::where('user_id',$row['user_id'])->where('booking_id',$row['id'])->first();

        if($review){
            $data['review_description'] = $review->description;
        }else{
            $data['review_description'] = "";
        }

        $item_name_arr = array();
        foreach($row->getAssociatedBookingItems as $bkitems){

            if($bkitems['is_service'] == '1')
            {
                $item_name_arr[] = [
                    'service_id' => $bkitems['booking_item_id'],
                    'name' => $bkitems->getAssociatedServiceDetail->getAssociatedService->$name,
                    'quantity' => $bkitems['quantity'],
                    'original_price' => round($bkitems['original_price'],2),
                    'best_price' => round($bkitems['best_price'],2),
                    'cashback' => round($bkitems['cashback'],2),
                ];
            }
            else{
                $offer_services = array();

                foreach($bkitems->getAssociatedOfferDetail->getAssociatedOfferServices as $os){

                    $offer_services[] = [
                        'service_id' => $os['sp_service_id'],
                        'quantity' => $os['quantity'],
                        'name' => $os->getAssociatedOfferServicesName->getAssociatedService->$name,
                    ];
                }


                $item_name_arr[] = [
                    'offer_id' => $bkitems['booking_item_id'],
                    'name' => $bkitems->getAssociatedOfferDetail->getAssociatedOfferName->$title,
                    'quantity' => $bkitems['quantity'],
                    'original_price' => round($bkitems['original_price'],2),
                    'best_price' => round($bkitems['best_price'],2),
                    'cashback' => round($bkitems['cashback'],2),
                    'services'=>$offer_services,
                ];
            }

        }
        $data['booking_items'] = $item_name_arr;
        // print_r($data);die;
        return $data;
                
    }

}
