<?php 
namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Model\Transaction;
use Illuminate\Support\Facades\Log; 
use DB;
use Storage;



class PaytabController extends Controller
{

     function paymentLog(Request $request){ //die('123');


        // $log = new Log('PAYTAB_API1.log');
        
        $header_data = getallheaders();
         
        Log::info('header = '.json_encode($header_data,JSON_UNESCAPED_UNICODE));
        $post = $request->all();

        Log::info('Post data = '.json_encode($post,JSON_UNESCAPED_UNICODE));
        Log::info('Post data1 = '.json_encode($_REQUEST,JSON_UNESCAPED_UNICODE)); 
        Log::info('Post data2 = '.json_encode($_POST,JSON_UNESCAPED_UNICODE));
        Log::info('GET data = '.json_encode($_GET,JSON_UNESCAPED_UNICODE));

        if(isset($_REQUEST['payment_reference']) && isset($_REQUEST['pt_customer_email']) && isset($_REQUEST['pt_customer_password']) && isset($_REQUEST['pt_token'])){

           $transaction = new Transaction();
            $transaction->payment_reference = $_REQUEST['payment_reference'];
            $transaction->pt_customer_email = $_REQUEST['pt_customer_email'];
            $transaction->pt_customer_password = $_REQUEST['pt_customer_password'];
            $transaction->pt_token = $_REQUEST['pt_token'];
            $transaction->save();

            
        }
        

        // $log->write('Post data = ' .json_encode($post,JSON_UNESCAPED_UNICODE)); 
        // $log->write('Post data1 = ' .json_encode($_REQUEST,JSON_UNESCAPED_UNICODE)); 
        // $log->write('Post data2 = ' .json_encode($_POST,JSON_UNESCAPED_UNICODE)); 
        // $log->write('GET data = ' .json_encode($_GET,JSON_UNESCAPED_UNICODE)); 

    }

    function ipnListener(Request $request){ //die('123');

        try{


            $data = $request->all();
            // $my_file = 'paymentInfo.txt';
            // Storage::put($my_file, $data);
            if($data){
                Transaction::insert($data);
            }
            
        
        }catch(\Exception $e){
           $msg = $e->getMessage();
           dd($msg);
         }

    }
}
