<?php 
namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Model\CustomPage;
use Illuminate\Support\Facades\Log; 
use DB;


class CustomPageViewController extends Controller
{
    
	public function showPages($slug,$lang)
	{

		if($lang == 'ar'){
			$col = 'content_ar';
		}else{
			$col = 'content';
		}

		$row = CustomPage::where('slug',$slug)->first();
		if($row){
			return view('front/customPages/index',compact('row','col'));
		}else{
			return view('errors/404');
		}

	}

	 // public function app_redirect_01(){
  //   	$arr_browsers = ["Chrome", "Safari", "Firefox"];

		// $agent = $_SERVER['HTTP_USER_AGENT'];
		// //echo '<pre>'; print_r($agent);die;
		// $user_browser = '';
		// foreach ($arr_browsers as $browser) {
		//     if (strpos($agent, $browser) !== false) {
		//         $user_browser = $browser;
		//         break;
		//     }   
		// }
		// if($user_browser == 'Safari'){
		// 	return redirect('https://apps.apple.com/ae/app/beutics-beauty-home-services/id1483752013?ls=1');
		// } else {
		// 	if($this->isMobileDevice()){
		// 	    return redirect('market://details?id=com.beutics');
		// 	} else {
		// 	    return redirect('https://play.google.com/store/apps/details?id=com.beutics');
		// 	}
		// }
  //   }

    public function app_redirect(){
    	$arr_browsers = ["Chrome", "Safari", "Firefox","Android","IOS"];

		$agent = $_SERVER['HTTP_USER_AGENT'];
		//echo '<pre>'; print_r($agent);die;
		$user_browser = '';
		foreach ($arr_browsers as $browser) {
		    if (strpos($agent, $browser) !== false) {
		        $user_browser = $browser;
		        break;
		    }   
		}

		if($this->isMobileDevice()){
			if($user_browser == 'Android'){
				return redirect('https://play.google.com/store/apps/details?id=com.beutics');
			}else{
				return redirect('https://apps.apple.com/ae/app/beutics-beauty-home-services/id1483752013?ls=1');
			}
		}else{
			if(strpos($agent, "Mac") !== FALSE){
				return redirect('https://apps.apple.com/ae/app/beutics-beauty-home-services/id1483752013?ls=1');
			}
			else{
				return redirect('https://play.google.com/store/apps/details?id=com.beutics');
			}
		}
    }

    public function sp_app_redirect(){

    	$arr_browsers = ["Chrome", "Safari", "Firefox"];

		$agent = $_SERVER['HTTP_USER_AGENT'];
		//echo '<pre>'; print_r($agent);die;
		$user_browser = '';
		foreach ($arr_browsers as $browser) {
		    if (strpos($agent, $browser) !== false) {
		        $user_browser = $browser;
		        break;
		    }   
		}
		
		if($user_browser == 'Safari'){
			// return redirect('itms://itunes.apple.com/us/app/apple-store/1483752454?mt=8');
			return redirect('https://apps.apple.com/ae/app/beutics-service-provider/id1483752454?ls=1');
		} else {
			if($this->isMobileDevice()){
			    return redirect('market://details?id=com.beuticsprovider');
			} else {
			    return redirect('https://play.google.com/store/apps/details?id=com.beuticsprovider');
			}
		}
    }


    function isMobileDevice() {
   	 return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
	}

	public function shopdetail_redirect($sp_id){

		return view('front.appRedirects/shop_detail',compact('sp_id'));
    	
    }

    public function refercode_redirect($refer_code){
    
		return view('front.appRedirects/refer_code_redirect',compact('refer_code'));
    	
    }

    public function offerdetail($offer_id,$sp_id){

		return view('front.appRedirects/offer_detail_redirect',compact('offer_id','sp_id'));
    	
    }

    public function rootRedirect(){

		return view('front.appRedirects/rootRedirect');
    	
    }

	// public function alertPage(Request $request)
	// {
	// 	// if(isset($request)){
	// 	// 	echo "<pre/>";print_r($request->all());
	// 	// }
		
	// 	return view('front.customPages.alertPage');
	// }

	 // function alertPage(Request $request){ //die('123');


  //       // $log = new Log('PAYTAB_API1.log');
        
  //       $header_data = getallheaders();
         
  //       Log::info('header = '.json_encode($header_data,JSON_UNESCAPED_UNICODE));
  //       $post = $request->all();

  //       Log::info('Post data = '.json_encode($post,JSON_UNESCAPED_UNICODE));
  //       Log::info('Post data1 = '.json_encode($_REQUEST,JSON_UNESCAPED_UNICODE)); 
  //       Log::info('Post data2 = '.json_encode($_POST,JSON_UNESCAPED_UNICODE));
  //       Log::info('GET data = '.json_encode($_GET,JSON_UNESCAPED_UNICODE));

  //       // $log->write('Post data = ' .json_encode($post,JSON_UNESCAPED_UNICODE)); 
  //       // $log->write('Post data1 = ' .json_encode($_REQUEST,JSON_UNESCAPED_UNICODE)); 
  //       // $log->write('Post data2 = ' .json_encode($_POST,JSON_UNESCAPED_UNICODE)); 
  //       // $log->write('GET data = ' .json_encode($_GET,JSON_UNESCAPED_UNICODE)); 

  //   }
}
