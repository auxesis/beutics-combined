<?php

namespace App\Http\Controllers\Front;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\SpUser;
use App\Model\Admin;
use DB;
use Illuminate\Support\Facades\Input;   
use Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
class UserController extends Controller
{
    
    
    public function userEmailConfirmation(Request $request)
    {
        
        if ($request->has('token'))
        {
          $token = $request->input('token');
          $row =  User::where('verifyToken',$token)->first();
           if($row)
           {
              //$row->status = '1';
              $row->verifyToken = null;
              $row->save();              
              return view('front.users.email_confirmed');
             
           }
           else
           {    
              return view('front.users.invalid_token');
           }
        }
        else
        {
            return view('front.users.invalid_token');
        }
    }

    public function spUserEmailConfirmation(Request $request)
    {
        
        if ($request->has('token'))
        {
          $token = $request->input('token');
          $row =  SpUser::where('verifyToken',$token)->first();
           if($row)
           {
              //$row->status = '1';
              $row->verifyToken = null;
              $row->save();              
              return view('front.users.email_confirmed');
             
           }
           else
           {    
              return view('front.users.invalid_token');
           }
        }
        else
        {
            return view('front.users.invalid_token');
        }
    }

    public function changePassword(Request $request)
    {
        
        if ($request->has('token'))
        {

            $token = $request->input('token');
            $row =  User::where('password_reset_token',$token)->first();
          
           if($row)
           {
            return view('front.users.change_password',compact('row','token'));
           }
           else
           {    
                return view('front.users.invalid_token');
           }
        }
        else
        {
            return view('front.users.invalid_token');
        }
    }

    public function forgotPassword()
    {
        $title = 'Forgot Password';
        return view('front.users.forgotPassword',compact('title'));
    }

    public function checkForgotPasswordEmail(Request $request)
    {

        $validation = array(
            'email' => 'required|email',
        );
        $validator = Validator::make(Input::all(), $validation);
        if ($validator->fails()) 
        {
           return redirect()->back()->withErrors($validator->errors());

        }
          else  
          {
            $user = Admin::where(['email' => $request->email])->first();
            
            if(!$user)
            {
                return redirect()->back()->with('message','Invalid Email');
            }else{
                $password_reset_token = time();
                $user->password_reset_token = $password_reset_token;
                $user->save();

                $data['templete'] = "forgot_password";
                $data['token'] = $user->password_reset_token;
                $data['email'] = $user->email;
                $data['name'] = $user->name;
                $data['subject'] = "Forgot Password Email";
                send($data);
                return redirect()->back()->with('message','Email Sent Successfully');
            }
            
          }         
    }

    public function forgotPassPage($token)
    {
        $title = 'Reset Password';
        return view('front.users.setForgotPassword',compact('title','token'));
    }

    public function updateForgotPassword(Request $request)
    {
     
        $validator = Validator::make($request->all(), [
                        'password' => [
                          'required',
                          'string',
                          'min:8',             // must be at least 8 characters in length
                        ], 
                       
                        'confirm_password' => [
                          'required',
                          'same:password',
                          'string',
                          'min:8',             // must be at least 8 characters in length
                        ]  
                    ]);
        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator->errors());
        }
        else
        {
            if ($request->has('token'))
            {
                $token = $request->token;
                $row =  Admin::where('password_reset_token',$token)->first();
                   if($row)
                   {
                        $row->password = bcrypt($request->password);
                        $row->password_reset_token = null;
                        $row->save();
                        //return redirect()->back()->with('message','Password Updated Successfully');
                        Session::flash('message', __('Password updated successfully.'));
                        return redirect()->route('admin.login');
                   }
                   else
                   {    
                        return view('front.users.invalid_token');
                   }
            }
            else
            {
                return view('front.users.invalid_token');
            }
        }
    }//  
}
