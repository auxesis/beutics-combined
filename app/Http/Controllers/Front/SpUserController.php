<?php

namespace App\Http\Controllers\Front;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\SpUser;
use App\Model\Category;
use App\Model\OtpNumber;
use App\Model\Country;
use App\Model\ShopTiming;
use DB;
use Illuminate\Support\Facades\Input;   
use Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class SpUserController extends Controller
{   

    public function signup()
    {
      $title = 'Signup';
      //get categories
      $categories = Category::where('parent_id','0')->where('category_name','!=','bridal')->get();
     
      foreach($categories as $val){
        $category[$val['id']] = $val['category_name'];    
      }

      return view('front.spUsers.signup',compact('title','category'));
    } 

    public function makeSignup(Request $request)
    {
        try
         {
            $data = $request->all();

            $validator = Validator::make($data, 
              [
                'category_id' => 'required|exists:categories,id',
                'store_name' => 'required|min:1|max:50',
                'store_number' => 'required|min:8|max:50',
                'name' => 'required|min:1|max:50',
                'address' => 'required',
                'longitude' => 'required',
                'latitude' => 'required',
                'landmark' => 'required|max:200',
                'email' => 'required|email|unique:sp_users',
                // 'country_code' => 'required|exists:countries,phonecode',
                'mobile_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:16|min:8',
                'whatsapp_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:16|min:8',
                'password' => 'required|min:8|max:50',
                'confirm_password' => 'required|min:8|max:50|same:password',
              ]);
              if ($validator->fails()) 
              {
                // echo "<pre>";print_r($validator->errors());die;
                return redirect()->back()->withInput()->withErrors($validator->errors());
              } 
              else 
              {
                $user = SpUser::where('mobile_no',$request->mobile_no)->where('country_code',$request->country_code)->count();
                
                if($user > 0)
                {
                  Session::flash('warning', __('Mobile number already exists.'));
                  return redirect()->back()->withInput();

                }else{
            
                  $otp = $this->getOpt($request->country_code . $request->mobile_no);
                  // return redirect()->back()->with('message','Otp Sent Successfully');
                  //       $data['templete'] = "sp_verify_account_mail";
                  // $data['name'] = $request->store_name;
                  // $data['email'] = $request->email;
                  // // $data['mobile_no'] = '+'.$request->country_code.substr($request->mobile_no, 0, 2).substr($request->mobile_no, -1, 2);
                  // $data['mobile_no'] = '+'.$request->country_code.$request->mobile_no;
                  // $data['subject'] = "Verify your Account";
                  // // $data['token'] = $user->verifyToken;
                  // $data['otp'] = $otp;
                  // send($data);

                  
                  Session::put('registration-info', $data);
                  Session::save();
                  return redirect()->route('front.sp.check.otp');
             
                
                }
              }
         }
         catch(\Exception $e){
            return redirect()->back()->with('message',$e->getMessage());
           // return redirect()->back()->with('message',$e->getMessage());
         }       
    }

    public function getOpt($mobileNoWithCode)
    {
      $row = OtpNumber::where('complete_mobile_no',$mobileNoWithCode)->first();
      if($row){

        $otp = $row->otp;
      }
      else{     
        $otp = mt_rand(100000, 999999);
        // $otp = 12345;
      }

      $username =  env('SMS_COUNTRY_USERNAME');
      $password =  env('SMS_COUNTRY_PASSWORD');
      $message ="Your one time password for registration is ".$otp." don't share this with anyone."; 
      $parami =['User'=>$username,'passwd'=>$password,'mobilenumber'=>$mobileNoWithCode,'message'=>$message,'sid'=>'smscntry','mtype'=>'N'];
      $ponmo = http_build_query($parami);
      $url = "http://api.smscountry.com/SMSCwebservice_bulk.aspx?$ponmo"; // json
      $res = $this->get_content($url);
      if(!$row){
            $new_row = new OtpNumber();
            $new_row->complete_mobile_no = $mobileNoWithCode;
            $new_row->otp = $otp;     
            $new_row->save();
        }
      return $otp;    
    }

    function get_content($URL)
    {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_URL, $URL);
      $data = curl_exec($ch);
      // curl_exec($ch);
      curl_close($ch);
      return $data;
    }

    public function verifyOtp($otp,$mobileNoWithCode){
      $row = OtpNumber::where('complete_mobile_no',$mobileNoWithCode)->where('otp',$otp)->count();
      if($row > 0){
        OtpNumber::where('complete_mobile_no',$mobileNoWithCode)->where('otp',$otp)->delete();
        return true;
      }else{
        return false;
      }
    }

    public function checkOtp()
    {
      // $registration = Session::get('registration-info');
      // print_r($registration);die;
      $title = 'Otp';
      return view('front.spUsers.otpRegistration',compact('title'));
    }

    public function registerServiceProvider(Request $request)
    {
        try
         {
            $data = $request->all();
          
              $validator = Validator::make($data, 
                [
                  'otp' => 'required',
                ]);
              if ($validator->fails()) 
              {
                
                return redirect()->back()->withInput()->withErrors($validator->errors());
              } 
              else 
              {
                  $registration = Session::get('registration-info');
                  
                  $country_row = Country::where('phonecode',$registration['country_code'])->first();

                  $mowithcode = $registration['country_code'] . $registration['mobile_no'];
                  $verifyotp = $this->verifyOtp($request->otp,$mowithcode);
                  if($verifyotp == true)
                  {
                      $user = new SpUser();
                      $user->name = $registration['name'];
                      $user->category_id = $registration['category_id'];
                      $user->country_id = $country_row->id;
                      $user->country_code = $country_row->phonecode;
                      $user->email = $registration['email'];  
                      $user->password = bcrypt($registration['password']);
                      $user->mobile_no = $registration['mobile_no'];
                      $user->whatsapp_no = $registration['whatsapp_no'];
                      $user->store_name = $registration['store_name'];
                      $user->store_number = $registration['store_number'];
                      $user->address = $registration['address'];
                      $user->latitude = $registration['latitude'];
                      $user->longitude = $registration['longitude'];
                      $user->landmark = $registration['landmark'];
                      $user->verifyToken = strtotime(date('Y-m-d H:i:s')).rand(1111,999999);
                      $user->is_individual = $registration['is_individual'];
                      $user->share_code = $this->generateUniqueShareCode();
                      if($registration['is_individual'] == 1)
                      {
                          $user->store_type = 'individual';
                      }else{
                        $user->store_type = 'store';
                      }
                      $user->save();
       
                      if($user->save())
                      {                          
                          $data['templete'] = "sp_confirmation_email";
                          $data['name'] = $user->name;
                          $data['token'] = $user->verifyToken;
                          $data['email'] = $user->email;
                          $data['subject'] = "Confirmation Email";
                          send($data);
                          //function for storing shop opening and closing time
                          $this->manageShopTimings($user->id);

                         //mail to admin
                        $data1['templete'] = "admin_mail";
                        $data1['email'] = env("CONTACT_US_EMAIL");
                        $data1['subject'] = "New Sp Registration";
                        $data1['message'] = "A new Service Provider ".$user->store_name." has requested to join the platform. Kindly review and action the registration.";
                        send($data1);
              
                          Session::flash('success', __('Your application to join Beutics marketplace has been sent. Look forward to seeing you soon!'));

                          return redirect()->route('spadmin.login');

                          // return redirect()->route('spadmin.login')->with('message','Your application to join Beutics marketplace has been sent. Look forward to seeing you soon!');
                      }

                  }else{
                  
                    return redirect()->back()->with('message','Otp is not valid');
                  }
              }
         }
         catch(\Exception $e){
         
           return redirect()->back()->with('message',$e->getMessage());
         }
    }

    public function generateUniqueShareCode()
    {
      // $unique_id = 'SP'.rand(1000,100);
      $unique_id = substr(str_shuffle(str_repeat("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", 6)), 0, 6);
       $check_no = SpUser::where('share_code',$unique_id)->exists();
           if($check_no){
            $unique_id = $this->generateUniqueShareCode();
           }
           return $unique_id;
    }
  
    public function manageShopTimings($userid)
    {   
      for($i=1;$i<=7;$i++){
        $row = new ShopTiming();
        $row->user_id = $userid;
        $row->day = $i;
        $row->opening_time = '00:00';
        $row->closing_time = '00:00';
        $row->save();

      }
    }

    public function forgotPassword()
    {
        $title = 'Forgot Password';
        return view('front.spUsers.forgotPassword',compact('title'));
    }

    public function checkForgotPasswordEmail(Request $request)
    {

        $validation = array(
            'email' => 'required|email',
        );
        $validator = Validator::make(Input::all(), $validation);
        if ($validator->fails()) 
        {
           return redirect()->back()->withErrors($validator->errors());

        }
          else  
          {
            $user = SpUser::where(['email' => $request->email])->first();
            
            if(!$user)
            {
                return redirect()->back()->with('message','Invalid Email');
            }else{
                $password_reset_token = time();
                $user->password_reset_token = $password_reset_token;
                $user->save();

                $data['templete'] = "sp_forgot_password";
                $data['token'] = $user->password_reset_token;
                $data['email'] = $user->email;
                $data['name'] = $user->name;
                $data['subject'] = "Forgot Password Email";
                send($data);
                return redirect()->back()->with('message','Email Sent Successfully');
            }
            
          }         
    }

    public function forgotPassPage($token)
    {
        $title = 'Reset Password';
        return view('front.spUsers.setForgotPassword',compact('title','token'));
    }

    public function updateForgotPassword(Request $request)
    {
     
        $validator = Validator::make($request->all(), [
                        'password' => [
                          'required',
                          'string',
                          'min:8',             // must be at least 8 characters in length
                        ], 
                       
                        'confirm_password' => [
                          'required',
                          'same:password',
                          'string',
                          'min:8',             // must be at least 8 characters in length
                        ]  
                    ]);
        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator->errors());
        }
        else
        {
            if ($request->has('token'))
            {
                $token = $request->token;
                $row =  SpUser::where('password_reset_token',$token)->first();
                   if($row)
                   {
                        $row->password = bcrypt($request->password);
                        $row->password_reset_token = null;
                        $row->save();
    
                        Session::flash('message', __('Password updated successfully.'));
                        return redirect()->route('spadmin.login');
                   }
                   else
                   {    
                        return view('front.users.invalid_token');
                   }
            }
            else
            {
                return view('front.users.invalid_token');
            }
        }
    }//
}
