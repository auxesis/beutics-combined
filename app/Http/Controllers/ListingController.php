<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Model\SpUser;
use App\Model\CategoryType;
use App\Model\PageSection;
use App\Model\Review;
use App\Model\RatingReview;
use App\Model\SpGallery;
use App\Model\Staff;
use App\Model\PromoCode;
use App\Model\PromoCodeLinkedStore;
use DB;

class ListingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
   
     //protected $offer;
    public function __construct(){
       
    }
    public function beautyindex()
    {
        $date=date("Y-m-d");
        $pagesectionArr = $SpStore= $Store_reviews = $SpStoreGallery = array();
        $url = \Request::segment(1);
        $words = explode("-", $url);
        $last_word = array_pop($words);
        $first_word = implode(' ', $words);
        $typeData = CategoryType::select('id')->where('type_name',$first_word)->first();
        $pageSectionContent = PageSection::getPageSectionData('listing-'.$first_word);
        foreach($pageSectionContent as $content)
        {
        $pagesectionArr[$content->page_section_name][] = $content;
        }
        $isFeatured='0';
        $SpStore = SpUser::getStoreData($typeData->id,$isFeatured);
        //echo '<pre>';
        //print_r($SpStore); die;
        $reviews = Review::where('is_approved','1')->get();
        foreach ($reviews as $key => $review) {
           $Store_reviews[$review['sp_id']][] = $review;
        }

        $SpGallery = SpGallery::select('sp_gallery.file_name','sp_gallery.sp_id')->where('sp_gallery.status','1')->where('sp_gallery.mime_type','image/jpeg')->get()->toArray();

        foreach ($SpGallery as $key => $SpGallerys) {
           $SpStoreGallery[$SpGallerys['sp_id']][] = $SpGallerys;
        }

        $Staff = Staff::select('countries.flag_url','staff.sp_id')
        ->where('is_deleted','!=','1')
        ->where('flag_url','!=','')
        ->leftjoin('countries', 'countries.nationality', '=', 'staff.nationality')
        ->get()->toArray();
        
        foreach ($Staff as $key => $rows) {
           $Staffsdata[$rows['sp_id']][] = $rows;
        }
         
        $PromoCode = PromoCodeLinkedStore::select('promo_code_linked_stores.sp_id','promo_code_linked_stores.promo_code_id','promo_codes.promo_code_title')
        ->where('to_date','>=',$date)
        ->leftjoin('promo_codes', 'promo_codes.id', '=', 'promo_code_linked_stores.promo_code_id')
        ->get()->toArray();
 
        foreach ($SpStore as $key => $Store) {
            $Store->average_rating = $Store->total_reviewers = 0;
            $Store->Gallerys = $Store->StaffNationality = array();
            $Store->PromoCode='';
            $store_id=$Store->id;
            
             foreach ($Store_reviews as $key => $Store_review) 
                {
                  if($store_id == $key)
                    {
                     $avrg_rating=$this->getStoreReviews($Store_reviews[$store_id]); 
                      $Store->average_rating=$avrg_rating['average_rating'];
                      $Store->total_reviewers=$avrg_rating['total_reviewers'];
                        break;
                    }
                }
             foreach ($SpStoreGallery as $key => $SpStoreGallerys) 
             {
                if($store_id == $key)
                 {
                  $Store->Gallerys=$SpStoreGallerys;
                  break;
                 }
             } 

             foreach ($Staffsdata as $key => $Staffsdatas) 
             {
                if($store_id == $key)
                 {
                  $Store->StaffNationality=$Staffsdatas;
                  break;
                 }
             }

             foreach ($PromoCode as $key => $PromoCodes) 
             {
                if($store_id == $PromoCodes['sp_id'])
                 {
                  $Store->PromoCode=$PromoCodes['promo_code_title'];
                  break;
                 }
             }         
        }
        //echo '<pre>';
       //print_r($SpStore); die;
         
        return view('listing/beauty',compact('SpStore','pagesectionArr'));
    }
    
    public function fitnessindex()
    {
        return view('listing/fitness');
    }

    public function wellnessindex()
    {
        return view('listing/wellness');
    }

    public function getStoreReviews($reviews)
    {

        $reviews_ques_arr = \Config::get('constants.review_questions_arr');
        $rating_arr = \Config::get('constants.rating_arr');
        if(count($reviews)>0){
            $review_arr = array();
        
            foreach($reviews as $rows)
            {
                $rating_calc = array();
                foreach($rows['getAssociatedReviewQuestions'] as $review_ques)
                {
                    
                    $ques_value =  $reviews_ques_arr[$review_ques['ques_id']]['value'];
                    $ans =  $rating_arr[$review_ques['rating']]['value'];

                    $rating_calc[] = $ques_value*$ans;
                }
            
                $final_rating = (array_sum($rating_calc))/10;

                $review_arr[$rows['id']] = $final_rating;

            }
             
            $total_reviews = array_sum($review_arr)/count($review_arr);
            $finale_data['average_rating'] = round($total_reviews,1);
            $finale_data['total_reviewers'] = count($review_arr);
            
         return  $finale_data;
        }
    }   
}






































   // {{ $SpStore1['links()'] }}




 
