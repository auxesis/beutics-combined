<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

use App\Model\HomeModel;
use App\Model\Banner;
use App\Model\Testimonial;
use App\Model\PageSection;



class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     protected $category;
     //protected $offer;
     public function __construct(){
        $this->category = new HomeModel();
    }
    public function index()
    {
        $BannersectionArr = array();
        $MiddleBanner = '';
        $MiddleLowerBanner = '';

        $row = $this->category->getCategoryInfo();
        foreach ($row as $key => $value) {
        $categoryInfo[$value->type_name][]= $value;         
        }
        $Testimonial = Testimonial::getTestimonialData();
        $pageSectionContent = PageSection::getPageSectionData('home');
        $pageBannerSectionContent = Banner::getBannerData('home');
        foreach($pageSectionContent as $content)
         {
            $BannersectionArr[$content->page_section_name][] = $content;
         }
        foreach($pageBannerSectionContent as $content)
        {
            $BannersectionArr[$content->page_section_name][] = $content;

        if($content->page_section_name == 'Middle Banner')
            {
               $MiddleBanner = (object)[
                'description' => $content->description,
                'image' =>$content->image,
                'title' =>$content->title,
                'page_name'=>$content->page_name,    
                ];
            } 

         if($content->page_section_name == 'Lower Middle Banner')
            {
                $id1=$content->id;
                $title1=$content->title;
                $description1=$content->description;
                $image1[]=array(
                    'image'=>$content->image,
                    'sub_title'=>$content->sub_title,
                    'sub_description'=>$content->sub_description,
                    'page_name'=>$content->page_name,
                );
                   $MiddleLowerBanner = (object)[
                    'id' => $id1,
                    'title' => $title1,
                    'description' => $description1,
                    'image' => $image1,  
                ];
            }     

        }
        return view('index',compact('categoryInfo','Testimonial','MiddleBanner','MiddleLowerBanner','BannersectionArr'));
    }
    
    
}
