<?php

namespace App\Http\Controllers\SpAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input; 

use Session;
use App\Model\SpOffer;
use App\Model\Offer;
use App\Model\SpUser;
use App\Model\SpService;
use App\Model\SpOfferService;
use DB;

class OffersController extends Controller
{
    
    public function index()
    {
        $title = __('messages.My Offers');
        $breadcum = [$title =>''];
        $user_data = spLoginData();
        $row = SpOffer::where('id',$user_data['id'])->get();
        return view('spadmin.offers.index',compact('title','row','breadcum'));
    }

    public function getData(Request $request)
    {
        $user_data = spLoginData();
        $columns = ['offer_id','spectacular_offer','is_nominated','gender','service_type','expiry_date','status','likes','Action'];
        $current_date = date('Y-m-d');

        $totalData = SpOffer::where('is_deleted','!=','1')->count();
      
        
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        $row = SpOffer::select('sp_offers.*')->where('user_id',$user_data['id'])->where('is_deleted','!=','1');


        if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = $row->leftJoin('offers', 'offers.id', '=', 'sp_offers.offer_id')
                    ->where(function($query) use ($search) {
                        $query->Where('offers.title', 'LIKE', "%{$search}%")
                        ->orWhere('sp_offers.id', '=', "{$search}");
                        
                    });
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('id', 'desc')
                ->get();
        // print_r($users);die;
        $data = array();
        if (!empty($row)) {
            $title = 'title'.$this->getLang();
            foreach ($row as $key => $rows) {
                $nestedData['id'] = $rows->id;
                $nestedData['offer_id'] = $rows->getAssociatedOfferName->$title;
                if($rows->spectacular_offer == '1'){
                    $nestedData['spectacular_offer'] = __('messages.Yes');
                }else{
                    $nestedData['spectacular_offer'] = __('messages.No');
                }

                if($rows->is_nominated == '1'){
                    $nestedData['is_nominated'] = __('messages.Yes');
                }else{
                    $nestedData['is_nominated'] = __('messages.No');
                }
                $nestedData['gender'] = $rows->gender;
                $nestedData['service_type'] = $rows->service_type;
                $nestedData['expiry_date'] = date('d-M-y h:i:s',strtotime($rows->expiry_date));
                if($rows->status == '1'){
                    $nestedData['status'] = __('messages.Approved');
                }else{
                    $nestedData['status'] = __('messages.Pending');
                }
                if(count($rows->getAssociatedOfferLikes)>0){
                  $nestedData['likes'] = count($rows->getAssociatedOfferLikes);
                }else{
                  $nestedData['likes'] = 0;
                }

                if($rows->status == '1'){

                 $nestedData['action'] =  getButtons([
                            ['key'=>'edit','link'=>route('offers.edit',[$rows->id])],
                            ['key'=>'view','link'=>route('offers.show',[$rows->id])],                                                        
                        ]);

                    if($rows->is_nominated == '1'){
                      $nestedData['action'] =  getButtons([
                            // ['key'=>'edit','link'=>route('offers.edit',[$rows->id])],
                            ['key'=>'view','link'=>route('offers.show',[$rows->id])],                                                        
                        ]);
                    }        
                }else{
                  $nestedData['action'] =  getButtons([
                            ['key'=>'edit','link'=>route('offers.edit',[$rows->id])],
                             ['key'=>'delete','link'=>route('offers.destroy',[$rows->id])],
                            ['key'=>'view','link'=>route('offers.show',[$rows->id])],                                                        
                        ]);
                        if($rows->is_nominated == '1'){
                      $nestedData['action'] =  getButtons([
                            // ['key'=>'edit','link'=>route('offers.edit',[$rows->id])],
                            ['key'=>'view','link'=>route('offers.show',[$rows->id])],                                                        
                        ]);
                    }       
                }

               
                $data[] = $nestedData;
            }

        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function create()
    {
        $action = "create";
        $title = __('messages.Create Offer');
        $model = 'offers';
        $breadcum = [__('messages.Offers')=>route($model.'.index'),$title =>''];
        $user_data = spLoginData();
        $sp_row = SpUser::where('id',$user_data['id'])->first();
        $titlee = 'title'.$this->getLang();
        $offer_name_rows = Offer::select('id',$titlee)->get();
        return view('spadmin.offers.create',compact('action','title','titlee','module','offer_name_rows','sp_row','breadcum','model'));
    }

    public function ajaxGetServices(Request $request)
    {
       try
       {               
            $user_data = spLoginData();
           
            $key_arr = [
                'men' => [
                    'store' => ['MEN_ORIGINALPRICE','MEN_DISCOUNT','MEN_BESTPRICE'],
                    'home' => ['MEN_HOME_ORIGINALPRICE','MEN_HOME_DISCOUNT','MEN_HOME_BESTPRICE'],
                ],
                'women' => [
                    'store' => ['WOMEN_ORIGINALPRICE','WOMEN_DISCOUNT','WOMEN_BESTPRICE'],
                    'home' => ['WOMEN_HOME_ORIGINALPRICE','WOMEN_HOME_DISCOUNT','WOMEN_HOME_BESTPRICE'],
                ],
                'kids' => [
                    'store' => ['KIDS_ORIGINALPRICE','KIDS_DISCOUNT','KIDS_BESTPRICE'],
                    'home' => ['KIDS_HOME_ORIGINALPRICE','KIDS_HOME_DISCOUNT','KIDS_HOME_BESTPRICE'],
                ],
                
            ];
            $services = SpService::
            select('sp_services.*')
            ->leftjoin('services', 'services.id', '=', 'sp_services.service_id')
            ->leftjoin('sp_service_prices', 'sp_services.id', '=', 'sp_service_prices.sp_service_id')
            ->where('sp_services.user_id', '=', $user_data['id'])
            ->where('is_deleted','!=','1')
            // ->where('services.name', 'LIKE', "%{$request->service_name}%")
            ->groupBy('sp_services.id')
            ->get();

            $service_row = array();

            foreach($services as $key => $row){
    
                $service_row_current = array();

                foreach($row->getRelatedPrices as $pricerow){

                    if(in_array($pricerow['field_key'],$key_arr[$request->gender][$request->service_type] ))
                    {
                        $key_name = explode('_',$pricerow['field_key']);
                             
                        $service_row_current['sp_service_id'] = $row['id'];
                        $service_row_current[$key_name[count($key_name) - 1]] = $pricerow['price'];
                        $service_row_current['name'] = $row->getAssociatedService->name;
                            
                    }
                }
                if(!empty($service_row_current)){
                    $service_row[] = $service_row_current;
                }
                
              
            }//outer loop end
                // print_r($service_row);die;
            //  return $service_row;
            return view('spadmin.offers.ajaxServiceListing',compact('service_row'));          
              
       }
       catch(\Exception $e){
         $msg = $e->getMessage();
         Session::flash('danger', $msg);
         return redirect()->back()->withInput();
       }
    }

    public function ajaxShowServiceListing(Request $request)
    {
        $service_data = explode(',',$request->servicedata);
        

        return view('spadmin.offers.ajaxShowServiceListing',compact('service_data'));
    }

    public function store(Request $request)
    {
        try
           {
              $data = $request->all();
              // print_r($data);die;
              $validator = Validator::make($data, 
                [
                  'offer_id' => 'required',
                  // 'spectacular_offer' => 'required',
                  'offer_details' => 'required',
                  'gender' => 'required|in:men,women,kids',
                  'service_type' => 'required|in:home,store',
                  'total_price' => 'required',
                  'discount' => 'required',
                  'best_price' => 'required',
                  'expiry_date' => 'required',
                  'offer_terms' => 'required',
                  'services' => 'required',
                ]);
              if ($validator->fails()) 
              {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
              } 
              else 
              {  
                $user_data = spLoginData();
                $user_id = $user_data['id'];    
                $row = new SpOffer();
            
                $row->user_id =$user_id;
                $row->offer_id =$request->offer_id;
                if($request->spectacular_offer){
                  $row->spectacular_offer = $request->spectacular_offer;
                }
                else{
                  $row->spectacular_offer = '0';
                }
                $row->offer_details = $request->offer_details;
                $row->gender = $request->gender;
                $row->service_type = $request->service_type;
                $row->total_price = $request->total_price;
                $row->discount = $request->discount;
                $row->best_price = $request->best_price;
                $row->expiry_date = date('Y-m-d',strtotime($request->expiry_date));
                $row->offer_terms = $request->offer_terms;
                
                $row->save();
                if($row->save())
                {
                    $service_arr = $request->services;
                    
                    foreach($service_arr as $key => $sr_row){
                        $service_row = new SpOfferService();
                        $service_row->sp_offer_id = $row->id;
                        $service_row->sp_service_id = $key;
                        $service_row->quantity = $sr_row;
                        $service_row->save();
                    }

                    //mail to admin
                    $data1['templete'] = "admin_mail";
                    $data1['email'] = env("CONTACT_US_EMAIL");
                    $data1['subject'] = "New Offer Approval";
                    $data1['message'] = $row->getAssociatedOfferProviderName->store_name." has created a new offer. Kindly review and approve if necessary. ";
                    send($data1);
              
                    Session::flash('success', __('messages.Offer added successfully.'));
                    return redirect()->route('offers.index');
                }           
              }
           }
           catch(\Exception $e){
             $msg = $e->getMessage();
             Session::flash('danger', $msg);
             return redirect()->back()->withInput();
           }
    }

    
   public function show($id)
    {
        $title = __('messages.View');
        $model = 'offers';
        $rows = SpOffer::where('id',$id)->first();
        $breadcum = [__('messages.Offers')=>route($model.'.index'),$title =>''];

        $total_bestprice = 0;
        $key_arr = [
                  'men' => [
                    'store' => ['MEN_ORIGINALPRICE','MEN_DISCOUNT','MEN_BESTPRICE'],
                    'home' => ['MEN_HOME_ORIGINALPRICE','MEN_HOME_DISCOUNT','MEN_HOME_BESTPRICE','IN_MEN_HOME_ORIGINALPRICE','IN_MEN_HOME_DISCOUNT','IN_MEN_HOME_BESTPRICE'],
                  ],
                  'women' => [
                    'store' => ['WOMEN_ORIGINALPRICE','WOMEN_DISCOUNT','WOMEN_BESTPRICE'],
                    'home' => ['WOMEN_HOME_ORIGINALPRICE','WOMEN_HOME_DISCOUNT','WOMEN_HOME_BESTPRICE','IN_WOMEN_HOME_ORIGINALPRICE','IN_WOMEN_HOME_DISCOUNT','IN_WOMEN_HOME_BESTPRICE'],
                  ],
                  'kids' => [
                    'store' => ['KIDS_ORIGINALPRICE','KIDS_DISCOUNT','KIDS_BESTPRICE'],
                    'home' => ['KIDS_HOME_ORIGINALPRICE','KIDS_HOME_DISCOUNT','KIDS_HOME_BESTPRICE','IN_KIDS_HOME_ORIGINALPRICE','IN_KIDS_HOME_DISCOUNT','IN_KIDS_HOME_BESTPRICE'],
                  ],
                  
                ];
                $name = 'name'.$this->getLang(); 
        foreach($rows->getAssociatedOfferServices as $srow){

                $offers_service1 = [
                  'quantity' => $srow->quantity,
                  'name' => $srow->getAssociatedOfferServicesName->getAssociatedService->$name,
                  
                ];

              
                foreach($srow->getAssociatedOfferServicesPrices as $pricerow){

                      if(in_array($pricerow['field_key'],$key_arr[$rows->gender][$rows->service_type] ))
                      {

                        if (strpos($pricerow['field_key'], 'IN_')!== false) { 
                      
                          $changecase = 'IN_'.strtoupper($rows->gender).'_';
                        }else{
                          
                          $changecase = strtoupper($rows->gender).'_';
                        }
                        // echo $changecase = strtoupper('IN_'.$row->gender).'_';die;
                        $replace = str_replace($changecase, '', $pricerow['field_key']);
                        
                        $offers_service1[$replace] = $pricerow['price'];

                          
                      }
                    }
                    // print_r( $offers_service1);die;
                  // $offers_service1['price'] = $price_row_current;

                  $original = '';
                    if(isset($offers_service1['ORIGINALPRICE'])){
                      $original = 'ORIGINALPRICE';
                    }else{
                      $original = 'HOME_ORIGINALPRICE';
                    }
                    $total_bestprice = $total_bestprice+($offers_service1[$original]*$srow->quantity);
                    $offers_service[] = $offers_service1;
              }
             
              $cal_bestprice =  $total_bestprice - (($total_bestprice*$rows->discount)/100);
              $rows['best_price'] = number_format((float)$cal_bestprice, 2, '.', '');
              $lang_title = 'title'.$this->getLang();
              return view('spadmin.offers.view',compact('title','model','module','breadcum','rows','offers_service','total_bestprice', 'lang_title'));
    }

  
    public function edit($id)
    {
        $action = "edit";
        $title = __('messages.Edit Offer');
        $model = 'offers';
        $breadcum = [__('messages.Offers')=>route($model.'.index'),$title =>''];
        $user_data = spLoginData();
        $sp_row = SpUser::where('id',$user_data['id'])->first();
        $titlee = 'title'.$this->getLang();
        $offer_name_rows = Offer::select('id',$titlee)->get();
        $rows = SpOffer::where('id',$id)->first();
        return view('spadmin.offers.edit',compact('action','title','titlee','module','offer_name_rows','sp_row','breadcum','model','rows'));
    }

   
    public function update(Request $request, $id)
    {
        try
           {
              $data = $request->all();
              $validator = Validator::make($data, 
                [
                  'offer_id' => 'required',
                  // 'spectacular_offer' => 'required',
                  'offer_details' => 'required',
                  'gender' => 'required|in:men,women,kids',
                  'service_type' => 'required|in:home,store',
                  'total_price' => 'required',
                  'discount' => 'required',
                  'best_price' => 'required',
                  'expiry_date' => 'required',
                  'offer_terms' => 'required',
                  'services' => 'required',
                ]);
              if ($validator->fails()) 
              {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
              } 
              else 
              {  
                $user_data = spLoginData();
                $user_id = $user_data['id'];    
                $row = SpOffer::where('id',$id)->first();
            
                $row->user_id =$user_id;
                $row->offer_id =$request->offer_id;
                if($request->spectacular_offer){
                  $row->spectacular_offer = $request->spectacular_offer;
                }
                else{
                  $row->spectacular_offer = '0';
                }
                $row->offer_details = $request->offer_details;
                $row->gender = $request->gender;
                $row->service_type = $request->service_type;
                $row->total_price = $request->total_price;
                $row->discount = $request->discount;
                $row->best_price = $request->best_price;
                $row->expiry_date = date('Y-m-d',strtotime($request->expiry_date));
                $row->offer_terms = $request->offer_terms;

                $row->save();
                if($row->save())
                {
                    SpOfferService::where('sp_offer_id',$id)->delete();
                    $service_arr = $request->services;
                    
                    foreach($service_arr as $key => $sr_row){
                        $service_row = new SpOfferService();
                        $service_row->sp_offer_id = $row->id;
                        $service_row->sp_service_id = $key;
                        $service_row->quantity = $sr_row;
                        $service_row->save();
                    }
                    Session::flash('success', __('messages.Offer updated successfully.'));
                    return redirect()->route('offers.index');
                }           
              }
           }
           catch(\Exception $e){
             $msg = $e->getMessage();
             Session::flash('danger', $msg);
             return redirect()->back()->withInput();
           }
    }

   
    public function destroy($id)
    {
        $row = SpOffer::where('id', $id)->first();
        if ($row) {
            // $row->delete();
            $row->is_deleted = '1';
            $row->save();

             Session::flash('success', __('messages.Offer deleted successfully.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('messages.Invalid request.'));
            return redirect()->back();
        }
    }
}
