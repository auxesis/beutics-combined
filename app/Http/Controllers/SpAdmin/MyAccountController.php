<?php

namespace App\Http\Controllers\SpAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input; 
use App\Model\SpUser;
use App\Model\Setting;
use App\Model\Booking;
use Session;
use DB;

class MyAccountController extends Controller
{

    public function index()
    {
        $title = __('messages.My Account');
        $breadcum = [$title =>''];
        $user_data = spLoginData();
        $user_id = $user_data['id'];

        $result_arr = array();

        $tier = SpUser::leftJoin('tiers', function($join) {
            $join->on('sp_users.tier_id', '=', 'tiers.id');
          })
              ->where('sp_users.id',$user_id)
              ->select('tiers.commission','sp_users.is_tax_applied')
              ->first();

        $tax = Setting::select('percentage')->first();

        //earnings
          $total_online_sales = Booking::where('sp_id',$user_id)
                ->where('status','5')
                ->where('payment_type','0')
                ->where('payment_settlement_status','unpaid')
                ->select(DB::raw('sum(CASE 
                        WHEN IFNULL(final_settlement_amount,0) = 0 THEN total_item_amount 
                        ELSE final_settlement_amount
                        END) AS total_online_sale'))
                ->first();
                // echo $total_online_sales;die;

          $total_offline_sales = Booking::where('sp_id',$user_id)
                ->where('status','5')
                ->where('payment_settlement_status','unpaid')
                ->where('payment_type','1')
                ->select(DB::raw('sum(CASE 
                        WHEN IFNULL(final_settlement_amount,0) = 0 THEN total_item_amount 
                        ELSE final_settlement_amount
                        END) AS total_offline_sale'))
                ->first();
                
          // check sp_referred customer

          $sp_ref = Booking::leftJoin('sp_users', function($join) {
              $join->on('bookings.sp_id', '=', 'sp_users.id');
            })
                  ->leftJoin('users', function($join) {
              $join->on('bookings.user_id', '=', 'users.id');
            })
            ->whereNotNull('bookings.user_id')
            ->where('bookings.status','5')
            ->where('bookings.payment_type','1')
            ->where('bookings.payment_settlement_status','unpaid')
            ->whereRaw('sp_users.share_code = users.refer_code')
            ->where('bookings.sp_id',$user_id)
            ->distinct('bookings.user_id')
                ->pluck('bookings.user_id');

          $total_off_without_sp = Booking::where('sp_id',$user_id)
                    ->where('status','5')
                    ->where('payment_settlement_status','unpaid')
                    ->whereNull('db_user_name')
                    ->whereNull('db_user_mobile_no')
                     ->where('payment_type','1')
                    ->whereNotIn('user_id',$sp_ref)
                    ->select(DB::raw('sum(CASE 
                            WHEN IFNULL(final_settlement_amount,0) = 0 THEN total_item_amount 
                            ELSE final_settlement_amount
                            END) AS total_sales'))
                    ->first();

          $total_sales['total_sales'] = $total_online_sales['total_online_sale'] + $total_off_without_sp['total_sales'];

          $beutics_commission = ($total_sales['total_sales'] * $tier['commission'])/100;

          $net_store_sales = $total_online_sales['total_online_sale'] + $total_offline_sales['total_offline_sale'];

          if($tier['is_tax_applied'] == '1'){
            $my_earnings = $total_sales['total_sales'] - $beutics_commission;
          }else{
            $earning = $total_sales['total_sales'] - $beutics_commission;
            $my_earnings = $earning + ($earning * $tax['percentage'])/100;
          }
          
          //outstanding

          $calc_pay_store = ((100-$tier['commission'])*$total_online_sales['total_online_sale'])/100;


          $calc_pay_beutics_amt = Booking::where('sp_id',$user_id)
                ->where('status','5')
                ->where('payment_settlement_status','unpaid')
                ->whereNull('db_user_name')
                ->whereNull('db_user_mobile_no')
                ->where('payment_type','1')
                ->whereNotIn('user_id',$sp_ref)
                ->select(DB::raw('sum(CASE 
                        WHEN IFNULL(final_settlement_amount,0) = 0 THEN total_item_amount 
                        ELSE final_settlement_amount
                        END) AS pay_to_beutics'))
                ->first();


          $calc_pay_beutics = ($tier['commission'] * $calc_pay_beutics_amt['pay_to_beutics'])/100;

          

          $outstanding = $calc_pay_store-$calc_pay_beutics;
          $sign = '';
          if($tier['is_tax_applied'] == '1'){
            if($outstanding>0){
              $calc_outstanding = $outstanding+(($outstanding*$tax['percentage'])/100);

            }else{
              $calc_outstanding = abs($outstanding)+((abs($outstanding)*$tax['percentage'])/100);
              $sign = '-';
            }
            $result_arr['outstanding'] = round($sign.$calc_outstanding,2);
          }else{
            $result_arr['outstanding'] = round($outstanding,2);
          }
            
                        //life time earning section

          $total_lt_online_sales = Booking::where('sp_id',$user_id)
                ->where('status','5')
                ->where('payment_type','0')
                ->where('payment_settlement_status','paid')
                ->select(DB::raw('sum(CASE 
                        WHEN IFNULL(final_settlement_amount,0) = 0 THEN total_item_amount 
                        ELSE final_settlement_amount
                        END) AS total_online_sale'))
                ->first();
                // echo $total_online_sales;die;

          $total__lt_offline_sales = Booking::where('sp_id',$user_id)
                ->where('status','5')
                ->where('payment_settlement_status','paid')
                ->where('payment_type','1')
                ->select(DB::raw('sum(CASE 
                        WHEN IFNULL(final_settlement_amount,0) = 0 THEN total_item_amount 
                        ELSE final_settlement_amount
                        END) AS total_offline_sale'))
                ->first();
          $life_time_earning = $total_lt_online_sales['total_online_sale'] + $total__lt_offline_sales['total_offline_sale'];

          $result_arr['total_lifetime_online_earning'] = $total_lt_online_sales['total_online_sale'];
          $result_arr['total_lifetime_offline_earning'] = $total__lt_offline_sales['total_offline_sale'];
          $result_arr['total_lifetime_earning'] = $life_time_earning;
          $result_arr['total_online_sales'] = round($total_online_sales['total_online_sale'],2);
          $result_arr['total_offline_sale'] = round($total_offline_sales['total_offline_sale'],2);
          $result_arr['net_store_sales'] = round($net_store_sales,2);
          $result_arr['total_sales'] = round($total_sales['total_sales'],2);
          $result_arr['beutics_commission'] = round($beutics_commission,2);
          $result_arr['my_earnings'] = round($my_earnings,2);
          $result_arr['VAT'] = $tier['is_tax_applied'];
          $result_arr['commission'] = $tier['commission'];


        return view('spadmin.myAccount.index',compact('title','row','breadcum','result_arr'));
    }

    public function getData(Request $request)
    {
        $user_data = spLoginData();
        $user_id = $user_data['id'];
        $columns = ['order_id','customer_name','order_date','services_name','service_closed'];

        $current_date = date('Y-m-d');
        // print_r($request->all());die;
        // $totalData = SpOffer::count();

         $sp_ref = Booking::leftJoin('sp_users', function($join) {
                  $join->on('bookings.sp_id', '=', 'sp_users.id');
                })
                      ->leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                })
                ->whereNotNull('user_id')
                ->where('bookings.status','5')
                ->where('payment_type','1')
                ->where('bookings.payment_settlement_status','paid')
                ->whereRaw('sp_users.share_code = users.refer_code')
                ->where('bookings.sp_id',$user_id)
                ->distinct('bookings.user_id')
                ->pluck('bookings.user_id');

        if($request->status == 'paid_order'){
            $totalData = Booking::where('sp_id',$user_id)
                ->where('status','5')
                ->where('payment_settlement_status','paid')
                ->whereNull('db_user_name')
                ->whereNull('db_user_mobile_no')
                ->whereNotIn('user_id',$sp_ref)
                ->count();
              

          }elseif ($request->status == 'online') {
            $totalData = Booking::where('sp_id',$user_id)
                ->where('status','5')
                ->where('payment_type','0')
                ->where('payment_settlement_status','unpaid')
                ->count();

          }elseif ($request->status == 'offline') {
            $totalData = Booking::where('sp_id',$user_id)
                ->where('status','5')
                ->where('payment_type','1')
                ->where('payment_settlement_status','unpaid')
                ->count();
          }else{
            $totalData = Booking::where('sp_id',$user_id)
                ->where('status','5')
                ->where('payment_settlement_status','unpaid')
                ->whereNull('db_user_name')
                ->whereNull('db_user_mobile_no')
                ->whereNotIn('user_id',$sp_ref)
                ->count();
          }
      
        
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

       

        if($request->status == 'paid_order'){
            $row = Booking::where('sp_id',$user_id)
                ->where('status','5')
                ->where('payment_settlement_status','paid')
                ->whereNull('db_user_name')
                ->whereNull('db_user_mobile_no')
                ->whereNotIn('user_id',$sp_ref);
              

          }elseif ($request->status == 'online') {
            $row = Booking::where('sp_id',$user_id)
                ->where('status','5')
                ->where('payment_type','0')
                ->where('payment_settlement_status','unpaid');

          }elseif ($request->status == 'offline') {
            $row = Booking::where('sp_id',$user_id)
                ->where('status','5')
                ->where('payment_type','1')
                ->where('payment_settlement_status','unpaid');
          }else{
            $row = Booking::where('sp_id',$user_id)
                ->where('status','5')
                ->where('payment_settlement_status','unpaid')
                ->whereNull('db_user_name')
                ->whereNull('db_user_mobile_no')
                ->whereNotIn('user_id',$sp_ref);
          }

        // $row = SpOffer::select('sp_offers.*')->where('user_id',$user_data['id']);


        // if(!empty($request->input('search.value'))) {
        //     $search = $request->input('search.value');
        //     $row = $row->leftJoin('offers', 'offers.id', '=', 'sp_offers.offer_id')
        //             ->where(function($query) use ($search) {
        //                 $query->Where('offers.title', 'LIKE', "%{$search}%");
                        
        //             });
        // }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('id', 'desc')
                ->get();
        // print_r($users);die;
        $data = array();
        if (!empty($row)) {
            foreach ($row as $key => $rows) {
                $nestedData['order_id'] = $rows->booking_unique_id;
                $nestedData['customer_name'] = $rows->user_id;
                $nestedData['order_date'] = date('Y-m-d',strtotime($rows->created_at));
                $nestedData['services_name'] = $rows->id;
                $nestedData['service_closed'] = date('Y-m-d',strtotime($rows->cancelled_date_time));
               
                $data[] = $nestedData;
            }

        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function onlineSales()
    {       
        $title = __('messages.Online Sales');
        return view('spadmin.myAccount.onlineSales',compact('title'));
    }


    public function getOnlineData(Request $request)
    {
        $user_data = spLoginData();
        $columns = ['booking_unique_id','name', 'booking_items','created_date','appointment_date','service_end_time','Action'];
        $order = $columns[$request->input('order.0.column')];

        $totalData = Booking::count();    
        $limit = $request->input('length');
        $start = $request->input('start');     

        if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                })
                ->where(function ($query) use ($search){
                  $query->orWhere('users.name', 'like', '%' .$search . '%')
                  ->orWhere('users.mobile_no', 'like', '%' .$search . '%');
                })                
                ->select('bookings.*', 'users.name', 'users.mobile_no')
                ->where('bookings.sp_id',$user_data['id'])
                ->where('bookings.status','5')
                ->where('bookings.payment_type','0')
                ->where('bookings.payment_settlement_status','unpaid')
                ->orderBy('bookings.id','desc');
        } else {
          $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                }) 
          ->select('bookings.*', 'users.name', 'users.mobile_no')
            ->where('bookings.sp_id',$user_data['id'])
            ->where('bookings.payment_type','0')
            ->where('bookings.payment_settlement_status','unpaid')
            ->where('bookings.status','5');
        }
       
        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('bookings.id', 'desc')
                ->get();
        //echo'<pre>'; print_r($row);die;
        $data = array();

        if (!empty($row)) {
            foreach($row as $val){
              $date = $val['appointment_date'];
              $time = $val['appointment_time'];
              $combinedDT = '';
              if($date!='' && $date!=null && $time!=null && $time!=''){
                 $combinedDT = date('M d, Y h:i A', strtotime("$date $time"));
               }else{
                $combinedDT = 'NA';
               }

               $is_referred = '<p style="color:red;"><i>('.__('messages.New Customer').')</i></p>';
                //check customer uses sp refer code or not

                if($val['user_id']!='' && $val['user_id']!=null){
                  $cust_refer_code = $val->getAssociatedUserInfo->refer_code;
                  // echo $cust_refer_code;die;
                  $check_sp_code = SpUser::where('share_code',$cust_refer_code)->where('id',$val['sp_id'])->exists();
                  if($check_sp_code){
                    $is_referred = '<p style="color:red;"><i>('.__('messages.Sp Referred').')</i></p>';
                  }
                }


            $nestedData = [
              "id" => $val['id'],
              "booking_unique_id" => $val['booking_unique_id'],
              "booking_service_type" => $val['booking_service_type'],
              "name" => $val['name'].'<br/>'.$is_referred,
              "mobile_no" => $val['mobile_no'],
              "appointment_date" => $combinedDT,
              "service_end_time" => date('Y-m-d h:i A', strtotime($val['completed_date_time'])),
              "tax_amount" => round($val['tax_amount'],2),
              "total_amount" => round($val['booking_total_amount'],2).' AED',
              "total_item_amount" => round($val['total_item_amount'],2).' AED',
              "paid_amount" => round($val['paid_amount'],2),
              "created_date" => date('Y-m-d h:i A',strtotime($val['created_at'])),
            ];
            if(!$val['user_id'] && !$val['is_gift'] && ($val['db_user_name'] || $val['db_user_mobile_no'])){
              $nestedData['name'] = $val['db_user_name'];
              $nestedData['mobile_no'] = $val['db_user_mobile_no'];
            }
            if(!$val['user_id'] && $val['is_gift'] && ($val['receiver_name'] || $val['receiver_mobile_no'])){
              $nestedData['name'] = $val['receiver_name'];
              $nestedData['mobile_no'] = $val['receiver_mobile_no'];
            }
            $name = 'name'.$this->getLang();
            $title = 'title'.$this->getLang();
             $item_name_arr = '';
              foreach($val->getAssociatedBookingItems as $bkitems){

                if($bkitems['is_service'] == '1')
                {
                  $item_name_arr = $bkitems->getAssociatedServiceDetail->getAssociatedService->$name.',';
                } else {
                  $offer_services = '';
                  foreach($bkitems->getAssociatedOfferDetail->getAssociatedOfferServices as $os){
                    $offer_services = $offer_services.$os->getAssociatedOfferServicesName->getAssociatedService->$name.', ';
                  }
                  if($offer_services){
                    $offer_services = rtrim($offer_services, ', ');
                  }
                  $item_name_arr = $bkitems->getAssociatedOfferDetail->getAssociatedOfferName->$title.'('.$offer_services.'), ';
                }
              }
              if($item_name_arr){
                $item_name_arr = rtrim($item_name_arr, ', ');
              }
             $nestedData['booking_items'] = $item_name_arr;
             $nestedData['action'] =  getButtons([
                ['key'=>'view','link'=>route('orders.show_order',[$val->id, 2])]]); 

            $data[] = $nestedData;
          }

        }
        //echo '<pre>'; print_r($data);
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function offlineSales()
    {       
        $title = __('messages.Offline Sales');
        return view('spadmin.myAccount.offlineSales',compact('title'));
    }


    public function getOfflineData(Request $request)
    {
        $user_data = spLoginData();
        $columns = ['booking_unique_id','name', 'booking_items','created_date','appointment_date','service_end_time','Action'];
        $order = $columns[$request->input('order.0.column')];

        $totalData = Booking::count();    
        $limit = $request->input('length');
        $start = $request->input('start');     

        if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                })
                ->where(function ($query) use ($search){
                  $query->orWhere('name', 'like', '%' .$search . '%')
                  ->orWhere('mobile_no', 'like', '%' .$search . '%');
                })                
                ->select('bookings.*', 'users.name', 'users.mobile_no')
                ->where('bookings.sp_id',$user_data['id'])
                ->where('bookings.status','5')
                ->where('bookings.payment_type','1')
                ->where('bookings.payment_settlement_status','unpaid')
                ->orderBy('bookings.id','desc');
        } else {
          $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                }) 
          ->select('bookings.*', 'users.name', 'users.mobile_no')
            ->where('bookings.sp_id',$user_data['id'])
            ->where('bookings.payment_type','1')
            ->where('bookings.payment_settlement_status','unpaid')
            ->where('bookings.status','5');
        }
       
        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('bookings.id', 'desc')
                ->get();
        //echo'<pre>'; print_r($row);die;
        $data = array();

        if (!empty($row)) {
            foreach($row as $val){
              $date = $val['appointment_date'];
              $time = $val['appointment_time'];
              $combinedDT = '';
              if($date!='' && $date!=null && $time!=null && $time!=''){
                 $combinedDT = date('M d, Y h:i A', strtotime("$date $time"));
               }else{
                $combinedDT = 'NA';
               }

               $is_referred = '<p style="color:red;"><i>('.__('messages.New Customer').')</i></p>';
                //check customer uses sp refer code or not

                if($val['user_id']!='' && $val['user_id']!=null){
                  $cust_refer_code = $val->getAssociatedUserInfo->refer_code;
                  // echo $cust_refer_code;die;
                  $check_sp_code = SpUser::where('share_code',$cust_refer_code)->where('id',$val['sp_id'])->exists();
                  if($check_sp_code){
                    $is_referred = '<p style="color:red;"><i>('.__('messages.Sp Referred').')</i></p>';
                  }
                }


            $nestedData = [
              "id" => $val['id'],
              "booking_unique_id" => $val['booking_unique_id'],
              "booking_service_type" => $val['booking_service_type'],
              "name" => $val['name'].'<br/>'.$is_referred,
              "mobile_no" => $val['mobile_no'],
              "appointment_date" => $combinedDT,
              "service_end_time" => date('Y-m-d h:i A', strtotime($val['completed_date_time'])),
              "tax_amount" => round($val['tax_amount'],2),
              "total_amount" => round($val['booking_total_amount'],2).' AED',
              "total_item_amount" => round($val['total_item_amount'],2).' AED',
              "paid_amount" => round($val['paid_amount'],2),
              "created_date" => date('Y-m-d h:i A',strtotime($val['created_at'])),
            ];
            if(!$val['user_id'] && !$val['is_gift'] && ($val['db_user_name'] || $val['db_user_mobile_no'])){
              $nestedData['name'] = $val['db_user_name'];
              $nestedData['mobile_no'] = $val['db_user_mobile_no'];
            }
            if(!$val['user_id'] && $val['is_gift'] && ($val['receiver_name'] || $val['receiver_mobile_no'])){
              $nestedData['name'] = $val['receiver_name'];
              $nestedData['mobile_no'] = $val['receiver_mobile_no'];
            }
            $name = 'name'.$this->getLang();
            $title = 'title'.$this->getLang();
             $item_name_arr = '';
              foreach($val->getAssociatedBookingItems as $bkitems){

                if($bkitems['is_service'] == '1')
                {
                  $item_name_arr = $bkitems->getAssociatedServiceDetail->getAssociatedService->$name.',';
                } else {
                  $offer_services = '';
                  foreach($bkitems->getAssociatedOfferDetail->getAssociatedOfferServices as $os){
                    $offer_services = $offer_services.$os->getAssociatedOfferServicesName->getAssociatedService->$name.', ';
                  }
                  if($offer_services){
                    $offer_services = rtrim($offer_services, ', ');
                  }
                  $item_name_arr = $bkitems->getAssociatedOfferDetail->getAssociatedOfferName->$title.'('.$offer_services.'), ';
                }
              }
              if($item_name_arr){
                $item_name_arr = rtrim($item_name_arr, ', ');
              }
             $nestedData['booking_items'] = $item_name_arr;
             $nestedData['action'] =  getButtons([
                ['key'=>'view','link'=>route('orders.show_order',[$val->id, 2])]]); 

            $data[] = $nestedData;
          }

        }
        //echo '<pre>'; print_r($data);
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function allSales()
    {       
        $title = __('messages.Total Sales (Beutics Order)');
        return view('spadmin.myAccount.allSales',compact('title'));
    }

    public function getAllData(Request $request)
    {
        $user_data = spLoginData();
        $columns = ['booking_unique_id','name', 'booking_items','created_date','appointment_date','service_end_time','Action'];
        $order = $columns[$request->input('order.0.column')];

        $totalData = Booking::count();    
        $limit = $request->input('length');
        $start = $request->input('start'); 

        $sp_ref = Booking::leftJoin('sp_users', function($join) {
                  $join->on('bookings.sp_id', '=', 'sp_users.id');
                })
                      ->leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                })
                ->whereNotNull('bookings.user_id')
                ->where('bookings.status','5')
                ->where('bookings.payment_type','1')
                ->where('bookings.payment_settlement_status','unpaid')
                ->whereRaw('sp_users.share_code = users.refer_code')
                ->where('bookings.sp_id',$user_data['id'])
                ->distinct('bookings.user_id')
                ->pluck('bookings.user_id');
        $user_id = $user_data['id'];
        if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                })
                ->where(function ($query) use ($search){
                  $query->orWhere('users.name', 'like', '%' .$search . '%')
                  ->orWhere('users.mobile_no', 'like', '%' .$search . '%');
                })                
                ->select('bookings.*', 'users.name', 'users.mobile_no')
                ->where(function ($query) use($user_id){                  
                  $query->where('bookings.sp_id',$user_id)
                    ->where('bookings.payment_type','0')
                    ->where('bookings.status','5')
                    ->where('bookings.payment_settlement_status','unpaid')
                    ->whereNull('bookings.db_user_name')
                    ->whereNull('bookings.db_user_mobile_no');
                    
                  })
                  ->orWhere(function ($query) use($sp_ref,$user_id){            
                  $query->where('bookings.sp_id',$user_id)
                    ->where('bookings.status','5')
                    ->where('bookings.payment_settlement_status','unpaid')
                    ->whereNull('bookings.db_user_name')
                    ->whereNull('bookings.db_user_mobile_no')
                    ->where('bookings.payment_type','1')
                    ->whereNotIn('bookings.user_id',$sp_ref);
                  })
                ->orderBy('bookings.id','desc');
        } else {
          $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                }) 
            ->select('bookings.*', 'users.name', 'users.mobile_no')
             ->where(function ($query) use($user_id){                  
              $query->where('bookings.sp_id',$user_id)
                ->where('bookings.payment_type','0')
                ->where('bookings.status','5')
                ->where('bookings.payment_settlement_status','unpaid')
                ->whereNull('bookings.db_user_name')
                ->whereNull('bookings.db_user_mobile_no');
                
              })
              ->orWhere(function ($query) use($sp_ref,$user_id){            
              $query->where('bookings.sp_id',$user_id)
                ->where('bookings.status','5')
                ->where('bookings.payment_settlement_status','unpaid')
                ->whereNull('bookings.db_user_name')
                ->whereNull('bookings.db_user_mobile_no')
                ->where('bookings.payment_type','1')
                ->whereNotIn('bookings.user_id',$sp_ref);
              })
              ->orderBy('bookings.id','desc');
        }
       
        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('bookings.id', 'desc')
                ->get();
        //echo'<pre>'; print_r($row);die;
        $data = array();

        if (!empty($row)) {
            foreach($row as $val){
              $date = $val['appointment_date'];
              $time = $val['appointment_time'];
              $combinedDT = '';
              if($date!='' && $date!=null && $time!=null && $time!=''){
                 $combinedDT = date('M d, Y h:i A', strtotime("$date $time"));
               }else{
                $combinedDT = 'NA';
               }

               $is_referred = '<p style="color:red;"><i>('.__('messages.New Customer').')</i></p>';
                //check customer uses sp refer code or not

                if($val['user_id']!='' && $val['user_id']!=null){
                  $cust_refer_code = $val->getAssociatedUserInfo->refer_code;
                  // echo $cust_refer_code;die;
                  $check_sp_code = SpUser::where('share_code',$cust_refer_code)->where('id',$val['sp_id'])->exists();
                  if($check_sp_code){
                    $is_referred = '<p style="color:red;"><i>('.__('messages.Sp Referred').')</i></p>';
                  }
                }


            $nestedData = [
              "id" => $val['id'],
              "booking_unique_id" => $val['booking_unique_id'],
              "booking_service_type" => $val['booking_service_type'],
              "name" => $val['name'].'<br/>'.$is_referred,
              "mobile_no" => $val['mobile_no'],
              "appointment_date" => $combinedDT,
              "service_end_time" => date('Y-m-d h:i A', strtotime($val['completed_date_time'])),
              "tax_amount" => round($val['tax_amount'],2),
              "total_amount" => round($val['booking_total_amount'],2).' AED',
              "total_item_amount" => round($val['total_item_amount'],2).' AED',
              "paid_amount" => round($val['paid_amount'],2),
              "created_date" => date('Y-m-d h:i A',strtotime($val['created_at'])),
            ];
            if(!$val['user_id'] && !$val['is_gift'] && ($val['db_user_name'] || $val['db_user_mobile_no'])){
              $nestedData['name'] = $val['db_user_name'];
              $nestedData['mobile_no'] = $val['db_user_mobile_no'];
            }
            if(!$val['user_id'] && $val['is_gift'] && ($val['receiver_name'] || $val['receiver_mobile_no'])){
              $nestedData['name'] = $val['receiver_name'];
              $nestedData['mobile_no'] = $val['receiver_mobile_no'];
            }
            $name = 'name'.$this->getLang();
            $title = 'title'.$this->getLang();
             $item_name_arr = '';
              foreach($val->getAssociatedBookingItems as $bkitems){

                if($bkitems['is_service'] == '1')
                {
                  $item_name_arr = $bkitems->getAssociatedServiceDetail->getAssociatedService->$name.',';
                } else {
                  $offer_services = '';
                  foreach($bkitems->getAssociatedOfferDetail->getAssociatedOfferServices as $os){
                    $offer_services = $offer_services.$os->getAssociatedOfferServicesName->getAssociatedService->$name.', ';
                  }
                  if($offer_services){
                    $offer_services = rtrim($offer_services, ', ');
                  }
                  $item_name_arr = $bkitems->getAssociatedOfferDetail->getAssociatedOfferName->$title.'('.$offer_services.'), ';
                }
              }
              if($item_name_arr){
                $item_name_arr = rtrim($item_name_arr, ', ');
              }
             $nestedData['booking_items'] = $item_name_arr;
             $nestedData['action'] =  getButtons([
                ['key'=>'view','link'=>route('orders.show_order',[$val->id, 2])]]); 

            $data[] = $nestedData;
          }

        }
        //echo '<pre>'; print_r($data);
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function paidSales()
    {       
        $title = __('messages.Total Paid Sales');
        return view('spadmin.myAccount.paidSales',compact('title'));
    }

    public function getPaidData(Request $request)
    {
        $user_data = spLoginData();
        $columns = ['booking_unique_id','name', 'booking_items','created_date','appointment_date','service_end_time','Action'];
        $order = $columns[$request->input('order.0.column')];

        $totalData = Booking::count();    
        $limit = $request->input('length');
        $start = $request->input('start'); 

        $sp_ref = Booking::leftJoin('sp_users', function($join) {
                  $join->on('bookings.sp_id', '=', 'sp_users.id');
                })
                      ->leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                })
                ->whereNotNull('bookings.user_id')
                ->where('bookings.status','5')
                ->where('bookings.payment_type','1')
                ->where('bookings.payment_settlement_status','paid')
                ->whereRaw('sp_users.share_code = users.refer_code')
                ->where('bookings.sp_id',$user_data['id'])
                ->distinct('bookings.user_id')
                ->pluck('bookings.user_id');

        if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                })
                ->where(function ($query) use ($search){
                  $query->orWhere('name', 'like', '%' .$search . '%')
                  ->orWhere('mobile_no', 'like', '%' .$search . '%');
                })                
                ->select('bookings.*', 'users.name', 'users.mobile_no')
                ->where('bookings.sp_id',$user_data['id'])
                ->where('bookings.status','5')
                ->where('bookings.payment_settlement_status','paid')        
                ->whereNull('bookings.db_user_name')
                ->whereNull('bookings.db_user_mobile_no')
                ->whereNotIn('bookings.user_id',$sp_ref)
                ->orderBy('bookings.id','desc');
        } else {
          $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                }) 
          ->select('bookings.*', 'users.name', 'users.mobile_no')
            ->where('bookings.sp_id',$user_data['id'])
            ->where('bookings.payment_settlement_status','paid')
            ->whereNull('bookings.db_user_name')
            ->whereNull('bookings.db_user_mobile_no')
            ->whereNotIn('bookings.user_id',$sp_ref)
            ->where('bookings.status','5');
        }
       
        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('bookings.id', 'desc')
                ->get();
        //echo'<pre>'; print_r($row);die;
        $data = array();

        if (!empty($row)) {
            foreach($row as $val){
              $date = $val['appointment_date'];
              $time = $val['appointment_time'];
              $combinedDT = '';
              if($date!='' && $date!=null && $time!=null && $time!=''){
                 $combinedDT = date('M d, Y h:i A', strtotime("$date $time"));
               }else{
                $combinedDT = 'NA';
               }

               $is_referred = '<p style="color:red;"><i>('.__('messages.New Customer').')</i></p>';
                //check customer uses sp refer code or not

                if($val['user_id']!='' && $val['user_id']!=null){
                  $cust_refer_code = $val->getAssociatedUserInfo->refer_code;
                  // echo $cust_refer_code;die;
                  $check_sp_code = SpUser::where('share_code',$cust_refer_code)->where('id',$val['sp_id'])->exists();
                  if($check_sp_code){
                    $is_referred = '<p style="color:red;"><i>('.__('messages.Sp Referred').')</i></p>';
                  }
                }


            $nestedData = [
              "id" => $val['id'],
              "booking_unique_id" => $val['booking_unique_id'],
              "booking_service_type" => $val['booking_service_type'],
              "name" => $val['name'].'<br/>'.$is_referred,
              "mobile_no" => $val['mobile_no'],
              "appointment_date" => $combinedDT,
              "service_end_time" => date('Y-m-d h:i A', strtotime($val['completed_date_time'])),
              "tax_amount" => round($val['tax_amount'],2),
              "total_amount" => round($val['booking_total_amount'],2).' AED',
              "total_item_amount" => round($val['total_item_amount'],2).' AED',
              "paid_amount" => round($val['paid_amount'],2),
              "created_date" => date('Y-m-d h:i A',strtotime($val['created_at'])),
            ];
            if(!$val['user_id'] && !$val['is_gift'] && ($val['db_user_name'] || $val['db_user_mobile_no'])){
              $nestedData['name'] = $val['db_user_name'];
              $nestedData['mobile_no'] = $val['db_user_mobile_no'];
            }
            if(!$val['user_id'] && $val['is_gift'] && ($val['receiver_name'] || $val['receiver_mobile_no'])){
              $nestedData['name'] = $val['receiver_name'];
              $nestedData['mobile_no'] = $val['receiver_mobile_no'];
            }
            $name = 'name'.$this->getLang();
            $title = 'title'.$this->getLang();
             $item_name_arr = '';
              foreach($val->getAssociatedBookingItems as $bkitems){

                if($bkitems['is_service'] == '1')
                {
                  $item_name_arr = $bkitems->getAssociatedServiceDetail->getAssociatedService->$name.',';
                } else {
                  $offer_services = '';
                  foreach($bkitems->getAssociatedOfferDetail->getAssociatedOfferServices as $os){
                    $offer_services = $offer_services.$os->getAssociatedOfferServicesName->getAssociatedService->$name.', ';
                  }
                  if($offer_services){
                    $offer_services = rtrim($offer_services, ', ');
                  }
                  $item_name_arr = $bkitems->getAssociatedOfferDetail->getAssociatedOfferName->$title.'('.$offer_services.'), ';
                }
              }
              if($item_name_arr){
                $item_name_arr = rtrim($item_name_arr, ', ');
              }
             $nestedData['booking_items'] = $item_name_arr;
             $nestedData['action'] =  getButtons([
                ['key'=>'view','link'=>route('orders.show_order',[$val->id, 2])]]); 

            $data[] = $nestedData;
          }

        }
        //echo '<pre>'; print_r($data);
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }
}
