<?php

namespace App\Http\Controllers\SpAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;   

use App\Model\Product;
use App\Model\SpUser;
use datatables;
use App;
use Session;

class ProductController extends Controller
{
    protected $title;
    protected $model;
    protected $module;

    public function __construct()
    {
        $this->title = 'Product';
        $this->model = 'products';
        $this->module = 'product';
    } 

    public function index()
    {
        $title = __('messages.View Product');
        $module = $this->module;
        $model = $this->model;
        $breadcum = [$title=>route($model.'.index')];
        return view('spadmin.product.index',compact('title','model','module','breadcum'));
    }

    public function getData(Request $request)
    {
        $user_data = spLoginData();//helper function
        $columns = ['product_name','product_image'];

        $totalData = Product::where('sp_id',$user_data['id'])->count();
                
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        
        $row = Product::where('sp_id',$user_data['id']);
     
        // $totalFiltered = User::count();

        if (!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = $row->where(function($query) use ($search) {
                $query->where('product_name', 'LIKE', "%{$search}%")
                        ->orWhere('created_at', 'LIKE', "%{$search}%");
            });
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('id', 'desc')
                ->get();
        // print_r($users);die;
        $data = array();
        if (!empty($row)) {
            foreach ($row as $key => $rows) {
                $nestedData['product_name'] = $rows->product_name;
                $nestedData['product_image'] = '<img src="'.changeImageUrlForFileExist(asset("sp_uploads/products/".$rows->product_image)).'" width="60" height="50"/>';                
                $nestedData['action'] =  getButtons([
                                ['key'=>'edit','link'=>route('products.edit',[$rows->id])],
                                ['key'=>'delete','link'=>route('products.destroy',$rows->id)],
                            ]);
                
                $data[] = $nestedData;
            }

        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }
  
    public function create()
    {
        $action = "create";
        $title = __('messages.Create Product');
        $model = $this->model;
        $breadcum = [__('messages.Product')=>route($model.'.index'),$title =>''];
        return view('spadmin.product.create',compact('action','title','breadcum','model','module'));
    }

    
    public function store(Request $request)
    {
        try{
             if (Input::isMethod('post')) {
                $validation = array(
                    'product_name'=>'required|max:50',
                    'product_image'=>'required|mimes:jpeg,jpg,png,gif',
                );

                $validator = Validator::make(Input::all(), $validation);

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {
                    $user_data = spLoginData();//helper function
                    $row = new Product();
                    $row['sp_id'] = $user_data['id'];
                    $row['product_name'] = $request->product_name;
                   
                    if ($request->hasFile('product_image') && $request->file('product_image'))
                    {

                        $image = $request->image_cr;
                        list($type, $image) = explode(';', $image);
                        list(, $image)      = explode(',', $image);
                        $image = base64_decode($image);
                        $image_name= time().'.png';
                        $path = public_path('/sp_uploads/products/'.$image_name);
                        file_put_contents($path, $image);
                        $row->product_image = $image_name;
                    }

                    $row->save();
                    if($row->save())
                    {
                        Session::flash('success', __('messages.Product added successfully.'));
                        return redirect()->route('products.index');
                    }
                    
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

   
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        $rows = Product::where('id',$id)->first();
        $title = __('messages.Edit Product');
        $model = $this->model;
        $breadcum = [__('messages.Product')=>route($model.'.index'),$title =>''];

        return view('spadmin.product.edit',compact('rows','title','breadcum'));
    }

   
    public function update(Request $request, $id)
    {
        try{
             if (Input::isMethod('PATCH')) {
                $validation = array(
                    'product_name'=>'required|max:50',
                    'product_image'=>'mimes:jpeg,jpg,png,gif',
                );

                $validator = Validator::make(Input::all(), $validation);

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {
                    $user_data = spLoginData();//helper function
                    $row = Product::where('id',$id)->first();
                    $row['sp_id'] = $user_data['id'];
                    $row['product_name'] = $request->product_name;
                   
                    if ($request->hasFile('product_image') && $request->file('product_image'))
                    {
                        $image = $request->image_cr;
                        list($type, $image) = explode(';', $image);
                        list(, $image)      = explode(',', $image);
                        $image = base64_decode($image);
                        $image_name= time().'.png';
                        $path = public_path('/sp_uploads/products/'.$image_name);
                        file_put_contents($path, $image);
                        $row->product_image = $image_name;
                    }

                    $row->save();
                    if($row->save())
                    {
                        Session::flash('success', __('messages.Product updated successfully.'));
                        return redirect()->route('products.index');
                    }
                    
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

  
    public function destroy($id)
    {
        $row = Product::where('id', $id)->first();
        if ($row) {
            $row->delete();
             Session::flash('success', __('messages.Product deleted successfully.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('messages.Invalid request.'));
            return redirect()->back();
        }
    }
}
