<?php

namespace App\Http\Controllers\SpAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;  
use Session;
use App\Model\SpUser;
use App\Model\SpOffer;
use App\Model\SpService;
use App\Model\Tier;
use DB;
class AgreementController extends Controller
{
    public function index()
    {           
        $title = __('messages.View Agreement');
        $breadcum = [__('messages.Agreement') =>''];
        $user_data = spLoginData();
        $row = SpUser::where('id',$user_data['id'])->first();
        $tier = Tier::where('id',$row->tier_id)->first();
        return view('spadmin.agreement.index',compact('title','row','tier','breadcum'));
    }

    // public function addMov(Request $request)
    // {
    //     try
    //     {
    //         if($request->is_provide_home_service){
    //             $validation = array(
    //                 'mov'=>'required|numeric',
    //             );
    //         }else{
    //             $validation = array();
                
    //         }
            
    //         $validator = Validator::make(Input::all(), $validation);

    //         if ($validator->fails()) {

    //             return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
    //         }else{
                
    //             $user_data = spLoginData();
    //             $row = SpUser::where('id',$user_data['id'])->first();

    //             if(!$request->is_provide_home_service){
                   
    //                 $row->is_provide_home_service = '0';
    //                 $row->mov = 0;
    //                 $msg = __('messages.Home services removed successfully.');
    //             }else{
    //                 $row->is_provide_home_service = $request->is_provide_home_service;
    //                 $row->mov = $request->mov;
    //                 $msg = __('messages.Home services added successfully.');
    //             }
    //             $row->save();
    //             if($row->save())
    //             {
    //                 Session::flash('success', __($msg));
    //                 return redirect()->route('spuser.agreement');
    //             }
    //         }
            
    //     }
    //     catch(\Exception $e){
    //         $msg = $e->getMessage();
    //         Session::flash('danger', $msg);
    //         return redirect()->back()->withInput();
    //     }
    // }

    public function addMov(Request $request)
    {
        try
        {
            if($request->is_provide_home_service){
                $validation = array(
                    'mov'=>'required|numeric',
                );
            }else{
                $validation = array();
                
            }
            
            $validator = Validator::make(Input::all(), $validation);

            if ($validator->fails()) {

                return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
            }else{
                
                $user_data = spLoginData();
                $row = SpUser::where('id',$user_data['id'])->first();

                if(!$request->is_provide_home_service){
                   
                    $row->is_provide_home_service = '0';
                    $row->mov = 0;
                    $msg = __('messages.Home services removed successfully.');
                }else{
                    $row->is_provide_home_service = $request->is_provide_home_service;
                    $row->mov = $request->mov;
                    $msg = __('messages.Home services added successfully.');
                }
                // $row->save();
                if($row->save())
                {
                    if($row->is_provide_home_service!= "1"){
                            //update offers
                        $offers = SpOffer::where('user_id',$user_data['id'])->where('service_type','home')->get();

                        foreach($offers as $off){
                            $up_off = SpOffer::where('id',$off['id'])->first();
                            $up_off->previous_status = $up_off->status;
                            $up_off->status = '2';
                            $up_off->save();

                        }

                        //update services
                        $services = SpService::
                        leftJoin('sp_service_prices', function($join) {
                          $join->on('sp_services.id', '=', 'sp_service_prices.sp_service_id');
                        })
                        ->where('sp_service_prices.field_key', 'like', '%_HOME%')
                        ->where('user_id',$user_data['id'])
                        ->update(['sp_service_prices.field_key' => DB::raw('CONCAT( "IN_",sp_service_prices.field_key)')]);
                       // ->get()->toArray();
                        // print_r($services);die;
                    }else{

                        $offers = SpOffer::where('user_id',$user_data['id'])->where('service_type','home')->where('status','2')->get();

                        foreach($offers as $off){
                            $up_off = SpOffer::where('id',$off['id'])->first();
                            $up_off->status = $up_off->previous_status;
                            $up_off->save();

                        }

                        $services = SpService::
                        leftJoin('sp_service_prices', function($join) {
                          $join->on('sp_services.id', '=', 'sp_service_prices.sp_service_id');
                        })
                        ->where('sp_service_prices.field_key', 'like', '%_HOME%')
                        ->where('user_id',$user_data['id'])
                        ->update(['sp_service_prices.field_key' => DB::raw('REPLACE(field_key,"IN_","")')]);
                        // ->get()->toArray();
                        // print_r($services);die;
                    }
                        
                    Session::flash('success', __($msg));
                    return redirect()->route('spuser.agreement');
                }
            }
            
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }
}
