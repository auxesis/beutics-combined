<?php

namespace App\Http\Controllers\SpAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input; 

use Session;
use App\Model\SpOffer;
use App\Model\Offer;
use App\Model\SpUser;
use App\Model\SpService;
use App\Model\SpOfferService;
use App\Model\ServiceAttribute;
use App\Model\Service;
use DB;

class NewOffersController extends Controller
{
    
    public function index()
    {
        $title = __('messages.My Offers');
        $breadcum = [$title =>''];
        $user_data = spLoginData();
        $row = SpOffer::where('id',$user_data['id'])->get();
        return view('spadmin.offers.new.index',compact('title','row','breadcum'));
    }

    public function getData(Request $request)
    {
        $user_data = spLoginData();
        $columns = ['offer_id','spectacular_offer','is_nominated','gender','service_type','expiry_date','status','likes','Action'];
        $current_date = date('Y-m-d');

        $totalData = SpOffer::where('is_deleted','!=','1')->count();
      
        
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        $row = SpOffer::select('sp_offers.*')->where('user_id',$user_data['id'])->where('is_deleted','!=','1');


        if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = $row->leftJoin('offers', 'offers.id', '=', 'sp_offers.offer_id')
                    ->where(function($query) use ($search) {
                        $query->Where('offers.title', 'LIKE', "%{$search}%")
                        ->orWhere('sp_offers.id', '=', "{$search}");
                        
                    });
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('id', 'desc')
                ->get();
        // print_r($users);die;
        $data = array();
        if (!empty($row)) {
            $title = 'title'.$this->getLang();
            foreach ($row as $key => $rows) {
                $nestedData['id'] = $rows->id;
                $nestedData['offer_id'] = $rows->getAssociatedOfferName->$title;
                if($rows->spectacular_offer == '1'){
                    $nestedData['spectacular_offer'] = __('messages.Yes');
                }else{
                    $nestedData['spectacular_offer'] = __('messages.No');
                }

                if($rows->is_nominated == '1'){
                    $nestedData['is_nominated'] = __('messages.Yes');
                }else{
                    $nestedData['is_nominated'] = __('messages.No');
                }
                $nestedData['gender'] = $rows->gender;
                $nestedData['service_type'] = $rows->service_type;
                $nestedData['expiry_date'] = date('d-M-y h:i:s',strtotime($rows->expiry_date));
                if($rows->status == '1'){
                    $nestedData['status'] = __('messages.Approved');
                }else{
                    $nestedData['status'] = __('messages.Pending');
                }
                if(count($rows->getAssociatedOfferLikes)>0){
                  $nestedData['likes'] = count($rows->getAssociatedOfferLikes);
                }else{
                  $nestedData['likes'] = 0;
                }

                if($rows->status == '1'){

                 $nestedData['action'] =  getButtons([
                            ['key'=>'edit','link'=>route('new-offers.edit',[$rows->id])],
                            ['key'=>'view','link'=>route('new-offers.show',[$rows->id])],                                                        
                        ]);

                    if($rows->is_nominated == '1'){
                      $nestedData['action'] =  getButtons([
                            // ['key'=>'edit','link'=>route('offers.edit',[$rows->id])],
                            ['key'=>'view','link'=>route('new-offers.show',[$rows->id])],                                                        
                        ]);
                    }        
                }else{
                  $nestedData['action'] =  getButtons([
                            ['key'=>'edit','link'=>route('new-offers.edit',[$rows->id])],
                             ['key'=>'delete','link'=>route('new-offers.destroy',[$rows->id])],
                            ['key'=>'view','link'=>route('new-offers.show',[$rows->id])],                                                        
                        ]);
                        if($rows->is_nominated == '1'){
                      $nestedData['action'] =  getButtons([
                            // ['key'=>'edit','link'=>route('offers.edit',[$rows->id])],
                            ['key'=>'view','link'=>route('new-offers.show',[$rows->id])],                                                        
                        ]);
                    }       
                }

               
                $data[] = $nestedData;
            }

        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function create()
    {
        $action = "create";
        $title = __('messages.Create Offer');
        $model = 'offers';
        $breadcum = [__('messages.Offers')=>route('new-'.$model.'.index'),$title =>''];
        $user_data = spLoginData();
        $sp_row = SpUser::where('id',$user_data['id'])->first();
        $titlee = 'title'.$this->getLang();
        $offer_name_rows = Offer::select('id',$titlee)->get();
        $delivery_mode = explode(',',$user_data['delivery_mode']);
        return view('spadmin.offers.new.create',compact('action','title','titlee','module','offer_name_rows','sp_row','breadcum','model','delivery_mode'));
    }

    public function ajaxGetServices(Request $request)
    {
      try
      {               
          $user_data = spLoginData();
          $attribute_arr = $this->getCategoryServiceAttribute('id','name');
          $mainKey = array('men','women','kids');
          $serviceKey = array('store','home','online');
          $key_arr = $this->getOffersKeyAttributeArr($attribute_arr);

          $services = SpService::
          select('sp_services.*')
          ->leftjoin('services', 'services.id', '=', 'sp_services.service_id')
          ->leftjoin('sp_service_prices', 'sp_services.id', '=', 'sp_service_prices.sp_service_id')
          ->where('sp_services.user_id', '=', $user_data['id'])
          ->where('is_deleted','!=','1')
          // ->where('services.name', 'LIKE', "%{$request->service_name}%")
          ->groupBy('sp_services.id')
          ->orderBy('sp_services.service_id','DESC')->get();
          
          $service_row = array();
          foreach($services as $key => $row)
          {
            $service_row_current = array();

            foreach($row->getRelatedPrices as $pricerow){
              if(array_key_exists($pricerow['service_attr_id'],$attribute_arr) && in_array($pricerow['field_key'],$key_arr[$request->gender][$request->service_type][$pricerow['service_attr_id']] ))
                {
                  $nameArr = array();
                  if($row['filter_key'] ==='group' || $row['filter_key'] ==='single')
                  { 
                    foreach($row->getAssociatedGroupService as $groupservice)
                    {
                        $parentServiceRow = $groupservice->getParentServiceId;
                        $parentRow = $groupservice->getAssociatedParentService($parentServiceRow->id);
                        if($parentRow != '')
                            $nameArr[] = $parentRow->name.' - '.$groupservice->getAssociatedService->name;
                        else
                            $nameArr[] = $groupservice->getAssociatedService->name;
                    }
                    $serviceName = implode(' | ',$nameArr);
                  }
                  else
                  {
                    $parentServiceRow = $row->getParentServiceId;
                    $parentRow = $row->getAssociatedParentService($parentServiceRow->id);
                    if($parentRow != '')
                    {
                        $serviceName = $parentRow->name.' - '.$row->getAssociatedService->name;
                    }
                    else
                    {
                        $serviceName = $row->getAssociatedService->name;
                    }
                  }
                  $key_name = explode('_',$pricerow['field_key']);
                  $service_row_current[$pricerow['service_attr_id']]['sp_service_id'] = $row['id'];
                  $service_row_current[$pricerow['service_attr_id']]['price_id'] = $pricerow['id'];
                  $service_row_current[$pricerow['service_attr_id']][$key_name[count($key_name) - 1]] = $pricerow['price'];
                  $service_row_current[$pricerow['service_attr_id']]['name'] = $serviceName;
                  $service_row_current[$pricerow['service_attr_id']]['service_attr'] = $attribute_arr[$pricerow['service_attr_id']];   
                } 
            }
            if(!empty($service_row_current)){
                $service_row[] = $service_row_current;
            }
          }

          return view('spadmin.offers.new.ajaxServiceListing',compact('service_row','attribute_arr'));  
      }
      catch(\Exception $e){
        $msg = $e->getMessage();
        Session::flash('danger', $msg);
        return redirect()->back()->withInput();
      }
    }

    public function ajaxShowServiceListing(Request $request)
    {
      $service_data = explode(',',$request->servicedata);
      return view('spadmin.offers.new.ajaxShowServiceListing',compact('service_data'));
    }

    public function store(Request $request)
    {
        try
           {
              $data = $request->all();
              $validator = Validator::make($data, 
                [
                  'offer_id' => 'required',
                  // 'spectacular_offer' => 'required',
                  'offer_details' => 'required',
                  'gender' => 'required|in:men,women,kids',
                  'service_type' => 'required|in:home,store,online',
                  'total_price' => 'required',
                  'discount' => 'required',
                  'best_price' => 'required',
                  'expiry_date' => 'required',
                  'offer_terms' => 'required',
                  'services' => 'required',
                ]);
              if ($validator->fails()) 
              {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
              } 
              else 
              {  
                $user_data = spLoginData();
                $user_id = $user_data['id'];    
                $row = new SpOffer();
            
                $row->user_id =$user_id;
                $row->offer_id =$request->offer_id;
                if($request->spectacular_offer){
                  $row->spectacular_offer = $request->spectacular_offer;
                }
                else{
                  $row->spectacular_offer = '0';
                }
                $row->offer_details = $request->offer_details;
                $row->gender = $request->gender;
                $row->service_type = $request->service_type;
                $row->total_price = $request->total_price;
                $row->discount = $request->discount;
                $row->best_price = $request->best_price;
                $row->expiry_date = date('Y-m-d',strtotime($request->expiry_date));
                $row->offer_terms = $request->offer_terms;
                
                $row->save();
                if($row->save())
                {
                    $service_arr = $request->services;
                    
                    foreach($service_arr as $key => $sr_row){
                        $service_id_key = explode('_',$key);
                        $service_row = new SpOfferService();
                        $service_row->sp_offer_id = $row->id;
                        $service_row->sp_service_id = $service_id_key[0];
                        $service_row->service_attr_id = $service_id_key[1];
                        $service_row->quantity = $sr_row;
                        $service_row->save();
                    }

                    //mail to admin
                    $data1['templete'] = "admin_mail";
                    $data1['email'] = env("CONTACT_US_EMAIL");
                    $data1['subject'] = "New Offer Approval";
                    $data1['message'] = $row->getAssociatedOfferProviderName->store_name." has created a new offer. Kindly review and approve if necessary. ";
                    //send($data1);
              
                    Session::flash('success', __('messages.Offer added successfully.'));
                    return redirect()->route('new-offers.index');
                }           
              }
           }
           catch(\Exception $e){
             $msg = $e->getMessage();
             Session::flash('danger', $msg);
             return redirect()->back()->withInput();
           }
    }

    
    public function show($id)
    {
        $title = __('messages.View');
        $model = 'offers';
        $rows = SpOffer::where('id',$id)->first();
        $breadcum = [__('messages.Offers')=>route('new-'.$model.'.index'),$title =>''];

        $total_bestprice = 0;
        $attribute_arr = $this->getCategoryServiceAttribute('id','name');
        $key_arr = $this->getOffersKeyAttributeArr($attribute_arr);
      
        $name = 'name'.$this->getLang(); 
        foreach($rows->getAssociatedOfferServices as $srow)
        {
          $nameArr = array();
          if($srow->getAssociatedOfferServicesName->filter_key ==='single' || $srow->getAssociatedOfferServicesName->filter_key ==='group')
          {
              foreach($srow->getAssociatedOfferServicesName->getAssociatedGroupService as $groupservice)
              {
                  $parentServiceRow = $groupservice->getParentServiceId;
                  $parentRow = $groupservice->getAssociatedParentService($parentServiceRow->id);
                  if($parentRow != '')
                  {
                      $serviceName = $parentRow->name.' - '.$groupservice->getAssociatedService->name;
                  }
                  else
                  {
                      $serviceName = $groupservice->getAssociatedService->name;
                  }
                  $nameArr[] = $serviceName;
              }
              $serviceName = implode(' | ',$nameArr);
          }
          else
          {
            $parentServiceRow = $srow->getAssociatedOfferServicesName->getParentServiceId;
            $parentRow = $srow->getAssociatedOfferServicesName->getAssociatedParentService($parentServiceRow->id);
            
            if($parentRow != '')
            {
                $serviceName = $parentRow->$name.' - '.$srow->getAssociatedOfferServicesName->getAssociatedService->$name;
            }
            else
            {
                $serviceName = $srow->getAssociatedOfferServicesName->getAssociatedService->$name;
            }
          }

          $offers_service1 = [
            'quantity' => $srow->quantity,
            'name' => $serviceName.' ('.$attribute_arr[$srow['service_attr_id']].')',
            
          ];

          foreach($srow->getAssociatedOfferServicesPrices as $pricerow)
          {
            if(in_array($pricerow['field_key'],$key_arr[$rows->gender][$rows->service_type][$srow['service_attr_id']]) && ($srow['service_attr_id'] == $pricerow['service_attr_id']) )
            {
              if (strpos($pricerow['field_key'], 'IN_')!== false) { 
                $changecase = 'IN_'.strtoupper($rows->gender).'_';
              }else{
                $changecase = strtoupper($rows->gender).'_';
              }
              // echo $changecase = strtoupper('IN_'.$row->gender).'_';die;
              $replace = str_replace($changecase, '', $pricerow['field_key']);
              $offers_service1[$replace] = $pricerow['price'];
            }
          }
          $original = '';
            if(isset($offers_service1['ORIGINALPRICE'])){
              $original = 'ORIGINALPRICE';
            }else{
              $original = 'HOME_ORIGINALPRICE';
            }
            $total_bestprice = $total_bestprice+($offers_service1[$original]*$srow->quantity);
            $offers_service[] = $offers_service1;
        }
     
      $cal_bestprice =  $total_bestprice - (($total_bestprice*$rows->discount)/100);
      $rows['best_price'] = number_format((float)$cal_bestprice, 2, '.', '');
      $lang_title = 'title'.$this->getLang();
      return view('spadmin.offers.new.view',compact('title','model','module','breadcum','rows','offers_service','total_bestprice', 'lang_title'));
    }

  
    public function edit($id)
    {
        $action = "edit";
        $title = __('messages.Edit Offer');
        $model = 'offers';
        $breadcum = [__('messages.Offers')=>route($model.'.index'),$title =>''];
        $user_data = spLoginData();
        $sp_row = SpUser::where('id',$user_data['id'])->first();
        $titlee = 'title'.$this->getLang();
        $offer_name_rows = Offer::select('id',$titlee)->get();
        $rows = SpOffer::where('id',$id)->first();
        $attribute_arr = $this->getCategoryServiceAttribute('id','name');
        $key_arr = $this->getOffersKeyAttributeArr($attribute_arr);
        $delivery_mode = explode(',',$user_data['delivery_mode']);

        $service_row = array();
        if($rows->getAssociatedOfferServices)
        {
          foreach($rows->getAssociatedOfferServices as $row)
          {
            $service_row_current = array();
            foreach($row->getAssociatedOfferServicesPrices as $pricerow)
            {
              if(isset($key_arr[$rows->gender][$rows->service_type][$row['service_attr_id']]) && in_array($pricerow['field_key'],$key_arr[$rows->gender][$rows->service_type][$row['service_attr_id']]) && ($row['service_attr_id'] == $pricerow['service_attr_id']) )
                {
                  $nameArr = array();
                  if($row->getAssociatedOfferServicesName->filter_key ==='single' || $row->getAssociatedOfferServicesName->filter_key ==='group')
                  {
                      foreach($row->getAssociatedOfferServicesName->getAssociatedGroupService as $groupservice)
                      {
                          $parentServiceRow = $groupservice->getParentServiceId;
                          $parentRow = $groupservice->getAssociatedParentService($parentServiceRow->id);
                          if($parentRow != '')
                          {
                              $serviceName = $parentRow->name.' - '.$groupservice->getAssociatedService->name;
                          }
                          else
                          {
                              $serviceName = $groupservice->getAssociatedService->name;
                          }
                          $nameArr[] = $serviceName;
                      }
                      $serviceName = implode(' | ',$nameArr);
                  }
                  else
                  {
                    $parentServiceRow = $row->getAssociatedOfferServicesName->getParentServiceId;
                    $parentRow = $row->getAssociatedOfferServicesName->getAssociatedParentService($parentServiceRow->id);
                    
                    if($parentRow != '')
                    {
                        $serviceName = $parentRow->name.' - '.$row->getAssociatedOfferServicesName->getAssociatedService->name;
                    }
                    else
                    {
                        $serviceName = $row->getAssociatedOfferServicesName->getAssociatedService->name;
                    }
                  }

                  $changecase = strtoupper($rows->gender).'_';
                  $replace = str_replace($changecase, '', $pricerow['field_key']);
                  $replace_arr = explode('_',$replace);

                  $service_row_current[$pricerow['service_attr_id']]['sp_service_id'] = $row['sp_service_id'];
                  $service_row_current[$pricerow['service_attr_id']][$replace_arr[count($replace_arr)-1]] = $pricerow['price'];
                  $service_row_current[$pricerow['service_attr_id']]['name'] = $serviceName;
                  $service_row_current[$pricerow['service_attr_id']]['service_attr'] = $attribute_arr[$pricerow['service_attr_id']];
                  $service_row_current[$pricerow['service_attr_id']]['quantity'] = $row->quantity;
                }
            }

            if(!empty($service_row_current)){
                $service_row[] = $service_row_current;
            }
          }
        }
        //dd($service_row);
        return view('spadmin.offers.new.edit',compact('action','title','titlee','module','offer_name_rows','sp_row','breadcum','model','rows','attribute_arr','key_arr','delivery_mode','service_row'));
    }

   
    public function update(Request $request, $id)
    {
        try
           {
              $data = $request->all();
              $validator = Validator::make($data, 
                [
                  'offer_id' => 'required',
                  // 'spectacular_offer' => 'required',
                  'offer_details' => 'required',
                  'gender' => 'required|in:men,women,kids',
                  'service_type' => 'required|in:home,store,online',
                  'total_price' => 'required',
                  'discount' => 'required',
                  'best_price' => 'required',
                  'expiry_date' => 'required',
                  'offer_terms' => 'required',
                  'services' => 'required',
                ]);
              if ($validator->fails()) 
              {
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
              } 
              else 
              {  
                $user_data = spLoginData();
                $user_id = $user_data['id'];    
                $row = SpOffer::where('id',$id)->first();
            
                $row->user_id =$user_id;
                $row->offer_id =$request->offer_id;
                if($request->spectacular_offer){
                  $row->spectacular_offer = $request->spectacular_offer;
                }
                else{
                  $row->spectacular_offer = '0';
                }
                $row->offer_details = $request->offer_details;
                $row->gender = $request->gender;
                $row->service_type = $request->service_type;
                $row->total_price = $request->total_price;
                $row->discount = $request->discount;
                $row->best_price = $request->best_price;
                $row->expiry_date = date('Y-m-d',strtotime($request->expiry_date));
                $row->offer_terms = $request->offer_terms;

                $row->save();
                if($row->save())
                {
                    SpOfferService::where('sp_offer_id',$id)->delete();
                    $service_arr = $request->services;
                    
                    foreach($service_arr as $key => $sr_row){
                        $service_id_key = explode('_',$key);
                        $service_row = new SpOfferService();
                        $service_row->sp_offer_id = $row->id;
                        $service_row->sp_service_id = $service_id_key[0];
                        $service_row->service_attr_id = $service_id_key[1];
                        $service_row->quantity = $sr_row;
                        $service_row->save();
                    }
                    Session::flash('success', __('messages.Offer updated successfully.'));
                    return redirect()->route('new-offers.index');
                }           
              }
           }
           catch(\Exception $e){
             $msg = $e->getMessage();
             Session::flash('danger', $msg);
             return redirect()->back()->withInput();
           }
    }

   
    public function destroy($id)
    {
        $row = SpOffer::where('id', $id)->first();
        if ($row) {
            // $row->delete();
            $row->is_deleted = '1';
            $row->save();

             Session::flash('success', __('messages.Offer deleted successfully.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('messages.Invalid request.'));
            return redirect()->back();
        }
    }

    public function getCategoryServiceAttribute($key,$value)
    {
        $user_data = spLoginData();//helper function
        $cat_id = $user_data['category_id'];
        $attribute_arr = ServiceAttribute::where('cat_id',$cat_id)->orderby('name', 'asc')->pluck($value,$key)->toArray();
        return $attribute_arr;
    }

    public function getOffersKeyAttributeArr($attribute_arr)
    {
      $key_arr = array();
      $mainKey = array('men','women','kids');
      $serviceKey = array('store','home','online');
      $pricekey_arr = [
        'men' => [
            'store' => ['MEN_ORIGINALPRICE','MEN_DISCOUNT','MEN_BESTPRICE'],
            'home' => ['MEN_HOME_ORIGINALPRICE','MEN_HOME_DISCOUNT','MEN_HOME_BESTPRICE'],
            'online' => ['MEN_ONLINE_ORIGINALPRICE','MEN_ONLINE_DISCOUNT','MEN_ONLINE_BESTPRICE'],
        ],
        'women' => [
            'store' => ['WOMEN_ORIGINALPRICE','WOMEN_DISCOUNT','WOMEN_BESTPRICE'],
            'home' => ['WOMEN_HOME_ORIGINALPRICE','WOMEN_HOME_DISCOUNT','WOMEN_HOME_BESTPRICE'],
            'online' => ['WOMEN_ONLINE_ORIGINALPRICE','WOMEN_ONLINE_DISCOUNT','WOMEN_ONLINE_BESTPRICE'],
        ],
        'kids' => [
            'store' => ['KIDS_ORIGINALPRICE','KIDS_DISCOUNT','KIDS_BESTPRICE'],
            'home' => ['KIDS_HOME_ORIGINALPRICE','KIDS_HOME_DISCOUNT','KIDS_HOME_BESTPRICE'],
            'online' => ['KIDS_ONLINE_ORIGINALPRICE','KIDS_ONLINE_DISCOUNT','KIDS_ONLINE_BESTPRICE'],
        ],
      ];

      foreach($mainKey as $gender_key => $gender_val)
      { 
        foreach($serviceKey as $provider_key => $provider_val)
        {
          foreach($attribute_arr as $attr_key => $attr_val)
          {
            $key_arr[$gender_val][$provider_val][$attr_key] = $pricekey_arr[$gender_val][$provider_val];
          }
        }
      }

      return $key_arr;
    }
}
