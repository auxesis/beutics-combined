<?php

namespace App\Http\Controllers\SpAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;   

use App\Model\SpGallery;
use App\Model\SpTags;
use App\Model\SpUser;
use datatables;
use App;
use Session;
use DB;

class GalleryController extends Controller
{
    protected $title;
    protected $model;
    protected $module;

    public function __construct()
    {
        $this->title = 'Gallery';
        $this->model = 'gallery';
        $this->module = 'gallery';
    } 

    public function index()
    {
        $title = __('messages.View Gallery');
        $module = $this->module;
        $model = $this->model;
        $breadcum = [$title=>'my-'.$model.'.index'];
        
        return view('spadmin.gallery.index',compact('title','model','module','breadcum','data'));
    }

    public function getData(Request $request)
    {
        $user_data = spLoginData();//helper function
        $columns = ['tag_name','title','file_name'];

        $totalData = SpGallery::where('sp_id',$user_data['id'])->where('sp_gallery.status','1')->count();
              
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        
        $row = SpGallery::where('sp_id',$user_data['id'])->where('sp_gallery.status','1');
        // $totalFiltered = User::count();

        if (!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = $row->where(function($query) use ($search) {
                $query->where('title', 'LIKE', "%{$search}%");
            });
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('id', 'desc')
                ->get();

        $tagsArr  = SpTags::pluck('tag_name', 'id')->toArray();
        $data = array();
        if (!empty($row)) {
            foreach ($row as $key => $rows) 
            {
                $tag_names = array();
                $mimetype = substr($rows->mime_type,0,5);
                if($mimetype == 'image')
                {
                    if($rows->file_name!='')
                        $file_name = '<img src="'.changeImageUrlForFileExist(asset("sp_uploads/gallery/".$mimetype."/".$rows->file_name)).'" width="60" height="50"/>';
                    else
                        $file_name = '<img src="'.changeImageUrlForFileExist(asset("images/defualt.jpeg")).'" alt="..." height="60" width="50">';
                    
                }
                elseif($mimetype == 'video')
                {
                    $file_name = '<a href="'.asset("public/sp_uploads/gallery/".$mimetype."/".$rows->file_name).'" download>'.$rows->file_name.'</a>';
                }
                else
                    $file_name = $rows->file_name;

                 // get tag names
                $tagIds = explode(',',$rows->tag_id);
                foreach($tagIds as $id)
                {
                    if(array_key_exists($id,$tagsArr))
                        $tag_names[] = $tagsArr[$id];
                }

                $nestedData['tag_name'] = implode(', ',$tag_names);
                $nestedData['title'] = $rows->title;
                $nestedData['file_name'] = $file_name;                
                $nestedData['action'] =  getButtons([
                                ['key'=>'edit','link'=>route('my-gallery.edit',[$rows->id])],
                                ['key'=>'delete','link'=>route('my-gallery.destroy',$rows->id)],
                            ]);
                
                $data[] = $nestedData;
            }

        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }
  
    public function create()
    {
        $action = "create";
        $title = __('messages.Create Media');
        $model = $this->model;
        $breadcum = [__('messages.Gallery')=>route('my-'.$model.'.index'),$title =>''];
        $all_tags  = SpTags::orderby('tag_name', 'asc')->pluck('tag_name', 'id')->toArray();
        return view('spadmin.gallery.create',compact('action','title','breadcum','model','module','all_tags'));
    }
    
    public function store(Request $request)
    {
        try{
             if (Input::isMethod('post')) {
                $validation = array(
                    'tags'=>'required',
                    'file_name' => 'required',
                    'title' => 'required',
                    //'file_image'=>'required|mimes:jpeg,jpg,png,gif',
                );

                $desc = explode(' ',$request->file_desc);
                $validator = Validator::make(Input::all(), $validation);

                if(count($desc)>500){
                     $validator -> errors() -> add('file_desc', 'Description cannot be more than 500 words.');
                     return redirect()->back()
                    -> withErrors($validator)
                    -> withInput(Input::all());
                }

                if($request->file_name == 'url')
                {
                    if(!isValidYoutubeUrl($request->file_url))
                    {
                        $validator -> errors() -> add('file_url', 'Please Enter Valid Youtube Url.');
                         return redirect()->back()
                        -> withErrors($validator)
                        -> withInput(Input::all());
                    }

                }
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {
                
                    $user_data = spLoginData();//helper function
                    $all_tags = SpTags::pluck('tag_name', 'id')->toArray();
                    $tag_ids = array();
                    foreach($request->tags as $tag_name_id)
                    {
                        if(array_key_exists($tag_name_id,$all_tags))
                        {
                            $tag_ids[] = $tag_name_id;
                        }
                        else
                        {
                            $tagsArr = array('all','ALL','All','aLL');
                            if(in_array($tag_name_id,$tagsArr))
                            {
                                $validator -> errors() -> add('tags', 'You Can Not create a tag with All names.');
                                 return redirect()->back()
                                -> withErrors($validator)
                                -> withInput(Input::all());
                            }
                            else
                            {
                                $tag_row = new SpTags();
                                $tag_row['tag_name'] = ucwords($tag_name_id);
                                $tag_row->save();
                                $tag_ids[] = $tag_row->id;
                            }
                        }
                    }
                   
                    $row = new SpGallery();
                    $row->sp_id = $user_data['id'];

                    if($request->file_name == 'image' || $request->file_name == 'video')
                    {
                        if ($request->hasFile('file_'.$request->file_name) && $request->file('file_'.$request->file_name))
                        {
                            $filetype = 'file_'.$request->file_name;
                            $file = $request->$filetype;
                            $getmimeType = $file->getClientMimeType();
                            $file_name = time().str_random(2).'.'.$file->getClientOriginalExtension();
                            $mimeType = substr($getmimeType,0,5);
                            $path = public_path('/sp_uploads/gallery/'.$mimeType.'/');
                            $file->move($path, $file_name);
                            $row->file_name = $file_name;
                            $row->mime_type = $getmimeType; 
                        }
                    }
                    else
                    {
                       $row->file_name = $request->file_url;
                       $row->mime_type = 'url';
                    }
                    
                    $row->tag_id = implode(',',$tag_ids);
                    $row->title = ucfirst($request->title);
                    $row->file_desc = $request->file_desc;
                    $row->sort_order = $request->sort_order;
                    $row->save();

                    if($row->save())
                    {
                        Session::flash('success', __('messages.File added successfully.'));
                        return redirect()->route('my-gallery.index');
                    }
                    
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

   
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        $rows = SpGallery::where('id',$id)->first();
        $title = __('messages.Edit Media');
        $model = $this->model;
        $breadcum = [__('messages.Gallery')=>route('my-'.$model.'.index'),$title =>''];
        $all_tags  = SpTags::orderby('tag_name', 'asc')->pluck('tag_name', 'id')->toArray();
        return view('spadmin.gallery.edit',compact('rows','title','breadcum','all_tags'));
    }

   
    public function update(Request $request, $id)
    {   
        try{
             if (Input::isMethod('PATCH')) {
                $validation = array(
                    'tags'=>'required',
                    'file_name' => 'required',
                    'title' => 'required',
                );

                $desc = explode(' ',$request->file_desc);
                $validator = Validator::make(Input::all(), $validation);

                if(count($desc)>500){
                     $validator -> errors() -> add('file_desc', 'Description cannot be more than 500 words.');
                     return redirect()->back()
                    -> withErrors($validator)
                    -> withInput(Input::all());
                }

                if($request->file_name == 'url')
                {
                    if(!isValidYoutubeUrl($request->file_url))
                    {
                        $validator -> errors() -> add('file_url', 'Please Enter Valid Youtube Url.');
                         return redirect()->back()
                        -> withErrors($validator)
                        -> withInput(Input::all());
                    }

                }

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {
                    $user_data = spLoginData();//helper function
                    $row = SpGallery::where('id',$id)->first();
                    $row->sp_id = $user_data['id'];
                    $all_tags = SpTags::pluck('tag_name', 'id')->toArray();
                    $tag_ids = array();

                    foreach($request->tags as $tag_name_id)
                    {
                        if(array_key_exists($tag_name_id,$all_tags))
                        {
                            $tag_ids[] = $tag_name_id;
                        }
                        else
                        {
                            $tagsArr = array('all','ALL','All','aLL');
                            if(in_array($tag_name_id,$tagsArr))
                            {
                                $validator -> errors() -> add('tags', 'You Can Not create a tag with All names.');
                                 return redirect()->back()
                                -> withErrors($validator)
                                -> withInput(Input::all());
                            }
                            else
                            {
                                $tag_row = new SpTags();
                                $tag_row['tag_name'] = ucwords($tag_name_id);
                                $tag_row->save();
                                $tag_ids[] = $tag_row->id;
                            }
                        }
                    }

                    if($request->file_name == 'image' || $request->file_name == 'video')
                    {
                        if ($request->hasFile('file_'.$request->file_name) && $request->file('file_'.$request->file_name))
                        {
                            $filetype = 'file_'.$request->file_name;
                            $file = $request->$filetype;
                            $getmimeType = $file->getClientMimeType();
                            $file_name = time().str_random(2).'.'.$file->getClientOriginalExtension();
                            $mimeType = substr($getmimeType,0,5);
                            $path = public_path('/sp_uploads/gallery/'.$mimeType.'/');
                            $file->move($path, $file_name);
                            $row->file_name = $file_name;
                            $row->mime_type = $getmimeType; 
                        }
                    }
                    else
                    {
                       $row->file_name = $request->file_url;
                       $row->mime_type = 'url';
                    }
                    
                    $row->tag_id = implode(',',$tag_ids);
                    $row->title = ucfirst($request->title);
                    $row->file_desc = $request->file_desc;
                    $row->sort_order = $request->sort_order;
                    $row->save();
                   
                    if($row->save())
                    {
                        Session::flash('success', __('messages.File updated successfully.'));
                        return redirect()->route('my-gallery.index');
                    }
                    
                }
             }
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

  
    public function destroy($id)
    {
        $row = SpGallery::where('id', $id)->first();
        if ($row) {
            $row->delete();
             Session::flash('success', __('messages.File deleted successfully.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('messages.Invalid request.'));
            return redirect()->back();
        }
    }
}
