<?php

namespace App\Http\Controllers\SpAdmin;
/* hgfiuh*/
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input; 

use Session;
use App\Model\SpUser;
use App\Model\StaffLeave;
use App\Model\User;
use App\Model\Booking;
use App\Model\BookingItem;
use App\Model\PromoCodeLinkedCategory;
use App\Model\PromoCodeLinkedCustomer;
use App\Model\PromoCodeLinkedService;
use App\Model\PromoCodeLinkedStore;
use App\Model\PromoCodeLinkedSubcategory;
use App\Model\CashbackHistory;
use App\Model\RescheduleOrderHistory;
use App\Model\ShopTiming;
use App\Model\Staff;
use App\Model\Notification;
use App\Model\Country;
use DB;
use Config;

use App\Model\Conversation;
use App\Model\Chat;

class OrdersController extends Controller
{
    
    public function index()
    {
        $title = __('messages.My Orders');
        $breadcum = [$title =>''];
        $user_data = spLoginData(); 
       // echo '<pre>'; print_r($user_data);
        //$rows = Booking::where('sp_id',1)->get()->toArray();
        //echo '<pre>'; print_r($rows);die;
        return view('spadmin.orders.index',compact('title','row','breadcum'));
    }

    public function getData(Request $request)
    {
        $user_data = spLoginData();
        $columns = ['id','tax_amount', 'total_amount','paid_amount','created_date','db_user_name','db_user_mobile_no','booking_unique_id','appointment_date','appointment_time','status','service_end_time','Action'];
        $order = $columns[$request->input('order.0.column')];

        $totalData = Booking::count();    
        $limit = $request->input('length');
        $start = $request->input('start');     

        $status_arr = array('0'=>'Awaiting','1'=>'Confirmed','2'=>'Cancelled','3'=>'Refunded','4'=>'Expired','5'=>'Completed');
        
        if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                })
                ->where(function ($query) use ($search){
                        $query->orWhere('name', 'like', '%' .$search . '%')
                  ->orWhere('mobile_no', 'like', '%' .$search . '%');
                    })
                
                ->select('bookings.*', 'users.name', 'users.mobile_no')
                ->where('bookings.status','0')
                ->where('bookings.user_id','!=','')
                ->where('bookings.sp_id',$user_data['id'])
                ->orderBy('bookings.id','desc');
        } else {
          // $row = Booking::select('bookings.*')->where('sp_id',$user_data['id']);
          $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                }) 
          ->select('bookings.*', 'users.name', 'users.mobile_no')
            ->where('bookings.sp_id',$user_data['id'])
            ->where('bookings.user_id','!=','')
            ->where('bookings.status','0');
        }
       
        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('bookings.id', 'desc')
                ->get();
        //echo'<pre>'; print_r($row);die;
        $data = array();

        if (!empty($row)) {
            foreach($row as $val){
              $date = $val['appointment_date'];
              $time = $val['appointment_time'];
              $combinedDT = '';
              if($date!='' && $date!=null && $time!=null && $time!=''){
                 $combinedDT = date('M d, Y h:i A', strtotime("$date $time"));
               }else{
                $combinedDT = 'NA';
               }

               $is_referred = '<p style="color:red;"><i>('.__('messages.New Customer').')</i></p>';
                //check customer uses sp refer code or not

                if($val['user_id']!='' && $val['user_id']!=null){
                  $cust_refer_code = $val->getAssociatedUserInfo->refer_code;
                  // echo $cust_refer_code;die;
                  $check_sp_code = SpUser::where('share_code',$cust_refer_code)->where('id',$val['sp_id'])->exists();
                  if($check_sp_code){
                    $is_referred = '<p style="color:red;"><i>('.__('messages.Sp Referred').')</i></p>';
                  }
                }


            $nestedData = [
              "id" => $val['id'],
              "booking_unique_id" => $val['booking_unique_id'],
              "booking_service_type" => $val['booking_service_type'],
              "name" => $val['name'].'<br/>'.$is_referred,
              "mobile_no" => $val['mobile_no'],
              "appointment_date" => $combinedDT,
              // "service_end_time" => $val['service_end_time']!='' && $val['service_end_time']!=null ? date('h:i A',strtotime($val['service_end_time'])):"",
              
              "tax_amount" => round($val['tax_amount'],2),
              "total_amount" => round($val['booking_total_amount'],2),
              "paid_amount" => round($val['paid_amount'],2),
              "created_date" => date('Y-m-d h:i A',strtotime($val['created_at'])),
              "status" => $status_arr[$val['status']],
            ];
            if(!$val['user_id'] && !$val['is_gift'] && ($val['db_user_name'] || $val['db_user_mobile_no'])){
              $nestedData['name'] = $val['db_user_name'];
              $nestedData['mobile_no'] = $val['db_user_mobile_no'];
            }
            if(!$val['user_id'] && $val['is_gift'] && ($val['receiver_name'] || $val['receiver_mobile_no'])){
              $nestedData['name'] = $val['receiver_name'];
              $nestedData['mobile_no'] = $val['receiver_mobile_no'];
            }
            $nestedData['action'] =  getButtons([
                            ['key'=>'chat','link'=>route('orders.myOrderChatHistory',$val->id)],
                            ['key'=>'confirm','link'=>route('orders.order_confirm_view',[$val->id])],
                            ['key'=>'view','link'=>route('orders.show_order',[$val->id, ' '])]
                                                                                    
                        ]);
            $data[] = $nestedData;
          }

        }
        
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function myOrderChatHistory($id)
    {
        
        $user = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                }) 
          ->select('bookings.*', 'users.image',  'users.name', 'users.mobile_no')
            ->where('bookings.id',$id)
            ->where('bookings.user_id','!=','')
            ->where('bookings.status','0')
            ->first();

            // dd($user);

        if ($user) {

            $user_data = spLoginData();
            // echo "user ".
            $userId=$user_data->id;

            // echo "<br/>";

            $udata = $user->toArray();

            $title = $udata['name'];            

            // echo "otherId ".
            $otherId = $udata['user_id'];

            // echo "<br/>";
            $booking_id=$udata['id'];

            $groupId   = ($userId>$otherId)?$userId."".$otherId."".$booking_id:$otherId."".$userId."".$booking_id;
            $groupId   = 'SU-'.$groupId;
            $chats = Chat::where("group_id",$groupId)->get()->toArray();  

            $other['id']          = $otherId;
            $other['image']       = (!empty($udata['image']))?url('/public/sp_uploads/profile/'.$udata['image']):'https://ptetutorials.com/images/user-profile.png';;
            $other['full_name']   = $udata['name'];
            $other['booking_id']   = $booking_id;

            // dd($chats);

            $user['user_id']          = $userId;
            $user['user_image']       = (!empty($user_data->profile_image))?url('/public/sp_uploads/profile/'.$user_data->profile_image):'https://ptetutorials.com/images/user-profile.png';
            $user['user_name']   = ucfirst($user_data->store_name);

            $breadcum = ['orders.index'];
            return view('spadmin.orders.myOrderChat',compact('userId','user','groupId','title','other','breadcum','chats')); 

        }else{

            Session::flash('warning', __('Invalid request.'));
            return redirect()->route('orders.index');
        }

    }


    public function confirmed_orders()
    {
        $title = __('messages.My Orders');
        $breadcum = [$title =>''];
        $user_data = spLoginData(); 
       // echo '<pre>'; print_r($user_data);
       // $rows = Booking::where('sp_id',1)->get()->toArray();
        //echo '<pre>'; print_r($rows);die;
        return view('spadmin.orders.ConfirmedOrders',compact('title','row','breadcum'));
    }

    public function getDataConfirmed(Request $request)
    {
        $user_data = spLoginData();
        $columns = ['id','tax_amount', 'total_amount','paid_amount','created_date','db_user_name','db_user_mobile_no','booking_unique_id','appointment_date','appointment_time','status','service_end_time','Action'];
        $order = $columns[$request->input('order.0.column')];

        $totalData = Booking::count();    
        $limit = $request->input('length');
        $start = $request->input('start');     

        $status_arr = array('0'=>'Awaiting','1'=>'Confirmed','2'=>'Cancelled','3'=>'Refunded','4'=>'Expired','5'=>'Completed');
        
        if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                })
                ->where(function ($query) use ($search){
                        $query->orWhere('name', 'like', '%' .$search . '%')
                  ->orWhere('mobile_no', 'like', '%' .$search . '%');
                    })
                
                ->select('bookings.*', 'users.name', 'users.mobile_no')
                ->where('bookings.status','1')
                ->where('bookings.sp_id',$user_data['id'])
                ->orderBy('bookings.id','desc');
        } else {
          // $row = Booking::select('bookings.*')->where('sp_id',$user_data['id']);
          $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                }) 
          ->select('bookings.*', 'users.name', 'users.mobile_no')
            ->where('bookings.sp_id',$user_data['id'])
            ->where('bookings.status','1');
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('bookings.id', 'desc')
                ->get();
        
        $data = array();

        if (!empty($row)) {
            foreach($row as $val){
               $is_referred = '<p style="color:red;"><i>('.__('messages.New Customer').')</i></p>';
                //check customer uses sp refer code or not

                if($val['user_id']!='' && $val['user_id']!=null){
                  $cust_refer_code = $val->getAssociatedUserInfo->refer_code;
                  // echo $cust_refer_code;die;
                  $check_sp_code = SpUser::where('share_code',$cust_refer_code)->where('id',$val['sp_id'])->exists();
                  if($check_sp_code){
                    $is_referred = '<p style="color:red;"><i>('.__('messages.Sp Referred').')</i></p>';
                  }
                }

                $date = $val['appointment_date'];
              $time = $val['appointment_time'];
              $combinedDT = '';
              if($date!='' && $date!=null && $time!=null && $time!=''){
                 $combinedDT = date('M d, Y h:i A', strtotime("$date $time"));
               }else{
                $combinedDT = 'NA';
               }

            $nestedData = [
              "id" => $val['id'],
              "booking_unique_id" => $val['booking_unique_id'],
              "booking_service_type" => $val['booking_service_type'],
              "name" => $val['name'].'<br/>'.$is_referred,
              "mobile_no" => $val['mobile_no'],
              // "appointment_time" => date('h:i A', strtotime($val['appointment_time'])),
              "appointment_date" => $combinedDT,
              "service_end_time" => date('h:i A', strtotime($val['service_end_time'])),
              "tax_amount" => round($val['tax_amount'],2),
              "total_amount" => round($val['booking_total_amount'],2),
              "paid_amount" => round($val['paid_amount'],2),
              "created_date" => date('Y-m-d',strtotime($val['created_at'])),
              "status" => $status_arr[$val['status']],
            ];
             if(!$val['user_id'] && !$val['is_gift'] && ($val['db_user_name'] || $val['db_user_mobile_no'])){
              $nestedData['name'] = $val['db_user_name'];
              $nestedData['mobile_no'] = $val['db_user_mobile_no'];
            }
            if(!$val['user_id'] && $val['is_gift'] && ($val['receiver_name'] || $val['receiver_mobile_no'])){
              $nestedData['name'] = $val['receiver_name'];
              $nestedData['mobile_no'] = $val['receiver_mobile_no'];
            }
            $nestedData['action'] =  getButtons([
                            ['key'=>'chat','link'=>route('orders.myConfirmOrderChatHistory',$val->id)],
                            ['key'=>'Reschedule','link'=>route('orders.reschedule_order',[$val->id])],
                            ['key'=>'Complete','link'=>route('orders.completed_order_view',[$val->id])],
                            ['key'=>'view','link'=>route('orders.show_order',[$val->id, 1])]                                                     
                        ]); 
            $data[] = $nestedData;
          }

        }
        
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function myConfirmOrderChatHistory($id)
    {
        
        $user = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                }) 
          ->select('bookings.*', 'users.image',  'users.name', 'users.mobile_no')
            ->where('bookings.id',$id)
            ->where('bookings.user_id','!=','')
            ->where('bookings.status','1')
            ->first();

            // dd($user);

        if ($user) {

            $user_data = spLoginData();
            // echo "user ".
            $userId=$user_data->id;

            // echo "<br/>";

            $udata = $user->toArray();

            $title = $udata['name'];            

            // echo "otherId ".
            $otherId = $udata['user_id'];

            // echo "<br/>";
            $booking_id=$udata['id'];

            $groupId   = 'SU-'.($userId>$otherId)?$userId."".$otherId."".$booking_id:$otherId."".$userId."".$booking_id;
            $groupId = 'SU-'.$groupId;
            $chats = Chat::where("group_id",$groupId)->get()->toArray();  

            $other['id']          = $otherId;
            $other['image']       = (!empty($udata['image']))?url('/public/sp_uploads/profile/'.$udata['image']):'https://ptetutorials.com/images/user-profile.png';;
            $other['full_name']   = $udata['name'];
            $other['booking_id']   = $booking_id;

            // dd($chats);

            $user['user_id']          = $userId;
            $user['user_image']       = (!empty($user_data->profile_image))?url('/public/sp_uploads/profile/'.$user_data->profile_image):'https://ptetutorials.com/images/user-profile.png';
            $user['user_name']   = ucfirst($user_data->store_name);

            $breadcum = ['orders.confirmed_orders'];
            return view('spadmin.orders.myOrderChat',compact('userId','user','groupId','title','other','breadcum','chats')); 

        }else{

            Session::flash('warning', __('Invalid request.'));
            return redirect()->route('orders.confirmed_orders');
        }

    }



    public function completed_orders()
    {
        $title = __('messages.My Orders');
        $breadcum = [$title =>''];
        $user_data = spLoginData(); 
       // echo '<pre>'; print_r($user_data);
        //$rows = Booking::where('sp_id',1)->get()->toArray();
        //echo '<pre>'; print_r($rows);die;
        return view('spadmin.orders.CompletedOrders',compact('title','row','breadcum'));
    }

    public function getDataCompleted(Request $request)
    {
        $user_data = spLoginData();
        $columns = ['id','tax_amount', 'total_amount','paid_amount','created_date','db_user_name','db_user_mobile_no','booking_unique_id','appointment_date','appointment_time','status','service_end_time','Action'];
        $order = $columns[$request->input('order.0.column')];

        $totalData = Booking::count();    
        $limit = $request->input('length');
        $start = $request->input('start');     

        $status_arr = array('0'=>'Awaiting','1'=>'Confirmed','2'=>'Cancelled','3'=>'Refunded','4'=>'Expired','5'=>'Completed');
        
        if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                })
                ->where(function ($query) use ($search){
                        $query->orWhere('name', 'like', '%' .$search . '%')
                  ->orWhere('mobile_no', 'like', '%' .$search . '%');
                    })
                
                ->select('bookings.*', 'users.name', 'users.mobile_no')
                ->where('bookings.status','5')
                ->where('bookings.sp_id',$user_data['id'])
                ->orderBy('bookings.id','desc');
        } else {
          // $row = Booking::select('bookings.*')->where('sp_id',$user_data['id']);
          $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                }) 
          ->select('bookings.*', 'users.name', 'users.mobile_no')
            ->where('bookings.sp_id',$user_data['id'])
            ->where('bookings.status','5');
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('bookings.id', 'desc')
                ->get();
        
        $data = array();

        if (!empty($row)) {
            foreach($row as $val){

              $date = $val['appointment_date'];
              $time = $val['appointment_time'];
              $combinedDT = '';
              if($date!='' && $date!=null && $time!=null && $time!=''){
                 $combinedDT = date('M d, Y h:i A', strtotime("$date $time"));
               }else{
                $combinedDT = 'NA';
               }

              $is_referred = '<p style="color:red;"><i>('.__('messages.New Customer').')</i></p>';
                //check customer uses sp refer code or not

                if($val['user_id']!='' && $val['user_id']!=null){
                  $cust_refer_code = $val->getAssociatedUserInfo->refer_code;
                  // echo $cust_refer_code;die; op
                  $check_sp_code = SpUser::where('share_code',$cust_refer_code)->where('id',$val['sp_id'])->exists();
                  if($check_sp_code){
                    $is_referred = '<p style="color:red;"><i>('.__('messages.Sp Referred').')</i></p>';
                  }
                }

            $nestedData = [
              "id" => $val['id'],
              "booking_unique_id" => $val['booking_unique_id'],
              "booking_service_type" => $val['booking_service_type'],
              "name" => $val['name'].'<br/>'.$is_referred,
              "mobile_no" => $val['mobile_no'],
              // "appointment_time" => date('h:i A', strtotime($val['appointment_time'])),
              "appointment_date" => $combinedDT,
              "service_end_time" => date('h:i A', strtotime($val['service_end_time'])),
              "tax_amount" => round($val['tax_amount'],2),
              "total_amount" => round($val['booking_total_amount'],2),
              "paid_amount" => round($val['paid_amount'],2),
              "total_item_amount" => round($val['total_item_amount'],2).' AED',
              "created_date" => date('Y-m-d',strtotime($val['created_at'])),
              "status" => $status_arr[$val['status']],
            ];
             if(!$val['user_id'] && !$val['is_gift'] && ($val['db_user_name'] || $val['db_user_mobile_no'])){
              $nestedData['name'] = $val['db_user_name'];
              $nestedData['mobile_no'] = $val['db_user_mobile_no'];
            }
            if(!$val['user_id'] && $val['is_gift'] && ($val['receiver_name'] || $val['receiver_mobile_no'])){
              $nestedData['name'] = $val['receiver_name'];
              $nestedData['mobile_no'] = $val['receiver_mobile_no'];
            }
            $nestedData['action'] =  getButtons([
                            ['key'=>'view','link'=>route('orders.show_order',[$val->id, 2])],                                                        
                        ]); 
            $data[] = $nestedData;
          }

        }
        
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }


    public function showBookingDetails($row)
  {

    $data = array();
    $data['id'] = $row['id'];
    $data['booking_unique_id'] = $row['booking_unique_id'];
    $data['status'] = $row['status'];
    $data['sp_id'] = $row['sp_id'];
    $data['db_user_name'] = $row['db_user_name'];
    $data['db_user_mobile_no'] = $row['db_user_mobile_no'];
    if(!$row['user_id'] && !$row['is_gift'] && ($row['db_user_name'] || $row['db_user_mobile_no'])){
      $data['user_name'] = $row['db_user_name'];                          
    }elseif(!$row['user_id'] && $row['is_gift'] && ($row['receiver_name'] || $row['receiver_mobile_no'])){
      $data['user_name'] = $row['receiver_name'];                         
    } else {
      $data['user_name'] = $row->getAssociatedUserInfo->name;
      $data['user_profile_image'] = $row->getAssociatedUserInfo->image!='' ? asset("uploads/".$row->getAssociatedUserInfo->image):'';
    }
    
    $data['user_id'] = $row['user_id'];
    $data['staff_id'] = $row['staff_id'];
    if($row['staff_id']!='' && $row['staff_id']!=null){
      $data['staff_name'] = $row->getAssociatedStaffInformation->staff_name;
    }else{
      $data['staff_name'] = "";
    }
    
    $data['appointment_time'] = $row['appointment_time'];
    $data['invoice'] = $row['invoice'];
    $data['service_end_time'] = $row['service_end_time'];
    $data['appointment_date'] = $row['appointment_date']!='' && $row['appointment_date']!= null ? $row['appointment_date']:"";
    $data['redeemed_reward_points'] = $row['redeemed_reward_points'];
    $data['payment_type'] = $row['payment_type'];
    $data['promo_code_amount'] = round($row['promo_code_amount'],2);
    $data['tax_amount'] = round($row['tax_amount'],2);
    $data['total_amount'] = round($row['booking_total_amount'],2);
    $data['paid_amount'] = round($row['paid_amount'],2);
    $data['total_item_amount'] = round($row['total_item_amount'],2);
    $data['total_cashback'] = round($row['total_cashback'],2);
    $data['created_date'] = date('Y-m-d h:i A',strtotime($row['created_at']));
    $name = 'name'.$this->getLang(); 
    $title = 'title'.$this->getLang(); 
    $item_name_arr = array();
    foreach($row->getAssociatedBookingItems as $bkitems){

      if($bkitems['is_service'] == '1')
      {
        $item_name_arr[] = [
          'service_id' => $bkitems['booking_item_id'],
          'name' => $bkitems->getAssociatedServiceDetail->getAssociatedService->$name,
          'quantity' => $bkitems['quantity'],
          'original_price' => round($bkitems['original_price'],2),
          'best_price' => round($bkitems['best_price'],2),
          'cashback' => round($bkitems['cashback'],2),
          'gender' => $bkitems['gender'],
        ];
      }
      else{
        $offer_services = array();

        foreach($bkitems->getAssociatedOfferDetail->getAssociatedOfferServices as $os){

          $offer_services[] = [
            'service_id' => $os['sp_service_id'],
            'quantity' => $os['quantity'],
            'name' => $os->getAssociatedOfferServicesName->getAssociatedService->$name,
          ];
        }

        $item_name_arr[] = [
          'offer_id' => $bkitems['booking_item_id'],
          'name' => $bkitems->getAssociatedOfferDetail->getAssociatedOfferName->$title,
          'offer_terms' => $bkitems->getAssociatedOfferDetail->offer_terms,
          'offer_details' => $bkitems->getAssociatedOfferDetail->offer_details,
          'quantity' => $bkitems['quantity'],
          'gender' => $bkitems['gender'],
          'original_price' => round($bkitems['original_price'],2),
          'best_price' => round($bkitems['best_price'],2),
          'cashback' => round($bkitems['cashback'],2),
          'services'=>$offer_services,
        ];
      }

    }
    $data['booking_items'] = $item_name_arr;
    // print_r($data);die;
    return $data;
        
  }

    public function show_order($id, $flag=''){
      $title = __('messages.My Orders Details');
      $model = 'orders';
      if($flag && $flag == 1){
        $breadcum = [__('messages.Orders')=>route($model.'.confirmed_orders'),$title =>''];
      } elseif($flag && $flag == 2){
        $breadcum = [__('messages.Orders')=>route($model.'.completed_orders'),$title =>''];
      } elseif($flag && $flag == 3){
        $breadcum = [__('messages.Orders')=>route($model.'.cancelled_order'),$title =>''];
      } else {
        $breadcum = [__('messages.Orders')=>route($model.'.index'),$title =>''];
      }
      $user_data = spLoginData(); 
      $order = Booking::where('id',$id)->first();
      $result = $this->showBookingDetails($order);  
     //echo '<pre>'; print_r($result);die;
     return view('spadmin.orders.ShowOrder',compact('title','result','breadcum'));
    }



    public function order_confirm_view($id){
      $title = __('messages.Confirm Order');
      $model = 'orders';
      $breadcum = [__('messages.Orders')=>route($model.'.index'),$title =>''];
      $user_data = spLoginData(); 
      $order = Booking::where('id',$id)->first();
      $result = $this->showBookingDetails($order);  
      $staff_name_rows = Staff::select('id','staff_name')->where('sp_id',$user_data['id'])->get();
    //  echo '<pre>'; print_r($result);die;
     return view('spadmin.orders.OrderConfirmView',compact('title','result','breadcum','staff_name_rows'));
    }



  public function order_confirm(Request $request)
  {
    try {
       
        $data = $request->all();   
        
        if($request->hours == '0' && $request->minutes == '0'){
            $error = __('messages.Please select a valid hours and minutes.');
              Session::flash('danger', $error);
              return redirect()->back()->withInput();
          }

        $service_time = strtotime(date("H:i", strtotime('+'.(int)$request->hours.' hours', strtotime($request->appointment_time))));        
        $data['service_end_time'] = date("H:i", strtotime('+'.(int)$request->minutes.' minutes', $service_time));

       // echo '<pre>'; print_r($data);die;
        $validator = Validator::make($data, 
            [
              'order_id' => 'required',
              'appointment_date' => 'required',
              'appointment_time' => 'required',
              'service_end_time' => 'required',
              'staff_id' => 'required',
            ]);
          if ($validator->fails()) 
          {
            
            $error = $this->validationHandle($validator->messages());
            Session::flash('danger', $error);
            return redirect()->back()->withInput();
          } 
          else 
          {
           // date_default_timezone_set('Asia/Kolkata');  
          //query to check time slots is valid or not
            date_default_timezone_set('Asia/Dubai');  
            $app_time = date("H:i:s", strtotime($data['appointment_time']));
            
            $sr_end_time = date("H:i:s", strtotime($data['service_end_time']));

            $rows = Booking::where('staff_id',$data['staff_id'])
                      ->where(function ($query){

                          $query->where('status','1')
                          ->orWhere(function ($query){
                            $query->where('status','0')
                            ->Where(function ($query){
                              $query->whereNull('staff_id')
                              ->orWhere('staff_id','!=','');
                            }); 
                            
                          });
                    
                      })
                      // ->orWhere('status','5')
                      ->where('appointment_date',date('Y-m-d',strtotime($data['appointment_date'])))

                      ->where(function ($query) use ($app_time, $sr_end_time) {
                          $query->where(function ($query) use ($app_time, $sr_end_time) {
                          $query
                              ->where('appointment_time', '<=', $app_time)
                              ->where('service_end_time', '>', $app_time);
                      })
                      ->orWhere(function ($query) use ($app_time, $sr_end_time) {
                          $query
                              ->where('appointment_time', '<', $sr_end_time)
                              ->where('service_end_time', '>=', $sr_end_time);
                      })
                      ->orWhere(function ($query) use ($app_time, $sr_end_time) {
                          $query
                            // ->where('status','0')
                            // ->where('service_end_time','')
                            
                            ->Where(function ($query){
                                      $query->whereNull('service_end_time')
                                      ->orWhere('service_end_time','');
                                    })

                              ->where('appointment_time', '>=', $app_time)
                              ->where('appointment_time', '<=', $sr_end_time);
                      });
                    })
                      ->where('id','!=',$data['order_id'])
                      
                      // ->select('appointment_time','service_end_time')
                      ->count();

              if($rows>0){
                Session::flash('danger', __('messages.This time slot is already booked.'));
                 return redirect()->back()->withInput();
                  }else{
                    
                    $rows = Booking::where('id',$data['order_id'])->first();

                    $rows->staff_id = $data['staff_id'];
                    $rows->appointment_date = date('Y-m-d',strtotime($data['appointment_date']));
                    $rows->appointment_time = $data['appointment_time'];
                    $rows->service_end_time = $data['service_end_time'];
                    $rows->status = '1';
                    $rows->confirmed_date_time = date('Y-m-d H:i:s');
                    $rows->save();

                    if($rows->save()){
                      
                      $date = $rows['appointment_date'];
                      $time = $rows['appointment_time'];
                      $combinedDT = date('Y-m-d h:i:A', strtotime("$date $time"));

                      $message_title_main = __('messages.Appointment Confirmed!');
                      if($rows->booking_service_type == 'home'){

                        $message_main = __('messages.Appointment Confirmed Home!',['date' => $combinedDT]);
                      }else{

                        $message_main =  __('messages.Appointment Confirmed Store!',['date' => $combinedDT,'user_name' => $rows->getAssociatedUserInfo->name,'store_name' => $rows->getAssociatedSpInformation->store_name]);

                      }
                      app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');
                      $message_title_other = __('messages.Appointment Confirmed!');
                      if($rows->booking_service_type == 'home'){
                          $message_other = __('messages.Appointment Confirmed Home!',['date' => $combinedDT]);
                        }else{

                          $message_other =  __('messages.Appointment Confirmed Store!',['date' => $combinedDT,'user_name' => $rows->getAssociatedUserInfo->name,'store_name' => $rows->getAssociatedSpInformation->store_name]);

                        }
                      
                      //get user detail
                      $user = User::where('id',$rows->user_id)->first();
                      if($rows->user_id!='' && $rows->user_id!=null && $user->notification_alert_status == '1'){

                        Notification::saveNotification($rows->user_id,$rows->sp_id,$rows->id,'CONFIRM_ORDER',$message_main,$message_other,$message_title_main,$message_title_other,'customer');
                        app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');
                      }
                      

                      Session::flash('success', __('messages.Order Confirmed Successfully.'));
                     return redirect()->route('orders.index');
                    }
                  }
                  
              }
            
     }
     catch(\Exception $e){
       $msg = $e->getMessage();
       Session::flash('danger', $msg);
       return redirect()->back()->withInput();
     }
  }

    public function reschedule_order($id){
      $title = __('messages.Reschedule Order');
      $model = 'orders';
      $breadcum = [__('messages.Orders')=>route($model.'.confirmed_orders'),$title =>''];
      $user_data = spLoginData(); 
      $order = Booking::where('id',$id)->first();
      $result = $this->showBookingDetails($order);  
      $staff_name_rows = Staff::select('id','staff_name')->where('sp_id',$user_data['id'])->get();
     //echo '<pre>'; print_r($result);die;
     return view('spadmin.orders.RescheduleOrder',compact('title','result','breadcum','staff_name_rows'));
    }

     public function ajaxGetTime(Request $request) {
        date_default_timezone_set('Asia/Dubai');
        $user_data = spLoginData();
        $selected_date = $request->selected_date;
        $db_day_arr = array('0'=>'7','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6');
        $day = $db_day_arr[date('w', strtotime($selected_date))];

        //$shop_time = ShopTiming::where('user_id',$user_data['id'])
        $shop_time = ShopTiming::where('user_id',$user_data['id'])
        ->where('isOpen','1')
        ->where('day',$day)
        ->first();
                
        $time_frequency = 15;
        $shoptime_slots = array();
        if($shop_time['opening_time']){
          $start_time = date('H:i',strtotime($shop_time['opening_time']));
          $end_time = date('H:i',strtotime($shop_time['closing_time']));  

          for($i = strtotime($start_time); $i<= strtotime($end_time); $i = $i + $time_frequency * 60) {
            if(strtotime($selected_date) == strtotime(date('Y-m-d'))){
              if(date("H:i", $i) > date("H:i")){
                $shoptime_slots[date("H:i", $i)] = date("h:i A", $i); 
              }
            } else {
              $shoptime_slots[date("H:i", $i)] = date("h:i A", $i);  
            }
          }
        }
          $selected_time = '';
          if($request->selected_time){
            $selected_time = $request->selected_time;
          }
        //echo '<pre>'; print_r($shoptime_slots);
        return view('spadmin.orders.ajaxGetTime',compact('shoptime_slots', 'selected_time'));
    }


  public function reschedule(Request $request)
  {
    try {
       
        $data = $request->all();  
        if($request->hours == '0' && $request->minutes == '0'){
            $error = __('messages.Please select a valid hours and minutes.');
              Session::flash('danger', $error);
              return redirect()->back()->withInput();
          }
               
        $service_time = strtotime(date("H:i", strtotime('+'.(int)$request->hours.' hours', strtotime($request->appointment_time))));        
        $data['service_end_time'] = date("H:i", strtotime('+'.(int)$request->minutes.' minutes', $service_time));

       // echo '<pre>'; print_r($data);die;
        $validator = Validator::make($data, 
            [
              'order_id' => 'required',
              'appointment_date' => 'required',
              'appointment_time' => 'required',
              'service_end_time' => 'required',
              'staff_id' => 'required',
            ]);
          if ($validator->fails()) 
          {
            
            $error = $this->validationHandle($validator->messages());
            Session::flash('danger', $error);
           return redirect()->back()->withInput();
          } 
          else 
          {
            date_default_timezone_set('Asia/Dubai');
          //query to check time slots is valid or not

            $app_time = date("H:i:s", strtotime($data['appointment_time']));
            
            $sr_end_time = date("H:i:s", strtotime($data['service_end_time']));

            $rows = Booking::where('staff_id',$data['staff_id'])
                      ->where(function ($query){

                          $query->where('status','1')
                          ->orWhere(function ($query){
                            $query->where('status','0')
                            ->Where(function ($query){
                              $query->whereNull('staff_id')
                              ->orWhere('staff_id','!=','');
                            }); 
                            
                          });
                    
                      })
                      // ->orWhere('status','5')
                      ->where('appointment_date',date('Y-m-d',strtotime($data['appointment_date'])))

                      ->where(function ($query) use ($app_time, $sr_end_time) {
                        $query->where(function ($query) use ($app_time, $sr_end_time) {
                        $query
                            ->where('appointment_time', '<=', $app_time)
                            ->where('service_end_time', '>', $app_time);
                    })
                    ->orWhere(function ($query) use ($app_time, $sr_end_time) {
                        $query
                            ->where('appointment_time', '<', $sr_end_time)
                            ->where('service_end_time', '>=', $sr_end_time);
                    })
                    ->orWhere(function ($query) use ($app_time, $sr_end_time) {
                          $query
                            // ->where('status','0')
                            // ->where('service_end_time','')
                            
                            ->Where(function ($query){
                                      $query->whereNull('service_end_time')
                                      ->orWhere('service_end_time','');
                                    })

                              ->where('appointment_time', '>=', $app_time)
                              ->where('appointment_time', '<=', $sr_end_time);
                      });
                      })
                      ->where('id','!=',$data['order_id'])
                      
                      // ->select('appointment_time','service_end_time')
                      ->count();

              if($rows>0){
                 Session::flash('danger', __('messages.This time slot is already booked.'));
                 return redirect()->back()->withInput();
                  }else{
                    
                    $rows = Booking::where('id',$data['order_id'])->first();

                    $rows->staff_id = $data['staff_id'];
                    $rows->appointment_date = date('Y-m-d',strtotime($data['appointment_date']));
                    $rows->appointment_time = $data['appointment_time'];
                    $rows->service_end_time = $data['service_end_time'];
                    $rows->is_rescheduled = '1';
                    //$rows->save();

                    if($rows->save()){
                      $re_row = new RescheduleOrderHistory();
                      $re_row->booking_id = $rows->id;
                      $re_row->appointment_date = $rows->appointment_date;
                      $re_row->appointment_time = $rows->appointment_time;
                      $re_row->service_end_time = $rows->service_end_time;
                      $re_row->save();

                      if($rows->user_id!=null && $rows->getAssociatedUserInfo->notification_alert_status == '1'){

                        $message_main = __('messages.Appointment Rescheduled!',['store_name' => $rows->getAssociatedSpInformation->store_name,'date' => $request->appointment_date,'time' => $request->appointment_time]);

                          $message_title_main = __('messages.Appointment Rescheduled!!');

                          app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

                          $message_other = __('messages.Appointment Rescheduled!',['store_name' => $rows->getAssociatedSpInformation->store_name,'date' => $request->appointment_date,'time' => $request->appointment_time]);

                          $message_title_other = __('messages.Appointment Rescheduled!!');

                          Notification::saveNotification($rows->user_id,$rows->sp_id,$rows->id,'RESCHEDULE_ORDER',$message_main,$message_other,$message_title_main,$message_title_other,'customer');
                          app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');
                      }
                      

                      Session::flash('success', __('messages.Order has been rescheduled Successfully.'));
                     return redirect()->route('orders.confirmed_orders');
                    }
                  }
                  
              }
            
     }
     catch(\Exception $e){
       $msg = $e->getMessage();
       Session::flash('danger', $msg);
       return redirect()->back()->withInput();
     }
  }



    public function completed_order_view($id){
      $title = __('messages.Completed Order');
      $model = 'orders';
      $breadcum = [__('messages.Orders')=>route($model.'.confirmed_orders'),$title =>''];
      $user_data = spLoginData(); 
      $order = Booking::where('id',$id)->first();
      $result = $this->showBookingDetails($order);  
      $staff_name_rows = Staff::select('id','staff_name')->where('sp_id',$user_data['id'])->get();
     //echo '<pre>'; print_r($result);die;
     return view('spadmin.orders.CompletedOrderView',compact('title','result','breadcum','staff_name_rows','user_data'));
    }

  public function completed(Request $request)
  {
    try {
       $user_data = spLoginData(); 
        $data = $request->all();       

        //echo '<pre>'; print_r($data);die;
        $validator = Validator::make($data, 
            [
              'order_id' => 'required',
              'unique_order_id' => 'required',
              'appointment_date' => 'required',
              'appointment_time' => 'required',
              'staff_id' => 'required',
            ]);
          if ($validator->fails()) 
          {
            
            $error = $this->validationHandle($validator->messages());
            //return response()->json(['status' => false, 'message' => $error]);
            Session::flash('danger', $error);
            return redirect()->back()->withInput();
          } 
          else 
          {
            date_default_timezone_set('Asia/Kolkata');
            $user_cat_id = SpUser::where('id',$user_data['id'])->select('category_id')->first();
            $rows = Booking::where('id',$request->order_id)->where('booking_unique_id',$request->unique_order_id)->first();
            if($rows){
                
                $date = $rows['appointment_date'];
                $time = $rows['appointment_time'];
                $combinedDT = date('Y-m-d h:i:A', strtotime("$date $time"));
                $current_timestamp = date('Y-m-d H:i:s');

                if(strtotime($current_timestamp)>strtotime($combinedDT)){
                  
                  $service_end_time = $rows['service_end_time'];
                }else{
                  $service_end_time = date('H:i');
            
                }

                $rows->staff_id = $request->staff_id;
                $rows->appointment_date = date('Y-m-d',strtotime($request->appointment_date));
                $rows->appointment_time = $request->appointment_time;
                $rows->service_end_time = $service_end_time;               
                $rows->completed_date_time = date('Y-m-d H:i:s');
                if ($request->hasFile('invoice') && $request->file('invoice'))
                  {
                      $file = $request->file('invoice');
                      $name = time().'.'.$file->getClientOriginalExtension();
                      $destinationPath = public_path('/sp_uploads/invoice/');
                      $img = $file->move($destinationPath, $name);
                      $rows->invoice = $name;
                  } 
                  $rows->status = '5';
                  $is_referred = 0;
                    //check customer uses sp refer code or not

                  if($rows['user_id']!='' && $rows['user_id']!=null){
                    $cust_refer_code = $rows->getAssociatedUserInfo->refer_code;
                  
                    $check_sp_code = SpUser::where('share_code',$cust_refer_code)->where('id',$rows['sp_id'])->exists();
                    if($check_sp_code){
                      $is_referred = 1;
                    }
                  }

                  $rows->payment_settlement_status = 'unpaid';
                  if($user_cat_id['category_id']== 4 && (string)$rows->payment_type == '1' && $rows->db_user_name == '' && $is_referred!=1){
                    $rows->payment_settlement_status = null;
                  }
                
                if($rows->save()){
                  
                  // $booking = Booking::where('id',$request->order_id)->select('booking_total_amount','user_id')->first();
                  if($rows->db_user_name == '' && $rows->db_user_name == null){
                    if($rows->is_gift == '' && $rows->is_gift == null){
                      $rewarded_user_id = $rows->user_id;
                    }else{
                      $rewarded_user_id = $rows->gifted_by;
                    }
                    
                    $tier = SpUser::leftJoin('tiers', function($join) {
                      $join->on('sp_users.tier_id', '=', 'tiers.id');
                      })
                          ->where('sp_users.id',$user_data['id'])
                          ->select('tiers.earning')
                          ->first();


                    // $rewards = ($rows->booking_total_amount*$tier->earning)/100;

                    if($user_cat_id['category_id'] == 4 && (string)$rows->payment_type == '1'  && $is_referred != 1){

                    }else{
                    
                           $rewards = $rows->total_cashback;
                    

                      $user = User::where('id',$rewarded_user_id)
                        ->first();
                        if($user->earning!=0 && $user->earning!=null){
                          $total_earning = $rewards+$user->earning;
                        }else{
                          $total_earning = $rewards;
                        }

                        //check cashback already exists or not
                        $check_cashback = CashbackHistory::where('type','0')->where('booking_id',$rows->id)->first();
                        if(!$check_cashback){
                          $user->earning = round($total_earning,2);
                          $user->save();

                          $cashback = new CashbackHistory();
                          $cashback->user_id = $rewarded_user_id;
                          $cashback->booking_id = $request->order_id;
                          $cashback->type = '0';
                          $cashback->amount =round($rewards,2);
                          $cashback->save();                    
                        }

                        if($user->notification_alert_status == '1'){
                          $date = $rows['appointment_date'];
                            $time = $rows['appointment_time'];
                            $combinedDT = date('Y-m-d h:i:A', strtotime("$date $time"));

                            $message_main = __('messages.Cashback Credited',['cashback' => round($rewards,2),'order_id' => $rows->booking_unique_id]);

                            $message_title_main = __('messages.Cashback Credited!');

                           app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

                            $message_other = __('messages.Cashback Credited',['cashback' => round($rewards,2),'order_id' => $rows->booking_unique_id]);

                            $message_title_other = __('messages.Cashback Credited!');

                            
                            Notification::saveNotification($rows->user_id,$rows->sp_id,$rows->id,'CASHBACK_CREDITED',$message_main,$message_other,$message_title_main,$message_title_other,'customer');
                            app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');
                            
                          }

                      }

                      if($rows->is_gift == '1' && $user->notification_alert_status == '1'){

                        $message_main = __('messages.Gift Redeemed',['receiver_name' => $rows->receiver_name]);
                        $message_title_main = __('messages.Gift Redeemed!');
                       app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

                            $message_other = __('messages.Gift Redeemed',['receiver_name' => $rows->receiver_name]);
                        $message_title_other = __('messages.Gift Redeemed!');
                            Notification::saveNotification($rows->user_id,$rows->sp_id,$rows->id,'GIFT_REDEEMED',$message_main,$message_other,$message_title_main,$message_title_other,'customer');
                            app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

                      }

                      if($rows->user_id!=null && $rows->user_id!='' && $rows->getAssociatedUserInfo->notification_alert_status == '1'){

                         $message_main = __('messages.How was the service?',['date' => date('Y-m-d',strtotime($rows['created_at'])),'store_name' => $rows->getAssociatedSpInformation->store_name]);

                          $message_title_main = __('messages.How was the service!');
                          app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');
                        $message_other = __('messages.How was the service?',['date' => date('Y-m-d',strtotime($rows['created_at'])),'store_name' => $rows->getAssociatedSpInformation->store_name]);

                          $message_title_other = __('messages.How was the service!');
                            
                        Notification::saveNotification($rows->user_id,$rows->sp_id,$rows->id,'REVIEW_REQUEST',$message_main,$message_other,$message_title_main,$message_title_other,'customer');
                        app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

                      }

                        $data['templete'] = "review_request";
                        $data['name'] = $rows->getAssociatedUserInfo->name;
                        $data['email'] = $rows->getAssociatedUserInfo->email;
                        $data['store_name'] = $rows->getAssociatedSpInformation->store_name;
                        $data['date'] = $combinedDT;
                        $data['subject'] = "Your feedback has the power!";
                        send($data);

                    }
                    Session::flash('success', __('messages.Order completed Successfully.'));
                     return redirect()->route('orders.confirmed_orders');
                }
              }else{
                Session::flash('danger', __('messages.OrderId is not valid.'));
                 return redirect()->back()->withInput();
              }
              
          }
            
     }
     catch(\Exception $e){
       $msg = $e->getMessage();
       Session::flash('danger', $msg);
       return redirect()->back()->withInput();
     }
  }

  public function add_order($staff_id = '', $date = '', $time = ''){
    
      $title = __('messages.Add New Order');
      $model = 'orders';
      $breadcum = [__('messages.Orders')=>route($model.'.index'),$title =>''];
      $user_data = spLoginData();
      $sp_row = SpUser::where('id',$user_data['id'])->first();
      $staff_name_rows = Staff::select('id','staff_name')->where('sp_id',$user_data['id'])->get();
      if($date){ $date = date('Y-m-d', $date); }
      if($time){ $time = date('H:i', $time); }

      // $country_codes = Country::orderBy('phonecode','ASC')->pluck('phonecode','phonecode','name');
      $country_codes = $this->getCountryCodes();
     //echo '<pre>'; print_r($result);die;
     return view('spadmin.orders.AddOrder',compact('title','breadcum','staff_name_rows','sp_row', 'staff_id', 'date', 'time','country_codes'));
    }

    public function getCountryCodes()
    {
      $country_arr = array();
      $country_codes = Country::orderBy('phonecode','ASC')->get();

      foreach($country_codes as $rows){
        $country_arr[$rows['phonecode']] = '('.$rows['phonecode'].') '.$rows['name'];
      }
      
      return $country_arr;
    }

  public function new_order(Request $request)
  {
    // echo app()->getLocale();die;
    if (Input::isMethod('post')) {
         try
         {
            $data = $request->all();
            if($request->hours == '0' && $request->minutes == '0'){
              $error = __('messages.Please select a valid hours and minutes.');
                Session::flash('danger', $error);
                return redirect()->back()->withInput();
            }

            $service_time = strtotime(date("H:i", strtotime('+'.(int)$request->hours.' hours', strtotime($request->appointment_time))));        
            $data['appointment_end_time'] = date("H:i", strtotime('+'.(int)$request->minutes.' minutes', $service_time));
             //echo '<pre>'; print_r($data);die;
              $validator = Validator::make($data, 
                [
                  'staff_id'=> 'required',
                  'customer_name'=> 'required',
                  'customer_mobile_no'=> 'required',
                  'country_code'=> 'required',
                  'appointment_date'=> 'required',
                  'appointment_time'=> 'required',
                  'appointment_end_time'=> 'required',
                  'services'=> 'required',
                  'service_type' => 'required'                 
                ]);
              if ($validator->fails()) 
              {
                
                $error = $this->validationHandle($validator->messages());
               // return response()->json(['status' => false, 'message' => $error]);
                Session::flash('danger', $error);
                return redirect()->back()->withInput();
              } 
              else 
              {

                date_default_timezone_set('Asia/Dubai');  
                $app_time = date("H:i:s", strtotime($data['appointment_time']));
            
                $sr_end_time = date("H:i:s", strtotime($data['appointment_end_time']));

                $rows = Booking::where('staff_id',$data['staff_id'])
                          ->where(function ($query){

                            $query->where('status','1')
                            ->orWhere(function ($query){
                              $query->where('status','0')
                              ->Where(function ($query){
                                $query->whereNull('staff_id')
                                ->orWhere('staff_id','!=','');
                              }); 
                              
                            });
                      
                        })
                          // ->orWhere('status','5')
                          ->where('appointment_date',date('Y-m-d',strtotime($data['appointment_date'])))

                          ->where(function ($query) use ($app_time, $sr_end_time) {
                            $query->where(function ($query) use ($app_time, $sr_end_time) {
                            $query
                                ->where('appointment_time', '<=', $app_time)
                                ->where('service_end_time', '>', $app_time);
                        })
                        ->orWhere(function ($query) use ($app_time, $sr_end_time) {
                            $query
                                ->where('appointment_time', '<', $sr_end_time)
                                ->where('service_end_time', '>=', $sr_end_time);
                        })
                        ->orWhere(function ($query) use ($app_time, $sr_end_time) {
                          $query
                            // ->where('status','0')
                            // ->where('service_end_time','')
                            
                            ->Where(function ($query){
                                      $query->whereNull('service_end_time')
                                      ->orWhere('service_end_time','');
                                    })

                              ->where('appointment_time', '>=', $app_time)
                              ->where('appointment_time', '<=', $sr_end_time);
                      });
                    })
                        
                  ->count();

                if($rows>0){
                     Session::flash('danger', __('messages.This time slot is already booked.'));
                     return redirect()->back()->withInput();
                 } else {  

                  $user_data = spLoginData();
                  $unique_id = $this->generateRandomNumbers();
                  
                  $row = new Booking();
                  $row->sp_id = $user_data['id'];

                  if($request->customer_mobile_no){
                    $user_check = User::where('mobile_no',$request->customer_mobile_no)->where('country_code',$request->country_code)->first();

                    if($user_check){
                      $row->user_id = $user_check->id;
                      $row->db_user_name = $request->customer_name;
                      $row->db_user_mobile_no = $request->country_code.'-'.$request->customer_mobile_no;
                    }else{
                      $row->db_user_name = $request->customer_name;
                      $row->db_user_mobile_no = $request->country_code.'-'.$request->customer_mobile_no;
                      
                    }
                  }else{
                    $row->db_user_name = $request->customer_name;
                  }                

                  $row->staff_id = $request->staff_id;
                  $row->appointment_date = $request->appointment_date;
                  $row->appointment_time = $request->appointment_time;
                  $row->service_end_time = $data['appointment_end_time'];
                  $row->total_item_amount = $request->total_price;
                  $row->booking_service_type = $request->service_type;
                  // $row->tax_amount = $request->tax_amount;
                  $row->booking_total_amount = $request->total_price;
                  $row->paid_amount = $request->total_price;
                  $row->booking_unique_id = $unique_id;
                  $row->payment_type = '1';
                  $row->settlement_option = 'nonbillable';
                  $row->status = '1';
                 
                  if($row->save()){
   
                    $item_arr = $request->services;
                    $org_price = $request->org_price;
                    $best_price = $request->best_price;

                    foreach($item_arr as $ikey => $items){
                      $item_row = new BookingItem();
                     
                      $item_row->is_service = '1';
                      $item_row->booking_id = $row->id;
                      $item_row->booking_item_id = $ikey;
                      $item_row->quantity = $items;
                      $item_row->original_price = $org_price[$ikey];
                      $item_row->best_price = $best_price[$ikey];
                      $item_row->gender = $request->gender; 
                      $item_row->service_type = $request->service_type; 
                      $item_row->save();

                      //send notification
                    
                    
                    
                    if($row->user_id!='' && $row->user_id!=null){
                      
                        $date = $row['appointment_date'];
                        $time = $row['appointment_time'];
                        $combinedDT = date('Y-m-d h:i:A', strtotime("$date $time"));

                        if($row->booking_service_type == 'home'){
                          $message_main = __('messages.Appointment Confirmed Home!',['date' => $combinedDT]);
                        }else{

                          $message_main = __('messages.Appointment Confirmed Store!',['date' => $combinedDT,'user_name' => $row->getAssociatedUserInfo->nameT,'store_name' => $row->getAssociatedSpInformation->store_name]);
                        }
                        $message_title_main =  __('messages.Appointment Confirmed!');
                        app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

                        if($row->booking_service_type == 'home'){
                          $message_other = __('messages.Appointment Confirmed Home!',['date' => $combinedDT]);
                        }else{
                          $message_other = __('messages.Appointment Confirmed Store!',['date' => $combinedDT,'user_name' => $row->getAssociatedUserInfo->nameT,'store_name' => $row->getAssociatedSpInformation->store_name]);
                        }
                        $message_title_other = __('messages.Appointment Confirmed!');

                        if($row->getAssociatedUserInfo->notification_alert_status == '1'){

                          Notification::saveNotification($row->user_id,$row->sp_id,$row->id,'CONFIRM_ORDER',$message_main,$message_other,$message_title_main,$message_title_other,'customer');
                          app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');
                        }
                        
                      }

                    }
                  
                    Session::flash('success', __('messages.Order Placed Successfully.'));
                    return redirect()->route('orders.confirmed_orders');
                 }
                }
              }
         }
         catch(\Exception $e){
           Session::flash('danger', $e->getMessage());
            return redirect()->back()->withInput();
         }
    }
  }
  public function generateRandomNumbers()
    {
      // return mt_rand(10,10000000000);
       $unique_id = substr(str_shuffle("0123456789"), 0, 10);
       $check_no = Booking::where('booking_unique_id',$unique_id)->exists();
           if($check_no){
            $unique_id = $this->generateRandomNumbers();
           }
           return $unique_id;
    }


    public function cancelledOrders()
    {
        $title = __('messages.My Cancelled Orders');
        $breadcum = [$title =>''];
        return view('spadmin.orders.cancelledOrders',compact('title','row','breadcum'));
    }

    public function getDataCancelled(Request $request)
    {
        $user_data = spLoginData(); 
        $columns = ['id','tax_amount', 'total_amount','paid_amount','created_date','db_user_name','db_user_mobile_no','booking_unique_id','appointment_date','appointment_time','status','service_end_time','Action'];
        $order = $columns[$request->input('order.0.column')];

        $totalData = Booking::count();    
        $limit = $request->input('length');
        $start = $request->input('start');     

        $status_arr = array('0'=>'Awaiting','1'=>'Confirmed','2'=>'Cancelled','3'=>'Refunded','4'=>'Expired','5'=>'Completed');
        
        if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                })
                ->where(function ($query) use ($search){
                        $query->orWhere('name', 'like', '%' .$search . '%')
                  ->orWhere('mobile_no', 'like', '%' .$search . '%');
                    })
                
                ->select('bookings.*', 'users.name', 'users.mobile_no')
                ->where('bookings.status','2')
                ->orderBy('bookings.id','desc');
        } else {
          
          $row = Booking::leftJoin('users', function($join) {
                  $join->on('bookings.user_id', '=', 'users.id');
                }) 
          ->select('bookings.*', 'users.name', 'users.mobile_no')
          ->where('bookings.sp_id',$user_data['id'])
            ->where('bookings.status','2');
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('bookings.id', 'desc')
                ->get();
        
        $data = array();

        if (!empty($row)) {
            foreach($row as $val){
              $date = $val['appointment_date'];
              $time = $val['appointment_time'];
              $combinedDT = '';
              if($date!='' && $date!=null && $time!=null && $time!=''){
                 $combinedDT = date('M d, Y h:i A', strtotime("$date $time"));
               }else{
                $combinedDT = 'NA';
               }

               $is_referred = '<p style="color:red;"><i>('.__('messages.New Customer').')</i></p>';
                //check customer uses sp refer code or not

                if($val['user_id']!='' && $val['user_id']!=null){
                  $cust_refer_code = $val->getAssociatedUserInfo->refer_code;
                  // echo $cust_refer_code;die;
                  $check_sp_code = SpUser::where('share_code',$cust_refer_code)->where('id',$val['sp_id'])->exists();
                  if($check_sp_code){
                    $is_referred = '<p style="color:red;"><i>('.__('messages.Sp Referred').')</i></p>';
                  }
                }

            $nestedData = [
              "id" => $val['id'],
              "booking_unique_id" => $val['booking_unique_id'],
              "booking_service_type" => $val['booking_service_type'],
              "name" => ucfirst($val['name']).'<br/>'.$is_referred,
              // "appointment_time" => date('h:i A', strtotime($val['appointment_time'])),
              "appointment_date" => $combinedDT,
              "service_end_time" => date('h:i A', strtotime($val['service_end_time'])),
              "tax_amount" => round($val['tax_amount'],2),
              "total_amount" => round($val['booking_total_amount'],2),
              "paid_amount" => round($val['paid_amount'],2),
              "created_date" => date('Y-m-d',strtotime($val['created_at'])),
              "status" => $status_arr[$val['status']],
            ];
             if(!$val['user_id'] && !$val['is_gift'] && ($val['db_user_name'] || $val['db_user_mobile_no'])){
              $nestedData['name'] = ucfirst($val['db_user_name']).'</br><b>('.$val['db_user_mobile_no'].')</b>';
             
            }
            if(!$val['user_id'] && $val['is_gift'] && ($val['receiver_name'] || $val['receiver_mobile_no'])){
              $nestedData['name'] = ucfirst($val['receiver_name']).'</br><b>('.$val['receiver_mobile_no'].')</b>';
            
            }
            $nestedData['sp_id'] = ucfirst($val->getAssociatedSpInformation->name).'</br><b>('.$val->getAssociatedSpInformation->mobile_no.')</b>';
            $nestedData['action'] =  getButtons([
                            ['key'=>'view','link'=>route('orders.show_order',[$val->id, 3])],                                                                
                        ]); 
            $data[] = $nestedData;
          }

        }
        
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }
}
