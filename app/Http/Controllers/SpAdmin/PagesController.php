<?php

namespace App\Http\Controllers\SpAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use App\Model\SpUser;
use App\Model\ShopTiming;
use App\Model\Booking;
use App\Model\Staff;
use App\Model\Notification;
use App\Model\Review;
use App\Model\ReviewSpService;
use App\Model\ReviewQuestionRating;
use App\Model\Setting;
use App\Model\Forum;
use App\Model\ForumComment;
use App\Model\ForumCategory;
use App\Model\SpOffer;
use App\Model\User;
use DB;
class PagesController extends Controller
{
    public function dashboard()
    {	    	
    	$title = 'Dashboard';
     	$user_detail = spLoginData();
        $user_id = $user_detail['id'];
        $user_count = 0;
        //---------------------------------------------


        $base_path = asset('sp_uploads/profile/').'/';
        $result_arr = array();
        $current_date = date('Y-m-d');

        $user = SpUser::where('id',$user_id)->select('store_name','share_code','address','category_id',DB::raw('CONCAT("'.$base_path.'", sp_users.profile_image) AS profile_image'))->first();

                //check shop opening time
        $db_day_arr = array('0'=>'7','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6');

        // $day = $db_day_arr[date('w', date('Y-m-d'))];
        $day = date('w', strtotime(date('Y-m-d')));
        
        $shop_time = ShopTiming::where('user_id',$user_id)
        ->where('isOpen','1')
        ->where('day',$day)
        ->select('opening_time','closing_time')
        ->first();

        $result_arr['store_name'] = $user['store_name'];
        $result_arr['profile_image'] = $user['profile_image'];
        $result_arr['address'] = $user['address'];
        $result_arr['today_shop_opening_time'] = $shop_time['opening_time'];
        
        //orders section
        $todays_new_shop_orders = Booking::Where('status','!=','2')
                                ->where('booking_service_type','shop')
                                ->where('sp_id',$user_id)
                                ->whereDate('created_at', $current_date)
                                ->count();

        $todays_new_home_orders = Booking::Where('status','!=','2')
                                  ->where('booking_service_type','home')
                                  ->where('sp_id',$user_id)
                                  ->whereDate('created_at', $current_date)
                                  ->count();

        $todays_online_sales = Booking::Where('status','!=','2')
                        ->select(DB::raw('sum(total_item_amount) as amount'))
                        ->whereDate('created_at', $current_date)
                        ->where('sp_id',$user_id)
                        ->where('payment_type','0')
                        ->first();

        $todays_offline_sales = Booking::Where('status','!=','2')
                        ->select(DB::raw('sum(total_item_amount) as amount'))
                        ->whereDate('created_at', $current_date)
                        ->where('sp_id',$user_id)
                        ->where('payment_type','1')
                        ->first();


        $total_confirmed_orders = Booking::Where('status','!=','2')
                        ->whereDate('appointment_date', $current_date)
                        ->where('sp_id',$user_id)
                        ->count();

        $todays_confirmed_orders = Booking::Where('status','1')
                        ->whereDate('appointment_date', $current_date)
                        ->where('sp_id',$user_id)
                        ->count();


        $todays_closed_orders = Booking::Where('status','5')
                        ->whereDate('created_at', $current_date)
                        ->where('sp_id',$user_id)
                        ->count();

        $result_arr['orders'] = [
            'todays_new_shop_orders'=>$todays_new_shop_orders, 
            'todays_new_home_orders'=>$todays_new_home_orders, 
            'todays_online_sales'=>round($todays_online_sales['amount'],2), 
            'todays_offline_sales'=>round($todays_offline_sales['amount'],2), 
            'todays_confirmed_orders'=>$todays_confirmed_orders, 
            'todays_total_orders'=>$total_confirmed_orders, 
            'todays_closed_orders'=>$todays_closed_orders, 
            
        ];
        //---------------------

        //customers section 
       
        // $total_referred_customer = Booking::Where('sp_id',$user_id)
        //              ->leftJoin('users', function($join) {
        //                $join->on('bookings.user_id', '=', 'users.id');
        //              })
        //              ->where('users.refer_code',$user->share_code)
        //              ->count();


        $total_referred_customer = User::where('refer_code',$user->share_code)->count();


        $share_code = $user->share_code;
        $total_online_customer = Booking::Where('sp_id',$user_id)
                        ->leftJoin('users', function($join) {
                          $join->on('bookings.user_id', '=', 'users.id');
                        })
                        ->where('bookings.user_id','!=','')
                        ->where(function ($query) use ($share_code){
                            $query->where('users.refer_code','!=',$share_code)
                            ->orWhereNull('users.refer_code');

                        })->distinct()->count('bookings.user_id');
                        
        $total_ask_res = ForumCategory::where('category_id',$user['category_id'])->pluck('forum_id');

        $total_res = ForumComment::whereIn('forum_id',$total_ask_res)->where('sp_id',$user_id)->count();

        $result_arr['customers'] = [
            'total_referred_customer'=>$total_referred_customer, 
            'total_online_customer'=>$total_online_customer, 
            'todays_ask_received'=>count($total_ask_res), 
            'pending_response'=>count($total_ask_res) - $total_res, 
        
            
        ];

        //----------------------

        //operations section
          $todays_shop_appointments = Booking::where('booking_service_type','shop')
                                // Where('status','0')
                                ->where('sp_id',$user_id)
                                ->where('appointment_date', $current_date)
                                ->count();
          $todays_home_appointments = Booking::where('booking_service_type','home')
                                // Where('status','0')
                                ->where('sp_id',$user_id)
                                ->where('appointment_date', $current_date)
                                ->count();

          $total_staff = Staff::where('sp_id',$user_id)->count();

          $leave_staff = Staff::leftJoin('staff_leaves', function($join) {
                                      $join->on('staff.id', '=', 'staff_leaves.staff_id');
                                    })
                                    ->where('staff.sp_id',$user_id)
                                    ->where('staff_leaves.from_date','<=',$current_date)
                                    ->where('staff_leaves.to_date','>=',$current_date)
                                    ->count();

          $available_staff = $total_staff - $leave_staff;

          $todays_cancellations = Booking::Where('status','2')
                                ->where('sp_id',$user_id)
                                ->whereDate('cancelled_date_time', $current_date)
                                ->count();


        $result_arr['operations'] = [
            'todays_shop_appointments'=>$todays_shop_appointments, 
            'todays_home_appointments'=>$todays_home_appointments, 
            'total_staff'=>$total_staff, 
            'available_staff'=>$available_staff, 
            'todays_cancellations'=>$todays_cancellations, 
        
            
        ];


        //----------------

        //reality check
         $lang = '';
            if(app()->getLocale()!= 'en'){
                $lang = '_ar';
            }
          $total_staff_likes = Review::
                        leftJoin('staff', function($join) {
                          $join->on('staff.id', '=', 'reviews.staff_id');
                        })
                        ->select('staff.id','staff.staff_name',DB::raw('count(reviews.staff_like_dislike) as total_like'))
                        ->where('reviews.sp_id',$user_id)
                        ->where('reviews.staff_like_dislike','1')
                        ->groupBy('reviews.staff_id')
                        ->orderBy('total_like','DESC')
                        ->first();

            $total_offer_likes = SpOffer::
                        leftJoin('spectacular_offer_likes', function($join) {
                          $join->on('sp_offers.id', '=', 'spectacular_offer_likes.offer_id');
                        })
                        ->leftJoin('offers', function($join) {
                          $join->on('offers.id', '=', 'sp_offers.offer_id');
                        })

                        // ->leftJoin('spectacular_offer_types', function($join) {
                        //   $join->on('spectacular_offer_types.id', '=', 'sp_offers.spectacular_offer_type_id');
                        // })


                        ->select('sp_offers.id','offers.title'.$lang.' as offer_name',DB::raw('count(spectacular_offer_likes.id) as total_like'))
                        ->where('sp_offers.user_id',$user_id)
                        ->groupBy('spectacular_offer_likes.offer_id')
                        ->orderBy('total_like','DESC')
                        ->first();
                        // echo $total_offer_likes;die;

            $total_services_likes = Review::
                        leftJoin('review_sp_services', function($join) {
                          $join->on('review_sp_services.review_id', '=', 'reviews.id');
                        })
                        ->leftJoin('sp_services', function($join) {
                          $join->on('review_sp_services.service_id', '=', 'sp_services.id');
                        })
                        ->leftJoin('services', function($join) {
                          $join->on('sp_services.service_id', '=', 'services.id');
                        })
                        ->select('services.name'.$lang.' as name',DB::raw('count(review_sp_services.like_dislike) as total_like'))
                        ->where('reviews.sp_id',$user_id)
                        ->where('review_sp_services.like_dislike','1')
                        ->groupBy('review_sp_services.service_id')
                        ->orderBy('total_like','DESC')
                        ->first();

                        // print_r($total_services_likes);die;
           $average_rating = $this->getCombinedReviews($user_id)['average_rating'];

           $result_arr['reality_check'] = [

            'beutics_score'=>$average_rating, 
            'beutics_score_label'=>returnColorCode($average_rating),
            'staff_preferred'=>$total_staff_likes, 
            'offer_preferred'=>$total_offer_likes, 
            'service_preferred'=>$total_services_likes, 
            
        ];
        // echo "<pre/>";print_r($result_arr);die;
    	
    	return view('spadmin.pages.dashboard',compact('user_detail','title','user_count','shop_open_time','result_arr'));
    }

    public function getCombinedReviews($sp_id)
    {
        $reviews_ques_arr = \Config::get('constants.review_questions_arr');
        $rating_arr = \Config::get('constants.rating_arr');

        $reviews = Review::where('sp_id',$sp_id)->get();

        if(count($reviews)>0){

            $review_arr = array();
        
            foreach($reviews as $rows)
            {
                $rating_calc = array();
                foreach($rows->getAssociatedReviewQuestions as $review_ques)
                {
                    $ques_value =  $reviews_ques_arr[$review_ques['ques_id']]['value'];
                    $ans =  $rating_arr[$review_ques['rating']]['value'];

                    $rating_calc[] = $ques_value*$ans;
                }

                $final_rating = (array_sum($rating_calc))/10;

                $review_arr[$rows['id']] = $final_rating;

            }
             
            $total_reviews = array_sum($review_arr)/count($review_arr);
        
            $finale_data = array();
            $finale_data['average_rating'] = round($total_reviews,1);
            $finale_data['total_reviewers'] = count($review_arr);
            
            return  $finale_data;
        }
    }
}
