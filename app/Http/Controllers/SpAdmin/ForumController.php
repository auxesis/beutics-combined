<?php

namespace App\Http\Controllers\SpAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input; 

use Session;
use App\Model\Forum;
use App\Model\ForumCategory;
use App\Model\ForumBannerImage;
use App\Model\ForumComment;
use App\Model\ForumReplyBannerImage;
use App\Model\ForumCommentLikeDislike;
use App\Model\User;
use DB;

class ForumController extends Controller
{
    
    protected $title;
    protected $model;
    protected $module;

    public function __construct()
    {
        $this->title = 'Forum';
        $this->model = 'forum';
        $this->module = 'forum';
    } 

    public function customerQueriesIndex()
    {
        $title = __('messages.Forum Queries');
        $breadcum = [$title =>''];

        return view('spadmin.forums.queries',compact('title','model','breadcum'));

    }

    public function customerGetQueries(Request $request){


        $user_data = spLoginData();
        $category_id = $user_data['category_id'];
        $category_query = ForumCategory::where('category_id',$user_data['category_id'])->get();
        $forum_arr = array();

        foreach($category_query as $cat_rows){
            $forum_arr[] = $cat_rows['forum_id'];
        }

        //dd($forum_arr);
        $userid = $user_data['id'];
        $columns = ['id','subject','message','date','username'];

        $forumids = ForumComment::where('sp_id',$userid)->pluck('forum_id');
        
        $totalData = Forum::select('forums.id','forums.message','forums.subject','forums.user_id','users.name as username','forums.created_at as date')
                ->leftJoin('users', function($join) {
                  $join->on('forums.user_id', '=', 'users.id');
                })
                ->where('forums.status','1')
                ->whereIn('forums.id',$forum_arr)
                ->whereNotIn('forums.id',$forumids)
                ->groupBy('forums.id')->count();
                

        //dd($totalData->get());
               
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        $row = Forum::select('forums.id','forums.message','forums.subject','forums.user_id','users.name as username','forums.created_at as date')
                ->leftJoin('users', function($join) {
                  $join->on('forums.user_id', '=', 'users.id');
                })
                ->where('forums.status','1')
                ->whereIn('forums.id',$forum_arr)
                ->whereNotIn('forums.id',$forumids)
                ->groupBy('forums.id');
        
      
        if (!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = $row->where(function($query) use ($search) {
                $query
                //where('forums.message', 'LIKE', "%{$search}%")
                        ->where('forums.subject', 'LIKE', "%{$search}%")
                        ->orWhere('users.name', 'LIKE', "%{$search}%");
                        
            });
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('id', 'DESC')
                ->get();
        
        $data = array();
        if (!empty($row)) {

            foreach ($row as $key => $rows) {

                //dd($rows);
                // $nestedData['id'] = $rows->id;
                //$nestedData['message'] = $rows->message;
                $nestedData['subject'] = $rows->subject;
                $nestedData['username'] = $rows->username;
                $nestedData['date'] = $rows->date;
                               
  
                $nestedData['action'] =  getButtons([
                                ['key'=>'view','link'=>route('forum.customer.queries.view',[$rows->id])]
                            ]);
                
                $data[] = $nestedData;
            }

        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

    
    public function customerViewQuery(Request $request ,$id){
        $title = __('messages.Forum');
        $breadcum = [$title =>''];

        $user_data = spLoginData();
        $category_id = $user_data['category_id'];
        $sp_user_id = $user_data['id'];

        $category_query = ForumCategory::where('category_id',$user_data['category_id'])->get();
        $forum_arr = array();

        foreach($category_query as $cat_rows){
            $forum_arr[] = $cat_rows['forum_id'];
        }

        if(!in_array($id, $forum_arr)){
            return redirect(route('forum.customer.queries'));
        }

        $forum = Forum::where('id',$id)->first();

        if(isset($_POST['message'])){
            $message = $_POST['message'];

            $res = ForumComment::Create([
                'forum_id'=>$id,
                'sp_id'=>$user_data['id'],
                // 'user_id'=>"",
                'message' => $message
            ]);


            if($_FILES && $res){
                $counter = 1;
                foreach($_FILES as $fl){
                    //dd($fl);
                    if ($request->hasFile('image'.$counter) && $request->file('image'.$counter))
                    {
                        $file = $request->file('image'.$counter);

                        $name = time().$counter.'.'.$file->getClientOriginalExtension();

                        $destinationPath = public_path('/sp_uploads/forum_reply_banners/');
                        $img = $file->move($destinationPath, $name);


                        $row = new ForumReplyBannerImage();
                        $row->comment_id = $res->id;
                        $row->banner_path = $name;
                        $row->save();

                        $counter++;
                    }
                }    
            }

            return redirect(route('forum.customer.responded.queries'));           

        }


        
        if(isset($_POST['like'])){
           $comment_id = $_POST['comment_id'];

           $check = ForumCommentLikeDislike::where('sp_id',$sp_user_id)->where('comment_id',$comment_id)->exists();
           
           if(!$check){
               $row = new ForumCommentLikeDislike();
               $row->comment_id = $comment_id;
               $row->sp_id = $sp_user_id;
               $row->isLike = '1';
               $row->save(); 
           }
           else{
               ForumCommentLikeDislike::where('sp_id',$sp_user_id)->where('comment_id',$comment_id)->update(['isLike'=>'1']);
           }
           
           return redirect(route('forum.customer.queries.view',$id));
        }

        if(isset($_POST['dislike'])){
           $comment_id = $_POST['comment_id'];
           $check = ForumCommentLikeDislike::where('sp_id',$sp_user_id)->where('comment_id',$comment_id)->exists();
           
           if(!$check){
               $row = new ForumCommentLikeDislike();
               $row->comment_id = $comment_id;
               $row->sp_id = $sp_user_id;
               $row->isLike = '0';
               $row->save(); 
           }
           else{
               ForumCommentLikeDislike::where('sp_id',$sp_user_id)->where('comment_id',$comment_id)->update(['isLike'=>'0']);
           }

           return redirect(route('forum.customer.queries.view',$id));
        }
        
        $banners = ForumBannerImage::where('forum_id',$id)->get();
        
        $responses = ForumComment::where('forum_id',$id)->count();

        $user = User::select('name')->where('id',$forum->user_id)->first();
        $username = $user->name;
        $posted_on = $forum->created_at;

        $field_check = 'sp_id';
   
        $user_comment_ids = ForumComment::where([['forum_id',$id], [$field_check,$sp_user_id]])->pluck('id')->toArray();

        $responses_list = ForumComment::select('sp_users.name as sp_username','users.name as username','forum_comments.message','users.image as user_image','sp_users.profile_image as sp_image','sp_users.store_name','sp_users.store_type', 'forum_comments.created_at as date','forum_comments.id')->where('forum_id',$id);
        
        if(!empty($user_comment_ids)){
            
            $responses_list = $responses_list->orderByRaw(DB::raw("FIELD(forum_comments.id, ".  implode(',', $user_comment_ids).") DESC"), 'forum_comments.id');

        }

        $responses_list = $responses_list
        ->with('getAssociatedCommentBannerImages')
        ->with('getAssociatedLikeDislike')
        ->leftJoin('users', function($join) {
                  $join->on('forum_comments.user_id', '=', 'users.id');
                })
        ->leftJoin('sp_users', function($join) {
                  $join->on('forum_comments.sp_id', '=', 'sp_users.id');
                })
        ->get();

        $response_check = ForumComment::where('forum_id',$id)->where('sp_id',$sp_user_id)->count();

        //dd($responses_list);
        
        return view('spadmin.forums.view',compact('title','model','breadcum','forum','banners','username','responses','responses_list','sp_user_id','response_check','posted_on'));
    }

    public function  customerRespondedQueriesIndex(Request $request)
    {
        $title = __('messages.Forum Responded Queries');
        $breadcum = [$title =>''];

        return view('spadmin.forums.responded',compact('title','model','breadcum'));
    }

    
    public function customerGetResponded(Request $request){

        $user_data = spLoginData();
        $category_id = $user_data['category_id'];
        $category_query = ForumCategory::where('category_id',$user_data['category_id'])->get();
        $forum_arr = array();

        foreach($category_query as $cat_rows){
            $forum_arr[] = $cat_rows['forum_id'];
        }

        //dd($forum_arr);
        $userid = $user_data['id'];
        $columns = ['id','subject','message','date','username'];

        $forumids = ForumComment::where('sp_id',$userid)->pluck('forum_id');
        
        $totalData = Forum::select('forums.id','forums.message','forums.subject','forums.user_id','users.name as username','forums.created_at as date')
                ->leftJoin('users', function($join) {
                  $join->on('forums.user_id', '=', 'users.id');
                })
                ->where('forums.status','1')
                //->whereIn('forums.id',$forum_arr)
                ->whereIn('forums.id',$forumids)
                ->groupBy('forums.id')->count();
                

        //dd($totalData->get());
               
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        $row = Forum::select('forums.id','forums.message','forums.subject','forums.user_id','users.name as username','forums.created_at as date')
                ->leftJoin('users', function($join) {
                  $join->on('forums.user_id', '=', 'users.id');
                })
                ->where('forums.status','1')
                //->whereIn('forums.id',$forum_arr)
                ->whereIn('forums.id',$forumids)
                ->groupBy('forums.id');
        
      
        if (!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = $row->where(function($query) use ($search) {
                $query
                //where('forums.message', 'LIKE', "%{$search}%")
                        ->where('forums.subject', 'LIKE', "%{$search}%")
                        ->orWhere('users.name', 'LIKE', "%{$search}%");
                        
            });
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('id', 'DESC')
                ->get();
        
        $data = array();
        if (!empty($row)) {

            foreach ($row as $key => $rows) {

                //dd($rows);
                $nestedData['id'] = $rows->id;
                //$nestedData['message'] = $rows->message;
                $nestedData['subject'] = $rows->subject;
                $nestedData['username'] = $rows->username;
                $nestedData['date'] = $rows->date;
                               
  
                $nestedData['action'] =  getButtons([
                                ['key'=>'view','link'=>route('forum.customer.queries.view',[$rows->id])]
                            ]);
                
                $data[] = $nestedData;
            }

        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

}
