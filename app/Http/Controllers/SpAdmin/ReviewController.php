<?php

namespace App\Http\Controllers\SpAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input; 

use Session;
use App\Model\Review;
use App\Model\RatingReview;
use DB;

class ReviewController extends Controller
{
    
    protected $title;
    protected $model;
    protected $module;

    public function __construct()
    {
        $this->title = 'Reviews';
        $this->model = 'review';
        $this->module = 'review';
    } 

    public function index()
    {
        $title = __('messages.Reviews');
        $breadcum = [$title =>''];
        $customer_reviews = array();
        $customer_combined_reviews = array();
        $customer_reviews = $this->getCustomerReviews();
        $customer_combined_reviews = $this->getCombinedReviews();

        return view('spadmin.review.index',compact('title','model','breadcum','customer_reviews','customer_combined_reviews'));

    }

    public function getCustomerReviews()
    {
       
        $user_data = spLoginData();//helper function
        $user_id = $user_data['id'];
       

        $results = RatingReview::select('rating_reviews.id','reviews.customer_name','reviews.description','reviews.created_at',DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1) as rating"))
            ->leftJoin('reviews', function($join) {
                  $join->on('rating_reviews.id', '=', 'reviews.id');
                })
            ->where('rating_reviews.sp_id',$user_id);            
            
            $results = $results->groupBy('rating_reviews.id')->paginate(10);
            // $rows_arr = $results->toArray();
            return $results;
    }


    public function getCustomerReviewsFilter(Request $request)
    {
       
        $user_data = spLoginData();//helper function
        $user_id = $user_data['id'];
       

        $results = RatingReview::select('rating_reviews.id','reviews.customer_name','reviews.user_id','reviews.description','reviews.created_at',DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1) as rating"))
            ->leftJoin('reviews', function($join) {
                  $join->on('rating_reviews.id', '=', 'reviews.id');
                })
            ->where('rating_reviews.sp_id',$user_id);


            if($request->filter == 'needs_improvement'){
                 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'>',2.3);
                 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'<',5);                         
            }
            elseif($request->filter == 'average'){
                 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'>',4.9);
                 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'<=',6);                            
            }
            elseif($request->filter == 'good'){
                 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'>',6);
                 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'<=',7);                            
            }
            elseif($request->filter == 'very_good'){
                 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'>',7);
                 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'<=',8);                            
            }
            elseif($request->filter == 'superb'){
                 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'>',8);
                 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'<=',9);                            
            }elseif($request->filter == 'exceptional'){
                 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'>',9);
                 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'<=',10);                           
            }               
            
            $results = $results->groupBy('rating_reviews.id')->paginate(10);
            // print_r($results);die;
            // $rows_arr = $results->toArray();
            return view('spadmin.review.customerReviewsFilter',compact('results'));
    }

    public function getCombinedReviews()
    {
        $user_data = spLoginData();//helper function
        $user_id = $user_data['id'];
        $reviews_ques_arr = \Config::get('constants.review_questions_arr');
        $rating_arr = \Config::get('constants.rating_arr');

        $reviews = Review::where('sp_id',$user_id)->get();
        if(count($reviews)>0){
            $rows_arr = $reviews->toArray();

            $review_arr = array();
        
            $service_wise_arr = array();
            foreach($reviews as $rows)
            {
                $rating_calc = array();
                foreach($rows->getAssociatedReviewQuestions as $review_ques)
                {
                    $ques_value =  $reviews_ques_arr[$review_ques['ques_id']]['value'];
                    $ans =  $rating_arr[$review_ques['rating']]['value'];

                    $rating_calc[] = $ques_value*$ans;
                }
                // echo "<pre/>";print_r($rows->getAssociatedReviewServices);die;
                foreach($rows->getAssociatedReviewServices as $review_sr)
                {
                    if(!isset($service_wise_arr[$review_sr['service_id']]['dislike_count'])) {
                        $service_wise_arr[$review_sr['service_id']]['dislike_count'] = 0;
                    }

                    if($review_sr['like_dislike'] == 0){
                        $service_wise_arr[$review_sr['service_id']]['dislike_count'] += (integer)1;
                        
                    }
                    
                    if(isset($service_wise_arr[$review_sr['service_id']]['like_count'])){
                        $service_wise_arr[$review_sr['service_id']]['like_count'] += (integer)$review_sr['like_dislike'];
                        
                    } else{
                        $service_wise_arr[$review_sr['service_id']]['like_count'] = 0 + (integer)$review_sr['like_dislike'];
                        
                    }
                    //$service_wise_arr[$review_sr['service_id']]['likes_dislikes'][] =  $review_sr['like_dislike'];
                    $sr_name = 'name'.$this->getLang();
                    if(!isset($service_wise_arr[$review_sr['service_id']]['service_id'])) {
                        $service_wise_arr[$review_sr['service_id']]['service_id'] =  $review_sr['service_id'];
                        $service_wise_arr[$review_sr['service_id']]['service_name'] =  $review_sr->getAssociatedReviewService->getAssociatedService->$sr_name;
                    }
                }


                $final_rating = (array_sum($rating_calc))/10;

                $review_arr[$rows['id']] = $final_rating;

            }
             array_multisort(array_map(function($element) {
                  return $element['like_count'];
              }, $service_wise_arr), SORT_DESC, $service_wise_arr);
             $top_ten = array_slice($service_wise_arr, 0,10);
             
            $total_reviews = array_sum($review_arr)/count($review_arr);
        
            $question_wise_arr = array();
            foreach($reviews as $rows)
            {
                foreach($rows->getAssociatedReviewQuestions as $review_ques)
                {
                    $question_wise_arr[$review_ques['ques_id']][] = $rating_arr[$review_ques['rating']]['value'];
                }

            }
            
            $ques_final_calc = array();

            foreach($question_wise_arr as $key => $val){
                $ques_final_calc[$reviews_ques_arr[$key]['key_value']] = round((array_sum($val))/count($val),0);
            }

            $finale_data = $ques_final_calc;
            $finale_data['average_rating'] = round($total_reviews,1);
            $finale_data['total_reviewers'] = count($review_arr);
            $finale_data['services'] = $service_wise_arr;
            // echo "<pre/>";print_r($finale_data);die;
            return  $finale_data;
        }
    }
}
