<?php

namespace App\Http\Controllers\SpAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;   

use App\Model\SpService;
use App\Model\SpUser;
use App\Model\Category;
use App\Model\Service;
use App\Model\SpServicePrice;
use App\Model\SpOffer;
use datatables;
use App;
use Session;
use DB;

class ServiceController extends Controller
{
    protected $title;
    protected $model;

    public function __construct()
    {
        $this->title = 'Services';
        $this->model = 'services';
    } 

    public function index()
    {
       
        $title = __('messages.Services'); 
        $model = $this->model;   
        $breadcum = [$title =>''];
        return view('spadmin.service.index',compact('title','model','breadcum'));
    }

    public function getData(Request $request)
    {
        $user_data = spLoginData();//helper function
        $spid = $user_data['id'];
        $columns = ['service_id','subcat_name','created_at','action'];
        $totalData = SpService::where('user_id',$spid)->where('is_deleted','!=','1')->count();
        
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        $row = SpService::where('user_id',$spid)->where('is_deleted','!=','1')->select('sp_services.*');

        if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = $row->leftJoin('services', 'services.id', '=', 'sp_services.service_id')
                    ->where(function($query) use ($search) {
                        $query->Where('services.name', 'LIKE', "%{$search}%");
                        
                    });
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('id', 'desc')
                ->get();
        // print_r($users);die;
        $data = array();
        if (!empty($row)) {
            $sr_name = 'name'.$this->getLang();
            $cat_name = 'category_name'.$this->getLang();
            foreach ($row as $key => $rows) {

                $associated_offfers = $rows->getAssociatedOffers;
                $offer_asso = count($associated_offfers);

                $nestedData['service_id'] = $rows->getAssociatedService->$sr_name;
                $nestedData['subcat_name'] = $rows->getServiceSubcategory->getSubcat->$cat_name;
                $nestedData['created_at'] = date('m-d-y h:i A',strtotime($rows->created_at));

                $nestedData['action'] =  getButtons([
                                ['key'=>'edit','link'=>route('services.edit',$rows->id)],
                                ['key'=>'view','link'=>route('services.show',$rows->id)],
                                ['key'=>'delete_assoc','link'=>route('services.destroy',$rows->id), 'asso'=>$offer_asso],
                            ]);

                
                
                $data[] = $nestedData;
            }

        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }


    public function create()
    {
        $title = __('messages.Create');
        $model = $this->model;   
        $breadcum = [__('messages.Services') =>route($model.'.index'),$title =>''];
        $subcat_arr = $this->getSubcategoryName();
        return view('spadmin.service.create',compact('title','model','breadcum','subcat_arr'));
    }

    public function getSubcategoryName()
    {
        $user_data = spLoginData();//helper function

        $cat_data = Category::where('parent_id',$user_data['category_id'])->get();
        $cat_arr = array();
        $cat_name = 'category_name'.$this->getLang();
        foreach($cat_data as $data)
        {
            $cat_arr[$data['id']] = $data[$cat_name]; 
        }
         
        return $cat_arr;
    }

    public function ajaxGetSubcatServices(Request $request)
    {
        $user_data = spLoginData();//helper function

        $subcat_id = $request->subcat_id;
        $rows = Service::where('subcat_id',$subcat_id)->get();
        $service_arr = array();
        foreach($rows as $data){
            $check = SpService::where('user_id',$user_data['id'])->where('service_id',$data['id'])->where('is_deleted','!=','1')->count();
            $sr_name = 'name'.$this->getLang();
            if($check == 0){
                 $service_arr[$data['id']] = $data[$sr_name]; 
            }
        }
        return view('spadmin.service.ajaxGetSubcatServices',compact('service_arr'));
    }

    public function ajaxAddServiceData(Request $request)
    {
        $service_id = $request->service_id;
        $user_data = spLoginData();//helper function
        $spid = $user_data['id'];

        $sp_row = SpUser::where('id',$spid)->first();
        return view('spadmin.service.ajaxAddServiceData',compact('sp_row'));
    }

    public function checkProvideHomeService(Request $request)
    {
        $user_data = spLoginData();//helper function
        $spid = $user_data['id'];

        $is_home_service = $request->is_home_service;
        if($is_home_service == '1'){
            $row = SpUser::where('id',$spid)->where('is_provide_home_service','1')->count();
           
            if($row>0){
                return 'noshow';
            }else{
                return 'show';
            }
        }
        
    }

    public function activeHomeService(Request $request)
    {
             
        $user_data = spLoginData();//helper function
        $user_id = $user_data['id'];
        // print_r($request->all());die;
        $srow = SpUser::where('id',$user_id)->first();
        $srow['is_provide_home_service'] = $request->is_home_service;

        if($request->is_home_service!=0){
            $srow['mov'] = $request->mov;
        }else{
            $srow['mov'] = '0.00';
        }

        $srow->save();
        if( $srow->save()){
            return 'success';
        }
                  
          
    }

    public function store(Request $request)
    {
       try
       {
            $data = $request->all();
              $validator = Validator::make($data, 
                [
                  'service_id' => 'required',
                ]);
              if ($validator->fails()) 
              {
                
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
              }     
              else 
              {  
                $user_data = spLoginData();//helper function
                $user_id = $user_data['id'];
                $row = SpService::where('service_id',$request->service_id)->where('user_id',$user_id)->where('is_deleted','1')->first();

                if(!$row){
                    $row = new SpService();
                }

                $row['service_id'] = $request->service_id;
                $row['user_id'] = $user_id;
                $row['is_deleted'] = '0';

                $row->save();

                if($row->save()){
                    
                    
                    $keyarr = [
                        'isMen'=>[
                            'MEN_ORIGINALPRICE'=>'men_originalprice',
                            'MEN_DISCOUNT'=>'men_discount',
                            'MEN_BESTPRICE'=>'men_bestprice'
                        ],
                        'isWomen'=>[
                            'WOMEN_ORIGINALPRICE'=>'women_originalprice',
                            'WOMEN_DISCOUNT'=>'women_discount',
                            'WOMEN_BESTPRICE'=>'women_bestprice'
                        ],
                        'isKids'=>[
                            'KIDS_ORIGINALPRICE'=>'kids_originalprice',
                            'KIDS_DISCOUNT'=>'kids_discount',
                            'KIDS_BESTPRICE'=>'kids_bestprice'
                        ],
                        'isMen_home_service'=>[
                            'MEN_HOME_ORIGINALPRICE'=>'men_home_originalprice',
                            'MEN_HOME_DISCOUNT'=>'men_home_discount',
                            'MEN_HOME_BESTPRICE'=>'men_home_bestprice'
                        ],
                        'isWomen_home_service'=>[
                            'WOMEN_HOME_ORIGINALPRICE'=>'women_home_originalprice',
                            'WOMEN_HOME_DISCOUNT'=>'women_home_discount',
                            'WOMEN_HOME_BESTPRICE'=>'women_home_bestprice'
                        ],
                        'isKids_home_service'=>[
                            'KIDS_HOME_ORIGINALPRICE'=>'kids_home_originalprice',
                            'KIDS_HOME_DISCOUNT'=>'kids_home_discount',
                            'KIDS_HOME_BESTPRICE'=>'kids_home_bestprice'
                        ]
                    ];
                    $entity_arr = [];
                    foreach($keyarr as $key => $key_vals){
                        if($request->$key){
                            foreach($key_vals as $key_key=>$key_row){
                                $entity = SpServicePrice::where('sp_service_id',$row->id)->count();
                                if(count($entity)>0){
                                    SpServicePrice::where('sp_service_id',$row->id)->delete();
                                }
                                $entity = new SpServicePrice();
                                $entity->sp_service_id = $row->id;
                                $entity->field_key = $key_key;
                                $entity->price = $request->$key_row;
                                $data_for_save[] = $entity;
                            }
                        }
                    }

                    $row->getRelatedPrices()->saveMany($data_for_save);
                    Session::flash('success', __('messages.Service Added successfully.'));
                    return redirect()->route('services.index');
                    
                }
                
              }
       }
       catch(\Exception $e){
         return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
       }
    }

 
    public function show($id)
    {
        $title = __('messages.View Services');
        $model = $this->model;

        $rows = SpService::where('id',$id)->first();
        $breadcum = [__('messages.Services') =>route($model.'.index'),$title =>''];
        $sr_name = 'name'.$this->getLang();
        $cat_name = 'category_name'.$this->getLang();
        return view('spadmin.service.view',compact('title','model','rows','breadcum', 'cat_name', 'sr_name'));
    }

    public function edit($id)
    {
        $title = __('messages.Edit');
        $model = $this->model;   
        $breadcum = [__('messages.Services') =>route($model.'.index'),$title =>''];
        $rows = SpService::where('id',$id)->first();

        //checking which gender is selected
        $keyarr = [
            'MEN_ORIGINALPRICE'=>'isMen',
            'WOMEN_ORIGINALPRICE'=>'isWomen',
            'KIDS_ORIGINALPRICE'=>'isKids',
            'MEN_HOME_ORIGINALPRICE'=>'isMen_home_service',
            'WOMEN_HOME_ORIGINALPRICE'=>'isWomen_home_service',
            'KIDS_HOME_ORIGINALPRICE'=>'isKids_home_service',
        ];
        $selected_gender_arr = array();
        foreach($rows->getRelatedPrices as $key => $prices){
            if(array_key_exists($prices['field_key'],$keyarr)){
                $selected_gender_arr[] = $keyarr[$prices['field_key']];
            }
        }
        $prices_arr = array();
        foreach($rows->getRelatedPrices as $key => $prices){
            $prices_arr[$prices['field_key']] = $prices['price'];
            
        }

        $service_current = array();
        // $associated_offers = $row->getAssociatedOffers->where('is_deleted','!=','1');
        foreach($rows->getAssociatedOffers as $key_price => $offer_row){
            
            // $service_current['offers']['offer_ids'] = $offer_row['sp_offer_id'];
            // $service_current['offers']['gender'] = $offer_row['gender'];

            $service_current[] = [
                'id' => $offer_row['sp_offer_id'],
                'gender' => $offer_row['gender'],
            ];
        }
        $sr_name = 'name'.$this->getLang();
        $cat_name = 'category_name'.$this->getLang();
        return view('spadmin.service.edit',compact('title','model','breadcum','rows','selected_gender_arr','prices_arr','sr_name','cat_name','service_current'));
    }


    public function update(Request $request, $id)
    {
       try
       { 
        $row = SpService::where('id',$id)->first();
        $offers = array();
        foreach($row->getAssociatedOffers as $key_price => $offer_row){
    
            $offers[] = $offer_row['sp_offer_id'];
        }

        $check_offer = [
            'isMen'=> ['men','shop'],
            'isWomen'=> ['women','shop'],
            'isKids'=> ['kids','shop'],
            'isMen_home_service'=> ['men','home'],
            'isWomen_home_service'=> ['women','home'],
            'isKids_home_service'=> ['kids','home'],
        ];
        $sim_key_arr = ['isMen','isWomen','isKids','isMen_home_service','isWomen_home_service','isKids_home_service'];

        $keyarr = [
            'isMen'=>[
                'MEN_ORIGINALPRICE'=>'men_originalprice',
                'MEN_DISCOUNT'=>'men_discount',
                'MEN_BESTPRICE'=>'men_bestprice'
            ],
            'isWomen'=>[
                'WOMEN_ORIGINALPRICE'=>'women_originalprice',
                'WOMEN_DISCOUNT'=>'women_discount',
                'WOMEN_BESTPRICE'=>'women_bestprice'
            ],
            'isKids'=>[
                'KIDS_ORIGINALPRICE'=>'kids_originalprice',
                'KIDS_DISCOUNT'=>'kids_discount',
                'KIDS_BESTPRICE'=>'kids_bestprice'
            ],
            'isMen_home_service'=>[
                'MEN_HOME_ORIGINALPRICE'=>'men_home_originalprice',
                'MEN_HOME_DISCOUNT'=>'men_home_discount',
                'MEN_HOME_BESTPRICE'=>'men_home_bestprice'
            ],
            'isWomen_home_service'=>[
                'WOMEN_HOME_ORIGINALPRICE'=>'women_home_originalprice',
                'WOMEN_HOME_DISCOUNT'=>'women_home_discount',
                'WOMEN_HOME_BESTPRICE'=>'women_home_bestprice'
            ],
            'isKids_home_service'=>[
                'KIDS_HOME_ORIGINALPRICE'=>'kids_home_originalprice',
                'KIDS_HOME_DISCOUNT'=>'kids_home_discount',
                'KIDS_HOME_BESTPRICE'=>'kids_home_bestprice'
            ]
        ];
        SpServicePrice::where('sp_service_id',$id)->delete();
        $get_offer_keys = array();
        $entity_arr = [];
        foreach($keyarr as $key => $key_vals){
            if($request->$key){
                $get_offer_keys[] = $key;
                foreach($key_vals as $key_key=>$key_row){

                    $entity = new SpServicePrice();
                    $entity->sp_service_id = $row->id;
                    $entity->field_key = $key_key;
                    $entity->price = $request->$key_row;
                    $data_for_save[] = $entity;
                }
            }
        }
        $row->getRelatedPrices()->saveMany($data_for_save);
        //check offer
        $k_arr = array_diff($sim_key_arr,$get_offer_keys);
        
        foreach($k_arr as $ky){
            if(count($offers)>0){
                foreach($offers as $off){
                    $gender = $check_offer[$ky][0];
                    $service_type = $check_offer[$ky][1];
                    $off_row = SpOffer::where('id',$off)->where('gender',$gender)->where('service_type',$service_type)->first();
                    if($off_row){
                        $off_row->is_deleted = '1';
                        $off_row->save();
                    }

                }
                
            }
        }
                        
        Session::flash('success', __('messages.Service Updated successfully.'));
        return redirect()->route('services.index');
        
        }
       catch(\Exception $e){
         return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
       }
    }

    public function destroy($id)
    {
        $row = SpService::where('id', $id)->first();
        
        if ($row) {

            $offer_arr = array();
            foreach($row->getAssociatedOffers as $offer_row){   
                $offer_arr[] = $offer_row['sp_offer_id'];
            }
            $row->is_deleted = '1';
            $row->save();

            SpOffer::whereIn('id',$offer_arr)->update(array('is_deleted'=>'1'));
           
            Session::flash('success', __('messages.Service deleted successfully'));
             return redirect()->back();
        } else {
            Session::flash('warning', __('messages.Invalid request.'));
            return redirect()->back();
        }
    }
}
