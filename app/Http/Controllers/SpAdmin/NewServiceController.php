<?php

namespace App\Http\Controllers\SpAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;   

use App\Model\SpService;
use App\Model\SpUser;
use App\Model\Category;
use App\Model\Service;
use App\Model\SpServicePrice;
use App\Model\SpOffer;
use App\Model\FitnessType;
use App\Model\FitnessService;
use App\Model\ServiceAttribute;
use App\Model\SpGroupService;
use App\Model\Staff;
use App\Model\SpServiceGoal;
use App\Model\SpServiceTrainer;
use datatables;
use App;
use Session;
use DB;

class NewServiceController extends Controller
{
    protected $title;
    protected $model;

    public function __construct()
    {
        $this->title = 'New Services';
        $this->model = 'services';
    } 

    public function index()
    {
        $title = __('messages.Services'); 
        $model = $this->model;   
        $breadcum = [$title =>''];
        $user_data = spLoginData();

        if($user_data['category_id'] == 3)
            return view('spadmin.service.new.indexmultiple',compact('title','model','breadcum'));
        else
            return view('spadmin.service.new.index',compact('title','model','breadcum'));
    }

    public function getData(Request $request)
    {
        $user_data = spLoginData();//helper function
        $spid = $user_data['id'];
        $columns = ['fitness_type','fitness_title','service_id','subcat_name','created_at','action'];
        $totalData = SpService::where('user_id',$spid)->where('is_deleted','!=','1')->where('services.cat_id',$user_data['category_id'])->leftJoin('services', 'services.id', '=', 'sp_services.service_id')->count();
        
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        $row = SpService::where('user_id',$spid)->where('is_deleted','!=','1')->where('services.cat_id',$user_data['category_id'])->select('sp_services.*','services.cat_id')->leftJoin('services', 'services.id', '=', 'sp_services.service_id'); 

        if(!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = $row->where(function($query) use ($search) {
                        $query->Where('services.name', 'LIKE', "%{$search}%");
                        
                    });
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('id', 'desc')
                ->get();

        $data = array();
        if (!empty($row)) {
            $sr_name = 'name'.$this->getLang();
            $cat_name = 'category_name'.$this->getLang();

            foreach ($row as $key => $rows) 
            {
                $nameArr = $catArr = $associated_offfers = array();
                if($rows->filter_key ==='single' || $rows->filter_key ==='multiple')
                {
                    //dd($rows->getAssociatedGroupService);
                    foreach($rows->getAssociatedGroupService as $groupservice)
                    {
                        $nameArr[] = $groupservice->getAssociatedService->$sr_name;
                        $catArr[] = $groupservice->getServiceSubcategory->getSubcat->$cat_name;
                        $associated_offfers = $rows->getAssociatedOffers;
                    }
                    $serviceName = implode(', ',$nameArr);
                    $catName = implode(', ',$catArr);
                }
                else  
                {
                    $serviceName = $rows->getAssociatedService->$sr_name;
                    $catName = $rows->getServiceSubcategory->getSubcat->$cat_name;
                    $associated_offfers = $rows->getAssociatedOffers;
                }
                $offer_asso = count($associated_offfers);
                if($user_data['category_id'] == 3)
                {
                    $nestedData['fitness_type'] = ($rows->service_type_id) ? $rows->getAssociatedFitnessType->name : '';
                    $nestedData['fitness_title'] = ($rows->service_title_id) ? $rows->getAssociatedFitnessTypeService->title : '';
                }
                $nestedData['service_id'] = $serviceName;
                $nestedData['subcat_name'] = $catName;
                $nestedData['created_at'] = date('m-d-y h:i A',strtotime($rows->created_at));

                $nestedData['action'] =  getButtons([
                                ['key'=>'edit','link'=> route('new-services.edit',$rows->id)],
                                ['key'=>'view','link'=>route('new-services.show',$rows->id)],
                                ['key'=>'delete_assoc','link'=>route('new-services.destroy',$rows->id), 'asso'=>$offer_asso],
                            ]);
                $data[] = $nestedData;
            }
        }
        //echo "<pre>"; print_r($data); exit;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }


    public function create()
    {
        $title = __('messages.Create');
        $user_data = spLoginData();
        $model = $this->model;   
        $breadcum = [__('messages.Services') =>route('new-'.$model.'.index'),$title =>''];
        $subcat_arr = $this->getSubcategoryName();
        $fitnesstype_arr = FitnessType::orderby('name', 'asc')->pluck('name', 'id')->toArray();
        $category_id = $user_data['category_id'];
        if($category_id == 3)
            return view('spadmin.service.new.createmultiple',compact('title','model','breadcum','subcat_arr','fitnesstype_arr','category_id'));
        else
            return view('spadmin.service.new.create',compact('title','model','breadcum','subcat_arr','fitnesstype_arr','category_id'));
    }

    public function getSubcategoryName()
    { 
        $user_data = spLoginData();//helper function

        $cat_data = Category::where('parent_id',$user_data['category_id'])->orderBy('category_name','ASC')->get();
        $cat_arr = array();
        $cat_name = 'category_name'.$this->getLang();
        foreach($cat_data as $data)
        {
            $cat_arr[$data['id']] = $data[$cat_name]; 
        }
         
        return $cat_arr;
    }

    public function ajaxGetSubcatServices(Request $request)
    {
        $user_data = spLoginData();//helper function
        $subcat_id = array();

        if($request->subcat_id != null){
            if(is_array($request->subcat_id))
            {
               $subcat_id = $request->subcat_id;
            }
            else
            {
                $subcat_id = array($request->subcat_id);
            }
        }

        $rows = Service::whereIn('subcat_id',$subcat_id)->where('parent_service_id','0')->orderBy('name','ASC')->get();
        $service_arr = array();
        foreach($rows as $data){
            $check = SpService::where('user_id',$user_data['id'])->where('service_id',$data['id'])->where('is_deleted','!=','1')->count();
            $sr_name = 'name'.$this->getLang();
            if($check == 0){
                 $service_arr[$data['id']] = $data[$sr_name]; 
            }
        }
        if($user_data['category_id'] == 3)
            return view('spadmin.service.new.ajaxGetSubcatServicesMultiple',compact('service_arr','subcat_id'));
        else
            return view('spadmin.service.new.ajaxGetSubcatServices',compact('service_arr','subcat_id'));
    }

    public function ajaxAddServiceData(Request $request)
    {
        $service_id = $request->service_id;
        $subcat_id = $request->subcat_id;
        $user_data = spLoginData();//helper function
        $spid = $user_data['id'];
        $delivery_mode = explode(',',$user_data['delivery_mode']);
        $sp_row = SpUser::where('id',$spid)->first();
        $cat_id = $user_data['category_id'];
        $attr_arr = $this->getCategoryServiceAttribute('id','name');
        $attribute_arr = $attr_arr['attribute_arr'];
        $attr_desc_arr = $attr_arr['attr_desc_arr'];

        $rows = staff::where('is_deleted','0')->where('sp_id',$spid)->orderBy('staff_name','ASC')->get();
        $trainer_arr = array();
        foreach($rows as $data){
            $speciality = explode(',',$data['speciality']);
            if (array_intersect($subcat_id, $speciality) || in_array('0',$speciality)) {
                $trainer_arr[$data['id']] = $data['staff_name'];
            }
        }
        $trainer_arr = array_unique($trainer_arr);
        if($user_data['category_id'] == 3)
        {
            return view('spadmin.service.new.ajaxAddServiceMultipleData',compact('sp_row','attribute_arr','attr_desc_arr','delivery_mode','subcat_id','trainer_arr','attr_desc_arr'));
        }else
        {
            return view('spadmin.service.new.ajaxAddServiceData',compact('sp_row','attribute_arr','attr_desc_arr','delivery_mode','subcat_id','trainer_arr','attr_desc_arr'));
        }
    }

    public function checkProvideHomeService(Request $request)
    {
        $user_data = spLoginData();//helper function
        $spid = $user_data['id'];

        $is_home_service = $request->is_home_service;
        if($is_home_service == '1'){
            $row = SpUser::where('id',$spid)->where('is_provide_home_service','1')->count();
           
            if($row>0){
                return 'noshow';
            }else{
                return 'show';
            }
        }
        
    }

    public function activeHomeService(Request $request)
    {
             
        $user_data = spLoginData();//helper function
        $user_id = $user_data['id'];
        // print_r($request->all());die;
        $srow = SpUser::where('id',$user_id)->first();
        $srow['is_provide_home_service'] = $request->is_home_service;

        if($request->is_home_service!=0){
            $srow['mov'] = $request->mov;
        }else{
            $srow['mov'] = '0.00';
        }

        $srow->save();
        if( $srow->save()){
            return 'success';
        }    
    }

    public function ajaxGetFitnessTypeServices(Request $request)
    { 
        $fitness_data = FitnessService::where('fitness_type_id',$request->fitness_type_id)->orderBy('title','ASC')->get();
        $fitness_service_arr = array();
        foreach($fitness_data as $data)
        {
            $fitness_service_arr[$data['id']] = $data['title']; 
        }

        return view('spadmin.service.new.ajaxGetFitnessServices',compact('fitness_service_arr'));
    }

    public function store(Request $request)
    { 
       try
       {
            $data = $request->all();
            $validator = Validator::make($data, 
            [
                'sub_category' => 'required',
                'service_id' => 'required',
            ]);
            if ($validator->fails()) 
            {
            
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }     
            else
            {
                $user_data = spLoginData();//helper function
                $user_id = $user_data['id'];
                $cat_id = $user_data['category_id'];

                $filter_key = '';
                if(!empty($request->sub_service_id))
                {
                    $service_id = $request->sub_service_id;
                }
                else
                {
                    $service_id = $request->service_id;
                }

                $row = SpService::where('service_id',$service_id)->where('user_id',$user_id)->where('is_deleted','1')->first();
                $attr_arr = $this->getCategoryServiceAttribute('url','id');
                $attributeKey = $attr_arr['attribute_arr'];
            
                if(!$row){
                    $row = new SpService();
                }

                $row['service_id'] = $service_id;
                $row['description'] = $request->description;
                $row['filter_key'] = $filter_key;
                $row['user_id'] = $user_id;
                $row['is_deleted'] = '0';

                $row->save();
                if($row->save())
                {
                    $keyarr = array();
                    $mainKey = array('isMen','isWomen','isKids');
                    $serviceKey = array('store_service','home_service','online_service');
                    $priceKey = [
                        'isMen' => [
                            'isMen_store_service'=>[
                                'MEN_ORIGINALPRICE'=>'men_originalprice',
                                'MEN_DISCOUNT'=>'men_discount',
                                'MEN_BESTPRICE'=>'men_bestprice'
                            ],
                            'isMen_home_service'=>[
                                'MEN_HOME_ORIGINALPRICE'=>'men_home_originalprice',
                                'MEN_HOME_DISCOUNT'=>'men_home_discount',
                                'MEN_HOME_BESTPRICE'=>'men_home_bestprice'
                            ],
                            'isMen_online_service'=>[
                                'MEN_ONLINE_ORIGINALPRICE'=>'men_online_originalprice',
                                'MEN_ONLINE_DISCOUNT'=>'men_online_discount',
                                'MEN_ONLINE_BESTPRICE'=>'men_online_bestprice'
                            ],
                        ],
                        'isWomen' => [
                            'isWomen_store_service'=>[
                                'WOMEN_ORIGINALPRICE'=>'women_originalprice',
                                'WOMEN_DISCOUNT'=>'women_discount',
                                'WOMEN_BESTPRICE'=>'women_bestprice'
                            ],
                            'isWomen_home_service'=>[
                                'WOMEN_HOME_ORIGINALPRICE'=>'women_home_originalprice',
                                'WOMEN_HOME_DISCOUNT'=>'women_home_discount',
                                'WOMEN_HOME_BESTPRICE'=>'women_home_bestprice'
                            ],
                            'isWomen_online_service'=>[
                                'WOMEN_ONLINE_ORIGINALPRICE'=>'women_online_originalprice',
                                'WOMEN_ONLINE_DISCOUNT'=>'women_online_discount',
                                
                                'WOMEN_ONLINE_BESTPRICE'=>'women_online_bestprice'
                            ]
                        ],
                        'isKids' => [
                            'isKids_store_service'=>[
                                'KIDS_ORIGINALPRICE'=>'kids_originalprice',
                                'KIDS_DISCOUNT'=>'kids_discount',
                                'KIDS_BESTPRICE'=>'kids_bestprice'
                            ],
                            'isKids_home_service'=>[
                                'KIDS_HOME_ORIGINALPRICE'=>'kids_home_originalprice',
                                'KIDS_HOME_DISCOUNT'=>'kids_home_discount',
                                'KIDS_HOME_BESTPRICE'=>'kids_home_bestprice'
                            ],
                            'isKids_online_service'=>[
                                'KIDS_ONLINE_ORIGINALPRICE'=>'kids_online_originalprice',
                                'KIDS_ONLINE_DISCOUNT'=>'kids_online_discount',
                                'KIDS_ONLINE_BESTPRICE'=>'kids_online_bestprice'
                            ]
                        ]
                    ];

                    foreach($mainKey as $gender_key => $gender_val)
                    {
                        foreach($serviceKey as $provider_key => $provider_val)
                        {
                            foreach($attributeKey as $attr_key => $attr_val)
                            {
                                $keyarr[$gender_val][$gender_val.'_'.$provider_val][$attr_key] = $priceKey[$gender_val][$gender_val.'_'.$provider_val];
                            }
                        }
                    }

                    $data_for_save = $trainer_data_for_save = [];
                    foreach($keyarr as $key => $key_provider_vals){
                        if($request->$key){
                            foreach($key_provider_vals as $service_key_key=>$service_key_row){
                                if($request->$service_key_key)
                                {
                                    $trainer_entity = SpServiceTrainer::where('sp_service_id',$row->id)->delete();
                                    $trainer_key = $service_key_key.'_trainer';
                                    if($request->$trainer_key)
                                    {
                                        $trainer_filed_key = strtoupper(str_replace('service','trainer',$service_key_key));
                                        foreach($request->$trainer_key as $trainer_id)
                                        {
                                            $trainer_entity = new SpServiceTrainer();
                                            $trainer_entity->sp_service_id = $row->id;
                                            $trainer_entity->service_set = 'individual';
                                            $trainer_entity->field_key = str_replace('IS','',$trainer_filed_key);
                                            $trainer_entity->sp_trainer_id = $trainer_id;
                                            $trainer_data_for_save[] = $trainer_entity;
                                        }
                                    }
                                            
                                    $entity = SpServicePrice::where('sp_service_id',$row->id)->delete();
                                    foreach($service_key_row as $type_key_key => $type_key_row)
                                    {
                                        $attr_id = $attributeKey[$type_key_key];
                                        $attr_key = $service_key_key.'_attribute_id';
                                        if(in_array($attr_id,$request->$attr_key))
                                        {
                                            foreach($type_key_row as $key_key => $key_row)
                                            {
                                                $desc_key = str_replace('service','description',$service_key_key);
                                                $timeHour = $service_key_key.'TimeHour';
                                                $timeMin = $service_key_key.'TimeMin';
                                                $time_key = $request[$type_key_key][$timeHour].':'.$request[$type_key_key][$timeMin].':'.'00';
 
                                                $entity = new SpServicePrice();
                                                $entity->sp_service_id = $row->id;
                                                $entity->service_attr_id = $attr_id;
                                                $entity->field_key = $key_key;
                                                $entity->price = $request[$type_key_key][$key_row];
                                                $entity->time = $time_key;
                                                $entity->description = $request[$type_key_key][$desc_key];
                                                $data_for_save[] = $entity;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $row->getRelatedTrainer()->saveMany($trainer_data_for_save);
                    $row->getRelatedPrices()->saveMany($data_for_save);
                    Session::flash('success', __('messages.Service Added successfully.'));
                    return redirect()->route('new-services.index');
                }
            }
       }
       catch(\Exception $e){
         return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
       }
    }

    public function storeMultiple(Request $request)
    {
       try
       {
            $data = $request->all();
            $validator = Validator::make($data, 
            [
                'sub_category' => 'required',
                'service_id' => 'required',
            ]);
            if ($validator->fails()) 
            {
            
                $error = $this->validationHandle($validator->messages());
                return response()->json(['status' => false, 'message' => $error]);
            }     
            else
            {
                $user_data = spLoginData();//helper function
                $user_id = $user_data['id'];
                $cat_id = $user_data['category_id'];

                $serviceRow = Service::select('id')->where('name','Fitness')->first();
                $service_id = $serviceRow->id;
                $filter_key = (count($request->service_id) > 1 || (isset($request->sub_service_id) && count($request->sub_service_id)) > 1) ? 'multiple' : 'single';

                $row = SpService::where('service_id',$service_id)->where('user_id',$user_id)->where('is_deleted','1')->first();
                $attr_arr = $this->getCategoryServiceAttribute('url','id');
                $attributeKey = $attr_arr['attribute_arr'];
            
                if(!$row){
                    $row = new SpService();
                }

                $row['service_id'] = $service_id;
                $row['description'] = $request->description;
                $row['filter_key'] = $filter_key;
                $row['service_type_id'] = $request->service_type_id;
                $row['service_title_id'] = $request->service_title_id;
                $row['user_id'] = $user_id;
                $row['is_deleted'] = '0';

                $row->save();
                if($row->save())
                {
                    if(!empty($request->sub_service_id))
                    {
                        $service_idArr = $request->sub_service_id;
                    }
                    else
                    {
                        $service_idArr = $request->service_id;
                    }

                    foreach($service_idArr as $service)
                    {
                        $grouprow = SpGroupService::where('service_id',$service)->where('sp_service_id',$row->id)->where('is_deleted','1')->first();
                        if(!$grouprow){
                            $grouprow = new SpGroupService();
                        }

                        $grouprow->service_id = $service;
                        $grouprow->sp_service_id = $row->id;
                        $grouprow->save();
                    }

                    $keyarr = array();
                    $mainKey = array('isMen','isWomen','isKids');
                    $serviceKey = array('store'=>'store_service','home'=>'home_service','online'=>'online_service');
                    $serviceSet = array('group','individual');
                    $priceKey = [
                        'isMen' => [
                            'goal'=>[
                                'MEN_LEVEL' => 'men_level',
                                'MEN_CALORIES' => 'men_calories',
                                'MEN_INTENSITY' => 'men_intensity',
                                'MEN_STORE_DESCRIPTION'=>'men_store_description',
                                'MEN_HOME_DESCRIPTION'=>'men_home_description',
                                'MEN_ONLINE_DESCRIPTION'=>'men_online_description',
                            ],
                            'isMen_store_service'=>[
                                'MEN_ORIGINALPRICE'=>'men_originalprice',
                                'MEN_DISCOUNT'=>'men_discount',
                                'MEN_BESTPRICE'=>'men_bestprice'
                            ],
                            'isMen_home_service'=>[
                                'MEN_HOME_ORIGINALPRICE'=>'men_home_originalprice',
                                'MEN_HOME_DISCOUNT'=>'men_home_discount',
                                'MEN_HOME_BESTPRICE'=>'men_home_bestprice'
                            ],
                            'isMen_online_service'=>[
                                'MEN_ONLINE_ORIGINALPRICE'=>'men_online_originalprice',
                                'MEN_ONLINE_DISCOUNT'=>'men_online_discount',
                                'MEN_ONLINE_BESTPRICE'=>'men_online_bestprice'
                            ],
                        ],
                        'isWomen' => [
                            'goal'=>[
                                'WOMEN_LEVEL' => 'women_level',
                                'WOMEN_CALORIES' => 'women_calories',
                                'WOMEN_INTENSITY' => 'women_intensity',
                                'WOMEN_STORE_DESCRIPTION'=>'women_store_description',
                                'WOMEN_HOME_DESCRIPTION'=>'women_home_description',
                                'WOMEN_ONLINE_DESCRIPTION'=>'women_online_description',
                            ],
                            'isWomen_store_service'=>[
                                'WOMEN_ORIGINALPRICE'=>'women_originalprice',
                                'WOMEN_DISCOUNT'=>'women_discount',
                                'WOMEN_BESTPRICE'=>'women_bestprice'
                            ],
                            'isWomen_home_service'=>[
                                'WOMEN_HOME_ORIGINALPRICE'=>'women_home_originalprice',
                                'WOMEN_HOME_DISCOUNT'=>'women_home_discount',
                                'WOMEN_HOME_BESTPRICE'=>'women_home_bestprice'
                            ],
                            'isWomen_online_service'=>[
                                'WOMEN_ONLINE_ORIGINALPRICE'=>'women_online_originalprice',
                                'WOMEN_ONLINE_DISCOUNT'=>'women_online_discount',
                                
                                'WOMEN_ONLINE_BESTPRICE'=>'women_online_bestprice'
                            ]
                        ],
                        'isKids' => [
                            'goal'=>[
                                'KIDS_LEVEL' => 'kids_level',
                                'KIDS_CALORIES' => 'kids_calories',
                                'KIDS_INTENSITY' => 'kids_intensity',
                                'KIDS_STORE_DESCRIPTION'=>'kids_store_description',
                                'KIDS_HOME_DESCRIPTION'=>'kids_home_description',
                                'KIDS_ONLINE_DESCRIPTION'=>'kids_online_description',
                            ],
                            'isKids_store_service'=>[
                                'KIDS_ORIGINALPRICE'=>'kids_originalprice',
                                'KIDS_DISCOUNT'=>'kids_discount',
                                'KIDS_BESTPRICE'=>'kids_bestprice'
                            ],
                            'isKids_home_service'=>[
                                'KIDS_HOME_ORIGINALPRICE'=>'kids_home_originalprice',
                                'KIDS_HOME_DISCOUNT'=>'kids_home_discount',
                                'KIDS_HOME_BESTPRICE'=>'kids_home_bestprice'
                            ],
                            'isKids_online_service'=>[
                                'KIDS_ONLINE_ORIGINALPRICE'=>'kids_online_originalprice',
                                'KIDS_ONLINE_DISCOUNT'=>'kids_online_discount',
                                'KIDS_ONLINE_BESTPRICE'=>'kids_online_bestprice'
                            ]
                        ]
                    ];

                    // create array for group combination keys
                    foreach($mainKey as $gender_key => $gender_val)
                    {
                        $keyarr[$gender_val][$gender_val.'_goal_service'] = $priceKey[$gender_val]['goal'];
                        foreach($serviceKey as $provider_key => $provider_val)
                        {
                            foreach($serviceSet as $division_key => $division_val)
                            {
                                foreach($attributeKey as $attr_key => $attr_val)
                                {
                                    $keyarr[$gender_val][$gender_val.'_'.$provider_val][$division_val][$attr_key] = $priceKey[$gender_val][$gender_val.'_'.$provider_val];
                                }
                            }
                        }
                    }

                    $data_for_save = $trainer_data_for_save = $goal_data_for_save = [];
                    foreach($keyarr as $key => $key_provider_vals){
                        if($request->$key){
                            $goal_entity = SpServiceGoal::where('sp_service_id',$row->id)->delete();
                            //$goal_data_for_save = [];
                            // Insert data for gender goals like intensity, calories, level
                            if($key.'_goal_service')
                            {
                                foreach($key_provider_vals[$key.'_goal_service'] as $goal_key_key => $goal_key_row)
                                {
                                    if($request->$goal_key_row)
                                    {
                                        $goal_entity =  new SpServiceGoal();
                                        $goal_entity->sp_service_id = $row->id;
                                        $goal_entity->field_key = $goal_key_key;
                                        $goal_entity->field_value = $request->$goal_key_row;
                                        $goal_data_for_save[] = $goal_entity;
                                    }
                                }
                            }
                                
                            foreach($key_provider_vals as $service_key_key=>$service_key_row)
                            {
                                if($request->$service_key_key)
                                {   
                                    foreach($service_key_row as $service_set_key_key => $service_set_key_val)
                                    {
                                        $set_key = $service_key_key.'_'.$service_set_key_key;
                                        if($request->$set_key)
                                        {
                                            // insert data for store wise trainer
                                            $trainer_entity = SpServiceTrainer::where('sp_service_id',$row->id)->delete();
                                            $trainer_key = $set_key.'_trainer';
                                            if($request->$trainer_key)
                                            {
                                                $trainer_filed_key = strtoupper(str_replace('service','trainer',$service_key_key));
                                                foreach($request->$trainer_key as $trainer_id)
                                                {
                                                    $trainer_entity = new SpServiceTrainer();
                                                    $trainer_entity->sp_service_id = $row->id;
                                                    $trainer_entity->service_set = $service_set_key_key;
                                                    $trainer_entity->field_key = str_replace('IS','',$trainer_filed_key);
                                                    $trainer_entity->sp_trainer_id = $trainer_id;
                                                    $trainer_data_for_save[] = $trainer_entity;
                                                }
                                            }
                                            
                                            // Insert pricde data for according to group, attr
                                            $entity = SpServicePrice::where('sp_service_id',$row->id)->delete();
                                            foreach($service_set_key_val as $type_key_key => $type_key_row)
                                            {
                                                $attr_id = $attributeKey[$type_key_key];
                                                $attr_key = $set_key.'_attribute_id';

                                                if(in_array($attr_id,$request->$attr_key))
                                                {
                                                    foreach($type_key_row as $key_key => $key_row)
                                                    {
                                                        $desc_key = str_replace('service','description',$service_key_key);
                                                        $timeHour = $service_key_key.'TimeHour';
                                                        $timeMin = $service_key_key.'TimeMin';
                                                        $time_key = $request[$service_set_key_key][$type_key_key][$timeHour].':'.$request[$type_key_key][$timeMin].':'.'00';
         
                                                        $entity = new SpServicePrice();
                                                        $entity->sp_service_id = $row->id;
                                                        $entity->service_set = $service_set_key_key;
                                                        $entity->service_attr_id = $attr_id;
                                                        $entity->field_key = $key_key;
                                                        $entity->price = $request[$service_set_key_key][$type_key_key][$key_row];
                                                        $entity->time = $time_key;
                                                        $entity->description = $request[$service_set_key_key][$type_key_key][$desc_key];
                                                        $data_for_save[] = $entity;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $row->getRelatedGoals()->saveMany($goal_data_for_save);
                    $row->getRelatedTrainer()->saveMany($trainer_data_for_save);
                    $row->getRelatedPrices()->saveMany($data_for_save);
                    Session::flash('success', __('messages.Service Added successfully.'));
                    return redirect()->route('new-services.index');
                }
            }
       }
       catch(\Exception $e){
         return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
       }
    }
 
    public function show($id)
    {
        $title = __('messages.View Services');
        $model = $this->model;

        $rows = SpService::where('id',$id)->first();
        $breadcum = [__('messages.Services') =>route($model.'.index'),$title =>''];
        $sr_name = 'name'.$this->getLang();
        $cat_name = 'category_name'.$this->getLang();
        $serviceName = $catName = '';
        if($rows->filter_key ==='single' || $rows->filter_key ==='multiple')
        {
            foreach($rows->getAssociatedGroupService as $groupservice)
            {
                $parentServiceRow = $groupservice->getParentServiceId;
                $parentRow = $groupservice->getAssociatedParentService($parentServiceRow->id);
                if($parentRow != '')
                {
                    $serviceName = $parentRow->$sr_name;
                    $subServiceName = $groupservice->getAssociatedService->$sr_name;
                }
                else
                {
                    $serviceName = $groupservice->getAssociatedService->$sr_name;
                    $subServiceName = '';
                }
                $nameArr[] = $serviceName;
                $subnameArr[] = $subServiceName;
                $catArr[] = $groupservice->getServiceSubcategory->getSubcat->$cat_name;
            }
            $serviceName = implode(', ',$nameArr);
            $catName = implode(', ',$catArr);
            $subServiceName = implode(', ',$subnameArr);;
        }
        else
        {
            $parentServiceRow = $rows->getParentServiceId;
            $parentRow = $rows->getAssociatedParentService($parentServiceRow->id);
            if($parentRow != '')
            {
                $serviceName = $parentRow->$sr_name;
                $subServiceName = $rows->getAssociatedService->$sr_name;
            }
            else
            {
                $serviceName = $rows->getAssociatedService->$sr_name;
                $subServiceName = '';
            }
            $catName = $rows->getServiceSubcategory->getSubcat->$cat_name;
        }

        return view('spadmin.service.new.view',compact('title','model','rows','breadcum', 'catName', 'serviceName'));
    }

    public function edit($id)
    {
        $title = __('messages.Edit');
        $user_data = spLoginData();
        $model = $this->model;   
        $breadcum = [__('messages.Services') =>route('new-'.$model.'.index'),$title =>''];
        $attr_arr = $this->getCategoryServiceAttribute('id','name');
        $attribute_arr = $attr_arr['attribute_arr'];
        $rows = SpService::where('id',$id)->first();
        $mainKey = array('isMen','isWomen','isKids');
        $cat_id = $user_data['category_id'];
        $staffRows = staff::where('is_deleted','0')->where('sp_id',$user_data['id'])->orderBy('staff_name','ASC')->get();

        $keyarr = [
            'MEN_ORIGINALPRICE'=>'isMen_store_service',
            'MEN_HOME_ORIGINALPRICE'=>'isMen_home_service',
            'MEN_ONLINE_ORIGINALPRICE'=>'isMen_online_service',
            'WOMEN_ORIGINALPRICE'=>'isWomen_store_service',
            'WOMEN_HOME_ORIGINALPRICE'=>'isWomen_home_service',
            'WOMEN_ONLINE_ORIGINALPRICE'=>'isWomen_online_service',
            'KIDS_ORIGINALPRICE'=>'isKids_store_service',
            'KIDS_HOME_ORIGINALPRICE'=>'isKids_home_service',
            'KIDS_ONLINE_ORIGINALPRICE'=>'isKids_online_service',
        ];

        $selected_gender_arr = $genderArr = $single_service_prices_arr = $service_current = $service_goals_arr = $multiple_service_prices_arr = $trainer_arr = $selected_trainer_arr = array();
        
        // get selected gender who have prices
        foreach($rows->getRelatedPrices as $key => $prices)
        {
            if(array_key_exists($prices['field_key'],$keyarr)){
                $genderArr[] = 'is'.ucfirst(strtolower(strstr($prices['field_key'], '_', true)));
                $selected_gender_arr[] = $keyarr[$prices['field_key']];
                $selected_gender_arr[] = $keyarr[$prices['field_key']].'_'.$prices['service_set'];
            }
        }
        $selected_gender_arr = array_unique(array_merge($selected_gender_arr,$genderArr));
       
        // get price data for single services
        foreach($rows->getRelatedPrices as $key => $prices){

            if(array_key_exists($prices['field_key'],$keyarr)){
                $service_group_key = $keyarr[$prices['field_key']];
            }
            if(array_key_exists($prices['service_attr_id'],$attribute_arr)){
                $single_service_prices_arr[$service_group_key][$prices['service_attr_id']]['time'] = $prices['time'];
                $single_service_prices_arr[$service_group_key][$prices['service_attr_id']]['description'] = $prices['description'];
                $single_service_prices_arr[$service_group_key][$prices['service_attr_id']][strtolower($prices['field_key'])] = $prices['price'];  
            }
        }

        // get service goals data
        foreach($rows->getRelatedGoals as $goal_key => $goal_value)
        {
            $service_goals_arr[strtolower($goal_value['field_key'])] = $goal_value['field_value'];
        }
        
        // get associated trainers according to mode of delivery
        foreach($rows->getRelatedTrainer as $trainer_key => $trainer_val)
        {
            //women_store_service_group_trainer
            $key = str_replace('trainer',$trainer_val['service_set'].'_trainer',strtolower($trainer_val['field_key']));
            $selected_trainer_arr[$key][] = $trainer_val['sp_trainer_id'];
        }
        
        // get price data for multiple services services
        foreach($rows->getRelatedPrices as $key => $prices){

            if(array_key_exists($prices['field_key'],$keyarr)){
                $service_group_key = $keyarr[$prices['field_key']];
            }
            if(array_key_exists($prices['service_attr_id'],$attribute_arr)){
                $multiple_service_prices_arr[$service_group_key][$prices['service_set']][$prices['service_attr_id']]['time'] = $prices['time'];
                $multiple_service_prices_arr[$service_group_key][$prices['service_set']][$prices['service_attr_id']]['description'] = $prices['description'];
                $multiple_service_prices_arr[$service_group_key][$prices['service_set']][$prices['service_attr_id']][strtolower($prices['field_key'])] = $prices['price'];  
            }
        }

        // get associated offers
        foreach($rows->getAssociatedOffers as $key_price => $offer_row){
        
            $service_current[] = [
                'id' => $offer_row['sp_offer_id'],
                'gender' => $offer_row ['gender'],
            ];
        }

        $sr_name = 'name'.$this->getLang();
        $cat_name = 'category_name'.$this->getLang();
        $subcat_arr = $this->getSubcategoryName();
        $fitnesstype_arr = FitnessType::orderby('name', 'asc')->pluck('name', 'id')->toArray();
        $delivery_mode = explode(',',$user_data['delivery_mode']);

        $nameArr = $subnameArr = $catArr = $subcat_id = array();
        if($rows->filter_key ==='single' || $rows->filter_key ==='multiple')
        {
            foreach($rows->getAssociatedGroupService as $groupservice)
            {
                $parentServiceRow = $groupservice->getParentServiceId;
                $parentRow = $groupservice->getAssociatedParentService($parentServiceRow->id);
                if($parentRow != '')
                {
                    $serviceName = $parentRow->$sr_name;
                    $subServiceName = $groupservice->getAssociatedService->$sr_name;
                }
                else
                {
                    $serviceName = $groupservice->getAssociatedService->$sr_name;
                    $subServiceName = '';
                }
                $nameArr[] = $serviceName;
                $subnameArr[] = $subServiceName;
                $catArr[] = $groupservice->getServiceSubcategory->getSubcat->$cat_name;
                $subcat_id[] = $groupservice->getServiceSubcategory->getSubcat->id;
            }
            $serviceName = implode(', ',$nameArr);
            $catName = implode(', ',$catArr);
            $subServiceName = implode(', ',$subnameArr);
        }
        else
        {
            $parentServiceRow = $rows->getParentServiceId;
            $parentRow = $rows->getAssociatedParentService($parentServiceRow->id);
            if($parentRow != '')
            {
                $serviceName = $parentRow->$sr_name;
                $subServiceName = $rows->getAssociatedService->$sr_name;
            }
            else
            {
                $serviceName = $rows->getAssociatedService->$sr_name;
                $subServiceName = '';
            }
            $catName = $rows->getServiceSubcategory->getSubcat->$cat_name;
            $subcat_id = array($rows->getServiceSubcategory->getSubcat->id);
        }

        foreach($staffRows as $data){
            $speciality = explode(',',$data['speciality']);
            if (array_intersect($subcat_id, $speciality) || in_array('0',$speciality)) {
                $trainer_arr[$data['id']] = $data['staff_name'];
            }
        }
        $trainer_arr = array_unique($trainer_arr);
        
        if($cat_id == 3)
        {
            return view('spadmin.service.new.editmultiple',compact('title','model','breadcum','rows','selected_gender_arr','multiple_service_prices_arr','sr_name','cat_name','service_current','subcat_arr','fitnesstype_arr','delivery_mode','attribute_arr','catName','serviceName','subServiceName','service_goals_arr','trainer_arr','selected_trainer_arr'));
        }
        else
        {
            return view('spadmin.service.new.edit',compact('title','model','breadcum','rows','selected_gender_arr','single_service_prices_arr','sr_name','cat_name','service_current','subcat_arr','delivery_mode','attribute_arr','catName','serviceName','subServiceName','trainer_arr','selected_trainer_arr'));
        }
    }

    public function update(Request $request, $id)
    {
       try
       { 
        $row = SpService::where('id',$id)->first();
        $offers = $data_for_save = $trainer_data_for_save = array();
        foreach($row->getAssociatedOffers as $key_price => $offer_row){
    
            $offers[] = $offer_row['sp_offer_id'];
        }

        $check_offer = [
            'isMen'=> ['men','shop'],
            'isWomen'=> ['women','shop'],
            'isKids'=> ['kids','shop'],
            'isMen_home_service'=> ['men','home'],
            'isWomen_home_service'=> ['women','home'],
            'isKids_home_service'=> ['kids','home'],
        ];
        $sim_key_arr = ['isMen','isWomen','isKids','isMen_home_service','isWomen_home_service','isKids_home_service'];

        SpServiceTrainer::where('sp_service_id',$id)->delete();
        SpServicePrice::where('sp_service_id',$id)->delete();
        $get_offer_keys = $keyarr = array();
        $mainKey = array('isMen','isWomen','isKids');
        $serviceKey = array('store_service','home_service','online_service');
        $attr_arr = $this->getCategoryServiceAttribute('url','id');
        $attributeKey = $attr_arr['attribute_arr'];
        
        $priceKey = [
            'isMen' => [
                'isMen_store_service'=>[
                    'MEN_ORIGINALPRICE'=>'men_originalprice',
                    'MEN_DISCOUNT'=>'men_discount',
                    'MEN_BESTPRICE'=>'men_bestprice'
                ],
                'isMen_home_service'=>[
                    'MEN_HOME_ORIGINALPRICE'=>'men_home_originalprice',
                    'MEN_HOME_DISCOUNT'=>'men_home_discount',
                    'MEN_HOME_BESTPRICE'=>'men_home_bestprice'
                ],
                'isMen_online_service'=>[
                    'MEN_ONLINE_ORIGINALPRICE'=>'men_online_originalprice',
                    'MEN_ONLINE_DISCOUNT'=>'men_online_discount',
                    'MEN_ONLINE_BESTPRICE'=>'men_online_bestprice'
                ],
            ],
            'isWomen' => [
                'isWomen_store_service'=>[
                    'WOMEN_ORIGINALPRICE'=>'women_originalprice',
                    'WOMEN_DISCOUNT'=>'women_discount',
                    'WOMEN_BESTPRICE'=>'women_bestprice'
                ],
                'isWomen_home_service'=>[
                    'WOMEN_HOME_ORIGINALPRICE'=>'women_home_originalprice',
                    'WOMEN_HOME_DISCOUNT'=>'women_home_discount',
                    
                    'WOMEN_HOME_BESTPRICE'=>'women_home_bestprice'
                ],
                'isWomen_online_service'=>[
                    'WOMEN_ONLINE_ORIGINALPRICE'=>'women_online_originalprice',
                    'WOMEN_ONLINE_DISCOUNT'=>'women_online_discount',
                    
                    'WOMEN_ONLINE_BESTPRICE'=>'women_online_bestprice'
                ]
            ],
            'isKids' => [
                'isKids_store_service'=>[
                    'KIDS_ORIGINALPRICE'=>'kids_originalprice',
                    'KIDS_DISCOUNT'=>'kids_discount',
                    'KIDS_BESTPRICE'=>'kids_bestprice'
                ],
                'isKids_home_service'=>[
                    'KIDS_HOME_ORIGINALPRICE'=>'kids_home_originalprice',
                    'KIDS_HOME_DISCOUNT'=>'kids_home_discount',
                    'KIDS_HOME_BESTPRICE'=>'kids_home_bestprice'
                ],
                'isKids_online_service'=>[
                    'KIDS_ONLINE_ORIGINALPRICE'=>'kids_online_originalprice',
                    'KIDS_ONLINE_DISCOUNT'=>'kids_online_discount',
                    'KIDS_ONLINE_BESTPRICE'=>'kids_online_bestprice'
                ]
            ]
        ];

        foreach($mainKey as $gender_key => $gender_val)
        {
            foreach($serviceKey as $provider_key => $provider_val)
            { 
                foreach($attributeKey as $type_key => $type_val)
                {
                    $keyarr[$gender_val][$gender_val.'_'.$provider_val][$type_key] = $priceKey[$gender_val][$gender_val.'_'.$provider_val];
                }
            }
        }

        foreach($keyarr as $key => $key_provider_vals){
            if($request->$key){
                $get_offer_keys[] = $key;
                foreach($key_provider_vals as $service_key_key=>$service_key_row){
                    if($request->$service_key_key)
                    {
                        // insert data for store wise trainer
                        $trainer_key = $service_key_key.'_trainer';
                        if($request->$trainer_key)
                        {
                            $trainer_filed_key = strtoupper(str_replace('service','trainer',$service_key_key));
                            foreach($request->$trainer_key as $trainer_id)
                            {
                                $trainer_entity = new SpServiceTrainer();
                                $trainer_entity->sp_service_id = $row->id;
                                $trainer_entity->service_set = 'individual';
                                $trainer_entity->field_key = str_replace('IS','',$trainer_filed_key);
                                $trainer_entity->sp_trainer_id = $trainer_id;
                                $trainer_data_for_save[] = $trainer_entity;
                            }
                        }

                        foreach($service_key_row as $type_key_key => $type_key_row)
                        {
                            $attr_id = $attributeKey[$type_key_key];
                            $attr_key = $service_key_key.'_attribute_id';
                            if(in_array($attr_id,$request->$attr_key))
                            {
                                foreach($type_key_row as $key_key => $key_row)
                                {
                                    $desc_key = str_replace('service','description',$service_key_key);
                                    $timeHour = $service_key_key.'TimeHour';
                                    $timeMin = $service_key_key.'TimeMin';
                                    $time_key = $request[$type_key_key][$timeHour].':'.$request[$type_key_key][$timeMin].':'.'00';

                                    $entity = new SpServicePrice();
                                    $entity->sp_service_id = $row->id;
                                    $entity->service_attr_id = $attr_id;
                                    $entity->field_key = $key_key;
                                    $entity->price = $request[$type_key_key][$key_row];
                                    $entity->time = $time_key;
                                    $entity->description = $request[$type_key_key][$desc_key];
                                    $data_for_save[] = $entity;
                                }
                            }
                        }
                    }
                }
            }
        }

        $row->getRelatedTrainer()->saveMany($trainer_data_for_save);
        $row->getRelatedPrices()->saveMany($data_for_save);
        //check offer
        $k_arr = array_diff($sim_key_arr,$get_offer_keys);
        
        foreach($k_arr as $ky){
            if(count($offers)>0){
                foreach($offers as $off){
                    $gender = $check_offer[$ky][0];
                    $service_type = $check_offer[$ky][1];
                    $off_row = SpOffer::where('id',$off)->where('gender',$gender)->where('service_type',$service_type)->first();
                    if($off_row){
                        $off_row->is_deleted = '1';
                        $off_row->save();
                    }

                }
                
            }
        }
                        
        Session::flash('success', __('messages.Service Updated successfully.'));
        return redirect()->route('new-services.index');
        
        }
       catch(\Exception $e){
         return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
       }
    }

    public function updateMultiple(Request $request, $id)
    {
        //dd($request);
       try
       { 
        $row = SpService::where('id',$id)->first();
        $offers = $data_for_save = $trainer_data_for_save = $goal_data_for_save = array();
        foreach($row->getAssociatedOffers as $key_price => $offer_row){
    
            $offers[] = $offer_row['sp_offer_id'];
        }

        $check_offer = [
            'isMen'=> ['men','shop'],
            'isWomen'=> ['women','shop'],
            'isKids'=> ['kids','shop'],
            'isMen_home_service'=> ['men','home'],
            'isWomen_home_service'=> ['women','home'],
            'isKids_home_service'=> ['kids','home'],
        ];
        $sim_key_arr = ['isMen','isWomen','isKids','isMen_home_service','isWomen_home_service','isKids_home_service'];

        SpServiceGoal::where('sp_service_id',$id)->delete();
        SpServiceTrainer::where('sp_service_id',$id)->delete();
        SpServicePrice::where('sp_service_id',$id)->delete();
        $get_offer_keys = $keyarr = array();
        $mainKey = array('isMen','isWomen','isKids');
        $serviceKey = array('store'=>'store_service','home'=>'home_service','online'=>'online_service');
        $serviceSet = array('group','individual');
        $attr_arr = $this->getCategoryServiceAttribute('url','id');
        $attributeKey = $attr_arr['attribute_arr'];
        
        $priceKey = [
            'isMen' => [
                'goal'=>[
                    'MEN_LEVEL' => 'men_level',
                    'MEN_CALORIES' => 'men_calories',
                    'MEN_INTENSITY' => 'men_intensity',
                    'MEN_STORE_DESCRIPTION'=>'men_store_description',
                    'MEN_HOME_DESCRIPTION'=>'men_home_description',
                    'MEN_ONLINE_DESCRIPTION'=>'men_online_description',
                ],
                'isMen_store_service'=>[
                    'MEN_ORIGINALPRICE'=>'men_originalprice',
                    'MEN_DISCOUNT'=>'men_discount',
                    'MEN_BESTPRICE'=>'men_bestprice'
                ],
                'isMen_home_service'=>[
                    'MEN_HOME_ORIGINALPRICE'=>'men_home_originalprice',
                    'MEN_HOME_DISCOUNT'=>'men_home_discount',
                    'MEN_HOME_BESTPRICE'=>'men_home_bestprice'
                ],
                'isMen_online_service'=>[
                    'MEN_ONLINE_ORIGINALPRICE'=>'men_online_originalprice',
                    'MEN_ONLINE_DISCOUNT'=>'men_online_discount',
                    'MEN_ONLINE_BESTPRICE'=>'men_online_bestprice'
                ],
            ],
            'isWomen' => [
                'goal'=>[
                    'WOMEN_LEVEL' => 'women_level',
                    'WOMEN_CALORIES' => 'women_calories',
                    'WOMEN_INTENSITY' => 'women_intensity',
                    'WOMEN_STORE_DESCRIPTION'=>'women_store_description',
                    'WOMEN_HOME_DESCRIPTION'=>'women_home_description',
                    'WOMEN_ONLINE_DESCRIPTION'=>'women_online_description',
                ],
                'isWomen_store_service'=>[
                    'WOMEN_ORIGINALPRICE'=>'women_originalprice',
                    'WOMEN_DISCOUNT'=>'women_discount',
                    'WOMEN_BESTPRICE'=>'women_bestprice'
                ],
                'isWomen_home_service'=>[
                    'WOMEN_HOME_ORIGINALPRICE'=>'women_home_originalprice',
                    'WOMEN_HOME_DISCOUNT'=>'women_home_discount',
                    'WOMEN_HOME_BESTPRICE'=>'women_home_bestprice'
                ],
                'isWomen_online_service'=>[
                    'WOMEN_ONLINE_ORIGINALPRICE'=>'women_online_originalprice',
                    'WOMEN_ONLINE_DISCOUNT'=>'women_online_discount',
                    
                    'WOMEN_ONLINE_BESTPRICE'=>'women_online_bestprice'
                ]
            ],
            'isKids' => [
                'goal'=>[
                    'KIDS_LEVEL' => 'kids_level',
                    'KIDS_CALORIES' => 'kids_calories',
                    'KIDS_INTENSITY' => 'kids_intensity',
                    'KIDS_STORE_DESCRIPTION'=>'kids_store_description',
                    'KIDS_HOME_DESCRIPTION'=>'kids_home_description',
                    'KIDS_ONLINE_DESCRIPTION'=>'kids_online_description',
                ],
                'isKids_store_service'=>[
                    'KIDS_ORIGINALPRICE'=>'kids_originalprice',
                    'KIDS_DISCOUNT'=>'kids_discount',
                    'KIDS_BESTPRICE'=>'kids_bestprice'
                ],
                'isKids_home_service'=>[
                    'KIDS_HOME_ORIGINALPRICE'=>'kids_home_originalprice',
                    'KIDS_HOME_DISCOUNT'=>'kids_home_discount',
                    'KIDS_HOME_BESTPRICE'=>'kids_home_bestprice'
                ],
                'isKids_online_service'=>[
                    'KIDS_ONLINE_ORIGINALPRICE'=>'kids_online_originalprice',
                    'KIDS_ONLINE_DISCOUNT'=>'kids_online_discount',
                    'KIDS_ONLINE_BESTPRICE'=>'kids_online_bestprice'
                ]
            ]
        ];

        // create array for group combination keys
        foreach($mainKey as $gender_key => $gender_val)
        {
            $keyarr[$gender_val][$gender_val.'_goal_service'] = $priceKey[$gender_val]['goal'];
            foreach($serviceKey as $provider_key => $provider_val)
            {
                foreach($serviceSet as $division_key => $division_val)
                {
                    foreach($attributeKey as $attr_key => $attr_val)
                    {
                        $keyarr[$gender_val][$gender_val.'_'.$provider_val][$division_val][$attr_key] = $priceKey[$gender_val][$gender_val.'_'.$provider_val];
                    }
                }
            }
        }

        foreach($keyarr as $key => $key_provider_vals){
            if($request->$key){
                $get_offer_keys[] = $key;
                // Insert data for gender goals like intensity, calories, level
                //$goal_data_for_save = [];
                if($key.'_goal_service')
                {
                    foreach($key_provider_vals[$key.'_goal_service'] as $goal_key_key => $goal_key_row)
                    {
                        if($request->$goal_key_row)
                        {
                            $goal_entity =  new SpServiceGoal();
                            $goal_entity->sp_service_id = $row->id;
                            $goal_entity->field_key = $goal_key_key;
                            $goal_entity->field_value = $request->$goal_key_row;
                            $goal_data_for_save[] = $goal_entity;
                        }
                    }
                }
                foreach($key_provider_vals as $service_key_key=>$service_key_row)
                {                    
                    if($request->$service_key_key)
                    {   
                        foreach($service_key_row as $service_set_key_key => $service_set_key_val)
                        {
                            $set_key = $service_key_key.'_'.$service_set_key_key;
                            if($request->$set_key)
                            {
                                // insert data for store wise trainer
                                $trainer_key = $set_key.'_trainer';
                                if($request->$trainer_key)
                                {
                                    $trainer_filed_key = strtoupper(str_replace('service','trainer',$service_key_key));
                                    foreach($request->$trainer_key as $trainer_id)
                                    {
                                        $trainer_entity = new SpServiceTrainer();
                                        $trainer_entity->sp_service_id = $row->id;
                                        $trainer_entity->service_set = $service_set_key_key;
                                        $trainer_entity->field_key = str_replace('IS','',$trainer_filed_key);
                                        $trainer_entity->sp_trainer_id = $trainer_id;
                                        $trainer_data_for_save[] = $trainer_entity;
                                    }
                                }
                                
                                // Insert pricde data for according to group, attr
                                foreach($service_set_key_val as $type_key_key => $type_key_row)
                                {
                                    $attr_id = $attributeKey[$type_key_key];
                                    $attr_key = $set_key.'_attribute_id';

                                    if(in_array($attr_id,$request->$attr_key))
                                    {
                                        foreach($type_key_row as $key_key => $key_row)
                                        {
                                            $desc_key = str_replace('service','description',$service_key_key);
                                            $timeHour = $service_key_key.'TimeHour';
                                            $timeMin = $service_key_key.'TimeMin';
                                            $time_key = $request[$service_set_key_key][$type_key_key][$timeHour].':'.$request[$type_key_key][$timeMin].':'.'00';

                                            $entity = new SpServicePrice();
                                            $entity->sp_service_id = $row->id;
                                            $entity->service_set = $service_set_key_key;
                                            $entity->service_attr_id = $attr_id;
                                            $entity->field_key = $key_key;
                                            $entity->price = $request[$service_set_key_key][$type_key_key][$key_row];
                                            $entity->time = $time_key;
                                            $entity->description = $request[$service_set_key_key][$type_key_key][$desc_key];
                                            $data_for_save[] = $entity;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $row->getRelatedGoals()->saveMany($goal_data_for_save);
        $row->getRelatedTrainer()->saveMany($trainer_data_for_save);
        $row->getRelatedPrices()->saveMany($data_for_save);

        //check offer
        $k_arr = array_diff($sim_key_arr,$get_offer_keys);
        
        foreach($k_arr as $ky){
            if(count($offers)>0){
                foreach($offers as $off){
                    $gender = $check_offer[$ky][0];
                    $service_type = $check_offer[$ky][1];
                    $off_row = SpOffer::where('id',$off)->where('gender',$gender)->where('service_type',$service_type)->first();
                    if($off_row){
                        $off_row->is_deleted = '1';
                        $off_row->save();
                    }

                }
                
            }
        }
                        
        Session::flash('success', __('messages.Service Updated successfully.'));
        return redirect()->route('new-services.index');
        
        }
       catch(\Exception $e){
         return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
       }
    }

    public function destroy($id)
    {
        $row = SpService::where('id', $id)->first();

        if ($row) {

            $offer_arr = $service_arr =  array();
            foreach($row->getAssociatedOffers as $offer_row){   
                $offer_arr[] = $offer_row['sp_offer_id'];
            }
            $row->is_deleted = '1';
            $row->save();

            //SpGroupService::where('sp_service_id',$row->id)->update(array('is_deleted'=>'1'));

            SpOffer::whereIn('id',$offer_arr)->update(array('is_deleted'=>'1'));
           
            Session::flash('success', __('messages.Service deleted successfully'));
             return redirect()->back();
        } else {
            Session::flash('warning', __('messages.Invalid request.'));
            return redirect()->back();
        }
    }

    public function ajaxGetSubcatSubServices(Request $request)
    {
        $user_data = spLoginData();//helper function
        $service_id = array();

        if($request->service_id != null){
            if(is_array($request->service_id))
            {
               $service_id = $request->service_id;
            }
            else
            {
                $service_id = array($request->service_id);
            }
        }

        $row = Service::where('id',$request->service_id)->first();
        $serviceDesc = ($row) ? $row->description : '';
        $rows = Service::whereIn('parent_service_id',$service_id)->orderBy('name','ASC')->get();
        $sub_service_arr = array();
        foreach($rows as $data){
            $check = SpService::where('user_id',$user_data['id'])->where('service_id',$data['id'])->where('is_deleted','!=','1')->count();
            $sr_name = 'name'.$this->getLang();
            if($check == 0){
                 $sub_service_arr[$data['id']] = $data[$sr_name]; 
            }
        }
        if($user_data['category_id'] == 3)
            return view('spadmin.service.new.ajaxGetSubcatSubServicesMultiple',compact('sub_service_arr','serviceDesc'));
        else
            return view('spadmin.service.new.ajaxGetSubcatSubServices',compact('sub_service_arr','serviceDesc'));
    }

    public function ajaxGetSubcatSubServicesInfo(Request $request)
    {
        $user_data = spLoginData();//helper function
        $service_id = array();

        if($request->service_id != null){
            if(is_array($request->service_id))
            {
               $service_id = $request->service_id;
            }
            else
            {
                $service_id = array($request->service_id);
            }
        }

        $row = Service::where('id',$request->service_id)->first();
        $subServiceDesc = ($row) ? $row->description : '';
        
        return view('spadmin.service.new.ajaxGetSubcatSubServicesInfo',compact('subServiceDesc'));
    }

    public function getCategoryServiceAttribute($key,$value)
    {
        $attribute_arr = $attrdesc_arr = array();
        $user_data = spLoginData();//helper function
        $cat_id = $user_data['category_id'];
        $attr_arr = ServiceAttribute::where('cat_id',$cat_id)->orderby('name', 'asc')->get();
        foreach($attr_arr as $attr)
        {
            $attribute_arr[$attr[$key]] = $attr[$value];
            $attrdesc_arr[$attr[$key]] = $attr['description'];
        }
        return array('attribute_arr'=>$attribute_arr, 'attr_desc_arr' => $attrdesc_arr);
    }
}
