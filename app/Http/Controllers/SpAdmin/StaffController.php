<?php

namespace App\Http\Controllers\SpAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;   

use App\Model\Staff;
use App\Model\SpUser;
use App\Model\StaffLeave;
use App\Model\ShopTiming;
use App\Model\StaffImagesVideos;
use App\Model\Category;
use App\Model\Booking;
use App\Model\Country;
use datatables;
use App;
use Session;
use DB;

class StaffController extends Controller
{

    protected $title;
    protected $model;
    protected $module;

    public function __construct()
    {
        $this->title = 'Staff';
        $this->model = 'staffs';
        $this->module = 'staff';
    } 
   
    public function index()
    {
        $title = __('messages.View Staff');
        $module = $this->module;
        $model = $this->model;
        $breadcum = [$title=>route($model.'.index')];
        return view('spadmin.staff.index',compact('title','model','module','breadcum'));
    }

    public function getData(Request $request)
    {
        $user_data = spLoginData();//helper function
           
        $columns = ['staff_name','nationality','experience','speciality','is_provide_home_service'];

        $totalData = Staff::where('sp_id',$user_data['id'])->where('is_deleted','!=','1')->count();
               
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

        $row = Staff::where('sp_id',$user_data['id'])->where('is_deleted','!=','1');
        
       
        // $totalFiltered = User::count();

        if (!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = $row->where(function($query) use ($search) {
                $query->where('staff_name', 'LIKE', "%{$search}%")
                        ->orWhere('nationality', 'LIKE', "%{$search}%")
                        ->orWhere('experience', 'LIKE', "%{$search}%");
                        
            });
        }

        $categories  = Category::where('category_name','!=','bridal')->where('parent_id','!=','0')->orderby('category_name', 'asc')->pluck('category_name', 'id')->toArray();
        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('id', 'desc')
                ->get();
    
        $data = array();
        if (!empty($row)) {
            foreach ($row as $key => $rows) {
                $speciality_arr = array();
                $arr = explode(",",$rows->speciality);
                foreach($arr as $cat_name_id)
                {
                    if($cat_name_id === '0')
                    {
                        $cat_name_id = 'All Rounders';
                    }
                    if(array_key_exists($cat_name_id,$categories))
                    {
                        $speciality_arr[] = $categories[$cat_name_id];
                    }
                    else
                    {
                        array_push($speciality_arr,$cat_name_id);
                    }
                }

                $nestedData['staff_name'] = $rows->staff_name;
                $nestedData['nationality'] = ($rows->getNationalityInfo['nationality']) ? $rows->getNationalityInfo['nationality'] : $rows->nationality;
                $nestedData['experience'] = $rows->experience;
                $nestedData['speciality'] = implode(',',$speciality_arr);
                if($rows->is_provide_home_service == 1){
                    $nestedData['is_provide_home_service'] = __('messages.Yes');
                }else{
                    $nestedData['is_provide_home_service'] = __('messages.No');
                }
                
  
                $nestedData['action'] =  getButtons([
                                ['key'=>'edit','link'=>route('staffs.edit',[$rows->id])],
                                ['key'=>'leaves','link'=>route('staffs.leaves',[$rows->id])],
                                ['key'=>'delete','link'=>route('staffs.destroy',$rows->id)],
                            ]);
                
                $data[] = $nestedData;
            }

        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }

   
    public function create()
    {
        $action = "create";
        $title = __('messages.Create Staff');
        $model = $this->model;
        $breadcum = [__('messages.Staff')=>route($model.'.index'),$title =>''];
        $categories  = Category::where('category_name','!=','bridal')->where('parent_id','!=','0')->orderby('category_name', 'asc')->pluck('category_name', 'id')->toArray();
        $nationality_arr = Country::whereNotNull('nationality')->pluck('nationality', 'id')->toArray();
        return view('spadmin.staff.create',compact('action','title','breadcum','model','module','categories','nationality_arr'));
    }

   
    public function store(Request $request)
    {
        try{
             if (Input::isMethod('post')) {
                $validation = array(
                    'staff_name'=>'required|max:50',
                    'experience'=>'required',
                    'speciality'=>'required',
                    'certificates'=>'required',
                    'staff_image'=>'mimes:jpeg,jpg,png,gif'
                );

                $desc = explode(' ',$request->description);
                $validator = Validator::make(Input::all(), $validation);

                if(count($desc)>500){
                     $validator -> errors() -> add('description', 'Description cannot be more than 500 words.');
                     return redirect()->back()
                    -> withErrors($validator)
                    -> withInput(Input::all());
                }

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                } 
                else {
                    $user_data = spLoginData();//helper function
                    $row = new Staff();
                    $row['sp_id'] = $user_data['id'];
                    $row['staff_name'] = $request->staff_name;
                    $row['nationality'] = $request->nationality;
                    $row['experience'] = $request->experience;
                    $row['speciality'] = implode(',',$request->speciality);
                    $row['certificates'] = $request->certificates;
                    $row['description'] = $request->description;
                    $row['description_ar'] = $request->description_ar;
                    $row['video_url'] = $request->video_url;

                    if(!$request->is_provide_home_service){
                        $row['is_provide_home_service'] = '0';
                    }else{
                        $row['is_provide_home_service'] = $request->is_provide_home_service;
                    }

                    if ($request->hasFile('staff_image') && $request->file('staff_image'))
                    {

                        $image = $request->image_cr;
                        list($type, $image) = explode(';', $image);
                        list(, $image)      = explode(',', $image);
                        $image = base64_decode($image);
                        $image_name= time().'.png';
                        $path = public_path('/sp_uploads/staff/'.$image_name);
                        file_put_contents($path, $image);
                        $row->staff_image = $image_name;
                    }

                    $row->save();
                    if($row->save()){

                        //upload image
                        if($request->staff_images){

                            $count = count($request->staff_images);
                        
                            foreach($request->staff_images as $key => $val){
                                if ($request->staff_images[$key])
                                {
                                    $img = $request->staff_images[$key];
                                    $file = $img;
                                    $getMimeType = $file->getClientMimeType();
                                    $name = time().str_random(2).'.'.$file->getClientOriginalExtension();
                                    $destinationPath = public_path('/sp_uploads/staff/');
                                    $img = $file->move($destinationPath, $name);
                                    $rowImage = new StaffImagesVideos();
                                    $rowImage->staff_id = $row->id;
                                    $rowImage->staff_image = $name;
                                    $rowImage->content_type = substr($getMimeType, 0, 5);
                                    $rowImage->save();
                                }
                            }
                        } 

                        //upload video
                        if($request->staff_videos){

                            $count = count($request->staff_videos);
                        
                            foreach($request->staff_videos as $key => $val){
                                if ($request->staff_videos[$key])
                                {
                                    $img = $request->staff_videos[$key];
                                    $file = $img;
                                    $getMimeType = $file->getClientMimeType();
                                    $name = time().str_random(2).'.'.$file->getClientOriginalExtension();
                                    $destinationPath = public_path('/sp_uploads/staff/');
                                    $img = $file->move($destinationPath, $name);
                                    $rowImage = new StaffImagesVideos();
                                    $rowImage->staff_id = $row->id;
                                    $rowImage->staff_video = $name;
                                    $rowImage->content_type = substr($getMimeType, 0, 5);
                                    $rowImage->save();
                                }
                            }
                        }
                    }

                    Session::flash('success', __('messages.Staff member added successfully.'));
                    return redirect()->route('staffs.index');
                }
             } 
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

   
    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $rows = Staff::where('id',$id)->first();
        $title = __('messages.Edit Staff');
        $model = $this->model;
        $breadcum = [__('messages.Staff')=>route($model.'.index'),$title =>''];

        $staff_images = $staff_videos = array();
        $staff_images_videos = StaffImagesVideos::where('staff_id',$rows->id)->get();
       
        foreach($staff_images_videos as $files)
        {
            $staff_images[$files['id']] = $files['staff_image'];
            $staff_videos[$files['id']] = $files['staff_video'];
        }

        $categories  = Category::where('category_name','!=','bridal')->where('parent_id','!=','0')->orderby('category_name', 'asc')->pluck('category_name', 'id')->toArray();
        $nationality_arr = Country::whereNotNull('nationality')->pluck('nationality', 'id')->toArray();
        return view('spadmin.staff.edit',compact('rows','title','breadcum','staff_images','staff_videos','categories','nationality_arr'));
    }

   
    public function update(Request $request, $id)
    {
        try{
            $validation = array(
                'staff_name'=>'required|max:50',
                'experience'=>'required',
                'speciality'=>'required',
                'certificates'=>'required',
                'staff_image'=>'mimes:jpeg,jpg,png,gif'
            );

            $desc = explode(' ',$request->description);
            $validator = Validator::make(Input::all(), $validation);

            if(count($desc)>500){
                 $validator -> errors() -> add('description', 'Description cannot be more than 500 words.');
                 return redirect()->back()
                -> withErrors($validator)
                -> withInput(Input::all());
            }

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
            } 
            else {

                $user_data = spLoginData();//helper function
                $row = Staff::where('id',$id)->first();
                $row['sp_id'] = $user_data['id'];
                $row['staff_name'] = $request->staff_name;
                $row['nationality'] = $request->nationality;
                $row['experience'] = $request->experience;
                $row['speciality'] = implode(',',$request->speciality);
                $row['certificates'] = $request->certificates;
                $row['description'] = $request->description;
                $row['description_ar'] = $request->description_ar;
                $row['video_url'] = $request->video_url;

                if(!$request->is_provide_home_service){
                    $row['is_provide_home_service'] = '0';
                }else{
                    $row['is_provide_home_service'] = $request->is_provide_home_service;
                }
                

                if ($request->hasFile('staff_image') && $request->file('staff_image'))
                {
                    $image = $request->image_cr;
                    list($type, $image) = explode(';', $image);
                    list(, $image)      = explode(',', $image);
                    $image = base64_decode($image);
                    $image_name= time().'.png';
                    $path = public_path('/sp_uploads/staff/'.$image_name);
                    file_put_contents($path, $image);
                    $row->staff_image = $image_name;
                }

                $row->save();
                if($row->save()){
                    //upload image
                    if($request->staff_images){

                        $count = count($request->staff_images);
                    
                        foreach($request->staff_images as $key => $val){
                            if ($request->staff_images[$key])
                            {
                                $img = $request->staff_images[$key];
                                $file = $img;
                                $getMimeType = $file->getClientMimeType();
                                $name = time().str_random(2).'.'.$file->getClientOriginalExtension();
                                $destinationPath = public_path('/sp_uploads/staff/');
                                $img = $file->move($destinationPath, $name);
                                $rowImage = new StaffImagesVideos();
                                $rowImage->staff_id = $row->id;
                                $rowImage->staff_image = $name;
                                $rowImage->content_type = substr($getMimeType, 0, 5);
                                $rowImage->save();
                            }
                        }
                    } 

                    //upload video
                    if($request->staff_videos){

                        $count = count($request->staff_videos);
                    
                        foreach($request->staff_videos as $key => $val){
                            if ($request->staff_videos[$key])
                            {
                                $img = $request->staff_videos[$key];
                                $file = $img;
                                $getMimeType = $file->getClientMimeType();
                                $name = time().str_random(2).'.'.$file->getClientOriginalExtension();
                                $destinationPath = public_path('/sp_uploads/staff/');
                                $img = $file->move($destinationPath, $name);
                                $rowImage = new StaffImagesVideos();
                                $rowImage->staff_id = $row->id;
                                $rowImage->staff_video = $name;
                                $rowImage->content_type = substr($getMimeType, 0, 5);
                                $rowImage->save();
                            }
                        }
                    }
                }

                Session::flash('success', __('messages.Staff member updated successfully.'));
                return redirect()->route('staffs.index');
            }

        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

  
    public function destroy($id)
    {
        $row = Staff::where('id', $id)->first();
        if ($row) {
            // $row->delete();
            $row->is_deleted = '1';
             $row->save();

             Session::flash('success', __('messages.Staff deleted successfully.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('messages.Invalid request.'));
            return redirect()->back();
        }
    }

    public function deleteImageVideos(Request $request)
    {
        $id = $request->staff_id;
        $row  = StaffImagesVideos::whereId($id)->first();
        if($row){
            if($row->content_type == 'image')
                unlink('public/sp_uploads/staff/'.$row->staff_image);
            else
                unlink('public/sp_uploads/staff/'.$row->staff_video);

           $row->delete();
           return 'success';
        }
    }

    public function leaves($id)
    {
        $action = "leaves";
        $title = __('messages.Manage Leaves');
        $model = $this->model;
        $breadcum = [__('messages.Staff')=>route($model.'.index'),$title =>''];
        $staff = Staff::where('id',$id)->first()->toArray();;
        //echo '<pre>'; print_r($staff);die;
        return view('spadmin.staff.leaves',compact('action','title','breadcum','model','leaves','id','staff'));
    }


    public function getLeave(Request $request)
    { 
       // echo $_POST['staff_id'];die;
        $user_data = spLoginData();//helper function
           
        $columns = ['staff_name','from_date','to_date','from_time','to_time','created_at'];

        $totalData = StaffLeave::where('staff_id',$_POST['staff_id'])->count();
               
        $limit = $request->input('length');
        $start = $request->input('start');
        
        $order = $columns[$request->input('order.0.column')];

        $dir = $request->input('order.0.dir');

       // $row = StaffLeave::where('staff_id',$_POST['staff_id']);
        $row = StaffLeave::leftJoin('staff', function($join) {
                  $join->on('staff_leaves.staff_id', '=', 'staff.id');
                }) 
          ->select('staff_leaves.*', 'staff.staff_name')
            ->where('staff_leaves.staff_id',$_POST['staff_id']);
       
        // $totalFiltered = User::count();

        if (!empty($request->input('search.value'))) {
            $search = $request->input('search.value');
            $row = $row->where(function($query) use ($search) {
                $query->where('staff_name', 'LIKE', "%{$search}%")
                        ->orWhere('nationality', 'LIKE', "%{$search}%")
                        ->orWhere('experience', 'LIKE', "%{$search}%");
                        
            });
        }

        $data_query_count = $row;
        $totalFiltered = $data_query_count->count();
        $row = $row->offset($start)
                ->limit($limit)
                ->orderBy('id', 'desc')
                ->get();
        //print_r($row);die;
        $data = array();
        if (!empty($row)) {
            foreach ($row as $key => $rows) {
                $nestedData['staff_name'] = $rows->staff_name;
                $nestedData['from_date'] = $rows->from_date;
                $nestedData['to_date'] = $rows->to_date;
                $nestedData['from_time'] = $rows->from_time;               
                $nestedData['to_time'] = $rows->to_time;                     
                $nestedData['created_date'] = date('Y-m-d h:i A',strtotime($rows['created_at']));          
               
                $nestedData['action'] =  getButtons([
                                ['key'=>'edit','link'=>route('staffs.edit_leave_form',[$rows->id])],
                                ['key'=>'delete','link'=>route('staffs.destroy_leave',[$rows->id])],
                            ]);
                
                $data[] = $nestedData;
            }

        }
        //$totalFiltered = isset($key) ? $key + 1 : 0;
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );
        echo json_encode($json_data);
    }


    public function add_leave_form($id)
    {
        $action = "leaves";
        $title = __('messages.Add New Leave');
        $model = $this->model;
        $breadcum = [__('messages.Staff')=>route($model.'.index'),$title =>''];
        $staff = Staff::where('id',$id)->first()->toArray();;
        //echo '<pre>'; print_r($staff);die;
        return view('spadmin.staff.addLeaveForm',compact('action','title','breadcum','model','leaves','id','staff'));
    }


    public function createLeave(Request $request)
    {
        if (Input::isMethod('post')) {
           try
           {
                $data = $request->all();
                  $validator = Validator::make($data, 
                    [
                      'staff_id' => 'required|exists:staff,id',
                      'from_date' => 'required',
                      'to_date' => 'required',
                      'from_time' => 'required',
                      'to_time' => 'required',
                    ]);
                  if ($validator->fails()) 
                  {
                    $error = $this->validationHandle($validator->messages());
                    Session::flash('danger',$error);
                     return redirect()->back()->withInput();
                  }     
                  else 
                  {  
                    $start_time = date("H:i:s", strtotime($request->from_time));
                
                    $end_time = date("H:i:s", strtotime($request->to_time));

                    $check_booking = Booking::where(function ($query) use ($request,$start_time,$end_time) {
                        $query->where(function ($query) use ($request,$start_time, $end_time) {
                            $query
                                ->where('bookings.appointment_date',date('Y-m-d',strtotime($request->from_date)))
                                ->Where('bookings.appointment_time','>=',$start_time)
                                ->Where('bookings.service_end_time','<=',$end_time);
                        })
                        ->orWhere(function ($query) use ($request,$start_time, $end_time) {
                            $query
                                ->where('bookings.appointment_date',date('Y-m-d',strtotime($request->to_date)))
                                ->Where('bookings.appointment_time','>=',$start_time)
                                ->Where('bookings.service_end_time','<=',$end_time);
                        });
                        if(strtotime($request->from_date)!=strtotime($request->to_date)){
                           $query = $query->orWhere(function ($query) use ($request) {
                                $query
                                ->where('bookings.appointment_date','>',date('Y-m-d',strtotime($request->from_date)))
                                ->Where('bookings.appointment_date','<',date('Y-m-d',strtotime($request->to_date)));
                            });
                        }
                        

                    })
                    ->where('bookings.staff_id',$request->staff_id)->exists();
                    
                    //echo "<pre/>";print_r($check_booking);die;
                  if($check_booking){ 
                        Session::flash('danger', __('messages.Staff has bookings on selected dates.'));
                       return redirect()->back()->withInput();
                    } else {
                        $srow = new StaffLeave();
                        $srow['staff_id'] = $request->staff_id;
                        $srow['from_date'] = date('Y-m-d',strtotime($request->from_date));
                        $srow['to_date'] = date('Y-m-d',strtotime($request->to_date));
                        $srow['from_time'] = date('H:i:s',strtotime($request->from_time));
                        $srow['to_time'] = date('H:i:s',strtotime($request->to_time));
                        $srow->save();
                        if($srow->save())
                        { 
                            Session::flash('success', __('messages.Leave has been added successfully.'));
                            return redirect()->route('staffs.leaves',[$request->staff_id]);
                        }    
                    }       
    
                  }
           }
           catch(\Exception $e){
              $msg = $e->getMessage();
               Session::flash('danger', $msg);
               return redirect()->back()->withInput();
           }
        }
    }


    public function edit_leave_form($id)
    {
        $action = "leaves";
        $title = __('messages.Update Leave');
        $model = $this->model;
        $breadcum = [__('messages.Staff')=>route($model.'.index'),$title =>''];
        $staff_leave = StaffLeave::where('id',$id)->first()->toArray();
        //echo '<pre>'; print_r($staff);die;
        return view('spadmin.staff.editLeaveForm',compact('action','title','breadcum','model','staff_leave','id','staff'));
    }

    public function editLeave(Request $request)
    {
        if (Input::isMethod('post')) {
           try
           {
                $data = $request->all();
                  $validator = Validator::make($data, 
                    [
                      'id' => 'required',
                      'from_date' => 'required',
                      'to_date' => 'required',
                      'from_time' => 'required',
                      'to_time' => 'required',
                    ]);
                  if ($validator->fails()) 
                  {
                    
                    $error = $this->validationHandle($validator->messages());
                    Session::flash('danger',$error);
                     return redirect()->back()->withInput();
                  }     
                  else 
                  {  
                    $start_time = date("H:i:s", strtotime($request->from_time));
                
                    $end_time = date("H:i:s", strtotime($request->to_time));

                    $check_booking = Booking::where(function ($query) use ($request,$start_time,$end_time) {
                        $query->where(function ($query) use ($request,$start_time, $end_time) {
                            $query
                                ->where('bookings.appointment_date',date('Y-m-d',strtotime($request->from_date)))
                                ->Where('bookings.appointment_time','>=',$start_time)
                                ->Where('bookings.service_end_time','<=',$end_time);
                        })
                        ->orWhere(function ($query) use ($request,$start_time, $end_time) {
                            $query
                                ->where('bookings.appointment_date',date('Y-m-d',strtotime($request->to_date)))
                                ->Where('bookings.appointment_time','>=',$start_time)
                                ->Where('bookings.service_end_time','<=',$end_time);
                        });
                        if(strtotime($request->from_date)!=strtotime($request->to_date)){
                           $query = $query->orWhere(function ($query) use ($request) {
                                $query
                                ->where('bookings.appointment_date','>',date('Y-m-d',strtotime($request->from_date)))
                                ->Where('bookings.appointment_date','<',date('Y-m-d',strtotime($request->to_date)));
                            });
                        }
                        

                    })
                    ->where('bookings.staff_id',$request->staff_id)->exists();

                    //$check_booking = Booking::whereBetween('appointment_date', [$request->from_date, $request->to_date])->exists();
                    if($check_booking){ 
                        Session::flash('danger', __('messages.Staff has bookings on selected dates.'));
                       return redirect()->back()->withInput();
                    } else {
                        $srow = StaffLeave::where('id',$request->id)->first();
                        if($srow){
                            
                            $srow['from_date'] = date('Y-m-d',strtotime($request->from_date));
                            $srow['to_date'] = date('Y-m-d',strtotime($request->to_date));
                            $srow['from_time'] = date('H:i:s',strtotime($request->from_time));
                            $srow['to_time'] = date('H:i:s',strtotime($request->to_time));
                            $srow->save();

                            if($srow->save())
                            {
                                Session::flash('success', __('messages.Leave has been updated successfully.'));
                             return redirect()->route('staffs.leaves',[$request->staff_id]);
                            }       
                    
                        }else{
                            Session::flash('danger', __('messages.Leave not found.'));
                            return redirect()->route('staffs.leaves',[$request->staff_id]);
                        }
                    }
                  }
           }
           catch(\Exception $e){
              $msg = $e->getMessage();
               Session::flash('danger', $msg);
               return redirect()->back()->withInput();
           }
        }
    }

      public function destroy_leave($id)
    {
        $row = StaffLeave::where('id', $id)->first();
        if ($row) {
            $row->delete();
             Session::flash('success', __('messages.Staff leave deleted successfully.'));
            return redirect()->back();
        } else {
            Session::flash('warning', __('messages.Invalid request.'));
            return redirect()->back();
        }
    }


    public function showBookingDetails($row)
  {

    $data = array();
    $data['id'] = $row['id'];
    $data['booking_unique_id'] = $row['booking_unique_id'];
    $data['sp_id'] = $row['sp_id'];
    $data['db_user_name'] = $row['db_user_name'];
    $data['db_user_mobile_no'] = $row['db_user_mobile_no'];
    if(!$row['user_id'] && !$row['is_gift'] && ($row['db_user_name'] || $row['db_user_mobile_no'])){
      $data['user_name'] = $row['db_user_name'];                          
    }elseif(!$row['user_id'] && $row['is_gift'] && ($row['receiver_name'] || $row['receiver_mobile_no'])){
      $data['user_name'] = $row['receiver_name'];                         
    } else {
      $data['user_name'] = $row->getAssociatedUserInfo->name;
      $data['user_profile_image'] = $row->getAssociatedUserInfo->image!='' ? asset("uploads/".$row->getAssociatedUserInfo->image):'';
    }
    
    $data['user_id'] = $row['user_id'];
    $data['staff_id'] = $row['staff_id'];
    if($row['staff_id']!='' && $row['staff_id']!=null){
      $data['staff_name'] = $row->getAssociatedStaffInformation->staff_name;
    }else{
      $data['staff_name'] = "";
    }
    
    $data['appointment_time'] = $row['appointment_time'];
    $data['invoice'] = $row['invoice'];
    $data['service_end_time'] = $row['service_end_time'];
    $data['appointment_date'] = $row['appointment_date']!='' && $row['appointment_date']!= null ? $row['appointment_date']:"";
    $data['redeemed_reward_points'] = $row['redeemed_reward_points'];
    $data['payment_type'] = $row['payment_type'];
    $data['promo_code_amount'] = round($row['promo_code_amount'],0);
    $data['tax_amount'] = round($row['tax_amount'],0);
    $data['total_amount'] = round($row['booking_total_amount'],0);
    $data['paid_amount'] = round($row['paid_amount'],0);
    $data['total_item_amount'] = round($row['total_item_amount'],0);
    $data['total_cashback'] = round($row['total_cashback'],0);
    $data['created_date'] = date('Y-m-d h:i A',strtotime($row['created_at']));

    $item_name_arr = array();
    foreach($row->getAssociatedBookingItems as $bkitems){

      if($bkitems['is_service'] == '1')
      {
        $item_name_arr[] = [
          'service_id' => $bkitems['booking_item_id'],
          'name' => $bkitems->getAssociatedServiceDetail->getAssociatedService->name,
          'quantity' => $bkitems['quantity'],
          'original_price' => round($bkitems['original_price'],0),
          'best_price' => round($bkitems['best_price'],0),
          'cashback' => round($bkitems['cashback'],0),
        ];
      }
      else{
        $item_name_arr[] = [
          'offer_id' => $bkitems['booking_item_id'],
          'name' => $bkitems->getAssociatedOfferDetail->getAssociatedOfferName->title,
          'quantity' => $bkitems['quantity'],
          'original_price' => round($bkitems['original_price'],0),
          'best_price' => round($bkitems['best_price'],0),
          'cashback' => round($bkitems['cashback'],0),
        ];
      }

    }
    $data['booking_items'] = $item_name_arr;
    
    return $data;
        
  }
     
     public function staffCalendar(Request $request) {
        $action = "Staff Calendar";
        $title = __('messages.Staff Calendar');
        $model = $this->model;
        $breadcum = [$title =>''];
        $selected_date = '';
        $selected_staff = 0;
        $user_data = spLoginData();
        if (Input::isMethod('post')) {
            $data = $request->all();
            $validator = Validator::make($data, ['selected_date' => 'required']);
            if ($validator->fails()) 
            {
                $error = $this->validationHandle($validator->messages());
                Session::flash('danger',$error);
                return redirect()->back()->withInput();
            } 
            else 
            {
                $selected_date = $request->selected_date;
                $selected_staff = $request->selected_staff;
               
            } 
        } else {
            $selected_date = date('Y-m-d'); 
        }

        if($selected_date) {
            $user_id = $user_data['id'];
            //$user_id = 21;

            $db_day_arr = array('0'=>'7','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6');

            $date = date('Y-m-d',strtotime($selected_date));

            $date = strtotime($selected_date);
            $day = $db_day_arr[date('w', $date)];

            $shop_time = ShopTiming::where('user_id',$user_id)
            ->where('isOpen','1')
            ->where('day',$day)
            ->first();
            
            if($shop_time){
                    //shop time slots
                $time_frequency = 15;
                $start_time = date('H:i',strtotime($shop_time['opening_time']));
                $end_time = date('H:i',strtotime($shop_time['closing_time']));  

                $i=0;
                for($i = strtotime($start_time); $i<= strtotime($end_time); $i = $i + $time_frequency * 60) {
                    $shoptime_slots[] = date("H:i", $i);  
                }
                
                //------------

                if($selected_staff){
                    $staff = Staff::where('sp_id',$user_id)->whereIn('id',$selected_staff)->where('is_deleted','!=','1')->get();
                }else{
                    $staff = Staff::where('sp_id',$user_id)->where('is_deleted','!=','1')->get();
                }

                $calender = array();
                $final_array = array();

                foreach($staff as $row)
                {
                    $calender = [
                        "id" => $row->id,
                        "staff_name" => $row->staff_name,
                        "nationality" => $row->nationality,
                        "experience" => $row->experience,
                        "speciality" => $row->speciality,
                        "certificates" => $row->certificates,
                        "is_provide_home_service" => $row->is_provide_home_service,
                        "staff_image" => $row->staff_image!='' ? changeImageUrlForFileExist(asset('sp_uploads/staff/'.$row->staff_image)):'',
                    ];
                    $sel_date = date('Y-m-d',strtotime($selected_date));
                    $leave = StaffLeave::where('from_date','<=',$sel_date)
                    ->where('to_date','>=',$sel_date)
                    ->where('staff_id',$row->id)
                    ->first();

                    $leave_slots = array();
                    if($leave){
                        //$lst leave start time
                        //$let leave end time

                        if($sel_date == $leave['from_date'] && $sel_date == $leave['to_date'])
                        {
                            
                            $lst = $leave['from_time'];
                            $let = $leave['to_time'];
                            
                        }
                        elseif($sel_date == $leave['from_date'])
                        {
                            
                            $lst = $leave['from_time'];
                            $let = $end_time;
                            
                        }
                        elseif($sel_date == $leave['to_date']){
                            
                            $lst = $start_time;
                            $let = $leave['to_time'];
                            
                        }else{
                            $lst = $start_time;
                            $let = $end_time;
                        }
                        $slot_diff = strtotime($lst)%900;

                        if($slot_diff > 0)
                        {
                            $lst = strtotime($lst)-$slot_diff;

                        }else{
                            $lst = strtotime($lst);
                        }

                        $slot_diff = strtotime($let)%900;
                 
                        if($slot_diff > 0)
                        {
                            $let = strtotime($let)+(900-$slot_diff);
                        }else{
                            $let = strtotime($let);
                        }
                        $i=0;
                        for($i = $lst; $i<= $let; $i = $i + $time_frequency * 60) {
                            
                            if($i == $let) {
                                $leave_slots[] = date("H:i", $i-60);
                            }else{
                                $leave_slots[] = date("H:i", $i);
                            }
                        }
                    }

                    $booking = Booking::where('staff_id',$row->id)
                    ->where(function ($query) {
                        $query->where(function ($query){
                            $query->where('status','1');

                        })
                        ->orWhere(function ($query) {
                            $query
                                ->where('staff_id','!=','')
                                ->where('status','0');
                        })
                        ->orWhere(function ($query) {
                            $query
                                ->where('status','5');
                        });
                    })

                    
                    ->where('appointment_date',$selected_date)
                    // ->select('appointment_time','service_end_time','id','status','booking_service_type')
                    ->get();
                    
                    $booked_slots = array();
                    $slots = array();
                    $time_frequency = 15;
                    $status_arr = array('0'=>'new','1'=>'confirmed','5'=>'completed');
                    //determining booked slots
                    foreach($booking as $val){
                        
                        $slot_diff = strtotime($val['appointment_time'])%900;
                        if(empty($val['service_end_time'])){
                            $val['service_end_time'] = date('H:i', strtotime($val['appointment_time'].'+ 5 minutes'));
                        }
                        if($slot_diff > 0)
                        {
                            $val['appointment_time'] = strtotime($val['appointment_time'])-$slot_diff;

                        }else{
                            $val['appointment_time'] = strtotime($val['appointment_time']);
                        }

                        $slot_diff = strtotime($val['service_end_time'])%900;
                 
                        if($slot_diff > 0)
                        {
                            $val['service_end_time'] = strtotime($val['service_end_time'])+(900-$slot_diff);
                        }else{
                            $val['service_end_time'] = strtotime($val['service_end_time']);
                        }

                        for($i = $val['appointment_time']; $i<= $val['service_end_time']; $i = $i + $time_frequency * 60) {
                            
                            if($i == $val['service_end_time']) {
                                $booked_slots[] = date("H:i", $i-60);
                                
                                $booking_id[$val['id']]['status'] = $val['status'];
                                $booking_id[$val['id']]['service_type']=$val['booking_service_type'];
                                $booking_id[$val['id']]['times'][] =  date("H:i", $i-60);
                            }else{
                                $booked_slots[] = date("H:i", $i);
                                $booking_id[$val['id']]['status'] = $val['status'];
                                $booking_id[$val['id']]['service_type']=$val['booking_service_type'];

                                $booking_id[$val['id']]['order_detail']=$this->showBookingDetails($val);

                                $booking_id[$val['id']]['times'][] =  date("H:i", $i);
                            }
                        }
                    }
                    /*echo '<pre>';
                     print_r($shoptime_slots);
                     print_r($booking_id);
                     print_r($booked_slots);die;*/
                    $slot_array = array();
                    $status = '';
                    $order_detail = '';
                    foreach($shoptime_slots as $slots){
                        if(in_array($slots,$booked_slots)){
                            foreach($booking_id as $aa){
                                foreach($aa['times'] as $bb){
                                    if($slots == $bb){
                                        $status = $status_arr[$aa['status']].'-'.$aa['service_type'].'-'.'booked';                                       
                                        $order_detail = $aa['order_detail'];
                                    }
                                }
                                
                            }
                            
                        }elseif(in_array($slots,$leave_slots)){
                            $status = 'leave';
                        }else{
                            $status = 'free';
                        }

                        $slot_array[] = [
                            'time' => $slots,
                            'status' => $status,
                            'order_detail' => $order_detail,
                        ];

                        
                    }
                    $calender['booking']= $slot_array;
                    $final_array[] = $calender;
                    
                }//end staff foreach
                //echo '<pre>'; print_r($final_array);die;
            } //end shoptime if
            else{
              Session::flash('danger', __('messages.Shop is closed now'));
            }
       }
      else {
         Session::flash('danger', __('messages.Date is required.'));
     }
     $staff_name_rows = Staff::select('id','staff_name')->where('sp_id',$user_id)->where('is_deleted', '0')->get();
    return view('spadmin.staff.staffCalendar',compact('action','title','breadcum','model','final_array', 'selected_date', 'staff_name_rows', 'selected_staff', 'shoptime_slots'));
    }

     public function ajaxGetToTime(Request $request) {
        $user_data = spLoginData();
        $selected_date = $request->selected_date;
        $db_day_arr = array('0'=>'7','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6');
        $day = $db_day_arr[date('w', strtotime($selected_date))];

        //$shop_time = ShopTiming::where('user_id',$user_data['id'])
        $shop_time = ShopTiming::where('user_id',$user_data['id'])
        ->where('isOpen','1')
        ->where('day',$day)
        ->first();
                
        $time_frequency = 15;
        $shoptime_slots = array();
        if($shop_time['opening_time']){
          $start_time = date('H:i',strtotime($shop_time['opening_time']));
          $end_time = date('H:i',strtotime($shop_time['closing_time']));  

          for($i = strtotime($start_time); $i<= strtotime($end_time); $i = $i + $time_frequency * 60) {
            $shoptime_slots[date("H:i", $i)] = date("H:i", $i);  
          }
        }
          $selected_time = '';
          if($request->selected_time){
            $selected_time = $request->selected_time;
          }
        //echo '<pre>'; print_r($shoptime_slots);
        return view('spadmin.staff.ajaxGetToTime',compact('shoptime_slots', 'selected_time'));
    }

     public function ajaxGetFromTime(Request $request) {
        $user_data = spLoginData();
        $selected_date = $request->selected_date;
        $db_day_arr = array('0'=>'7','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6');
        $day = $db_day_arr[date('w', strtotime($selected_date))];

        //$shop_time = ShopTiming::where('user_id',$user_data['id'])
        $shop_time = ShopTiming::where('user_id',$user_data['id'])
        ->where('isOpen','1')
        ->where('day',$day)
        ->first();
                
        $time_frequency = 15;
        $shoptime_slots = array();
        if($shop_time['opening_time']){
          $start_time = date('H:i',strtotime($shop_time['opening_time']));
          $end_time = date('H:i',strtotime($shop_time['closing_time']));  

          for($i = strtotime($start_time); $i<= strtotime($end_time); $i = $i + $time_frequency * 60) {
            $shoptime_slots[date("H:i", $i)] = date("H:i", $i);  
          }
        }
          $selected_time = '';
          if($request->selected_time){
            $selected_time = $request->selected_time;
          }
        //echo '<pre>'; print_r($shoptime_slots);
        return view('spadmin.staff.ajaxGetFromTime',compact('shoptime_slots', 'selected_time'));
    }
}
