<?php

namespace App\Http\Controllers\SpAdmin;
use datatables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use DB;
use Illuminate\Support\Facades\Input;   
use Illuminate\Support\Facades\Hash;
use Intervention\Image\ImageManagerStatic as Image;
use Session;
use App\Model\SpUser;
use App\Model\Category;
use App\Model\Tier;
use App\Model\SpAmenity;
use App\Model\Amenity;
use App\Model\SpBannerImages;
use App\Model\ShopTiming;
use App\Model\OtpNumber;

use App\Model\Conversation;
use App\Model\Chat;
use App\Model\User;
use App\Model\Admin;
use App\Model\SpCountry;

class UserController extends Controller
{
    protected $title;

    public function __construct()
    {
        $this->title = 'Service Provider';
    } 

    public function login()
    {
    	$title = 'Login';
    	return view('spadmin.users.login',compact('title'));
    }
    public function checklogin(Request $request)
    {
    	 try {
                $data = $request->all();
                $validator = Validator::make($request->all(), [
                  'country_code' => 'required|exists:countries,phonecode',
                  'mobile_no' => 'required|required|regex:/^([0-9\s\-\+\(\)]*)$/|max:16|min:8',
                  'password' => 'required|min:8|max:50',
                ]);

                if ($validator->fails()) 
                {
                    return redirect()->back()->withInput()->withErrors($validator->errors());

                }
                else 
                {
                   $user = SpUser::where(['mobile_no' => $data['mobile_no'],'country_code' => $data['country_code']])->first();
                if($user){

                    if($user->is_approved!= 1){
                        $msg = 'Your application is under review. Thank you for your patience.';
                        Session::flash('warning', $msg);
                        return redirect()->back()->withInput();
                     
                    }
                    elseif($user->is_approved==1 && $user->status==0)
                    {
                        $msg = 'Your account has been deactivated';
                        Session::flash('warning', $msg);
                        return redirect()->back()->withInput();
                    }
                    else{
                        if($user->is_approved == 1)
                        {
                            if (Hash::check($data['password'], $user->password)){

                                 Session::put('SpAdminLoggedIn', ['user_id'=>$user->id,'userData'=> $user]);
                                Session::save();
                                return redirect()->route('spadmin.dashboard');
                            }
                            else
                            {
                                $msg = 'Incorrect Password';
                                Session::flash('warning', $msg);
                                return redirect()->back()->withInput();

                            } 
                        }else{
                            $error_message = ' Your account is not approved by admin.';
                            Session::flash('warning', $error_message);
                            return redirect()->back()->withInput();
                          
                        }
                    }
                }
                else{
                    $error_message = ' Invalid Credentials';
                    Session::flash('warning', $error_message);
                    return redirect()->back()->withInput();
                }
              }   
            } 
            catch (\Exception $e) 
            {
                $msg = $e->getMessage();
                Session::flash('invalid',$msg);
                return redirect()->back()->withInput();
            }
    }

    public function logout()
    {
        Session::forget('SpAdminLoggedIn');
        return redirect()->route('spadmin.logout');
    }

    public function changePassword()
    {
        $title = 'Password Setting';
        $breadcum = [$title =>''];
        return view('spadmin.users.changePassword',compact('title','breadcum'));
    }

    public function gallery()
    {
        $user_data = spLoginData();
        $userId=$user_data->id;
        $title = 'Banner Image';
        $breadcum = [$title =>''];
        $banner_images = SpBannerImages::where('user_id',$userId)->get();
        return view('spadmin.users.gallery',compact('title','breadcum','banner_images'));
    }

    public function updateGallery(Request $request)
    {

        $validation = array('banner_image' => 'required|mimes:jpeg,jpg,png,gif|dimensions:min_width=1300,min_height=800',);
        $validator = Validator::make(Input::all(), $validation);
        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator->errors());
        }
        else
        {
            $limit=10;
            $user_data = spLoginData();
            $userId = $user_data['id'];
            $banner_images = SpBannerImages::where('user_id',$userId)->count();
            if($limit > $banner_images)
            {
                $row = new SpBannerImages();
                $row->user_id = $userId;

                if ($request->hasFile('banner_image') && $request->file('banner_image'))
                {
                    $file = $request->file('banner_image');

                    $name = time().'.'.$file->getClientOriginalExtension();

                    $image_resize = Image::make($file->getRealPath());
                    $image_resize->resize(1300, 800);    
                    // $destinationPath = public_path('/sp_uploads/banner_images/');
                    $image_resize->save(public_path('/sp_uploads/banner_images/' .$name));
                    // $img = $file->move($destinationPath, $name);

                    $row->banner_path = $name;
                }
                $row->save();
                return redirect()->route('spuser.gallery');
            }
            else
            {
                return redirect()->back()->withErrors('Maximum 10 banners allowed');
            }
        }
        
    }

    public function doUpdatePassword()
    {
        $validation = array(
            'old_password' => 'required',
            'new_password' => 'required|min:8',
            'confirm_password' => 'required|same:new_password',
        );
        $validator = Validator::make(Input::all(), $validation);
        if ($validator->fails()) 
        {
            return redirect()->back()->withErrors($validator->errors());
        }
        else
        {
            $user_data = spLoginData();//helper function
           
            $user =  SpUser::where('id',$user_data['id'])->first();
           
            if (Hash::check(Input::get('old_password'), $user->password))
            {
                $user->password = Hash::make(Input::get('new_password'));
                $user->save();
                Session::flash('success', __('Password updated successfully.'));
                return redirect()->route('spuser.change-password');
            } 
            else
            {
                Session::flash('warning', __('Wrong old password'));
                return redirect()->back();
            }
        }
    }

    public function edit()
    {
        $user_data = spLoginData();
        $id = $user_data['id'];
        $row = SpUser::where('id',$id)->first();
        $title = 'Edit Profile';
        // $model = $this->model;
        $breadcum = [$title=>''];
        $categories = $this->getCategoryName();
        $tiers = $this->getTierName();    
        $user_amenities = SpAmenity::where('user_id',$id)->get();
        $usr_amen = array();
        foreach($user_amenities as $usr_am){
            $usr_amen[] = $usr_am['amenity_id'];
        }
        $amenities = $this->getAmenities();
        $banner_images = SpBannerImages::where('user_id',$id)->get();
        $country_list = $this->getmapcountry();
        $user_countries = SpCountry::where('user_id',$id)->first();
        return view('spadmin.users.edit',compact('row','title','breadcum','categories','tiers','amenities','usr_amen','banner_images','country_list','user_countries'));
    }

    public function update(Request $request)
    {
       
        try{
            $validation = array(
             // 'is_individual' => 'required',
             // 'category_id' => 'required',
              'store_name' => 'required|max:50',
              'store_number' => 'required|min:8|max:50',
              'name' => 'required|max:50',
              'address' => 'required',
              'email' => 'email|unique:sp_users,email,'.$request->id,
              'country_code' => 'required|exists:countries,phonecode',
              'mobile_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:16|min:8|unique:sp_users,mobile_no,'.$request->id,
              'whatsapp_no' => 'regex:/^([0-9\s\-\+\(\)]*)$/|max:16|min:8',
              'longitude' => 'required',
              'latitude' => 'required',
              'landmark' => 'max:200',
              'service_criteria' => 'required',
              'amenity' => 'required',
              'profile_image' => 'mimes:jpeg,jpg,png,gif',
              'country'=>'required',
            );

            $validator = Validator::make(Input::all(), $validation);

            if ($validator->fails()) {
                 return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
            } 
            else {
                $email_check = 0;
                $user_id = $request->id;
                $user = SpUser::where('id', $request->id)->first();
                $user->name = $request->name;
                //$user->is_individual = $request->is_individual;
                $user->service_criteria = $request->service_criteria;
                //$user->category_id = $request->category_id;
                //$user->tier_id = $request->tier_id;
                $user->store_name = $request->store_name;
                $user->store_number = $request->store_number;
                $user->country_code = $request->country_code;
                if($request->email && $request->email!= $user->email){
                    $user->verifyToken = strtotime(date('Y-m-d H:i:s')).rand(1111,999999);
                    $user->email = $request->email;
                    $email_check = 1;
                    
                }elseif($request->email){
                    $user->email = $request->email;
                }
                $user->mobile_no = $request->mobile_no;
                $user->whatsapp_no = $request->whatsapp_no;
                $user->address = $request->address;
                $user->latitude = $request->latitude;
                $user->longitude = $request->longitude;
                $user->landmark = $request->landmark;
                $user->description = $request->description;
                $user->area_description = $request->area_description;
                $user->service_criteria = implode(',',$request->service_criteria);
                $user->sub_locality = $request->sub_locality;

                if ($request->hasFile('profile_image') && $request->file('profile_image'))
                {
                    $image = $request->image_cr;
                    list($type, $image) = explode(';', $image);
                    list(, $image)      = explode(',', $image);
                    $image = base64_decode($image);
                    $image_name= time().'.png';
                    $path = public_path('/sp_uploads/profile/'.$image_name);
                    file_put_contents($path, $image);
                    $user->profile_image = $image_name;
                } 

                $amenities = $request->amenity;
                foreach($amenities as $am){
                    SpAmenity::manageAmenities($user_id, $am,'remove');
                }
                foreach($amenities as $am){
                    SpAmenity::manageAmenities($user_id, $am,'add');
                }

                if($request->banner_path){

                    $count = count($request->banner_path);
                
                    for($i=0;$i< $count;$i++){
           
                        $img = $request->banner_path[$i];
                        if ($img)
                        {
                            $file = $img;
                            $name = time().str_random(2).'.'.$file->getClientOriginalExtension();
                            $destinationPath = public_path('sp_uploads/banner_images/');
                            $img = $file->move($destinationPath, $name);
                            $row = new SpBannerImages();
                            $row->user_id = $user_id;
                            $row->banner_path = $name;
                            $row->save();
                        }
                    }
                }

                if($request->country)
                { 
                    $country_code = $country_name = $state_code = $state_name = $city_name = ''; 
                    $city_code = 0;       
                    $get_country = explode('/',$request->country);
                    $country_code = $get_country[0];
                    $country_name = $get_country[1];

                    if(isset($request->state) && $request->state !='' && $request->state !=null)
                    {
                        $get_state = explode('/',$request->state);
                        $state_code = $get_state[0];
                        $state_name = $get_state[1];
                    }
                    
                    if(isset($request->city) && $request->city !='' && $request->city !=null)
                    {
                        $get_city = explode('/',$request->city);
                        $city_code = $get_city[0];
                        $city_name = $get_city[1];
                    }
                    
                    $attribute = ['user_id' => $user_id];
                    $values = ['country_isocode' => $country_code,'country_name'=> $country_name, 'state_isocode' => $state_code,'state_name' => $state_name,'city_isocode' => $city_code,'city_name' =>$city_name];

                    $maprow = SpCountry::where('user_id', $user_id);
                    if($maprow->count() !=0)
                    {
                        $maprow->update($values);
                    }
                    else
                    {
                        SpCountry::updateOrCreate($attribute,$values);
                    }
                }

                $user->account_title = $request->account_title;
                $user->bank = $request->bank;
                $user->account_iban = $request->account_iban;
                $user->branch_name = $request->branch_name;
                $user->branch_address = $request->branch_address;
                $user->save();

                if($user->save() && $email_check == 1)
                {
                    $data['templete'] = "sp_confirmation_email";
                    $data['name'] = $user->name;
                    $data['token'] = $user->verifyToken;
                    $data['email'] = $request->email;
                    $data['subject'] = "Confirmation Email";
                    send($data);
                }
                
                Session::flash('success', __('Service Provider Updated successfully.'));
                return redirect()->route('spadmin.dashboard');
            }//else
        }
        catch(\Exception $e){
            $msg = $e->getMessage();
            Session::flash('danger', $msg);
            return redirect()->back()->withInput();
        }
    }

    public function resendMail()
    {
        $user_data = spLoginData();
        $user = SpUser::whereId($user_data['id'])->first();
        if($user)
        {
            $data['templete'] = "sp_confirmation_email";
            $data['name'] = $user->name;
            $data['token'] = $user->verifyToken;
            $data['email'] = $user->email;
            $data['subject'] = "Confirmation Email";
            send($data);
            return 'success';
        }
    }


    public function showShopTiming()
    {
        $title = 'Shop Timings';
        $breadcum = [$title=>''];
        $user_data = spLoginData();
        $row = ShopTiming::where('user_id',$user_data['id'])->get();
        return view('spadmin.users.shopTiming',compact('title','row','breadcum'));
    }

    public function updateShopTimings(Request $request)
    {
        if (Input::isMethod('post')) {
           try
           {

                $data = $request->all();
                // echo "<pre>";print_r($data);die;
                $time_arr = array();
                $day_arr = array('1'=>'Monday','2'=>'Tuesday','3'=>'Wednesday','4'=>'Thursday','5'=>'Friday','6'=>'Saturday','7'=>'Sunday');
                $dataobject = [];
                $finalData = [];
                for($i=1;$i<=7;$i++)
                {
                    $op_t = 'opening_time_'.$day_arr[$i];
                    $cl_t = 'closing_time_'.$day_arr[$i];
           
                    $op_1 = str_replace(['AM', 'PM'], [' AM', ' PM'], $request->$op_t);
                    $cl_1 = str_replace(['AM', 'PM'], [' AM', ' PM'], $request->$cl_t);
                    
                    
                    $request->$op_t = date("H:i", strtotime($op_1));
                    $request->$cl_t = date("H:i", strtotime($cl_1));

                    $validation[$op_t] = 'required|before_or_equal:'.$request->$cl_t;
                }
                 $validator = Validator::make(Input::all(), $validation);

                if ($validator->fails()) 
                {
                     return redirect()->back()->withErrors($validator->errors())->withInput(Input::all());
                }
                else
                {
                    $user_data = spLoginData();
                    $user_id = $user_data['id'];
                    $time_arr = array();

                    for($i=1;$i<=7;$i++)
                    {
                        $op = 'isOpen_'.$i;
                        if($request->$op!= 1){
                            $request->$op = 0;
                        }
                        $op_t = 'opening_time_'.$day_arr[$i];
                        $cl_t = 'closing_time_'.$day_arr[$i];
                        $op_1 = str_replace(['AM', 'PM'], [' AM', ' PM'], $request->$op_t);
                        $cl_1 = str_replace(['AM', 'PM'], [' AM', ' PM'], $request->$cl_t);
                        
                        
                        $request->$op_t = date("H:i", strtotime($op_1));
                        $request->$cl_t = date("H:i", strtotime($cl_1));

                       
                        $time_arr[] = [
                            'day' => $i,
                            'isOpen' => $request->$op,
                            'opening_time' => date("H:i", strtotime($request->$op_t)),
                            'closing_time' => date("H:i", strtotime($request->$cl_t)),
                        ];
                       
                    }

                    foreach($time_arr as $val){
                        $row = ShopTiming::where('user_id',$user_id)->where('day',$val['day'])->first();
                        $row->isOpen = $val['isOpen'];
                        $row->opening_time = $val['opening_time'];
                        $row->closing_time = $val['closing_time'];
                        $row->save();
                    }
                    Session::flash('success', __('Shop Timings Updated successfully.'));
                    return redirect()->route('spuser.shop-timings'); 
                } 
           }
           catch(\Exception $e){
             $msg = $e->getMessage();
             Session::flash('invalid',$msg);
             return redirect()->back()->withInput();
           }
        }
    }

    public function getTierName()
    {

        $tier_data = Tier::get();
        $tier_arr = array();

        foreach($tier_data as $data)
        {
            $tier_arr[$data['id']] = $data['title'].'(Commission - '.$data['commission'].',redeem - '.$data['redeem'].',earning - '.$data['earning'].')'; 
        }
         
        return $tier_arr;
    }

    public function getCategoryName()
    {
        $cat_data = Category::get();
        $cat_arr = array();

        foreach($cat_data as $data)
        {
            $cat_arr[$data['id']] = $data['category_name']; 
        }
         
        return $cat_arr;
    }

    public function getAmenities()
    {
        $amenity_data = Amenity::get();
        $amenity_arr = array();

        foreach($amenity_data as $data)
        {
            $amenity_arr[$data['id']] = $data['name']; 
        }
         
        return $amenity_arr;
    }

    public function ajaxChangeMobileNo(Request $request)
    {
        $user = SpUser::where('mobile_no',$request->mobile_no)->where('country_code',$request->country_code)->first();
      
        if($user)
        {
            return 'exists';
        }
        else{
            $mobileNoWithCode = $request->country_code . $request->mobile_no;
            $otp = $this->getOpt($mobileNoWithCode);
            return $otp;     
        }
    }

    public function getOpt($mobileNoWithCode)
    {
      $row = OtpNumber::where('complete_mobile_no',$mobileNoWithCode)->first();
      if($row){

        $otp = $row->otp;
      }
      else{     
        //$otp = mt_rand(100000, 999999);
        $otp = 12345;
      }

      $username =  env('SMS_COUNTRY_USERNAME');
      $password =  env('SMS_COUNTRY_PASSWORD');
      $message ="Your one time password for registration is ".$otp." don't share this with anyone."; 
      $parami =['User'=>$username,'passwd'=>$password,'mobilenumber'=>$mobileNoWithCode,'message'=>$message,'sid'=>'smscntry','mtype'=>'N'];
      $ponmo = http_build_query($parami);
      $url = "http://api.smscountry.com/SMSCwebservice_bulk.aspx?$ponmo"; // json
      $res = $this->get_content($url);
      if(!$row){
            $new_row = new OtpNumber();
            $new_row->complete_mobile_no = $mobileNoWithCode;
            $new_row->otp = $otp;     
            $new_row->save();
        }
      return $otp;    
    }

    function get_content($URL)
    {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_URL, $URL);
      $data = curl_exec($ch);
      curl_exec($ch);
      curl_close($ch);
      return $data;
    }

    public function verifyOtp(Request $request){
      $mobileWithCode = explode(',',$request->mobileWithCode);

      $mocode = str_replace(',', '', $request->mobileWithCode);

      $row = OtpNumber::where('complete_mobile_no',$mocode)->where('otp',$request->otp)->count();

      if($row > 0){
        OtpNumber::where('complete_mobile_no',$mocode)->where('otp',$request->otp)->delete();
        
        return $mobileWithCode[1];

      }
      else{
        return 'not_verified';
      }
    }

    public function deleteBannerImage(Request $request)
    {
        $id = $request->banner_id;
        $row  = SpBannerImages::whereId($id)->first();

        if($row){
           unlink('public/sp_uploads/banner_images/'.$row->banner_path);
           $row->delete();
           return 'success';
        }
    }

    // beuticsChat
    public function beuticsChat()
    {
        
        $title = __('messages.Beutics Chat');

        $user_data = spLoginData();
        $user_id=$user_data->id;

        $spuser_row = SpUser::where('id',$user_id)->first()->toArray();

        if(!empty($spuser_row)){

            $user_table="sp_users";

            $chatlist =  DB::select("SELECT b1.status as booking_status, c.*, (Select count(*) from chat WHERE deleted_by != '".$user_id."' AND group_id =c.group_id AND other_user_id =c.other_user_id AND user_id =c.user_id AND user_id !='".$user_id."' AND ( read_status='0' OR read_status='-1') ) as unread_message, (CASE WHEN (c.user_id_tbl='users') THEN u1.`name`  WHEN c.user_id_tbl='sp_users' THEN (SELECT store_name from sp_users WHERE id=c.user_id ) ELSE u1.`name` END ) AS user_name, (CASE WHEN c.user_id_tbl='users' THEN (SELECT image from users WHERE id=c.user_id )  WHEN c.user_id_tbl='sp_users' THEN (SELECT profile_image from sp_users WHERE id=c.user_id ) ELSE '' END ) AS user_image,  c.other_user_id_tbl as other_user_table, (CASE WHEN c.other_user_id_tbl='users' THEN (SELECT name from users WHERE id=c.other_user_id ) WHEN c.other_user_id_tbl='sp_users' THEN (SELECT store_name from sp_users WHERE id=c.other_user_id ) ELSE (SELECT name from admins WHERE id=c.other_user_id ) END ) as other_user_name, (CASE WHEN c.other_user_id_tbl='users' THEN (SELECT image from users WHERE id=c.other_user_id ) WHEN c.other_user_id_tbl='sp_users' THEN (SELECT profile_image from sp_users WHERE id=c.other_user_id ) ELSE 0 END ) as other_user_image FROM  `conversations` c LEFT JOIN users u1 ON c.user_id=u1.id INNER JOIN bookings b1 ON c.booking_id=b1.id WHERE ( c.other_user_id_tbl != 'admins' AND c.user_id_tbl != 'admins' ) AND ((c.user_id ='".$user_id."' AND c.user_id_tbl='sp_users')  OR (c.other_user_id ='".$user_id."' AND c.other_user_id_tbl='sp_users')) order by c.updated_at desc");

            // $adminChatList = DB::select("SELECT c.*, (Select count(*) from chat WHERE deleted_by != '".$user_id."' AND group_id =c.group_id AND other_user_id =c.other_user_id AND user_id =c.user_id AND user_id !='".$user_id."' AND ( read_status='0' OR read_status='-1') ) as unread_message, u1.name AS user_name, u2.name AS other_user_name FROM `conversations` c LEFT JOIN sp_users u1 ON c.user_id=u1.id LEFT JOIN admins u2 ON c.other_user_id=u2.id WHERE ( c.other_user_id_tbl = 'admins' OR c.user_id_tbl = 'admins' ) AND  ( c.deleted_by != '".$user_id."' AND c.user_id_tbl='sp_users' ) AND ((c.user_id ='".$user_id."' AND c.user_id_tbl='sp_users') OR (c.other_user_id ='".$user_id."' AND c.other_user_id_tbl='sp_users')) order by c.updated_at desc ");

            $adminChatList = DB::select("SELECT c.*, (Select count(*) from chat WHERE deleted_by != '".$user_id."' AND group_id =c.group_id AND other_user_id =c.other_user_id AND user_id =c.user_id AND user_id !='".$user_id."' AND ( read_status='0' OR read_status='-1') ) as unread_message, u1.name AS user_name, u2.name AS other_user_name FROM `conversations` c LEFT JOIN sp_users u1 ON c.user_id=u1.id LEFT JOIN admins u2 ON c.other_user_id=u2.id WHERE ( c.other_user_id_tbl = 'admins' OR c.user_id_tbl = 'admins' ) AND  ( c.deleted_by != '".$user_id."') AND ((c.user_id ='".$user_id."' AND c.user_id_tbl='sp_users') OR (c.other_user_id ='".$user_id."' AND c.other_user_id_tbl='sp_users')) order by c.updated_at desc ");

            $chats = [];
            if(!empty($adminChatList)){
                $chats = Chat::where("group_id",$adminChatList[0]->group_id)->get()->toArray();    
            }

            $user['id']          = $user_id;
            $user['image']       = (!empty($spuser_row['profile_image']))?url('/public/sp_uploads/profile/'.$spuser_row['profile_image']):'https://ptetutorials.com/images/user-profile.png';;
            $user['full_name']   = ucfirst($spuser_row['store_name']);

            // dd($chats);

            $breadcum = [$title=>route('spuser.beutics-chat')];
            return view('spadmin.users.beuticsChat',compact('title','breadcum','chatlist','adminChatList','chats','user'));

        }

    }

    public function getChat(Request $request) {

        $user_data = spLoginData();
        $user_id=$user_data->id;
        $booking_id = $request['booking_id'];
        $other_id = $request['other_id'];
        $other_tabel = $request['other_tabel'];

        $chats = [];
        $groupId = ($other_id>$user_id)?$other_id."".$user_id."".$booking_id:$user_id."".$other_id."".$booking_id;
        $prefix = ($other_tabel=='admins') ? 'AS-':'SU-';
        $groupId =$prefix.$groupId;
        //$groupId ='AS-'.$groupId;
        


        
        Chat::where('other_user_id',$user_id)->where('other_user_id_tbl','sp_users')->update(['read_status'=>'1']);

        if($request['booking_id']>0)
            $isOnline = User::find($request['other_id'])->is_online;
        else
            $isOnline = Admin::find($request['other_id'])->is_online;

        $chats = Chat::where("group_id",$groupId)->get()->toArray();   
       
        if(count($chats)>0){
            return ['status' => true, 'message' => "Chat data.","data"=>$chats,"is_online"=>$isOnline];
        }else{
            return ['status' => false, 'message' => "Chat data.","data"=>$chats,"is_online"=>$isOnline];
        }

    }

    public function getStateList(Request $request)
    {
        $inputtype = 'state';
        $data = $this->getmapcountry([$request->country_id,'states']);

        if(empty($data))
        {
            $inputtype = 'city';
            $data = $this->getmapcountry([$request->country_id,'cities']);
        }

        if(empty($data))
        {
            $inputtype = 'country';
            $data = $this->getmapcountry();
        }

        $json_data = array(
            "inputtype" => $inputtype,
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function getCityList(Request $request)
    {
        $inputtype = 'city';
        $data = $this->getmapcountry([$request->country_id,'states',$request->state_id,'cities']);

        if(empty($data))
        {
            $data = $this->getmapcountry([$request->country_id,'cities']);
        }
        
        $json_data = array(
            "data" => $data
        );
        echo json_encode($json_data);
    }

    public function getmapcountry($parami=[])
    {
        $ponmo = implode('/',$parami);
        $url = "https://api.countrystatecity.in/v1/countries/$ponmo"; //echo $url;
        $res = $this->get_address_content($url);
        return json_decode($res);
    }

    function get_address_content($URL)
    {
        $headers = ['X-CSCAPI-KEY: MDI5WjFSYjVZWTdlemJ5WlJrZWlMSjV5VERaM3BlcmVseUV3THRqag=='];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL,$URL);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
}
