<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct(Request $request) {
        
        $lang = 'en';
        $header_lang = $request->header('language');
        if(isset($header_lang) && $header_lang == 'ar'){
            $lang = 'ar';   
        }
        App::setLocale($lang);
    }

        
    

    function validationHandle($validation)
    {
        foreach ($validation->getMessages() as $field_name => $messages){
            if(!isset($firstError)){

                $firstError =$messages[0];
            }
        }
        return $firstError;
    }

    public function getLang()
    {
        $col_postfix = '';
        $nag = app()->getLocale();
       
        if($nag == "ar"){
            $col_postfix = '_ar';
        }
        return $col_postfix;

    } 
}
