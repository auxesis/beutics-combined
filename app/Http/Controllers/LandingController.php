<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

use App\Model\Banner;
use App\Model\Category;
use App\Model\PageSection;
use App\Model\GiftTheme;
use App\Model\Forum;
use App\Model\ForumComment;
use App\Model\CategoryType;
use App\Model\SpUser;
use App\Model\Contact;

class LandingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    protected $category;
     //protected $offer;
    public function __construct(){
       
    }

    /**
     * Show the banners, category, offers, forum , and social media content, specified resource.
     *
     * @param  
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $array = array('Dental','Laser', 'Cosmetic Dermatology','Plastic Surgery','Slimming');
        $pagesectionArr = $featureSectionArr = array();
        $url = \Request::segment(1);
        $typeData = CategoryType::select('id')->where('type_name',ucfirst($url))->first();
        $pageBannerSectionContent = Banner::getBannerData('landing-'.$url);
        $pageSectionContent = PageSection::getPageSectionData('landing-'.$url);
        $subCategory = Category::getsubcategory($typeData->id);
        $ecardThemeContent = GiftTheme::where('is_publish','1')->get();
        $forumContentArr = Forum::getForumData($typeData->id);
        $forumContent = array_chunk($forumContentArr,3);
        $featuredContentArr = SpUser::getStoreData($typeData->id,'1');
        $subCagArr = ($url == 'beauty') ? array('Dental','Laser', 'Cosmetic Dermatology','Plastic Surgery','Slimming') : array('Relaxation Massage','Ayurvedic Treatments','Aromatherapy','Foot Reflexology','Couple Massage','Swedish Massage','Deep Tissue Massage','Hot Stone Massage','Thai Massage','Cellulite Massage','Sports Massage','Four Hands Massage');
        $programsCategory = Category::whereIn('category_name',$subCagArr)->get();
        
        foreach($pageBannerSectionContent as $content)
        {
            $pagesectionArr[$content->page_section_name][] = $content;
        }

        foreach($pageSectionContent as $content)
        {
            $pagesectionArr[$content->page_section_name][] = $content;
        }

        foreach($featuredContentArr as $content)
        {
            $featureSectionArr[$content->store_type][] = $content;
        }
        
        $offerdata=array();
        $forums_data=array();
        $BeautyTopBanner = '';
        /*$offer = $this->category->getoffers($url);
        foreach ($offer as $key => $value) {
        $offerdata[$value->service_type][]= $value;         
        }
*/
        return view('landing/'.$url,compact('pagesectionArr','subCategory','ecardThemeContent','forumContent','offerdata','forums_data','featureSectionArr','programsCategory'));
    }
    
    /**
     * Send contact enquiry
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function sendcontact(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => ['required', 'string','max:255'],
            'email' => ['required','email:filter','max:255'],
            'message' => ['required','string'],
            'phonenumber' => ['required','numeric'],
            'specialization' => ['required']
        ]);

        if($validation->fails()) {
            return response()->json(['code' => 400, 'msg' => $validation->errors()->keys()]);
        }

        $row = new Contact();
        $row['name']        = $request->name;
        $row['email']         = $request->email;
        $row['specialization']= $request->specialization;
        $row['phonenumber']   = $request->phonenumber;
        $row['message']      = $request->message;
        $row->save();

        //mail to admin
        $data['templete'] = "admin_mail";
        $data['email'] = $request->email;
        $data['subject'] = "New Contact Enquiry";
        $data['message'] = "Hi ".$request->name.", Thanks for contacting us, we will get back to you soon.";
        send($data);

        return response()->json(['code' => 200, 'msg' => 'Thanks for contacting us, we will get back to you soon.']);
    }
}
