<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use JWTAuth;
use JWTAuthException;

use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\SpUser;
use App\Model\OtpNumber;
use App\Model\Country;
use App\Model\SpDevice;
use App\Model\Amenity;
use App\Model\SpAmenity;
use App\Model\Category;
use App\Model\ShopTiming;
use App\Model\SpBannerImages;
use App\Model\Tier;
use App\Model\Setting;
use App\Model\Conversation;
use App\Model\Chat;
use Config;

use App\lib\PushNotification;

class ChatController extends Controller
{
	function __construct()
	{

	}


	protected function sendNotification(Request $request)
    {

        $msg = json_encode($_REQUEST);
        
        $notificationMsg = htmlspecialchars(urldecode(@$_REQUEST['message']));
        
        // @mail("mahaveer@ninehertzindia.com","My subject mm 1 ",json_encode($_REQUEST));

        $userId = (isset($_REQUEST['user_id']))?$_REQUEST['user_id']:'';
        $user_name = (isset($_REQUEST['user_name']))?$_REQUEST['user_name']:'';
        $user_table = (isset($_REQUEST['user_table']))?$_REQUEST['user_table']:'';

        $other_user_id = (isset($_REQUEST['other_user_id']))?$_REQUEST['other_user_id']:'';
        $booking_id = (isset($_REQUEST['booking_id']))?$_REQUEST['booking_id']:'';

        $group_id = (isset($_REQUEST['group_id']))?$_REQUEST['group_id']:'';

        $chat_id = (isset($_REQUEST['chat_id']))?$_REQUEST['chat_id']:'';

        $current_user_type = (isset($_REQUEST['current_user_type']))?$_REQUEST['current_user_type']:'';

        $chatdatanew =  Chat::where('id',$chat_id)->first();

        $chatdatanew =(!empty($chatdatanew))?$chatdatanew->toArray():[];        

        if($user_table=="sp_users")
        {   
            $user_data = SpUser::where('id',$other_user_id)->first()->toArray();

        }else{

            $user_data = User::where('id',$other_user_id)->first()->toArray();
        }

        $chatdatanew['user_data'] = $user_data;

        // dd($chatdatanew);

        if(!empty($userId))
        {
            // @mail("nidhi123@yopmail.com","My subject mm 2 ",json_encode($_REQUEST));

        	PushNotification::Notify($userId, "", $chatdatanew, 'Chat', $notificationMsg,"", 'Chat',"", $current_user_type, $chatdatanew);

        }

    }
	

}
  