<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\Paginator;
use JWTAuth;
use JWTAuthException;

use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Category;
use App\Model\QuickFact;
use App\Model\Forum;
use App\Model\ForumBannerImage;
use App\Model\ForumCategory;
use App\Model\ForumComment;
use App\Model\ForumCommentLikeDislike;
use App\Model\CustomerContactSync;
use App\Model\Service;
use App\Model\SpService;
use App\Model\SpUser;
use App\Model\Bookmark;
use App\Model\Review;
use App\Model\SpOffer;
use App\Model\Tier;
use App\Model\RatingReview;
use App\Model\SpectacularOfferType;
use App\Model\SpectacularOfferLike;
use App\Model\Booking;
use App\Model\CashbackHistory;
use App\Model\Notification;
use App\Model\PromoCode;
use App\Model\PromoCodeLinkedCustomer;
use App\Model\Setting;
use App\Model\SpTags;
use App\Model\SpGallery;
use App\Model\Staff;
use App\Model\StaffImagesVideos;
use DB;

class UserPagesController extends Controller
{
	public function __construct(Request $request) {
 	 parent::__construct($request);
    	
    	$this->col_postfix = '';

        $this->header = $request->header('language');
        if($this->header!= "en"){
        	$this->col_postfix = '_ar';
        }
    }

	public function getMainCategories()
	{
		try
	    {
	    	$rows = Category::where('category_name','!=','bridal')->where('parent_id','0')->get();
	    	if($rows){
		    	foreach($rows as $row)
				{
					$col = 'category_name'.$this->col_postfix;
					$categories[] = [
						'id'=>$row->id,
						'category_name'=>$row->$col,
						];
				}
				return response()->json(['status' => true, 'message' => 'Categories List','data'=>$categories]);
			}else{
				return response()->json(['status' => true, 'message' => 'No Record Found','data'=>[]]);
			}
	    }
	    catch(\Exception $e){
	       	return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	    }
	}

	public function getSubCategories()
	{
		try
	    {
	    	$rows = Category::where('category_name','!=','bridal')->where('parent_id','!=','0')->get();
	    	if($rows){
		    	foreach($rows as $row)
				{
					$categories[] = [
						'id'=>$row->id,
						'category_name'=>$row->category_name,
						];
				}
				return response()->json(['status' => true, 'message' => 'Sub Categories List','data'=>$categories]);
			}else{
				return response()->json(['status' => true, 'message' => 'No Record Found','data'=>[]]);
			}
	    }
	    catch(\Exception $e){
	       	return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	    }
	}
	
	public function getQuickFactFromAdmin(Request $request)
	{

		if (Input::isMethod('post')) {
	       try
	       {
	       		
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'category_id' => 'required',
		              'language' => 'required',
		              'page_no' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {  	

		          	$currentpage = $request->page_no;

	          		Paginator::currentPageResolver(function () use ($currentpage) {
				        return $currentpage;
				    });

		          	$row = QuickFact::where('category_id',$request->category_id)->orderBy('id','DESC')->paginate(10);
		          	$rows_arr = $row->toArray();

		          	$final_data = array();
		          	$title_ar = 'title'.$this->col_postfix;
		          	$description_ar = 'description'.$this->col_postfix;
		          	$service_ar = 'name'.$this->col_postfix;
		          	foreach($row as $rows)
		          	{
		          		
		          		$data_1 = [
		          			'id' => $rows->id,
		          			'category_id' => $rows->category_id,
		          			'subcategory_id' => $rows->subcategory_id,
		          			// 'service_id' => $rows->service_id,
		          			'title' => $rows->$title_ar,
		          			'description' => $rows->$description_ar,
		          			'image' => changeImageUrl(asset('sp_uploads/quick_facts/'.$rows->image)),
		          		];

		          		$service = [
		          				'id' => $rows->service_id,
		          				'name' => $rows->getAssociatedServiceName->$service_ar,
		          			];

		          		$data_2 = array();
		          		foreach($rows->getAssociatedBannerImages as $img)
		          		{
		          			$data_2[] = [
		          				'id' => $img->id,
		          				'banner_path' => changeImageUrl(asset('sp_uploads/quick_facts/'.$img->banner_path)),
		          			];
		          		}
		          		$data_1['service']  = $service;
		          		$data_1['banner_images']  = $data_2;
		          		$final_data[] = $data_1;
		          	}

		          	$result['data'] = $final_data;
	                $result['current_page']     = $rows_arr['current_page'];
	                $result['from']             = $rows_arr['from'];
	                $result['last_page']        = $rows_arr['last_page'];
	                $result['next_page_url']    = $rows_arr['next_page_url'];
	                $result['per_page']         = $rows_arr['per_page'];
	                $result['prev_page_url']    = $rows_arr['prev_page_url'];
	                $result['to']               = $rows_arr['to'];
	                $result['total']            = $rows_arr['total'];

		          	if($row)
		          	{
		          		
					    return response()->json(['status' => true, 'message' => 'Quick Facts','data' => $result]);	
						
		          	}else{
		          		return response()->json(['status' => false, 'message' => 'Quick Fact not found']);	
		          	}
		          	
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function askExpert(Request $request)
    {

    	if (Input::isMethod('post')) {
    		try{
    			$validator = Validator::make($request->all(), [
                    'category_id'=>'required',
                    'subject'=>'required',
                    'message'=>'required',
                    'language' => 'required',
                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{
		        	
		        	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	        		$row = new Forum();

	        		$row['user_id'] = $user_id;
	        		if($request->is_open_forum){
	        			$row['is_open_forum'] = $request->is_open_forum;
	        		}
	        		
	        		$row['subject'] = $request->subject;
	        		$row['message'] = $request->message;

	        		if($request->isNotify){
	        			$row['isNotify'] = $request->isNotify;
	        		}
	        		$row['is_message_sent'] = '0';
	        		$row['status'] = '0';
	        		if($row->save())
	        		{
	        			$cat_arr = explode(',',$request->category_id);
	        			foreach($cat_arr as $cats){
	        				$rows = new ForumCategory();
	        				$rows->forum_id = $row->id;
	        				$rows->category_id = $cats;
	        				$rows->save();
	        			}

	        			if($request->bannerimagecount){

		                	$count = $request->bannerimagecount;

		                	for($i=1;$i<= $count;$i++){
		           
		                		$img = 'bannerimages'.$i;

		                		if ($request->$img)
				        		{
				                    $file = $request->$img;
				                    $name = time().str_random(2).'.'.$file->getClientOriginalExtension();
				                    $destinationPath = public_path('sp_uploads/forum/');
				                    $img = $file->move($destinationPath, $name);

				                    $rows = new ForumBannerImage();

				                    $rows->forum_id = $row->id;
				                    $rows->banner_path = $name;
				                    $rows->save();
				                }
		                	}
		                }
	        	
	        			return response()->json(['status' => true, 'message' => __('messages.ask expert')]);
	        		}
		            
		        }
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage().$e->getLine().$e->getFile(), 'data' => []]);
	       }
    	}
    }

    public function getForum(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'category_id' => 'required',
		             // 'ismine' => 'required',
		              'language' => 'required',
		              'page_no' => 'required',
		              'user_type' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {  

		          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

	          		$category_query = ForumCategory::where('category_id',$request->category_id)->get();
	          		$forum_arr = array();
	          		foreach($category_query as $cat_rows){
						$forum_arr[] = $cat_rows['forum_id'];
	          		}
	          		$currentpage = $request->page_no;

	          		Paginator::currentPageResolver(function () use ($currentpage) {
				        return $currentpage;
				    });

		            if($request->ismine == 1)	{
		            	$row = Forum::whereIn('id',$forum_arr)->where('user_id',$user_id)->orderBy('id','DESC')->paginate(10);
		            }else{
		            	$row = Forum::whereIn('id',$forum_arr)->orderBy('id','DESC')->paginate(10);
		            }

		             $rows_arr = $row->toArray();
		          	
		          	$final_data = array();
		          	foreach($row as $rows)
		          	{
		          		$data_1 = [
		          			'id' => $rows->id,
		          			'user_id' => $rows->user_id,
		          			'user_name' => isset($rows->getAssociatedUserInformation->name) ? $rows->getAssociatedUserInformation->name:'',
		          			'is_open_forum' => $rows->is_open_forum,
		          			'subject' => $rows->subject,
		          			'message' => $rows->message,
		          			'isNotify' => $rows->isNotify,
		          			'created_at' => date('d-M-Y H:i:s',strtotime($rows->created_at)),
		          		];

		          		$data_2 = array();
		          		foreach($rows->getAssociatedForumBannerImages as $img)
		          		{
		          			$data_2[] = [
		          				'id' => $img->id,
		          				'banner_path' => asset('sp_uploads/forum/'.$img->banner_path),
		          			];
		          		}
		          		$is_commented = 0;
		          		$data_1['is_commented'] = 0;
						if($rows->getAssociatedForumComments){
							 foreach($rows->getAssociatedForumComments as $comments){
							 	if($request->user_type == 'customer')
							 	{
							 		if($comments['user_id'] == $user_id){
							 			$data_1['is_commented'] = 1;
							 		}
							 		
							 	}else{
							 		if($comments['sp_id'] == $user_id){
							 			$data_1['is_commented'] = 1;
							 		}
							 	}
			 				 }
						}
		 
						$data_1['total_comments'] = count($rows->getAssociatedForumComments);
		          		$data_1['banner_images'] = $data_2;
		          		$final_data[] = $data_1;
		          	}

		          	$result['data'] = $final_data;
	                $result['current_page']     = $rows_arr['current_page'];
	                $result['from']             = $rows_arr['from'];
	                $result['last_page']        = $rows_arr['last_page'];
	                $result['next_page_url']    = $rows_arr['next_page_url'];
	                $result['per_page']         = $rows_arr['per_page'];
	                $result['prev_page_url']    = $rows_arr['prev_page_url'];
	                $result['to']               = $rows_arr['to'];
	                $result['total']            = $rows_arr['total'];

		          	if($row)
		          	{
		          		
					    return response()->json(['status' => true, 'message' => 'Forum','data' => $result]);	
						
		          	}else{
		          		return response()->json(['status' => false, 'message' => 'Forum not found']);	
		          	}
		          	
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function addForumComment(Request $request)
	{
		if (Input::isMethod('post')) {
    		try{
    			$validator = Validator::make($request->all(), [
                    // 'user_id'=>'required',
                    'forum_id'=>'required',
                    'message'=>'required',
                    'user_type' => 'required',
                    'language' => 'required',
                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{
		        	
		        	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
		        	
		        	$check_isNotify = Forum::where('id',$request->forum_id)->first();

		        	$user = User::where('id',$check_isNotify->user_id)->first();

	        		$row = new ForumComment();

	        		$row['forum_id'] = $request->forum_id;
	        		$row['message'] = $request->message;

	        		if($request->user_type == 'customer'){

	        			$row['user_id'] = $user_id;

	        			$commented_user_name = User::where('id',$user_id)->where('id','!=',$check_isNotify['user_id'])->select('name')->first();

	        		}

	        		if($request->user_type == 'serviceprovider'){

	        			$row['sp_id'] = $user_id;
	        		}
	        		
	                $row->save();
	                $data['comment_id'] = $row->id;
	        		if($row->save())
	        		{
	        			if($check_isNotify['isNotify'] == '1') 
	        			{
	        				if($user->email!='' && $user->verifyToken ==''){
	        					$edata['templete'] = "forum_notification";
								$edata['name'] = $user->name;
								$edata['email'] = $user->email;
								$edata['subject'] = $check_isNotify->subject;
								$edata['commented_user_name'] = $commented_user_name['name'];
								$edata['subject'] = "Your query is firing up!";
								send($edata);
	        				}
	        				
	        			}
	        			if($user->notification_alert_status == '1'){

		      				// $message = 'Hi '.$user->name.', You have received a new response to your query posted on '.$check_isNotify->subject.' '.date('Y-m-d',strtotime($row['created_at'])).'. Take a look…';

						    $message_title_main = __('messages.New Advice!');
			          		$message_main =  __('messages.New Advice',['name' => $user->name,'subject' => $check_isNotify->subject,'date' => date('Y-m-d',strtotime($row['created_at']))]);

			          		app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');


							$message_title_other = __('messages.New Advice!');
			          		$message_other =  __('messages.New Advice',['name' => $user->name,'subject' => $check_isNotify->subject,'date' => date('Y-m-d',strtotime($row['created_at']))]);
			          		
				          	Notification::saveNotification($user->id,$row->user_id,$check_isNotify->id,'FORUM_RESPONSE',$message_main,$message_other,$message_title_main,$message_title_other,'customer');

		      			}
		      			app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

	        			return response()->json(['status' => true, 'message' => __('messages.forum comment'), 'data'=>$data]);
	        		}
		            
		        }
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
    	}
	}

	public function getForumComment(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'forum_id' => 'required',
		              'user_type' => 'required',
		              'language' => 'required',
		              'page' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {  

		          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

		          	$field_check = 'user_id';
		          	if($request->user_type == 'serviceprovider'){
		          		$field_check = 'sp_id';
		          	}
		          	$user_comment_ids = ForumComment::where([['forum_id',$request->forum_id], [$field_check,$user_id]])->pluck('id')->toArray();

		          	$comments = ForumComment::where([['forum_id',$request->forum_id]]);
		          	
		          	if(!empty($user_comment_ids)){
		          		
	          			$comments = $comments->orderByRaw(DB::raw("FIELD(forum_comments.id, ".  implode(',', $user_comment_ids).") DESC"), 'forum_comments.id');

		          	}
		          	$currentpage = $request->page;

	          		Paginator::currentPageResolver(function () use ($currentpage) {
				        return $currentpage;
				    });
		          	$row = $comments->paginate(10);
		          	
		            $rows_arr = $row->toArray();
		          	
		          	$final_data = array();
		          	
		          	foreach($row as $rows)
		          	{
		          		$data = array();
		          		$data['id'] = $rows->id;
		          		$data['forum_id'] = $rows->forum_id;

		          		if($rows->user_id!=null && $rows->user_id!=''){
		          			$role = 'user_id';
		          			$data['user_type'] = 'customer';
		          		}
		          		else{
		          			$role = 'sp_id';
		          			$data['user_type'] = 'serviceprovider';
		          		}
		          		if($data['user_type'] == 'customer'){
		          			$data['name'] = $rows->getAssociatedForumCustomer->name;
		          			if($rows->getAssociatedForumCustomer->image!=''){
		          				$data['image'] = asset($rows->getAssociatedForumCustomer->image);
		          			}else{
		          				$data['image'] = '';
		          			}
		          			

		          		}else{
		          			$data['name'] = $rows->getAssociatedForumServiceProvider->name;
		          			$data['store_name'] = $rows->getAssociatedForumServiceProvider->store_name;

		          			if($rows->getAssociatedForumServiceProvider->profile_image!=''){
		          				$data['image'] = changeImageUrlForFileExist(asset('sp_uploads/profile/'.$rows->getAssociatedForumServiceProvider->profile_image));
		          			}else{
		          				$data['image'] = '';
		          			}

		          			if($rows->getAssociatedCommentBannerImages){
		          				$data['banner_images'] = array();
		          				foreach($rows->getAssociatedCommentBannerImages as $bimage){
		          					$data['banner_images'][] = asset('sp_uploads/forum_reply_banners/'.$bimage->banner_path);
		          				}
		          			}
		          			
		          		}
		          		$data['like'] = '';
		          		$data['dislike'] = '';
		          		$data['isLike'] = '';
		          		$like_count = 0;
		          		$dislike_count = 0;
		          		foreach($rows->getAssociatedLikeDislike as $ld){
		          			if($ld['user_id'] == $user_id){
	          					if($ld['isLike'] == 1){
	          						$data['isLike'] = 1;
	          					}else{
	          						$data['isLike'] = 0;
	          					}
	          				}

		          			if($ld['isLike'] == 1){
		          				$like_count = $like_count+1;
		          				$data['like'] = $like_count;
		          			}
		          			if($ld['isLike'] == 0){
		          				$dislike_count = $dislike_count+1;
		          				$data['dislike'] = $dislike_count;
		          			}
		          		}
		          		
		          		
		          		$data[$role] = $rows->$role;

		          		$data['message'] = $rows->message;
		          		$data['created_at'] = date('d-M-Y H:i:s',strtotime($rows->created_at));
		         
		          		
		          		$final_data[] = $data;
		          	}

		          	$result['data'] = $final_data;
	                $result['current_page']     = $rows_arr['current_page'];
	                $result['from']             = $rows_arr['from'];
	                $result['last_page']        = $rows_arr['last_page'];
	                $result['next_page_url']    = $rows_arr['next_page_url'];
	                $result['per_page']         = $rows_arr['per_page'];
	                $result['prev_page_url']    = $rows_arr['prev_page_url'];
	                $result['to']               = $rows_arr['to'];
	                $result['total']            = $rows_arr['total'];
	
		          	if($row)
		          	{
		          		
					    return response()->json(['status' => true, 'message' => 'Forum','data' => $result]);	
						
		          	}else{
		          		return response()->json(['status' => false, 'message' => 'Forum not found']);	
		          	}
		          	
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function commentLikeDislike(Request $request)
	{
		if (Input::isMethod('post')) {
    		try{
    			$validator = Validator::make($request->all(), [
                    // 'user_id'=>'required',
                    'comment_id'=>'required',
                    'isLike'=>'required',
                    'user_type' => 'required',
                    'language' => 'required',
                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{
		        	
		        	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

		        	$check_field = '';

		        	if($request->user_type == 'customer'){
		        		$check_field = 'user_id';
		        	}else{
		        		$check_field = 'sp_id';
		        	}
		        	$check_row = ForumCommentLikeDislike::where([['comment_id',$request->comment_id],[$check_field,$user_id]])->count();
		        	
		        	if($check_row > 0){
		        		$row = ForumCommentLikeDislike::where([['comment_id',$request->comment_id],[$check_field,$user_id]])->first();

		        	}else{
		        		$row = new ForumCommentLikeDislike();
		        	}

	        		$row['comment_id'] = $request->comment_id;
	        		$row['isLike'] = $request->isLike;

	        		if($request->user_type == 'customer'){

	        			$row['user_id'] = $user_id;
	        		}

	        		if($request->user_type == 'serviceprovider'){

	        			$row['sp_id'] = $user_id;
	        		}
	        
	                $row->save();
	        		if($row->save())
	        		{ 
	        			$new_row = ForumCommentLikeDislike::where('comment_id',$request->comment_id)->get();

	        			$data['like'] = '';
		          		$data['dislike'] = '';
		          		$like_count = 0;
		          		$dislike_count = 0;

	        			foreach($new_row as $nrow){
	        				if($nrow['isLike'] == 1){
		          				$like_count = $like_count+1;
		          				$data['like'] = $like_count;
		          			}
		          			if($nrow['isLike'] == 0){
		          				$dislike_count = $dislike_count+1;
		          				$data['dislike'] = $dislike_count;
		          			}
	        			}
	        			return response()->json(['status' => true,'data'=>$data]);
	        		}
		            
		        }
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
    	}
	}

	public function contactSync(Request $request)
	{
		if (Input::isMethod('post')) {
    		try{
    			$validator = Validator::make($request->all(), [
                    'contacts'=>'required',
                    'language' => 'required',
                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{
		        	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

		        	$crow = CustomerContactSync::where('user_id',$user_id)->count();
		        	if($crow>0){
		        		
		        		CustomerContactSync::where('user_id',$user_id)->delete();
		        	}else{
		        		$check = CashbackHistory::where('user_id',$user_id)->where('type','3')->first();
		        		if(!$check){
			        		$setting = Setting::first();
			        		$cashback_amt = $setting->contact_sync_cashback;
			        		$cashback = new CashbackHistory();
			                $cashback->user_id = $user_id;
			                $cashback->type = '3';
			                $cashback->amount = $cashback_amt;
			                $cashback->save();

			                $update_user = User::where('id',$user_id)->first();
			                $total_earning = $update_user->earning+$cashback_amt;
			                $update_user->earning = $total_earning;
			                $update_user->save();
			               }

		        	}
		        	
	        		$contact_arr = json_decode($request->contacts,true);

	        		foreach($contact_arr as $rows){
	        			$contact_row = new CustomerContactSync();
	        			$contact_row->user_id = $user_id;
	        			$contact_row->name = $rows['name'];
	        			$contact_row->phone_no = preg_replace('/[^0-9]/', '', $rows['number']);
	        			$contact_row->save();
	        		}

	        		return response()->json(['status' => true, 'message' => __('messages.contact sync')]);	
		            
		        }
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getLine(), 'data' => []]);
	       }
    	}
	}

	public function getSubcategoryWithServices(Request $request)
	{
		if (Input::isMethod('post')) {
    		try{
    			$validator = Validator::make($request->all(), [
                    'category_id'=>'required',
                    'gender'=>'required|in:men,women,kids',
                    'language' => 'required',
                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{

		        	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
		        	if($request->category_id!='home'){
		        		$services = \App\Model\SpService::whereHas('getAssociatedService', function ($query) use ($request) {
    						$query->where('cat_id', $request->category_id);
						})->whereHas('getRelatedPrices', function ($query) use ($request) {
    						$query->where('field_key', 'like', strtoupper($request->gender).'%');
						})->get();
		        	}else{

		        		// $home_key = strtoupper($request->gender).'_HOME';
		        		
		        		$services = \App\Model\SpService::whereHas('getRelatedPrices', function ($query) use ($request) {
    						$query->where('field_key', 'like', strtoupper($request->gender).'_HOME%');
						})->get();

		        	}
		        	
		        	
		        	$service_ids = array();
		        	foreach($services as $rows){
		        		$service_ids[] = $rows['service_id'];
		        	}
		
		        	$service_rows = Service::whereIn('id',array_unique($service_ids))->get();
		        	$subcat_arr = array();
					$service_arr = array();
					$cat_name = 'category_name'.$this->col_postfix;
					$sr_name = 'name'.$this->col_postfix;

		        	foreach($service_rows as $sr_rows){
		        		if(!isset($subcat_arr[$sr_rows['subcat_id']])){

		        			
	        				$subcat_arr[$sr_rows['subcat_id']] = [
			        			'id'=>$sr_rows->getSubcat->id,
			        			'name'=>$sr_rows->getSubcat->$cat_name,
			        			'icon'=>$sr_rows->getSubcat->icon!='' ? changeImageUrl(asset('sp_uploads/sub_categories/'.$sr_rows->getSubcat->icon)) : "",
		        			];
		        		}
		        		unset($sr_rows->getSubcat);
		        		$service_arr[$sr_rows['subcat_id']][] = [
		        			'id'=>$sr_rows->id,
		        			'name'=>$sr_rows->$sr_name,
		        		];
		        	}
		        	$result_arr = [];
		        	foreach($subcat_arr as $sub_cat_id => $sub_cate_row) {

		        		$row_current = $sub_cate_row;
						$cat_services = isset($service_arr[$sub_cat_id]) ? $service_arr[$sub_cat_id] : [];
						 $row_current['service'] = $cat_services;

						$result_arr[] = $row_current;
		        	}

		        	
		        	return response()->json(['status' => true, 'message' => '', 'data' => $result_arr]);
		        }
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
    	}
	}

	public function addBookmark(Request $request)
	{
		if (Input::isMethod('post')) {
    		try{
    			$validator = Validator::make($request->all(), [
                    'service_provider_id'=>'required',
                    'language'=>'required',
                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{
		        	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
		        	$status = "";
		        	$row = Bookmark::where('user_id',$user_id)->where('sp_id',$request->service_provider_id)->first();
		        	if($row){
		        		$status = "delete";
		        		$row->delete();
		        	}else{
		        		$status = "add";
		        		$bookmark = new Bookmark();
		        		$bookmark->user_id = $user_id;
		        		$bookmark->sp_id = $request->service_provider_id;
		        		$bookmark->save();

		        	}
		        	if($status == "add"){
		        		return response()->json(['status' => true, 'message' => __('messages.bookmark add')]);	
		        	}else{
		        		return response()->json(['status' => true, 'message' => __('messages.bookmark remove')]);	
		        	}
		        			            
		        }
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
    	}
	}

	public function showBookmark(Request $request)
	{
		if (Input::isMethod('post')) {
    		try{
    			$validator = Validator::make($request->all(), [
                    'language'=>'required',
                    'page_no'=>'required',
                    'latitude'=>'required',
                    'longitude'=>'required',
                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{
		        	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
		        	// echo $user_id;die;
		        	$reviews_ques_arr = \Config::get('constants.review_questions_arr');
        			$rating_arr = \Config::get('constants.rating_arr');

        			$currentpage = $request->page_no;

	          		Paginator::currentPageResolver(function () use ($currentpage) {
				        return $currentpage;
				    });

		        	$row = Bookmark::where('user_id',$user_id)->orderBy('id','DESC')->paginate(10);
		        	$rows_arr = $row->toArray();
		        	if($row){
		        		$sp_details = array();
		        		$overall_rating=0;
		        		$reviews_count = 0;
		        		$distanceQuery = "ROUND( 6371 * acos( cos( radians(" . $request->latitude . ") ) * cos( radians(sp_users.latitude) ) * cos( radians(sp_users.longitude) - radians(" . $request->longitude . ") ) + sin( radians(" . $request->latitude . ") ) * sin( radians(sp_users.latitude) ) ) ,1) AS distance";

		        		foreach($row as $data){

		        			$sp_row = SpUser::where('id',$data['sp_id'])->where('status','1')->where('is_approved','1')->select('*',DB::raw($distanceQuery))->first();
		        			if($sp_row)
		        			{
		        				$final_rating = array();
		        				// $distance = $this->distance($request->latitude, $request->longitude, $sp_row->latitude, $sp_row->longitude, 'A'); 
		        				$distance = $sp_row->distance; 
			        			if(count($sp_row->getAssociatedReviews)>0)
			        			{
			        				$reviews_count = count($sp_row->getAssociatedReviews);

				        			foreach($sp_row->getAssociatedReviews as $reviews)
				        			{
				        				$rating_calc = array();
						        		foreach($reviews->getAssociatedReviewQuestions as $review_ques)
						        		{
						        			$ques_value =  $reviews_ques_arr[$review_ques['ques_id']]['value'];
						        			$ans =  $rating_arr[$review_ques['rating']]['value'];

						        			$rating_calc[] = $ques_value*$ans;
						        		}
						        		$final_rating[] = (array_sum($rating_calc))/10;
				        			}
				        			$overall_rating = array_sum($final_rating)/$reviews_count;
			        			}
			        			$image = "";
			        			if($sp_row->profile_image!="" && $sp_row->profile_image!=null){
			        				$image = changeImageUrlForFileExist(asset('sp_uploads/profile/'.$sp_row->profile_image));
			        			}

			        			$customer_mobile_no = Booking::leftJoin('users', function($join) {
							      $join->on('bookings.user_id', '=', 'users.id');
							    })
					          	->where('sp_id',$data['sp_id'])
					          	->select('users.id',DB::raw("CONCAT(users.country_code,users.mobile_no) AS mobile_no"))
					          	->distinct('bookings.user_id')
					          	->pluck('mobile_no');

					          	$friends_visited = CustomerContactSync::whereIn('phone_no',$customer_mobile_no)->where('user_id',$user_id)->count();
					          	$is_contact_synced = 0;
					          	$check_con_sync = CustomerContactSync::where('user_id',$user_id)->count();

					          	if($check_con_sync > 0){
					          		$is_contact_synced = 1;
					          	}
		          	
			        			$sp_details[] = [
			        				'id'=>$sp_row->id,
			        				'store_name'=>$sp_row->store_name,
			        				'image'=>$image,
			        				'friends_visited'=>$friends_visited,
			        				'total_reviews'=>$reviews_count,
			        				'rating'=>round($overall_rating,1),
			        				'distance'=>$distance,
			        				'is_contact_synced'=>$is_contact_synced,
			        			];
		        			}
		        			
		        		}
		        		$result['data'] = $sp_details;
		                $result['current_page']     = $rows_arr['current_page'];
		                $result['from']             = $rows_arr['from'];
		                $result['last_page']        = $rows_arr['last_page'];
		                $result['next_page_url']    = $rows_arr['next_page_url'];
		                $result['per_page']         = $rows_arr['per_page'];
		                $result['prev_page_url']    = $rows_arr['prev_page_url'];
		                $result['to']               = $rows_arr['to'];
		                $result['total']            = $rows_arr['total'];
		        		return response()->json(['status' => true, 'message' => 'Bookmarks','data'=>$result]);

		        	}else{

		        		return response()->json(['status' => true, 'message' => 'No Bookmarks']);
		        	}
		        }
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
    	}
	}


	public function getProviders(Request $request)
	{
		if (Input::isMethod('post')) {

    		try{
    			$validator = Validator::make($request->all(), [
                    'category_id'=>'required',
                    'services'=>'required',
                    'language' => 'required',
                    'gender' => 'required',
                    'lat'=>'required',
                    'long'=>'required',
                    'sort_by'=>'in:PROXIMITY,RATING,COST_L_H,COST_H_L,CASHBACK',
                    'radius'=>'required',
                    'page'=>'required'
                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{
		        	$sr_name = 'name'.$this->col_postfix;
		        	$services = array_map('intval', explode(',', $request->services));
		        	sort($services);
					$c_services = implode(',',$services);

		        	$current_date = date('Y-m-d');
		        	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
		        	$services_arr = explode(',',$request->services);
		        	
		        	if(count($services_arr)==2)
		        	{
		        		$service_arr_count = 1;	
		        	}
		        	else
		        	{
		        		$service_arr_count = floor((count($services_arr)/2))+1;
		        		
		        	}
		        	
		 			

		        	if($request->category_id!='home'){
		        		$gender_key = strtoupper($request->gender).'_ORI';
		        		$service_criteria = 'shop';
		        	}else{
		        		$gender_key = strtoupper($request->gender).'_HOME_ORI';
		        		$service_criteria = 'home';
		        	}
		        	
		        		$latitude = $request->lat;
	        			$longitude = $request->long;
	        			$radius = $request->radius;
						// $userIdFilter = implode(",", $user_ids);		
							
						$base_path = asset('sp_uploads/profile/').'/';

						if($request->sort_by=="COST_L_H"){
							$orderBy = "final_best_price asc";
							$radius  = $request->radius;
						}else if($request->sort_by=="COST_H_L"){
							$orderBy = "final_best_price desc";
							$radius  = $request->radius;
						}else if($request->sort_by=="RATING"){
							$orderBy = "rating desc";
							$radius  = $request->radius;
						}else if($request->sort_by=="PROXIMITY"){
							$orderBy = "distance asc";
							$radius  = $request->radius;
						}else{
							$orderBy = "cashback desc";
							$radius  = $request->radius;
						}
						if(isset($request->start_price) && isset($request->end_price)){
							$range = " and ( final_best_price > ".$request->start_price." AND final_best_price < ".$request->end_price." )";
						}else{
							$range = "";
						}
						if($request->category_id!='home'){
							$gender_best = strtoupper($request->gender).'_BESTPRICE';
							$gender_original = strtoupper($request->gender).'_ORIGINALPRICE';
						}else{
							$gender_best = strtoupper($request->gender).'_HOME_BESTPRICE';
							$gender_original = strtoupper($request->gender).'_HOME_ORIGINALPRICE';
						}
						


						$distanceQuery = "ROUND( 6371 * acos( cos( radians(" . $latitude . ") ) * cos( radians(sp_users.latitude) ) * cos( radians(sp_users.longitude) - radians(" . $longitude . ") ) + sin( radians(" . $latitude . ") ) * sin( radians(sp_users.latitude) ) ) ,1) AS distance";
						// DB::enableQueryLog();
						$page = $request->page;
						$paginate = 10;
						// DB::enableQueryLog();`sp_users`.`is_verified`,
						$ex_query = "select *,round(CAST(earning*IF(ISNULL(discount),final_best_price,(final_best_price-(discount*final_best_price/100)))/100 as decimal),0) as cashback from (select `id`,`store_name`,`is_verified`,IF( image<> '', CONCAT('" . $base_path . "',image), image ) as profile_image, IF(ISNULL(discount),  round(CAST(best_price as decimal),0), LEAST(round(CAST(original_price*discount as decimal),0),round(CAST(best_price as decimal),0))) as final_best_price  , best_price, round(CAST(original_price as decimal),0) as original_price ,OfferId, discount, OfferServices, SpServices,ServiceName,SpServiceCount ,distance , earning,CAST(rating as decimal(10,1)) as rating,review_count   from (select `id`,`store_name`,image,`is_verified`, IF(INSTR (GROUP_CONCAT(`field_key`), '".$gender_best.",".$gender_original."'),SUBSTR(GROUP_CONCAT(bestprice), 1, INSTR (GROUP_CONCAT(bestprice), ',')-1),SUBSTR(GROUP_CONCAT(bestprice), INSTR (GROUP_CONCAT(bestprice), ',')+1)) as best_price,  IF(INSTR (GROUP_CONCAT(`field_key`), '".$gender_best.",".$gender_original."'),SUBSTR(GROUP_CONCAT(bestprice), INSTR (GROUP_CONCAT(bestprice), ',')+1),SUBSTR(GROUP_CONCAT(bestprice), 1, INSTR (GROUP_CONCAT(bestprice), ',')-1)) as original_price ,distance ,earning, ServiceID as SpServices, SpServiceCount,ServiceName from   (select `sp_users`.`id`,GROUP_CONCAT(`sp_services`.`service_id` order by `sp_services`.`service_id`) as ServiceID,Count(*) as SpServiceCount,trim(GROUP_CONCAT(CONCAT(' ',`services`.`".$sr_name."`) order by `services`.`id`)) as ServiceName,  `sp_users`.`store_name`,`sp_users`.`is_verified`,`sp_users`.`profile_image` as image,`field_key`, sum(sp_service_prices.price) as bestprice , ".$distanceQuery." , tiers.earning from `sp_users` left join `sp_services` on `sp_services`.`user_id` = `sp_users`.`id` left join `sp_service_prices` on `sp_service_prices`.`sp_service_id` = `sp_services`.`id`  left join tiers on sp_users.tier_id = tiers.id left join services on services.id = sp_services.service_id where IFNULL(sp_service_prices.price,0)!=0 and `sp_services`.`service_id` in (".$c_services.") and `sp_services`.`is_deleted` != '1' and `field_key` in ( '".$gender_best."', '".$gender_original."') and `sp_users`.`status` = '1' and `sp_users`.`is_approved` = '1'  group by `field_key`, `sp_users`.`id`) as main group by `id`) as best
							LEFT JOIN
							(select OfferId, user_id, MIN(discount) as discount, OfferServices from (select OfferId, user_id, MIN(discount)  as discount, GROUP_CONCAT(ServiceId order by ServiceId) as OfferServices from (SELECT sp_offers.id as OfferId, sp_offers.user_id,(sp_offer_services.quantity - ((sp_offers.discount/100)*sp_offer_services.quantity)) as discount,sp_services.service_id as ServiceId,sp_offer_services.quantity as quantity  FROM `sp_offers` left join `sp_offer_services` on `sp_offers`.`id`= `sp_offer_services`.`sp_offer_id` left join sp_services on sp_offer_services.sp_service_id = sp_services.id where sp_offers.status = '1' and sp_offers.is_deleted != '1' and spectacular_status = '1' and sp_offers.expiry_date >= '".$current_date."' and  sp_offers.service_type = '".$service_criteria."' and gender = '".$request->gender."' order by sp_offers.id,sp_services.id) as offers GROUP BY OfferId HAVING  GROUP_CONCAT(ServiceId order by ServiceId) = '".$c_services."' ORDER by discount desc) as finaloffer group by user_id) as offer
							on best.`id` = offer.`user_id` and best.SpServices = offer.OfferServices
							LEFT JOIN
							(SELECT `sp_id`, (sum(((rating_reviews.question_values * rating_reviews.rating_values))/10)/count(DISTINCT rating_reviews.id)) as rating,count(DISTINCT rating_reviews.id) as review_count from rating_reviews group by rating_reviews.sp_id ) as ratings
							 on best.`id` = ratings.`sp_id` having distance < ".$radius." ".$range." ) as master left JOIN (SELECT If(sp_id,'1','0') as isBookmark,sp_id FROM bookmarks WHERE user_id=".$user_id.") as userbookmarks on userbookmarks.sp_id=master.id LEFT JOIN (SELECT sp_id, count(*) as friends_visited FROM (SELECT  distinct sp_id,user_id FROM `bookings` WHERE user_id in(SELECT id FROM `users` WHERE CONCAT(country_code,mobile_no) in (SELECT phone_no FROM `customer_contact_syncs` where user_id=".$user_id." ))) as fvmain GROUP by sp_id) as fv on fv.sp_id = master.id where SpServiceCount >= ".$service_arr_count."  order by SpServiceCount desc,".$orderBy;

// echo $ex_query;die;
						$users = DB::select($ex_query);
						// $query = DB::getQueryLog();
						// print_r($users);die;
						$offSet = ($page * $paginate) - $paginate;
						$itemsForCurrentPage = array_slice($users, $offSet, $paginate, true);
						$data = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($users), $paginate, $page);
						// $query = DB::getQueryLog();
						 
						$tsp = response()->json(['status' => true, 'message' => 'Service Providers', 'data' => $data]);
						//print_r($tsp->getData()->data->data);
						$os = [];
						$k = 0; 
						$ssss = (array)($tsp->getData()->data);
						$os = $ssss;
						unset($os['data']);
						
						if(!empty($ssss['data']))
						{

							foreach((array)$ssss['data'] as $spdk => $spdinfo)
							{
								$spdinfo->profile_image = changeImageUrlForFileExist($spdinfo->profile_image);
								$os['data'][$k] = ($spdinfo);
								$k++;
							}
						}
						if(empty($os['data']))
						{
							$os['data'] = [];
						}

						$is_contact_synced = 0;
			          	$check_con_sync = CustomerContactSync::where('user_id',$user_id)->count();

			          	if($check_con_sync > 0){
			          		$is_contact_synced = 1;
			          	}


			        	return response()->json(['status' => true, 'message' => 'Service Providers', 'data' => $os,'is_contact_synced'=>$is_contact_synced]);
		       		
		      		
		        }
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage().$e->getLine(), 'data' => []]);
	       }
    	}
	}

	public function getServiceProviderInfoTab(Request $request)
	{
		if (Input::isMethod('post')) {

    		try{
    			$validator = Validator::make($request->all(), [
                    'service_provider_id'=>'required',
                    'latitude'=>'required',
                    'longitude'=>'required',
                    'language' => 'required',
                   
                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{
		        	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

		        	$distanceQuery = "ROUND( 6371 * acos( cos( radians(" . $request->latitude . ") ) * cos( radians(sp_users.latitude) ) * cos( radians(sp_users.longitude) - radians(" . $request->longitude . ") ) + sin( radians(" . $request->latitude . ") ) * sin( radians(sp_users.latitude) ) ) ,1) AS distance";

					$row = SpUser::where('id',$request->service_provider_id)->where('status','1')->where('is_approved','1')->select('*',DB::raw($distanceQuery))->first();
					
					
					$sp_details = array();

					if($row){
						$am_name = 'name'.$this->col_postfix;
						$tier = Tier::where('id',$row->tier_id)->select('title')->first();
						// $distance = $this->distance($request->latitude, $request->longitude, $row->latitude, $row->longitude, 'A'); 

						$distance = $row['distance'];

						//calculate rating---
						$reviews_ques_arr = \Config::get('constants.review_questions_arr');
        				$rating_arr = \Config::get('constants.rating_arr');

						$overall_rating=0;
		        		$reviews_count = 0;

						if(count($row->getAssociatedReviews)>0)
	        			{
	        				$reviews_count = count($row->getAssociatedReviews);

		        			foreach($row->getAssociatedReviews as $reviews)
		        			{
		        				$rating_calc = array();

				        		foreach($reviews->getAssociatedReviewQuestions as $review_ques)
				        		{
				        			$ques_value =  $reviews_ques_arr[$review_ques['ques_id']]['value'];
				        			$ans =  $rating_arr[$review_ques['rating']]['value'];

				        			$rating_calc[] = $ques_value*$ans;
				        		}
				        		$final_rating[] = (array_sum($rating_calc))/10;
		        			}
		        			$overall_rating = array_sum($final_rating)/$reviews_count;
	        			}
	        			//-------------------
	        			$am_array = array();
	        			foreach($row->userAmenities as $am){
	        				$image = "";
	        				if($am->getAmenityName->image!='' && $am->getAmenityName->image!=null){
	        					$image = changeImageUrl(asset('sp_uploads/amenities/'.$am->getAmenityName->image));

	        				}

	        				$am_array[] = [
	        					'name'=>$am->getAmenityName->$am_name,
	        					'image'=>$image,
	        				];
	        			}

	        			$shop_array = array();
	        			foreach($row->userShopTiming as $shop){
	        				$shop_array[] = $shop;
	        			}

	        			$banner_array = array();

	        			foreach($row->userBannerImages as $banner){
	        				$banner_array[] = changeImageUrlForFileExist(asset('sp_uploads/banner_images/'.$banner->banner_path));
	        			}

	        			//calculating no of friends visited
	        			$customer_mobile_no = Booking::leftJoin('users', function($join) {
						      $join->on('bookings.user_id', '=', 'users.id');
						    })
			          	->where('sp_id',$request->service_provider_id)
			          	->select('users.id',DB::raw("CONCAT(users.country_code,users.mobile_no) AS mobile_no"))
			          	->distinct('bookings.user_id')
			          	->pluck('mobile_no');


			          	$friends_visited = CustomerContactSync::whereIn('phone_no',$customer_mobile_no)->where('user_id',$user_id)->count();
			          	
			          	$is_contact_synced = 0;
			          	$check_con_sync = CustomerContactSync::where('user_id',$user_id)->count();

			          	if($check_con_sync > 0){
			          		$is_contact_synced = 1;
			          	}
	        			
	   					$bookmark = Bookmark::where('sp_id',$request->service_provider_id)->where('user_id',$user_id)->first();
	   					$is_bookmark = 0;
	   					if($bookmark){
	   						$is_bookmark = 1;
	   					}

	   					$stateVal = (isset($row->getAssociatedCountryInfo[0]->state_isocode) && $row->getAssociatedCountryInfo[0]->state_isocode !='') ? array('iso2' => $row->getAssociatedCountryInfo[0]->state_isocode,'name'=>$row->getAssociatedCountryInfo[0]->state_name) : null;
	   					$cityVal = (isset($row->getAssociatedCountryInfo[0]->city_isocode) && $row->getAssociatedCountryInfo[0]->city_isocode !=0) ? array('iso2' => $row->getAssociatedCountryInfo[0]->city_isocode,'name'=>$row->getAssociatedCountryInfo[0]->city_name) : null;

	   					$optionArr = array('online' => 'Online', 'store' => 'In-Stores', 'home' => 'At Home');
	   					$delivery_mode = '';
	   					if(isset($row->delivery_mode) && $row->delivery_mode !='')
	   					{
	   						$options = explode(',',$row->delivery_mode);
	   						$option1 = isset($options[0]) ? $optionArr[$options[0]] : '';
	   						$option2 = isset($options[1]) ? $optionArr[$options[1]] : '';
	   						$option3 = isset($options[2]) ? $optionArr[$options[2]] : '';
	   						$optionss = array($option1,$option2,$option3);
	   						$delivery_mode = rtrim(implode(',',$optionss),',');
	   					}

						$sp_details = [
							'id'=>$row->id,
							'store_name'=>$row->store_name,
							'address'=>$row->address,
							'landmark'=>$row->landmark,
							'image'=>$row->profile_image!='' && $row->profile_image!=null ? changeImageUrlForFileExist(asset('sp_uploads/profile/'.$row->profile_image)):'',
							'friends_visited'=>$friends_visited,
							'rating'=>round($overall_rating,1),
							'total_reviews'=>$reviews_count,
							'distance'=>$distance,
							'amenitites'=>$am_array,
							'shop_timings'=>$shop_array,
							'description'=>$row->description,
							'latitude'=>$row->latitude,
							'longitude'=>$row->longitude,
							'banners'=>$banner_array,
							'isBookmark'=>$is_bookmark,
							'tier'=>(isset($tier->title) && $tier->title !=null) ? $tier->title : "",
							'is_contact_synced'=>$is_contact_synced,
							'is_verified'=>$row->is_verified,
							'payment_option'=>explode(',',$row->payment_option),
							'delivery_mode' => (isset($row->delivery_mode) && $row->delivery_mode !='') ? $delivery_mode : null,
							'trail_session' => (isset($row->getAssociatedSessionName->name)) ? array('id'=> $row->trail_session_id , 'name'=>$row->getAssociatedSessionName->name) : null,
							'country' => (isset($row->getAssociatedCountryInfo[0]->country_isocode)) ? array('iso2' => $row->getAssociatedCountryInfo[0]->country_isocode,'name'=>$row->getAssociatedCountryInfo[0]->country_name) : null,
							'state' => $stateVal,
							'city' => $cityVal,
							'sub_locality' => ($row->sub_locality !='') ? $row->sub_locality : null,
							'session_description' => ($row->session_description !='') ? $row->session_description : null,
							'session_sub_description' => ($row->session_sub_description !='') ? $row->session_sub_description : null,
						];

						return response()->json(['status' => true, 'message' => '', 'data' => $sp_details]);
					}else
					{
						return response()->json(['status' => false, 'message' => __('messages.sp deactive')]);
					}

		        	
		        }
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage().$e->getLine(), 'data' => []]);
	       }
    	}
	}

	public function getServiceProviderZoomIns(Request $request)
	{
		if (Input::isMethod('post')) {

    		try{
    			$validator = Validator::make($request->all(), [
                    'service_provider_id'=>'required',
                    'language' => 'required',                   
                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{

		        	$categories  = Category::where('category_name','!=','bridal')->where('parent_id','!=','0')->orderby('category_name', 'asc')->pluck('category_name', 'id')->toArray();

					$row = SpUser::where('id',$request->service_provider_id)->first();
					$sp_details = array();

					if($row){

						$final_array = array();
						$product_array = array();
						$staff_array = array();

						foreach($row->getAssociatedProducts as $products){
							$product_array[] = [
								'id'=>$products->id,
								'product_name'=>$products->product_name,
								'product_image'=>$products->product_image!='' && $products->product_image!=null ? asset('sp_uploads/products/'.$products->product_image):'',
							];
						}

						foreach($row->getAssociatedStaff as $staff){
							$review_row = Review::where('sp_id',$request->service_provider_id)->where('staff_id',$staff->id)->select('staff_like_dislike')->get();
							$sum_staff_like = 0;
							foreach($review_row as $val){
								$sum_staff_like += $val['staff_like_dislike'];
							}

							$multipleImagesVedio = StaffImagesVideos::where('staff_id',$staff->id)->select('id','staff_image','staff_video')->get();

							$staff_images = $staff_videos = $speciality_arr = array();

							foreach($multipleImagesVedio as $data)
							{
								if($data['staff_image'] !='' && $data['staff_image'] !=null)
									$staff_images[] = asset('sp_uploads/staff/'.$data['staff_image']);

								if($data['staff_video'] !='' && $data['staff_video'] !=null)
									$staff_videos[] = asset('sp_uploads/staff/'.$data['staff_video']);
							}

							$arr = explode(",",$staff->speciality);

							foreach($arr as $cat_name_id)
							{
								if($cat_name_id === '0')
								{
									$cat_name_id = 'All Rounders';
								}
								if(array_key_exists($cat_name_id,$categories))
								{
									$speciality_arr[] = $categories[$cat_name_id];
								}
								else
								{
									array_push($speciality_arr,$cat_name_id);
								}
							}
							
							$staff_array[] = [
								'id'=>$staff->id,
								'staff_name'=>$staff->staff_name,
								'experience'=>$staff->experience,
								'speciality'=> implode(',',$speciality_arr),
								'certificates'=>$staff->certificates,
								'nationality'=>($staff->getNationalityInfo['nationality']) ? $staff->getNationalityInfo['nationality'] : $staff->nationality,//$staff->nationality,	
 								'likes'=>$sum_staff_like,
								'description' => $staff->description!='' && $staff->description!=null ? $staff->description : '',
								'description_ar' => $staff->description_ar!='' && $staff->description_ar!=null ? $staff->description_ar : '',
								'staff_image'=>$staff->staff_image!='' && $staff->staff_image!=null ? changeImageUrlForFileExist(asset('sp_uploads/staff/'.$staff->staff_image)):'',
								'video_url' => $staff->video_url!='' && $staff->video_url!=null ? $staff->video_url : '',
								'staff_images' => $staff_images,
								'staff_videos' => $staff_videos,
							];
						}

						$final_array['products'] = $product_array;
						$final_array['staffs'] = $staff_array;
						return response()->json(['status' => true, 'message' => '', 'data' => $final_array]);
					}else{
						return response()->json(['status' => false, 'message' => '', 'data' => []]);
					}
		        	
		        }
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage().$e->getLine(), 'data' => []]);
	       }
    	}
	}

	//function for getting location distance
	function distance($lat1, $lon1, $lat2, $lon2, $unit) 
    {
        /***********************************************************************************************
            function for get the distance between two coordinates
            $unit = K(kilometer),M(Miles),N(Natural miles)
        ***********************************************************************************************/
            //echo $lat1.'=='.$lon1.'=='.$lat2.'=='.$lon2.'=='.$unit; die;
      $theta = $lon1 - $lon2;
      $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
      $dist = acos($dist);
      $dist = rad2deg($dist);
      $miles = $dist * 60 * 1.1515;
      $unit = strtoupper($unit);

      if ($unit == "K") 
      {
        //this is for miles
        return round($miles,1);
          
      } 
      else if ($unit == "N") 
      {
          return ($miles * 0.8684);
      } 
      else 
      {
        //this is for kilometer
         // return round(($miles * 1.609344),1);  
      	return ($miles * 1.609344);  
      }
    }
	//---------------

	public function getCustomerReviews(Request $request)
	{
		if (Input::isMethod('post')) {
    		try{
    			$validator = Validator::make($request->all(), [

                    'language' => 'required',
                    'page' => 'required',
                    'service_provider_id'=> 'required',
                    'filter'=> 'required|in:needs_improvement,average,good,very_good,superb,exceptional,all',
                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{
		        	
		        	// $user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
		        	$user_id = $request->service_provider_id;
		        	$currentpage = $request->page;

	          		Paginator::currentPageResolver(function () use ($currentpage) {
				        return $currentpage;
				    });

		        	$results = RatingReview::select('rating_reviews.id','reviews.customer_name','reviews.user_id','reviews.description','reviews.created_at','reviews.is_anonymous','users.name as user_name',DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1) as rating"))
		        		->leftJoin('reviews', function($join) {
						      $join->on('rating_reviews.id', '=', 'reviews.id');
						    })
		        		->leftJoin('users', function($join) {
						      $join->on('reviews.user_id', '=', 'users.id');
						    })

		        		->where('rating_reviews.sp_id',$user_id)
		        		->where('reviews.is_approved','1');


		        		if($request->filter == 'needs_improvement'){
		        			 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'>',2.3);
		        			 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'<',5);		        			
		        		}
		        		elseif($request->filter == 'average'){
		        			 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'>',4.9);
		        			 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'<=',6);		        			
		        		}
		        		elseif($request->filter == 'good'){
		        			 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'>',6);
		        			 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'<=',7);		        			
		        		}
		        		elseif($request->filter == 'very_good'){
		        			 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'>',7);
		        			 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'<=',8);		        			
		        		}
		        		elseif($request->filter == 'superb'){
		        			 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'>',8);
		        			 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'<=',9);		        			
		        		}elseif($request->filter == 'exceptional'){
		        			 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'>',9);
		        			 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'<=',10);		        			
		        		}				
		        		
		        		$results = $results->groupBy('rating_reviews.id')->paginate(10);
		        		$rows_arr = $results->toArray();
		        		$result['data'] = $results;
		                $result['current_page']     = $rows_arr['current_page'];
		                $result['from']             = $rows_arr['from'];
		                $result['last_page']        = $rows_arr['last_page'];
		                $result['next_page_url']    = $rows_arr['next_page_url'];
		                $result['per_page']         = $rows_arr['per_page'];
		                $result['prev_page_url']    = $rows_arr['prev_page_url'];
		                $result['to']               = $rows_arr['to'];
		                $result['total']            = $rows_arr['total'];

		
			        	return response()->json(['status' => true, 'message' => 'My Reviews', 'data' =>$results]);
		          	}	        	
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
    	}
	}

	public function getCombinedReviews(Request $request)
	{
		if (Input::isMethod('post')) {
    		try{
    			$validator = Validator::make($request->all(), [

                    'language' => 'required',
                    'service_provider_id'=> 'required',

                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{
		        	
		        	$user_id = $request->service_provider_id;
		        	$reviews_ques_arr = \Config::get('constants.review_questions_arr');
        			$rating_arr = \Config::get('constants.rating_arr');

		        	$reviews = Review::where('sp_id',$user_id)->where('is_approved','1')->get();
		        	$sr_name = 'name'.$this->col_postfix;
		          	if(count($reviews)>0)
		          	{
		          		$rows_arr = $reviews->toArray();

			        	$review_arr = array();
			        
			        	$service_wise_arr = array();
			        	foreach($reviews as $rows)
			        	{
			        		$rating_calc = array();
			        		foreach($rows->getAssociatedReviewQuestions as $review_ques)
			        		{
			        			$ques_value =  $reviews_ques_arr[$review_ques['ques_id']]['value'];
			        			$ans =  $rating_arr[$review_ques['rating']]['value'];

			        			$rating_calc[] = $ques_value*$ans;
			        		}

			        		foreach($rows->getAssociatedReviewServices as $review_sr)
			        		{
			        			if(!isset($service_wise_arr[$review_sr['service_id']]['dislike_count'])) {
			        				$service_wise_arr[$review_sr['service_id']]['dislike_count'] = 0;
			        			}

			        			if($review_sr['like_dislike'] == 0){
			        				$service_wise_arr[$review_sr['service_id']]['dislike_count'] += (integer)1;
		        					
		        				}
			        			
			        			if(isset($service_wise_arr[$review_sr['service_id']]['like_count'])){
			        				$service_wise_arr[$review_sr['service_id']]['like_count'] += (integer)$review_sr['like_dislike'];
			        				
			        			} else{
			        				$service_wise_arr[$review_sr['service_id']]['like_count'] = 0 + (integer)$review_sr['like_dislike'];
			        				
			        			}
			        			//$service_wise_arr[$review_sr['service_id']]['likes_dislikes'][] =  $review_sr['like_dislike'];
			        			if(!isset($service_wise_arr[$review_sr['service_id']]['service_id'])) {
			        				$service_wise_arr[$review_sr['service_id']]['service_id'] =  $review_sr['service_id'];
			        				$service_wise_arr[$review_sr['service_id']]['service_name'] =  $review_sr->getAssociatedReviewService->getAssociatedService->$sr_name;
			        			}
			        		}


			        		$final_rating = (array_sum($rating_calc))/10;

			        		$review_arr[$rows['id']] = $final_rating;

			        	}
			        	 array_multisort(array_map(function($element) {
						      return $element['like_count'];
						  }, $service_wise_arr), SORT_DESC, $service_wise_arr);
			        	 $top_ten = array_slice($service_wise_arr, 0,10);
			        	 
			        	$total_reviews = array_sum($review_arr)/count($review_arr);
			        
			        	$question_wise_arr = array();
			        	foreach($reviews as $rows)
			        	{
			        		foreach($rows->getAssociatedReviewQuestions as $review_ques)
			        		{
			        			$question_wise_arr[$review_ques['ques_id']][] = $rating_arr[$review_ques['rating']]['value'];
			        		}

			        	}
			        	
			        	$ques_final_calc = array();

			        	foreach($question_wise_arr as $key => $val){
			        		$ques_final_calc[$reviews_ques_arr[$key]['key_value']] = round((array_sum($val))/count($val),0);
			        	}

			        	$finale_data = $ques_final_calc;
			        	$finale_data['average_rating'] = round($total_reviews,1);
			        	$finale_data['total_reviewers'] = count($review_arr);
			        	$finale_data['services'] = $service_wise_arr;
			        	
			        	return response()->json(['status' => true, 'message' => 'My Reviews', 'data' =>$finale_data]);
		          	}else{
		          		return response()->json(['status' => true, 'message' => 'Reviews not found', 'data' =>[]]);
		          	}
		        }
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage().$e->getLine(), 'data' => []]);
	       }
    	}
	}


	public function getServiceProviderAllServices(Request $request)
	{
		if (Input::isMethod('post')) {

    		try{
    			$validator = Validator::make($request->all(), [
                    'service_provider_id'=>'required',
                    'gender'=>'required',  
                    'language' => 'required',                       
                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{
		        	$current_date = date('Y-m-d');
		        	$sr_name = 'name'.$this->col_postfix;
			        $sr_desc = 'description'.$this->col_postfix;
			        $offer_title = 'title'.$this->col_postfix;
			        $cat_name = 'category_name'.$this->col_postfix;

		        	if($request->gender == 'all'){
		        		// DB::enableQueryLog();


			        	$rows = DB::select("select * from (select  SUBSTR(`sp_service_prices`.field_key,1,INSTR(`sp_service_prices`.field_key,'_')-1) as gender, `sp_services`.`id`,`sp_services`.`service_id`,`services`.`".$sr_name."` as service_name,`services`.`".$sr_desc."` as service_description,`services`.`time` as service_time,`subcat_id`,SUBSTR(GROUP_CONCAT(`sp_service_prices`.price), 1, INSTR (GROUP_CONCAT(`sp_service_prices`.price), ',')-1) as original_price, SUBSTR(GROUP_CONCAT(`sp_service_prices`.price), INSTR (GROUP_CONCAT(`sp_service_prices`.price), ',')+1) as best_price from `sp_services` left join `sp_service_prices` on `sp_service_prices`.`sp_service_id` = `sp_services`.`id` left join `services` on `sp_services`.`service_id` = `services`.id  where `user_id` = ".$request->service_provider_id." and `sp_services`.`is_deleted`!='1' and `field_key` in ('MEN_BESTPRICE', 'MEN_ORIGINALPRICE','WOMEN_ORIGINALPRICE','WOMEN_BESTPRICE','KIDS_BESTPRICE','KIDS_ORIGINALPRICE') group by `sp_services`.`id`,SUBSTR(`sp_service_prices`.field_key,1,INSTR(`sp_service_prices`.field_key,'_')-1) ORDER BY service_name, FIELD(field_key,'MEN_ORIGINALPRICE','WOMEN_ORIGINALPRICE','KIDS_ORIGINALPRICE')) as main where `original_price` != 0.00");
			    //     	$query = DB::getQueryLog();
							// print_r($query);die;

		        	}else{
		        		$gender_best_key = strtoupper($request->gender).'_BESTPRICE';
			        	$gender_original_key = strtoupper($request->gender).'_ORIGINALPRICE';
			        	
			        	// DB::enableQueryLog();


			        	$rows = DB::select("select * from (select  `sp_services`.`id`,`sp_services`.`service_id`,`services`.`".$sr_name."` as service_name,`services`.`".$sr_desc."` as service_description,`services`.`time` as service_time,`subcat_id`,SUBSTR(GROUP_CONCAT(`sp_service_prices`.price), 1, INSTR (GROUP_CONCAT(`sp_service_prices`.price), ',')-1) as original_price, SUBSTR(GROUP_CONCAT(`sp_service_prices`.price), INSTR (GROUP_CONCAT(`sp_service_prices`.price), ',')+1) as best_price from `sp_services` left join `sp_service_prices` on `sp_service_prices`.`sp_service_id` = `sp_services`.`id` left join `services` on `sp_services`.`service_id` = `services`.id  where `user_id` = ".$request->service_provider_id." and `sp_services`.`is_deleted`!='1' and `field_key` in ('".$gender_best_key."', '".$gender_original_key."') group by `sp_services`.`id` ORDER BY service_name ASC, FIELD(field_key,'MEN_ORIGINALPRICE','WOMEN_ORIGINALPRICE','KIDS_ORIGINALPRICE')) as main where `original_price` != 0.00");
			    //     	$query = DB::getQueryLog();
							// print_r($query);die;
		        	}
		        	
		        	
		        	if($rows){
		        		$user = SpUser::where('id',$request->service_provider_id)->select('tier_id')->first();
		        		$tier = Tier::where('id',$user['tier_id'])->select('earning')->first();
		        		$service_arr = array();
		        		$category_arr = [];
			        	foreach($rows as $values)
			        	{
			        		if(array_key_exists('gender',$values)){
			        			$gender = $values->gender;
			        		}else{
			        			$gender = $request->gender;
			        		}

			        		$subcat_name = Category::where('id',$values->subcat_id)->select($cat_name.' AS category_name','id')->first();
			        		$category_arr[$subcat_name->id] = ['id'=>$subcat_name->id, 'name'=>$subcat_name['category_name']];
			        		
			        		$service_arr[$subcat_name->id][] = [
			        			'sp_service_id'=>$values->id,
			        			'service_id'=>$values->service_id,
			        			'service_name'=>$values->service_name,
			        			'service_description'=>$values->service_description,
			        			'service_time'=>$values->service_time,
			        			'best_price'=>round($values->best_price,0),
			        			'original_price'=>round($values->original_price,0),
			        			'cashback'=>round(($values->best_price*$tier['earning'])/100,0),
			        			'gender'=>strtoupper($gender),
			        		];

			        		$service_prices_best[$values->id][] = $values->best_price;
			        		$service_prices_original[$values->id][strtoupper($gender)] = $values->original_price;
			        	}
			        	
			        	// print_r($service_prices_original);die;
			        	$service_finale = [];
			        	$category_arr_fin = [];
			        	foreach($service_arr as $category_id => $row){
			        		$category_arr_fin[] = $category_arr[$category_id]['name'];
			        		$service_finale[] = [
			        			'subcategory_id'=>$category_id,
			        			'subcategory_name'=>$category_arr[$category_id]['name'],
		        			 	'service'=>$row
			        			];
			        	}

		        	  	array_multisort($category_arr_fin, SORT_ASC, $service_finale); 

			        	// print_r($service_finale); die;
			        	// DB::enableQueryLog();

			        	$offers = SpOffer::
			        	where(function ($query) use ($current_date,$request){

	                        $query->where('status','1')
	                        ->where('user_id',$request->service_provider_id)
				        	->where('expiry_date','>=',$current_date)
				        	->where('service_type','shop')
				        	->where('is_deleted','!=','1')
				        	->where('is_nominated','0');
				        	if($request->gender != 'all'){
				        		$query->where('gender',$request->gender);
				        	}
	                    })
			        	->orWhere(function ($query) use ($current_date,$request){
			        		$query->where('status','1')
			        		->where('user_id',$request->service_provider_id)
				        	->where('expiry_date','>=',$current_date)
				        	->where('service_type','shop')
				        	->where('is_nominated','1')
				        	->where('is_deleted','!=','1')
				        	->where('spectacular_status', '1');
				        	if($request->gender != 'all'){
				        		$query->where('gender',$request->gender);
				        	}

	                        
	                    })

			        	
			        	->get();
			        	// print_r($offers);die;
			        	
			   //      	$query = DB::getQueryLog();
						// print_r($query);die;
			        	$offers_array = array();
			        	$final_offer_array = array();
			        	$sort_best_prices_arr = [];
			        	if(count($offers)>0){

			        		foreach($offers as $off_rows){

				        		$offers_array = [
				        			'id'=>$off_rows->id,
				        			'offer_name'=>$off_rows->getAssociatedOfferName->$offer_title,
				        			'gender'=>strtoupper($off_rows->gender),
				        		];
				        		$offer_services_arr = array();
				        		$total_original_price = 0;
				        		$total_best_price = 0;
				
				        		foreach($off_rows->getAssociatedOfferServices as $off_sr)
				        		{
				        			// $gen_upp = strtoupper($off_rows->gender);
				        			// if(!isset($service_prices_original[$off_sr['sp_service_id']]) || !array_key_exists($gen_upp, $service_prices_original[$off_sr['sp_service_id']])) {
				        			// 	continue;
				        			// }

				        			//print_r($service_prices_original); die;
				        			//print_r($off_sr); die;
				        	
				        			// $total_best_price += $service_prices_original[$off_sr['sp_service_id']][strtoupper($off_rows->gender)]*$off_sr['quantity'];
				        			
				        			$total_best_price += array_key_exists($off_sr['sp_service_id'], $service_prices_original) ? $service_prices_original[$off_sr['sp_service_id']][strtoupper($off_rows->gender)]*$off_sr['quantity']:0;


				        			// echo $off_sr->getAssociatedOfferServicesName->getAssociatedService->sr_name;die;
					        			// echo $total_best_price;die;
				        			if($off_sr->getAssociatedOfferServicesName->getAssociatedService->$sr_name !=null){
				        				$offer_services_arr[] = [
					        				'sp_service_id' => $off_sr->sp_service_id,
					        				'sp_service_name' => $off_sr->getAssociatedOfferServicesName->getAssociatedService->$sr_name,
					        				'service_time' => $off_sr->getAssociatedOfferServicesName->getAssociatedService->time ? $off_sr->getAssociatedOfferServicesName->getAssociatedService->time : "",
					        				'quantity' => $off_sr['quantity'],
					        				
					        			];

				        			}
				        			
				        		}
				        		$offers_array['total_original_price'] = round($total_best_price,0);
				        		$offers_array['discount'] = $off_rows->discount;
				        		$offers_array['total_best_price'] = round($total_best_price-($total_best_price*$off_rows->discount)/100,0);

				        		$offers_array['total_cashback'] = round(($offers_array['total_best_price']*$tier['earning'])/100,0);
				        		
				        		$offers_array['services'] = $offer_services_arr;
				        		$sort_best_prices_arr[] = $offers_array['total_best_price'];
				        		if(count($offer_services_arr)>0){
				        			$final_offer_array[] = $offers_array;
				        		}
				        	} //loop
				        	// print_r($best_prices_arr);die;
				        	if(count($offer_services_arr)>0){
				        		if(!empty($sort_best_prices_arr)){
					        		array_multisort($sort_best_prices_arr, SORT_ASC, $final_offer_array);
					        	}
				        	}
				        	
			        	}
			        	

			        	return response()->json(['status' => true, 'message' => 'All Services', 'data' => $service_finale, 'offers' => $final_offer_array]);
		        	}else{
		        		return response()->json(['status' => true, 'message' => 'No data found', 'data' => []]);
		        	}
		        	
		        }
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage().';;;;'.$e->getLine(), 'data' => []]);
	       }
    	}
	}

	public function searchServiceAndServiceProvider(Request $request)
	{
		if (Input::isMethod('post')) {
    		try{
    			$validator = Validator::make($request->all(), [

                    'language' => 'required',
                    'search_keyword'=> 'required',

                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{
		        	$final_array = array();
		        	 // DB::enableQueryLog();
		        	$sr_name = 'name'.$this->col_postfix;
		        	$sp_users = SpUser::where('store_name', 'like', '%' .$request->search_keyword . '%')->where('status','1')->where('is_approved','1')->select('id','store_name')->get();

		        	$services = Service::where('name', 'like', '%' .$request->search_keyword . '%')->select('id','name','name_ar')->get();

		        	$sp_array = array();
		        	if($sp_users){
		        		foreach($sp_users as $sp_row){
			        		$sp_array[] = [
			        			'id'=>$sp_row['id'],
			        			'store_name'=>$sp_row['store_name'],
			        		];
			        	}
		        	}

		        	$service_array = array();
		        	if($services){
		        		foreach($services as $sr_row){
			        		$service_array[] = [
			        			'id'=>$sr_row['id'],
			        			'name'=>$sr_row[$sr_name],
			        		];
			        	}
		        	}
		        	
		        	$final_array[] = [
		        		'service_providers_id' => $sp_array,
		        		'services_id' => $service_array,
		        	];
	
		        	return response()->json(['status' => true, 'message' => 'Search Suggestions', 'data' =>$final_array]);
		        	
		        }
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
    	}
	}

	public function getAllHomeServices(Request $request)
	{
		if (Input::isMethod('post')) {
    		try{
    			$validator = Validator::make($request->all(), [

                    'language' => 'required',
                    'service_provider_id'=>'required',
                    'gender'=> 'required',

                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{

		        	$current_date = date('Y-m-d');
		        	$sr_name = 'name'.$this->col_postfix;
			        $sr_desc = 'description'.$this->col_postfix;
			        $offer_title = 'title'.$this->col_postfix;
			        $cat_name = 'category_name'.$this->col_postfix;
		        	if($request->gender == 'all'){
		        		// DB::enableQueryLog();


			        	$rows = DB::select("select * from (select  SUBSTR(`sp_service_prices`.field_key,1,INSTR(`sp_service_prices`.field_key,'_')-1) as gender, `sp_services`.`id`,`sp_services`.`service_id`,`services`.`".$sr_name."` as service_name,`services`.`".$sr_desc."` as service_description,`services`.`time` as service_time,`subcat_id`,SUBSTR(GROUP_CONCAT(`sp_service_prices`.price), 1, INSTR (GROUP_CONCAT(`sp_service_prices`.price), ',')-1) as original_price, SUBSTR(GROUP_CONCAT(`sp_service_prices`.price), INSTR (GROUP_CONCAT(`sp_service_prices`.price), ',')+1) as best_price from `sp_services` left join `sp_service_prices` on `sp_service_prices`.`sp_service_id` = `sp_services`.`id` left join `services` on `sp_services`.`service_id` = `services`.id  where `user_id` = ".$request->service_provider_id." and `sp_services`.`is_deleted`!='1' and `field_key` in ('MEN_HOME_BESTPRICE', 'MEN_HOME_ORIGINALPRICE','WOMEN_HOME_ORIGINALPRICE','WOMEN_HOME_BESTPRICE','KIDS_HOME_BESTPRICE','KIDS_HOME_ORIGINALPRICE') group by `sp_services`.`id`,SUBSTR(`sp_service_prices`.field_key,1,INSTR(`sp_service_prices`.field_key,'_')-1) ORDER BY service_name, FIELD(field_key,'MEN_ORIGINALPRICE','WOMEN_ORIGINALPRICE','KIDS_ORIGINALPRICE')) as main where `original_price` != 0.00");
			    //     	$query = DB::getQueryLog();
							// print_r($query);die;

		        	}else{
		        		// DB::enableQueryLog();
		        		$gender_best_key = strtoupper($request->gender).'_HOME_BESTPRICE';
			        	$gender_original_key = strtoupper($request->gender).'_HOME_ORIGINALPRICE';
						
						$rows = DB::select("select * from (select  `sp_services`.`id`,`sp_services`.`service_id`,`services`.`".$sr_name."` as service_name,`services`.`".$sr_desc."` as service_description,`services`.`time` as service_time,`subcat_id`,SUBSTR(GROUP_CONCAT(`sp_service_prices`.price), 1, INSTR (GROUP_CONCAT(`sp_service_prices`.price), ',')-1) as original_price, SUBSTR(GROUP_CONCAT(`sp_service_prices`.price), INSTR (GROUP_CONCAT(`sp_service_prices`.price), ',')+1) as best_price from `sp_services` left join `sp_service_prices` on `sp_service_prices`.`sp_service_id` = `sp_services`.`id` left join `services` on `sp_services`.`service_id` = `services`.id where `user_id` = ".$request->service_provider_id." and `sp_services`.`is_deleted`!='1' and `field_key` in ('".$gender_best_key."', '".$gender_original_key."') group by `sp_services`.`id` ORDER BY service_name ASC, FIELD(field_key,'MEN_ORIGINALPRICE','WOMEN_ORIGINALPRICE','KIDS_ORIGINALPRICE')) as main where `original_price` != 0.00");
						// $query = DB::getQueryLog();
						// 	print_r($query);die;
		        	}


					if($rows){
		        		$user = SpUser::where('id',$request->service_provider_id)->select('tier_id')->first();
		        		$tier = Tier::where('id',$user['tier_id'])->select('earning')->first();
		        		$service_arr = array();
		        		$category_arr = [];
			        	foreach($rows as $values)
			        	{
			        		if(array_key_exists('gender',$values)){
			        			$gender = $values->gender;
			        		}else{
			        			$gender = $request->gender;
			        		}

			        		$subcat_name = Category::where('id',$values->subcat_id)->select($cat_name.' As category_name','id')->first();
			        		$category_arr[$subcat_name->id] = ['id'=>$subcat_name->id, 'name'=>$subcat_name['category_name']];

			        		$service_arr[$subcat_name->id][] = [
			        			'sp_service_id'=>$values->id,
			        			'service_id'=>$values->service_id,
			        			'service_name'=>$values->service_name,
			        			'service_description'=>$values->service_description,
			        			'service_time'=>$values->service_time,
			        			'best_price'=>round($values->best_price,0),
			        			'original_price'=>round($values->original_price,0),
			        			'cashback'=>round(($values->best_price*$tier['earning'])/100,0),
			        			'gender'=>strtoupper($gender),
			        		];

			        		$service_prices_best[$values->id] = $values->best_price;
			        		$service_prices_original[$values->id][strtoupper($gender)] = $values->original_price;
			        	}
			        	// print_r($service_prices_best);
			        	// print_r($service_prices_original);die;
			        	$service_finale = [];
			        	foreach($service_arr as $category_id => $row){
			        		$category_arr_fin[] = $category_arr[$category_id]['name'];
			        		$service_finale[] = [
			        			'subcategory_id'=>$category_id,
			        			'subcategory_name'=>$category_arr[$category_id]['name'],
		        			 	'service'=>$row
			        			];
			        	}
			        	array_multisort($category_arr_fin, SORT_ASC, $service_finale); 
			        	// print_r($service_finale);die;
			        	// $offers = SpOffer::where('user_id',$request->service_provider_id)->where('service_type','home')->where('gender',$request->gender)->where('status','1')->where('expiry_date','>',$current_date)->get();

			        	// $offers = SpOffer::where('user_id',$request->service_provider_id);
			        	// if($request->gender != 'all'){
			        	// 	$offers->where('gender',$request->gender);
			        	// }
			        	
			        	// $offers = $offers->where('status','1')
			        	// ->where('expiry_date','>=',$current_date)
			        	// ->where('service_type','home')
			        	// ->get();

			        	$offers = SpOffer::
			        	where(function ($query) use ($current_date,$request){

	                        $query->where('status','1')
	                        ->where('user_id',$request->service_provider_id)
				        	->where('expiry_date','>=',$current_date)
				        	->where('service_type','home')
				        	->where('is_deleted','!=','1')
				        	->where('is_nominated','0');
				        	if($request->gender != 'all'){
				        		$query->where('gender',$request->gender);
				        	}
	                    })
			        	->orWhere(function ($query) use ($current_date,$request){
			        		$query->where('status','1')
			        		->where('user_id',$request->service_provider_id)
				        	->where('expiry_date','>=',$current_date)
				        	->where('service_type','home')
				        	->where('is_nominated','1')
				        	->where('is_deleted','!=','1')
				        	->where('spectacular_status', '1');
				        	if($request->gender != 'all'){
				        		$query->where('gender',$request->gender);
				        	}

	                        
	                    })

			        	
			        	->get();
							$final_offer_array = array();
			        	if(count($offers)>0){
			        		// print_r($offers);die;
			        		$offers_array = array();
				        	
				        	$sort_best_prices_arr = [];
				        	
				        	foreach($offers as $off_rows){

				        		$offers_array = [
				        			'id'=>$off_rows->id,
				        			'offer_name'=>$off_rows->getAssociatedOfferName->$offer_title,
				        			'gender'=>strtoupper($off_rows->gender),
				        		];
				        		$offer_services_arr = array();
				        		$total_original_price = 0;
				        		$total_best_price = 0;
				
				        		foreach($off_rows->getAssociatedOfferServices as $off_sr)
				        		{

				        			// $gen_upp = strtoupper($off_rows->gender);
				        			// if(!isset($service_prices_original[$off_sr['sp_service_id']]) || !array_key_exists($gen_upp, $service_prices_original[$off_sr['sp_service_id']])) {
				        			// 	continue;
				        			// }
				        			
				        			$total_best_price += array_key_exists($off_sr['sp_service_id'], $service_prices_original) ? $service_prices_original[$off_sr['sp_service_id']][strtoupper($off_rows->gender)]*$off_sr['quantity']:0;

				        			// $total_best_price += $service_prices_original[$off_sr['sp_service_id']][strtoupper($off_rows->gender)]*$off_sr['quantity'];
				        			if($off_sr->getAssociatedOfferServicesName->getAssociatedService->$sr_name !=null){
				        				$offer_services_arr[] = [
					        				'sp_service_id' => $off_sr->sp_service_id,
					        				'sp_service_name' => $off_sr->getAssociatedOfferServicesName->getAssociatedService->$sr_name,
					        				'quantity' => $off_sr['quantity'],
					        				'service_time' => $off_sr->getAssociatedOfferServicesName->getAssociatedService->time ? $off_sr->getAssociatedOfferServicesName->getAssociatedService->time : "",
					        				
					        			];
				        			}
				        			
				        		}
				        		$offers_array['total_original_price'] = round($total_best_price,0);
				        		$offers_array['discount'] = $off_rows->discount;
				        		$offers_array['total_best_price'] = round($total_best_price-($total_best_price*$off_rows->discount)/100,0);
				        		$offers_array['total_cashback'] = round(($offers_array['total_best_price']*$tier['earning'])/100,0);
				        		$offers_array['services'] = $offer_services_arr;
				        		$sort_best_prices_arr[] = $offers_array['total_best_price'];
				        		if(count($offer_services_arr)>0){
				        			$final_offer_array[] = $offers_array;
				        		}
				        		
				        	}
				        	if(count($offer_services_arr)>0){
				        		if(!empty($sort_best_prices_arr)){
					        		array_multisort($sort_best_prices_arr, SORT_ASC, $final_offer_array);
					        	}
				        	}
				        	
			        	}
			        	

			        	return response()->json(['status' => true, 'message' => 'All Home Services', 'data' => $service_finale, 'offers' => $final_offer_array]);
		        	}else{
		        		return response()->json(['status' => true, 'message' => 'No data found', 'data' => []]);
		        	}
		        	
		        }
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage().'--'.$e->getLine(), 'data' => []]);
	       }
    	}
	}

	public function getServiceProviderChoices(Request $request)
	{
		if (Input::isMethod('post')) {
    		try{
    			$validator = Validator::make($request->all(), [

                    'language' => 'required',
                    'service_provider_id'=>'required',
                    'gender'=> 'required',
                    'services'=> 'required',
                    'service_type'=> 'required',

                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{
		        	
		        	$admin_service_ids = explode(',',$request->services);
		        	if($request->service_type == 'home'){
		        		$gender_best_key = strtoupper($request->gender).'_HOME_BESTPRICE';
		        		$gender_original_key = strtoupper($request->gender).'_HOME_ORIGINALPRICE';
		        		$check_service_type_col_name = 'home';
		        	}else{
		        		$gender_best_key = strtoupper($request->gender).'_BESTPRICE';
		        		$gender_original_key = strtoupper($request->gender).'_ORIGINALPRICE';
		        		$check_service_type_col_name = 'shop';
		        	}

		      		$sp_services_id = array();
		      		// DB::enableQueryLog();
					$sr_name = 'name'.$this->col_postfix;
			        $sr_desc = 'description'.$this->col_postfix;

		        	$rows = DB::select("select * from (select  `sp_services`.`id`,`sp_services`.`service_id`,`services`.`".$sr_name."` as service_name,`services`.`".$sr_desc."` as service_description,`services`.`time` as service_time,`subcat_id`,SUBSTR(GROUP_CONCAT(`sp_service_prices`.price), 1, INSTR (GROUP_CONCAT(`sp_service_prices`.price), ',')-1) as original_price, SUBSTR(GROUP_CONCAT(`sp_service_prices`.price), INSTR (GROUP_CONCAT(`sp_service_prices`.price), ',')+1) as best_price from `sp_services` left join `sp_service_prices` on `sp_service_prices`.`sp_service_id` = `sp_services`.`id` left join `services` on `sp_services`.`service_id` = `services`.id where `user_id` = ".$request->service_provider_id." and `sp_services`.`is_deleted`!='1' and `field_key` in ('".$gender_best_key."', '".$gender_original_key."') group by `sp_services`.`id`) as main where `original_price` != 0.00");
		    //     	$query = DB::getQueryLog();
						// print_r($query);die;
		        	if($rows){

		        		$resultArray = json_decode(json_encode($rows), true);

		        		$user = SpUser::where('id',$request->service_provider_id)->select('tier_id')->first();
			        	$tier = Tier::where('id',$user['tier_id'])->select('earning')->first();
			        	$all_service_arr = array();
		        		foreach($resultArray as $values)
			        	{
			        		$all_service_arr[$values['service_id']] = [
			        			'sp_service_id'=>$values['id'],
			        			'service_id'=>$values['service_id'],
			        			'service_name'=>$values['service_name'],
			        			'service_description'=>$values['service_description'],
			        			'service_time'=>$values['service_time'],
			        			'best_price'=>round($values['best_price'],0),
			        			'original_price'=>round($values['original_price'],0),
			        			'cashback'=>round(($values['best_price']*$tier['earning'])/100,0),
			        			'isOffer'=>0,
			        			'gender'=>$request->gender,
			        		];

			        		$service_prices_best[$values['id']] = $values['best_price'];
			        		$service_prices_original[$values['id']] = $values['original_price'];
			        	}
			        	
			        	$service_arr = array();
		      			$sp_services_id = array();
		      			$best_prices_arr = [];
			        	foreach($admin_service_ids as $values)
			        	{
			        		if(array_key_exists($values, $all_service_arr)){
			        			$best_prices_arr[] = $all_service_arr[$values]['best_price'];
			        			$service_arr[] = $all_service_arr[$values];
			        			$sp_services_id[] = $all_service_arr[$values]['sp_service_id'];
			        		}
			        		
			        	}

			        	if(!empty($best_prices_arr)){
			        		array_multisort($best_prices_arr, SORT_ASC, $service_arr);
			        	}
			        	
			        	// print_r($service_arr);die;
			        	if($sp_services_id){
			        		sort($sp_services_id);
			        		// print_r($sp_services_id);die;
			        		$current_date = date('Y-m-d');
			        		
	    				 //exact offer choices query
			        		// DB::enableQueryLog();
			        		$exact_offer_row = DB::select("select `sp_offers`.`id`, COUNT(*) as service_count, GROUP_CONCAT(`sp_offer_services`.`sp_service_id` ORDER BY `sp_offer_services`.`sp_service_id`) as OfferServices from `sp_offers` left join `sp_offer_services` on `sp_offers`.`id` = `sp_offer_services`.`sp_offer_id` where `sp_offers`.`user_id` = ".$request->service_provider_id." and `sp_offers`.`spectacular_status` = '1' and `sp_offers`.`gender` = '".$request->gender."' and `service_type` = '".$check_service_type_col_name."' and `sp_offers`.`status` = '1' and `sp_offers`.`is_deleted` != '1' and `sp_offers`.`expiry_date` >= '".$current_date."' group by `sp_offers`.`id` having OfferServices = '".implode(',',$sp_services_id)."'");
			        	

						//     $query = DB::getQueryLog();
						// print_r($exact_offer_row);die;	
						  //------------
			        		$exact_offer_row = json_decode(json_encode($exact_offer_row), true);

						    $exact_offer_id_array = array();
						    if($exact_offer_row){
						    	foreach($exact_offer_row as $offer_ids){
							    	$exact_offer_id_array[] = $offer_ids['id'];
							    }
						    }
						    
						    // print_r($exact_offer_id_array);die;
						    //combination offer choices query
		                    $combo_offer_row = SpOffer::leftJoin('sp_offer_services', function($join) {
						      $join->on('sp_offers.id', '=', 'sp_offer_services.sp_offer_id');
						    })
		                    ->select('sp_offers.id')
		                    ->where(function ($query) use ($current_date,$request,$sp_services_id,$exact_offer_id_array,$check_service_type_col_name){

		                        $query->whereIn('sp_offer_services.sp_service_id',$sp_services_id)
									    ->whereNotIn('sp_offers.id',$exact_offer_id_array)
									    ->where('sp_offers.user_id',$request->service_provider_id)
									    ->where('sp_offers.service_type',$check_service_type_col_name)
									    ->where('sp_offers.status','1')
									    ->where('is_deleted','!=','1')
									    ->where('sp_offers.expiry_date','>=',$current_date)
									    ->where('sp_offers.gender',$request->gender)
									    ->where('sp_offers.is_nominated','0');
		                    })
		                    ->orWhere(function ($query) use ($current_date,$request,$sp_services_id,$exact_offer_id_array,$check_service_type_col_name){

		                        $query->whereIn('sp_offer_services.sp_service_id',$sp_services_id)
									    ->whereNotIn('sp_offers.id',$exact_offer_id_array)
									    ->where('sp_offers.user_id',$request->service_provider_id)
									    ->where('sp_offers.service_type',$check_service_type_col_name)
									    ->where('sp_offers.status','1')
									    ->where('is_deleted','!=','1')
									    ->where('sp_offers.expiry_date','>=',$current_date)
									    ->where('sp_offers.gender',$request->gender)
									    ->where('sp_offers.is_nominated','1')
									    ->where('sp_offers.spectacular_status','1');
		                    })

						    
						    ->get();
						  //------------
						    $combo_offer_id_array = array();
						    if($combo_offer_row){
						    	foreach($combo_offer_row as $offer_ids){
						    		$combo_offer_id_array[] = $offer_ids['id'];
						    	}
						    	$combo_offer_id_array = array_unique($combo_offer_id_array);
						    }

						    $get_exact_array = array();
						    $get_combo_array = array();

						    if(count($exact_offer_id_array)>0){

						    	$get_exact_array = $this->getOffersDetails($service_prices_original,$exact_offer_id_array,$tier['earning']);
						    }
						   
						    if(count($combo_offer_id_array)>0){

						    	$get_combo_array = $this->getOffersDetails($service_prices_original,$combo_offer_id_array,$tier['earning']);
						    }
						    

						    //getting service array which has exact 2 or 1 word match
						    $service_id_arr = Service::whereIn('id',$admin_service_ids)->select('id','name')->get();
						    // print_r($service_id_arr);die;
						    $service_name_match_arr = array();
						    if($service_id_arr){
						    	$sr_name_arr = array();
						    	foreach($service_id_arr as $service_ids){
						    		$message = explode(' ', $service_ids['name']);
						    		// echo count($message);die;
						    		if(count($message) >= 2)
						    		{
						    			$name_str = $message[0].' '.$message[1];
						    		}else{
						    			$name_str = $message[0];
						    		}
						    		
						    		
							    	$sr_arr = Service::select('id')->Where('name', 'like', $name_str . '%')->whereNotIn('id',$admin_service_ids)->get();
						
							    	if(count($sr_arr)>0){

							    		foreach($sr_arr as $val){
							    			$sr_name_arr[] = $val['id'];
							    		}
							    		
							    	}
							    }//end foreach

							    if(count($sr_name_arr)>0){
							    	// $sr_name_arr = array_unique($sr_name_arr['id']);
								   // print_r($sr_name_arr);die;
								    foreach($sr_name_arr as $values)
						        	{

						        		if(array_key_exists($values, $all_service_arr)){
						        			$best_prices_sr_name_arr[] = $all_service_arr[$values]['best_price'];
						        			$service_name_match_arr[] = $all_service_arr[$values];
						        		}
						        		
						        	}
						        	if(!empty($best_prices_sr_name_arr)){
						        		array_multisort($best_prices_sr_name_arr, SORT_ASC, $service_name_match_arr);
						        	}
						        	
							    }							   
						    }
						    // print_r($service_name_match_arr);die;
						    //------------------	
						    if(count($get_exact_array)>0){
						    	$data_return = array_merge($get_exact_array, $service_arr, $get_combo_array,$service_name_match_arr);
						    }else{
						    	$data_return = array_merge($service_arr, $get_exact_array, $get_combo_array,$service_name_match_arr);
						    }
						    
			        		return response()->json(['status' => true, 'message' => 'Choices', 'data' => $data_return]);

			        	}else{
		        			return response()->json(['status' => true, 'message' => 'No data found', 'data' => []]);
		        		}
			        	
		        	}else{
		        		return response()->json(['status' => true, 'message' => 'No data found', 'data' => []]);
		        	}
		        }
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage().'--'.$e->getLine(), 'data' => []]);
	       }
    	}
	}
	//normal function for getting offers
	function getOffersDetails($service_prices_best,$offer_array,$tier)
	{

		$offers = SpOffer::whereIn('id',$offer_array)->get();
    	$offers_array = array();
    	$final_offer_array = array();
    	$sort_best_prices_arr = [];
    	$sr_name = 'name'.$this->col_postfix;
    	$offer_title = 'title'.$this->col_postfix;
    	foreach($offers as $off_rows){

    		$offers_array = [
    			'id'=>$off_rows->id,
    			'offer_name'=>$off_rows->getAssociatedOfferName->$offer_title,
    			'gender'=>$off_rows->gender,
    		];
    		$offer_services_arr = array();
    		$total_original_price = 0;
    		$total_best_price = 0;
    		

    		foreach($off_rows->getAssociatedOfferServices as $off_sr)
    		{
    			$total_best_price += $service_prices_best[$off_sr['sp_service_id']]*$off_sr['quantity'];
    	
    			$offer_services_arr[] = [
    				'sp_service_id' => $off_sr->sp_service_id,
    				'sp_service_name' => $off_sr->getAssociatedOfferServicesName->getAssociatedService->$sr_name,
    				'quantity' => $off_sr['quantity'],
    				'service_time' => $off_sr->getAssociatedOfferServicesName->getAssociatedService->time ? $off_sr->getAssociatedOfferServicesName->getAssociatedService->time : "",
    				
    			];
    		}
    		$offers_array['isOffer'] = 1;
    		$offers_array['total_original_price'] = round($total_best_price,0);
    		$offers_array['discount'] = $off_rows->discount;
    		$offers_array['total_best_price'] = round($total_best_price-($total_best_price*$off_rows->discount)/100,0);
    		$offers_array['total_cashback'] = round(($offers_array['total_best_price']*$tier)/100,0);
    		$offers_array['services'] = $offer_services_arr;
    		$sort_best_prices_arr[] = $offers_array['total_best_price'];
    		$final_offer_array[] = $offers_array;
    	}
    	if(!empty($sort_best_prices_arr)){
    		array_multisort($sort_best_prices_arr, SORT_ASC, $final_offer_array);
    	}
    	return $final_offer_array;
	}
	//----------

	public function getOfferDetails(Request $request)
	{
		if (Input::isMethod('post')) {

    		try{
    			$validator = Validator::make($request->all(), [

                    'language' => 'required',
                    'offer_id'=>'required',
                    'service_provider_id'=>'required',

                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{

		        	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
		        	$sr_name = 'name'.$this->col_postfix;
		        	$offer_title = 'title'.$this->col_postfix;
		        	$current_date = date('Y-m-d');
		        	$row = SpOffer::where('id',$request->offer_id)->where('expiry_date','>=',$current_date)->where('is_deleted','!=','1')->first();


		        	$user = SpUser::where('id',$request->service_provider_id)->select('tier_id','name','store_name','profile_image','address')->first();
			        $tier = Tier::where('id',$user['tier_id'])->select('earning')->first();

		        	if($row){
		        		$check_is_like = SpectacularOfferLike::where('offer_id',$row->id)->where('user_id',$user_id)->count();
		        		$isLike = 0;
		        		if($check_is_like>0){
							$isLike = 1;
		        		}
			        	$offers_array = [
			    			'id'=>$row->id,
			    			'sp_name'=>$user['name'],
			    			'sp_store_name'=>$user['store_name'],
			    			'sp_store_image'=>($user['profile_image']!='' && $user['profile_image']!=null)? asset('sp_uploads/profile/'.$user['profile_image']) : '',
			    			'sp_address'=>$user['address'],
			    			'offer_name'=>$row->getAssociatedOfferName->$offer_title,
			    			'spectacular_offer'=>$row->spectacular_offer,
			    			'offer_description'=>$row->offer_details,
			    			'offer_terms'=>$row->offer_terms,
			    			'service_type'=>$row->service_type,
			    			'gender'=>$row->gender,
			    			'expiry_date'=>$row->expiry_date,
			    			'status'=>$row->status,
			    			'is_nominated'=>$row->is_nominated,
			    			'nominate_thumb_image'=>($row->nominate_thumb_image!='' && $row->nominate_thumb_image!=null)? changeImageUrl(asset('sp_uploads/nominated_offers/thumb/'.$row->nominate_thumb_image)) : '',
    						'nominate_banner_image'=>($row->nominate_banner_image!='' && $row->nominate_banner_image!=null) ? changeImageUrl(asset('sp_uploads/nominated_offers/'.$row->nominate_banner_image)) :'',
    						'total_likes'=>count($row->getAssociatedOfferLikes),
    						'isLike'=>$isLike,
		    			];
		    			$offer_services_id = array();
		    			$offer_services_qty = array();
		    			$offer_services_arr = array();
		    			
		    			foreach($row->getAssociatedOfferServices as $off_sr)
			    		{
			    			// $total_best_price += $service_prices_best[$off_sr['sp_service_id']]*$off_sr['quantity'];
			    	
			    			$offer_services_arr[] = [
			    				'sp_service_id' => $off_sr->sp_service_id,
			    				'sp_service_name' => $off_sr->getAssociatedOfferServicesName->getAssociatedService->$sr_name,
			    				'quantity' => $off_sr['quantity'],
			    				'service_time' => $off_sr->getAssociatedOfferServicesName->getAssociatedService->service_time ? $off_sr->getAssociatedOfferServicesName->getAssociatedService->name : "",
			    				
			    			];
			    			$offer_services_id[] = $off_sr->sp_service_id;
			    			$offer_services_qty[$off_sr->sp_service_id] = $off_sr->quantity;
			    		}


			    		if($row->service_type == 'shop'){
			    			$gender_best_key = strtoupper($row->gender).'_BESTPRICE';
		        			$gender_original_key = strtoupper($row->gender).'_ORIGINALPRICE';
			    		}else{
			    			$gender_best_key = strtoupper($row->gender).'_HOME_BESTPRICE';
		        			$gender_original_key = strtoupper($row->gender).'_HOME_ORIGINALPRICE';
			    		}
			    		// print_r($offer_services_id);die;
			    		// DB::enableQueryLog();

			    		$offer_sr_prices = DB::select("select  `sp_services`.`id`,`sp_services`.`service_id`,SUBSTR(GROUP_CONCAT(`sp_service_prices`.price), 1, INSTR (GROUP_CONCAT(`sp_service_prices`.price), ',')-1) as original_price, SUBSTR(GROUP_CONCAT(`sp_service_prices`.price), INSTR (GROUP_CONCAT(`sp_service_prices`.price), ',')+1) as best_price from `sp_services` left join `sp_service_prices` on `sp_service_prices`.`sp_service_id` = `sp_services`.`id` where `sp_services`.`id` in ('".implode("','",$offer_services_id)."') and `field_key` in ('".$gender_best_key."', '".$gender_original_key."') group by `sp_services`.`id`");
			   //  		$query = DB::getQueryLog();
						// print_r($query);die;	

			    		$offer_sr_prices = json_decode(json_encode($offer_sr_prices), true);
			    		$sr_original_price = 0;
			    		// print_r($offer_sr_prices);die;
			    		foreach($offer_sr_prices as $pr){
			    			
			    			$sr_original_price = $sr_original_price+($pr['original_price']*$offer_services_qty[$pr['id']]);
			    		}


			    		$offers_array['offer_original_price'] = $sr_original_price;
			    		$offers_array['offer_discount'] = $row->discount;
			    		$offers_array['offer_best_price'] = round($sr_original_price-($sr_original_price*$row->discount)/100,0);
			    		$offers_array['offer_cashback'] = round(($offers_array['offer_best_price']*$tier['earning'])/100,0);



			    		$offers_array['services'] = $offer_services_arr;

		    			return response()->json(['status' => true, 'message' => 'Offer Detail', 'data' => $offers_array]);
		        	}else{
		        		return response()->json(['status' => false, 'message' => __('messages.offer expire'), 'data' => []]);

		        	}		        	
	        	}
		       
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
    	}
	}

	public function getSpectacularOfferListing(Request $request)
	{
		if (Input::isMethod('post')) {
    		try{
    			$validator = Validator::make($request->all(), [

                    'language' => 'required',
                    'category_id'=>'required',
                    'sort_by'=>'in:location,popular,price_L_H,highly_bought,price_L_H',
                    'filter_radius'=>'required',

                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else
		        {
		        	$current_date = date('Y-m-d');
		        	$latitude = $request->lat;
		        	$longitude = $request->long;
		        	
		        	$sp_title = 'title'.$this->col_postfix;
		        	

		        	$user = SpUser::
		        	leftJoin('sp_offers', function($join) {
						      $join->on('sp_offers.user_id', '=', 'sp_users.id');
						    });
		        	
		        	if($request->category_id!='home'){
		        		$user->where('sp_users.category_id',$request->category_id);
		        		$user->where('sp_offers.service_type','shop');
		        	}else{
		        		$user->where('sp_offers.service_type','home');
		        	}

		        	if($request->gender){
		        		$gender_arr = explode(',',$request->gender);
		        		$user->whereIn('sp_offers.gender',$gender_arr);
		        	}

		        	$user = $user->where('sp_offers.is_nominated','1')
		        	->where('sp_offers.spectacular_offer','1')
		        	->where('sp_offers.is_deleted','0')
		        	->where('sp_offers.status','1')
		        	->where('expiry_date','>=',$current_date)
		        	->where('spectacular_status','1')
		        	->where('sp_users.status','1')
		        	->where('sp_users.is_approved','1')
		        	->select('sp_offers.spectacular_offer_type_id')
		        	->groupBy('sp_offers.spectacular_offer_type_id')
		        	->get();
		        	$distanceQuery = "(
		        	6371 * acos
		        	( 
		        		cos( radians({$latitude}) )
		        		 * 
		        		cos( radians(sp_users.latitude) ) 
		        		* 
		        		cos( radians(sp_users.longitude) - radians({$longitude}) )
		        		 + 
		        		 sin( radians({$latitude}) )
		        		 * 
		        		 sin( radians(sp_users.latitude) )
		        	)) AS distance";

		        	$distanceQuery1 = "(6371 * acos( cos( radians({$latitude}) ) * cos( radians(sp_users.latitude) ) * cos( radians(sp_users.longitude) - radians({$longitude}) ) + sin( radians({$latitude}) ) * sin( radians(sp_users.latitude) ))) <= {$request->filter_radius}";
		        	// echo $distanceQuery1;die;
					$spec_offer_arr = [];
		        	foreach($user as $row){

		        		// DB::enableQueryLog();
						
		        		$qry = SpUser::where(function($q) use ($distanceQuery1,$request){
		        			$q->whereRaw($distanceQuery1);
		        		})
			        	->leftJoin('sp_offers', function($join) {
							      $join->on('sp_offers.user_id', '=', 'sp_users.id');
							    })
			        	->leftJoin('sp_offer_services', function($join) {
							      $join->on('sp_offers.id', '=', 'sp_offer_services.sp_offer_id');
							    })
			        	->leftJoin('sp_service_prices', function($join) {
							      $join->on('sp_service_prices.sp_service_id', '=', 'sp_offer_services.sp_service_id')
							      ->whereRaw('CONCAT(UPPER(sp_offers.gender),
							          "_ORIGINALPRICE")= sp_service_prices.field_key');
							    });

			        	if($request->category_id!='home'){
			        		$qry->where('sp_users.category_id',$request->category_id);
			        		$qry->where('sp_offers.service_type','shop');
			        	}else{
			        		$qry->where('sp_offers.service_type','home');
			        	}

			        	if($request->gender){
			        		$gender_arr = explode(',',$request->gender);
			        		$qry->whereIn('sp_offers.gender',$gender_arr);
			        	}

			        	$qry = $qry->where('sp_offers.is_nominated','1')
			        	->where('sp_offers.spectacular_offer','1')
			        	->where('sp_offers.status','1')
			        	->where('sp_offers.is_deleted','0')
			        	->where('expiry_date','>=',$current_date)
			        	->where('spectacular_status','1')
			        	->where('sp_users.status','1')
		        	     ->where('sp_users.is_approved','1')
			        	->where('spectacular_offer_type_id',$row['spectacular_offer_type_id'])
			        	->select('sp_offers.id',DB::raw("(select count(*) from spectacular_offer_likes where offer_id = sp_offers.id) as likes_count"),  DB::raw($distanceQuery), DB::raw(" (SUM(
        sp_service_prices.price * sp_offer_services.quantity)- (SUM(
            sp_service_prices.price * sp_offer_services.quantity
        ) * sp_offers.discount / 100)) as total"))
    
			        	// ->HAVING('distance','<=',$request->filter_radius)
			        	->groupBy('sp_offers.id');

			        	if($request->sort_by == 'popular'){
			        		$qry->orderBy('likes_count','DESC');
			        	}

			        	if($request->sort_by == 'location'){
			        		$qry->orderBy('distance','ASC');
			        	}
			        	if($request->sort_by == 'price_L_H'){
			        		$qry->orderBy('total','ASC');
			        	}


			        	$qry = $qry->paginate(10)->toArray();
						
			        	$spec_data = SpectacularOfferType::where('id',$row['spectacular_offer_type_id'])->first();
			        	$spec_offer_arr_row = [
			        		'spec_id'=> $spec_data->id,
			        		'spec_name'=> $spec_data->$sp_title,
			        		//'offers'=> $this->getSpectacularOfferDetails($qry['id']),
			        	];

			        	$ofs = array();
			        	foreach($qry['data'] as $qry_res){
			        		$ofs[] = $this->getSpectacularOfferDetails($qry_res['id']);
			        	}
			        	//print_r($qry['data']);die;
			        	if(!empty($qry['data'])) {
			        		$qry['data'] = $ofs;
				        	$spec_offer_arr_row['offers'] = $qry;
				        	$spec_offer_arr[] = $spec_offer_arr_row;
			        	}
			        	
			        	// print_r($qry);die;
			        	
		        	// $query = DB::getQueryLog();
		        	// print_r($query); die;
		        	}
		        	return response()->json(['status' => true, 'message' => 'Offers', 'data' => $spec_offer_arr]);

		        }
		       
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
    	}
	}

	

	public function getSpectacularOfferListingloadMore(Request $request)
	{
		if (Input::isMethod('post')) {
    		try{
    			$validator = Validator::make($request->all(), [

                    'language' => 'required',
                    'category_id'=>'required',
                    'spect_offer_id'=>'required',
                    'sort_by'=>'in:location,popular,price_L_H,highly_bought',
                    'page'=>'required',


                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else
		        {
		        	$current_date = date('Y-m-d');
		        	$latitude = $request->lat;
		        	$longitude = $request->long;

		        	// $distanceQuery = " 6371 * acos( cos( radians(" . $latitude . ") ) * cos( radians(sp_users.latitude) ) * cos( radians(sp_users.longitude) - radians(" . $longitude . ") ) + sin( radians(" . $latitude . ") ) * sin( radians(sp_users.latitude) ) ) AS distance";
		        	$distanceQuery = "(
		        	6371 * acos
		        	( 
		        		cos( radians({$latitude}) )
		        		 * 
		        		cos( radians(sp_users.latitude) ) 
		        		* 
		        		cos( radians(sp_users.longitude) - radians({$longitude}) )
		        		 + 
		        		 sin( radians({$latitude}) )
		        		 * 
		        		 sin( radians(sp_users.latitude) )
		        	)) AS distance";

		        	$distanceQuery1 = "(6371 * acos( cos( radians({$latitude}) ) * cos( radians(sp_users.latitude) ) * cos( radians(sp_users.longitude) - radians({$longitude}) ) + sin( radians({$latitude}) ) * sin( radians(sp_users.latitude) ))) <= {$request->filter_radius}";

		        	$user = SpUser::where(function($q) use ($distanceQuery1,$request){
		        			$q->whereRaw($distanceQuery1);
		        		})
		        	->leftJoin('sp_offers', function($join) {
							      $join->on('sp_offers.user_id', '=', 'sp_users.id');
							    })
			        	->leftJoin('sp_offer_services', function($join) {
							      $join->on('sp_offers.id', '=', 'sp_offer_services.sp_offer_id');
							    })
			        	->leftJoin('sp_service_prices', function($join) {
							      $join->on('sp_service_prices.sp_service_id', '=', 'sp_offer_services.sp_service_id')
							      ->whereRaw('CONCAT(UPPER(sp_offers.gender),
							          "_ORIGINALPRICE")= sp_service_prices.field_key');
							    });

		        	if($request->category_id!='home'){
		        		$user->where('sp_users.category_id',$request->category_id);
		        		$user->where('sp_offers.service_type','shop');
		        	}else{
		        		$user->where('sp_offers.service_type','home');
		        	}

		        	if($request->gender){
		        		$gender_arr = explode(',',$request->gender);
		        		$user->whereIn('sp_offers.gender',$gender_arr);
		        	}

		        	$user = $user->where('sp_offers.is_nominated','1')
		        	->where('sp_offers.spectacular_offer','1')
		        	->where('sp_offers.status','1')
		        	->where('spectacular_status','1')
		        	->where('sp_offers.is_deleted','0')
		        	->where('expiry_date','>=',$current_date)
		        	->where('sp_users.status','1')
		        	->where('sp_users.is_approved','1')
		        	->where('spectacular_offer_type_id',$request->spect_offer_id)
		        	->select('sp_offers.id',DB::raw("(select count(*) from spectacular_offer_likes where offer_id = sp_offers.id) as likes_count"),  DB::raw($distanceQuery), DB::raw(" (SUM(
        sp_service_prices.price * sp_offer_services.quantity)- (SUM(
            sp_service_prices.price * sp_offer_services.quantity
        ) * sp_offers.discount / 100)) as total"))

		        	->groupBy('sp_offers.id');

		        	if($request->sort_by == 'popular'){
			        		$user->orderBy('likes_count','DESC');
		        	}

		        	if($request->sort_by == 'location'){
		        		$user->orderBy('distance','ASC');
		        	}

		        	if($request->sort_by == 'price_L_H'){
		        		$user->orderBy('total','ASC');
		        	}

		        	$user = $user->paginate(10)->toArray();
				
		        	$offer_arr = [];
		        	foreach($user['data'] as $row) {
		        		
		        		$offer_arr[] = $this->getSpectacularOfferDetails($row['id']);
		        	}
		        	$user['data'] = $offer_arr;
		        	return response()->json(['status' => true, 'message' => 'Offers', 'data' => $user]);

		        }
		       
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
    	}
	}

	public function getSpectacularOfferDetails($offer_id)
	{

    	$row = SpOffer::where('id',$offer_id)->first();
    	$user = SpUser::where('id',$row['user_id'])->select('id','tier_id','name','store_name','profile_image')->first();
    	
        $tier = Tier::where('id',$user['tier_id'])->select('earning')->first();
        $sr_name = 'name'.$this->col_postfix;
        $off_title = 'title'.$this->col_postfix;
    	if($row){
        	$offers_array = [
    			'id'=>$row->id,
    			'sp_id'=>$user['id'],
    			'sp_name'=>$user['name'],
    			'sp_store_name'=>$user['store_name'],
    			'sp_store_image'=>($user['profile_image']!='' && $user['profile_image']!=null)? asset('sp_uploads/profile/'.$user['profile_image']) : '',
    			'offer_name'=>$row->getAssociatedOfferName->$off_title,
    			'gender'=>$row->gender,
    			'nominate_thumb_image'=>($row->nominate_thumb_image!='' && $row->nominate_thumb_image!=null)? changeImageUrl(asset('sp_uploads/nominated_offers/thumb/'.$row->nominate_thumb_image)) : '',
    			'nominate_banner_image'=>($row->nominate_banner_image!='' && $row->nominate_banner_image!=null) ? changeImageUrl(asset('sp_uploads/nominated_offers/'.$row->nominate_banner_image)) :'',
			];
			$offer_services_id = array();
			$offer_services_arr = array();
			$offer_services_qty = array();
			foreach($row->getAssociatedOfferServices as $off_sr)
    		{
    			$offer_services_arr[] = 
    				 $off_sr->getAssociatedOfferServicesName->getAssociatedService->$sr_name;
    			
    			$offer_services_id[] = $off_sr->sp_service_id;
    			$offer_services_qty[$off_sr->sp_service_id] = $off_sr->quantity;
    		}
    		
    		if($row->service_type == 'shop'){
    			$gender_best_key = strtoupper($row->gender).'_BESTPRICE';
    			$gender_original_key = strtoupper($row->gender).'_ORIGINALPRICE';
    		}else{
    			$gender_best_key = strtoupper($row->gender).'_HOME_BESTPRICE';
    			$gender_original_key = strtoupper($row->gender).'_HOME_ORIGINALPRICE';
    		}
    		// print_r($offer_services_id);die;
    		// DB::enableQueryLog();

    		$offer_sr_prices = DB::select("select  `sp_services`.`id`,`sp_services`.`service_id`,SUBSTR(GROUP_CONCAT(`sp_service_prices`.price), 1, INSTR (GROUP_CONCAT(`sp_service_prices`.price), ',')-1) as original_price, SUBSTR(GROUP_CONCAT(`sp_service_prices`.price), INSTR (GROUP_CONCAT(`sp_service_prices`.price), ',')+1) as best_price from `sp_services` left join `sp_service_prices` on `sp_service_prices`.`sp_service_id` = `sp_services`.`id` where `sp_services`.`id` in ('".implode("','",$offer_services_id)."') and `field_key` in ('".$gender_best_key."', '".$gender_original_key."') group by `sp_services`.`id`");
   //  		$query = DB::getQueryLog();
			// print_r($query);die;	

    		$offer_sr_prices = json_decode(json_encode($offer_sr_prices), true);
    		$sr_original_price = 0;
    		
    		foreach($offer_sr_prices as $pr){
    			$sr_original_price = $sr_original_price+($pr['original_price']*$offer_services_qty[$pr['id']]);
    		}

    		$offers_array['offer_original_price'] = $sr_original_price;
    		$offers_array['offer_discount'] = $row->discount;
    		$offers_array['offer_best_price'] = round($sr_original_price-($sr_original_price*$row->discount)/100,0);

    		$offers_array['services'] = $offer_services_arr;
    		return $offers_array;
    	}	        	
	}

	public function spectacularOfferLike(Request $request)
	{
		if (Input::isMethod('post')) {
    		try{
    			$validator = Validator::make($request->all(), [
                    'offer_id'=>'required',
                    // 'isLike'=>'required',
                    'language' => 'required',
                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{
		        	
		        	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

		        	$check_row = SpectacularOfferLike::where('offer_id',$request->offer_id)->where('user_id',$user_id)->count();
		        	
		        	if($check_row > 0){
		        		// echo "in";die;

		        		SpectacularOfferLike::where('offer_id',$request->offer_id)->where('user_id',$user_id)->delete();
		        		
		        		$total = SpectacularOfferLike::where('offer_id',$request->offer_id)->count();
		        			return response()->json(['status' => true,'total_like' => $total]);
		        		

		        	}else{
		        		// echo "out";die;
		        		$row = new SpectacularOfferLike();
		        		$row['offer_id'] = $request->offer_id;
		        		$row['user_id'] = $user_id;
		        		// $row['isLike'] = $request->isLike;
		        		
		                $row->save();
		        		if($row->save())
		        		{ 
		        			$total = SpectacularOfferLike::where('offer_id',$request->offer_id)->count();
		        			return response()->json(['status' => true,'total_like' => $total]);
		        		}
		        	}
		            
		        }
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
    	}
	}	

	public function getForumDetail(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'language' => 'required',
		              'forum_id' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {  

		          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

		          	$rows = Forum::where('id',$request->forum_id)->first();
		          	if($rows)
		          	{
		          		$data_1 = [
		          			'id' => $rows->id,
		          			'user_id' => $rows->user_id,
		          			'user_name' => isset($rows->getAssociatedUserInformation->name) ? $rows->getAssociatedUserInformation->name:'',
		          			'is_open_forum' => $rows->is_open_forum,
		          			'subject' => $rows->subject,
		          			'message' => $rows->message,
		          			'isNotify' => $rows->isNotify,
		          			'created_at' => date('d-M-Y H:i:s',strtotime($rows->created_at)),
		          		];

		          		$data_2 = array();
		          		foreach($rows->getAssociatedForumBannerImages as $img)
		          		{
		          			$data_2[] = [
		          				'id' => $img->id,
		          				'banner_path' => asset('sp_uploads/forum/'.$img->banner_path),
		          			];
		          		}
		          		$is_commented = 0;
		          		$data_1['is_commented'] = 0;
						if($rows->getAssociatedForumComments){

							 foreach($rows->getAssociatedForumComments as $comments){
							 	if($request->user_type == 'customer')
							 	{
							 		
							 		if($comments['user_id'] == $user_id){

							 			$data_1['is_commented'] = 1;
							 		}
							 		
							 	}else{
							 		if($comments['sp_id'] == $user_id){
							 			$data_1['is_commented'] = 1;
							 		}
							 	}
			 				 }
						}
		 
						$data_1['total_comments'] = count($rows->getAssociatedForumComments);
		          		$data_1['banner_images'] = $data_2;
		          		
			            return response()->json(['status' => true, 'message' => 'Forum Detail','data' => $data_1]);	

		          	}else{
		          		return response()->json(['status' => true, 'message' => 'Forum not found']);	
		          	}
		          	
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}


    //Mass Notification Cron.
    public function mass_notification_cron()
    {  
    	$forum = Forum::where('is_message_sent','0')->first();
    	if($forum){
			$categories = $forum->getAssociatedForumCategories->toArray();
	    	$cat_arr = array();
	    	if(!empty($categories)){
	    		foreach($categories as $cat){
	    			$cat_arr[] = $cat['category_id'];
	    		}
	    	}
    	}
    	
    	
    	//sending notification to sp for ask expert		
    	if(!empty($cat_arr)){
			$sp_user = SpUser::whereIn('category_id',$cat_arr)->select('id','notification_alert_status','store_name','email','name','category_id','status')->get()->toArray();
			// echo "<pre>";print_r($sp_user);die;
			foreach($sp_user as $sp){
				
				if($sp['notification_alert_status'] == '1')
				{
					// $message = 'Hi '.$sp['store_name'].', '.$forum->getAssociatedUserInformation->name.' is asking '.$forum->subject.'. If you have expertise on the subject, do advise. Take a look…';

					$message_main = __('messages.Someone asking you an advice',['sp_name' => $sp['store_name'],'c_name' => $forum->getAssociatedUserInformation->name,'subject' => $forum->subject]);


				    $message_title_main = __('messages.Someone asking you an advice!');

				    app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');


					$message_other = __('messages.Someone asking you an advice',['sp_name' => $sp['store_name'],'c_name' => $forum->getAssociatedUserInformation->name,'subject' => $forum->subject]);


				    $message_title_other = __('messages.Someone asking you an advice!');
	          		
		          	Notification::saveNotification($sp['id'],$forum->user_id,$forum->id,'ASK_EXPERT',$message_main,$message_other,$message_title_main,$message_title_other,'sp');
				}

				if($sp['email']!='' && $sp['status']!='0'){
					$edata['templete'] = "ask_expert_sp_mail";
					$edata['name'] = $sp['name'];
					$edata['email'] = $sp['email'];
					$edata['customer_name'] = $forum->getAssociatedUserInformation->name;
					$edata['subject'] = "Seeking your Advice!";

					send($edata);
				}		
				
			}
			$forum->is_message_sent = '1';
			$forum->save();
		}
    	die('Mass Notification - Cron Run Successfully');
    }

    public function mass_promo_cron(){
    	$rows = PromoCode::where("cron_status",'1')->get();
    	
    	if(count($rows)>0){
    		foreach($rows as $row){
    			if($row->linked_customer == '1'){
    				$cust = PromoCodeLinkedCustomer::where('promo_code_id',$row->id)->pluck('user_id');
    			}else{
    				$cust = User::pluck('id');
    			}
	    		

	    		//send notification to selected customers
	            if($row->linked_customer == '1' || $row->linked_customer == '0'){

	                $dis_label = '';
	                if($row->promo_code_type == '0'){
	                    $dis_label = 'flat';
	                }else{
	                    $dis_label = '%';
	                }
	                foreach($cust as $c_row){

	                    $u_info = User::where('id',$c_row)->select('name','notification_alert_status','email','id')->first();
	                    // print_r($u_info);die;
	                    if($u_info->notification_alert_status == '1'){


	                       $message_main = __('messages.Vouchers Credited',['name' => $u_info->name,'limit'=>$row->code_limit,'amt'=>round($row->promo_code_amount,2),'label'=>$dis_label]);


	                        $message_title_main = __('messages.Vouchers Credited!');

	                        app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

	                        $message_other = __('messages.Vouchers Credited',['name' => $u_info->name,'limit'=>$row->code_limit,'amt'=>round($row->promo_code_amount,2),'label'=>$dis_label]);
	                        
	                        $message_title_other = __('messages.Vouchers Credited!');

	                
	                        Notification::saveNotification($u_info->id,'',$row->id,'VOUCHER_CREDITED',$message_main,$message_other,$message_title_main,$message_title_other,'customer');

	                        app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');
	                        
	                        }

	                        if($u_info->email != ''){

	                            $promo_created_date = strtotime($row->from_date); // or your date as well
	                            $expiry_date = strtotime($row->to_date);
	                            $datediff = $expiry_date - $promo_created_date ;

	                            $total_days = round($datediff / (60 * 60 * 24));

	                            $data['templete'] = "redeem_discount_voucher_mail";
	                            $data['customer_name'] = $u_info->name;
	                            $data['email'] = $u_info->email;
	                            $data['discount_label'] = $dis_label;
	                            $data['discount'] = round($row->promo_code_amount,2);
	                            $data['code_limit'] = $row->code_limit;
	                            $data['total_days'] = $total_days;
	                            $data['subject'] = "Redeem your discount vouchers";
	                            send($data);
	                        }
	                    }//end of inner loop
	                }//end of condition

	                PromoCode::where('id',$row->id)->update(array("cron_status"=>'0'));

	    	}//end of outer
    	}

    }

    public function getListGallery(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	$validator = Validator::make($data, 
	            [
	              'service_provider_id' => 'required',
	              'page_no' => 'required',
	              'tags' => 'required', // default all
	            ]);
		       	if ($validator->fails()) 
		        {
		        	$error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		        } 
		        else 
		        {  	
		          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          		$currentpage = $request->page_no;
	          		Paginator::currentPageResolver(function () use ($currentpage) {
				        return $currentpage;
				    });

	          		// get staff images data
	          		$staffrows = Staff::select('staff_images_videos.id','staff_images_videos.staff_image','staff_video','staff_images_videos.content_type as mime_type','staff_images_videos.created_at','staff_images_videos.updated_at')
	          		->leftJoin('staff_images_videos', function($join) {
				      $join->on('staff.id', '=', 'staff_images_videos.staff_id');
				    })->where('sp_id',$request->service_provider_id)->where('is_deleted','!=','1')
				    ->where(function ($query) {
		               $query->whereNotNull('staff_images_videos.staff_image')->orWhereNotNull('staff_images_videos.staff_video');
		           	})->paginate(12)->toArray();
		
	          		$staff_array = array();
	          		$i=0;

				    foreach($staffrows['data'] as $row)
				    {
				    	$staffrows['data'][$i]['id'] = $row['id'];
				    	$staffrows['data'][$i]['sp_id'] = (int)$request->service_provider_id;
				    	$staffrows['data'][$i]['tag_id'] = "0";
				    	if($row['mime_type'] == 'image')
		                {
		                    $staffrows['data'][$i]['file_name'] = asset('public/sp_uploads/staff/'.$row['staff_image']);
		                }
		                else
		                {
		                	$staffrows['data'][$i]['file_name'] = asset('public/sp_uploads/staff/'.$row['staff_video']);
		                }
				    	$staffrows['data'][$i]['title'] = null;
				    	$staffrows['data'][$i]['file_desc'] = null;
				    	$staffrows['data'][$i]['sort_order'] = null;
				    	$staffrows['data'][$i]['status'] = "1";
				    	$staffrows['data'][$i]['tags'] = [array('id' => 0, 'tag_name' => 'Staff')];
				    	unset($staffrows['data'][$i]['staff_image']);
				    	unset($staffrows['data'][$i]['staff_video']);
				    	$i++;
				    }
				   
	          		// get tags filter
	          		$tagsFilter = array();
	          		$tagsArr  = SpTags::pluck('tag_name', 'id')->toArray();
	          		$rowdata = SpGallery::where('sp_id',$request->service_provider_id)->select('tag_id')->orderBy('id','ASC')->get();

	          		foreach($rowdata as $data)
	          		{  
	          			$tagIds = explode(',',$data->tag_id);

	          			foreach($tagIds as $id)
	          			{
	          				$tagsFilter['tag']['id'] = 0;
	          				$tagsFilter['tag']['tag_name'] = 'Staff';
	          				$arrr['id'] = (int)$id;
	          				$arrr['tag_name'] = $tagsArr[$id];
	          				$tagsFilter[] = $arrr;
	          			}
	          		}
	          		$tagsFilter = array_values(array_map("unserialize", array_unique(array_map("serialize", $tagsFilter))));
	          		
	          		// get sql rows data
	          		$sqlrows = SpGallery::where('sp_id',$request->service_provider_id);
	          		if($request->tags != 'all')
	          		{
	          			$sqlrows = $sqlrows->whereRaw("FIND_IN_SET('".$request->tags."',tag_id)");
	          		}
	          		$rows = $sqlrows->orderBy('sort_order','ASC')->paginate(12)->toArray();	          		
	          		$i=0;
	          		foreach($rows['data'] as $row)
	          		{
	          			$tags = array();
	          			$tagIds = explode(',',$row['tag_id']);

	          			foreach($tagIds as $id)
	          			{
	          				$arrr['id'] = (int)$id;
	          				$arrr['tag_name'] = $tagsArr[$id];
	          				$tags[] = $arrr;
	          			}
	          			
	          			$rows['data'][$i]['tags'] = $tags;

	          			$mimetype = substr($row['mime_type'],0,5);
		                if($mimetype == 'image' || $mimetype == 'video')
		                {
		                    $file_name = asset('public/sp_uploads/gallery/'.$mimetype.'/'.$row['file_name']);
		                }
		                else
		                    $file_name = $row['file_name'];

	          			$rows['data'][$i]['file_name'] = $file_name;
	          			$rows['data'][$i]['mime_type'] = $mimetype;
	          			$i++;
	          		}
	          		
	          		return response()->json(['status' => true, 'message' => 'Media List', 'tags'=> $tagsFilter ,'data' => ($request->tags === "0") ? $staffrows :  $rows]);
		        }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

}
