<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\Paginator;
use JWTAuth;
use JWTAuthException;

use App\Http\Controllers\Controller;
use App\Model\Staff;
use App\Model\ShopTiming;
use App\Model\Service;
use App\Model\Setting;
use App\Model\PromoCode;
use App\Model\Booking;
use App\Model\BookingItem;
use App\Model\PromoCodeLinkedCategory;
use App\Model\PromoCodeLinkedCustomer;
use App\Model\PromoCodeLinkedService;
use App\Model\PromoCodeLinkedStore;
use App\Model\PromoCodeLinkedSubcategory;
use App\Model\Review;
use App\Model\ReviewSpService;
use App\Model\ReviewQuestionRating;
use App\Model\SpUser;
use App\Model\User;
use App\Model\CashbackHistory;
use App\Model\StaffLeave;
use App\Model\CustomerContactSync;
use App\Model\BookingPaymentDetail;
use App\Model\WalletCashHistory;
use App\Model\UserSavedCard;
use App\Model\Transaction;
use App\Model\Notification;
use App\Model\Occasion;
use App\Model\GiftTheme;
use App\Model\FaqCategory;
use App\Model\Faq;
use App\Model\Contact;
use App\Model\ECard;
use App\Model\Device;
use App\Model\SpOfferService;
use App\Model\TrailSession;
use App\Model\TrailSessionQuestion;
use App\Model\TrailSessionQuestionsFaqs;
use DB;
use App\lib\PushNotification;
use Lang;
use File;
// use App\lib\PushNotification;

class UserBookingsController extends Controller
{
	public function __construct(Request $request)
    {
    	parent::__construct($request);
    	$this->col_postfix = '';

        $this->header = $request->header('language');
        if($this->header!= "en"){
        	$this->col_postfix = '_ar';
        }
    }

	public function getSpStaffDetails(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              // 'service_provider_id' => 'required',
		              'language' => 'required',
		              'booking_type' => 'required',
		              // 'page' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {
		          	// $rows = Staff::where('sp_id',$request->service_provider_id)->paginate(10)->toArray(); 
		          	if($request->booking_type == 'shop'){
		          		$rows = Staff::where('sp_id',$request->service_provider_id)->where('is_deleted','!=','1')->get(); 
		          	}else{
						$rows = Staff::where('sp_id',$request->service_provider_id)->where('is_deleted','!=','1')->where('is_provide_home_service','1')->get(); 
		          	}
		          	 
		          	

		          	if($rows)
		          	{
		          		$staff_array = array();

			          	foreach($rows as $values){
			          		$staff_array[] = [
			          			"id" => $values->id,
			          			"staff_name" => $values->staff_name,
			          			"nationality" => $values->nationality,
			          			"experience" => $values->experience,
			          			"speciality" => $values->speciality,
			          			"certificates" => $values->certificates,
			          			"staff_image" =>  $values->staff_image ? changeImageUrlForFileExist(asset('sp_uploads/staff/'.$values->staff_image)):"",
			          			"is_provide_home_service" => $values->is_provide_home_service,
			          		];
			          	}
					    return response()->json(['status' => true, 'message' => 'Staffs','data' => $staff_array]);	
		          	}
		          	
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function getStoreTaxDetails(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              // 'service_provider_id' => 'required',
		              'language' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {
		          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
		          	$final_data = array();

		          	$user = User::where('id',$user_id)->first();
		          	
		          	$rewards = 0;
		          	$rewards = $user->earning - $user->redemption;

		          	$setting = Setting::first();
		          
					$final_data['tax'] = $setting->percentage;
					$final_data['referred_amount'] = $setting->referred_cashback;
					$final_data['appointment_date_selection'] = $setting->appointment_date_select;
					$final_data['total_rewards'] = round($rewards,2);	


		          	if($request->service_provider_id){

		          		//get shop timing
		          		$timings = ShopTiming::where('user_id',$request->service_provider_id)->where('isOpen','0')->select('day')->get();

		          		$final_data['shop_closing_days'] = array();
		          	
			          	if(count($timings)>0)
			          	{
			          		foreach($timings as $row){
			          			$final_data['shop_closing_days'][] = $row['day'];
			          		}
						    
			          	}

			          	$tier = SpUser::leftJoin('tiers', function($join) {
						      $join->on('sp_users.tier_id', '=', 'tiers.id');
						    })
		                ->where('sp_users.id',$request->service_provider_id)
		                ->select('tiers.*','sp_users.category_id','sp_users.share_code','sp_users.mov','sp_users.area_description','sp_users.is_provide_home_service')
		                ->first();
		               
			            //check customer has used sp_refer_code or not
			            $sp_referred_customer = 0;
			            
			            if($user->refer_code == $tier->share_code && $user->refer_code !=null && $tier->share_code !=null){
			            	$sp_referred_customer = 1;
			            }
			            //--------------------------------
			            $redeemed_rewards = ($request->item_amount*$tier['redeem'])/100;
			            if($redeemed_rewards>$rewards){
			            	$redeemed_rewards = $rewards;
			            }

			            $final_data['redeem_rewards'] = round($redeemed_rewards,2);
			            $final_data['earning_percentage'] = round($tier['earning'],2);
			          	$final_data['category_id'] = $tier['category_id'];	
			          	$final_data['is_provide_home_service'] = $tier['is_provide_home_service'];			
			          	$final_data['mov'] = round($tier['mov'],2);	
			          	$final_data['area_description'] = $tier['area_description']!=null ? $tier['area_description']:"";			
			          	$final_data['is_sp_referred_customer'] = $sp_referred_customer;				


		          	}
		          			
		          	return response()->json(['status' => true, 'message' => 'Details','data' => $final_data]);	
		          	
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage().$e->getLine(), 'data' => []]);
	       }
		}
	}


	public function applyPromoCode(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'service_provider_id' => 'required',
		              'promo_code' => 'required',
		              'language' => 'required',
		              // 'services' => 'required',
		              // 'category_id' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {

		          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
		          	$check_count = 0;
		          	$check_sp = 0;
		          	$check_cust = 0;
		          	$check_level = 0;
		          	$cal_total_amount = 0;
		          	$cal_total_offer_amount = 0;
		          	$promo_total_amount = 0;
		          	$promo_total_offer_amount = 0;
		          	$is_apply_other = 0;
		          	$cal_total_amount = 0;
		          	$current_date = date('Y-m-d');
		          	$msg ="";
		          	$max_limit =0;
		          	$f_amt = 0;

		          	$tier = SpUser::leftJoin('tiers', function($join) {
						      $join->on('sp_users.tier_id', '=', 'tiers.id');
						    })
		                ->where('sp_users.id',$request->service_provider_id)
		                ->select('tiers.*')
		                ->first();

		            $promo_title = 'promo_code_title'.$this->col_postfix;
		          	$row = PromoCode::where($promo_title,strtoupper($request->promo_code))
		          	->where('from_date','<=',$current_date)
		          	->where('to_date','>=',$current_date)
		          	->where('status','1')
		          	// ->select('id','promo_code_title','description','from_date','to_date','code_limit','minimum_order_amount','promo_code_amount','linked_store','linked_customer','linked_level')
		          	->first();
		          	// print_r($row);die;
		          	if($row)
		          	{
		          		// print_r($row);die;
		          		//query for getting code limit
			          	$code_use_count = Booking::where('promo_code_id',$row->id)->where('user_id',$user_id)->count();

			          	if($code_use_count>=$row->code_limit){
			          		$check_count = 1;
			          		$msg = __('messages.Promo Code reached maximum limit');
			          	}

			          	//check minimum order amount 

			          	$m_amt = 0;
			          	if($request->services && !$request->offers){
			          		$sr_arr = json_decode($request->services, true);
			          		$m_amt = array_sum(array_values($sr_arr));
			          	}
			          	if (!$request->services && $request->offers) {
			          		$sr_arr = json_decode($request->offers, true);
			          		$m_amt = array_sum(array_values($sr_arr));
			          	}
			          	if($request->services && $request->offers){
			          		$sr_arr = json_decode($request->services, true);
			          		$of_arr = json_decode($request->offers, true);
			          		$m_amt = array_sum(array_values($sr_arr)) + array_sum(array_values($of_arr));
			          	}
			          	$total_selected_service_amount = $m_amt;

			          	if($total_selected_service_amount<$row->minimum_order_amount){
			          		$check_count = 1;

			          		$msg = __('messages.Promo Code minimum order amount',['minimum_order_amount' => $row->minimum_order_amount]);
			          		$max_limit =1;
			          	}

			          	//check linked serviceprovider
			          	if($row->linked_store != '0'){
			          		$linked_store = PromoCodeLinkedStore::where('promo_code_id',$row->id)->where('sp_id',$request->service_provider_id)->count();

			          		if($linked_store == 0){
			          			$check_sp = 1;
			          			$msg = __('messages.Promo Code is not for selected service provider');
			          		}
			          	}

			          	//check linked customer

			          	if($row->linked_customer != '0'){
			          		$linked_cust = PromoCodeLinkedCustomer::where('promo_code_id',$row->id)->where('user_id',$user_id)->count();
			          		if($linked_cust == 0){
			          			$check_cust = 1;
			          			$msg = __('messages.Promo Code is not for this customer');
			          		}
			          	}

			          	//check linked category,subcategory,services
			          	if($request->services){


				          	if($row->linked_level == '0'){
				          		
				          		$sr_arr = json_decode($request->services, true);

				          		$sr_arr_ids = array_keys($sr_arr);

				          		$linked_sr = PromoCodeLinkedService::where('promo_code_id',$row->id)->whereIn('service_id',$sr_arr_ids)->pluck('service_id')->toArray();


				          		if(count($linked_sr) > 0){
				          			$non_sr_arr= array_diff($sr_arr_ids,$linked_sr);
				          			
					          		$cal_non_amt = array();
					          		if(count($non_sr_arr)>0){

					          			foreach($non_sr_arr as $sr){
					          				$cal_non_amt[] = $sr_arr[$sr];
					          			}
					          		}
					          		
				          			$cal_amt = array();
				          			foreach($linked_sr as $sr){
				          				$cal_amt[] = $sr_arr[$sr];
				          			}

				          			if($row->promo_code_type == '0')
				          			{
				          				$promo_total_amount = $row->promo_code_amount;
				          				
				          				$cal_total_amount = array_sum($cal_amt)-$row->promo_code_amount;
				          				$cal_total_amount = $cal_total_amount+array_sum($cal_non_amt);
				          			}else{
				          				$promo_total_amount = (array_sum($cal_amt)*$row->promo_code_amount)/100;

				          				$cal_total_amount = array_sum($cal_amt) - (array_sum($cal_amt)*$row->promo_code_amount)/100;

				          				$cal_total_amount = $cal_total_amount+array_sum($cal_non_amt);

				          			}
				          			
				          			// if($cal_total_amount < $row->minimum_order_amount){
				          				
				          			// 	$check_level = 1;
				          			// 	$msg = "You can not apply this promo code because minimum order amount is ".$row->minimum_order_amount;
				          			// }else{
				          			// 	$is_apply_other = 1;
				          			// }
				          			
				          		}else{
				          			$check_level = 1;
				          			$msg = __('messages.selected services not applied');
				          		}
				          	}
				          	if($row->linked_level == '1'){
				          		
				          		$sr_arr = json_decode($request->services, true);
				          		$sr_arr_ids = array_keys($sr_arr);
				          		//get subcategories
				          		$s_row = Service::whereIn('id',$sr_arr_ids)->select('subcat_id','id')->get();

				          		$subcat_arr = array();
				          		foreach($s_row as $val){
				          			$subcat_arr[$val['subcat_id']][] = $val['id'];
				          		}

				          		$linked_sub = PromoCodeLinkedSubcategory::where('promo_code_id',$row->id)->whereIn('subcategory_id',array_keys($subcat_arr))->get();


				          		if(count($linked_sub) > 0){

				          			$cal_amt = array();
				          			$sub_cat_arr = array();
				          			foreach($linked_sub as $sr){
				          				$sub_cat_arr[] = $subcat_arr[$sr['subcategory_id']];
				          				$sub_cat_ids[] = $sr['subcategory_id'];
				          			}

				          			//non added services
				          			$non_subcat_id = array_diff(array_keys($subcat_arr), $sub_cat_ids);

				          			$non_sub_cat_arr = array();
				          			$non_cal_amt = array();
				          			foreach($non_subcat_id as $non_sr){
				          				$non_sub_cat_arr[] = $subcat_arr[$non_sr];
				          			}

				          			$non_amt = 0;
				          			foreach($non_sub_cat_arr as $a){
				          				foreach($a as $in_sr){
				          					$non_amt+=$sr_arr[$in_sr];
				          				}
				          				$non_cal_amt[] = $non_amt;
				          			}
				          			
				          			//------------------

				          			$amt = 0;
				          			foreach($sub_cat_arr as $a){
				          				foreach($a as $in_sr){
				          					$amt+=$sr_arr[$in_sr];
				          				}
				          				$cal_amt[] = $amt;
				          			}
				          			
				          			if($row->promo_code_type == '0')
				          			{
				          				$promo_total_amount = $row->promo_code_amount;
				          				$cal_total_amount = (array_sum($cal_amt)-$row->promo_code_amount)+array_sum($non_cal_amt);
				          			}else{
				          				$promo_total_amount = (array_sum($cal_amt)*$row->promo_code_amount)/100;
				          				$cal_total_amount = (array_sum($cal_amt) - (array_sum($cal_amt)*$row->promo_code_amount)/100)+array_sum($non_cal_amt);
				          			}
				          			
				          			// if($cal_total_amount<$row->minimum_order_amount){
				          			// 	$check_level = 1;
				          			// 	$msg = "You can not apply this promo code because minimum order amount is ".$row->minimum_order_amount;
				          			// }else{
				          			// 	$is_apply_other = 1;
				          			// }
				          		}else{
				          			$check_level = 1;
				          			$msg = __('messages.selected subcategory not applied');
				          		}
				          	}
				          	if($row->linked_level == '2'){
				          		
				          		$linked_cat = PromoCodeLinkedCategory::where('promo_code_id',$row->id)->where('category_id',explode(',',$request->category_id))->count();

				          		if($linked_cat > 0){
				          			$sr_arr = json_decode($request->services, true);
					          		$amt_arr = array();
					          		foreach($sr_arr as $vals){
					          			$amt_arr[] = $vals;
					          		}
					          		if($row->promo_code_type == '0')
				          			{
				          				$promo_total_amount = $row->promo_code_amount;
				          				$cal_total_amount = array_sum($amt_arr)-$row->promo_code_amount;
				          			}else{
				          				$promo_total_amount = (array_sum($amt_arr)*$row->promo_code_amount)/100;
				          				$cal_total_amount = array_sum($amt_arr) - (array_sum($amt_arr)*$row->promo_code_amount)/100;
				          			}
				          		}else{
									$check_level = 1;
				          			$msg = __('messages.selected category not applied');
				          		}

				          	}
				         }

			          	
			          	//check offer 
			          	$cal_offer_amount = 0;
	          			if($row->linked_store != '1'){
	          				if($request->offers){

	          					$off_arr = json_decode($request->offers, true);
		          				$off_arr_ids = array_keys($off_arr);
		          				$p_offer = array();
		          				
		          				foreach($off_arr_ids as $val){
		          					//get offer services
		          					$offer_sr_ids = SpOfferService::leftJoin('sp_services', function($join) {
									      $join->on('sp_services.id', '=', 'sp_offer_services.sp_service_id');
									    })
		          					->where('sp_services.user_id',$request->service_provider_id)
		          					->whereIn('sp_offer_services.sp_offer_id',$off_arr_ids)
		          					->pluck('sp_services.service_id')->toArray();

		          					//firstly check services
		          					if($row->linked_level == '0'){

		          						$linked_sr = PromoCodeLinkedService::where('promo_code_id',$row->id)->pluck('service_id')->toArray();

		          						$result = count(array_intersect($offer_sr_ids, $linked_sr)) == count($offer_sr_ids);

										if($result == 1){
											$p_offer[] = $val;
										}
			          		
		          					}
		          					if($row->linked_level == '1'){
		          						//secondly check sub category
			          					$s_row = Service::whereIn('id',$offer_sr_ids)->pluck('subcat_id')->toArray();
										$linked_sub = PromoCodeLinkedSubcategory::where('promo_code_id',$row->id)->pluck('subcategory_id')->toArray();

										$result = count(array_intersect($s_row, $linked_sub)) == count($s_row);

										if($result == 1){
											$p_offer[] = $val;
										}
		          					}
		          					
		          					if($row->linked_level == '2'){
		          						//secondly check  category
			          					$s_row = Service::whereIn('id',$offer_sr_ids)->pluck('cat_id')->toArray();
										$linked_cat = PromoCodeLinkedCategory::where('promo_code_id',$row->id)->pluck('category_id')->toArray();

										$result = count(array_intersect($s_row, $linked_cat)) == count($s_row);

										if($result == 1){
											$p_offer[] = $val;
										}
		          					}
		          				}

		          				if(count($p_offer)>0){
		          					foreach($p_offer as $p_off){
		          						$cal_offer_amount += $off_arr[$p_off];
		          					}

		          					$promo_per = 0;
		          				
		          					if($row->promo_code_type == '1'){
		          						$promo_per = $row->promo_code_amount;
		          					}else{
		          						$promo_per = ($row->promo_code_amount/$cal_offer_amount)*100;
		          					}
		          					if($promo_per < $tier['commission']){
		          						
		      							if($row->promo_code_type == '0')
					          			{
					          				$promo_total_offer_amount = $row->promo_code_amount;
					          				$cal_total_offer_amount = ($cal_offer_amount-$row->promo_code_amount);
					          			}else{
					          				$promo_total_offer_amount = ($cal_offer_amount*$row->promo_code_amount)/100;
					          				$cal_total_offer_amount = ($cal_offer_amount - ($cal_offer_amount*$row->promo_code_amount)/100);
					          			}

		      						}
	      						
		          				}

		          				
	          				}//check main offer request 
	          			}//
	          			// echo $check_count;
	          			// echo $check_sp;
	          			// echo $check_cust;
	          			// echo $check_level;die;
	          			
			          	if($check_count!=1 && $check_sp!=1 && $check_cust!=1 && $check_level!=1){
			          		$f_amt = $cal_total_offer_amount + $cal_total_amount;
			          		$p_amt = $promo_total_offer_amount + $promo_total_amount;
			          		$row['calculated_amount'] = round($f_amt,2);
			          		$row['promo_amount'] = round($p_amt,2);

			          		return response()->json(['status' => true, 'message' => __('messages.Promo Code Applied'),'data' => $row]);	
			          	}else{
			          		return response()->json(['status' => false, 'message' => $msg,'data' => [],'limit'=>$max_limit]);	
			          	}		          		
		          	}else{
		          		return response()->json(['status' => false, 'message' => __('messages.Promo Code is not valid'),'data' => round($f_amt,2)]);	
		          	}
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage().$e->getLine(), 'data' => []]);
	       }
		}
	}



	public function booking(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'language' => 'required',
		              'service_provider_id' => 'required',
		              'is_staff_selected'=> 'required',
		              'applied_offer'=> 'required|in:promo,reward,none',
		              'is_gift'=> 'required',
		              'payment_type'=> 'required|in:online,offline',
		              'selected_items'=> 'required',
		              'tax_amount'=> 'required',
		              'total_amount'=> 'required',
		              // 'promo_code_amount'=> 'required',
		              'total_item_amount'=> 'required',
		              'paid_amount'=> 'required',
		              'total_cashback'=> 'required',
		              'is_wallet_used'=> 'required',
		              'is_card_used'=> 'required',
		              
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {

		          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
		          	$settlement_option = SpUser::where('id',$request->service_provider_id)->first();
		          	$user_info = User::where('id',$user_id)->first();
		          	//check sp referred customer
		          	$sp_referred_customer = 0;
			            
		            if($user_info->refer_code == $settlement_option->share_code && $user_info->refer_code !=null && $settlement_option->share_code !=null){
		            	$sp_referred_customer = 1;
		            }

		          	// echo $user_id;die;
		          	$unique_id = $this->generateRandomNumbers();
		          	$unique_id_1 = $unique_id;

		          	$row = new Booking();
		          	$row->sp_id = $request->service_provider_id;
		          	
		          	if((int)$request->is_staff_selected == 1){
		          		$row->staff_id = $request->staff_id;
		          	}

		          	if($request->applied_offer == 'promo'){
		          		$row->promo_code_id = $request->promo_code_id;
		          	}

		          	if($request->applied_offer == 'reward'){
		          		$row->redeemed_reward_points = $request->redeemed_reward_points;
		          	}

		          	if((int)$request->is_gift == 1){


		          		$receiver = User::where('country_code',$request->receiver_country_code)
		          		->where('mobile_no',$request->receiver_mobile_no)
				      	->first();

				      
				      	
				      	if($receiver){
							$row->user_id = $receiver->id;
				      	}

				      	$row->is_gift = '1';
		          		$row->receiver_name = $request->receiver_name;
		          		$row->receiver_mobile_no = $request->receiver_country_code.'-'.$request->receiver_mobile_no;
		          		$row->receiver_email = $request->receiver_email;
		          		$row->receiver_message = $request->receiver_message;
		          		$row->gifted_by = $user_id;

		          		if((int)$request->is_send_now != 1){
		          			$row->gift_send_date = $request->gift_send_date;
		          		}

		          	}else{
		          		$row->user_id = $user_id;
		          	}

		          	if($request->payment_type == 'online'){
		          		$row->payment_type = '0';
		          	}else{
		          		$row->payment_type = '1';
		          		$row->settlement_option = $settlement_option['settlement_option'];
		          	}

		          	if($sp_referred_customer == '1'){
		          		$row->settlement_option = 'nonbillable';
		          	}
		          	// echo $row->settlement_option;die;
		          	$row->appointment_date = $request->appointment_date;
		          	$row->booking_service_type = $request->service_type;
		          	$row->appointment_time = $request->appointment_time;
		          	$row->tax_amount = $request->tax_amount;
		          	$row->booking_total_amount = $request->total_amount;
		          	$row->total_cashback = $request->total_cashback;
		          	$row->paid_amount = $request->paid_amount;
		          	$row->promo_code_amount = $request->promo_code_amount;
		          	$row->total_item_amount = $request->total_item_amount;
		          	$row->net_saving = $request->net_savings;
		          	$row->booking_unique_id = $unique_id_1;
		          	
		          	

		          	if($row->save()){

		          	
		          		
		          		$item_arr = json_decode($request->selected_items,true);

		          		foreach($item_arr as $items){
		          			$item_row = new BookingItem();
		          			$check = '';
		          			if(isset($items['sp_service_id']) && $items['sp_service_id']!=''){
		          				$item_row->is_service = '1';
		          				$check = 'sp_service_id';
		          			}else{
		          				$item_row->is_service = '0';	
		          				$check = 'offer_id'; 
		          			}
		          			$item_row->booking_id = $row->id;
		          			$item_row->booking_item_id = $items[$check];
		          			$item_row->quantity = $items['quantity'];
		          			$item_row->cashback = $items['cashback'];
		          			$item_row->original_price = $items['original_price'];
		          			$item_row->best_price = $items['best_price'];
		          			$item_row->gender = $items['gender']; 
		          			$item_row->service_type = $items['bookedfor'];  
		          			$item_row->save();

		          		}
		          		if($request->redeemed_reward_points){
		          			$user = User::where('id',$user_id)->first();
		          			$total_redemption = 0;
		          			$total_redemption = $request->redeemed_reward_points + $user->redemption;
		          			// if($user->redemption!=null && $user->redemption!=0){
		          			// 	if($user->redemption > $request->redeemed_reward_points){
		          			// 		$total_redemption = $user->redemption - $request->redeemed_reward_points;
		          			// 	}else{
		          			// 		$total_redemption = $request->redeemed_reward_points - $user->redemption;
		          			// 	}
		          				
		          			// }else{
		          			// 	$total_redemption = $request->redeemed_reward_points;
		          			// }
		          			
		          			$user->redemption = $total_redemption;
		          			$user->save();
		          		}


		          		//payment option entry in table
		          		if((int)$request->is_wallet_used == 1){
		          			$booking_payment = new BookingPaymentDetail();
		          			$booking_payment->booking_id = $row->id;
		          			$booking_payment->payment_option = 'wallet';
			          		$booking_payment->amount = $request->wallet_amount;
			          		$booking_payment->save();

			          		//add row in wallet history

			          		$wallet_hist = new WalletCashHistory();
		          			$wallet_hist->user_id = $user_id;
		          			$wallet_hist->trans_ref_id = $row->id;
		          			$wallet_hist->transaction_type = '1';
			          		$wallet_hist->amount = $request->wallet_amount;
			          		$wallet_hist->transaction_date_time = date('Y-m-d H:i:s');
			          		$wallet_hist->save();

			          		//deduct amount from wallet

			          		$usr = User::where('id',$user_id)->first();

			          		$wallet_amount = $usr->wallet_amount-$request->wallet_amount;

			          		$usr->wallet_amount = $wallet_amount;
			          		$usr->save();

			          	}

			          	if((int)$request->is_card_used == 1){
			          		$booking_card_payment = new BookingPaymentDetail();
		          			$booking_card_payment->transaction_id = $request->transaction_id;
		          			$booking_card_payment->booking_id = $row->id;
		          			$booking_card_payment->card_no = $request->card_no;
		          			$booking_card_payment->reference_no = $request->reference_no;
		          			$booking_card_payment->payment_option = 'card';
			          		$booking_card_payment->amount = $request->card_amount;
			          		$booking_card_payment->save();
			          	}

			          	//send notification
		          		$sender = User::where('id',$user_id)->first();

		          		if($row->user_id!='' && $row->user_id!=null && (int)$request->is_send_now==1 && (int)$request->is_gift==1 && $row->getAssociatedUserInfo->notification_alert_status == '1'){
		          			
			          		// $message = 'Your friend '.$sender->name.' has sent you a gift. You are certainly special and your friendship. Cherish it! The Gift has been placed in “New Bookings” section.';

			          		$message_main = __('messages.You have received a Gift',['name' => $sender->name]);

			          		$message_title_main = __('messages.You have received a Gift!');
			          		app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

							$message_other = __('messages.You have received a Gift',['name' => $sender->name]);

			          		$message_title_other =  __('messages.You have received a Gift!');

			          		// $db_message = 'You have received a gift from ';
					        Notification::saveNotification($row->user_id,$row->sp_id,$row->id,'GIFT_RECEIVED',$message_main,$message_other,$message_title_main,$message_title_other,'customer');
					        app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');
		          		}

		          		$sender = User::where('id',$user_id)->first();

		          		if($row->user_id!='' && $row->user_id!=null){
		          			
		          			if((int)$request->is_gift==1 && (int)$request->is_send_now==0){	
							} else{
								if($row->getAssociatedSpInformation->notification_alert_status == '1')
								{
									// $message = 'Congratulations '.$row->getAssociatedSpInformation->store_name.'! You have received a new booking from '.$row->getAssociatedUserInfo->name.'. Allocate and approve the booking NOW.';

									$message_main = __('messages.New Booking Received',['store_name' => $row->getAssociatedSpInformation->store_name, 'user_name' => $row->getAssociatedUserInfo->name ]);

					          		$message_title_main = __('messages.New Booking Received!');
					          		app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');
									$message_other = __('messages.New Booking Received',['store_name' => $row->getAssociatedSpInformation->store_name, 'user_name' => $row->getAssociatedUserInfo->name ]);

					          		$message_title_other = __('messages.New Booking Received!');


					          		// $db_message = 'You have received a gift from ';
							        Notification::saveNotification($row->sp_id,$row->user_id,$row->id,'NEW_BOOKING',$message_main,$message_other,$message_title_main,$message_title_other,'sp');
							        app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

								}
								

						        $item_name_arr1 = array();

						        if($row->getAssociatedSpInformation->email!='' && $row->getAssociatedSpInformation->verifyToken =='' && $row->getAssociatedSpInformation->status =='1'){

						        	$mail_data['templete'] = "sp_new_booking_mail";
							        $mail_data['name'] = $row->getAssociatedSpInformation->store_name;
							        $mail_data['customer_name'] = $row->getAssociatedUserInfo->name;
							        $mail_data['email'] = $row->getAssociatedSpInformation->email;
							        $mail_data['subject'] = "You have a new booking! ";
							        foreach($row->getAssociatedBookingItems as $bkitems){
										if($bkitems['is_service'] == '1')
										{
											if(isset($bkitems->getAssociatedServiceDetail->getAssociatedService->name)){
												$item_name_arr1[] = $bkitems->getAssociatedServiceDetail->getAssociatedService->name;
											}
											
										}
										else{
											$item_name_arr1[] = $bkitems->getAssociatedOfferDetail->getAssociatedOfferName->title;
										}

									}

							        $mail_data['services'] = implode(',',$item_name_arr1);
							        send($mail_data);

						        }


							}
			          		
		          		}
		          		//send email to admin
		          		$message1 = $row->getAssociatedSpInformation->store_name." has received a new booking.";
		          		
		          		$mail_data['templete'] = "admin_new_booking";
				        // $mail_data['name'] = $request->name;
				        $mail_data['email'] = env("CONTACT_US_EMAIL");
				        $mail_data['subject'] = "New Booking Received";
				        $mail_data['message'] = $message1;
				        
				        send($mail_data);

				        //send mail
				  		$item_name_arr = array();
				        if($request->receiver_email && (int)$request->is_gift==1 && (int)$request->is_send_now==1){
				       
				        	$mail_data['templete'] = "gift_service_mail";
					        $mail_data['sender_name'] = $sender->name;
					        $mail_data['name'] = $request->receiver_name;
					        $mail_data['receiver_message'] = $request->receiver_message;
					        $mail_data['email'] = $request->receiver_email;
					        $mail_data['subject'] = $sender->name." has sent you a Gift!";

					        foreach($row->getAssociatedBookingItems as $bkitems){
								if($bkitems['is_service'] == '1')
								{
									if(isset($bkitems->getAssociatedServiceDetail->getAssociatedService->name)){
										$item_name_arr[] = $bkitems->getAssociatedServiceDetail->getAssociatedService->name;
									}
									
								}
								else{
									$item_name_arr[] = $bkitems->getAssociatedOfferDetail->getAssociatedOfferName->title;
								}

							}

					        $mail_data['services'] = implode(',',$item_name_arr);
					       
					        
					        send($mail_data);
				        }

				        if((int)$request->is_wallet_used == 1){
	          			
		          			if($sender->low_balance_alert_status == '1' && $sender->notification_alert_status == '1'){
				          		$balance = $sender->wallet_amount;
				          		if($balance<100){
				          			// $message = 'Hey, the balance in your Beutics wallet is quite low '.round($balance,2).'. AED Top-up your Wallet to enjoy seamless transactions, card-free benefits and a lot more. ';

				          			$message_main = __('messages.Low cash balance',['balance' => round($balance,2)]);

				          			$message_title_main = __('messages.Alert! Low cash balance');
				          			app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');
									$message_other = __('messages.Low cash balance',['balance' => round($balance,2)]);

				          			$message_title_other = __('messages.Alert! Low cash balance');

							        Notification::saveNotification($user_id,null,null,'LOW_BALANCE',$message_main,$message_other,$message_title_main,$message_title_other,'customer');
							        // app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

				          		}
				          	}
		          		}
		          		

		          		//send sms
		          		


		          		if($row->user_id!='' && $row->user_id!=null && $row->payment_type!='1'){

		          			if((int)$request->is_gift == 1){
			          			$mobileNoWithCode = $row->getAssociatedGiftByUserInfo->country_code.$row->getAssociatedGiftByUserInfo->mobile_no;
			          		}else{
			          			$mobileNoWithCode = $row->getAssociatedUserInfo->country_code.$row->getAssociatedUserInfo->mobile_no;
			          		}

		          			

		          			$username =  env('SMS_COUNTRY_USERNAME');
							$password =  env('SMS_COUNTRY_PASSWORD');
							$message ="Your payment for order id ".$row->booking_unique_id." was successful, and your booking has been initiated. You will be notified as soon as the service provider confirms the appointment."; 

							$parami =['User'=>$username,'passwd'=>$password,'mobilenumber'=>$mobileNoWithCode,'message'=>$message,'sid'=>'smscntry','mtype'=>'N'];
							$ponmo = http_build_query($parami);
							$url = "http://api.smscountry.com/SMSCwebservice_bulk.aspx?$ponmo"; // json
							$res = $this->get_content($url);
		          		}
		          		

		          		// $new_row = Booking::where('id',$row->id)->first();
		          		// $final_data = $this->showBookingDetails($new_row,$user_id);

		          		app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');
		          		return response()->json(['status' => true, 'message' => __('messages.Order Placed Successfully'),'data'=>$row]);	
		          	}
		          	
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage().'-'.$e->getLine(), 'data' => []]);
	       }
		}
	}

	function get_content($URL)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $URL);
		$data = curl_exec($ch);
		//curl_exec($ch);
		curl_close($ch);
		return $data;
	}

	public function generateRandomNumbers()
	{
		// return mt_rand(10,10000000000);
		 $unique_id = substr(str_shuffle("0123456789"), 0, 10);
		 $check_no = Booking::where('booking_unique_id',$unique_id)->exists();
      	 if($check_no){
      		$unique_id = $this->generateRandomNumbers();
      	 }
      	 return $unique_id;
	}

	public function generateGiftCardRandomNumbers()
	{
		// return mt_rand(10,10000000000);
		 $unique_id = substr(str_shuffle("0123456789"), 0, 10);
		 $check_no = ECard::where('card_unique_id',$unique_id)->exists();
      	 if($check_no){
      		$unique_id = $this->generateGiftCardRandomNumbers();
      	 }
      	 return $unique_id;
	}

	public function checkFirstOrder($user_id)
	{
		$is_first_order = 1;
		$row = Booking::where('gifted_by', $user_id)

			   ->orWhere(function ($query) use ($user_id) {
				        $query->where('user_id',$user_id)

				        ->where(function ($query) {
				        	$query->whereNull('gifted_by')
			   				->orWhere('gifted_by','');
				        });
				            
				    })
	          	
			   ->count();
		
		if($row>1){
			$is_first_order = 0;
		}
		return $is_first_order;
	}

	public function showBookingDetails($row,$usr_id)
	{

		$status_arr = array('0'=>'Awaiting','1'=>'Confirmed','2'=>'Cancelled','3'=>'Refunded','4'=>'Expired','5'=>'Completed');

		$sr_name = 'name'.$this->col_postfix;
		$offer_title = 'title'.$this->col_postfix;
		if($row['db_user_name']!=''){
			$booking_type = 'manual';

		}elseif ($row['is_gift']=='1') {
			$booking_type = 'gift';
		}else{
			$booking_type = 'direct';
		}

		$data = array();
		$tier = SpUser::leftJoin('tiers', function($join) {
			      $join->on('sp_users.tier_id', '=', 'tiers.id');
			    })
            ->where('sp_users.id',$row['sp_id'])
            ->select('tiers.earning')
            ->first();

		$data['id'] = $row['id'];
		$data['booking_unique_id'] = $row['booking_unique_id'];
		$data['sp_id'] = $row['sp_id'];
		$data['sp_store_name'] = $row->getAssociatedSpInformation->store_name;
		$data['sp_address'] = $row->getAssociatedSpInformation->address;
		$data['sp_store_number'] = $row->getAssociatedSpInformation->store_number;
		$data['sp_latitude'] = $row->getAssociatedSpInformation->latitude;
		$data['sp_longitude'] = $row->getAssociatedSpInformation->longitude;
		$data['sp_landmark'] = $row->getAssociatedSpInformation->landmark;
		$data['sp_profile_image'] = $row->getAssociatedSpInformation->profile_image!='' ? changeImageUrlForFileExist(asset('sp_uploads/profile/'.$row->getAssociatedSpInformation->profile_image)):'';
		$data['sp_mobile_no'] = $row->getAssociatedSpInformation->mobile_no;
		$data['sp_country_code'] = $row->getAssociatedSpInformation->country_code;
		$data['user_id'] = $row['user_id'];
		$data['staff_id'] = $row['staff_id'];
		if($row['staff_id']!='' && $row['staff_id']!=null){
			$data['staff_name'] = $row->getAssociatedStaffInformation->staff_name;
		}else{
			$data['staff_name'] = "";
		}
		$data["booking_type"] = $booking_type;
		$data['is_gift'] = $row['is_gift'];
		$data['gifted_by'] = $row['gifted_by'];
		$data['sender_name'] = $row['gifted_by']!='' && $row['gifted_by']!=null ? $row->getAssociatedGiftByUserInfo->name:'';
		$data['receiver_name'] = $row['receiver_name'];
		$data['receiver_mobile_no'] = $row['receiver_mobile_no'];

		$data['appointment_time'] = $row['appointment_time'];
		$data['appointment_date'] = $row['appointment_date']!='' && $row['appointment_date']!= null ? $row['appointment_date']:"";
		$data['redeemed_reward_points'] = round($row['redeemed_reward_points'],2);
		$data['payment_type'] = $row['payment_type'];
		if($row['promo_code_id']!='' && $row['promo_code_id']!=null)
		{
			$data['promo_code_name'] = $row->getAssociatedPromoCodeInformation->promo_code_title;
		}
		$data['promo_code_amount'] = round($row['promo_code_amount'],2);


		$data['final_tax_amount'] = round($row['final_settlement_tax'],2);
		$data['final_paid_amount'] = round($row['final_settlement_amount'],2);
		$data['final_total_cashback'] = round($row['final_settlement_cashback'],2);

		$data['tax_amount'] = round($row['tax_amount'],2);
		$data['paid_amount'] = round($row['paid_amount'],2);
		// $data['total_cashback'] = round($row['total_cashback'],2);
		if($row['db_user_name'] == '' || $row['db_user_name'] == null){
			// $data['total_cashback'] = round(($row['booking_total_amount']*$tier->earning)/100,2);
			$data['total_cashback'] = round($row['total_cashback'],2);
		}else{
			$data['total_cashback'] = 0;
		}

	


		// $data['tax_amount'] = round($row['tax_amount'],2);
		$data['total_amount'] = round($row['booking_total_amount'],2);
		// $data['paid_amount'] = round($row['paid_amount'],2);
		$data['total_item_amount'] = round($row['total_item_amount'],2);
		$data['net_savings'] = round($row['net_saving'],2);
		if($row['status'] == '2'){
			$data['cancellation_reason'] = $row['cancellation_reason'];
			$data['cancellation_refunded_amount'] = round($row['cancellation_refunded_amount'],2);
		}
				
		
		$data['payment_type'] = $row['payment_type']=='0'?'online':'offline';
		$data['created_date'] = date('Y-m-d',strtotime($row['created_at']));
		$data['status'] = $status_arr[$row['status']];
		$review = Review::where('user_id',$row['user_id'])->where('booking_id',$row['id'])->first();

		if($review){
			$data['review_description'] = $review->description;
		}else{
			$data['review_description'] = "";
		}

		if($usr_id!=''){
			$data['is_first_order'] = $this->checkFirstOrder($usr_id);
		}
		
		if(count($row->getAssociatedBookingPaymentDetail)>0){
			// print_r($row->getAssociatedBookingPaymentDetail);die;
			$data['wallet_used_amount'] = $row->getAssociatedBookingPaymentDetail[0]->amount;
		}else{
			$data['wallet_used_amount'] = 0;
		}

		$item_name_arr = array();
		foreach($row->getAssociatedBookingItems as $bkitems){

			if($bkitems['is_service'] == '1')
			{
				$item_name_arr[] = [
					'service_id' => $bkitems['booking_item_id'],
					'name' => $bkitems->getAssociatedServiceDetail->getAssociatedService->$sr_name,
					'quantity' => $bkitems['quantity'],
					'original_price' => round($bkitems['original_price'],2),
					'best_price' => round($bkitems['best_price'],2),
					'cashback' => round($bkitems['cashback'],2),
				];
			}
			else{
				$offer_services = array();

				if(isset($bkitems->getAssociatedOfferDetail->getAssociatedOfferServices)){
					foreach($bkitems->getAssociatedOfferDetail->getAssociatedOfferServices as $os){

						$offer_services[] = [
							'service_id' => $os['sp_service_id'],
							'quantity' => $os['quantity'],
							'name' => $os->getAssociatedOfferServicesName->getAssociatedService->$sr_name,
						];
					}


					$item_name_arr[] = [
						'offer_id' => $bkitems['booking_item_id'],
						'offer_terms' => $bkitems->getAssociatedOfferDetail->offer_terms,
						'offer_details' => $bkitems->getAssociatedOfferDetail->offer_details,
						'name' => $bkitems->getAssociatedOfferDetail->getAssociatedOfferName->$offer_title,
						'quantity' => $bkitems['quantity'],
						'original_price' => round($bkitems['original_price'],2),
						'best_price' => round($bkitems['best_price'],2),
						'cashback' => round($bkitems['cashback'],2),
						'services'=>$offer_services,
					];
				}
				
			}

		}
		$data['booking_items'] = $item_name_arr;
		


		return $data;
				
	}

	public function getStaffTimeSlots(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'language' => 'required',
		              'service_provider_id'=>'required',
		              // 'staff_id' => 'required',
		              'selected_date' => 'required',
		              
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {
		          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

		          	$db_day_arr = array('0'=>'7','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6');
		          	date_default_timezone_set('Asia/Dubai');
		          	$current = strtotime(date("Y-m-d"));
	 				$date    = strtotime($request->selected_date);
	 				$datediff = $date - $current;
	 				$difference = floor($datediff/(60*60*24));

		          	$date = date('Y-m-d',strtotime($request->selected_date));
		          	$date = strtotime($request->selected_date);
					$day = $db_day_arr[date('w', $date)];
					
		          	$shop_time = ShopTiming::where('user_id',$request->service_provider_id)
		          	->where('isOpen','1')
		          	->where('day',$day)
		          	->first();
		          	
	          		// $start_time = strtotime('09:47')%900;
	          		// echo $start_time;die;
					$start_time = date('H:i',strtotime($shop_time['opening_time']));
					$end_time = date('H:i',strtotime($shop_time['closing_time']));	

		          	if($shop_time){

		          		if($request->staff_id){
		          			$time_frequency = 15;

		          			//calculating staff leave
		          			$sel_date = date('Y-m-d',strtotime($request->selected_date));
								$leave = StaffLeave::where('from_date','<=',$sel_date)
								->where('to_date','>=',$sel_date)
								->where('staff_id',$request->staff_id)
								->first();

								$leave_slots = array();
								if($leave){
									//$lst leave start time
									//$let leave end time

									if($sel_date == $leave['from_date'] && $sel_date == $leave['to_date'])
									{
										
										$lst = $leave['from_time'];
										$let = $leave['to_time'];
										
									}
									elseif($sel_date == $leave['from_date'])
									{
										
										$lst = $leave['from_time'];
										$let = $end_time;
										
									}
									else if($sel_date == $leave['to_date']){
										$lst = $start_time;
										$let = $leave['to_time'];
										
									}else{
										$lst = $start_time;
										$let = $end_time;
									}
									$slot_diff = strtotime($lst)%900;

						          	if($slot_diff > 0)
						          	{
						          		$lst = strtotime($lst)-$slot_diff;

						          	}else{
						          		$lst = strtotime($lst);
						          	}

					          		$slot_diff = strtotime($let)%900;
					         
						          	if($slot_diff > 0)
						          	{
						          		$let = strtotime($let)+(900-$slot_diff);
						          	}else{
						          		$let = strtotime($let);
						          	}
						          	$i=0;
						          	for($i = $lst; $i<= $let; $i = $i + $time_frequency * 60) {
										
										if($i == $let) {
											$leave_slots[] = date("H:i", $i-60);
										}else{
											$leave_slots[] = date("H:i", $i);
										}
									}
								}
								
								//-----------------------------

								
								
		          			$i=0;
		          			
			          		$rows = Booking::where('staff_id',$request->staff_id)
					          	
					          	
					          	->where(function ($query){

			                        $query->where('status','1')
			                        ->orWhere('status','5')
					          		->orWhere('status','0');
			                    })

					          	->where('appointment_date',$request->selected_date)
					          	->select('appointment_time','service_end_time')
					          	
					          	->get();
					          
					          	$booked_slots = array();
					          	$slots = array();
					          	

					          	
					          	foreach($rows as $val){
					          		
					          		$slot_diff = strtotime($val['appointment_time'])%900;
					          		//echo $slot_diff;die;
					          		if(empty($val['service_end_time'])){

			                            $val['service_end_time'] = date('H:i', strtotime($val['appointment_time'].'+ 15 minutes'));
			                        }
						          	if($slot_diff > 0)
						          	{
						     			
						          		$val['appointment_time'] = strtotime($val['appointment_time'])-$slot_diff;

						          		// echo date('H:i',$val['appointment_time']);die;
						          	}else{
						          		$val['appointment_time'] = strtotime($val['appointment_time']);
						          	}

					          		$slot_diff = strtotime($val['service_end_time'])%900;
					          		// echo $slot_diff;die;
						          	if($slot_diff > 0)
						          	{
						          		// echo $slot_diff;die;
						          		$val['service_end_time'] = strtotime($val['service_end_time'])+(900-$slot_diff);
						          		// echo date("H:i", $val['service_end_time']);die;
						          	}else{
						          		$val['service_end_time'] = strtotime($val['service_end_time']);
						          	}

						          	for($i = $val['appointment_time']; $i<= $val['service_end_time']; $i = $i + $time_frequency * 60) {
										
										if($i == $val['service_end_time']) {
											$booked_slots[] = date("H:i", $i-60);
										}else{
											$booked_slots[] = date("H:i", $i);
										}
									}
					          	}

					          	$ct = date('H:i');
					          	for($i = strtotime($start_time); $i<= strtotime($end_time); $i = $i + $time_frequency * 60) {

					          		if($difference==0){
										if($i>=strtotime($ct)){
										
											$slots[] = date("H:i", $i);
										}
									}else{
										$slots[] = date("H:i", $i); 
									}

									
								}

					          	$final_slots=array_diff($slots,$booked_slots);
					          	
					          	if(count($leave_slots)>1){

					          		$final_slots = array_diff($final_slots,$leave_slots);
					          	}
					          	
					          	array_pop($final_slots);
					          	$final_slots = array_values($final_slots);
					          	
		          		}
		          		else{
							// echo "in";die;
		          			$time_frequency = 15;
		          			$ct = date('H:i');
		          			$final_slots = array();
		          			
				          	for($i = strtotime($start_time); $i<= strtotime($end_time); $i = $i + $time_frequency * 60) {

				          		if($difference==0){
									if($i>=strtotime($ct)){
									
										$final_slots[] = date("H:i", $i);
									}
								}else{
									$final_slots[] = date("H:i", $i); 
								}
							}

							array_pop($final_slots);

		          		}

	          			return response()->json(['status' => true, 'message' => 'Available Time Slots','data' => $final_slots]);

	          		}
	          	}
	          }

	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage().$e->getLine(), 'data' => []]);
	       }
		}
	}

	public function getStaffLeaves(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'language' => 'required',
		              'staff_id' => 'required',
		              'service_provider_id' => 'required',
		              // 'month' => 'required',
		              // 'year' => 'required',
		              
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          }

		          else 
		          {
		          	$current_date = date('Y-m-d');
		          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

		          	$leaves = StaffLeave::where('staff_id',$request->staff_id)
		          	// ->where('from_date','<=',$current_date)
		          	// ->where('to_date','>=',$current_date)
		          	// // ->whereMonth('from_date','>=',$request->month)
		          	// // ->whereYear('from_date','>=',$request->year)
		          	// ->get();

		          	->where(function ($query) use ($current_date){
		          		$query->where(function ($query) use ($current_date){
					        $query->where('from_date','>=',$current_date)
		          			->where('to_date','<=',$current_date);

					    })
					    ->orWhere(function ($query) use ($current_date) {
					        $query
					            ->where('from_date','>=',$current_date)
		          				->where('to_date','>=',$current_date);
					    })
					    ->orWhere(function ($query) use ($current_date) {
					        $query
					            ->where('from_date','<=',$current_date)
		          				->where('to_date','>=',$current_date);
					    });
		          	})
		          	->get();

		          	
		          	$leave_arr = array();
		          	$range = array();
		          	foreach($leaves as $row){
		          		
		          		$from_date_shop_time = $this->getShopTime($row['from_date'],$request->service_provider_id);


		          		$to_date_shop_time = $this->getShopTime($row['to_date'],$request->service_provider_id);

		          		$date_from = $row['from_date'];   
						$date_from = strtotime($date_from); 
						$date_to = $row['to_date'];
						$date_to = strtotime($date_to); 
					
		          		for ($i=$date_from; $i<=$date_to; $i+=86400) {
		          			if($from_date_shop_time && $i==$date_from && (strtotime($row['from_time']) > strtotime($from_date_shop_time['opening_time']))){
									
								continue;
							} 

							if($to_date_shop_time && $i==$date_to  && (strtotime($row['to_time']) < strtotime($to_date_shop_time['closing_time']))){
								
								continue;
							} 
						    $range[] =  date("Y-m-d", $i);
						}


		          	}

		          	return response()->json(['status' => true, 'message' => 'Staff Leaves','data' => $range]);	
		          }
		          
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function getShopTime($dat,$user_id)
	{
		$db_day_arr = array('0'=>'7','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6');
		$date = strtotime($dat);

  		$day = $db_day_arr[date('w', $date)];

		$shop_time = ShopTiming::where('user_id',$user_id)
		->where('isOpen','1')
		->where('day',$day)
		->first();

		return $shop_time;
	}
	public function orderListing(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
				  'page' => 'required',
				  'status' => 'required|in:upcoming,completed',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$currentpage = $request->page;

          		Paginator::currentPageResolver(function () use ($currentpage) {
			        return $currentpage;
			    });


          		$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
          		// echo $user_id;die;
          		if($request->status == 'upcoming'){
          			$rows = Booking::where(function ($query) use ($user_id){
		          		
          				$query->where('gifted_by',$user_id)
          				
          				->orWhere(function ($query) use ($user_id){
		          		
	          				$query->where('user_id',$user_id)
	          				->whereNull('gift_send_date');
			          		
			          	})
			          	->orWhere(function ($query) use ($user_id){
		          		
	          				$query->where('user_id',$user_id)
	          				->whereNotNull('gift_send_date')
	          				->where('gift_send_date',date('Y-m-d'));
			          		
			          	});
          				
		          		
		          	})
		          	->where(function ($query){
		          		
          				$query->where('status','0')
          				->orWhere('status','1');
		          		
		          	})
          		
          			->orderBy('id','desc')
          			->paginate(10);
          		}else{
          			$rows = Booking::where('user_id',$user_id)
          			->where(function ($query) {
		          		$query->where(function ($query){
					        $query
					            ->where('status','5');
					            
					    })
					    ->orWhere(function ($query){
					        $query->where('status','3');
					            
					    })
					    ->orWhere(function ($query) {
					        $query->where('status','2');
					            
					    })
					    ->orWhere(function ($query){
					        $query
					            ->Where('status','4');
					            
					    });

		          	})

          			// ->where('status','5')
          			// ->orWhere('status','4')
          			// ->orWhere('status','2')
          			// ->orWhere('status','3')
          			->orderBy('id','desc')
          			->paginate(10);	
          		}
          		
          		$rows_arr = $rows->toArray();
          		
          		$booking_detail = array();
          		$final_array = array();
          		if($this->header == 'en'){
          			$status_arr = array('0'=>'Awaiting','1'=>'Confirmed','2'=>'Cancelled','3'=>'Refunded','4'=>'Expired','5'=>'Completed');
          			$pay_type = array('0'=>'online','1'=>'offline');
          		}else{
          			$status_arr = array('0'=>'في انتظار','1'=>'تم تأكيد','2'=>'ألغيت','3'=>'تم رد الأموال','4'=>'منتهية الصلاحية','5'=>'منجز');
          			$pay_type = array('0'=>'عبر الانترنت','1'=>'غير متصل على الانترنت');
          		}


          		
				foreach($rows as $val){
					$booking_detail = [
						"id" => $val['id'],
						"booking_unique_id" => $val['booking_unique_id'],
						"sp_id" => $val['sp_id'],
						"sp_store_name" => $val->getAssociatedSpInformation->store_name,
						"sp_profile_image" => $val->getAssociatedSpInformation->profile_image!='' ? changeImageUrlForFileExist(asset('sp_uploads/profile/'.$val->getAssociatedSpInformation->profile_image)):'',
						"user_id" => $val['user_id'],
						"staff_id" => $val['staff_id'],
						"is_gift" => $val['is_gift'],
						"receiver_name" => $val['receiver_name'],
						"receiver_mobile_no" => $val['	receiver_mobile_no'],
						"gifted_by" => $val['gifted_by'],
						"appointment_time" => $val['appointment_time'],
						"appointment_date" => $val['appointment_date']!='' && $val['appointment_date']!=null ? date('Y-m-d',strtotime($val['appointment_date'])):"",
						"tax_amount" => round($val['tax_amount'],2),
						"total_amount" => round($val['booking_total_amount'],2),
						"paid_amount" => round($val['paid_amount'],2),
						"created_date" => date('Y-m-d',strtotime($val['created_at'])),
						"status" => $status_arr[$val['status']],
						"payment_type" => $pay_type[$val['payment_type']],
					];

					$booking_item = array();
					$item_name_arr = array();
					$sr_name = 'name'.$this->col_postfix;
					$offer_name = 'title'.$this->col_postfix;
					foreach($val->getAssociatedBookingItems as $bkitems){
						if($bkitems['is_service'] == '1')
						{
							if(isset($bkitems->getAssociatedServiceDetail->getAssociatedService->name)){
								$item_name_arr[] = $bkitems->getAssociatedServiceDetail->getAssociatedService->$sr_name;
							}
							
						}
						else{
							if(isset($bkitems->getAssociatedOfferDetail->getAssociatedOfferName->title)){
								$item_name_arr[] = $bkitems->getAssociatedOfferDetail->getAssociatedOfferName->$offer_name;
							}
							
						}

					}
					$booking_detail['booking_items'] = $item_name_arr;
					$final_array[] = $booking_detail;
				}
				$result['data'] = $final_array;
                $result['current_page']     = $rows_arr['current_page'];
                $result['from']             = $rows_arr['from'];
                $result['last_page']        = $rows_arr['last_page'];
                $result['next_page_url']    = $rows_arr['next_page_url'];
                $result['per_page']         = $rows_arr['per_page'];
                $result['prev_page_url']    = $rows_arr['prev_page_url'];
                $result['to']               = $rows_arr['to'];
                $result['total']            = $rows_arr['total'];
          		return response()->json(['status' => true, 'message' => 'Order Details','data' => $result]);	
          	  }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage().$e->getLine(), 'data' => []]);
	       }
		}
	}

	public function orderSummery(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
				  'order_id' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$order = Booking::where('id',$request->order_id)->first();
	          	if($order){
	          		$result = $this->showBookingDetails($order,'');
          			return response()->json(['status' => true, 'message' => 'Order Details','data' => $result]);
	          	}else{
	          		return response()->json(['status' => false, 'message' => 'Order not found']);
          	  	}
	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function addReviews(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	              'service_provider_id' => 'required',
	              'booking_id' => 'required',
	              'question1' => 'required',
	              'question2' => 'required',
	              'question3' => 'required',
	              'question4' => 'required',
	              'question5' => 'required',
	              'question6' => 'required',
	              // 'staff_id' => 'required',
	              // 'staff_like_dislike' => 'required',
	              // 'services' => 'required',
	              'description' => 'required',

	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

	          	$row = new Review();
                $row->sp_id = $request->service_provider_id;
                $row->booking_id = $request->booking_id;
                $row->user_id = $user_id;
                $row->description = $request->description;
                if(isset($request->staff_id)){
                	$row->staff_id = $request->staff_id;
                }
                if(isset($request->staff_like_dislike)){
                	 $row->staff_like_dislike = (string)$request->staff_like_dislike;
                }
                if($request->rate_beutics){
                	$row->rate_beutics = $request->rate_beutics;
                }

                if($request->is_anonymous){
                	$row->is_anonymous = $request->is_anonymous;
                }
                                
                if($row->save()){
                  
	                   for($i=1;$i<=6;$i++){
	                       
	                       $rating_row = new ReviewQuestionRating();
	                        $rating_row['review_id'] = $row->id;
	                        $rating_row['ques_id'] = $i;
	                        $rating_row['rating'] = $request['question'.$i];
	                        $rating_row->save();
	                        
	                    }
	                    if($request->services){
		                	$services = json_decode($request->services,true);
		                    foreach($services as $srow)
		                    {
		                        $service_row = new ReviewSpService();
		                        $service_row['review_id'] = $row->id;
		                        $service_row['service_id'] = $srow['service_id'];
		                        $service_row['like_dislike'] = (string)$srow['is_like'];
		                        $service_row->save();
		                        
		                    }
		                }
		               
	                    return response()->json(['status' => true, 'message' => __('messages.add review')]);	
                       
                    }
          	  }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage().$e->getLine(), 'data' => []]);
	       }
		}
	}

	public function myWallet(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

	          	$user = User::where('id',$user_id)->select('earning','redemption','wallet_amount')->first();

	          	$final_data = array();
	          	// $final_data['earning'] = $user->earning;
	          	// $final_data['redemption'] = $user->redemption;
	          	$final_data['beutics_rewards'] = $user->earning-$user->redemption;
	          	$final_data['cash_amount'] = round($user->wallet_amount,2);
	          	return response()->json(['status' => true, 'message' => 'Wallet Details', 'data' => $final_data]);

	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function earningsListing(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	              'page' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

	          	$booking_cashback = CashbackHistory::
	          	leftJoin('bookings', function($join) {
			      $join->on('cashback_histories.booking_id', '=', 'bookings.id');
			    })
	          	->where('cashback_histories.user_id',$user_id)
	          	->where(function ($query){

	                        $query->where('type','0')
	                        ->orWhere('type','4');
	                    })

	          	->select('cashback_histories.id','cashback_histories.user_id','booking_id','bookings.booking_unique_id','cashback_histories.amount','cashback_histories.created_at',DB::raw('(CASE WHEN cashback_histories.type = "0" THEN "booking" ELSE "booking_refund" END) AS label'))
	          	->orderBy('id','DESC')
	          	->paginate(10)
	          	->toArray();

	          	$total_refer_earning = CashbackHistory::where('user_id',$user_id)
	          	->where(function ($query){

                    $query->where('type','0')
                    ->orWhere('type','4');
                })
	          	->select(DB::raw('sum(amount) as amount'))
	          	->groupBy('user_id')->first();
	          	

	          	$final_data = array();

	          	$final_data['total_cashback'] = $total_refer_earning ? $total_refer_earning->amount:array();
	          	$final_data['cashback'] = $booking_cashback;

	          	return response()->json(['status' => true, 'message' => 'Cashback Earnings', 'data' => $final_data]);

	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getLine(), 'data' => []]);
	       }
		}
	}

	public function referredEarningsListing(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	              'page' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

	          	$currentpage = $request->page;

          		Paginator::currentPageResolver(function () use ($currentpage) {
			        return $currentpage;
			    });

	          	$refer_earning = CashbackHistory::where('user_id',$user_id)
	          	->whereIn('type',array('1','2'))
	          	->orderBy('id','DESC')
	          	->paginate(10);
	          	$rows_arr = $refer_earning->toArray();

	          	//contact sync query
	          	$contact_sync_earning = CashbackHistory::where('user_id',$user_id)
	          	->where('type','3')
	          	->first();
	          

	          	$contact_sync_earning_arr = array();
	          	if($contact_sync_earning){
	          		$contact_sync_earning_arr = [
	      				// 'user_id'=>$row->user_id,
	      				'label'=>'contact_sync',
	      				'amount'=>$contact_sync_earning->amount,
	      				'created_at'=>date('Y-m-d',strtotime($contact_sync_earning->created_at)),
	      			];
	          	}

	          	$signup_earning = CashbackHistory::where('user_id',$user_id)
	          	->where('type','2')
	          	->first();

	          	$signup_earning_arr = array();
	          	if($signup_earning){
	          		$signup_earning_arr = [
	      				// 'user_id'=>$row->user_id,
	      				'label'=>'signup',
	      				'amount'=>$signup_earning->amount,
	      				'created_at'=>date('Y-m-d',strtotime($signup_earning->created_at)),
	      			];
	          	}
	          
      			
	          	$refer_earning_arr = array();
	          	foreach($refer_earning as $row)
	          	{
	          		if($row->type == '1')
	          		{
	          			$refer_earning_arr[] = [
	          				// 'user_id'=>$row->user_id,
	          				// 'user_name'=>$row->getAssociatedUsrName->name,
	          				'label'=>'refer_to',
	          				'referral_user_id'=>$row->referral_user_id,
	          				'referral_user_name'=>isset($row->getAssociatedReferralUsrName)? $row->getAssociatedReferralUsrName->name:"",
	          				'amount'=>$row->amount,
	          				'created_at'=>date('Y-m-d',strtotime($row->created_at)),
	          			];
	          		}

	          // 		if($row->type == '2')
	          // 		{
	          // 			$refer_earning_arr[] = [
		      			// 	'label'=>'signup',
		      			// 	'amount'=>$row->amount,
		      			// 	'created_at'=>date('Y-m-d',strtotime($row->created_at)),
	      				// ];
	          			
	          // 		}

	          	}
	          	$result['data'] = $refer_earning_arr;
                $result['current_page']     = $rows_arr['current_page'];
                $result['from']             = $rows_arr['from'];
                $result['last_page']        = $rows_arr['last_page'];
                $result['next_page_url']    = $rows_arr['next_page_url'];
                $result['per_page']         = $rows_arr['per_page'];
                $result['prev_page_url']    = $rows_arr['prev_page_url'];
                $result['to']               = $rows_arr['to'];
                $result['total']            = $rows_arr['total'];

	          	$final_data = array();

	          	$total_refer_earning = CashbackHistory::where('user_id',$user_id)->whereIn('type',array('1','2'))
	          	->select(DB::raw('sum(amount) as amount'))->groupBy('user_id')->first();

	          	// print_r($total_refer_earning);die;

				$final_data['total_refer_earning'] = $total_refer_earning ? $total_refer_earning['amount']:array();

	          	$final_data['refer_earning'] = $result;

	          	return response()->json(['status' => true, 'message' => 'Refer Earnings', 'data' => $final_data,'contact_sync_data'=>$contact_sync_earning_arr,'signup_earning'=>$signup_earning_arr]);

	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage().$e->getLine(), 'data' => []]);
	       }
		}
	}

	public function redemptionListing(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	              'page' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

	          	$base_path = changeImageUrlForFileExist(asset('sp_uploads/profile/')).'/';

	          	$booking_redemption = Booking::where(function ($query) use ($request,$user_id) {
	          		$query->where(function ($query) use ($user_id){
				        $query
				            ->whereNull('is_gift')
				            ->where('user_id',$user_id);
				            
				    })
				    ->orWhere(function ($query) use ($user_id) {
				        $query
				           ->where('is_gift','1')
				           ->where('gifted_by',$user_id);
				          
				    });
				   
	          	})

	          	->leftJoin('sp_users', function($join) {
			      $join->on('sp_users.id', '=', 'bookings.sp_id');
			    })
	          	->where('bookings.redeemed_reward_points','!=','')
	          	// ->where('bookings.status','5')
	          	->select('bookings.id',DB::raw('ROUND(bookings.redeemed_reward_points,2) as redeemed_reward_points'),'bookings.sp_id','bookings.created_at','sp_users.store_name',DB::raw('CONCAT("'.$base_path.'", sp_users.profile_image) AS profile_image'))
	          	->orderBy('id','DESC')
	          	->paginate(10)
	          	->toArray();

	          	$user = User::where('id',$user_id)->select('redemption')->first();

	          	$final_data = array();
	          	$sum = 0;
	          	foreach($booking_redemption['data'] as $val){
	          		$sum+= $val['redeemed_reward_points'];
	          	}

	          	$final_data['total_redemption'] = round($user->redemption,2);
	          	$final_data['redemptions'] = $booking_redemption;

	          	return response()->json(['status' => true, 'message' => 'Redemptions', 'data' => $final_data]);

	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function friendsVisited(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	              'service_provider_id' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          	$base_path = asset('/');
	          	$customer_mobile_no = Booking::leftJoin('users', function($join) {
						      $join->on('bookings.user_id', '=', 'users.id');
						    })
	          	->where('sp_id',$request->service_provider_id)
	          	->where('bookings.user_id','!=','')
	          	->select('users.id',DB::raw("CONCAT(users.country_code,users.mobile_no) AS mobile_no"))
	          	->distinct('bookings.user_id')
	          	->pluck('mobile_no');

	          	$friends_visited = CustomerContactSync::whereIn('customer_contact_syncs.phone_no',$customer_mobile_no)	          	
	          	->where('customer_contact_syncs.user_id',$user_id)
	          	->get();

	          	$final_array = array();
	          	foreach($friends_visited as $val){
	          		
	          		$user_info = User::where(DB::raw("CONCAT(country_code, mobile_no)"),$val['phone_no'])->select('name',DB::raw('CONCAT("'.$base_path.'", image) AS profile_image'))->first();

	          		$final_array[] = $user_info;
	          	}
	          	return response()->json(['status' => true, 'message' => 'Total Friends Visited', 'data' => $final_array]);

	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function showAppliedPromoCode(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	              'service_provider_id' => 'required',
	              'category_id' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          	$current_date = date('Y-m-d');
	          	$p_title = 'promo_code_title'.$this->col_postfix;
	          	$p_desc = 'description'.$this->col_postfix;

	          	$row = promoCode::leftJoin('promo_code_linked_customers', function($join) use ($user_id) {
						      $join->on('promo_codes.id', '=', 'promo_code_linked_customers.promo_code_id');
						    })
	          				->leftJoin('promo_code_linked_stores', function($join) use ($request) {
						      $join->on('promo_codes.id', '=', 'promo_code_linked_stores.promo_code_id');
						    })
						    ->leftJoin('promo_code_linked_categories', function($join) use ($request) {
						      $join->on('promo_codes.id', '=', 'promo_code_linked_categories.promo_code_id');
						    })
						    ->where(function ($query) use ($request,$user_id) {
				          		$query->where(function ($query) use ($request){
							        $query
							            ->where('promo_codes.linked_store','=','0')
							            ->where('promo_codes.linked_customer','=','0')

							            ->where(function ($query) use ($request){
							            	$query->whereIn('promo_codes.linked_level',array('0','1'))
								            ->orWhere(function ($query) use ($request) {
										        $query
										            ->where('promo_codes.linked_level','2')
										            ->where('promo_code_linked_categories.category_id',$request->category_id);
										    });
							            });	
							            
							            
							    })
							    ->orWhere(function ($query) use ($user_id,$request) {
							        $query
							            ->where('promo_codes.linked_store','0')
							            ->where('promo_codes.linked_customer','1')
							            ->where('promo_code_linked_customers.user_id',$user_id)
							            ->where(function ($query) use ($request){
							            	$query->whereIn('promo_codes.linked_level',array('0','1'))
								            ->orWhere(function ($query) use ($request) {
										        $query
										            ->where('promo_codes.linked_level','2')
										            ->where('promo_code_linked_categories.category_id',$request->category_id);
										    });
							            });	
							    })
							    ->orWhere(function ($query) use ($request) {
							        $query
							            ->where('promo_codes.linked_store','1')
							            ->where('promo_codes.linked_customer','0')
							            ->where('promo_code_linked_stores.sp_id',$request->service_provider_id)
							            ->where(function ($query) use ($request){
							            	$query->whereIn('promo_codes.linked_level',array('0','1'))
								            ->orWhere(function ($query) use ($request) {
										        $query
										            ->where('promo_codes.linked_level','2')
										            ->where('promo_code_linked_categories.category_id',$request->category_id);
										    });
							            });	
							    })
							    ->orWhere(function ($query) use ($request,$user_id) {
							        $query
							            ->where('promo_codes.linked_store','1')
							            ->where('promo_codes.linked_customer','1')
							            ->where('promo_code_linked_stores.sp_id',$request->service_provider_id)
							            ->where('promo_code_linked_customers.user_id',$user_id)
							            ->where(function ($query) use ($request){
							            	$query->whereIn('promo_codes.linked_level',array('0','1'))
								            ->orWhere(function ($query) use ($request) {
										        $query
										            ->where('promo_codes.linked_level','2')
										            ->where('promo_code_linked_categories.category_id',$request->category_id);
										    });
							            });	
							    });

				          	})
				          	->where('promo_codes.from_date','<=',$current_date)
				          	->where('promo_codes.to_date','>=',$current_date)
				          	->where('promo_codes.status','1')
				          	->select('promo_codes.id','promo_codes.'.$p_title.' AS promo_code_title','promo_codes.'.$p_desc.' AS description')
				          	->orderBy('promo_codes.id','desc')
				          	->groupBy('promo_codes.id')

	          				->get();
	          				// print_r($row);die;

	          	return response()->json(['status' => true, 'message' => 'Promo Codes', 'data' => $row]);

	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage().$e->getLine(), 'data' => []]);
	       }
		}
	}

	public function downloadOrderImage(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();

	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	              'order_id' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
			
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	       //    	$row = Booking::where('id',$request->order_id)->first();

        // $final_data = $this->orderDetail($row);
        // print_r( $final_data);die;
        // die;
	          	$options = [
				  'width' => 500,
				  'quality' => 90
				];
	          	$conv = new \Anam\PhantomMagick\Converter();
         		$conv->source(url('/').'/generate-pdf/'.$request->order_id.'/'.$this->header)
	            ->setImageOptions($options)
	            ->toJpg()
	            ->save(public_path('/order_invoice/order_'.$request->order_id.'.jpg'));
	            $image_path = asset('public/order_invoice/order_'.$request->order_id.'.jpg');
	          	return response()->json(['status' => true, 'message' => 'Image Path', 'image_path' => $image_path]);

	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function orderDetail($row)
    {

        $data = array();

        $tier = SpUser::leftJoin('tiers', function($join) {
                  $join->on('sp_users.tier_id', '=', 'tiers.id');
                })
            ->where('sp_users.id',$row['sp_id'])
            ->select('tiers.earning')
            ->first();

        $sr_name = 'name'.$this->col_postfix;
		$offer_name = 'title'.$this->col_postfix;

        $data['id'] = $row['id'];
        $data['booking_unique_id'] = $row['booking_unique_id'];
        $data['sp_id'] = $row['sp_id'];
        $data['sp_store_name'] = $row->getAssociatedSpInformation->store_name;
        $data['sp_address'] = $row->getAssociatedSpInformation->address;
        $data['sp_landmark'] = $row->getAssociatedSpInformation->landmark;
        $data['sp_mobile_no'] = $row->getAssociatedSpInformation->store_number;
        $data['sp_profile_image'] = $row->getAssociatedSpInformation->profile_image!='' ? changeImageUrlForFileExist(asset('sp_uploads/profile/'.$row->getAssociatedSpInformation->profile_image)):'';
        // $data['sp_mobile_no'] = $row->getAssociatedSpInformation->mobile_no;
        $data['sp_country_code'] = $row->getAssociatedSpInformation->country_code;
        $data['user_id'] = $row['user_id'];
        $data['staff_id'] = $row['staff_id'];
        if($row['staff_id']!='' && $row['staff_id']!=null){
            $data['staff_name'] = $row->getAssociatedStaffInformation->staff_name;
        }else{
            $data['staff_name'] = "";
        }
        $data['is_gift'] = $row['is_gift'];
        $data['gifted_by'] = $row['gifted_by'];
        $data['sender_name'] = $row['gifted_by']!='' && $row['gifted_by']!=null ? $row->getAssociatedGiftByUserInfo->name:'';
        $data['receiver_name'] = $row['receiver_name'];
        $data['receiver_mobile_no'] = $row['receiver_mobile_no'];

        $data['appointment_time'] = $row['appointment_time'];
        $data['appointment_date'] = $row['appointment_date']!='' && $row['appointment_date']!= null ? $row['appointment_date']:"";
        $data['redeemed_reward_points'] = $row['redeemed_reward_points'];
        $data['payment_type'] = $row['payment_type'];
        $data['promo_code_amount'] = round($row['promo_code_amount'],2);
        $data['tax_amount'] = round($row['tax_amount'],2);
        $data['total_amount'] = round($row['booking_total_amount'],2);
        $data['paid_amount'] = round($row['paid_amount'],2);
        $data['booking_service_type'] = $row['booking_service_type'];
        $data['total_item_amount'] = round($row['total_item_amount'],2);

        if($row['db_user_name'] == '' || $row['db_user_name'] == null){
            $data['total_cashback'] = round(($row['booking_total_amount']*$tier->earning)/100,0);
        }else{
            $data['total_cashback'] = 0;
        }
        
        $data['status'] = $row['status'];
        $data['created_date'] = date('Y-m-d',strtotime($row['created_at']));

        $review = Review::where('user_id',$row['user_id'])->where('booking_id',$row['id'])->first();

        if($review){
            $data['review_description'] = $review->description;
        }else{
            $data['review_description'] = "";
        }

        $item_name_arr = array();
        foreach($row->getAssociatedBookingItems as $bkitems){

            if($bkitems['is_service'] == '1')
            {
                $item_name_arr[] = [
                    'service_id' => $bkitems['booking_item_id'],
                    'name' => $bkitems->getAssociatedServiceDetail->getAssociatedService->$sr_name,
                    'quantity' => $bkitems['quantity'],
                    'original_price' => round($bkitems['original_price'],2),
                    'best_price' => round($bkitems['best_price'],2),
                    'cashback' => round($bkitems['cashback'],2),
                ];
            }
            else{
                $offer_services = array();

                if(isset($bkitems->getAssociatedOfferDetail->getAssociatedOfferServices)){
                	foreach($bkitems->getAssociatedOfferDetail->getAssociatedOfferServices as $os){

	                    $offer_services[] = [
	                        'service_id' => $os['sp_service_id'],
	                        'quantity' => $os['quantity'],
	                        'name' => $os->getAssociatedOfferServicesName->getAssociatedService->$sr_name,
	                    ];
	                }


	                $item_name_arr[] = [
	                    'offer_id' => $bkitems['booking_item_id'],
	                    'name' => $bkitems->getAssociatedOfferDetail->getAssociatedOfferName->$offer_name,
	                    'quantity' => $bkitems['quantity'],
	                    'original_price' => round($bkitems['original_price'],2),
	                    'best_price' => round($bkitems['best_price'],2),
	                    'cashback' => round($bkitems['cashback'],2),
	                    'services'=>$offer_services,
	                ];
                }
            }

        }
        $data['booking_items'] = $item_name_arr;
        // print_r($data);die;
        return $data;
                
    }

	public function checkGiftedUserExistence(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	              // 'receiver_name' => 'required',
	              'receiver_country_code' => 'required',
	              'receiver_mobile_no' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
			
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;


	          	$row = User::where('country_code',$request->receiver_country_code)
		          		->where('mobile_no',$request->receiver_mobile_no)
		          		// ->where('name',$request->receiver_name)
				      	->exists();
				$msg = '';
				if($row){
					// $msg = 'Congrats we found name of the recipient on Beutics. Will send the gift instantly.';
					// $msg = 'Hey Guess what? I was able to track the recipient on Beutics platform. I can send your gift in a jiffy!';
					$msg = __('messages.Gifted user found');
				}else{
					$msg = __('messages.Gifted user not found');
				}
	          	return response()->json(['status' => true, 'message' => $msg]);

	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function addUserCard(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	              'transaction_id' => 'required',
	              'payment_reference' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
			
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;


	          	//check transaction id in transaction table

	          	$check_pay_ref = Transaction::where('payment_reference',$request->payment_reference)->first();

	          	$check_trans_id = Transaction::where('transaction_id',$request->transaction_id)->first();

	          	if($check_pay_ref && $check_trans_id){
	          		//check card exists or not
		          	$check = UserSavedCard::where('user_id',$user_id)->where('card_last_4_digits',$check_trans_id->last_4_digits)->where('card_first_4_digits',$check_trans_id->first_4_digits)->first();

		          	if($check){
						return response()->json(['status' => false, 'message' =>  __('messages.Card already added')]);
		          	}else{
		          		$row = new UserSavedCard();
			          	$row->user_id = $user_id;
			          	$row->email = $check_pay_ref->pt_customer_email;
			          	$row->password = $check_pay_ref->pt_customer_password;
			          	$row->token_no = $check_pay_ref->pt_token;
			          	$row->card_last_4_digits = $check_trans_id->last_4_digits;
			          	$row->card_first_4_digits = $check_trans_id->first_4_digits;
			          	$row->card_type = $check_trans_id->card_brand;
			          	$row->save();
			          	if($row->save()){
			          		return response()->json(['status' => true, 'message' => __('messages.add card'),'data'=>$row]);
			          	}
		          	}
          			
	          	}else{
	          		return response()->json(['status' => false]);
	          	}
	          	
	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function deleteUserCard(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	              'id' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
			
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

	          	$row = UserSavedCard::where('id',$request->id)->first();
	          	if($row){
	          		$row->delete();
		          	return response()->json(['status' => true, 'message' => __('messages.delete card Successfully')]);
		          	
	          	}else{
	          		return response()->json(['status' => false, 'message' => 'No Record Found.','data'=>[]]);
	          	}
	          	
	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function getUserCards(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
			
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          	$rows = UserSavedCard::where('user_id',$user_id)->get();
	          	
	          	if($rows){
	          		return response()->json(['status' => true, 'message' => 'Saved Cards','data'=>$rows]);
	          	}else{
	          		return response()->json(['status' => true, 'message' => 'No Saved Cards','data'=>[]]);
	          	}
	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function checkGiftAcceptanceCron()
    {

    	$current_date = date('Y-m-d');

        $rows = Booking::where('is_gift','1')
        ->select('appointment_date','id','paid_amount','gifted_by')
        ->where('status','1')
        ->where(function ($query) {
            $query->whereNull('user_id')
            ->orWhere('user_id','');
        })
        ->get();
        $setting = Setting::first();
        
        foreach($rows as $val){
        	$appointment_date = $val['appointment_date'];
        	$cancellation_date = date('Y-m-d', strtotime($appointment_date. ' + '.$setting['gift_cancellation_days'].' days'));
        	
        	if(strtotime($current_date) == strtotime($cancellation_date)){
        		$book = Booking::where('id',$val['id'])->first();
        		$book->status = '2';
        		$book->save();


        		//refund amount
        		$refunded_amount = $val['paid_amount'] - ($val['paid_amount']*5)/100;

        		$user = User::where('id',$val['gifted_by'])->first();
        		$user->wallet_amount = $user->wallet_amount+$refunded_amount;
        		$user->save();

        		$wallet_row = new WalletCashHistory();
                   
                $wallet_row->user_id = $val['gifted_by'];
                $wallet_row->trans_ref_id = $val['id'];
                $wallet_row->amount = $refunded_amount;
                $wallet_row->transaction_type = '0';
                $wallet_row->description = 'Refund';
                $wallet_row->transaction_date_time = date('Y-m-d H:i:s');
                $wallet_row->save();   

        	}
        }
        echo "done";die;

        // print_r($booking);die;
    }

    public function checkOrderCompletionCron()
    {
    	$current_date = date('Y-m-d');

        $rows = Booking::where('status','0')
		        ->orWhere('status','1')
		        ->get()->toArray();
		        
		$setting = Setting::first();
        
        foreach($rows as $val){
        	$order_placed_date = date('Y-m-d',strtotime($val['created_at']));
        	$expire_date = date('Y-m-d', strtotime($order_placed_date. ' + '.$setting['order_expiration_days'].' days'));

        	if(strtotime($current_date) == strtotime($expire_date)){
        
				$book = Booking::where('id',$val['id'])->first();
				$book->status = '4';
		        $book->save();
        	}
        	
        }
        echo "done";die;

        // print_r($booking);die;
    }

    public function sendGiftCron()
    {
    	$current_date = date('Y-m-d');

        $rows = Booking::where('gift_send_date','!=','')
		        ->whereNotNull('gift_send_date')
		        ->where('user_id','!=','')
		        ->whereNotNull('user_id')
		        ->get();
       
        foreach($rows as $val){

        	if(strtotime($current_date) == strtotime($val['gift_send_date'])){
        		
        		if($val->getAssociatedSpInformation->notification_alert_status == '1')
				{
					// $message = 'Congratulations '.$val->getAssociatedSpInformation->store_name.'! You have received a new booking from '.$val->getAssociatedUserInfo->name.'. Allocate and approve the booking NOW.';

					$message_main = __('messages.New Booking Received',['store_name' => $val->getAssociatedSpInformation->store_name,'user_name' => $val->getAssociatedUserInfo->name]);

	          		$message_title_main = __('messages.New Booking Received!');
	          		app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

					$message_other = __('messages.New Booking Received',['store_name' => $val->getAssociatedSpInformation->store_name,'user_name' => $val->getAssociatedUserInfo->name]);

	          		$message_title_other = __('messages.New Booking Received!');

			        Notification::saveNotification($val->sp_id,$val->user_id,$val->id,'NEW_BOOKING',$message_main,$message_other,$message_title_main,$message_title_other,'sp');
			        app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

				}
				

		        $item_name_arr1 = array();

		        if($val->getAssociatedSpInformation->email!='' && $val->getAssociatedSpInformation->verifyToken ==''){

		        	$mail_data['templete'] = "sp_new_booking_mail";
			        $mail_data['name'] = $val->getAssociatedSpInformation->store_name;
			        $mail_data['customer_name'] = $val->getAssociatedUserInfo->name;
			        $mail_data['email'] = $val->getAssociatedSpInformation->email;
			        $mail_data['subject'] = "You have a new booking! ";
			        foreach($val->getAssociatedBookingItems as $bkitems){
						if($bkitems['is_service'] == '1')
						{
							if(isset($bkitems->getAssociatedServiceDetail->getAssociatedService->name)){
								$item_name_arr1[] = $bkitems->getAssociatedServiceDetail->getAssociatedService->name;
							}
							
						}
						else{
							$item_name_arr1[] = $bkitems->getAssociatedOfferDetail->getAssociatedOfferName->title;
						}

					}

			        $mail_data['services'] = implode(',',$item_name_arr1);
			        send($mail_data);

		        }
		        $sender = User::where('id',$val['gifted_by'])->first();
		        $item_name_arr = array();
		        if($val->receiver_email && (int)$val->is_gift==1){
		       
		        	$mail_data['templete'] = "gift_service_mail";
			        $mail_data['sender_name'] = $sender->name;
			        $mail_data['name'] = $val->receiver_name;
			        $mail_data['receiver_message'] = $val->receiver_message;
			        $mail_data['email'] = $val->receiver_email;
			        $mail_data['subject'] = $sender->name." has sent you a Gift!";

			        foreach($val->getAssociatedBookingItems as $bkitems){
						if($bkitems['is_service'] == '1')
						{
							if(isset($bkitems->getAssociatedServiceDetail->getAssociatedService->name)){
								$item_name_arr[] = $bkitems->getAssociatedServiceDetail->getAssociatedService->name;
							}
							
						}
						else{
							$item_name_arr[] = $bkitems->getAssociatedOfferDetail->getAssociatedOfferName->title;
						}

					}

			        $mail_data['services'] = implode(',',$item_name_arr);
			       
			        
			        send($mail_data);
		        }

		        //notification
// 
		        $gift_sender_info = User::where('id',$val['gifted_by'])->first();
		       
          		if($val['user_id']!='' && $val['user_id']!=null && $val['gift_send_date'] != null && $val['is_gift']=='1' && $val->getAssociatedUserInfo->notification_alert_status == '1'){

          			
	          		// $message = 'Your friend '.$gift_sender_info->name.' has sent you a gift. You are certainly special and your friendship. Cherish it! The Gift has been placed in “New Bookings” section.';
	          		$message_main = __('messages.You have received a Gift',['name' => $gift_sender_info->name]);

	          		$message_title_main = __('messages.You have received a Gift!');

	          		app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');


					$message_other = __('messages.You have received a Gift',['name' => $gift_sender_info->name]);

	          		$message_title_other = __('messages.You have received a Gift!');

	          		// $db_message = 'You have received a gift from ';
			        Notification::saveNotification($val['user_id'],$val['sp_id'],$val['id'],'GIFT_RECEIVED',$message_main,$message_other,$message_title_main,$message_title_other,'customer');
			        app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');


          		}


        		
    //     		$sender = User::where('id',$val['gifted_by'])->first();
				// $mail_data['templete'] = "gift_service_mail";
		  //       $mail_data['name'] = $sender->name;
		  //       $mail_data['email'] = $val['receiver_email'];
		  //       $mail_data['subject'] = "Gift Received";
		  //       $mail_data['message'] = "You have received a gift from ".$sender->name.".";
		        
		  //       send($mail_data);

				// Notification::saveNotification($row->user_id,$row->id,'GIFT_RECEIVED',$message,'customer');
        	}
        	
        }
        echo "done";die;

        // print_r($booking);die;
    }

    public function sendGiftCardCron()
    {
    	$current_date = date('Y-m-d');

        $rows = ECard::where('card_send_date','!=','')
		        ->whereNotNull('card_send_date')
		        ->where('receiver_id','!=','')
		        ->whereNotNull('receiver_id')
		        ->get();

        foreach($rows as $val){

        	if(strtotime($current_date) == strtotime($val['card_send_date'])){
        		
        		if($val->status!='1'){
        			$usr = User::where('id',$val->receiver_id)->first();

	        		if($usr){
	        			$wallet_amount = $usr->wallet_amount+$val->amount;

		          		$usr->wallet_amount = $wallet_amount;
		          		$usr->save();

		          		$wallet_hist = new WalletCashHistory();
		      			$wallet_hist->user_id = $val->receiver_id;
		      			$wallet_hist->trans_ref_id = $val->id;
		      			$wallet_hist->transaction_type = '0';
		          		$wallet_hist->amount = $val->amount;
		          		$wallet_hist->description = 'e-giftcard';
		          		$wallet_hist->transaction_date_time = date('Y-m-d H:i:s');
		          		$wallet_hist->save();
	        		}

	        		$ecard = ECard::where('id',$val['id'])->update(array('status'=>'1','card_received_date'=>date('Y-m-d H:i:s')));

	        		$sender_info = User::where('id',$val['user_id'])->first();

	        		if(isset($val->getAssociatedReceiverInfo->notification_alert_status) && $val->getAssociatedReceiverInfo->notification_alert_status == '1'){

		          		// $message = 'Your friend '.$sender_info->name.' has remembered your '.$val->getAssociatedOccasionInfo->name.'. He has sent you a Gift Cash Card. You have a special friendship! Your wallet has been topped up with the cash card. Enjoy shopping';

		          		$message_main = __('messages.Promo Code minimum order amount',['sender_name' => $sender_info->name,'occasion_name' => $val->getAssociatedOccasionInfo->name]);
		          		$message_title_main = __('messages.You have received a Gift Card!');

		          		app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');


						$message_other = __('messages.Promo Code minimum order amount',['sender_name' => $sender_info->name,'occasion_name' => $val->getAssociatedOccasionInfo->name]);
		          		$message_title_other = __('messages.You have received a Gift Card!');

				        Notification::saveNotification($val->receiver_id,$val->user_id,$val->id,'E_GIFT_CARD_RECEIVED',$message_main,$message_other,$message_title_main,$message_title_other,'customer');
	          		}


	        		
					if($val->receiver_email!=null){
						

			        	$mail_data['templete'] = "ecard_gift_mail";
				        $mail_data['sender_name'] = $sender_info->name;
				        $mail_data['receiver_name'] = $val->receiver_name;
				        $mail_data['receiver_message'] = $val->receiver_message;
				        $mail_data['gift_card_amount'] = $val->amount;
				        $mail_data['email'] = $val->receiver_email;
				        $mail_data['occasion'] = $val->getAssociatedOccasionInfo->name;
				        $mail_data['theme'] = ($val->getAssociatedOccasionThemeInfo->image!='' && $val->getAssociatedOccasionThemeInfo->image!=null)? changeImageUrl(asset('public/eCardThemes/'.$val->getAssociatedOccasionThemeInfo->image)):'';
				        $mail_data['subject'] = $sender_info->name." remembers your ".$val->getAssociatedOccasionInfo->name." with a Gift!";
				        
				        send($mail_data);
			        }
        		}
        	}
        	
        }
        echo "done";die;

        // print_r($booking);die;
    }


    //Appointment reminder – customer.
    public function appointmentReminderCron()
    {  
    	// 2 hour	
    	$bookings_two_hours = booking::leftJoin('sp_users', function($join) {
			      $join->on('bookings.sp_id', '=', 'sp_users.id');
		    })
    		->leftJoin('users', function($join) {
			      $join->on('bookings.user_id', '=', 'users.id');
		    })
    		->where('users.notification_alert_status', '1')
    		->where('bookings.status', '1') 
    		->where('bookings.appointment_date', date('Y-m-d'))
    		->where('bookings.appointment_time', date('H:i', strtotime('+2 hours')).':00')
    		->select('bookings.id','bookings.sp_id','bookings.user_id','sp_users.store_name')
    		->get(); 

    	if(!empty($bookings_two_hours)){
    		$message = '';
    		
    		foreach($bookings_two_hours as $rows){
    			// $message = "(2 hours): Just a quick reminder. The service that you booked at ".$rows->store_name." is due in 2 hours. If you cannot make it, kindly reschedule to avoid any inconvenience to the store. Go to My Orders>>Upcoming>>Select your order>>Reschedule"; 

    			$message_main = __('messages.Upcoming Appointment in 2 hours',['store_name' => $rows->store_name]);
    			$message_title_main = __("messages.Upcoming Appointment in 2 hours!");

    			app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

				$message_other = __('messages.Upcoming Appointment in 2 hours',['store_name' => $rows->store_name]);
    			$message_title_other = __("messages.Upcoming Appointment in 2 hours!");

	    		Notification::saveNotification($rows->user_id,$rows->sp_id,$rows->id,'CONFIRM_ORDER',$message_main,$message_other,$message_title_main,$message_title_other,'customer');
	    		app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');
	    	}
    	}

    	// 1 hour
    	$bookings_one_hours = booking::leftJoin('sp_users', function($join) {
			      $join->on('bookings.sp_id', '=', 'sp_users.id');
		    })
    		->leftJoin('users', function($join) {
			      $join->on('bookings.user_id', '=', 'users.id');
		    })
    		->where('users.notification_alert_status', '1')
    		->where('bookings.status', '1') 
    		->where('bookings.appointment_date', date('Y-m-d'))
    		->where('bookings.appointment_time', date('H:i', strtotime('+1 hour')).':00')
    		->select('bookings.id','bookings.sp_id','bookings.user_id', 'sp_users.store_name')
    		->get(); 
    	
    	if(!empty($bookings_one_hours)){
    		$message = '';
    		
    		foreach($bookings_one_hours as $row){
    			// $message = "(1 hour): Nearing your booking time. Your appointment is just an hour away. If you cannot make it, kindly reschedule to avoid any inconvenience to the store. Go to My Orders>>Upcoming>>Select your order>>Reschedule";
    			$message_title_main = __("messages.Upcoming Appointment in 1 hour!");
    			$message_main = __('messages.Upcoming Appointment in 1 hour');
    			app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');
				$message_title_other = __("messages.Upcoming Appointment in 1 hour!");
    			$message_other = __('messages.Upcoming Appointment in 1 hour');

	    		Notification::saveNotification($row->user_id,$row->sp_id,$row->id,'CONFIRM_ORDER',$message_main,$message_other,$message_title_main,$message_title_other,'customer');
	    	}
    	}
    	die('Appointment reminder – customer - Cron Run Successfully');
    }


    public function calculateTotalNetSaving(Request $request)
    {
    	if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
			
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          	
	          	$rows = Booking::where('user_id',$user_id)
	          			 ->select(DB::raw("ROUND(SUM(net_saving),2) as total_savings"))
	          			 ->groupBy('user_id')
	          			->get();


	          	
	          	if($rows){
	          		return response()->json(['status' => true, 'message' => 'Total Saving','data'=>$rows]);
	          	}
	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
    }

    public function walletTransactions(Request $request)
    {
    	if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	              'page' => 'required',	
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          	$usr_bal_status = $row = User::where('id',$user_id)->select('low_balance_alert_status')->first();

	          	$currentpage = $request->page;

          		Paginator::currentPageResolver(function () use ($currentpage) {
			        return $currentpage;
			    });


	          	$trans = WalletCashHistory::where('user_id',$user_id)->orderBy('id','DESC')
	          			// ->select("*",
		            //         \DB::raw('(CASE 
		            //             WHEN transaction_type = "0" THEN "credit" 
		            //             WHEN transaction_type = "1" THEN "debit" 
		            //             END) AS transaction_type'))
	          	->paginate(10);
	          	$rows_arr = $trans->toArray();
	          	// ->toArray();
	          	// print_r($trans);die;
	          	
	          	$result_arr_1 = array();
	          	if(count($trans)>0){

	          		foreach($trans as $val){
	          			$result_arr = array();
	          			// print_r($val);die;
		          		$result_arr['id'] = $val['id'];
		          		$result_arr['amount'] = round($val['amount'],2);
		          		$result_arr['transaction_type'] = $val['transaction_type']=='0'?'credit':'debit';
		          		$result_arr['description'] = $val['description'];
		          		$result_arr['transaction_date_time'] = ($val->transaction_date_time!='' && $val->transaction_date_time!=null)? $val->transaction_date_time:'';
		          		
		          		
	          			if(($val['description'] == "Refund" && $val['transaction_type'] == "0") || ($val['transaction_type'] == "1" && $val['description'] != "e-giftcard" )){

	          				$result_arr['transaction_id'] = $val['trans_ref_id'];
	          				$result_arr['booking_unique_id'] = isset($val->getAssociatedBookingDetails->booking_unique_id)?$val->getAssociatedBookingDetails->booking_unique_id:'';
	          				$result_arr['sp_id'] = isset($val->getAssociatedBookingDetails->sp_id)?$val->getAssociatedBookingDetails->sp_id:'';
	          				$result_arr['sp_name'] = isset($val->getAssociatedBookingDetails->getAssociatedSpInformation->store_name)?$val->getAssociatedBookingDetails->getAssociatedSpInformation->store_name:'';
	          				$result_arr['sp_image'] = isset($val->getAssociatedBookingDetails->getAssociatedSpInformation->profile_image)? changeImageUrlForFileExist(asset('sp_uploads/profile/'.$val->getAssociatedBookingDetails->getAssociatedSpInformation->profile_image)):'';

	          				
	          			}

	          			elseif($val['description'] == "e-giftcard") {

	          				$result_arr['transaction_id'] = $val['trans_ref_id'];
	          				if($val['transaction_type'] == "0"){
	          					// $result_arr['user_id'] = $val->getAssociatedECardDetails->user_id;
	          					$result_arr['user_name'] = $val->getAssociatedECardDetails->getAssociatedUserInfo->name;
	          				}else{
	          					
	          					// $result_arr['user_id'] = $val->getAssociatedECardDetails->user_id;
	          					if($val->getAssociatedECardDetails->receiver_id != "" && $val->getAssociatedECardDetails->receiver_id != null){

	          						$result_arr['user_name'] = $val->getAssociatedECardDetails->getAssociatedReceiverInfo->name;
	          					}else{
	          						$result_arr['user_name'] = $val->getAssociatedECardDetails->receiver_name;
	          					}
	          					
	          				}

	          				$result_arr['gift_theme'] = ($val->getAssociatedECardDetails->getAssociatedOccasionThemeInfo->image!='' && $val->getAssociatedECardDetails->getAssociatedOccasionThemeInfo->image!=null)? changeImageUrl(asset('public/eCardThemes/'.$val->getAssociatedECardDetails->getAssociatedOccasionThemeInfo->image)):'';
	          				$result_arr['gift_message'] = $val->getAssociatedECardDetails->receiver_message;
	          				$result_arr['gift_occasion'] = $val->getAssociatedECardDetails->getAssociatedOccasionInfo->name;
	          				
	          			}
	          			else{
	          				$result_arr['transaction_id'] = $val['trans_ref_id'];
	          			}

	          			$result_arr_1[] = $result_arr;
		          		
		          	}

		          	$result['data'] = $result_arr_1;
	                $result['current_page']     = $rows_arr['current_page'];
	                $result['from']             = $rows_arr['from'];
	                $result['last_page']        = $rows_arr['last_page'];
	                $result['next_page_url']    = $rows_arr['next_page_url'];
	                $result['per_page']         = $rows_arr['per_page'];
	                $result['prev_page_url']    = $rows_arr['prev_page_url'];
	                $result['to']               = $rows_arr['to'];
	                $result['total']            = $rows_arr['total'];
		          	return response()->json(['status' => true, 'message' => 'My Cash Transactions', 'data' => $result,'low_bal_alert_status'=>$usr_bal_status->low_balance_alert_status]);
		          	}else{
		          		return response()->json(['status' => false, 'message' => 'No Record Found', 'data' => []]);
		          	}

	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage().$e->getLine(), 'data' => []]);
	       }
		}
    }

    public function addMoneyToWallet(Request $request)
    {
    	if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	              'amount' => 'required',	
	              'transaction_ref_id' => 'required',	
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

	          	$user = User::where('id',$user_id)->first();
	          	$user->wallet_amount = $user->wallet_amount + $request->amount;
	          	$user->save();

	          	$wallet_row = new WalletCashHistory();
                   
                $wallet_row->user_id = $user_id;
                $wallet_row->trans_ref_id = $request->transaction_ref_id;
                $wallet_row->amount = $request->amount;
                $wallet_row->transaction_type = '0';
                $wallet_row->description = $request->description;
                $wallet_row->transaction_date_time = date('Y-m-d H:i:s');
                $wallet_row->save();  

                if($wallet_row->getAssociatedUserDetails->notification_alert_status == '1'){

                	// $message = $request->amount.' has been credited to your Beutics wallet successfully on '.date('Y-m-d H:i:s').'.';

                	$message_main = __('messages.Top up Successful',['amount' => $request->amount,'date' => date('Y-m-d H:i:s')]);
				    $message_title_main = __('messages.Top up Successful!');

				   app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

					$message_other = __('messages.Top up Successful',['amount' => $request->amount,'date' => date('Y-m-d H:i:s')]);
				    $message_title_other = __('messages.Top up Successful!');

	          		
		          	Notification::saveNotification($user_id,'','','TOPUP_WALLET',$message_main,$message_other,$message_title_main,$message_title_other,'customer');
		          	app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');
                }
               
                
	          	return response()->json(['status' => true, 'message' => __('messages.add wallet')]);

	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
    }


    public function getOccasions(Request $request)
    {
    	if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          	$col = 'name'.$this->col_postfix;
	          	$rows = Occasion::select('id',$col.' AS name')->orderBy('id','DESC')->get();
	         
	          	return response()->json(['status' => true, 'message' => 'Occasions', 'data' => $rows]);

	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
    }

    public function getOccasionThemes(Request $request)
    {
    	if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	              'occasion_id' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          	$base_path = changeImageUrl(asset('public/eCardThemes')).'/';
	          	
	          	$rows = GiftTheme::where('occasion_id',$request->occasion_id)->select('id',DB::raw('CONCAT("'.$base_path.'", gift_themes.image) AS image'))->orderBy('id','DESC')->get();
	         
	          	return response()->json(['status' => true, 'message' => 'Themes', 'data' => $rows]);

	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
    }

    public function getFaqCategories(Request $request)
    {
    	if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          	$col = 'name'.$this->col_postfix;
	          	$rows = FaqCategory::select('id',$col.' AS name')->orderBy('id','ASC')->get();
	         
	          	return response()->json(['status' => true, 'message' => 'Faq Categories', 'data' => $rows]);

	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
    }

    public function getFaqs(Request $request)
    {
    	if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	              'faq_category_id' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          	$question = 'question'.$this->col_postfix;
	          	$answer = 'answer'.$this->col_postfix;
	          	$rows = Faq::where('faq_category_id',$request->faq_category_id)->select('id',$question.' AS question',$answer.' AS answer')->orderBy('id','ASC')->get();
	         
	          	return response()->json(['status' => true, 'message' => 'Faqs', 'data' => $rows]);

	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
    }

    public function contactUs(Request $request)
    {
    	if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	              'name' => 'required',
	              'email' => 'required',
	              'message' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          	
	          	$row = new Contact();
	          	$row->name = $request->name;
	          	$row->email = $request->email;
	          	$row->message = $request->message;
	          	if($row->save()){
		        	$mail_data['templete'] = "contact_us_mail";
			        $mail_data['name'] = $request->name;
			        $mail_data['email'] = 'contact@beutics.com';
			        $mail_data['subject'] = "New Contact Enquiry";
			        $mail_data['message'] = $request->message;
			        
			        send($mail_data);

	          		return response()->json(['status' => true, 'message' => __('messages.contact us')]);
	          	}
	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
    }

    public function sendGiftCard(Request $request)
    {
    	if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	              'occasion_id' => 'required',
	              'theme_id' => 'required',
	              'gift_card_amount' => 'required',
	              'receiver_country_code' => 'required',	
	              'receiver_mobile_number' => 'required',	
	              'receiver_name' => 'required',	
	              'receiver_email' => 'required',	
	              'receiver_message' => 'required',	
	              'is_send_now' => 'required',	
	              'is_wallet_used' => 'required',	
	              'is_card_used' => 'required',	
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          	$unique_id = $this->generateGiftCardRandomNumbers();
		        $unique_id_1 = $unique_id;

	          	$check_user = User::where('country_code',$request->receiver_country_code)->where('mobile_no',$request->receiver_mobile_number)->first();

	          	$row = new ECard();
	          	$row->occasion_id = $request->occasion_id;
	          	$row->theme_id = $request->theme_id;
	          	$row->user_id = $user_id;
	          	$row->amount = $request->gift_card_amount;
	          	$row->receiver_mobile_no = $request->receiver_country_code.'-'.$request->receiver_mobile_number;
	          	$row->receiver_name = $request->receiver_name;
	          	$row->receiver_email = $request->receiver_email;
	          	$row->receiver_message = $request->receiver_message;
	          	$row->card_unique_id = $unique_id_1;
	          	if((int)$request->is_send_now!=1){
	          		$row->card_send_date = $request->card_send_date;
	          	}

	          	//check user exists or not 

	          	

	          	// if((int)$request->is_send_now ==1){
	          	// 	$row->status = '1';
	          	// }
	          	if($check_user){
	          		$row->receiver_id = $check_user->id;

	          		if((int)$request->is_send_now ==1){
		          		$row->status = '1';
		          		$row->card_received_date = date('Y-m-d H:i:s');
		          	}

	          	}
	          	// else{
	          	// 	$row->status = '0';
	          	// }
	          	
	          	if($row->save()){

	          		if((int)$request->is_wallet_used == 1){

	          			$booking_payment = new BookingPaymentDetail();
	          			$booking_payment->booking_id = $row->id;
	          			$booking_payment->payment_option = 'wallet';
	          			$booking_payment->booking_type = 'ecard';
		          		$booking_payment->amount = $request->wallet_amount;
		          		$booking_payment->save();



		          		//add row in wallet history

		          		$wallet_hist = new WalletCashHistory();
	          			$wallet_hist->user_id = $user_id;
	          			$wallet_hist->trans_ref_id = $row->id;
	          			$wallet_hist->transaction_type = '1';
		          		$wallet_hist->amount = $request->wallet_amount;
		          		$wallet_hist->description = 'e-giftcard';
		          		$wallet_hist->transaction_date_time = date('Y-m-d H:i:s');
		          		$wallet_hist->save();



		          		//deduct amount from wallet

		          		$usr = User::where('id',$user_id)->first();

		          		$wallet_amount = $usr->wallet_amount-$request->wallet_amount;

		          		$usr->wallet_amount = $wallet_amount;
		          		$usr->save();



		          	}

		          	if((int)$request->is_card_used == 1){
		          		$booking_card_payment = new BookingPaymentDetail();
	          			$booking_card_payment->transaction_id = $request->transaction_id;
	          			$booking_card_payment->booking_id = $row->id;
	          			$booking_card_payment->card_no = $request->card_no;
	          			$booking_card_payment->reference_no = $request->reference_no;
	          			$booking_card_payment->payment_option = 'card';
	          			$booking_card_payment->booking_type = 'ecard';
		          		$booking_card_payment->amount = $request->card_amount;
		          		$booking_card_payment->save();

		          	}
		         
		          	$sender_info = User::where('id',$user_id)->first();
	          		if($check_user && (int)$request->is_send_now ==1){
	          			//update receiver wallet amount
	      				$usr = User::where('id',$check_user->id)->first();

		          		$wallet_amount = $usr->wallet_amount+$request->gift_card_amount;

		          		$usr->wallet_amount = $wallet_amount;
		          		$usr->save();

		          		$wallet_hist = new WalletCashHistory();
	          			$wallet_hist->user_id = $check_user->id;
	          			$wallet_hist->trans_ref_id = $row->id;
	          			$wallet_hist->transaction_type = '0';
		          		$wallet_hist->amount = $request->gift_card_amount;
		          		$wallet_hist->description = 'e-giftcard';
		          		$wallet_hist->transaction_date_time = date('Y-m-d H:i:s');
		          		$wallet_hist->save();
		          		

						if($row->receiver_id!='' && $row->receiver_id!=null && $request->is_send_now==1 && $row->getAssociatedReceiverInfo->notification_alert_status == '1'){

			          		// $message = 'Your friend '.$sender_info->name.' has remembered your '.$row->getAssociatedOccasionInfo->name.'. He has sent you a Gift Cash Card. You have a special friendship! Your wallet has been topped up with the cash card. Enjoy shopping';

			          		$message_main = __('messages.You have received a Gift Card',['sender_name' => $sender_info->name,'occasion_name' => $row->getAssociatedOccasionInfo->name]);

			          		$message_title_main =  __('messages.You have received a Gift Card!');
			          		app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

							$message_other = __('messages.You have received a Gift Card',['sender_name' => $sender_info->name,'occasion_name' => $row->getAssociatedOccasionInfo->name]);

			          		$message_title_other = __('messages.You have received a Gift Card!');

					        Notification::saveNotification($row->receiver_id,$row->user_id,$row->id,'E_GIFT_CARD_RECEIVED',$message_main,$message_other,$message_title_main,$message_title_other,'customer');
					        app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');
		          		}

		          		

		          		if( $request->is_send_now==1 && $sender_info->notification_alert_status == '1'){

			          		// $message = 'On occasion of your friend '.$request->receiver_name.' '.$row->getAssociatedOccasionInfo->name.', you have gifted a Cash Card worth AED '.$request->gift_card_amount.'. This amount will be credited into your friends Beutics Wallet to be used for any purchase on the platform.';

			          		$message_main = __('messages.Gift card notification',['receiver_name' => $request->receiver_name,'occasion_name' => $row->getAssociatedOccasionInfo->name,'amount' => $request->gift_card_amount]);

			          		$message_title_main = __('messages.Gift card notification!');
			          		app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');
							$message_other = __('messages.Gift card notification',['receiver_name' => $request->receiver_name,'occasion_name' => $row->getAssociatedOccasionInfo->name,'amount' => $request->gift_card_amount]);

			          		$message_title_other = __('messages.Gift card notification!');

					        Notification::saveNotification($row->user_id,$row->receiver_id,$row->id,'E_GIFT_CARD_SENT',$message_main,$message_other,$message_title_main,$message_title_other,'customer');
					        app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');
		          		}
				        
	          		}
	          		// send mail
	          		if($request->receiver_email!='' && (int)$request->is_send_now ==1){
	          			$mail_data['templete'] = "ecard_gift_mail";
				        $mail_data['sender_name'] = $sender_info->name;
				        $mail_data['receiver_name'] = $request->receiver_name;
				        $mail_data['receiver_message'] = $request->receiver_message;
				        $mail_data['gift_card_amount'] = $request->gift_card_amount;
				        $mail_data['email'] = $request->receiver_email;
				        $mail_data['occasion'] = $row->getAssociatedOccasionInfo->name;
				        $mail_data['theme'] = ($row->getAssociatedOccasionThemeInfo->image!='' && $row->getAssociatedOccasionThemeInfo->image!=null)? changeImageUrl(asset('public/eCardThemes/'.$row->getAssociatedOccasionThemeInfo->image)):'';

				        $mail_data['subject'] = $sender_info->name." remembers your ".$row->getAssociatedOccasionInfo->name." with a Gift!";
				        
				        send($mail_data);
	          		}

	          		//check wallet balance

	          		if((int)$request->is_wallet_used == 1){
	          			
		          			if($sender_info->low_balance_alert_status == '1'){
				          		$balance = $sender_info->wallet_amount;
				          		if($balance<100){
				          			// $message = 'Hey, the balance in your Beutics wallet is quite low '.$balance.'. AED Top-up your Wallet to enjoy seamless transactions, card-free benefits and a lot more. ';
				          			$message_main = $message = __('messages.You have received a Gift Card',['balance' => $balance]);
								     $message_title_main = __('messages.Alert! Low cash balance');

								    app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');
									$message_other = $message = __('messages.You have received a Gift Card',['balance' => $balance]);
								    $message_title_other = __('messages.Alert! Low cash balance');

							        Notification::saveNotification($user_id,null,null,'LOW_BALANCE',$message_main,$message_other,$message_title_main,$message_title_other,'customer');
							        app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

				          		}
				          	}
		          		}
	          		

	          		
	          		// return response()->json(['status' => true, 'message' => 'You have gifted a Cash Card worth AED '.$request->gift_card_amount.' to your friend '.$request->receiver_name.'. This amount will be credited to your friends Beutics Wallet to be used for any purchase on the platform.']);
		          		// app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

	          		return response()->json(['status' => true, 'message' => __('messages.gift card send msg',['amount' => $request->gift_card_amount,'receiver_name' => $request->receiver_name])]);
	          	}
	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage().$e->getLine(), 'data' => []]);
	       }
		}
    }

     public function eCardListing(Request $request)
    {
    	if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	              'page' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          	$currentpage = $request->page;

          		Paginator::currentPageResolver(function () use ($currentpage) {
			        return $currentpage;
			    });

	          	$rows = ECard::where('user_id',$user_id)->orderBy('id','DESC')->paginate(10);
	          	$rows_arr = $rows->toArray();
	          	
	          	$ecard_arr = array();

	          	foreach($rows as $val){

	          		if($val->receiver_id != "" && $val->receiver_id!= null){

  						$res_name = $val->getAssociatedReceiverInfo->name;
  					}else{
  						$res_name = $val->receiver_name;
  					}
  					if($this->header == 'en'){
  						$status = array('0'=>'pending','1'=>'received','2'=>'cancelled');
  					}else{
  						$status = array('0'=>'قيد الانتظار','1'=>'تم الاستلام','2'=>'ألغيت');
  					}
  					
	          		$ecard_arr[] = [
	          			"id"=>$val['id'],
	          			"card_unique_id"=>$val['card_unique_id'],
	          			"occasion_id"=>$val['occasion_id'],
	          			"occasion_name"=>$val->getAssociatedOccasionInfo->name,
	          			"theme_id"=>$val['theme_id'],
	          			"theme_name"=>($val->getAssociatedOccasionThemeInfo->image!='' && $val->getAssociatedOccasionThemeInfo->image!=null)? changeImageUrl(asset('public/eCardThemes/'.$val->getAssociatedOccasionThemeInfo->image)):'',
	          			"user_id"=>$val['user_id'],
	          			"receiver_id"=>$val['receiver_id'],
	          			"receiver_name"=>$res_name,
	          			"receiver_mobile_no"=>$val['receiver_mobile_no'],
	          			"receiver_email"=>$val['receiver_email'],
	          			"receiver_message"=>$val['receiver_message'],
	          			"card_send_date"=>($val['card_send_date']!='' && $val['card_send_date']!=null)?$val['card_send_date']:'',
	          			"amount"=>round($val['amount'],2),
	          			"status"=>$status[$val['status']],
	          			"order_date"=>date('Y-m-d',strtotime($val['created_at'])),
	          		];
	          	}

	          	$result['data'] = $ecard_arr;
                $result['current_page']     = $rows_arr['current_page'];
                $result['from']             = $rows_arr['from'];
                $result['last_page']        = $rows_arr['last_page'];
                $result['next_page_url']    = $rows_arr['next_page_url'];
                $result['per_page']         = $rows_arr['per_page'];
                $result['prev_page_url']    = $rows_arr['prev_page_url'];
                $result['to']               = $rows_arr['to'];
                $result['total']            = $rows_arr['total'];
	          	
	          	return response()->json(['status' => true, 'data' => $result]);
	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage().$e->getLine(), 'data' => []]);
	       }
		}
    }

    public function changeNotificationSetting(Request $request)
    {
    	if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	              'status' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          	
	          	$row = User::where('id',$user_id)->first();

	          	if($row){
	          		if($request->status == ""){
	          			$request->status = '0';
	          		}
	          		$row->notification_alert_status = (string)$request->status;
		          	$row->save();

		          	return response()->json(['status' => true,'data'=>$row]);
	          	}
	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
    }

    public function lowBalanceAlert(Request $request)
    {
    	if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	              'status' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          	$row = User::where('id',$user_id)->first();
	          	
	          	if($row){
	          		if($request->status == ""){
	          			$request->status = '0';
	          		}
	          		
	          		$row->low_balance_alert_status = (string)$request->status;
		          	$row->save();

		          	if($request->status == '1'){
		          		$balance = $row->wallet_amount;
		          		if($balance<100){
		          			
		          			// $message = 'Hey, the balance in your Beutics wallet is quite low '.round($balance,2).'. AED Top-up your Wallet to enjoy seamless transactions, card-free benefits and a lot more. ';

		          			$message_main = __('messages.Low cash balance',['balance' => round($balance,2)]);
		          			$message_title_main = __('messages.Alert! Low cash balance');


		          			if($this->header == 'en'){
		          				app()->setLocale('ar');
		          			}else{
		          				app()->setLocale('en');
		          			}

		          			$message_other = __('messages.Low cash balance',['balance' => round($balance,2)]);
		          			$message_title_other = __('messages.Alert! Low cash balance');
		          			
			          		// $db_message = 'Your wallet amount reached below 100 AED.';
		          			
					        Notification::saveNotification($row->id,null,null,'LOW_BALANCE',$message_main,$message_other,$message_title_main,$message_title_other,'customer');
		          		}
		          	}

		          	return response()->json(['status' => true]);
	          	}
	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
    }

    public function notificationsListing(Request $request)
    {
    	if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	              'page' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

	          	$currentpage = $request->page;

          		Paginator::currentPageResolver(function () use ($currentpage) {
			        return $currentpage;
			    });

	          	$rows = Notification::where('user_id',$user_id)->where('user_type','customer')->orderBy('id','DESC')->paginate(10);
	          	$rows_arr = $rows->toArray();

	          	$notification_arr_1 = array();
	          	$ref_name = '';
	          	if($rows){
	          		foreach($rows as $val){

	          			$notification_arr = array();

	          			$notification_arr['id'] = $val['id'];
	          			$notification_arr['ref_id'] = $val['ref_id'];
	          			$notification_arr['ref_user_id'] = $val['ref_user_id'];

	          			if($val['ref_type']!='E_GIFT_CARD_RECEIVED' && $val['ref_type']!='LOW_BALANCE')
	          			{
	          				$ref_name = isset($val->getAssociatedRefSpInfo->store_name)?$val->getAssociatedRefSpInfo->store_name:'';

	          				$notification_arr['sp_name'] = isset($val->getAssociatedRefSpInfo->store_name)?$val->getAssociatedRefSpInfo->store_name:'';
	          				$notification_arr['sp_image'] = isset($val->getAssociatedRefSpInfo->profile_image) ? changeImageUrlForFileExist(asset('sp_uploads/profile/'.$val->getAssociatedRefSpInfo->profile_image)):'';
	          			}
	          			if($val['ref_type'] =='E_GIFT_CARD_RECEIVED' || $val['ref_type'] =='REFERRAL_JOINED' || ($val['ref_type'] =='FORUM_RESPONSE')){
	          				
	          				$ref_name = isset($val->getAssociatedRefUserInfo->name)?$val->getAssociatedRefUserInfo->name:'';

	          				$notification_arr['user_name'] = isset($val->getAssociatedRefUserInfo->name)?$val->getAssociatedRefUserInfo->name:'';
	          				$notification_arr['user_image'] = isset($val->getAssociatedRefUserInfo->image) ? asset($val->getAssociatedRefUserInfo->image):'';
	          			}

	          			
	          			$notification_arr['ref_type'] = $val['ref_type'];
	          			$notification_arr['message'] = $val['message'.$this->col_postfix];
	          			$notification_arr['title'] = $val['notification_title'.$this->col_postfix];
	          			$notification_arr['created_at'] = date('Y-m-d H:i:s',strtotime($val['created_at']));
	          			$notification_arr_1[] = $notification_arr;		          		
	          		}

	          		$result['data'] = $notification_arr_1;
	                $result['current_page']     = $rows_arr['current_page'];
	                $result['from']             = $rows_arr['from'];
	                $result['last_page']        = $rows_arr['last_page'];
	                $result['next_page_url']    = $rows_arr['next_page_url'];
	                $result['per_page']         = $rows_arr['per_page'];
	                $result['prev_page_url']    = $rows_arr['prev_page_url'];
	                $result['to']               = $rows_arr['to'];
	                $result['total']            = $rows_arr['total'];
	          		return response()->json(['status' => true, 'message' => 'Notification Listing', 'data' => $result]);
	          	}
	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
    }

    public function checkUnreadNotification(Request $request)
    {
    	if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          	$unread_notifications = Notification::where('user_id',$user_id)
	          			->where('user_type','customer')
	          			->where('read_notification','0')
	          			->count();

	            return response()->json(['status' => true, 'message' => 'Unread Notifications', 'data' => $unread_notifications]);
          		
	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
    }

    public function readNotifications(Request $request)
    {
    	if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          	$rows = Notification::where('user_id',$user_id)
	          			->where('user_type','customer')
	          			->where('read_notification','0')
	          			->update(array('read_notification'=>'1'));

	            return response()->json(['status' => true]);
          		
	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
    }

    public function updateAppVersion(Request $request)
    {
    	if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          	$app_version = Setting::select('customer_android_app_version','customer_ios_app_version','customer_android_app_force','customer_ios_app_force')->first()->toArray();
	          	$paytab_urls = Setting::select('paytab_site_url','paytab_success_url')->first()->toArray();

	          	// $user = User::where('id',$user_id)->first();

	            return response()->json(['status' => true,'message'=>'The app is outdated.Please, update it to the latest version.','app_version'=>$app_version,'paytab_urls'=>$paytab_urls]);
          		
	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
    }

    public function updateUserBadge(Request $request)
    {
    	if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	              'token' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

	          	$device = Device::where('user_id',$user_id)->where('device_token',$request->token)->first();
	          	if($device){
	          		$device->badge_count = 0;
		          	$device->save();
		            return response()->json(['status' => true]);
	          	}else{
	          		return response()->json(['status' => true,'message'=>'Record not found']);
	          	}
	          	
          		
	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
    }

    public function updateUserLanguage(Request $request)
    {
    	if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required|in:en,ar',
	              'device_token' => 'required',
	              'device_type' => 'required|in:IPHONE,ANDROID,IOS',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

	          	$device = Device::where('user_id',$user_id)->where('device_token',$request->device_token)->first();

	          	if($device){
	          		$device->language = $request->language;
		          	$device->save();
		            
	          	}else{
	          		$device = new Device();
	          		$device->user_id = $user_id;
	          		$device->device_type = $request->device_type;
	          		$device->device_token = $request->device_token;
	          		$device->language = $request->language;
		          	$device->save();
	          	}
	          	return response()->json(['status' => true]);
          		
	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
    }

    public function addTrailSessionFaq(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	            	'trail_session_id' => 'required|exists:trail_sessions,id',
	            	'store_id' => 'required|exists:sp_users,id',
	            	'faqs' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
			
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
          		$faqsArr = json_decode($request->faqs, true);
          		foreach($faqsArr as $arr)
          		{
          			$idExist = TrailSessionQuestion::where('id',$arr['question_id'])->where('trail_session_id',$request->trail_session_id)->exists();

          			if($idExist)
          			{
          				$row = new TrailSessionQuestionsFaqs();
          				$row->sp_id = $request->store_id;
          				$row->user_id = $user_id;
          				$row->question_id = $arr['question_id'];
          				$row->answer = $arr['answer'];
          				$row->save();
          			}
          			else
          			{
          				return response()->json(['status' => false, 'message' => 'This Question Id - '.$arr['question_id'].' is not associated with the given session.' , 'data' =>[]]);
          			}
          		}

        		if($row->save())
        		{
        			return response()->json(['status' => true, 'message' => 'Session Question Faq Added Successfully' , 'data' =>TrailSessionQuestionsFaqs::where('user_id',$user_id)->get()]);
        		}
	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function getTrailSessionFaqs(Request $request)
    {
    	if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	              'trail_session_id' => 'required|exists:trail_sessions,id',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          	
	          	$rows = TrailSessionQuestion::where('trail_session_id',$request->trail_session_id)->select('id','question')->orderBy('id','ASC')->get();
	         
	          	return response()->json(['status' => true, 'message' => 'Trail Session Faqs', 'data' => $rows]);

	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
    }

    // public function testNotification()
    // {
    // 	$message_title_main = __('messages.Order Cancellation!');
    //   // $message_main = 'Due to certain unforeseen circumstances Beutics user '.$name.' had to cancel his order. We regret the inconvenience but stay tuned for more orders.';

    //   $message_main = __('messages.cancel msg',['name' => 'nidi']);

    //   app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');


    //   $message_title_other = __('messages.Order Cancellation!');
    //   $message_other = __('messages.cancel msg',['name' => 'nidi']);

    //   // Notification::saveNotification($rows->sp_id,$rows->user_id,$rows->id,'ORDER_CANCELLED',$message_main,$message_other,$message_title_main,$message_title_other,'sp');

    // 	PushNotification::Notify(543,254, 135, 'ORDER_CANCELLED',$message_main,$message_other,$message_title_main,$message_title_other,'sp', $dic = []);
    // }


}
