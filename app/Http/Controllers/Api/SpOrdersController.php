<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\Paginator;
use JWTAuth;
use JWTAuthException;

use App\Http\Controllers\Controller;
use App\Model\SpUser;
use App\Model\StaffLeave;
use App\Model\User;
use App\Model\Booking;
use App\Model\BookingItem;
use App\Model\PromoCodeLinkedCategory;
use App\Model\PromoCodeLinkedCustomer;
use App\Model\PromoCodeLinkedService;
use App\Model\PromoCodeLinkedStore;
use App\Model\PromoCodeLinkedSubcategory;
use App\Model\CashbackHistory;
use App\Model\RescheduleOrderHistory;
use App\Model\ShopTiming;
use App\Model\Staff;
use App\Model\Notification;
use App\Model\Review;
use App\Model\ReviewSpService;
use App\Model\ReviewQuestionRating;
use App\Model\Setting;
use App\Model\Forum;
use App\Model\ForumComment;
use App\Model\ForumCategory;
use App\Model\SpOffer;
use App\Model\PromoCode;
use App\Model\SpDevice;

use DB;
use Config;



class SpOrdersController extends Controller
{
	function __construct(Request $request)
	{
		parent::__construct($request);
	    Config::set('jwt.user', SpUser::class);
	    Config::set('auth.providers', ['users' => [
	            'driver' => 'eloquent',
	            'model' => SpUser::class,
	        ]]);


	    $this->col_postfix = '';

        $this->header = $request->header('language');
        if($this->header!= "en"){
        	$this->col_postfix = '_ar';
        }

        
	}

	public function getOrderListing(Request $request)
    {
    	if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
				  'page' => 'required',
				  'status' => 'required|in:new,confirmed,completed,cancelled',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$currentpage = $request->page;
	          	date_default_timezone_set('Asia/Dubai');
          		Paginator::currentPageResolver(function () use ($currentpage) {
			        return $currentpage;
			    });

          		$sr_name = 'name'.$this->col_postfix;
          		$offer_title = 'title'.$this->col_postfix;
          		$status_arr = array('0'=>'Awaiting','1'=>'Confirmed','2'=>'Cancelled','3'=>'Refunded','4'=>'Expired','5'=>'Completed');
          		$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

          		$status = '';
     
          		if($request->status == 'new'){
          			$status = '0';
          		}elseif($request->status == 'confirmed'){
          			$status = '1';
          		}elseif($request->status == 'completed'){
          			$status = '5';
          		}else{
          			$status = '2';
          		}

          		if($request->keyword)
          		{
          			$search = $request->keyword;
          			$rows = Booking::leftJoin('users', function($join) {
						      $join->on('bookings.user_id', '=', 'users.id');
						    })
          			->where(function ($query) use ($search){

                        $query->orWhere('name', 'like', '%' .$search . '%')
		          		->orWhere('mobile_no', 'like', '%' .$search . '%');
                    })
		          	
		          	->select('bookings.*')
		          	->where('bookings.status',$status);

		          	if($status == '0'){
          				$rows->where('user_id','!=','');
          			}

		            $rows = $rows->where('sp_id',$user_id)
		          	->orderBy('bookings.id','desc')
		          	->paginate(10);
          		}else{

          			$rows = Booking::where('sp_id',$user_id)
          			->where('status',$status);
          			if($status == '0'){
          				$rows->where('user_id','!=','');
          			}
          			
          			$rows = $rows->orderBy('id','desc')
          			->paginate(10);
          		}
          		
          		
          		$rows_arr = $rows->toArray();
          			
          		$booking_detail = array();
          		$final_array = array();

				foreach($rows as $val){

					$user_name = '';
					$booking_type = '';
					$is_referred = 0;
					//check customer uses sp refer code or not

					if($val['user_id']!='' && $val['user_id']!=null){
						$cust_refer_code = $val->getAssociatedUserInfo->refer_code;
						// echo $cust_refer_code;die;
						$check_sp_code = SpUser::where('share_code',$cust_refer_code)->where('id',$val['sp_id'])->exists();
						if($check_sp_code){
							$is_referred = 1;
						}
					}



					if($val['db_user_name']!=''){
						$user_name = $val['db_user_name'];
						$booking_type = 'manual';

					}elseif ($val['is_gift']=='1') {
						$user_name = $val['receiver_name'];
						$booking_type = 'gift';
					}else{
						$user_name = $val->getAssociatedUserInfo->name;
						$booking_type = 'direct';
					}
					$booking_detail = [
						"id" => $val['id'],
						"booking_unique_id" => $val['booking_unique_id'],
						"sp_id" => $val['sp_id'],
						"user_name" => $user_name,
						"user_profile_image" => (!empty($val->getAssociatedUserInfo->image) && !empty($val['user_id'])) ? asset($val->getAssociatedUserInfo->image):'',
						"user_id" => $val['user_id'],
						"staff_id" => $val['staff_id'],
						"staff_name" => ($val['staff_id']!='' && $val['staff_id']!= null) ? $val->getAssociatedStaffInformation->staff_name:'',
						"appointment_time" => $val['appointment_time'],
						"appointment_date" => $val['appointment_date']!='' && $val['appointment_date']!=null ? date('Y-m-d',strtotime($val['appointment_date'])):"",
						"tax_amount" => round($val['tax_amount'],2),
						"total_amount" => round($val['total_item_amount'],2),
						"paid_amount" => round($val['paid_amount'],2),
						"created_date" => date('d-M-Y H:i:s',strtotime($val->created_at)),
						"completed_date_time" => $val['completed_date_time']!='' && $val['completed_date_time']!=null ? date('d-M-Y H:i:s',strtotime($val['completed_date_time'])):"",
						"status" => $status_arr[$val['status']],
						"booking_type" => $booking_type,
						"booking_service_type" => $val['booking_service_type'],
						"is_sp_referred" => $is_referred,
						"payment_type" => $val['payment_type']=='0'?'online':'offline',
						// "cancel_reason" => $val['cancellation_reason']!='' ? $val['cancellation_reason']:'',
						// "cancel_refunded_amount" => round($val['cancellation_refunded_amount'],2),
					];

					$booking_item = array();
					$item_name_arr = array();
					foreach($val->getAssociatedBookingItems as $bkitems){
						if($bkitems['is_service'] == '1')
						{
							$item_name_arr[] = [
								'service_id' => $bkitems['booking_item_id'],
								'name' => $bkitems->getAssociatedServiceDetail->getAssociatedService->$sr_name,
								'quantity' => $bkitems['quantity'],
							];
						}
						else{

							$offer_services = array();
							if(isset($bkitems->getAssociatedOfferDetail->getAssociatedOfferServices)){
								foreach($bkitems->getAssociatedOfferDetail->getAssociatedOfferServices as $os){

									$offer_services[] = [
										'quantity' => $os['quantity'],
										'name' => $os->getAssociatedOfferServicesName->getAssociatedService->$sr_name,
									];
								}

								$item_name_arr[] = [
									'offer_id' => $bkitems['booking_item_id'],
									'name' => $bkitems->getAssociatedOfferDetail->getAssociatedOfferName->$offer_title,
									'quantity' => $bkitems['quantity'],
									'original_price' => round($bkitems['original_price'],2),
									'best_price' => round($bkitems['best_price'],2),
									'cashback' => round($bkitems['cashback'],2),
									'services'=>$offer_services,
								];
							}
							
						}

					}
					$booking_detail['booking_items'] = $item_name_arr;
					$final_array[] = $booking_detail;
				}
				$result['data'] = $final_array;
                $result['current_page']     = $rows_arr['current_page'];
                $result['from']             = $rows_arr['from'];
                $result['last_page']        = $rows_arr['last_page'];
                $result['next_page_url']    = $rows_arr['next_page_url'];
                $result['per_page']         = $rows_arr['per_page'];
                $result['prev_page_url']    = $rows_arr['prev_page_url'];
                $result['to']               = $rows_arr['to'];
                $result['total']            = $rows_arr['total'];
          		return response()->json(['status' => true, 'message' => 'Order Details','data' => $result]);	
          	  }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
    }
    public function checkFirstOrder($user_id)
	{
		$is_first_order = 1;
		$row = Booking::where('sp_id', $user_id)->count();
			   
		if($row>1){
			$is_first_order = 0;
		}
		return $is_first_order;
	}
    public function showBookingDetails($row)
	{
		$status_arr = array('0'=>'Awaiting','1'=>'Confirmed','2'=>'Cancelled','3'=>'Refunded','4'=>'Expired','5'=>'Completed');
		$user_name = '';
		$sr_name = 'name'.$this->col_postfix;
        $offer_title = 'title'.$this->col_postfix;

		if($row['db_user_name']!=''){
			$user_name = $row['db_user_name'];
			$booking_type = 'manual';

		}elseif ($row['is_gift']=='1') {
			$user_name = $row['receiver_name'];
			$booking_type = 'gift';
		}else{
			$user_name = $row->getAssociatedUserInfo->name;
			$booking_type = 'direct';
		}

		$is_referred = 0;
		//check customer uses sp refer code or not

		if($row['user_id']!='' && $row['user_id']!=null){
			$cust_refer_code = $row->getAssociatedUserInfo->refer_code;
		
			$check_sp_code = SpUser::where('share_code',$cust_refer_code)->where('id',$row['sp_id'])->exists();
			if($check_sp_code){
				$is_referred = 1;
			}
		}
	

		$data = array();
		$data['id'] = $row['id'];
		$data['booking_unique_id'] = $row['booking_unique_id'];
		$data['sp_id'] = $row['sp_id'];
		$data['user_name'] = $user_name;
		$data['user_profile_image'] = (!empty($row->getAssociatedUserInfo->image) && !empty($row['user_id'])) ? asset($row->getAssociatedUserInfo->image):'';
		$data['user_id'] = $row['user_id'];
		$data['staff_id'] = $row['staff_id'];
		if($row['staff_id']!='' && $row['staff_id']!=null){
			$data['staff_name'] = $row->getAssociatedStaffInformation->staff_name;
		}else{
			$data['staff_name'] = "";
		}
		$data["booking_type"] = $booking_type;
		$data['appointment_time'] = ($row['appointment_time']==null || strpos($row['appointment_time'], ':') === 2) ? $row['appointment_time'] : date('H:i',$row['appointment_time']);
		$data['service_end_time'] = ($row['service_end_time']==null||  strpos($row['service_end_time'], ':') === 2) ? $row['service_end_time'] : date('H:i',$row['service_end_time']);
		$data['appointment_date'] = $row['appointment_date']!='' && $row['appointment_date']!= null ? $row['appointment_date']:"";

		$data['completed_date_time'] = $row['completed_date_time']!='' && $row['completed_date_time']!= null ? date('d-M-Y H:i:s',strtotime($row['completed_date_time'])):"";


		

		$data['redeemed_reward_points'] = $row['redeemed_reward_points'];
		// $data['payment_type'] = $row['payment_type'];
		$data['promo_code_amount'] = round($row['promo_code_amount'],2);
		$data['tax_amount'] = round($row['tax_amount'],2);
		$data['total_amount'] = round($row['booking_total_amount'],2);
		$data['paid_amount'] = round($row['paid_amount'],2);
		$data['total_item_amount'] = round($row['total_item_amount'],2);
		$data['total_cashback'] = round($row['total_cashback'],2);
		$data['created_date'] = date('d-M-Y H:i:s',strtotime($row->created_at));
		$data['payment_type'] = $row['payment_type']=='0'?'online':'offline';
		$data['is_sp_referred'] = $is_referred;
		$data['booking_service_type'] = $row['booking_service_type'];
		if($row['status'] == '2'){
			$data['cancel_reason'] = $row['cancellation_reason'];
			$data['cancel_refunded_amount'] = round($row['cancellation_refunded_amount'],2);
		}
		$item_name_arr = array();
		foreach($row->getAssociatedBookingItems as $bkitems){

			if($bkitems['is_service'] == '1')
			{
				$item_name_arr[] = [
					'service_id' => $bkitems['booking_item_id'],
					'name' => $bkitems->getAssociatedServiceDetail->getAssociatedService->$sr_name,
					'quantity' => $bkitems['quantity'],
					'original_price' => round($bkitems['original_price'],2),
					'best_price' => round($bkitems['best_price'],2),
					'cashback' => round($bkitems['cashback'],2),
				];
			}
			else{
				$offer_services = array();

					if(isset($bkitems->getAssociatedOfferDetail->getAssociatedOfferServices)){
						foreach($bkitems->getAssociatedOfferDetail->getAssociatedOfferServices as $os){

							$offer_services[] = [
								'quantity' => $os['quantity'],
								'name' => $os->getAssociatedOfferServicesName->getAssociatedService->$sr_name,
							];
						}

						$item_name_arr[] = [
							'offer_id' => $bkitems['booking_item_id'],
							'offer_terms' => $bkitems->getAssociatedOfferDetail->offer_terms,
							'offer_details' => $bkitems->getAssociatedOfferDetail->offer_details,
							'name' => $bkitems->getAssociatedOfferDetail->getAssociatedOfferName->$offer_title,
							'quantity' => $bkitems['quantity'],
							'original_price' => round($bkitems['original_price'],2),
							'best_price' => round($bkitems['best_price'],2),
							'cashback' => round($bkitems['cashback'],2),
							'services'=>$offer_services,
						];
					}
			}

		}
		$data['booking_items'] = $item_name_arr;
		$data['status'] = $status_arr[$row['status']];
		
		return $data;
				
	}

	public function orderSummery(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
				  'order_id' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$order = Booking::where('id',$request->order_id)->first();
	          	$result = $this->showBookingDetails($order);
          		return response()->json(['status' => true, 'message' => 'Order Details','data' => $result]);	
          	  }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}


	public function confirmOrder(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	              'order_id' => 'required',
	              'appointment_date' => 'required',
	              'appointment_time' => 'required',
	              'service_end_time' => 'required',
				  'staff_id' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          	
 			    date_default_timezone_set('Asia/Dubai');  
				//query to check time slots is valid or not

				$app_time = date("H:i:s", strtotime($request->appointment_time));
				
				$sr_end_time = date("H:i:s", strtotime($request->service_end_time));

				$rows = Booking::where('staff_id',$request->staff_id)

			          	->where(function ($query){

	                        $query->where('status','1')
	                        ->orWhere(function ($query){
	                        	$query->where('status','0')
	                        	->Where(function ($query){
	                        		$query->whereNull('staff_id')
	                        		->orWhere('staff_id','!=','');
	                        	});	
	                        	
	                        });
			          		
	                    })
			          	// ->orWhere('status','5')
			          	->where('appointment_date',date('Y-m-d',strtotime($request->appointment_date)))

			          	->where(function ($query) use ($app_time, $sr_end_time) {
			          		$query->where(function ($query) use ($app_time, $sr_end_time) {
						        $query
						            ->where('appointment_time', '<=', $app_time)
						            ->where('service_end_time', '>', $app_time);
						    })
						    ->orWhere(function ($query) use ($app_time, $sr_end_time) {
						        $query
						            ->where('appointment_time', '<', $sr_end_time)
						            ->where('service_end_time', '>=', $sr_end_time);
						    })
						    ->orWhere(function ($query) use ($app_time, $sr_end_time) {
						        $query
						        	// ->where('status','0')
						        	// ->where('service_end_time','')
						        	
						        	->Where(function ($query){
		                        		$query->whereNull('service_end_time')
		                        		->orWhere('service_end_time','');
		                        	})

						            ->where('appointment_time', '>=', $app_time)
						            ->where('appointment_time', '<=', $sr_end_time);
						    });
			          	})

			          	->where('id','!=',$request->order_id)
			          	
			          	// ->select('appointment_time','service_end_time')
			          	->count();
			          	
			    if($rows>0){
			    	return response()->json(['status' => false, 'message' => __('messages.This time slot is already booked.')]);
		          }else{
		          	
		          	$rows = Booking::where('id',$request->order_id)->first();
		          	$rows->staff_id = $request->staff_id;
		          	$rows->appointment_date = date('Y-m-d',strtotime($request->appointment_date));
		          	$rows->appointment_time = $request->appointment_time;
		          	$rows->service_end_time = $request->service_end_time;
		          	$rows->status = '1';
		          	$rows->confirmed_date_time = date('Y-m-d H:i:s');
		          	$is_first_order = $this->checkFirstOrder($user_id);
		          	if($rows->save()){
		          		$date = $rows['appointment_date'];
		          		$time = $rows['appointment_time'];
		          		$combinedDT = date('Y-m-d h:i:A', strtotime("$date $time"));
		          		$message_title_main = __('messages.Appointment Confirmed!');
		          		if($rows->booking_service_type == 'home'){
		          			// $message = 'The service will be initiated at your place on '.$combinedDT.'. Have a good time! Find this order details in “My Orders”. ';

		          			$message_main = __('messages.Appointment Confirmed Home!',['date' => $combinedDT]);
		          		}else{
		          			// $message = 'Booking Confirmed for '.$combinedDT.'! '.$rows->getAssociatedUserInfo->name.', your order has been confirmed by '.$rows->getAssociatedSpInformation->store_name.'. Please try to arrive at the venue 10 minutes prior.';

		          			$message_main =  __('messages.Appointment Confirmed Store!',['date' => $combinedDT,'user_name' => $rows->getAssociatedUserInfo->name,'store_name' => $rows->getAssociatedSpInformation->store_name]);

		          		}
		          		app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');


						$message_title_other = __('messages.Appointment Confirmed!');
						if($rows->booking_service_type == 'home'){
		          			$message_other = __('messages.Appointment Confirmed Home!',['date' => $combinedDT]);
		          		}else{

		          			$message_other =  __('messages.Appointment Confirmed Store!',['date' => $combinedDT,'user_name' => $rows->getAssociatedUserInfo->name,'store_name' => $rows->getAssociatedSpInformation->store_name]);

		          		}
		          		// echo $message_other;die;
		          		
		          		//get user detail
		          		$user = User::where('id',$rows->user_id)->first();
		          		if($rows->user_id!='' && $rows->user_id!=null && $user->notification_alert_status == '1'){

			          		Notification::saveNotification($rows->user_id,$rows->sp_id,$rows->id,'CONFIRM_ORDER',$message_main,$message_other,$message_title_main,$message_title_other,'customer');
			          	}

			          	app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

		          		return response()->json(['status' => true, 'message' => __('messages.Order Confirmed Successfully.'),'is_first_order'=>$is_first_order]);
		          	
		          }
	          	
          	  }
	       }
	   }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage().$e->getLine(), 'data' => []]);
	       }
		}

	}

	public function completeOrder(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	              'order_id' => 'required',
	              'unique_order_id' => 'required',
	              'appointment_date' => 'required',
	              'appointment_time' => 'required',
				  'staff_id' => 'required',
				  // 'service_end_time' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	date_default_timezone_set('Asia/Dubai');
	          	
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          	$user_cat_id = SpUser::where('id',$user_id)->select('category_id')->first();


	          	$rows = Booking::where('id',$request->order_id)->where('booking_unique_id',$request->unique_order_id)->first();

	          	if($rows){

	          		$date = $rows['appointment_date'];
	          		$time = $rows['appointment_time'];
	          		$combinedDT = date('Y-m-d h:i:A', strtotime("$date $time"));
	          		$current_timestamp = date('Y-m-d H:i:s');

	          		if(strtotime($current_timestamp)>strtotime($combinedDT)){
	          			
	          			$service_end_time = $rows['service_end_time'];
	          		}else{
						$service_end_time = date('H:i');
						
	          		}

	          		$rows->staff_id = $request->staff_id;
		          	$rows->appointment_date = date('Y-m-d',strtotime($request->appointment_date));
		          	$rows->appointment_time = $request->appointment_time;
		          	$rows->service_end_time = $service_end_time;
		          	$rows->completed_date_time = date('Y-m-d H:i:s');
		          	if ($request->hasFile('invoice') && $request->file('invoice'))
	                {
	                    $file = $request->file('invoice');
	                    $name = time().'.'.$file->getClientOriginalExtension();
	                    $destinationPath = public_path('/sp_uploads/invoice/');
	                    $img = $file->move($destinationPath, $name);
	                    $rows->invoice = $name;
	                } 
	                $rows->status = '5';

	                $is_referred = 0;
						//check customer uses sp refer code or not

					if($rows['user_id']!='' && $rows['user_id']!=null){
						$cust_refer_code = $rows->getAssociatedUserInfo->refer_code;
					
						$check_sp_code = SpUser::where('share_code',$cust_refer_code)->where('id',$rows['sp_id'])->exists();
						if($check_sp_code){
							$is_referred = 1;
						}
					}

	                $rows->payment_settlement_status = 'unpaid';
	                if($user_cat_id['category_id']== 4 && (string)$rows->payment_type == '1' && $rows->db_user_name == '' && $is_referred!=1){
						$rows->payment_settlement_status = null;
	                }
	                // print_r($rows);die;
		          	if($rows->save()){

		          		if($rows->db_user_name == '' && $rows->db_user_name == null){
		          			
		          			if($rows->is_gift == '' && $rows->is_gift == null){
		          			$rewarded_user_id = $rows->user_id;
			          		}else{
			          			$rewarded_user_id = $rows->gifted_by;
			          		}
			          		
		          			$tier = SpUser::leftJoin('tiers', function($join) {
						      $join->on('sp_users.tier_id', '=', 'tiers.id');
							    })
			                ->where('sp_users.id',$user_id)
			                ->select('tiers.earning')
			                ->first();


			            	// $rewards = ($rows->booking_total_amount*$tier->earning)/100;

			            	if($user_cat_id['category_id'] == 4 && (string)$rows->payment_type == '1'  && $is_referred != 1){
			            		
                    		}else{
                    			
				            	$rewards = $rows->total_cashback;
				           		

				           		$user = User::where('id',$rewarded_user_id)
				                ->first();
				                if($user->earning!=0 && $user->earning!=null){
				                	$total_earning = $rewards+$user->earning;
				                }else{
				                	$total_earning = $rewards;
				                }

				                //check cashback already exists or not
				                $check_cashback = CashbackHistory::where('type','0')->where('booking_id',$rows->id)->first();

				                if(!$check_cashback){
				                	
				                	$user->earning = round($total_earning,2);
					                $user->save();

					                $cashback = new CashbackHistory();
					                $cashback->user_id = $rewarded_user_id;
					                $cashback->booking_id = $request->order_id;
					                $cashback->type = '0';
					                $cashback->amount =round($rewards,2);
					                $cashback->save();
				                }
				                

				      			if($user->notification_alert_status == '1'){
				      				$date = $rows['appointment_date'];
					          		$time = $rows['appointment_time'];
					          		$combinedDT = date('Y-m-d h:i:A', strtotime("$date $time"));

					          		// $message = 'Hey! Just to let you know, I have credited your cashback earning of AED '.round($rewards,2).' onto your wallet for your recent purchase {Order ID: '.$rows->booking_unique_id.'} .';

					          		$message_main = __('messages.Cashback Credited',['cashback' => round($rewards,2),'order_id' => $rows->booking_unique_id]);

					          		$message_title_main = __('messages.Cashback Credited!');

					          		app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

									$message_other = __('messages.Cashback Credited',['cashback' => round($rewards,2),'order_id' => $rows->booking_unique_id]);

					          		$message_title_other = __('messages.Cashback Credited!');

					          		
						          	Notification::saveNotification($rows->user_id,$rows->sp_id,$rows->id,'CASHBACK_CREDITED',$message_main,$message_other,$message_title_main,$message_title_other,'customer');
						          	app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

						          	
					      			}
					      		}//end category if

				      			if($rows->is_gift == '1' && $user->notification_alert_status == '1'){

				      				// $message = 'Your friend '.$rows->receiver_name.' has used your gift to make a booking. He cherishes your friendship! ';

				      				$message_main = __('messages.Gift Redeemed',['receiver_name' => $rows->receiver_name]);
								    $message_title_main = __('messages.Gift Redeemed!');
								   app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

					          		$message_other = __('messages.Gift Redeemed',['receiver_name' => $rows->receiver_name]);
								    $message_title_other = __('messages.Gift Redeemed!');
						          	Notification::saveNotification($rows->user_id,$rows->sp_id,$rows->id,'GIFT_REDEEMED',$message_main,$message_other,$message_title_main,$message_title_other,'customer');
						          	app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');


				      			}

				      	

			      				if($rows->user_id!=null && $rows->user_id!='' && $rows->getAssociatedUserInfo->notification_alert_status == '1'){

				      				// $review_message = 'Hey! We hope you enjoyed the service that you ordered through Beutics on '.date('Y-m-d',strtotime($rows['created_at'])).'. How was your experience? Please leave a review for '.$rows->getAssociatedSpInformation->store_name.' to help your fellow Beutics users.';

				      				$message_main = __('messages.How was the service?',['date' => date('Y-m-d',strtotime($rows['created_at'])),'store_name' => $rows->getAssociatedSpInformation->store_name]);

							    	$message_title_main =  __('messages.How was the service!');
							    	app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');
									$message_other = __('messages.How was the service?',['date' => date('Y-m-d',strtotime($rows['created_at'])),'store_name' => $rows->getAssociatedSpInformation->store_name]);

							    	$message_title_other = __('messages.How was the service!');

				          		
					          		Notification::saveNotification($rows->user_id,$rows->sp_id,$rows->id,'REVIEW_REQUEST',$message_main,$message_other,$message_title_main,$message_title_other,'customer');
					          		app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');


				      			}
				      		// if($rows->getAssociatedUserInfo->verifyToken == "")			      {
				      			$data['templete'] = "review_request";
			                    $data['name'] = $rows->getAssociatedUserInfo->name;
			                    $data['email'] = $rows->getAssociatedUserInfo->email;
			                    $data['store_name'] = $rows->getAssociatedSpInformation->store_name;
			                    $data['date'] = $combinedDT;
			                    $data['subject'] = "Your feedback has the power!";
			                    send($data);
				      		// }
				         
		          		}
		          		
		          		
		          		return response()->json(['status' => true, 'message' => __('messages.Order completed Successfully.')]);
		          	}
	          	}else{
	          		return response()->json(['status' => false, 'message' => __('messages.OrderId is not valid.')]);
	          	}
          	  }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function rescheduleOrder(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	              'order_id' => 'required',
	              'appointment_date' => 'required',
	              'appointment_time' => 'required',
	              'service_end_time' => 'required',
				  'staff_id' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	date_default_timezone_set('Asia/Dubai');
	          	
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          	//query to check time slots is valid or not

				$app_time = date("H:i:s", strtotime($request->appointment_time));
				
				$sr_end_time = date("H:i:s", strtotime($request->service_end_time));

				$rows = Booking::where('staff_id',$request->staff_id)

			          	->where(function ($query){

	                        $query->where('status','1')
	                        ->orWhere(function ($query){
	                        	$query->where('status','0')
	                        	->Where(function ($query){
	                        		$query->whereNull('staff_id')
	                        		->orWhere('staff_id','!=','');
	                        	});	
	                        	
	                        });
			          		
	                    })
			          	// ->orWhere('status','5')
			          	->where('appointment_date',date('Y-m-d',strtotime($request->appointment_date)))

			          	->where(function ($query) use ($app_time, $sr_end_time) {
			          		$query->where(function ($query) use ($app_time, $sr_end_time) {
						        $query
						            ->where('appointment_time', '<=', $app_time)
						            ->where('service_end_time', '>', $app_time);
						    })
						    ->orWhere(function ($query) use ($app_time, $sr_end_time) {
						        $query
						            ->where('appointment_time', '<', $sr_end_time)
						            ->where('service_end_time', '>=', $sr_end_time);
						    })
						    ->orWhere(function ($query) use ($app_time, $sr_end_time) {
						        $query
						        	// ->where('status','0')
						        	->Where(function ($query){
		                        		$query->whereNull('service_end_time')
		                        		->orWhere('service_end_time','');
		                        	})

						            ->where('appointment_time', '>=', $app_time)
						            ->where('appointment_time', '<=', $sr_end_time);
						    });
			          	})

			          	->where('id','!=',$request->order_id)
			          	
			          	// ->select('appointment_time','service_end_time')
			          	->count();
	          	
	          	if($rows>0){
	          		return response()->json(['status' => false, 'message' => 'This time slot is already booked.']);
	          	}else{
	          		$rows = Booking::where('id',$request->order_id)->where('status','1')->first();

		          	if($rows){

		          		$rows->staff_id = $request->staff_id;
			          	$rows->appointment_date = date('Y-m-d',strtotime($request->appointment_date));
			          	$rows->appointment_time = $request->appointment_time;
			          	$rows->service_end_time = $request->service_end_time;
			          	$rows->is_rescheduled = '1';

			          	if($rows->save()){

			          		$re_row = new RescheduleOrderHistory();
			          		$re_row->booking_id = $rows->id;
			          		$re_row->appointment_date = $rows->appointment_date;
			          		$re_row->appointment_time = $rows->appointment_time;
			          		$re_row->service_end_time = $rows->service_end_time;
			          		$re_row->save();
			          		
			          		if($rows->user_id!=null && $rows->getAssociatedUserInfo->notification_alert_status == '1'){

			          			// $message = 'Hey, Your appointment with '.$rows->getAssociatedSpInformation->store_name.' has been rescheduled to: '.$request->appointment_date.', '.$request->appointment_time.'. Please contact '.$rows->getAssociatedSpInformation->store_name.' for any discrepancy.';

			          			$message_main = __('messages.Appointment Rescheduled!',['store_name' => $rows->getAssociatedSpInformation->store_name,'date' => $request->appointment_date,'time' => $request->appointment_time]);

				          		$message_title_main = __('messages.Appointment Rescheduled!!');

				          		app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

				          		$message_other = __('messages.Appointment Rescheduled!',['store_name' => $rows->getAssociatedSpInformation->store_name,'date' => $request->appointment_date,'time' => $request->appointment_time]);

				          		$message_title_other = __('messages.Appointment Rescheduled!!');

					          	Notification::saveNotification($rows->user_id,$rows->sp_id,$rows->id,'RESCHEDULE_ORDER',$message_main,$message_other,$message_title_main,$message_title_other,'customer');
					          	app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');
			          		}
			          		
			          		return response()->json(['status' => true, 'message' => __('messages.Order has been rescheduled Successfully.')]);
			          	}
		          	}
	          	}
          	  }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage().$e->getLine(), 'data' => []]);
	       }
		}
	}

	public function addAppointment(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'language' => 'required',
		              'service_provider_id' => 'required',
		              'staff_id'=> 'required',
		              'customer_name'=> 'required',
		              // 'customer_mobile_no'=> 'required',
		              'appointment_date'=> 'required',
		              'appointment_time'=> 'required',
		              'appointment_end_time'=> 'required',
		              'selected_items'=> 'required',
		              'total_item_amount'=> 'required',
		              'tax_amount'=> 'required',
		              'total_amount'=> 'required',		              
		              'paid_amount'=> 'required',
		              
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {

		          	// $user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
		          	 date_default_timezone_set('Asia/Dubai');  
				//query to check time slots is valid or not

					$app_time = date("H:i:s", strtotime($request->appointment_time));
					
					$sr_end_time = date("H:i:s", strtotime($request->appointment_end_time));

					$rows = Booking::where('staff_id',$request->staff_id)

				          	->where(function ($query){

		                        $query->where('status','1')
		                        ->orWhere(function ($query){
		                        	$query->where('status','0')
		                        	->Where(function ($query){
		                        		$query->whereNull('staff_id')
		                        		->orWhere('staff_id','!=','');
		                        	});	
		                        	
		                        });
				          		
		                    })
				          	// ->orWhere('status','5')
				          	->where('appointment_date',date('Y-m-d',strtotime($request->appointment_date)))

				          	->where(function ($query) use ($app_time, $sr_end_time) {
				          		$query->where(function ($query) use ($app_time, $sr_end_time) {
							        $query
							            ->where('appointment_time', '<=', $app_time)
							            ->where('service_end_time', '>', $app_time);
							    })
							    ->orWhere(function ($query) use ($app_time, $sr_end_time) {
							        $query
							            ->where('appointment_time', '<', $sr_end_time)
							            ->where('service_end_time', '>=', $sr_end_time);
							    })
							    ->orWhere(function ($query) use ($app_time, $sr_end_time) {
							        $query
							        	// ->where('status','0')
							        	// ->where('service_end_time','')
							        	
							        	->Where(function ($query){
			                        		$query->whereNull('service_end_time')
			                        		->orWhere('service_end_time','');
			                        	})

							            ->where('appointment_time', '>=', $app_time)
							            ->where('appointment_time', '<=', $sr_end_time);
							    });
				          	})

				          	// ->where('id','!=',$request->order_id)
				          	
				          	// ->select('appointment_time','service_end_time')
				          	->count();

				     if($rows>0){
				     	return response()->json(['status' => false, 'message' => __('messages.This time slot is already booked.')]);

				     }else{
				     	$unique_id = $this->generateRandomNumbers();
		          		$settlement_option = SpUser::where('id',$request->service_provider_id)->select('settlement_option')->first();
			          	$row = new Booking();
			          	$row->sp_id = $request->service_provider_id;

			          	if($request->customer_mobile_no){
			          		$user_check = User::where('mobile_no',$request->customer_mobile_no)->where('country_code',$request->country_code)->first();

			          		if($user_check){
				          		$row->user_id = $user_check->id;
				          		$row->db_user_name = $request->customer_name;
				          		$row->db_user_mobile_no = $request->country_code.'-'.$request->customer_mobile_no;
				          	}else{
				          		$row->db_user_name = $request->customer_name;
				          		$row->db_user_mobile_no = $request->country_code.'-'.$request->customer_mobile_no;
				          		
				          	}
			          	}else{
			          		$row->db_user_name = $request->customer_name;
			          	}
			          	

			          	$row->staff_id = $request->staff_id;
			          	$row->appointment_date = $request->appointment_date;
			          	$row->appointment_time = $request->appointment_time;
			          	$row->service_end_time = $request->appointment_end_time;
			          	$row->total_item_amount = $request->total_item_amount;
			          	$row->booking_service_type = $request->service_type;
			          	// $row->tax_amount = $request->tax_amount;
			          	$row->booking_total_amount = $request->total_amount;
			          	$row->paid_amount = $request->paid_amount;
			          	$row->booking_unique_id = $unique_id;
			          	$row->payment_type = '1';
			          	$row->settlement_option = 'nonbillable';
			          	$row->status = '1';

			          	if($row->save()){
	 
			          		$item_arr = json_decode($request->selected_items,true);

			          		foreach($item_arr as $items){
			          			$item_row = new BookingItem();
			          			$check = '';
			          			if(isset($items['sp_service_id'])){
			          				$item_row->is_service = '1';
			          				$check = 'sp_service_id';
			          			}else{
			          				$item_row->is_service = '0';	
			          				$check = 'offer_id'; 
			          			}
			          			$item_row->booking_id = $row->id;
			          			$item_row->booking_item_id = $items[$check];
			          			$item_row->quantity = $items['quantity'];
			          			// $item_row->cashback = $items['cashback'];
			          			$item_row->original_price = $items['original_price'];
			          			$item_row->best_price = $items['best_price'];
			          			$item_row->gender = $items['gender']; 
			          			$item_row->service_type = $items['bookedfor'];  
			          			$item_row->save();

			          		}
			          		
			          		//send notification
			          		
			          		
			          		
			          		if($row->user_id!='' && $row->user_id!=null){
			          			
			          			$date = $row['appointment_date'];
				          		$time = $row['appointment_time'];
				          		$combinedDT = date('Y-m-d h:i:A', strtotime("$date $time"));

				          		if($row->booking_service_type == 'home'){
				          			// $message = 'The service will be initiated at your place on '.$combinedDT.'. Have a good time! Find this order details in “My Orders”. ';
				          			$message_main = __('messages.Appointment Confirmed Home!',['date' => $combinedDT]);
				          		}else{
				          			// $message = 'Booking Confirmed for '.$combinedDT.'! '.$row->getAssociatedUserInfo->name.', your order has been confirmed by '.$row->getAssociatedSpInformation->store_name.'. Please try to arrive at the venue 10 minutes prior.';

				          			$message_main = __('messages.Appointment Confirmed Store!',['date' => $combinedDT,'user_name' => $row->getAssociatedUserInfo->nameT,'store_name' => $row->getAssociatedSpInformation->store_name]);
				          		}
				          		$message_title_main =  __('messages.Appointment Confirmed!');
				          		app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

								if($row->booking_service_type == 'home'){
				          			$message_other = __('messages.Appointment Confirmed Home!',['date' => $combinedDT]);
				          		}else{
				          			$message_other = __('messages.Appointment Confirmed Store!',['date' => $combinedDT,'user_name' => $row->getAssociatedUserInfo->nameT,'store_name' => $row->getAssociatedSpInformation->store_name]);
				          		}
				          		$message_title_other = __('messages.Appointment Confirmed!');

				          		if($row->getAssociatedUserInfo->notification_alert_status == '1'){

				          			Notification::saveNotification($row->user_id,$row->sp_id,$row->id,'CONFIRM_ORDER',$message_main,$message_other,$message_title_main,$message_title_other,'customer');
				          			app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');
				          		}
				          		
				          	}

			          		$new_row = Booking::where('id',$row->id)->first();
			          		$final_data = $this->showManualBookingDetails($new_row);
			          		
			          		return response()->json(['status' => true, 'message' => __('messages.Order Placed Successfully'),'data'=>$final_data]);	
			          	}
				     }
		          	
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage().'-'.$e->getLine(), 'data' => []]);
	       }
		}
	}


	public function showManualBookingDetails($row)
	{

		$data = array();
		$sr_name = 'name'.$this->col_postfix;
        $offer_title = 'title'.$this->col_postfix;

		$data['id'] = $row['id'];
		$data['booking_unique_id'] = $row['booking_unique_id'];
		$data['sp_id'] = $row['sp_id'];
		if($row->user_id!=''){
			$data['user_name'] = $row['user_id']!='' && $row['user_id']!=null ? $row->getAssociatedUserInfo->name:'';
			$data['user_profile_image'] = (!empty($row->getAssociatedUserInfo->image) && !empty($row['user_id'])) ? asset($row->getAssociatedUserInfo->image):'';
			$data['user_id'] = $row['user_id'];
		}else{
			$data['user_name'] = $row->db_user_name;
			$data['user_mobile_no'] = $row->db_user_mobile_no;
		}
		
		$data['staff_id'] = $row['staff_id'];
		if($row['staff_id']!='' && $row['staff_id']!=null){
			$data['staff_name'] = $row->getAssociatedStaffInformation->staff_name;
		}else{
			$data['staff_name'] = "";
		}
		
		$data['appointment_time'] = $row['appointment_time'];
		$data['service_end_time'] = $row['service_end_time'];
		$data['appointment_date'] = $row['appointment_date']!='' && $row['appointment_date']!= null ? $row['appointment_date']:"";
		// $data['redeemed_reward_points'] = $row['redeemed_reward_points'];
		$data['payment_type'] = $row['payment_type'];
		$data['promo_code_amount'] = round($row['promo_code_amount'],2);
		$data['tax_amount'] = round($row['tax_amount'],2);
		$data['total_amount'] = round($row['booking_total_amount'],2);
		$data['paid_amount'] = round($row['paid_amount'],2);
		$data['total_item_amount'] = round($row['total_item_amount'],2);
		// $data['total_cashback'] = round($row['total_cashback'],0);
		$data['created_date'] = date('d-M-Y H:i:s',strtotime($row->created_at));

		$item_name_arr = array();
		foreach($row->getAssociatedBookingItems as $bkitems){

			if($bkitems['is_service'] == '1')
			{
				$item_name_arr[] = [
					'service_id' => $bkitems['booking_item_id'],
					'name' => $bkitems->getAssociatedServiceDetail->getAssociatedService->$sr_name,
					'quantity' => $bkitems['quantity'],
					'original_price' => round($bkitems['original_price'],2),
					'best_price' => round($bkitems['best_price'],2),
					'cashback' => round($bkitems['cashback'],2),
				];
			}
			else{
				$offer_services = array();

				if(isset($bkitems->getAssociatedOfferDetail->getAssociatedOfferServices)){
					foreach($bkitems->getAssociatedOfferDetail->getAssociatedOfferServices as $os){

						$offer_services[] = [
							'quantity' => $os['quantity'],
							'name' => $os->getAssociatedOfferServicesName->getAssociatedService->$sr_name,
						];
					}

					$item_name_arr[] = [
						'offer_id' => $bkitems['booking_item_id'],
						'name' => $bkitems->getAssociatedOfferDetail->getAssociatedOfferName->$offer_title,
						'quantity' => $bkitems['quantity'],
						'original_price' => round($bkitems['original_price'],2),
						'best_price' => round($bkitems['best_price'],2),
						'cashback' => round($bkitems['cashback'],2),
						'services'=>$offer_services,
					];
				}
			}

		}
		$data['booking_items'] = $item_name_arr;
		
		return $data;
				
	}

	public function generateRandomNumbers()
	{
		// return mt_rand(10,10000000000);
		 $unique_id = substr(str_shuffle("0123456789"), 0, 10);
		 $check_no = Booking::where('booking_unique_id',$unique_id)->exists();
      	 if($check_no){
      		$unique_id = $this->generateRandomNumbers();
      	 }
      	 return $unique_id;
	}

	public function staffCalendar(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'language' => 'required',
		              'selected_date' => 'required',
		              
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {

						$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

						$db_day_arr = array('0'=>'7','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6');

						$date = date('Y-m-d',strtotime($request->selected_date));

						$date = strtotime($request->selected_date);
						$day = $db_day_arr[date('w', $date)];

						$shop_time = ShopTiming::where('user_id',$user_id)
						->where('isOpen','1')
						->where('day',$day)
						->first();
						
						if($shop_time){
								//shop time slots
							$time_frequency = 15;
							$start_time = date('H:i',strtotime($shop_time['opening_time']));
							$end_time = date('H:i',strtotime($shop_time['closing_time']));	

							$i=0;
							for($i = strtotime($start_time); $i<= strtotime($end_time); $i = $i + $time_frequency * 60) {
								$shoptime_slots[] = date("H:i", $i);  
							}
							array_pop($shoptime_slots);
							//------------

							if($request->selected_staff){
								$staff = Staff::where('sp_id',$user_id)->whereIn('id',explode(',',$request->selected_staff))->where('is_deleted','!=','1')->get();
							}else{
								$staff = Staff::where('sp_id',$user_id)->where('is_deleted','!=','1')->get();
							}

							$calender = array();
							$final_array = array();

							foreach($staff as $row)
							{
								$calender = [
									"id" => $row->id,
									"staff_name" => $row->staff_name,
									"nationality" => $row->nationality,
									"experience" => $row->experience,
									"speciality" => $row->speciality,
									"certificates" => $row->certificates,
									"is_provide_home_service" => $row->is_provide_home_service,
									"staff_image" => $row->staff_image!='' ? changeImageUrlForFileExist(asset('sp_uploads/staff/'.$row->staff_image)):'',
								];
								$sel_date = date('Y-m-d',strtotime($request->selected_date));
								$leave = StaffLeave::where('from_date','<=',$sel_date)
								->where('to_date','>=',$sel_date)
								->where('staff_id',$row->id)
								->first();

								$leave_slots = array();
								if($leave){
									//$lst leave start time
									//$let leave end time
									if($sel_date == $leave['from_date'] && $sel_date == $leave['to_date'])
									{
										
										$lst = $leave['from_time'];
										$let = $leave['to_time'];
										
									}
									elseif($sel_date == $leave['from_date'])
									{
										
										$lst = $leave['from_time'];
										$let = $end_time;
										
									}
									elseif($sel_date == $leave['to_date']){
										
										$lst = $start_time;
										$let = $leave['to_time'];
										
									}else{
										$lst = $start_time;
										$let = $end_time;
									}
									
									$slot_diff = strtotime($lst)%900;

						          	if($slot_diff > 0)
						          	{
						          		$lst = strtotime($lst)-$slot_diff;

						          	}else{
						          		$lst = strtotime($lst);
						          	}

					          		$slot_diff = strtotime($let)%900;
					         
						          	if($slot_diff > 0)
						          	{
						          		$let = strtotime($let)+(900-$slot_diff);
						          	}else{
						          		$let = strtotime($let);
						          	}
						          	$i=0;
						          	for($i = $lst; $i<= $let; $i = $i + $time_frequency * 60) {
										
										if($i == $let) {
											$leave_slots[] = date("H:i", $i-60);
										}else{
											$leave_slots[] = date("H:i", $i);
										}
									}
								}
								
								// DB::enableQueryLog();
								$booking = Booking::where('staff_id',$row->id)
								->where(function ($query) {
					          		$query->where(function ($query){
								        $query->where('status','1');

								    })
								    ->orWhere(function ($query) {
								        $query
								        	->where('staff_id','!=','')
								            ->where('status','0');
								    })
								    ->orWhere(function ($query) {
								        $query
								            ->where('status','5');
								    });
					          	})
								
					          	
					          	->where('appointment_date',$request->selected_date)
					          	// ->select('appointment_time','service_end_time','id','status','booking_service_type')
					          	->get();
					 //          	$query = DB::getQueryLog();
						// print_r($query);die;
					          	
					          	$booked_slots = array();
					          	$slots = array();
					          	$time_frequency = 15;
					          	$status_arr = array('0'=>'new','1'=>'confirmed','5'=>'completed');
					          	//determining booked slots
					          	foreach($booking as $val){
					          		
					          		$slot_diff = strtotime($val['appointment_time'])%900;
					          		if(empty($val['service_end_time'])){
			                            $val['service_end_time'] = date('H:i', strtotime($val['appointment_time'].'+ 5 minutes'));
			                        }
			                        
						          	if($slot_diff > 0)
						          	{
						          		$val['appointment_time'] = strtotime($val['appointment_time'])-$slot_diff;

						          	}else{
						          		$val['appointment_time'] = strtotime($val['appointment_time']);
						          	}

					          		$slot_diff = strtotime($val['service_end_time'])%900;
					         
						          	if($slot_diff > 0)
						          	{
						          		$val['service_end_time'] = strtotime($val['service_end_time'])+(900-$slot_diff);
						          	}else{
						          		$val['service_end_time'] = strtotime($val['service_end_time']);
						          	}

						          	for($i = $val['appointment_time']; $i<= $val['service_end_time']; $i = $i + $time_frequency * 60) {
										
										if($i == $val['service_end_time']) {
											$booked_slots[] = date("H:i", $i-60);
											
											$booking_id[$val['id']]['status'] = $val['status'];
											$booking_id[$val['id']]['service_type']=$val['booking_service_type'];
											$booking_id[$val['id']]['times'][] =  date("H:i", $i-60);
										}else{
											$booked_slots[] = date("H:i", $i);
											$booking_id[$val['id']]['status'] = $val['status'];
											$booking_id[$val['id']]['service_type']=$val['booking_service_type'];

											$booking_id[$val['id']]['order_detail']=$this->showBookingDetails($val);

											$booking_id[$val['id']]['times'][] =  date("H:i", $i);
										}
									}
					          	}

					          	// print_r($shoptime_slots);
					          	// print_r($booking_id);
					          	// print_r($booked_slots);die;
					          	$slot_array = array();
					          	$status = '';
					          	$order_detail = '';
					          	foreach($shoptime_slots as $slots){
					          		if(in_array($slots,$booked_slots)){
					          			foreach($booking_id as $aa){
					          				foreach($aa['times'] as $bb){
					          					if($slots == $bb){
						          					$status = $status_arr[$aa['status']].'-'.$aa['service_type'].'-'.'booked';
						          					$order_detail = $aa['order_detail'];
						          				}
					          				}
					          				
					          			}
					          			
					          		}elseif(in_array($slots,$leave_slots)){
										$status = 'leave';
					          		}else{
					          			$status = 'free';
					          		}

					          		$slot_array[] = [
					          			'time' => $slots,
					          			'status' => $status,
					          			'order_detail' => $order_detail,
					          		];

					          		
					          	}
					          	$calender['booking']= $slot_array;
					          	$final_array[] = $calender;
								
							}//end staff foreach
							return response()->json(['status' => true, 'message' => 'Calender','data' => $final_array]);
						}//end shoptime if
						else{
							return response()->json(['status' => false, 'message' => __('messages.Shop is closed now')]);
						}		

		          }//end else
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage().'-'.$e->getLine(), 'data' => []]);
	       }
		}
	}

	public function getDashboardStats(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          	$offer_title = 'title'.$this->col_postfix;
	          	$sr_name = 'name'.$this->col_postfix;

	          	$base_path = changeImageUrlForFileExist(asset('sp_uploads/profile/')).'/';
	          	$result_arr = array();
	          	$current_date = date('Y-m-d');

	          	$user = SpUser::where('id',$user_id)->select('store_name','share_code','address','category_id',DB::raw('CONCAT("'.$base_path.'", sp_users.profile_image) AS profile_image'))->first();

	          	//check shop opening time
	          	$db_day_arr = array('0'=>'7','1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6');

		  		// $day = $db_day_arr[date('w', date('Y-m-d'))];
		  		$day = date('w', strtotime(date('Y-m-d')));
		  		
				$shop_time = ShopTiming::where('user_id',$user_id)
				->where('isOpen','1')
				->where('day',$day)
				->select('opening_time','closing_time')
				->first();

				$result_arr['store_name'] = $user['store_name'];
	          	$result_arr['profile_image'] = changeImageUrlForFileExist($user['profile_image']);
	          	$result_arr['address'] = $user['address'];
	          	$result_arr['today_shop_opening_time'] = $shop_time['opening_time'];
				
				//orders section
				$todays_new_shop_orders = Booking::Where('status','!=','2')
										->where('booking_service_type','shop')
										->where('sp_id',$user_id)
										->whereDate('created_at', $current_date)
										->count();

				$todays_new_home_orders = Booking::Where('status','!=','2')
										  ->where('booking_service_type','home')
										  ->where('sp_id',$user_id)
										  ->whereDate('created_at', $current_date)
										  ->count();

				$todays_online_sales = Booking::Where('status','!=','2')
								->select(DB::raw('sum(total_item_amount) as amount'))
								->whereDate('created_at', $current_date)
								->where('sp_id',$user_id)
								->where('payment_type','0')
								->first();

				$todays_offline_sales = Booking::Where('status','!=','2')
								->select(DB::raw('sum(total_item_amount) as amount'))
								->whereDate('created_at', $current_date)
								->where('sp_id',$user_id)
								->where('payment_type','1')
								->first();


			    $total_confirmed_orders = Booking::Where('status','!=','2')
								->whereDate('appointment_date', $current_date)
								->where('sp_id',$user_id)
								->count();

			    $todays_confirmed_orders = Booking::Where('status','1')
								->whereDate('appointment_date', $current_date)
								->where('sp_id',$user_id)
								->count();


				$todays_closed_orders = Booking::Where('status','5')
								->whereDate('created_at', $current_date)
								->where('sp_id',$user_id)
								->count();

				$result_arr['orders'] = [
					'todays_new_shop_orders'=>$todays_new_shop_orders, 
					'todays_new_home_orders'=>$todays_new_home_orders, 
					'todays_online_sales'=>round($todays_online_sales['amount'],2), 
					'todays_offline_sales'=>round($todays_offline_sales['amount'],2), 
					'todays_confirmed_orders'=>$todays_confirmed_orders, 
					'todays_total_orders'=>$total_confirmed_orders, 
					'todays_closed_orders'=>$todays_closed_orders, 
	          		
	          	];
				//---------------------
 
				//customers section 
			   
				// $total_referred_customer = Booking::Where('sp_id',$user_id)
				// 				->leftJoin('users', function($join) {
				// 			      $join->on('bookings.user_id', '=', 'users.id');
				// 			    })
				// 			    ->where('users.refer_code',$user->share_code)
				// 				->count();


				$total_referred_customer = User::where('refer_code',$user->share_code)->count();


				$share_code = $user->share_code;
				$total_online_customer = Booking::Where('sp_id',$user_id)
								->leftJoin('users', function($join) {
							      $join->on('bookings.user_id', '=', 'users.id');
							    })
							    ->where('bookings.user_id','!=','')
				          		->where(function ($query) use ($share_code){
							        $query->where('users.refer_code','!=',$share_code)
				          			->orWhereNull('users.refer_code');

							    })->distinct()->count('bookings.user_id');
				          		
				$total_ask_res = ForumCategory::where('category_id',$user['category_id'])->pluck('forum_id');
		
				$total_res = ForumComment::whereIn('forum_id',$total_ask_res)->where('sp_id',$user_id)->count();

				$result_arr['customers'] = [
					'total_referred_customer'=>$total_referred_customer, 
					'total_online_customer'=>$total_online_customer, 
					'todays_ask_received'=>count($total_ask_res), 
					'pending_response'=>count($total_ask_res) - $total_res, 
				
	          		
	          	];

				//----------------------

				//operations section
				  $todays_shop_appointments = Booking::where('booking_service_type','shop')
				  						// Where('status','0')
										->where('sp_id',$user_id)
										->where('appointment_date', $current_date)
										->count();
				  $todays_home_appointments = Booking::where('booking_service_type','home')
				  						// Where('status','0')
										->where('sp_id',$user_id)
										->where('appointment_date', $current_date)
										->count();

				  $total_staff = Staff::where('sp_id',$user_id)->count();

				  $leave_staff = Staff::leftJoin('staff_leaves', function($join) {
										      $join->on('staff.id', '=', 'staff_leaves.staff_id');
										    })
										    ->where('staff.sp_id',$user_id)
										    ->where('staff_leaves.from_date','<=',$current_date)
											->where('staff_leaves.to_date','>=',$current_date)
				  							->count();

				  $available_staff = $total_staff - $leave_staff;

				  $todays_cancellations = Booking::Where('status','2')
										->where('sp_id',$user_id)
										->whereDate('cancelled_date_time', $current_date)
										->count();


				$result_arr['operations'] = [
					'todays_shop_appointments'=>$todays_shop_appointments, 
					'todays_home_appointments'=>$todays_home_appointments, 
					'total_staff'=>$total_staff, 
					'available_staff'=>$available_staff, 
					'todays_cancellations'=>$todays_cancellations, 
				
	          		
	          	];


				//----------------

				//reality check
				  $total_staff_likes = Review::
								leftJoin('staff', function($join) {
							      $join->on('staff.id', '=', 'reviews.staff_id');
							    })
							    ->select('staff.id','staff.staff_name',DB::raw('count(reviews.staff_like_dislike) as total_like'))
							    ->where('reviews.sp_id',$user_id)
							    ->where('reviews.staff_like_dislike','1')
							    ->groupBy('reviews.staff_id')
							    ->orderBy('total_like','DESC')
							    ->first();

				    $total_offer_likes = SpOffer::
								leftJoin('spectacular_offer_likes', function($join) {
							      $join->on('sp_offers.id', '=', 'spectacular_offer_likes.offer_id');
							    })
							    ->leftJoin('offers', function($join) {
							      $join->on('offers.id', '=', 'sp_offers.offer_id');
							    })

							    // ->leftJoin('spectacular_offer_types', function($join) {
							    //   $join->on('spectacular_offer_types.id', '=', 'sp_offers.spectacular_offer_type_id');
							    // })


							    ->select('sp_offers.id','offers.'.$offer_title.' AS offer_name',DB::raw('count(spectacular_offer_likes.id) as total_like'))
							    ->where('sp_offers.user_id',$user_id)
							    ->groupBy('spectacular_offer_likes.offer_id')
							    ->orderBy('total_like','DESC')
							    ->first();
							    // echo $total_offer_likes;die;

					$total_services_likes = Review::
								leftJoin('review_sp_services', function($join) {
							      $join->on('review_sp_services.review_id', '=', 'reviews.id');
							    })
							    ->leftJoin('sp_services', function($join) {
							      $join->on('review_sp_services.service_id', '=', 'sp_services.id');
							    })
							    ->leftJoin('services', function($join) {
							      $join->on('sp_services.service_id', '=', 'services.id');
							    })
							    ->select('services.'.$sr_name.' AS name',DB::raw('count(review_sp_services.like_dislike) as total_like'))
							    ->where('reviews.sp_id',$user_id)
							    ->where('review_sp_services.like_dislike','1')
							    ->groupBy('review_sp_services.service_id')
							    ->orderBy('total_like','DESC')
							    ->first();

							    // print_r($total_services_likes);die;
				   $average_rating = $this->getCombinedReviews($user_id)['average_rating'];
				   
				   $result_arr['reality_check'] = [

					'beutics_score'=>$average_rating, 
					'staff_preferred'=>$total_staff_likes, 
					'offer_preferred'=>$total_offer_likes, 
					'service_preferred'=>$total_services_likes, 
	          		
	          	];
				//-----------
	          	
          		return response()->json(['status' => true, 'message' => 'Data','data' => $result_arr]);	
          	  }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function getCombinedReviews($sp_id)
    {
        $reviews_ques_arr = \Config::get('constants.review_questions_arr');
        $rating_arr = \Config::get('constants.rating_arr');

        $reviews = Review::where('sp_id',$sp_id)->get();

        if(count($reviews)>0){

            $review_arr = array();
        
            foreach($reviews as $rows)
            {
                $rating_calc = array();
                foreach($rows->getAssociatedReviewQuestions as $review_ques)
                {
                    $ques_value =  $reviews_ques_arr[$review_ques['ques_id']]['value'];
                    $ans =  $rating_arr[$review_ques['rating']]['value'];

                    $rating_calc[] = $ques_value*$ans;
                }

                $final_rating = (array_sum($rating_calc))/10;

                $review_arr[$rows['id']] = $final_rating;

            }
             
            $total_reviews = array_sum($review_arr)/count($review_arr);
        
            $finale_data = array();
            $finale_data['average_rating'] = round($total_reviews,1);
            $finale_data['total_reviewers'] = count($review_arr);
            
            return  $finale_data;
        }
    }

    public function myAccount(Request $request)
    {
    	if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          	$result_arr = array();

	          	$tier = SpUser::leftJoin('tiers', function($join) {
						      $join->on('sp_users.tier_id', '=', 'tiers.id');
						    })
		                ->where('sp_users.id',$user_id)
		                ->select('tiers.commission','sp_users.is_tax_applied')
		                ->first();

		        $tax = Setting::select('percentage')->first();

		        //earnings
	          	$total_online_sales = Booking::where('sp_id',$user_id)
	          				->where('status','5')
	          				->where('payment_type','0')
	          				->where('payment_settlement_status','unpaid')
	          				->select(DB::raw('sum(CASE 
		                        WHEN IFNULL(final_settlement_amount,0) = 0 THEN total_item_amount 
		                        ELSE final_settlement_amount
		                        END) AS total_online_sale'))
	          				->first();
	          				// echo $total_online_sales;die;

	          	$total_offline_sales = Booking::where('sp_id',$user_id)
	          				->where('status','5')
	          				->where('payment_settlement_status','unpaid')
	          				->where('payment_type','1')
	          				->select(DB::raw('sum(CASE 
		                        WHEN IFNULL(final_settlement_amount,0) = 0 THEN total_item_amount 
		                        ELSE final_settlement_amount
		                        END) AS total_offline_sale'))
	          				->first();
	          				
	          	// check sp_referred customer

	            $sp_ref = Booking::leftJoin('sp_users', function($join) {
						      $join->on('bookings.sp_id', '=', 'sp_users.id');
						    })
	            		    ->leftJoin('users', function($join) {
						      $join->on('bookings.user_id', '=', 'users.id');
						    })
						    ->whereNotNull('user_id')
						    ->where('bookings.status','5')
						    ->where('payment_type','1')
						    ->where('bookings.payment_settlement_status','unpaid')
						    ->whereRaw('sp_users.share_code = users.refer_code')
						    ->where('bookings.sp_id',$user_id)
						    ->distinct('bookings.user_id')
	            			->pluck('bookings.user_id');


	          	$total_off_without_sp = Booking::where('sp_id',$user_id)
	          				->where('status','5')
	          				->where('payment_settlement_status','unpaid')
	          				->whereNull('db_user_name')
	          				->whereNull('db_user_mobile_no')
	          				 ->where('payment_type','1')
	          				->whereNotIn('user_id',$sp_ref)
	          				->select(DB::raw('sum(CASE 
		                        WHEN IFNULL(final_settlement_amount,0) = 0 THEN total_item_amount 
		                        ELSE final_settlement_amount
		                        END) AS total_sales'))
	          				->first();

	          	$total_sales['total_sales'] = $total_online_sales['total_online_sale'] + $total_off_without_sp['total_sales'];

	            $beutics_commission = ($total_sales['total_sales'] * $tier['commission'])/100;

	          	$net_store_sales = $total_online_sales['total_online_sale'] + $total_offline_sales['total_offline_sale'];

	          	if($tier['is_tax_applied'] != '1'){
	          		$my_earnings = $total_sales['total_sales'] - $beutics_commission;
	          	}else{

	          		$earning = $total_sales['total_sales'] - $beutics_commission;
	          		// echo $total_sales['total_sales'].'<br>';
	          		// echo $beutics_commission.'<br>';
	          		// echo $earning;die;
	          		$my_earnings = $earning + ($earning * $tax['percentage'])/100;
	          	}
	          	// echo $my_earnings;die;
	          	//outstanding

	          	$calc_pay_store = ((100-$tier['commission'])*$total_online_sales['total_online_sale'])/100;


	          	$calc_pay_beutics_amt = Booking::where('sp_id',$user_id)
	          				->where('status','5')
	          				->where('payment_settlement_status','unpaid')
	          				->whereNull('db_user_name')
	          				->whereNull('db_user_mobile_no')
	          				->where('payment_type','1')
	          				->whereNotIn('user_id',$sp_ref)
	          				->select(DB::raw('sum(CASE 
		                        WHEN IFNULL(final_settlement_amount,0) = 0 THEN total_item_amount 
		                        ELSE final_settlement_amount
		                        END) AS pay_to_beutics'))
	          				->first();


	          	$calc_pay_beutics = ($tier['commission'] * $calc_pay_beutics_amt['pay_to_beutics'])/100;

	          	

	          	$outstanding = $calc_pay_store-$calc_pay_beutics;
	          	
	          	$sign = '';
          		if($tier['is_tax_applied'] == '1'){
          			if($outstanding>0){
          				$calc_outstanding = $outstanding+(($outstanding*$tax['percentage'])/100);

          			}else{
          				$calc_outstanding = abs($outstanding)+((abs($outstanding)*$tax['percentage'])/100);
          				$sign = '-';
          			}
          			$result_arr['outstanding'] = round($sign.$calc_outstanding,2);
          		}else{
          			$result_arr['outstanding'] = round($outstanding,2);
          		}
	          		
	          		          	//life time earning section

	          	$total_lt_online_sales = Booking::where('sp_id',$user_id)
	          				->where('status','5')
	          				->where('payment_type','0')
	          				->where('payment_settlement_status','paid')
	          				->select(DB::raw('sum(CASE 
		                        WHEN IFNULL(final_settlement_amount,0) = 0 THEN total_item_amount 
		                        ELSE final_settlement_amount
		                        END) AS total_online_sale'))
	          				->first();
	          				// echo $total_online_sales;die;

	          	$total__lt_offline_sales = Booking::where('sp_id',$user_id)
	          				->where('status','5')
	          				->where('payment_settlement_status','paid')
	          				->where('payment_type','1')
	          				->select(DB::raw('sum(CASE 
		                        WHEN IFNULL(final_settlement_amount,0) = 0 THEN total_item_amount 
		                        ELSE final_settlement_amount
		                        END) AS total_offline_sale'))
	          				->first();
	          	$life_time_earning = $total_lt_online_sales['total_online_sale'] + $total__lt_offline_sales['total_offline_sale'];

	          	$result_arr['total_lifetime_online_earning'] = round($total_lt_online_sales['total_online_sale'],2);
	          	$result_arr['total_lifetime_offline_earning'] = round($total__lt_offline_sales['total_offline_sale'],2);
	          	$result_arr['total_lifetime_earning'] = round($life_time_earning,2);
	          	$result_arr['total_online_sales'] = round($total_online_sales['total_online_sale'],2);
	          	$result_arr['total_offline_sale'] = round($total_offline_sales['total_offline_sale'],2);
	          	$result_arr['net_store_sales'] = round($net_store_sales,2);
	          	$result_arr['total_sales'] = round($total_sales['total_sales'],2);
	          	$result_arr['beutics_commission'] = round($beutics_commission,2);
	          	$result_arr['my_earnings'] = round($my_earnings,2);
	          	$result_arr['VAT'] = $tier['is_tax_applied'];
	          	$result_arr['commission'] = $tier['commission'];
	          	
	          	
          		return response()->json(['status' => true, 'message' => 'Order Details','data' => $result_arr]);	
          	  }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
    }

    public function getOnlineOfflineSales(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
				  'status' => 'required|in:online,offline,all,paid_order',
				  'page'=>'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {

	          	// check sp_referred customer
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          	$currentpage = $request->page;

          		Paginator::currentPageResolver(function () use ($currentpage) {
			        return $currentpage;
			    });
	            $sp_ref = Booking::leftJoin('sp_users', function($join) {
						      $join->on('bookings.sp_id', '=', 'sp_users.id');
						    })
	            		    ->leftJoin('users', function($join) {
						      $join->on('bookings.user_id', '=', 'users.id');
						    })
						    ->whereNotNull('user_id')
						    ->where('bookings.status','5')
						    ->where('payment_type','1')
						    // ->where('bookings.payment_settlement_status','paid')
						    ->where('bookings.payment_settlement_status','unpaid')
						    ->whereRaw('sp_users.share_code = users.refer_code')
						    ->where('bookings.sp_id',$user_id)
						    ->distinct('bookings.user_id')
	            			->pluck('bookings.user_id');

	            if($request->status == 'paid_order'){
	            	$orders = Booking::where('sp_id',$user_id)
	          				->where('status','5')
	          				->where('payment_settlement_status','paid')
	          				// ->whereNull('db_user_name')
	          				// ->whereNull('db_user_mobile_no')
	          				// ->whereNotIn('user_id',$sp_ref)
	          				->orderBy('id','DESC')
	          				->paginate(10);
	            }elseif ($request->status == 'online') {
	            	$orders = Booking::where('sp_id',$user_id)
	          				->where('status','5')
	          				->where('payment_type','0')
	          				->where('payment_settlement_status','unpaid')
	          				->orderBy('id','DESC')
	          				->paginate(10);
	            }elseif ($request->status == 'offline') {
	            	$orders = Booking::where('sp_id',$user_id)
	          				->where('status','5')
	          				->where('payment_type','1')
	          				->where('payment_settlement_status','unpaid')
	          				->orderBy('id','DESC')
	          				->paginate(10);
	            }else{

	            	$orders = Booking::
	          				where(function ($query) use($user_id){
		          		
		          				$query->where('sp_id',$user_id)
		          			    ->where('payment_type','0')
		          				->where('status','5')
		          				->where('payment_settlement_status','unpaid')
		          				->whereNull('db_user_name')
		          				->whereNull('db_user_mobile_no');
				          		
				          	})

	          				->orWhere(function ($query) use($sp_ref,$user_id){
		          		
		          				$query->where('sp_id',$user_id)
		          				->where('status','5')
		          				->where('payment_settlement_status','unpaid')
		          				->whereNull('db_user_name')
		          				->whereNull('db_user_mobile_no')
		          				->where('payment_type','1')
		          				->whereNotIn('user_id',$sp_ref);
				          		
				          	})
	          				
	          				->orderBy('id','DESC')
	          				->paginate(10);
	            }
	            $rows_arr = $orders->toArray();
	           
	   			$res = $this->showAllBookingDetails($orders);

	   			$result['data'] = $res;
                $result['current_page']     = $rows_arr['current_page'];
                $result['from']             = $rows_arr['from'];
                $result['last_page']        = $rows_arr['last_page'];
                $result['next_page_url']    = $rows_arr['next_page_url'];
                $result['per_page']         = $rows_arr['per_page'];
                $result['prev_page_url']    = $rows_arr['prev_page_url'];
                $result['to']               = $rows_arr['to'];
                $result['total']            = $rows_arr['total'];
          		return response()->json(['status' => true, 'message' => 'Order Details','data' => $result]);	
          	  }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function showAllBookingDetails($rows)
	{
		
		$result = array();
		$user_name = '';
		foreach($rows as $row){
			
			$data = array();
			if($row['user_id']=='' && $row['user_id']==null && $row['db_user_name']!=''){
				$user_name = $row['db_user_name'];

			}elseif ($row['user_id']=='' && $row['user_id']==null && $row['is_gift']=='1') {
				$user_name = $row['receiver_name'];
			}else{
				$user_name = $row->getAssociatedUserInfo->name;
			}

			$is_referred = 0;
			//check customer uses sp refer code or not

			if($row['user_id']!='' && $row['user_id']!=null){
				$cust_refer_code = $row->getAssociatedUserInfo->refer_code;
			
				$check_sp_code = SpUser::where('share_code',$cust_refer_code)->where('id',$row['sp_id'])->exists();
				if($check_sp_code){
					$is_referred = 1;
				}
			}


			
			$data['id'] = $row['id'];
			$data['booking_unique_id'] = $row['booking_unique_id'];
			$data['sp_id'] = $row['sp_id'];
			$data['user_name'] = $user_name;
			$data['user_profile_image'] = (!empty($row->getAssociatedUserInfo->image) && !empty($row['user_id'])) ? asset($row->getAssociatedUserInfo->image):'';
			$data['user_id'] = $row['user_id'];
			$data['staff_id'] = $row['staff_id'];
			if($row['staff_id']!='' && $row['staff_id']!=null){
				$data['staff_name'] = $row->getAssociatedStaffInformation->staff_name;
			}else{
				$data['staff_name'] = "";
			}
			$data['db_user_name'] = $row['db_user_name'];
			$data['db_user_mobile_no'] = $row['db_user_mobile_no'];
			
			$data['appointment_time'] = ($row['appointment_time']==null || strpos($row['appointment_time'], ':') === 2) ? $row['appointment_time'] : date('H:i',$row['appointment_time']);
			$data['service_end_time'] = ($row['service_end_time']==null||  strpos($row['service_end_time'], ':') === 2) ? $row['service_end_time'] : date('H:i',$row['service_end_time']);
			$data['appointment_date'] = $row['appointment_date']!='' && $row['appointment_date']!= null ? $row['appointment_date']:"";
			
			$data['completed_date_time'] = $row['completed_date_time']!='' && $row['completed_date_time']!= null ? date('d-M-Y H:i:s',strtotime($row['completed_date_time'])):"";

			$data['redeemed_reward_points'] = $row['redeemed_reward_points'];
			// $data['payment_type'] = $row['payment_type'];
			$data['promo_code_amount'] = round($row['promo_code_amount'],2);

			if($row['final_settlement_amount']!='' && $row['final_settlement_amount']!=null){
				$data['tax_amount'] = round($row['final_settlement_tax'],2);
				$data['total_amount'] = round($row['final_settlement_amount'],2);
				
				$data['total_cashback'] = round($row['final_settlement_cashback'],2);
			}else{
				$data['tax_amount'] = round($row['tax_amount'],2);
				// $data['paid_amount'] = round($row['paid_amount'],2);
				// $data['total_amount'] = round($row['booking_total_amount'],2);
				$data['total_amount'] = round($row['total_item_amount'],2);
				$data['total_cashback'] = round($row['total_cashback'],2);
			}
			
			$data['paid_amount'] = round($row['paid_amount'],2);
			$data['total_item_amount'] = round($row['total_item_amount'],2);
			$data['created_date'] = date('d-M-Y H:i:s',strtotime($row->created_at));
			$data['payment_type'] = $row['payment_type']=='0'?'online':'offline';
			$data['is_sp_referred'] = $is_referred;
			$data['booking_service_type'] = $row['booking_service_type'];
			if($row['status'] == '2'){
				$data['cancel_reason'] = $row['cancellation_reason'];
				$data['cancel_refunded_amount'] = round($row['cancellation_refunded_amount'],2);
			}
			$item_name_arr = array();
			foreach($row->getAssociatedBookingItems as $bkitems){

				if($bkitems['is_service'] == '1')
				{
					$item_name_arr[] = [
						'service_id' => $bkitems['booking_item_id'],
						'name' => $bkitems->getAssociatedServiceDetail->getAssociatedService->name,
						'quantity' => $bkitems['quantity'],
						'original_price' => round($bkitems['original_price'],2),
						'best_price' => round($bkitems['best_price'],2),
						'cashback' => round($bkitems['cashback'],2),
					];
				}
				else{
					$offer_services = array();

					if(isset($bkitems->getAssociatedOfferDetail->getAssociatedOfferServices)){
						foreach($bkitems->getAssociatedOfferDetail->getAssociatedOfferServices as $os){

							$offer_services[] = [
								'quantity' => $os['quantity'],
								'name' => $os->getAssociatedOfferServicesName->getAssociatedService->name,
							];
						}

						$item_name_arr[] = [
							'offer_id' => $bkitems['booking_item_id'],
							'name' => $bkitems->getAssociatedOfferDetail->getAssociatedOfferName->title,
							'quantity' => $bkitems['quantity'],
							'original_price' => round($bkitems['original_price'],2),
							'best_price' => round($bkitems['best_price'],2),
							'cashback' => round($bkitems['cashback'],2),
							'services'=>$offer_services,
						];
					}
				}

			}
			$data['booking_items'] = $item_name_arr;
			$result[] = $data;
		}
		
		return $result;
				
	}

	public function changeNotificationSetting(Request $request)
    {
    	if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	              'status' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          	
	          	$row = SpUser::where('id',$user_id)->first();

	          	if($row){

	          		$row->notification_alert_status = (string)$request->status;
		          	$row->save();

		          	return response()->json(['status' => true,'data'=>$row]);
	          	}
	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
    }

    public function notificationsListing(Request $request)
    {
    	if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	              'page' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          	
	          	$currentpage = $request->page;

          		Paginator::currentPageResolver(function () use ($currentpage) {
			        return $currentpage;
			    });

	          	$rows = Notification::where('user_id',$user_id)->where('user_type','sp')->orderBy('id','DESC')->paginate(10);

	          	$rows_arr = $rows->toArray();

	          	$notification_arr_1 = array();
	          	$ref_name = '';
	          	if(count($rows)>0){
	          		foreach($rows as $val){

	          			$notification_arr = array();
	          			$item_name_arr = array();
	          			$notification_arr['id'] = $val['id'];
	          			$notification_arr['ref_id'] = $val['ref_id'];
	          			$notification_arr['ref_user_id'] = $val['ref_user_id'];

	          			// if($val['ref_type']!='E_GIFT_CARD_RECEIVED' && $val['ref_type']!='LOW_BALANCE')
	          			// {
	          			// 	$ref_name = isset($val->getAssociatedRefSpInfo->store_name)?$val->getAssociatedRefSpInfo->store_name:'';

	          			// 	$notification_arr['sp_name'] = isset($val->getAssociatedRefSpInfo->store_name)?$val->getAssociatedRefSpInfo->store_name:'';
	          			// 	$notification_arr['sp_image'] = isset($val->getAssociatedRefSpInfo->profile_image) ? asset('sp_uploads/profile/'.$val->getAssociatedRefSpInfo->profile_image):'';
	          			// }
	          			if($val['ref_type'] =='NEW_BOOKING' || $val['ref_type'] =='ORDER_CANCELLED' || $val['ref_type'] =='ASK_EXPERT' || $val['ref_type'] =='REFERRAL_JOINED'){
	          				
	          				$ref_name = isset($val->getAssociatedRefUserInfo->name)?$val->getAssociatedRefUserInfo->name:'';

	          				$notification_arr['user_name'] = isset($val->getAssociatedRefUserInfo->name)?$val->getAssociatedRefUserInfo->name:'';
	          				$notification_arr['user_image'] = isset($val->getAssociatedRefUserInfo->image) ? asset($val->getAssociatedRefUserInfo->image):'';

	          			}
	          			if($val['ref_type'] =='NEW_BOOKING' || $val['ref_type'] =='ORDER_CANCELLED')
	          			{
	          				if(isset($val->getAssociatedBookingDetail->getAssociatedBookingItems)){
	          					foreach($val->getAssociatedBookingDetail->getAssociatedBookingItems as $bkitems){
									if($bkitems['is_service'] == '1')
									{
										$item_name_arr[] = [
											'service_id' => $bkitems['booking_item_id'],
											'name' => $bkitems->getAssociatedServiceDetail->getAssociatedService->name,
											'quantity' => $bkitems['quantity'],
										];
									}
									else{

										$offer_services = array();

										if(isset($bkitems->getAssociatedOfferDetail->getAssociatedOfferServices)){
											foreach($bkitems->getAssociatedOfferDetail->getAssociatedOfferServices as $os){

												$offer_services[] = [
													'quantity' => $os['quantity'],
													'name' => $os->getAssociatedOfferServicesName->getAssociatedService->name,
												];
											}

											$item_name_arr[] = [
												'offer_id' => $bkitems['booking_item_id'],
												'name' => $bkitems->getAssociatedOfferDetail->getAssociatedOfferName->title,
												'quantity' => $bkitems['quantity'],
												'original_price' => round($bkitems['original_price'],2),
												'best_price' => round($bkitems['best_price'],2),
												'cashback' => round($bkitems['cashback'],2),
												'services'=>$offer_services,
											];
										}
									}

								}

		          				
	          				}
	          				
	          				$notification_arr['services'] = $item_name_arr;
	          			}
	          			
	          			$notification_arr['ref_type'] = $val['ref_type'];
	          			// $notification_arr['message'] = $val['message'];
	          			// $notification_arr['title'] = $val['notification_title'];
	          			$notification_arr['message'] = $val['message'.$this->col_postfix];
	          			$notification_arr['title'] = $val['notification_title'.$this->col_postfix];
	          			$notification_arr['created_at'] = date('Y-m-d H:i:s',strtotime($val['created_at']));
	          			$notification_arr_1[] = $notification_arr;		          		
	          		}

	          		$result['data'] = $notification_arr_1;
	                $result['current_page']     = $rows_arr['current_page'];
	                $result['from']             = $rows_arr['from'];
	                $result['last_page']        = $rows_arr['last_page'];
	                $result['next_page_url']    = $rows_arr['next_page_url'];
	                $result['per_page']         = $rows_arr['per_page'];
	                $result['prev_page_url']    = $rows_arr['prev_page_url'];
	                $result['to']               = $rows_arr['to'];
	                $result['total']            = $rows_arr['total'];
	          		return response()->json(['status' => true, 'message' => 'Notification Listing', 'data' => $result]);
	          	}else{
	          		return response()->json(['status' => true, 'message' => 'No Data Found', 'data' => []]);
	          	}
	          }//end else
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
    }

    public function updateAppVersion(Request $request)
    {
    	if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	              // 'update_force'=>'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          	$app_version = Setting::select('sp_android_app_version','sp_ios_app_version','sp_android_app_force','sp_ios_app_force')->first()->toArray();

	          	// $user = SpUser::where('id',$user_id)->first();

	            return response()->json(['status' => true,'message'=>'The app is outdated.Please, update it to the latest version.','app_version'=>$app_version]);
          		
	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
    }

    public function updateUserBadge(Request $request)
    {
    	if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	              'token' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

	          	$device = SpDevice::where('user_id',$user_id)->where('device_token',$request->token)->first();
	          	if($device){
	          		$device->badge_count = 0;
		          	$device->save();
		            return response()->json(['status' => true]);
	          	}else{
	          		return response()->json(['status' => true,'message'=>'Record not found']);
	          	}
	          	
          		
	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
    }

    public function updateUserLanguage(Request $request)
    {
    	if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required|in:en,ar',
	              'device_token' => 'required',
	              'device_type' => 'required|in:IPHONE,ANDROID,IOS',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          	
	          	$device = SpDevice::where('user_id',$user_id)->where('device_token',$request->device_token)->first();
	          	if($device){
	          		$device->language = $request->language;
		          	$device->save();
		           
	          	}else{
	          		$device = new SpDevice();
	          		$device->user_id = $user_id;
	          		$device->device_type = $request->device_type;
	          		$device->device_token = $request->device_token;
	          		$device->language = $request->language;
		          	$device->save();
	          	}
	          	 return response()->json(['status' => true]);
          		
	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getLine(), 'data' => []]);
	       }
		}
    }


    public function checkUnreadNotification(Request $request)
    {
    	if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          	$unread_notifications = Notification::where('user_id',$user_id)
	          			->where('user_type','sp')
	          			->where('read_notification','0')
	          			->count();

	            return response()->json(['status' => true, 'message' => 'Unread Notifications', 'data' => $unread_notifications]);
          		
	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
    }

    public function readNotifications(Request $request)
    {
    	if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'language' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          	$rows = Notification::where('user_id',$user_id)
	          			->where('user_type','sp')
	          			->where('read_notification','0')
	          			->update(array('read_notification'=>'1'));

	            return response()->json(['status' => true]);
          		
	          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
    }

    /* CRON FUNCTIONS START */
    
    //Offer expiry – Store
    function sp_offer_expire(){ //echo date('Y-m-d', strtotime('+7days'));die;
   		
   		// For 7 days
    	$seven_sp_users = SpOffer::where('expiry_date',date('Y-m-d', strtotime('+7days')))->where('is_nominated','!=','1')->select('id', 'user_id','offer_id')->get(); 

    	if(!empty($seven_sp_users)){
    		foreach($seven_sp_users as $rows){
	    		$message = '';
	    		if($rows->getAssociatedOfferProviderName->notification_alert_status == '1'){
	    			// $message = "Your offer is about to expire! This is just a heads up to let you know that your ".$rows->getAssociatedOfferName->title." offer is expiring in 7 days. Tap here to update the offer.";

	    			$message_main = __('messages.Offer Expiry',['title' => $rows->getAssociatedOfferName->title]);
	    			$message_title_main = __("messages.Offer Expiry!");

	    			app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

					$message_other = __('messages.Offer Expiry',['title' => $rows->getAssociatedOfferName->title]);
	    			$message_title_other = __("messages.Offer Expiry!");


					Notification::saveNotification($rows->user_id,'',$rows->id,'OFFER_EXPIRY',$message_main,$message_other,$message_title_main,$message_title_other,'sp');
					app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');
				}
	    	}
    	} 	
    	
    	// For 1 day
    	$one_sp_users = SpOffer::where('expiry_date',date('Y-m-d', strtotime('+1day')))->where('is_nominated','!=','1')->select('id', 'user_id','offer_id')->get(); 

    	if(!empty($one_sp_users)){
    		foreach($one_sp_users as $row){
	    		$message = '';
	    		if($row->getAssociatedOfferProviderName->notification_alert_status == '1'){
	    			// $message = "Your offer is about to expire! This is just a heads up to let you know that your ".$row->getAssociatedOfferName->title." offer is expiring in 1 day. Extend your offer to encourage more orders. Tap here to update the offer.";

	    			$message_main = __('messages.Offer Expiry',['title' => $rows->getAssociatedOfferName->title]);
	    			$message_title_main = __("messages.Offer Expiry!");



	    			app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

					$message_other = __('messages.Offer Expiry',['title' => $rows->getAssociatedOfferName->title]);
	    			$message_title_other = __("messages.Offer Expiry!");

					Notification::saveNotification($row->user_id,'',$row->id,'OFFER_EXPIRY',$message_main,$message_other,$message_title_main,$message_title_other,'sp');
				}
	    	}
    	}
    	die('Offer expiry – Store - Cron Run Successfully');
    }

    //Enter staff – Store
    function sp_enter_staff(){
   		$message_title = __("messages.Staff details missing!!");
   		$message_main = __('messages.Staff details missing!');

   		app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

		$message_title_other = __("messages.Staff details missing!!");
   		$message_other = __('messages.Staff details missing!');

    	$sp_users = SpUser::select('id','notification_alert_status')->where('notification_alert_status', '1')->get(); 

    	if(!empty($sp_users)){
    		foreach($sp_users as $rows){
	    		if(empty($rows->getAssociatedStaff->toArray())){
	    			Notification::saveNotification($rows->id,'','','ENTER_STAFF',$message_main,$message_other,$message_title_main,$message_title_other,'sp');
				}
	    	}
    	} 	
    	
    	die('Enter staff – Store - Cron Run Successfully');
    }

    //Enter products – Store
    function sp_enter_product(){
   		$message_title_main = __("messages.Add your products!!");
   		$message_main = __('messages.Add your products!');

   		app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

		$message_title_other = __("messages.Add your products!!");
   		$message_other = __('messages.Add your products!');

    	$sp_users = SpUser::select('id','notification_alert_status')->where('notification_alert_status', '1')->get(); 

    	if(!empty($sp_users)){
    		foreach($sp_users as $rows){
	    		if(empty($rows->getAssociatedProducts->toArray())){
	    			Notification::saveNotification($rows->id,'','','ENTER_PRODUCTS',$message_main,$message_other,$message_title_main,$message_title_other,'sp');
				}
	    	}
    	}
    	
    	die('Enter products – Store - Cron Run Successfully');
    }

    //Enter services – Store
    function sp_enter_service(){
   		$message_title_main = __("messages.Add your services!!");
   		$message_main = __('messages.Add your services!');

   		app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

   		$message_title_other = __("messages.Add your services!!");
   		$message_other = __('messages.Add your services!');

    	$sp_users = SpUser::select('id','notification_alert_status')->where('notification_alert_status', '1')->get(); 

    	if(!empty($sp_users)){
    		foreach($sp_users as $rows){
	    		if(empty($rows->getSpService->toArray())){
	    			Notification::saveNotification($rows->id,'','','ENTER_SERVICES',$message_main,$message_other,$message_title_main,$message_title_other,'sp');
	    			app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');
				}
	    	}
    	}
    	
    	die('Enter services – Store - Cron Run Successfully');
    }

    //Enter offers – Store
    function sp_enter_offer(){
   		$message_title_main = __("messages.Add your offers!!");
   		$message_main = __('messages.Add your offers!');

   		app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

		$message_title_other = __("messages.Add your offers!!");
   		$message_other = __('messages.Add your offers!');

    	$sp_users = SpUser::select('id','notification_alert_status')->where('notification_alert_status', '1')->get(); 

    	if(!empty($sp_users)){
    		foreach($sp_users as $rows){
	    		if(empty($rows->getSpOffers->toArray())){
	    			Notification::saveNotification($rows->id,'','','ENTER_OFFERS',$message_main,$message_other,$message_title_main,$message_title_other,'sp');
	    			app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');
				}
	    	}
    	}
    	
    	die('Enter offers – Store - Cron Run Successfully');
    }

    //Enter store timings – Store
    function sp_enter_store_timing(){
   		$message_title_main = __("messages.Add store timings!");
   		$message_main = __('messages.Add store timings');

   		app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

		$message_title_other = __("messages.Add store timings!");
   		$message_other = __('messages.Add store timings');

    	$sp_users = SpUser::select('id','notification_alert_status')->where('notification_alert_status', '1')->get(); 
    
    	if(!empty($sp_users)){
    		foreach($sp_users as $rows){
    			$shop_timing_count = 0;
    			$shop_timing_count = ShopTiming::where('user_id', $rows->id)->where('isOpen', '0')->count();
    			if($shop_timing_count == '7'){
					Notification::saveNotification($rows->id,'','','ENTER_STORE_TIMINGS',$message_main,$message_other,$message_title_main,$message_title_other,'sp');
	    		}
	    	}
    	}
    	
    	die('Enter store timings – Store - Cron Run Successfully');
    }


    //Enter bank account – Store
    function sp_enter_bank(){
   		$message_title_main = __("messages.Add bank transfer details!!");
   		$message_main = __('messages.Add bank transfer details!');

   		app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

		$message_title_other = __("messages.Add bank transfer details!!");
   		$message_other = __('messages.Add bank transfer details!');


    	$sp_users = SpUser::select('id','notification_alert_status','account_title','account_iban','branch_name','branch_address','bank')
    		->where('notification_alert_status', '1')
    		->where('account_title', null)
    		->where('account_iban', null)
    		->where('branch_name', null)
    		->where('branch_address', null)
    		->where('bank', null)
    		->get(); 

    	if(!empty($sp_users)){
    		foreach($sp_users as $rows){ 
	    		Notification::saveNotification($rows->id,'','','ENTER_BANK',$message_main,$message_other,$message_title_main,$message_title_other,'sp');
	    	}
    	}
    	
    	die('Enter bank account – Store - Cron Run Successfully');
    }

    //Close the service – Store
    function sp_close_service(){
   		
    	$bookings = Booking::leftJoin('sp_users', function($join) {
				      $join->on('bookings.sp_id', '=', 'sp_users.id');
			    })
    		->select('bookings.id','bookings.sp_id', 'sp_users.store_name')
    		->where('sp_users.notification_alert_status', '1')
    		->where('bookings.appointment_date', '<', date('Y-m-d'))
    		->where('bookings.completed_date_time', null)
    		->where('bookings.status', '1')
    		->get();
    	
    	if(!empty($bookings)){
    		$message = '';
    		foreach($bookings as $rows){
    			$message_title_main = __("messages.Close the Service!!");
    			$message_main = __('messages.Close the Service!',['store_name' => $rows->store_name]);

    			app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

				$message_title_other =  __("messages.Close the Service!!");
    			$message_other = __('messages.Close the Service!',['store_name' => $rows->store_name]);

	    		Notification::saveNotification($rows->sp_id,'',$rows->id,'CLOSE_SERVICE',$message_main,$message_other,$message_title_main,$message_title_other,'sp');
	    	}
    	}
    	
    	die('Close the service – Store - Cron Run Successfully');
    }

    //Promo Code reminder – customer
    function cs_promo_reminder(){
   		
    	$promo_codes = PromoCode::where('promo_codes.to_date', '>', date('Y-m-d'))
    		->where('promo_codes.reminder_date', date('Y-m-d'))
    		->where('promo_codes.linked_customer', '1')
    		->where('promo_codes.status', '1') 
    		->get(); 
    	//echo '<pre>'; print_r($promo_codes);
    	if(count($promo_codes)>0){
    		foreach($promo_codes as $rows){
    			$promo_code_linked = $rows->getAssociatedCustomers->toArray();
    			if($promo_code_linked){
    				foreach($promo_code_linked as $link_promo){
    					$user_data = User::where('id', $link_promo['user_id'])->first();
    					if($user_data){
    						$user_id = $user_data->id;
    						$promo_code_id = $link_promo['promo_code_id'];

    						$check_bookings = Booking::where('user_id', $user_id)
    							->where('promo_code_id', $promo_code_id)
    							->whereIn('status', array('1', '5'))
    							->count();
    						if($rows['code_limit'] > $check_bookings){
    							$disc = ($rows['code_limit']-$check_bookings);
								$datediff = strtotime($rows['to_date'])-time();
    							$exp_days = round($datediff / (60 * 60 * 24));
    							$message = '';
    	
    							// $message = 'Hey '.$user_data->name.'! You have got '.$disc.' more discount vouchers to be used up. Hurry before they expire in '.$exp_days.' days.';
    							$message_main = __('messages.It about to expire!',['name' => $user_data->name,'discount' => $disc,'exp_days' => $exp_days]);
    							$message_title_main = __("messages.It’s about to expire!");
    							app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');
								$message_other = __('messages.It about to expire!',['name' => $user_data->name,'discount' => $disc,'exp_days' => $exp_days]);
    							$message_title_other = __("messages.It’s about to expire!");

    							Notification::saveNotification($user_id,'','','PROMO_CODE_REMINDER',$message_main,$message_other,$message_title_main,$message_title_other,'customer');
    							 
		                        $reminder_date_fifteen = date('Y-m-d', strtotime(' +15days'));
		                        $reminder_date_three = date('Y-m-d', strtotime($rows['to_date'].' -3days'));
		                       
		                        if($reminder_date_fifteen > $reminder_date_three){
		                           $reminder_date = $reminder_date_three;
		                        } else {
		                            $reminder_date = $reminder_date_fifteen;
		                        }

			                    $update_promo_code = DB::table('promo_codes')->whereIn('promo_codes.id', $promo_code_id)->update(array('promo_codes.reminder_date' => $reminder_date));

    						}
    					}
    				}
    			}
    			//echo '<pre>'; print_r($users);	    		
	    	}
    	}
    	
    	die('Promo Code reminder – customer - Cron Run Successfully');
    }


  
      //Appointment reminder – customer.
    public function appointmentConfirmReminder()
    { 

    	$bookings_three_hours = booking::where('bookings.status', '0') 
    		//->where('bookings.created_at', date('Y-m-d'))
    		->where('bookings.created_at', '>', date('Y-m-d H:i', strtotime('-3 hours')).':01')
    		->where('bookings.created_at', '<', date('Y-m-d H:i', strtotime('-3 hours')).':59')
    		->select('bookings.id','bookings.sp_id','bookings.user_id','booking_unique_id')
    		->get(); 

    	if(!empty($bookings_three_hours)){
    		foreach($bookings_three_hours as $rows){
    			//mail to admin
				$data1['templete'] = "admin_mail";
				$data1['email'] = env("CONTACT_US_EMAIL");
				$data1['subject'] = "Order Confirm Reminder";
				$data1['message'] = $rows->getAssociatedSpInformation->store_name." has a new order please ask him to confirm order{".$rows['booking_unique_id']."}. ";
				send($data1);

    		}

    	}
    	
    	die('Appointment reminder – customer - Cron Run Successfully');
    }


    /* CRON FUNCTIONS END */
}
