<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\Paginator;
use JWTAuth;
use JWTAuthException;

use App\Http\Controllers\Controller;
use App\Model\SpUser;
use App\Model\Product;
use App\Model\Staff;
use App\Model\StaffLeave;
use App\Model\Service;
use App\Model\SpService;
use App\Model\SpServicePrice;
use App\Model\Category;
use App\Model\Offer;
use App\Model\SpOffer;
use App\Model\SpOfferService;
use App\Model\ForumCategory;
use App\Model\Forum;
use App\Model\ForumReplyBannerImage;
use App\Model\ForumComment;
use App\Model\ForumCommentLikeDislike;
use App\Model\Review;
use App\Model\RatingReview;
use App\Model\User;
use App\Model\Booking;
use App\Model\Notification;
use App\Model\StaffImagesVideos;
use App\Model\Country;
use App\Model\SpTags;
use App\Model\SpGallery;
use DB;
use Config;


class ServiceProviderPagesController extends Controller
{
	function __construct(Request $request)
	{
		parent::__construct($request);
	    Config::set('jwt.user', SpUser::class);
	    Config::set('auth.providers', ['users' => [
	            'driver' => 'eloquent',
	            'model' => SpUser::class,
	        ]]);

	    $this->col_postfix = '';

        $this->header = $request->header('language');
        if($this->header!= "en"){
        	$this->col_postfix = '_ar';
        }
	}

	public function addProducts(Request $request)
    {

    	if (Input::isMethod('post')) {
    		try{

    			$validator = Validator::make($request->all(), [
                    'product_name'=>'required|max:50',
                    'product_image'=>'required|mimes:jpeg,jpg,png,gif',
                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{
		        	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	        		$row = new Product();
	        		$row['sp_id'] = $user_id;
	        		$row['product_name'] = $request->product_name;
	        		if ($request->hasFile('product_image') && $request->file('product_image'))
	        		{

	                    $file = $request->file('product_image');
	                    $name = time().'.'.$file->getClientOriginalExtension();
	                    $destinationPath = public_path('sp_uploads/products/');
	                    $img = $file->move($destinationPath, $name);

	                    $row->product_image = $name;
	                }

	                $row->save();
	        		if($row->save())
	        		{
	        			return response()->json(['status' => true, 'message' => __('messages.Product added successfully.')]);
	        		}
		            
		        }
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
    	}
    }

    public function getProducts(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'page_no' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {  	
		          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          		$currentpage = $request->page_no;
	          		Paginator::currentPageResolver(function () use ($currentpage) {
				        return $currentpage;
				    });

	          		$rows = Product::where('sp_id',$user_id)->paginate(10);
	          		$i=0;
	          		foreach($rows as $row)
	          		{
	          			$rows[$i]['product_name'] = $row->product_name;
	          			$rows[$i]['product_image'] = changeImageUrlForFileExist(asset('sp_uploads/products/'.$row->product_image));
	          			$i++;
	          		}
	          		return response()->json(['status' => true, 'message' => 'Products List','data' => $rows]);		          		
		          		
		          	
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function deleteProduct(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'product_id' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {  	
		          	$row = Product::where('id',$request->product_id)->first();
		          	if($row)
		          	{
		          		$image_path = "public/sp_uploads/products/".$row->product_image;

		          		unlink($image_path);
			          
					    $row->delete();
					    return response()->json(['status' => true, 'message' => __('messages.Product deleted successfully.')]);	
						
		          	}else{
		          		return response()->json(['status' => false, 'message' => 'Products not found']);	
		          	}
		          	
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function editProduct(Request $request)
    {

    	if (Input::isMethod('post')) {
    		try{

    			$validator = Validator::make($request->all(), [
    				'product_id'=>'required',
                    'product_name'=>'required|max:50',
                    'product_image'=>'mimes:jpeg,jpg,png,gif',
                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{

	        		$row = Product::where('id',$request->product_id)->first();
	        		if($row){
	        			$row['product_name'] = $request->product_name;
		        		if ($request->hasFile('product_image') && $request->file('product_image'))
		        		{

		                    $file = $request->file('product_image');
		                    $name = time().'.'.$file->getClientOriginalExtension();
		                    $destinationPath = public_path('sp_uploads/products/');
		                    $img = $file->move($destinationPath, $name);

		                    $row->product_image = $name;
		                }

		                $row->save();
		        		if($row->save())
		        		{
		        			return response()->json(['status' => true, 'message' => __('messages.Product updated successfully.')]);
		        		}
	        		}else{
	        			return response()->json(['status' => false, 'message' => __('messages.Product not found')]);
	        		}
		        }
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
    	}
    }

    public function addStaff(Request $request)
    {
    	if (Input::isMethod('post')) {
    		try{

    			$validator = Validator::make($request->all(), [
                    'staff_name'=>'required|max:50',
                    'nationality'=>'exists:countries,id',
                    'experience'=>'required',
                    'speciality'=>'required',
                    'certificates'=>'required',
                    'staff_image'=>'mimes:jpeg,jpg,png,gif',
                    'is_provide_home_service'=>'required',
                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{

		        	$categories  = Category::where('category_name','!=','bridal')->where('parent_id','!=','0')->orderby('category_name', 'asc')->pluck('category_name', 'id')->toArray();
		        	$categories['0'] = 'All Rounders';
		        	
		        	$speciality_arr = array();
		        	$arr = explode(',',$request->speciality);
		        	foreach($arr as $cat_id)
					{
						if(array_key_exists($cat_id,$categories))
						{
							array_push($speciality_arr,$cat_id);
						}
						else
						{
							return response()->json(['status' => false, 'message' => $cat_id.' - Speciality Not Available.']);
						}
					}

		        	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	        		$row = new Staff();
	        		$row['sp_id'] = $user_id;
	        		$row['staff_name'] = $request->staff_name;
	        		$row['nationality'] = $request->nationality;
	        		$row['experience'] = $request->experience;
	        		$row['speciality'] = implode(',',$speciality_arr);
	        		$row['certificates'] = $request->certificates;
	        		$row['is_provide_home_service'] = $request->is_provide_home_service;
	        		$row['description'] = $request->description;
                    $row['description_ar'] = $request->description_ar;
                    $row['video_url'] = $request->video_url;
                    
	        		if ($request->hasFile('staff_image') && $request->file('staff_image'))
	        		{
	                    $file = $request->file('staff_image');
	                    $name = time().'.'.$file->getClientOriginalExtension();
	                    $destinationPath = public_path('sp_uploads/staff/');
	                    $img = $file->move($destinationPath, $name);

	                    $row->staff_image = $name;
	                }
	                
	                
	        		if($row->save())
	        		{
	        		 	$images= $videos = array();	
	        		 	if($request->hasFile('staff_images') && $request->file('staff_images'))
	                    {
			                $allowedfileExtension=['pdf','jpg','png'];
						    $files = $request->file('staff_images');
						    foreach($files as $key => $file) 
						    {      
						        $extension = $file->getClientOriginalExtension();
						        $check = in_array($extension,$allowedfileExtension);

						        if($check) {
	                                $getMimeType = $file->getClientMimeType();
	                                $name = time().str_random(2).$row->id.'.'.$file->getClientOriginalExtension();
	                                $destinationPath = public_path('/sp_uploads/staff/');
	                                $img = $file->move($destinationPath, $name);
	                                $rowImage = new StaffImagesVideos();
	                                $rowImage->staff_id = $row->id;
	                                $rowImage->staff_image = $name;
	                                $rowImage->content_type = substr($getMimeType, 0, 5);
	                                $rowImage->save();
	                                $file_url = asset('/public/sp_uploads/staff/'.$name);
	                                array_push($images,$file_url);
						        }
						        else
						        {
						        	return response()->json(['success'=>false,'message'=>'Invalid File Format'],422);
						        }
						    }
						}
                        
                        //upload video
                        if($request->hasFile('staff_videos') && $request->file('staff_videos'))
                        {
                        	$files = $request->file('staff_videos');
                            foreach($files as $key => $val)
                            {
                                if ($files[$key])
                                {
                                    $file = $files[$key];
                                    $getMimeType = $file->getClientMimeType();
                                    $name = time().str_random(2).$row->id.'.'.$file->getClientOriginalExtension();
                                    $destinationPath = public_path('/sp_uploads/staff/');
                                    $file = $file->move($destinationPath, $name);
                                    $file_url = asset('/public/sp_uploads/staff/'.$name);
                                    $rowImage = new StaffImagesVideos();
                                    $rowImage->staff_id = $row->id;
                                    $rowImage->staff_video = $name;
                                    $rowImage->content_type = substr($getMimeType, 0, 5);
                                    $rowImage->save();
                                    array_push($videos,$file_url);
                                }
                                else {
						            return response()->json(['success'=>false,'message'=>'Invalid File'],422);
						        }
                            }
                        }

	        			return response()->json(['status' => true, 'message' => 'Staff Added Successfully','images'=>$images, 'videos'=>$videos]);
	        		}
		            
		        }
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
    	}
    }

    public function getStaff(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'page' => 'required',
		              'status' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {
		          	$categories  = Category::where('category_name','!=','bridal')->where('parent_id','!=','0')->orderby('category_name', 'asc')->pluck('category_name', 'id')->toArray();

		          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
		          	$base_path = asset('sp_uploads/staff/').'/';
					$current_date = date('Y-m-d');

					if($request->status!=1){
						$rows = Staff::where('sp_id',$user_id)->where('is_deleted','!=','1')
		          		->with(array('getStaffLeave' => function($query) use ($current_date)
						{
						     $query->where('from_date','>=', $current_date);
						}))

						->select('staff.*','countries.nationality','countries.id as nationality_id',DB::raw('CONCAT("'.$base_path.'", staff_image) AS staff_image'))
						->leftJoin('countries', function($join) {
					      $join->on('staff.nationality', '=', 'countries.id');
					    })
		          		->paginate(10)->toArray();
					}else{
						$rows = Staff::where('sp_id',$user_id)->where('is_deleted','!=','1')
		          		->with(array('getStaffLeave' => function($query) use ($current_date)
						{
						     $query->where('from_date','>=', $current_date);
						}))
						->select('staff.*','countries.nationality','countries.id as nationality_id',DB::raw('CONCAT("'.$base_path.'", staff_image) AS staff_image'))
						->leftJoin('countries', function($join) {
					      $join->on('staff.nationality', '=', 'countries.id');
					    })
		          		->get()->toArray();
					}

					if($request->status==1)
						$data = $rows;
					else
						$data = $rows['data'];

					foreach($data as $key => $row)
					{
						$arr = $speciality_arr = $nationality_arr = $staff_images = $staff_videos = array();
						
						// get specility
						$arr = explode(",",$row['speciality']);
						foreach($arr as $cat_name_id)
						{
							if(array_key_exists($cat_name_id,$categories))
							{
								$arrr['id'] = (int)$cat_name_id;
								$arrr['name'] = $categories[$cat_name_id];
							}
							else
							{
								$arrr['id'] = (int)$cat_name_id;
								$arrr['name'] = ($cat_name_id == '0') ? 'All Rounders' : $cat_name_id;
							}

							$speciality_arr[] = $arrr;
						}

						// get nationality
						if($row['nationality_id'] != null)
						{
							$nationality_arr['id'] = $row['nationality_id'];
							$nationality_arr['nationality'] = $row['nationality'];
						}

						// get images videos
						$multipleImagesVedio = StaffImagesVideos::where('staff_id',$row['id'])->select('id','staff_image','staff_video')->get();

						foreach($multipleImagesVedio as $data)
						{
							if($data['staff_image'] !='' && $data['staff_image'] !=null)
								$staff_images[$data['id']] = asset('sp_uploads/staff/'.$data['staff_image']);

							if($data['staff_video'] !='' && $data['staff_video'] !=null)
								$staff_videos[$data['id']] = asset('sp_uploads/staff/'.$data['staff_video']);
						}

						if($request->status!=1)
						{
							$rows['data'][$key]['speciality']  = $speciality_arr;
							$rows['data'][$key]['nationality']  = $nationality_arr;
							$rows['data'][$key]['staff_images']  = $staff_images;
							$rows['data'][$key]['staff_videos']  = $staff_videos;
						}
						else
						{
							$rows[$key]['speciality']  = $speciality_arr;
							$rows[$key]['nationality']  = $nationality_arr;
							$rows[$key]['staff_images']  = $staff_images;
							$rows[$key]['staff_videos']  = $staff_videos;
						}
					}
	          		
	          		return response()->json(['status' => true, 'message' => 'Staff List','data' => $rows]);
		        }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function editStaff(Request $request)
    {
    	if (Input::isMethod('post')) {
    		try{

    			$validator = Validator::make($request->all(), [
    				'member_id'=>'required',
                    'staff_name'=>'required|max:50',
                    //'nationality'=>'required',
                    'experience'=>'required',
                    'speciality'=>'required',
                    'certificates'=>'required',
                    'staff_image'=>'mimes:jpeg,jpg,png,gif',
                    'is_provide_home_service'=>'required',
                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{

		        	$categories  = Category::where('category_name','!=','bridal')->where('parent_id','!=','0')->orderby('category_name', 'asc')->pluck('category_name', 'id')->toArray();
		        	$categories['0'] = 'All Rounders';
		        	
		        	$speciality_arr = array();
		        	$arr = explode(',',$request->speciality);
		        	foreach($arr as $cat_id)
					{
						if(array_key_exists($cat_id,$categories))
						{
							array_push($speciality_arr,$cat_id);
						}
						else
						{
							$speciality_arr['id'] = $cat_id;
							$speciality_arr['speciality'] = $cat_id;
						}
					}

	        		$row = Staff::where('id',$request->member_id)->first();
	        		$row['staff_name'] = $request->staff_name;
	        		$row['nationality'] = $request->nationality;
	        		$row['experience'] = $request->experience;
	        		$row['speciality'] = implode(',',$speciality_arr);
	        		$row['certificates'] = $request->certificates;
	        		$row['is_provide_home_service'] = $request->is_provide_home_service;
	        		$row['description'] = $request->description;
                    $row['description_ar'] = $request->description_ar;
                    $row['video_url'] = $request->video_url;

	        		if ($request->hasFile('staff_image') && $request->file('staff_image'))
	        		{

	                    $file = $request->file('staff_image');
	                    $name = time().'.'.$file->getClientOriginalExtension();
	                    $destinationPath = public_path('sp_uploads/staff/');
	                    $img = $file->move($destinationPath, $name);

	                    $row->staff_image = $name;
	                }

	        		if($row->save())
	        		{
	        			$images= $videos = array();	
	        		 	if($request->hasFile('staff_images') && $request->file('staff_images'))
	                    {
			                $allowedfileExtension=['pdf','jpg','png'];
						    $files = $request->file('staff_images');
						    foreach($files as $key => $file) 
						    {      
						        $extension = $file->getClientOriginalExtension();
						        $check = in_array($extension,$allowedfileExtension);

						        if($check) {
	                                $getMimeType = $file->getClientMimeType();
	                                $name = time().str_random(2).$row->id.'.'.$file->getClientOriginalExtension();
	                                $destinationPath = public_path('/sp_uploads/staff/');
	                                $img = $file->move($destinationPath, $name);
	                                $rowImage = new StaffImagesVideos();
	                                $rowImage->staff_id = $row->id;
	                                $rowImage->staff_image = $name;
	                                $rowImage->content_type = substr($getMimeType, 0, 5);
	                                $rowImage->save();
	                                $file_url = asset('/public/sp_uploads/staff/'.$name);
	                                array_push($images,$file_url);
						        }
						        else
						        {
						        	return response()->json(['success'=>false,'message'=>'Invalid File Format'],422);
						        }
						    }
						}
                        
                        //upload video
                        if($request->hasFile('staff_videos') && $request->file('staff_videos'))
                        {
                        	$files = $request->file('staff_videos');
                            foreach($files as $key => $val)
                            {
                                if ($files[$key])
                                {
                                    $file = $files[$key];
                                    $getMimeType = $file->getClientMimeType();
                                    $name = time().str_random(2).$row->id.'.'.$file->getClientOriginalExtension();
                                    $destinationPath = public_path('/sp_uploads/staff/');
                                    $file = $file->move($destinationPath, $name);
                                    $file_url = asset('/public/sp_uploads/staff/'.$name);
                                    $rowImage = new StaffImagesVideos();
                                    $rowImage->staff_id = $row->id;
                                    $rowImage->staff_video = $name;
                                    $rowImage->content_type = substr($getMimeType, 0, 5);
                                    $rowImage->save();
                                    array_push($videos,$file_url);
                                }
                                else {
						            return response()->json(['success'=>false,'message'=>'Invalid File'],422);
						        }
                            }
                        }

	        			return response()->json(['status' => true, 'message' => 'Staff Updated Successfully','images'=>$images, 'videos'=>$videos]);
	        		}
		            
		        }
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
    	}
    }

    public function deleteStaff(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'member_id' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {  	
		          	$row = Staff::where('id',$request->member_id)->first();
		          	if($row)
		          	{
		       //    		if($row->staff_image!='' && $row->staff_image!=null){
		       //    			$image_path = "public/sp_uploads/staff/".$row->staff_image;

		       //    			unlink($image_path);
		       //    		}
		          		
			          
					    // $row->delete();
					    $row->is_deleted = '1';
					    $row->save();
					    return response()->json(['status' => true, 'message' => 'Member deleted successfully']);	
						
		          	}else{
		          		return response()->json(['status' => false, 'message' => 'Member not found']);	
		          	}
		          	
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function deleteStaffImage(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'file_id' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {  	
		          	$row = StaffImagesVideos::where('id',$request->file_id)->first();
		          	if($row)
		          	{
		          		if($row->content_type == 'image')
		          		{
		          			$text = 'Images';
		          			$image_path = "public/sp_uploads/staff/".$row->staff_image;
		          		}
		          		else
		          		{
		          			$text = 'Videos';
		          			$image_path = "public/sp_uploads/staff/".$row->staff_video;
		          		}

		          		unlink($image_path);
			          
					    $row->delete();
					    return response()->json(['status' => true, 'message' => $text.' deleted successfully']);	
						
		          	}else{
		          		return response()->json(['status' => false, 'message' => 'File not found']);	
		          	}
		          	
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function staffLeave(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'staff_id' => 'required|exists:staff,id',
		              'from_date' => 'required',
		              'to_date' => 'required',
		              'from_time' => 'required',
		              'to_time' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 	
		          else 
		          {  
		          	// $check_booking = Booking::whereBetween('appointment_date', [$request->from_date, $request->to_date])->where('staff_id',$request->staff_id)->exists();

		          	$start_time = date("H:i:s", strtotime($request->from_time));
                
                    $end_time = date("H:i:s", strtotime($request->to_time));

		          	$check_booking = Booking::where(function ($query) use ($request,$start_time,$end_time) {
                        $query->where(function ($query) use ($request,$start_time, $end_time) {
                            $query
                                ->where('bookings.appointment_date',date('Y-m-d',strtotime($request->from_date)))
                                ->Where('bookings.appointment_time','>=',$start_time)
                                ->Where('bookings.service_end_time','<=',$end_time);
                        })
                        ->orWhere(function ($query) use ($request,$start_time, $end_time) {
                            $query
                                ->where('bookings.appointment_date',date('Y-m-d',strtotime($request->to_date)))
                                ->Where('bookings.appointment_time','>=',$start_time)
                                ->Where('bookings.service_end_time','<=',$end_time);
                        });
                        if(strtotime($request->from_date)!=strtotime($request->to_date)){
                           $query = $query->orWhere(function ($query) use ($request) {
                                $query
                                ->where('bookings.appointment_date','>',date('Y-m-d',strtotime($request->from_date)))
                                ->Where('bookings.appointment_date','<',date('Y-m-d',strtotime($request->to_date)));
                            });
                        }
                        

                    })
                    ->where('bookings.staff_id',$request->staff_id)->exists();


		          	if($check_booking){
						return response()->json(['status' => false, 'message' => __('messages.Staff has bookings on selected dates.')]);	
		          	}else{
		          		$srow = new StaffLeave();
		          		$srow['staff_id'] = $request->staff_id;
		          		$srow['from_date'] = date('Y-m-d',strtotime($request->from_date));
		          		$srow['to_date'] = date('Y-m-d',strtotime($request->to_date));
		          		$srow['from_time'] = date('H:i:s',strtotime($request->from_time));
		          		$srow['to_time'] = date('H:i:s',strtotime($request->to_time));
		          		
		          		if($srow->save())
		        		{
		        			return response()->json(['status' => true, 'message' => __('messages.Member Leave Added successfully')]);	
		        		}		
		          	}
	
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function editStaffLeave(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'id' => 'required',
		              'from_date' => 'required',
		              'to_date' => 'required',
		              'from_time' => 'required',
		              'to_time' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 	
		          else 
		          {  

		          	$srow = StaffLeave::where('id',$request->id)->first();
		          	
		          	if($srow){
		          		//$check_booking = Booking::whereBetween('appointment_date', [$request->from_date, $request->to_date])->where('staff_id',$srow->staff_id)->exists();
		          		$start_time = date("H:i:s", strtotime($request->from_time));
                    	$end_time = date("H:i:s", strtotime($request->to_time));

		          		$check_booking = Booking::where(function ($query) use ($request,$start_time,$end_time) {
                        $query->where(function ($query) use ($request,$start_time, $end_time) {
                            $query
                                ->where('bookings.appointment_date',date('Y-m-d',strtotime($request->from_date)))
                                ->Where('bookings.appointment_time','>=',$start_time)
                                ->Where('bookings.service_end_time','<=',$end_time);
                        })
                        ->orWhere(function ($query) use ($request,$start_time, $end_time) {
                            $query
                                ->where('bookings.appointment_date',date('Y-m-d',strtotime($request->to_date)))
                                ->Where('bookings.appointment_time','>=',$start_time)
                                ->Where('bookings.service_end_time','<=',$end_time);
                        });
                        if(strtotime($request->from_date)!=strtotime($request->to_date)){
                           $query = $query->orWhere(function ($query) use ($request) {
                                $query
                                ->where('bookings.appointment_date','>',date('Y-m-d',strtotime($request->from_date)))
                                ->Where('bookings.appointment_date','<',date('Y-m-d',strtotime($request->to_date)));
                            });
                        }
                        

                    })
                    ->where('bookings.staff_id',$request->staff_id)->exists();

			          	if($check_booking){
							return response()->json(['status' => false, 'message' => __('messages.Staff has bookings on selected dates.')]);	
			          	}else{

			          		$srow['from_date'] = date('Y-m-d',strtotime($request->from_date));
			          		$srow['to_date'] = date('Y-m-d',strtotime($request->to_date));
			          		$srow['from_time'] = date('H:i:s',strtotime($request->from_time));
			          		$srow['to_time'] = date('H:i:s',strtotime($request->to_time));
			          		

			          		if($srow->save())
			        		{
			        			return response()->json(['status' => true, 'message' => __('messages.Leave has been updated successfully.')]);	
			        		}		
			          		
			          	}

		          	}else{
							return response()->json(['status' => true, 'message' => __('messages.Leave not found.')]);	
		          		}
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function staffHomeService(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'is_home_service' => 'required',
		              'mov' => 'required',

		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 	
		          else 
		          {  

		          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          		$srow = SpUser::where('id',$user_id)->first();
	          		$srow['is_provide_home_service'] = (string)$request->is_home_service;

	          		if((string)$request->is_home_service!="0"){
	          			$srow['mov'] = $request->mov;
	          		}else{
	          			$srow['mov'] = '0.00';
	          		}
	          		

	          		if($srow->save())
	        		{
	        			if($srow->is_provide_home_service!= "1"){
	        				//update offers
							$offers = SpOffer::where('user_id',$user_id)->where('service_type','home')->get();

							foreach($offers as $off){
								$up_off = SpOffer::where('id',$off['id'])->first();
								$up_off->previous_status = $up_off->status;
								$up_off->status = '2';
								$up_off->save();

							}

							//update services
							$services = SpService::
							leftJoin('sp_service_prices', function($join) {
						      $join->on('sp_services.id', '=', 'sp_service_prices.sp_service_id');
						    })
						    ->where('sp_service_prices.field_key', 'like', '%_HOME%')
						    ->where('user_id',$user_id)
						    ->update(['sp_service_prices.field_key' => DB::raw('CONCAT( "IN_",sp_service_prices.field_key)')]);
						   // ->get()->toArray();
						    // print_r($services);die;
	        			}else{

	        				$offers = SpOffer::where('user_id',$user_id)->where('service_type','home')->where('status','2')->get();

							foreach($offers as $off){
								$up_off = SpOffer::where('id',$off['id'])->first();
								$up_off->status = $up_off->previous_status;
								$up_off->save();

							}

	        				$services = SpService::
							leftJoin('sp_service_prices', function($join) {
						      $join->on('sp_services.id', '=', 'sp_service_prices.sp_service_id');
						    })
						    ->where('sp_service_prices.field_key', 'like', '%_HOME%')
						    ->where('user_id',$user_id)
	        				->update(['sp_service_prices.field_key' => DB::raw('REPLACE(field_key,"IN_","")')]);
	        				// ->get()->toArray();
						    // print_r($services);die;
	        			}

	      //   			$services = SpService::
							// leftJoin('sp_service_prices', function($join) {
						 //      $join->on('sp_services.id', '=', 'sp_service_prices.sp_service_id');
						 //    })
						 //    ->where('sp_service_prices.field_key', 'like', '%_HOME%')
						 //    ->where('user_id',$user_id)
	      //   				->get()->toArray();
						 //    print_r($services);

						 //    $offers = SpOffer::where('user_id',$user_id)->where('service_type','home')->get()->toArray();print_r($offers);die;

	        			return response()->json(['status' => true, 'message' => __('messages.Home service Updated successfully')]);	
	        		}		    
	
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function serviceFromAdmin()
	{
		try
	    {
	    	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	    	$category_id = SpUser::where('id',$user_id)->select('category_id')->first();
			$cat_rows = Category::where('parent_id',$category_id['category_id'])->get();
    		$sr_arr = array();
			$final_arr = array();

			$cat_name = 'category_name'.$this->col_postfix;
			$sr_name = 'name'.$this->col_postfix;

	    	foreach($cat_rows as $row)
			{
				$row_arr = [
					'name'=>$row->$cat_name,
					'id' => $row->id
				];

				$sr=[];
				foreach($row->getServiceInfo as $srow){
					$check = SpService::where('user_id',$user_id)->where('service_id',$srow->id)->where('is_deleted','!=','1')->count();

					if($check == 0){
						
						$sr[] = [
							'id'=>$srow->id, 
							'name'=>$srow->$sr_name
						];
						
					}
				}
				if(!empty($sr)){
					$row_arr['service'] = $sr;
					$final_arr[] = $row_arr;
				}
				
			}
			
			return response()->json(['status' => true, 'message' => 'Service List','data'=>$final_arr]);
			
	    }
	    catch(\Exception $e){
	       	return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	    }
	}

	public function addServices(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'service_id' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 	
		          else 
		          {  
		         	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
		         	
		         	$row = SpService::where('service_id',$request->service_id)->where('user_id',$user_id)->where('is_deleted','1')->first();

		         	if(!$row){
		         		$row = new SpService();
		         	}
	         		

	          		$row['service_id'] = $request->service_id;
	          		$row['user_id'] = $user_id;
	          		$row['is_deleted'] = '0';

	          		if($row->save()){
	          			
	          			
	          			$keyarr = [
	          				'isMen'=>[
	          					'MEN_ORIGINALPRICE'=>'men_originalprice',
	          					'MEN_DISCOUNT'=>'men_discount',
	          					'MEN_BESTPRICE'=>'men_bestprice'
	          				],
	          				'isWomen'=>[
	          					'WOMEN_ORIGINALPRICE'=>'women_originalprice',
	          					'WOMEN_DISCOUNT'=>'women_discount',
	          					'WOMEN_BESTPRICE'=>'women_bestprice'
	          				],
	          				'isKids'=>[
	          					'KIDS_ORIGINALPRICE'=>'kid_originalprice',
	          					'KIDS_DISCOUNT'=>'kid_discount',
	          					'KIDS_BESTPRICE'=>'kid_bestprice'
	          				],
	          				'isMen_home_service'=>[
	          					'MEN_HOME_ORIGINALPRICE'=>'men_home_originalprice',
	          					'MEN_HOME_DISCOUNT'=>'men_home_discount',
	          					'MEN_HOME_BESTPRICE'=>'men_home_bestprice'
	          				],
	          				'isWomen_home_service'=>[
	          					'WOMEN_HOME_ORIGINALPRICE'=>'women_home_originalprice',
	          					'WOMEN_HOME_DISCOUNT'=>'women_home_discount',
	          					'WOMEN_HOME_BESTPRICE'=>'women_home_bestprice'
	          				],
	          				'isKids_home_service'=>[
	          					'KIDS_HOME_ORIGINALPRICE'=>'kids_home_originalprice',
	          					'KIDS_HOME_DISCOUNT'=>'kids_home_discount',
	          					'KIDS_HOME_BESTPRICE'=>'kids_home_bestprice'
	          				]
	          			];
	          			$entity_arr = $data_for_save = [];
	          			foreach($keyarr as $key => $key_vals){
	          				if($request->$key){

	          					foreach($key_vals as $key_key=>$key_row){
	          						$entity = SpServicePrice::where('sp_service_id',$row->id)->count();
	          						if(count($entity)>0){
										SpServicePrice::where('sp_service_id',$row->id)->delete();
	          						}
	          						$entity = new SpServicePrice();
	          						$entity->sp_service_id = $row->id;
	          						$entity->field_key = $key_key;
	          						$entity->price = $request->$key_row;
	          						$data_for_save[] = $entity;
	          					}
	          				}
	          			}
	          			$row->getRelatedPrices()->saveMany($data_for_save);
	          			return response()->json(['status' => true, 'message' => __('messages.Service Added successfully')]);
	          			
	          		}
	         		
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function EditServices(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'sp_service_id' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 	
		          else 
		          {  

						$row = SpService::where('id',$request->sp_service_id)->first();
						$offers = array();
						foreach($row->getAssociatedOffers as $key_price => $offer_row){
					
							$offers[] = $offer_row['sp_offer_id'];
						}

						$check_offer = [
							'isMen'=> ['men','shop'],
							'isWomen'=> ['women','shop'],
							'isKids'=> ['kids','shop'],
							'isMen_home_service'=> ['men','home'],
							'isWomen_home_service'=> ['women','home'],
							'isKids_home_service'=> ['kids','home'],
						];
						$sim_key_arr = ['isMen','isWomen','isKids','isMen_home_service','isWomen_home_service','isKids_home_service'];
	          			$keyarr = [
	          				'isMen'=>[
	          					'MEN_ORIGINALPRICE'=>'men_originalprice',
	          					'MEN_DISCOUNT'=>'men_discount',
	          					'MEN_BESTPRICE'=>'men_bestprice'
	          				],
	          				'isWomen'=>[
	          					'WOMEN_ORIGINALPRICE'=>'women_originalprice',
	          					'WOMEN_DISCOUNT'=>'women_discount',
	          					'WOMEN_BESTPRICE'=>'women_bestprice'
	          				],
	          				'isKids'=>[
	          					'KIDS_ORIGINALPRICE'=>'kid_originalprice',
	          					'KIDS_DISCOUNT'=>'kid_discount',
	          					'KIDS_BESTPRICE'=>'kid_bestprice'
	          				],
	          				'isMen_home_service'=>[
	          					'MEN_HOME_ORIGINALPRICE'=>'men_home_originalprice',
	          					'MEN_HOME_DISCOUNT'=>'men_home_discount',
	          					'MEN_HOME_BESTPRICE'=>'men_home_bestprice'
	          				],
	          				'isWomen_home_service'=>[
	          					'WOMEN_HOME_ORIGINALPRICE'=>'women_home_originalprice',
	          					'WOMEN_HOME_DISCOUNT'=>'women_home_discount',
	          					'WOMEN_HOME_BESTPRICE'=>'women_home_bestprice'
	          				],
	          				'isKids_home_service'=>[
	          					'KIDS_HOME_ORIGINALPRICE'=>'kids_home_originalprice',
	          					'KIDS_HOME_DISCOUNT'=>'kids_home_discount',
	          					'KIDS_HOME_BESTPRICE'=>'kids_home_bestprice'
	          				]
	          			];
	          			SpServicePrice::where('sp_service_id',$request->sp_service_id)->delete();
	          			$get_offer_keys = array();
	          			$entity_arr = [];
	          			foreach($keyarr as $key => $key_vals){
	          				if($request->$key){
	          					$get_offer_keys[] = $key;
	          					foreach($key_vals as $key_key=>$key_row){

	          						$entity = new SpServicePrice();
	          						$entity->sp_service_id = $row->id;
	          						$entity->field_key = $key_key;
	          						$entity->price = $request->$key_row;
	          						$data_for_save[] = $entity;
	          					}
	          				}
	          			}
	          			$row->getRelatedPrices()->saveMany($data_for_save);

	          			//check offer
	          			$k_arr = array_diff($sim_key_arr,$get_offer_keys);
	          			
	          			foreach($k_arr as $ky){
	          				if(count($offers)>0){
	      						foreach($offers as $off){
	      							$gender = $check_offer[$ky][0];
	      							$service_type = $check_offer[$ky][1];
	      							$off_row = SpOffer::where('id',$off)->where('gender',$gender)->where('service_type',$service_type)->first();
	      							if($off_row){
	      								$off_row->is_deleted = '1';
	      								$off_row->save();
	      							}

	      						}
	      						
	      					}
	          			}
	          			
	          			return response()->json(['status' => true, 'message' => __('messages.Service Updated successfully.')]);
	          			
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage().$e->getLine(), 'data' => []]);
	       }
		}
	}

	public function getServices()
	{
		try
	    {
	    	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
			$rows = SpService::where('user_id',$user_id)
			->where('is_deleted','!=','1')
			//->with('getAssociatedOffers.getAssociatedOffer')
			// ->whereHas('getAssociatedOffers.getAssociatedOffer', function($query) {
		 //        $query->where('is_deleted','!=','1');
		 //    })
			->get();
			$cat_service_arr = $sub_cat_arr = [];

			$cat_name = 'category_name'.$this->col_postfix;
			$sr_name = 'name'.$this->col_postfix;

	    	foreach($rows as $row) {

	    		$service_row = $row->getAssociatedService;
				$service_category_row = $service_row->getSubcat;
				
				$sub_cat_arr[$service_category_row->id] = [
					'id'=> $service_category_row->id,
					'name'=> $service_category_row->$cat_name
				];

				$service_current = [
					'id'=> $service_row->id,
					'name'=> $service_row->$sr_name,
				];

				foreach($row->getRelatedPrices as $key_price => $price_row){
					
					$service_current['sp_service_id'] = $price_row['sp_service_id'];
					$service_current[$price_row['field_key']] = $price_row['price'];
				}
				$service_current['offers'] = array();
				// $associated_offers = $row->getAssociatedOffers->where('is_deleted','!=','1');
				foreach($row->getAssociatedOffers as $key_price => $offer_row){
					
					// $service_current['offers']['offer_ids'] = $offer_row['sp_offer_id'];
					// $service_current['offers']['gender'] = $offer_row['gender'];

					$service_current['offers'][] = [
						'id' => $offer_row['sp_offer_id'],
						'gender' => $offer_row['gender'],
						'service_type' => $offer_row['service_type'],
					];
				}
				
				$cat_service_arr[$service_category_row->id][] = $service_current;
				

			}
			$result_arr = [];
			foreach($sub_cat_arr as $cat_id => $cat_row){
				$row_current = $cat_row;
				$cat_services = isset($cat_service_arr[$cat_id]) ? $cat_service_arr[$cat_id] : [];
				$row_current['service'] = $cat_services;

				$result_arr[] = $row_current;
			}



			return response()->json(['status' => true, 'message' => 'Service List','data'=>$result_arr]);
			
	    }
	    catch(\Exception $e){
	       	return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	    }
	}


	public function deleteServices(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'sp_service_id' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {  	
		          	$row = SpService::where('id',$request->sp_service_id)->first();

		          	$offer_arr = array();
		          	foreach($row->getAssociatedOffers as $offer_row){	
						$offer_arr[] = $offer_row['sp_offer_id'];
					}
					
		          	if($row)
		          	{
					    $row->is_deleted = '1';
					    $row->save();
		
					    SpOffer::whereIn('id',$offer_arr)->update(array('is_deleted'=>'1'));
					    return response()->json(['status' => true, 'message' => __('messages.Service deleted successfully')]);	
						
		          	}else{
		          		return response()->json(['status' => false, 'message' => __('messages.Service not found')]);	
		          	}
		          	
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function getMainOffers()
	{
		try
	    {
	    	$rows = Offer::get();
	    	$offers = array();
	    	if($rows){
	    		$col = 'title'.$this->col_postfix;
		    	foreach($rows as $row)
				{
					$offers[] = [
						'id'=>$row->id,
						'title'=>$row->$col,
						];
				}
				return response()->json(['status' => true, 'message' => 'Offer List','data'=>$offers]);
			}else{
				return response()->json(['status' => true, 'message' => 'No Record Found','data'=>[]]);
			}
	    }
	    catch(\Exception $e){
	       	return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	    }
	}

	public function OfferServiceSuggestion(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'gender' => 'required',
		              'service_type' => 'required',
		              'keyword' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {  
		            $user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
		            $selected_service_arr = explode(',',$request->selected_service_id);	
		          	$key_arr = [
		          		'men' => [
		          			'shop' => ['MEN_ORIGINALPRICE','MEN_DISCOUNT','MEN_BESTPRICE'],
		          			'home' => ['MEN_HOME_ORIGINALPRICE','MEN_HOME_DISCOUNT','MEN_HOME_BESTPRICE'],
		          		],
		          		'women' => [
		          			'shop' => ['WOMEN_ORIGINALPRICE','WOMEN_DISCOUNT','WOMEN_BESTPRICE'],
		          			'home' => ['WOMEN_HOME_ORIGINALPRICE','WOMEN_HOME_DISCOUNT','WOMEN_HOME_BESTPRICE'],
		          		],
		          		'kids' => [
		          			'shop' => ['KIDS_ORIGINALPRICE','KIDS_DISCOUNT','KIDS_BESTPRICE'],
		          			'home' => ['KIDS_HOME_ORIGINALPRICE','KIDS_HOME_DISCOUNT','KIDS_HOME_BESTPRICE'],
		          		],
		          		
		          	];
		          	$sr_name = 'name'.$this->col_postfix;
		          	$services = SpService::
		          	select('sp_services.*')
				    ->leftjoin('services', 'services.id', '=', 'sp_services.service_id')
				    ->leftjoin('sp_service_prices', 'sp_services.id', '=', 'sp_service_prices.sp_service_id')
				    ->where('sp_services.user_id', '=', $user_id)
				    ->where('services.name', 'LIKE', "%{$request->keyword}%")
				    ->whereNotIn('sp_services.id',$selected_service_arr )
				    ->where('sp_services.is_deleted','!=','1')
				    ->groupBy('sp_services.id')
				    ->get();
				  
				  	$service_row = array();
				    foreach($services as $key => $row){
			
				    	$service_row_current = array();
				    	foreach($row->getRelatedPrices as $pricerow){

				    		if(in_array($pricerow['field_key'],$key_arr[$request->gender][$request->service_type] ))
				    		{
				    			$changecase = strtoupper($request->gender).'_';
				    			$replace = str_replace($changecase, '', $pricerow['field_key']);
				    			$service_row_current['sp_service_id'] = $row['id'];
				    			$service_row_current['name'] = $row->getAssociatedService->$sr_name;
				    			$service_row_current[$replace] = $pricerow['price'];
				    				
				    		}
				    	}
				    	if(!empty($service_row_current)){
				    		$service_row[] = $service_row_current;
				    	}
				    	
					  
				    }//outer loop end
				    return response()->json(['status' => true, 'message' => 'Service Suggestion','data'=>$service_row]);         	
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function addOffers(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'offer_id' => 'required',
		              'spectacular_offer' => 'required',
		              'offer_details' => 'required',
		              'gender' => 'required|in:men,women,kids',
		              'service_type' => 'required|in:home,shop',
		              'total_price' => 'required',
		              'discount' => 'required',
		              'best_price' => 'required',
		              'offer_expire_date' => 'required',
		              'offer_terms' => 'required',
		              'services' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {  

		          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;	
		          	$row = new SpOffer();
		        
		 			$row->user_id =$user_id;
		 			$row->offer_id =$request->offer_id;
		          	$row->spectacular_offer = $request->spectacular_offer;
		          	$row->offer_details = $request->offer_details;
		          	$row->gender = $request->gender;
		          	$row->service_type = $request->service_type;
		          	$row->total_price = $request->total_price;
		          	$row->discount = $request->discount;
		          	$row->best_price = $request->best_price;
		          	$row->expiry_date = date('Y-m-d',strtotime($request->offer_expire_date));
		          	$row->offer_terms = $request->offer_terms;

		          	if($row->save())
	        		{
	        			$service_arr = json_decode($request->services,true);
	        			
	        			foreach($service_arr as $sr_row){
	        				$service_row = new SpOfferService();
	        				$service_row->sp_offer_id = $row->id;
	        				$service_row->sp_service_id = $sr_row['service_id'];
	        				$service_row->quantity = $sr_row['quantity'];
	        				$service_row->save();
	        			}

	        			//mail to admin
	        			
							$data1['templete'] = "admin_mail";
							$data1['email'] = env("CONTACT_US_EMAIL");
							$data1['subject'] = "New Offer Approval";
							$data1['message'] = $row->getAssociatedOfferProviderName->store_name." has created a new offer. Kindly review and approve if necessary. ";
							send($data1);

	        			return response()->json(['status' => true, 'message' => __('messages.Offer has been sent successfully to admin for approval.')]);
	        		}        	
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function getOffers(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'page_no' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {  	
		          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          		$currentpage = $request->page_no;
	          		Paginator::currentPageResolver(function () use ($currentpage) {
				        return $currentpage;
				    });

				    $key_arr = [
		          		'men' => [
		          			'shop' => ['MEN_ORIGINALPRICE','MEN_DISCOUNT','MEN_BESTPRICE'],
		          			'home' => ['MEN_HOME_ORIGINALPRICE','MEN_HOME_DISCOUNT','MEN_HOME_BESTPRICE','IN_MEN_HOME_ORIGINALPRICE','IN_MEN_HOME_DISCOUNT','IN_MEN_HOME_BESTPRICE'],
		          		],
		          		'women' => [
		          			'shop' => ['WOMEN_ORIGINALPRICE','WOMEN_DISCOUNT','WOMEN_BESTPRICE'],
		          			'home' => ['WOMEN_HOME_ORIGINALPRICE','WOMEN_HOME_DISCOUNT','WOMEN_HOME_BESTPRICE','IN_WOMEN_HOME_ORIGINALPRICE','IN_WOMEN_HOME_DISCOUNT','IN_WOMEN_HOME_BESTPRICE'],
		          		],
		          		'kids' => [
		          			'shop' => ['KIDS_ORIGINALPRICE','KIDS_DISCOUNT','KIDS_BESTPRICE'],
		          			'home' => ['KIDS_HOME_ORIGINALPRICE','KIDS_HOME_DISCOUNT','KIDS_HOME_BESTPRICE','IN_KIDS_HOME_ORIGINALPRICE','IN_KIDS_HOME_DISCOUNT','IN_KIDS_HOME_BESTPRICE'],
		          		],
		          		
		          	];

				   $rows = SpOffer::where('user_id',$user_id)->where('is_deleted','!=','1')->orderBy('id','DESC')->paginate(10);

				    $rows_arr = $rows->toArray();
	    			$offers = array();
	    			$sr_name = 'name'.$this->col_postfix;
	    			$offer_title = 'title'.$this->col_postfix;
	    			if($rows){
		    			foreach($rows as $row)
						{
							$offers_current = [
								'id'=>$row->id,
								'user_id'=>$row->user_id,
								'offer_id'=>$row->offer_id,
								'offer_name'=>$row->getAssociatedOfferName->$offer_title,
								'spectacular_offer'=>$row->spectacular_offer,
								'offer_details'=>$row->offer_details,
								'gender'=>$row->gender,
								'service_type'=>$row->service_type,
								'total_price'=>$row->total_price,
								'discount'=>$row->discount,
								// 'best_price'=>$row->best_price,
								'expiry_date'=>$row->expiry_date,
								'offer_terms'=>$row->offer_terms,
								'likes'=>count($row->getAssociatedOfferLikes),
								'status'=>$row->status,
								'is_nominated'=>$row->is_nominated,
								];
								$offers_service = array();
								$total_bestprice = 0;
								foreach($row->getAssociatedOfferServices as $srow){

									$offers_service1 = [
										'service_id' => $srow->sp_service_id,
										'quantity' => $srow->quantity,
										'name' => $srow->getAssociatedOfferServicesName->getAssociatedService->$sr_name,
										
									];

							
									foreach($srow->getAssociatedOfferServicesPrices as $pricerow){

							    		if(in_array($pricerow['field_key'],$key_arr[$row->gender][$row->service_type] ))
							    		{

							    			if (strpos($pricerow['field_key'], 'IN_')!== false) { 
							    		
							    				$changecase = 'IN_'.strtoupper($row->gender).'_';
							    			}else{
							    				
							    				$changecase = strtoupper($row->gender).'_';
							    			}
							    			// echo $changecase = strtoupper('IN_'.$row->gender).'_';die;
							    			$replace = str_replace($changecase, '', $pricerow['field_key']);
							    			
							    			$offers_service1[$replace] = $pricerow['price'];

							    				
							    		}
							    	}
							    	// print_r($offers_service1);die;
							    	// $offers_service1['price'] = $price_row_current;
							    	// print_r($offers_service1);die;
							    	$original = '';
							    	if(isset($offers_service1['ORIGINALPRICE'])){
							    		$original = 'ORIGINALPRICE';
							    	}
							    	else{
							    		$original = 'HOME_ORIGINALPRICE';
							    	}
							    	$total_bestprice = $total_bestprice+($offers_service1[$original]*$srow->quantity);
							    	$offers_service[] = $offers_service1;
								}

								$cal_bestprice =  $total_bestprice - (($total_bestprice*$row->discount)/100);
								$offers_current['best_price'] = number_format((float)$cal_bestprice, 4, '.', '');

								$offers_current['services'] = $offers_service;
								$offers[] = $offers_current;
						}


						$result['data'] = $offers;
		                $result['current_page']     = $rows_arr['current_page'];
		                $result['from']             = $rows_arr['from'];
		                $result['last_page']        = $rows_arr['last_page'];
		                $result['next_page_url']    = $rows_arr['next_page_url'];
		                $result['per_page']         = $rows_arr['per_page'];
		                $result['prev_page_url']    = $rows_arr['prev_page_url'];
		                $result['to']               = $rows_arr['to'];
		                $result['total']            = $rows_arr['total'];
		          		return response()->json(['status' => true, 'message' => 'Offer Listing','data' => $result]);
						
					}else{
						return response()->json(['status' => true, 'message' => 'No Record Found','data'=>[]]);
					}
		          	
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage().$e->getLine(), 'data' => []]);
	       }
		}
	}

	public function getOfferDetails(Request $request)
	{
		if (Input::isMethod('post')) {

    		try{
    			$validator = Validator::make($request->all(), [

                    'language' => 'required',
                    'offer_id'=>'required',

                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{

		        	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
		        	$row = SpOffer::where('id',$request->offer_id)->first();
		        	$key_arr = [
		          		'men' => [
		          			'shop' => ['MEN_ORIGINALPRICE','MEN_DISCOUNT','MEN_BESTPRICE'],
		          			'home' => ['MEN_HOME_ORIGINALPRICE','MEN_HOME_DISCOUNT','MEN_HOME_BESTPRICE','IN_MEN_HOME_ORIGINALPRICE','IN_MEN_HOME_DISCOUNT','IN_MEN_HOME_BESTPRICE'],
		          		],
		          		'women' => [
		          			'shop' => ['WOMEN_ORIGINALPRICE','WOMEN_DISCOUNT','WOMEN_BESTPRICE'],
		          			'home' => ['WOMEN_HOME_ORIGINALPRICE','WOMEN_HOME_DISCOUNT','WOMEN_HOME_BESTPRICE','IN_WOMEN_HOME_ORIGINALPRICE','IN_WOMEN_HOME_DISCOUNT','IN_WOMEN_HOME_BESTPRICE'],
		          		],
		          		'kids' => [
		          			'shop' => ['KIDS_ORIGINALPRICE','KIDS_DISCOUNT','KIDS_BESTPRICE'],
		          			'home' => ['KIDS_HOME_ORIGINALPRICE','KIDS_HOME_DISCOUNT','KIDS_HOME_BESTPRICE','IN_KIDS_HOME_ORIGINALPRICE','IN_KIDS_HOME_DISCOUNT','IN_KIDS_HOME_BESTPRICE'],
		          		],
		          		
		          	];
		          	$sr_name = 'name'.$this->col_postfix;
		          	$offer_title = 'title'.$this->col_postfix;
		        	$offers_current = [
						'id'=>$row->id,
						'user_id'=>$row->user_id,
						'offer_id'=>$row->offer_id,
						'offer_name'=>$row->getAssociatedOfferName->$offer_title,
						'spectacular_offer'=>$row->spectacular_offer,
						'offer_details'=>$row->offer_details,
						'gender'=>$row->gender,
						'service_type'=>$row->service_type,
						'total_price'=>$row->total_price,
						'discount'=>$row->discount,
						// 'best_price'=>$row->best_price,
						'expiry_date'=>$row->expiry_date,
						'offer_terms'=>$row->offer_terms,
						'likes'=>count($row->getAssociatedOfferLikes),
						'status'=>$row->status,
						'is_nominated'=>$row->is_nominated,
						];
						$offers_service = array();
						$total_bestprice = 0;
						foreach($row->getAssociatedOfferServices as $srow){

							$offers_service1 = [
								'service_id' => $srow->sp_service_id,
								'quantity' => $srow->quantity,
								'name' => $srow->getAssociatedOfferServicesName->getAssociatedService->$sr_name,
								
							];

					
							foreach($srow->getAssociatedOfferServicesPrices as $pricerow){

					    		if(in_array($pricerow['field_key'],$key_arr[$row->gender][$row->service_type] ))
					    		{

					    			if (strpos($pricerow['field_key'], 'IN_')!== false) { 
					    		
					    				$changecase = 'IN_'.strtoupper($row->gender).'_';
					    			}else{
					    				
					    				$changecase = strtoupper($row->gender).'_';
					    			}
					    			// echo $changecase = strtoupper('IN_'.$row->gender).'_';die;
					    			$replace = str_replace($changecase, '', $pricerow['field_key']);
					    			
					    			$offers_service1[$replace] = $pricerow['price'];

					    				
					    		}
					    	}

					    	// $offers_service1['price'] = $price_row_current;
					    	// print_r($offers_service1);die;
					    	$original = '';
					    	if(isset($offers_service1['ORIGINALPRICE'])){
					    		$original = 'ORIGINALPRICE';
					    	}
					    	else{
					    		$original = 'HOME_ORIGINALPRICE';
							}
					    	$total_bestprice = $total_bestprice+($offers_service1[$original]*$srow->quantity);
					    	$offers_service[] = $offers_service1;
						}

						$cal_bestprice =  $total_bestprice - (($total_bestprice*$row->discount)/100);
						$offers_current['best_price'] = number_format((float)$cal_bestprice, 4, '.', '');

						$offers_current['services'] = $offers_service;
			    	

		    		return response()->json(['status' => true, 'message' => 'Offer Detail', 'data' => $offers_current]);
		        	
	        	}	        	
		       
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage().$e->getLine(), 'data' => []]);
	       }
    	}
	}


	public function deleteOffer(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'offer_id' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {  	
		          	$row = SpOffer::where('id',$request->offer_id)->first();
		          	if($row)
		          	{
					    // $row->delete();
					    $row->is_deleted = '1';
					    $row->save();
					    return response()->json(['status' => true, 'message' => __('messages.Offer deleted successfully.')]);	
						
		          	}else{
		          		return response()->json(['status' => false, 'message' => __('messages.Offer not found')]);	
		          	}
		          	
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function editOffers(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'sp_offer_id' => 'required',
		              'offer_id' => 'required',
		              'spectacular_offer' => 'required',
		              'offer_details' => 'required',
		              'gender' => 'required|in:men,women,kids',
		              'service_type' => 'required|in:home,shop',
		              'total_price' => 'required',
		              'discount' => 'required',
		              'best_price' => 'required',
		              'offer_expire_date' => 'required',
		              'offer_terms' => 'required',
		              'services' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {  
		          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;	
		          	$row = SpOffer::where('id',$request->sp_offer_id)->first();
		        	// echo "<pre/>";print_r($row);die;
		 			$row->user_id =$user_id;
		 			$row->offer_id =$request->offer_id;
		          	$row->spectacular_offer = $request->spectacular_offer;
		          	$row->offer_details = $request->offer_details;
		          	$row->gender = $request->gender;
		          	$row->service_type = $request->service_type;
		          	$row->total_price = $request->total_price;
		          	$row->discount = $request->discount;
		          	$row->best_price = $request->best_price;
		          	$row->expiry_date = date('Y-m-d',strtotime($request->offer_expire_date));
		          	$row->offer_terms = $request->offer_terms;

		          	if($row->save())
	        		{
	        			SpOfferService::where('sp_offer_id',$request->sp_offer_id)->delete();
	        			$service_arr = json_decode($request->services,true);
	        			
	        			foreach($service_arr as $sr_row){
	        				$service_row = new SpOfferService();
	        				$service_row->sp_offer_id = $row->id;
	        				$service_row->sp_service_id = $sr_row['service_id'];
	        				$service_row->quantity = $sr_row['quantity'];
	        				$service_row->save();
	        			}
	        			return response()->json(['status' => true, 'message' => __('messages.Offer updated successfully.')]);
	        		}        	
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function getRepliedForum(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'language' => 'required',
		              'page_no' => 'required',
		              'user_type' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {  

		          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
		          	$user = SpUser::where('id',$user_id)->select('category_id')->first();
		
	          		$category_query = ForumCategory::where('category_id',$user['category_id'])->get()->toArray();
	          		
	          		$forum_arr = array();
	          		foreach($category_query as $cat_rows){
						$forum_arr[] = $cat_rows['forum_id'];
	          		}

		          	$currentpage = $request->page_no;
	          		Paginator::currentPageResolver(function () use ($currentpage) {
				        return $currentpage;
				    });

		            $row = Forum::where('status','1')->whereIn('id',$forum_arr)->orderBy('id','DESC')->paginate(10);

		            $rows_arr = $row->toArray();
		           
		          	
		          	$final_data = array();
		          	foreach($row as $rows)
		          	{
		          		$is_commented = 0;
						if($rows->getAssociatedForumComments){
							 foreach($rows->getAssociatedForumComments as $comments){
							 	if($request->user_type == 'serviceprovider')
							 	{
							 		if($comments['sp_id'] == $user_id){
							 			$is_commented = 1;
							 		}
							 		
							 	}
			 				 }
						}

						if($is_commented == 1){
								$data_1 = [
			          			'id' => $rows->id,
			          			'customer_id' => $rows->user_id,
			          			'customer_name' => $rows->getAssociatedUserInformation->name,
			          			'is_open_forum' => $rows->is_open_forum,
			          			'subject' => $rows->subject,
			          			'message' => $rows->message,
			          			'isNotify' => $rows->isNotify,
			          			'created_at' => date('d-M-Y H:i:s',strtotime($rows->created_at)),
			          		];

			          		$data_2 = array();
			          		foreach($rows->getAssociatedForumBannerImages as $img)
			          		{
			          			$data_2[] = [
			          				'id' => $img->id,
			          				'banner_path' => asset('sp_uploads/forum/'.$img->banner_path),
			          			];
			          		}
			          		
			 
							$data_1['total_comments'] = count($rows->getAssociatedForumComments);
			          	
			          		$data_1['banner_images'] = $data_2;
			          		$final_data[] = $data_1;
						}
		          		

		          	}//end foreach

		          	$result['data'] = $final_data;
	                $result['current_page']     = $rows_arr['current_page'];
	                $result['from']             = $rows_arr['from'];
	                $result['last_page']        = $rows_arr['last_page'];
	                $result['next_page_url']    = $rows_arr['next_page_url'];
	                $result['per_page']         = $rows_arr['per_page'];
	                $result['prev_page_url']    = $rows_arr['prev_page_url'];
	                $result['to']               = $rows_arr['to'];
	                $result['total']            = $rows_arr['total'];

		          	if($row)
		          	{
		          		
					    return response()->json(['status' => true, 'message' => 'Forum','data' => $result]);	
						
		          	}else{
		          		return response()->json(['status' => false, 'message' => 'Forum not found']);	
		          	}
		          	
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function getNoRepliedForum(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'language' => 'required',
		              'page_no' => 'required',
		              'user_type' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {  

		          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
		          	$user = SpUser::where('id',$user_id)->select('category_id')->first();
		
	          		$category_query = ForumCategory::where('category_id',$user['category_id'])->get();
	          		$forum_arr = array();
	          		foreach($category_query as $cat_rows){
						$forum_arr[] = $cat_rows['forum_id'];
	          		}

		          	$currentpage = $request->page_no;
	          		Paginator::currentPageResolver(function () use ($currentpage) {
				        return $currentpage;
				    });
		            $row = Forum::where('status','1')->whereIn('id',$forum_arr)->orderBy('id','DESC')->paginate(10);

		            $rows_arr = $row->toArray();
		          	
		          	$final_data = array();
		          	foreach($row as $rows)
		          	{
		          		$is_commented = 0;
						if($rows->getAssociatedForumComments){
							 foreach($rows->getAssociatedForumComments as $comments){
							 	if($request->user_type == 'serviceprovider')
							 	{
							 		if($comments['sp_id'] == $user_id){
							 			$is_commented = 1;
							 		}
							 		
							 	}
			 				 }
						}

						if($is_commented!= 1){
								$data_1 = [
			          			'id' => $rows->id,
			          			'customer_id' => $rows->user_id,
			          			'customer_name' => $rows->getAssociatedUserInformation->name,
			          			'is_open_forum' => $rows->is_open_forum,
			          			'subject' => $rows->subject,
			          			'message' => $rows->message,
			          			'isNotify' => $rows->isNotify,
			          			'created_at' => date('d-M-Y H:i:s',strtotime($rows->created_at)),
			          		];

			          		$data_2 = array();
			          		foreach($rows->getAssociatedForumBannerImages as $img)
			          		{
			          			$data_2[] = [
			          				'id' => $img->id,
			          				'banner_path' => asset('sp_uploads/forum/'.$img->banner_path),
			          			];
			          		}
			          		
			 
							$data_1['total_comments'] = count($rows->getAssociatedForumComments);
			          	
			          		$data_1['banner_images'] = $data_2;
			          		$final_data[] = $data_1;
						}
		          		

		          	}//end foreach

		          	$result['data'] = $final_data;
	                $result['current_page']     = $rows_arr['current_page'];
	                $result['from']             = $rows_arr['from'];
	                $result['last_page']        = $rows_arr['last_page'];
	                $result['next_page_url']    = $rows_arr['next_page_url'];
	                $result['per_page']         = $rows_arr['per_page'];
	                $result['prev_page_url']    = $rows_arr['prev_page_url'];
	                $result['to']               = $rows_arr['to'];
	                $result['total']            = $rows_arr['total'];

		          	if($row)
		          	{
		          		
					    return response()->json(['status' => true, 'message' => 'Forum','data' => $result]);	
						
		          	}else{
		          		return response()->json(['status' => false, 'message' => 'Forum not found']);	
		          	}
		          	
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function addForumReply(Request $request)
	{
		if (Input::isMethod('post')) {
    		try{
    			$validator = Validator::make($request->all(), [
                    'forum_id'=>'required',
                    'message'=>'required',
                    'user_type' => 'required',
                    'language' => 'required',
                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{
		        	
		        	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
		        	$check_isNotify = Forum::where('id',$request->forum_id)->first();

		        	$user = User::where('id',$check_isNotify->user_id)->first();
		        	
	        		$row = new ForumComment();

	        		$row['forum_id'] = $request->forum_id;
	        		$row['message'] = $request->message;

	        		if($request->user_type == 'customer'){

	        			$row['user_id'] = $user_id;
	        		}

	        		if($request->user_type == 'serviceprovider'){

	        			$row['sp_id'] = $user_id;
	        			$commented_user_name = SpUser::where('id',$user_id)->select('store_name')->first();
	        		}
	        
	                $row->save();
	                $data['comment_id'] = $row->id;
	        		if($row->save())
	        		{ 
	        			if($request->bannerimagecount){

		                	$count = $request->bannerimagecount;

		                	for($i=1;$i<= $count;$i++){
		           
		                		$img = 'bannerimages'.$i;

		                		if ($request->$img)
				        		{
				                    $file = $request->$img;
				                    $name = time().str_random(2).'.'.$file->getClientOriginalExtension();
				                    $destinationPath = public_path('sp_uploads/forum_reply_banners/');
				                    $img = $file->move($destinationPath, $name);

				                    $rows = new ForumReplyBannerImage();

				                    $rows->comment_id = $row->id;
				                    $rows->banner_path = $name;
				                    $rows->save();
				                }
		                	}
		                } 

		                if($check_isNotify['isNotify'] == 1) 
	        			{
	        				if($user->email!='' && $user->verifyToken == ''){

	        					$edata['templete'] = "forum_notification";
								$edata['name'] = $user->name;
								$edata['email'] = $user->email;
								$edata['commented_user_name'] = $commented_user_name['store_name'];
								$edata['subject'] = "Your query is firing up!";
								send($edata);
	        				}

	        			}

        				if($user->notification_alert_status == '1'){

        					$message_main = __('messages.New Advice',['name' => $user->name,'subject'=>$check_isNotify->subject,'date'=>date('Y-m-d',strtotime($row['created_at']))]);

		      				// $message = 'Hi '.$user->name.', You have received a new response to your query posted on '.$check_isNotify->subject.date('Y-m-d',strtotime($row['created_at'])).'. Take a look…';

						    $message_title_main = __('messages.New Advice!');

						    app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');


							$message_other = __('messages.New Advice',['name' => $user->name,'subject'=>$check_isNotify->subject,'date'=>date('Y-m-d',strtotime($row['created_at']))]);

						    $message_title_other = __('messages.New Advice!');
			          		
				          	Notification::saveNotification($user->id,$row->sp_id,$check_isNotify->id,'FORUM_SP_RESPONSE',$message_main,$message_other,$message_title_main,$message_title_other,'customer');
				          	app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

		      			}
		      			

	        			return response()->json(['status' => true, 'message' => __('messages.forum comment'), 'data'=>$data]);
	        		}
		            
		        }
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
    	}
	}

	public function commentLikeDislike(Request $request)
	{
		if (Input::isMethod('post')) {
    		try{
    			$validator = Validator::make($request->all(), [
                    // 'user_id'=>'required',
                    'comment_id'=>'required',
                    'isLike'=>'required',
                    'user_type' => 'required',
                    'language' => 'required',
                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{
		        	
		        	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

		        	$check_field = '';

		        	if($request->user_type == 'customer'){
		        		$check_field = 'user_id';
		        	}else{
		        		$check_field = 'sp_id';
		        	}
		        	$check_row = ForumCommentLikeDislike::where([['comment_id',$request->comment_id],[$check_field,$user_id]])->count();
		        	
		        	if($check_row > 0){
		        		$row = ForumCommentLikeDislike::where([['comment_id',$request->comment_id],[$check_field,$user_id]])->first();

		        	}else{
		        		$row = new ForumCommentLikeDislike();
		        	}

	        		$row['comment_id'] = $request->comment_id;
	        		$row['isLike'] = $request->isLike;

	        		if($request->user_type == 'customer'){

	        			$row['user_id'] = $user_id;
	        		}

	        		if($request->user_type == 'serviceprovider'){

	        			$row['sp_id'] = $user_id;
	        		}
	        
	               
	        		if($row->save())
	        		{ 
	        			$new_row = ForumCommentLikeDislike::where('comment_id',$request->comment_id)->get();

	        			$data['like'] = '';
		          		$data['dislike'] = '';
		          		$like_count = 0;
		          		$dislike_count = 0;

	        			foreach($new_row as $nrow){
	        				if($nrow['isLike'] == 1){
		          				$like_count = $like_count+1;
		          				$data['like'] = $like_count;
		          			}
		          			if($nrow['isLike'] == 0){
		          				$dislike_count = $dislike_count+1;
		          				$data['dislike'] = $dislike_count;
		          			}
	        			}
	        			return response()->json(['status' => true,'data'=>$data]);
	        		}
		            
		        }
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
    	}
	}

	public function getForumComment(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'forum_id' => 'required',
		              'user_type' => 'required',
		              'language' => 'required',
		              'page' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {  
		          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

		          	$currentpage = $request->page;
	          		Paginator::currentPageResolver(function () use ($currentpage) {
				        return $currentpage;
				    });

		          	$field_check = 'user_id';
		          	if($request->user_type == 'serviceprovider'){
		          		$field_check = 'sp_id';
		          	}
		          	$user_comment_ids = ForumComment::where([['forum_id',$request->forum_id], [$field_check,$user_id]])->pluck('id')->toArray();

		          	$comments = ForumComment::where([['forum_id',$request->forum_id]]);
		          	
		          	if(!empty($user_comment_ids)){
		          		
	          			$comments = $comments->orderByRaw(DB::raw("FIELD(forum_comments.id, ".  implode(',', $user_comment_ids).") DESC"), 'forum_comments.id');

		          	}
		          	$row = $comments->paginate(10);
		          	
		            $rows_arr = $row->toArray();
		          	
		          	$final_data = array();
		          	
		          	foreach($row as $rows)
		          	{
		          		$data = array();
		          		$data['id'] = $rows->id;
		          		$data['forum_id'] = $rows->forum_id;

		          		if($rows->user_id!=null && $rows->user_id!=''){
		          			$role = 'user_id';
		          			$data['user_type'] = 'customer';
		          		}
		          		else{
		          			$role = 'sp_id';
		          			$data['user_type'] = 'serviceprovider';
		          		}
		          		if($data['user_type'] == 'customer'){
		          			$data['name'] = $rows->getAssociatedForumCustomer->name;
		          			if($rows->getAssociatedForumCustomer->image!=''){
		          				$data['image'] = asset($rows->getAssociatedForumCustomer->image);
		          			}else{
		          				$data['image'] = '';
		          			}
		          			

		          		}else{
		          			$data['name'] = $rows->getAssociatedForumServiceProvider->name;
		          			$data['store_name'] = $rows->getAssociatedForumServiceProvider->store_name;

		          			if($rows->getAssociatedForumServiceProvider->profile_image!=''){
		          				$data['image'] = changeImageUrlForFileExist(asset('sp_uploads/profile/'.$rows->getAssociatedForumServiceProvider->profile_image));
		          			}else{
		          				$data['image'] = '';
		          			}

		          			if($rows->getAssociatedCommentBannerImages){
		          				$data['banner_images'] = array();
		          				foreach($rows->getAssociatedCommentBannerImages as $bimage){
		          					$data['banner_images'][] = asset('sp_uploads/forum_reply_banners/'.$bimage->banner_path);
		          				}
		          			}
		          		}
		          		$data['like'] = '';
		          		$data['dislike'] = '';
		          		$data['isLike'] = '';
		          		$like_count = 0;
		          		$dislike_count = 0;
		          		foreach($rows->getAssociatedLikeDislike as $ld){
		          			if($ld['sp_id'] == $user_id){
	          					if($ld['isLike'] == 1){
	          						$data['isLike'] = 1;
	          					}else{
	          						$data['isLike'] = 0;
	          					}
	          				}
		          			if($ld['isLike'] == 1){
		          				$like_count = $like_count+1;
		          				$data['like'] = $like_count;
		          			}
		          			if($ld['isLike'] == 0){
		          				$dislike_count = $dislike_count+1;
		          				$data['dislike'] = $dislike_count;
		          			}
		          		}
		          		
		          		
		          		$data[$role] = $rows->$role;

		          		$data['message'] = $rows->message;
		          		$data['created_at'] = date('d-M-Y H:i:s',strtotime($rows->created_at));
		         
		          		
		          		$final_data[] = $data;
		          	}

		          	$result['data'] = $final_data;
	                $result['current_page']     = $rows_arr['current_page'];
	                $result['from']             = $rows_arr['from'];
	                $result['last_page']        = $rows_arr['last_page'];
	                $result['next_page_url']    = $rows_arr['next_page_url'];
	                $result['per_page']         = $rows_arr['per_page'];
	                $result['prev_page_url']    = $rows_arr['prev_page_url'];
	                $result['to']               = $rows_arr['to'];
	                $result['total']            = $rows_arr['total'];
	
		          	if($row)
		          	{
		          		
					    return response()->json(['status' => true, 'message' => 'Forum','data' => $result]);	
						
		          	}else{
		          		return response()->json(['status' => false, 'message' => 'Forum not found']);	
		          	}
		          	
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function getForumDetail(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'language' => 'required',
		              'forum_id' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {  

		          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

		          	$rows = Forum::where('id',$request->forum_id)->first();
		          	if($rows)
		          	{
		          		$data_1 = [
		          			'id' => $rows->id,
		          			'user_id' => $rows->user_id,
		          			'user_name' => isset($rows->getAssociatedUserInformation->name) ? $rows->getAssociatedUserInformation->name:'',
		          			'is_open_forum' => $rows->is_open_forum,
		          			'subject' => $rows->subject,
		          			'message' => $rows->message,
		          			'isNotify' => $rows->isNotify,
		          			'created_at' => date('d-M-Y H:i:s',strtotime($rows->created_at)),
		          		];

		          		$data_2 = array();
		          		foreach($rows->getAssociatedForumBannerImages as $img)
		          		{
		          			$data_2[] = [
		          				'id' => $img->id,
		          				'banner_path' => asset('sp_uploads/forum/'.$img->banner_path),
		          			];
		          		}
		          		$is_commented = 0;
		          		$data_1['is_commented'] = 0;
						if($rows->getAssociatedForumComments){
							 foreach($rows->getAssociatedForumComments as $comments){
							 	if($request->user_type == 'customer')
							 	{
							 		if($comments['user_id'] == $user_id){
							 			$data_1['is_commented'] = 1;
							 		}
							 		
							 	}else{
							 		if($comments['sp_id'] == $user_id){
							 			$data_1['is_commented'] = 1;
							 		}
							 	}
			 				 }
						}
		 
						$data_1['total_comments'] = count($rows->getAssociatedForumComments);
		          		$data_1['banner_images'] = $data_2;
		          		
			            return response()->json(['status' => true, 'message' => 'Forum Detail','data' => $data_1]);	

		          	}else{
		          		return response()->json(['status' => true, 'message' => 'Forum not found']);	
		          	}
		          	
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function getCustomerReviews(Request $request)
	{
		if (Input::isMethod('post')) {
    		try{
    			$validator = Validator::make($request->all(), [

                    'language' => 'required',
                    'page' => 'required',
                    'service_provider_id'=> 'required',
                    'filter'=> 'required|in:needs_improvement,average,good,very_good,superb,exceptional,all',
                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{
		        	
		        	// $user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
		        	$user_id = $request->service_provider_id;
		        	$currentpage = $request->page;

	          		Paginator::currentPageResolver(function () use ($currentpage) {
				        return $currentpage;
				    });

		        	$results = RatingReview::select('rating_reviews.id','reviews.customer_name','reviews.user_id','reviews.description','reviews.created_at','reviews.is_anonymous','users.name as user_name',DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1) as rating"))
		        		->leftJoin('reviews', function($join) {
						      $join->on('rating_reviews.id', '=', 'reviews.id');
						    })
		        		->leftJoin('users', function($join) {
						      $join->on('reviews.user_id', '=', 'users.id');
						    })

		        		->where('rating_reviews.sp_id',$user_id)
		        		->where('reviews.is_approved','1');


		        		if($request->filter == 'needs_improvement'){
		        			 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'>',2.3);
		        			 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'<',5);		        			
		        		}
		        		elseif($request->filter == 'average'){
		        			 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'>',4.9);
		        			 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'<=',6);		        			
		        		}
		        		elseif($request->filter == 'good'){
		        			 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'>',6);
		        			 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'<=',7);		        			
		        		}
		        		elseif($request->filter == 'very_good'){
		        			 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'>',7);
		        			 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'<=',8);		        			
		        		}
		        		elseif($request->filter == 'superb'){
		        			 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'>',8);
		        			 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'<=',9);		        			
		        		}elseif($request->filter == 'exceptional'){
		        			 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'>',9);
		        			 $results->having(DB::raw("round(SUM(rating_reviews.question_values * rating_reviews.rating_values)/10,1)"),'<=',10);		        			
		        		}				
		        		
		        		$results = $results->groupBy('rating_reviews.id')->paginate(10);
		        		$rows_arr = $results->toArray();
		        		$result['data'] = $results;
		                $result['current_page']     = $rows_arr['current_page'];
		                $result['from']             = $rows_arr['from'];
		                $result['last_page']        = $rows_arr['last_page'];
		                $result['next_page_url']    = $rows_arr['next_page_url'];
		                $result['per_page']         = $rows_arr['per_page'];
		                $result['prev_page_url']    = $rows_arr['prev_page_url'];
		                $result['to']               = $rows_arr['to'];
		                $result['total']            = $rows_arr['total'];

		
			        	return response()->json(['status' => true, 'message' => 'My Reviews', 'data' =>$results]);
		          	}	        	
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
    	}
	}

	public function getCombinedReviews(Request $request)
	{
		if (Input::isMethod('post')) {
    		try{
    			$validator = Validator::make($request->all(), [

                    'language' => 'required',
                    'service_provider_id'=> 'required',
                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{
		        	
		        	$user_id = $request->service_provider_id;
		        	$reviews_ques_arr = \Config::get('constants.review_questions_arr');
        			$rating_arr = \Config::get('constants.rating_arr');

		        	$reviews = Review::where('sp_id',$user_id)->where('is_approved','1')->get();
		        	$sr_name = 'name'.$this->col_postfix;
		          	if(count($reviews)>0){
		          		$rows_arr = $reviews->toArray();

			        	$review_arr = array();
			        
			        	$service_wise_arr = array();
			        	foreach($reviews as $rows)
			        	{
			        		$rating_calc = array();
			        		foreach($rows->getAssociatedReviewQuestions as $review_ques)
			        		{
			        			$ques_value =  $reviews_ques_arr[$review_ques['ques_id']]['value'];
			        			$ans =  $rating_arr[$review_ques['rating']]['value'];

			        			$rating_calc[] = $ques_value*$ans;
			        		}

			        		foreach($rows->getAssociatedReviewServices as $review_sr)
			        		{
			        			if(!isset($service_wise_arr[$review_sr['service_id']]['dislike_count'])) {
			        				$service_wise_arr[$review_sr['service_id']]['dislike_count'] = 0;
			        			}

			        			if($review_sr['like_dislike'] == 0){
			        				$service_wise_arr[$review_sr['service_id']]['dislike_count'] += (integer)1;
		        					
		        				}
			        			
			        			if(isset($service_wise_arr[$review_sr['service_id']]['like_count'])){
			        				$service_wise_arr[$review_sr['service_id']]['like_count'] += (integer)$review_sr['like_dislike'];
			        				
			        			} else{
			        				$service_wise_arr[$review_sr['service_id']]['like_count'] = 0 + (integer)$review_sr['like_dislike'];
			        				
			        			}
			        			//$service_wise_arr[$review_sr['service_id']]['likes_dislikes'][] =  $review_sr['like_dislike'];
			        			if(!isset($service_wise_arr[$review_sr['service_id']]['service_id'])) {
			        				$service_wise_arr[$review_sr['service_id']]['service_id'] =  $review_sr['service_id'];
			        				$service_wise_arr[$review_sr['service_id']]['service_name'] =  $review_sr->getAssociatedReviewService->getAssociatedService->$sr_name;
			        			}
			        		}


			        		$final_rating = (array_sum($rating_calc))/10;

			        		$review_arr[$rows['id']] = $final_rating;

			        	}
			        	 array_multisort(array_map(function($element) {
						      return $element['like_count'];
						  }, $service_wise_arr), SORT_DESC, $service_wise_arr);
			        	 $top_ten = array_slice($service_wise_arr, 0,10);
			        	 
			        	$total_reviews = array_sum($review_arr)/count($review_arr);
			        
			        	$question_wise_arr = array();
			        	foreach($reviews as $rows)
			        	{
			        		foreach($rows->getAssociatedReviewQuestions as $review_ques)
			        		{
			        			$question_wise_arr[$review_ques['ques_id']][] = $rating_arr[$review_ques['rating']]['value'];
			        		}

			        	}
			        	
			        	$ques_final_calc = array();

			        	foreach($question_wise_arr as $key => $val){
			        		$ques_final_calc[$reviews_ques_arr[$key]['key_value']] = round((array_sum($val))/count($val),0);
			        	}

			        	$finale_data = $ques_final_calc;
			        	$finale_data['average_rating'] = round($total_reviews,1);
			        	$finale_data['total_reviewers'] = count($review_arr);
			        	$finale_data['services'] = $service_wise_arr;
			        	
			        	return response()->json(['status' => true, 'message' => 'My Reviews', 'data' =>$finale_data]);
		          	}else{
		          		return response()->json(['status' => true, 'message' => 'Reviews not found', 'data' =>[]]);
		          	}
		        	
		        }
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getLine(), 'data' => []]);
	       }
    	}
	}

	public function getTags()
	{
		try
	    {
	    	$tags = array();
	    	$rows = SpTags::get();
	    	if($rows){
		    	foreach($rows as $row)
				{
					$tags[] = [
						'id'=>$row->id,
						'tag_name'=>$row->tag_name,
						];
				}
				return response()->json(['status' => true, 'message' => 'Tags List','data'=>$tags]);
			}else{
				return response()->json(['status' => true, 'message' => 'No Record Found','data'=>[]]);
			}
	    }
	    catch(\Exception $e){
	       	return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	    }
	}

	public function addTag(Request $request)
    {

    	if (Input::isMethod('post')) {
    		try{

    			$validator = Validator::make($request->all(), [
                    'tag_name'=>'required|max:50',
                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{
		        	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
		        	$tagsArr = array('all','ALL','All','aLL','staff','STAFF');
		        	if(in_array($request->tag_name,$tagsArr))
		        	{ 
		        		$name = strtolower($request->tag_name);
		        		return response()->json(['status' => false, 'message' => 'You Can Not create a tag with "'.$name.'" names.']);
		        	}

	        		$row = new SpTags();
	        		$row['tag_name'] = ucwords($request->tag_name);
	        		
	                $row->save();
	        		if($row->save())
	        		{
	        			return response()->json(['status' => true, 'message' => 'Tag Added Successfully']);
	        		}
		        }
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
    	}
    }

    public function addMedia(Request $request)
    {
    	if (Input::isMethod('post')) {
    		try{

    			$validator = Validator::make($request->all(), [
                    'title'=>'required|max:50',
                    'tags'=>'required',
                    'file_type'=>'required|in:image,video,url', // image, video, url
                    'file_name'=>'required',
                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{

		        	$tagsArr  = SpTags::pluck('tag_name', 'id')->toArray();
		        	
		        	$tagIds = array();
		        	$arr = explode(',',$request->tags);
		        	foreach($arr as $tag_id)
					{
						if(array_key_exists($tag_id,$tagsArr))
						{
							array_push($tagIds,$tag_id);
						}
						else
						{
							return response()->json(['status' => false, 'message' => $tag_id.' - Tag Id Not Available.']);
						}
					}

		        	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	        		$row = new SpGallery();
	        		$row['sp_id'] = $user_id;
	        		$row['tag_id'] = implode(',',$tagIds);
	        		$row['title'] = ucfirst($request->title);
	        		$row['file_desc'] = $request->description;
                    $row['sort_order'] = $request->sort_order;
                    
                    if($request->file_type == 'image' || $request->file_type == 'video')
                    {
                    	$allowedfileExtension= ($request->file_type == 'image') ? ['pdf','jpg','png','jpeg'] : ['mp4','webm'];

                    	if ($request->hasFile('file_name') && $request->file('file_name'))
	        			{
                          	$file = $request->file('file_name');
                          	$extension = $file->getClientOriginalExtension();
                          	$check = in_array($extension,$allowedfileExtension);

                          	if($check) {
	                            $getmimeType = $file->getClientMimeType();
	                            $file_name = time().str_random(2).'.'.$extension;
	                            $mimeType = substr($getmimeType,0,5);
	                            $path = public_path('/sp_uploads/gallery/'.$mimeType.'/');
	                            $file->move($path, $file_name);
	                            $row['file_name'] = $file_name;
	                            $row['mime_type'] = $getmimeType;
	                        }
	                        else
					        {
					        	return response()->json(['success'=>false,'message'=>'Invalid File Format'],422);
					        }
                        }
                    }
                    else
                    {
                    	if(!isValidYoutubeUrl($request->file_name))
                    	{
                    		return response()->json(['status' => false, 'message' => 'Invalid Youtube url.']);
                    	}
                    	else
                    	{
	                       $row['file_name'] = $request->file_name;
	                       $row['mime_type'] = 'url';
	                   	}
                    }

                    $row->save();
	        		if($row->save())
	        		{
	        			return response()->json(['status' => true, 'message' => 'Gallery Added Successfully']);
	        		}
		            
		        }
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
    	}
    }

    public function editMedia(Request $request)
    {
    	if (Input::isMethod('post')) {
    		try{

    			$validator = Validator::make($request->all(), [
    				'media_id' => 'required',
                    'title'=>'required|max:50',
                    'tags'=>'required',
                    'file_type'=>'required|in:image,video,url', // image, video, url
                    'file_name'=>'required',
                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{

		        	$tagsArr  = SpTags::pluck('tag_name', 'id')->toArray();
		        	
		        	$tagIds = array();
		        	$arr = explode(',',$request->tags);
		        	foreach($arr as $tag_id)
					{
						if(array_key_exists($tag_id,$tagsArr))
						{
							array_push($tagIds,$tag_id);
						}
						else
						{
							return response()->json(['status' => false, 'message' => $tag_id.' - Tag Id Not Available.']);
						}
					}

		        	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
		        	$row = SpGallery::where('id',$request->media_id)->first();
	        		$row['sp_id'] = $user_id;
	        		$row['tag_id'] = implode(',',$tagIds);
	        		$row['title'] = ucfirst($request->title);
	        		$row['file_desc'] = $request->description;
                    $row['sort_order'] = $request->sort_order;
                    
                    if($request->file_type == 'image' || $request->file_type == 'video')
                    {
                    	$allowedfileExtension= ($request->file_type == 'image') ? ['pdf','jpg','png','jpeg'] : ['mp4','webm'];

                    	if($request->hasFile('file_name') && $request->file('file_name'))
	        			{
                          	$file = $request->file('file_name');
                          	if (str_contains($file, 'sp_uploads/gallery')) { 
							    //echo 'true';
							}
							else
							{
	                          	$extension = $file->getClientOriginalExtension();
	                          	$check = in_array($extension,$allowedfileExtension);

	                          	if($check) {
		                            $getmimeType = $file->getClientMimeType();
		                            $file_name = time().str_random(2).'.'.$extension;
		                            $mimeType = substr($getmimeType,0,5);
		                            $path = public_path('/sp_uploads/gallery/'.$mimeType.'/');
		                            $file->move($path, $file_name);
		                            $row['file_name'] = $file_name;
		                            $row['mime_type'] = $getmimeType;
		                        }
		                        else
						        {
						        	return response()->json(['success'=>false,'message'=>'Invalid File Format'],422);
						        }
						    }
                        }
                    }
                    else
                    {
                    	if(!isValidYoutubeUrl($request->file_name))
                    	{
                    		return response()->json(['status' => false, 'message' => 'Invalid Youtube url.']);
                    	}
                    	else
                    	{
	                       $row['file_name'] = $request->file_name;
	                       $row['mime_type'] = 'url';
	                   	}
                    }

                    $row->save();
	        		if($row->save())
	        		{
	        			return response()->json(['status' => true, 'message' => 'Gallery Updated Successfully']);
	        		}
		            
		        }
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
    	}
    }

    public function listGallery(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'page_no' => 'required',
		              'tags' => 'required', // default all
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {  	
		          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
	          		$currentpage = $request->page_no;
	          		Paginator::currentPageResolver(function () use ($currentpage) {
				        return $currentpage;
				    });

	          		$tagsFilter = array();
	          		$tagsArr  = SpTags::pluck('tag_name', 'id')->toArray();
	          		$rowdata = SpGallery::where('sp_id',$user_id)->select('tag_id')->orderBy('sort_order','ASC')->get();

	          		// get Tags filter
	          		foreach($rowdata as $data)
	          		{  
	          			$tagIds = explode(',',$data->tag_id);
	          			foreach($tagIds as $id)
	          			{
	          				$arrr['id'] = (int)$id;
	          				$arrr['tag_name'] = $tagsArr[$id];
	          				$tagsFilter[] = $arrr;
	          			}
	          		}
	          		$tagsFilter = array_values(array_map("unserialize", array_unique(array_map("serialize", $tagsFilter))));
	          		
	          		// get sql rows data
	          		$sqlrows = SpGallery::where('sp_id',$user_id);
	          		if($request->tags != 'all')
	          		{
	          			$sqlrows = $sqlrows->whereRaw("FIND_IN_SET('".$request->tags."',tag_id)");
	          		}
	          		$rows = $sqlrows->orderBy('sort_order','ASC')->paginate(12);
	          		$i=0;
	          		foreach($rows as $row)
	          		{
	          			$tags = array();
	          			$tagIds = explode(',',$row->tag_id);

	          			foreach($tagIds as $id)
	          			{
	          				$arrr['id'] = (int)$id;
	          				$arrr['tag_name'] = $tagsArr[$id];
	          				$tags[] = $arrr;
	          			}
	          			
	          			$rows[$i]['tags'] = $tags;

	          			$mimetype = substr($row->mime_type,0,5);
		                if($mimetype == 'image' || $mimetype == 'video')
		                {
		                    $file_name = asset('public/sp_uploads/gallery/'.$mimetype.'/'.$row->file_name);
		                }
		                else
		                    $file_name = $row->file_name;

	          			$rows[$i]['file_name'] = $file_name;
	          			$rows[$i]['mime_type'] = $mimetype;
	          			$i++;
	          		}
	          		return response()->json(['status' => true, 'message' => 'Media List', 'tags'=> $tagsFilter ,'data' => $rows]);
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function deleteMedia(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'media_id' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {  	
		          	$row = SpGallery::where('id',$request->media_id)->first();
		          	if($row)
		          	{	
		          		$mimetype = substr($row->mime_type,0,5);
		          		if($mimetype !="url")
		          		{
			          		$image_path = "public/sp_uploads/gallery/".$mimetype."/".$row->file_name;

			          		unlink($image_path);
			          	}
			          
					    $row->delete();
					    return response()->json(['status' => true, 'message' => 'Media deleted successfully']);	
						
		          	}else{
		          		return response()->json(['status' => false, 'message' => 'Media not found']);	
		          	}
		          	
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}
}
  