<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use JWTAuth;
use JWTAuthException;

use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Device;
use App\Model\Country;
use App\Model\OtpNumber;
use App\Model\SocialAccount;
use App\Model\CashbackHistory;
use App\Model\Setting;
use App\Model\Booking;
use App\Model\SpUser;
use App\Model\ECard;
use App\Model\WalletCashHistory;
use App\Model\Notification;

class UserController extends Controller
{

	public function __construct(Request $request)
    {
    	 parent::__construct($request);

    }
 

    public function addNationality()
	{
		try
	    {
	    	$arr = ["Afghanistan"=>"Afghan", "Åland Islands"=>"Åland Island", "Albania"=>"Albanian", "Algeria"=>"Algerian", "American Samoa"=>"American Samoan", "Andorra"=>"Andorran", "Angola"=>"Angolan", "Anguilla"=>"Anguillan", "Antarctica"=>"Antarctic", "Antigua And Barbuda"=>"Antigua And Barbuda", "Argentina"=>"Argentine", "Armenia"=>"Armenian", "Aruba"=>"Aruban", "Australia"=>"Australian", "Austria"=>"Austrian", "Azerbaijan"=>"Azerbaijani, Azeri", "Bahamas"=>"Bahamian", "Bahrain"=>"Bahraini", "Bangladesh"=>"Bangladeshi", "Barbados"=>"Barbadian", "Belarus"=>"Belarusian", "Belgium"=>"Belgian", "Belize"=>"Belizean", "Benin"=>"Beninese, Beninois", "Bermuda"=>"Bermudian, Bermudan", "Bhutan"=>"Bhutanese", "Bolivia"=>"Bolivian", "Bosnia and Herzegovina" => "Bosnian or Herzegovinian", "Botswana"=>"Motswana, Botswanan", "Bouvet Island"=>"Bouvet Island", "Brazil"=>"Brazilian", "British Indian Ocean Territory"=>"BIOT", "Brunei"=>"Bruneian", "Bulgaria"=>"Bulgarian", "Burkina Faso"=>"Burkinabé", "Burundi"=>"Burundian", "Cabo Verde"=>"Cabo Verdean", "Cambodia"=>"Cambodian", "Cameroon"=>"Cameroonian", "Canada"=>"Canadian", "Cayman Islands"=>"Caymanian", "Central African Republic"=>"Central African", "Chad"=>"Chadian", "Chile"=>"Chilean", "China"=>"Chinese", "Christmas Island"=>"Christmas Island", "Cocos (Keeling) Islands"=>"Cocos Island", "Colombia"=>"Colombian", "Comoros"=>"Comoran, Comorian", "Republic Of The Congo"=>"Congolese", "Democratic Republic Of The Congo"=>"Congolese", "Cook Islands"=>"Cook Island", "Costa Rica"=>"Costa Rican", "Cote D'Ivoire (Ivory Coast)"=>"Ivorian", "Croatia (Hrvatska)"=>"Croatian", "Cuba"=>"Cuban", "Curaçao"=>"Curaçaoan", "Cyprus"=>"Cypriot", "Czech Republic"=>"Czech", "Denmark"=>"Danish", "Djibouti"=>"Djiboutian", "Dominica"=>"Dominican", "Dominican Republic"=>"Dominican", "External Territories of Australia" => "Australian", "Ecuador"=>"Ecuadorian", "Egypt"=>"Egyptian", "El Salvador"=>"Salvadoran", "Equatorial Guinea"=>"Equatorial Guinean, Equatoguinean", "Eritrea"=>"Eritrean", "Estonia"=>"Estonian", "Ethiopia"=>"Ethiopian", "Falkland Islands"=>"Falkland Island", "Faroe Islands"=>"Faroese", "Fiji Islands"=>"Fijian", "Finland"=>"Finnish", "France"=>"French", "French Guiana"=>"French Guianese", "French Polynesia"=>"French Polynesian", "French Southern Territories"=>"French Southern Territories", "Gabon"=>"Gabonese", "Gambia"=>"Gambian", "Georgia"=>"Georgian", "Germany"=>"German", "Ghana"=>"Ghanaian", "Gibraltar"=>"Gibraltar", "Greece"=>"Greek,Hellenic", "Greenland"=>"Greenlandic", "Grenada"=>"Grenadian", "Guadeloupe"=>"Guadeloupe", "Guam"=>"Guamanian, Guambat", "Guatemala"=>"Guatemalan", "Guernsey and Alderney"=>"Channel Island", "Guinea"=>"Guinean", "Guinea-Bissau"=>"Bissau-Guinean", "Guyana"=>"Guyanese", "Hong Kong S.A.R." => "Hong Kong", "Haiti"=>"Haitian", "Heard Island And McDonald Islands"=>"Heard Island And McDonald Islands", "Vatican City State (Holy See)"=>"Vatican", "Honduras"=>"Honduran", "Hong Kong"=>"Hong Kong,Hong Kongese", "Hungary"=>"Hungarian, Magyar", "Iceland"=>"Icelandic", "India"=>"Indian", "Indonesia"=>"Indonesian", "Iran"=>"Iranian,Persian", "Iraq"=>"Iraqi", "Ireland"=>"Irish", "Man (Isle of)"=>"Manx", "Israel"=>"Israeli", "Italy"=>"Italian", "Jamaica"=>"Jamaican", "Japan"=>"Japanese", "Jersey"=>"Channel Island", "Jordan"=>"Jordanian", "Kazakhstan"=>"Kazakhstani,Kazakh", "Kenya"=>"Kenyan", "Kiribati"=>"I-Kiribati", "Korea North"=>"Korean", "Korea South"=>"Korean", "Kuwait"=>"Kuwaiti", "Kyrgyzstan"=>"Kyrgyzstani, Kyrgyz, Kirgiz, Kirghiz", "Laos"=>"Laotian", "Latvia"=>"Latvian", "Lebanon"=>"Lebanese", "Lesotho"=>"Basotho", "Liberia"=>"Liberian", "Libya"=>"Libyan", "Liechtenstein"=>"Liechtenstein", "Lithuania"=>"Lithuanian", "Luxembourg"=>"Luxembourg, Luxembourgish", "Macau S.A.R."=>"Macanese, Chinese", "Macedonia"=>"Macedonian", "Madagascar"=>"Malagasy", "Malawi"=>"Malawian", "Malaysia"=>"Malaysian", "Maldives"=>"Maldivian", "Mali"=>"Malian, Malinese", "Malta"=>"Maltese", "Marshall Islands"=>"Marshallese", "Martinique"=>"Martiniquais,Martinican", "Mauritania"=>"Mauritanian", "Mauritius"=>"Mauritian", "Mayotte"=>"Mahoran", "Mexico"=>"Mexican", "Micronesia"=>"Micronesian", "Moldova"=>"Moldovan", "Monaco"=>"Monégasque, Monacan", "Mongolia"=>"Mongolian", "Montenegro"=>"Montenegrin", "Montserrat"=>"Montserratian", "Morocco"=>"Moroccan", "Mozambique"=>"Mozambican", "Myanmar"=>"Burmese", "Namibia"=>"Namibian", "Nauru"=>"Nauruan", "Nepal"=>"Nepali,Nepalese", "Netherlands Antilles"=>"Dutch,Netherlandic", "Netherlands The"=>"Dutch,Netherlandic", "New Caledonia"=>"New Caledonian", "New Zealand"=>"New Zealand, NZ", "Nicaragua"=>"Nicaraguan", "Niger"=>"Nigerien", "Nigeria"=>"Nigerian", "Niue"=>"Niuean", "Norfolk Island"=>"Norfolk Island", "Northern Mariana Islands"=>"Northern Marianan", "Norway"=>"Norwegian", "Oman"=>"Omani", "Pakistan"=>"Pakistani", "Palau"=>"Palauan", "Palestinian Territory Occupied"=>"Palestinian", "Panama"=>"Panamanian", "Papua new Guinea"=>"Papua New Guinea, Papuan", "Paraguay"=>"Paraguayan", "Peru"=>"Peruvian", "Philippines"=>"Philippine, Filipino", "Poland"=>"Polish", "Portugal"=>"Portuguese", "Puerto Rico"=>"Puerto Rican", "Qatar"=>"Qatari", "Reunion"=>"Réunionese, Réunionnais", "Romania"=>"Romanian", "Rwanda"=>"Rwandan", "Russia" => "Russian", "Saint Barthélemy"=>"Barthélemois", "Saint Helena"=>"Saint Helenian", "Saint Kitts And Nevis"=>"Kittitian or Nevisian", "Saint Lucia"=>"Saint Lucian", "Saint Martin"=>"Saint-Martinoise", "Saint Pierre and Miquelon"=>"Saint-Pierrais or Miquelonnais", "Saint Vincent And The Grenadines"=>"Saint Vincentian, Vincentian", "Samoa"=>"Samoan", "San Marino"=>"Sammarinese", "Sao Tome and Principe"=>"São Toméan", "Saudi Arabia"=>"Saudi, Saudi Arabian", "Senegal"=>"Senegalese", "Serbia"=>"Serbian", "Seychelles"=>"Seychellois", "Sierra Leone"=>"Sierra Leonean", "Singapore"=>"Singaporean", "Slovakia"=>"Slovak", "Slovenia"=>"Slovenian, Slovene", "Solomon Islands"=>"Solomon Island", "Somalia"=>"Somali, Somalian", "South Africa"=>"South African", "South Sudan"=>"South Sudanese", "Spain"=>"Spanish", "Sri Lanka"=>"Sri Lankan", "Sudan"=>"Sudanese", "Suriname"=>"Surinamese", "Svalbard And Jan Mayen Islands" => "Svalbard", "Swaziland"=>"Swazi", "Sweden"=>"Swedish", "Switzerland"=>"Swiss", "Syria"=>"Syrian", "Taiwan"=>"Chinese, Taiwanese", "Tajikistan"=>"Tajikistani", "Tanzania"=>"Tanzanian", "Thailand"=>"Thai", "Timor-Leste"=>"Timorese", "Togo"=>"Togolese", "Tokelau"=>"Tokelauan", "Tonga"=>"Tongan", "Trinidad And Tobago"=>"Trinidadian or Tobagonian", "Tunisia"=>"Tunisian", "Turkey"=>"Turkish", "Turkmenistan"=>"Turkmen", "Turks And Caicos Islands"=>"Turks And Caicos Island", "Tuvalu"=>"Tuvaluan", "Uganda"=>"Ugandan", "Ukraine"=>"Ukrainian", "United Arab Emirates"=>"Emirati, Emirian, Emiri", "United Kingdom"=>"British, UK", "United States"=>"American", "Uruguay"=>"Uruguayan", "Uzbekistan"=>"Uzbekistani, Uzbek", "Vanuatu"=>"Ni-Vanuatu, Vanuatuan", "Venezuela"=>"Venezuelan", "Vietnam"=>"Vietnamese", "Virgin Islands (British)"=>"British Virgin Island", "Virgin Islands (US)"=>"U.S. Virgin Island", "Wallis And Futuna Islands"=>"Wallisian or Futunan", "Western Sahara"=>"Sahrawi,Sahrawian,Sahraouian", "Yemen"=>"Yemeni", "Zambia"=>"Zambian", "Zimbabwe" => "Zimbabwean"];
	    	
	    	$not_found = array();
	    	$rows = Country::get();
	    	if($rows){
		    	foreach($rows as $row)
				{
					if(array_key_exists($row->name,$arr))
					{
						$UpdateDetails = Country::where('name', '=',  $row->name)->first();
				        $UpdateDetails->nationality = $arr[$row->name];
				        $UpdateDetails->save();
					}
					else
					{
						$not_found[] = $row->id.'-'.$row->name.' - Country Not Found.'.'<br>';
					}
				}
				return response()->json(['status' => true, 'message' => 'Nationality Updated Successfully' , 'data'=>$not_found]);
			}else{
				return response()->json(['status' => true, 'message' => 'No Record Found','data'=>[]]);
			}
	    }
	    catch(\Exception $e){
	       	return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	    }
	}

	public function getCountryList()
	{
		try
	    {
	    	$rows = Country::get();
	    	if($rows){
		    	foreach($rows as $row)
				{
					$countries[] = [
						'id'=>$row->id,
						'country_code'=>$row->sortname,
						'country_name'=>$row->name,
						'nationality'=>$row->nationality,
						'phone_code'=>$row->phonecode,
						'flag_url' => asset('public/sp_uploads/flags/'.$row['flag_url'].'.svg')
						];
				}
				return response()->json(['status' => true, 'message' => 'Country Code List','data'=>$countries]);
			}else{
				return response()->json(['status' => true, 'message' => 'No Record Found','data'=>[]]);
			}
	    }
	    catch(\Exception $e){
	       	return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	    }
	}

	public function registerOtp(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'full_name' => 'required',
		              //'email' => 'email|unique:users',
		              'country_code' => 'required|exists:countries,phonecode',
		              'mobile_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:16|min:8',
		              'registration_type' => 'required|in:manual,facebook,google,whatsapp,apple',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {
					$email_check = 0;
					
	          		$user = User::where('mobile_no',$request->mobile_no)->where('country_code',$request->country_code)->count();

	          		if($request->refer_code){
	          			$refer_code = strtoupper($request->refer_code);
	          			$check_refer_code = User::where('share_code',$refer_code)->count();

	          			$check_sp_refer_code = SpUser::where('share_code',$refer_code)->count();
	          		}
	          		
	          		if($request->email){
							$email_check = User::where('email',$request->email)->count();
					}
	          		if($user > 0)
	          		{	
	          			return response()->json(['status' => false,'message' =>  __('messages.Mobile number already exist')  ]);
	          		}elseif($email_check > 0)
	          		{
						return response()->json(['status' => false,'message' =>  __('messages.Email already exist') ]);
					}elseif(isset($check_refer_code) && $check_refer_code == 0 && isset($check_sp_refer_code) && $check_sp_refer_code == 0) 
	          		{
	          			return response()->json(['status' => false,'message' =>  __('messages.Refer code is not valid') ]);

					}else{
						$mobileNoWithCode = $request->country_code . $request->mobile_no;
	          			$otp = $this->getOpt($mobileNoWithCode); 


	          			return response()->json(['status' => true,'otp' => $otp]);
	          		
	          		}
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function forgotPassword(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'country_code' => 'required|exists:countries,phonecode',
		              'mobile_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:16|min:8',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {
	          		$user = User::where('mobile_no',$request->mobile_no)->where('country_code',$request->country_code)->first();
	          		
	          		if($user)
	          		{
	          			$otp = $this->getOpt($request->country_code . $request->mobile_no);
	          			$rows = $this->showUserDetails($user);
                		return response()->json(['status' => true,'otp' => $otp,'data' => $rows]);	
	          		}
	          		else{
	          			
                		return response()->json(['status' => false,'message' => __('messages.Please enter valid mobile no')]);
	          		}
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function forgotResetPassword(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'country_code' => 'required|exists:countries,phonecode',
		              'mobile_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:16|min:8',
		              'password' => 'required|min:8|max:50',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {
	          		$user = User::where('mobile_no',$request->mobile_no)->where('country_code',$request->country_code)->first();
	          		
	          		if($user)
	          		{
	          			$user->password = bcrypt($request->password);
	          			$user->save();
                		return response()->json(['status' => true,'message' => __('messages.Password Updated Successfully')]);	
	          		}
	          		else{
	          			
                		return response()->json(['status' => false,'message' => __('messages.Please enter valid mobile no')]);
	          		}
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function getOpt($mobileNoWithCode)
	{
		$row = OtpNumber::where('complete_mobile_no',$mobileNoWithCode)->first();
		if($row){

			$otp = $row->otp;
		}
		else{			
			$otp = mt_rand(100000, 999999);
			// $otp = 12345;
		}

		$username =  env('SMS_COUNTRY_USERNAME');
		$password =  env('SMS_COUNTRY_PASSWORD');
		$message ="Your one time password for registration is ".$otp." don't share this with anyone."; 
		$parami =['User'=>$username,'passwd'=>$password,'mobilenumber'=>$mobileNoWithCode,'message'=>$message,'sid'=>'smscntry','mtype'=>'N'];
		$ponmo = http_build_query($parami);

		$url = "http://api.smscountry.com/SMSCwebservice_bulk.aspx?".$ponmo; // json

		$res = $this->get_content($url);

		if(!$row){
	        $new_row = new OtpNumber();
	      	$new_row->complete_mobile_no = $mobileNoWithCode;
	      	$new_row->otp = $otp;     
	        $new_row->save();
    	}

		return $otp;
		
		// if($res == 'SMS message(s) sent'){
		// 	return $otp;
		// }else{
		// 	return 'not sent';
		// }		
	}

	public function verificationApi(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'country_code' => 'required|exists:countries,phonecode',
		              'mobile_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:16|min:8',
		              'otp' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {
					$mobileNoWithCode = $request->country_code . $request->mobile_no;
	          		$row = OtpNumber::where('complete_mobile_no',$mobileNoWithCode)->where('otp',$request->otp)->count();
					if($row > 0){
						OtpNumber::where('complete_mobile_no',$mobileNoWithCode)->where('otp',$request->otp)->delete();
						return response()->json(['status' => true]);
					}else{
						return response()->json(['status' => false]);
					}
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}
	function get_content($URL)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $URL);
		$data = curl_exec($ch);
		// curl_exec($ch);
		curl_close($ch);
		return $data;
	} 

	public function registerUser(Request $request)
	{

		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		      	
		       	  $validator = Validator::make($data, 
		            [
		              'full_name' => 'required',
		              //'email' => 'required|email|unique:users',
		              'country_code' => 'required|exists:countries,phonecode',
		              'mobile_no' => 'required|required|regex:/^([0-9\s\-\+\(\)]*)$/|max:16|min:8',
		              'registration_type' => 'required|in:manual,facebook,google,whatsapp,apple',
		            //  'password' => 'required|min:8|max:50',
		              'device_type' => 'required|in:IPHONE,ANDROID,IOS',
		              'device_token' => 'required',
		              'otp' => 'required',
		              // 'gender' => 'in:Male,Female,Other',

		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {
		          	if($request->refer_code){
		          		$refer_code = strtoupper($request->refer_code);
	          			$check_refer_code = User::where('share_code',$refer_code)->count();

	          			$check_sp_refer_code = SpUser::where('share_code',$refer_code)->count();
	          		}

		          	if(isset($check_refer_code) && $check_refer_code == 0 && isset($check_sp_refer_code) && $check_sp_refer_code == 0) 
	          		{
	          			return response()->json(['status' => false,'message' => __('messages.Refer code is not valid')]);

					}else{
						$user = User::where('mobile_no',$request->mobile_no)->where('country_code',$request->country_code)->count();
			          	$country_row = Country::where('phonecode',$request->country_code)->first();
		          		if($user > 0)
		          		{
		          			return response()->json(['status' => false,'message' => __('messages.Mobile number already exist')]);
		          		}else{
		          			
		          			$mowithcode = $request->country_code . $request->mobile_no;
		          			$verifyotp = $this->verifyOtp($request->otp,$mowithcode);

		          			if($verifyotp == true){
		          				$user = new User();
					          	$user->name = $request->full_name;
					          	$user->country_id = $country_row->id;
				          		$user->country_code = $country_row->phonecode;
				          		if($request->email)
				          		{
				          			$user->email = $request->email;  
				          		}
			                    if($request->password)
				          		{
				          			$user->password = bcrypt($request->password);
				          		}                
			                    $user->gender = $request->gender;
			                    $user->mobile_no = $request->mobile_no;
			                    $user->registration_type = $request->registration_type;
			                    
			                    $user->refer_code = strtoupper($request->refer_code);
			                    $user->share_code = $this->generateUniqueShareCode();
			                    $user->status = '1';      
			                    if($request->registration_type == 'manual'){
			                    	$user->isPassword = 1;
			                    	$user->verifyToken = strtotime(date('Y-m-d H:i:s')).rand(1111,999999);
			                    }
			                    if ($request->image) {

			                    	$fileContents = file_get_contents($request->image);
			                    	$name = time();
			                    	$path = public_path() . '/uploads/' . $name.'.png';
			                    	try{
			                            file_put_contents($path,$fileContents);
			                    		$user->image = "uploads/".$name.'.png';
			                        }catch(\Exception $e) {
			                            $user->image = '';
			                        }
			                 
				                } 
			                    $user->save();
			     
			                    if($user->save())
			                    {

			                    	
			                    	$user = User::where('id',$user->id)->first();
			                    	
			                    	$user_mobile_no_with_code = $request->country_code.'-'.$request->mobile_no;

		                    		//check e-giftcard user existence
		                    		$current_date = date('Y-m-d');
		                    		$ecard = ECard::where('receiver_mobile_no',$user_mobile_no_with_code)
		                    						
			                    			->Where(function ($query){
										        $query
										        	->where('receiver_id','')
										            ->orWhereNull('receiver_id');

										    })
		                    				->get();
		                    		if(count($ecard)>0)
		                    		{
		                    			foreach($ecard as $card)
		                    			{
		                    				$e_row = ECard::where('id',$card->id)->first();
			                    			$e_row->receiver_id = $user->id;
			                    			// $e_row->status = '1';
			                    			$e_row->save();

			                    			if($card['card_send_date'] == '' || (strtotime($current_date) >= strtotime($card['card_send_date']))){

			                    				ECard::where('id',$card->id)->update(array('status'=>'1','card_received_date'=>date('Y-m-d H:i:s')));


			                    				$usr = User::where('id',$user->id)->first();

								          		$wallet_amount = $usr->wallet_amount+$card->amount;

								          		$usr->wallet_amount = $wallet_amount;
								          		$usr->save();

				                    			$wallet_hist = new WalletCashHistory();
							          			$wallet_hist->user_id = $user->id;
							          			$wallet_hist->trans_ref_id = $card->id;
							          			$wallet_hist->transaction_type = '0';
								          		$wallet_hist->amount = $card->amount;
								          		$wallet_hist->description = 'e-giftcard';
								          		$wallet_hist->transaction_date_time = date('Y-m-d H:i:s');
								          		$wallet_hist->save();
			                    			}
			                    			
		                    			}

		                    		}
			                 		//update user id when gifted user signup or manual booking user signup
				                	$user_mobile_no_with_code = $user->country_code.'-'.$user->mobile_no;

				                	$check_booking = Booking::where(function ($query) use ($user_mobile_no_with_code) {
						          		$query->where(function ($query) use ($user_mobile_no_with_code) {
									        $query->where('is_gift','1')
									            
									        ->where('receiver_mobile_no',$user_mobile_no_with_code);
									           
									    })
									    ->orWhere(function ($query) use ($user_mobile_no_with_code) {
									        $query
									            ->where('db_user_mobile_no',$user_mobile_no_with_code);
									    });
						          	})

						          	
						          	->Where(function ($query){
									        $query
									        	->where('user_id','')
									            ->orWhereNull('user_id');

									    })
									
									->get();
									

				            		if(count($check_booking)>0){
				            			foreach($check_booking as $chk_booking){
				            				$b_row = Booking::where('id',$chk_booking['id'])->first();
				            			
				                			$b_row->user_id = $user->id;
				                			$b_row->save();
							          	}
				            		
				            		}

				            		//cash back when customer use refer code
				            		$given_cashback = Setting::select('referred_cashback')->first();

				                	if($user->refer_code)
				                	{
				                		$reffered_user_id = User::where('share_code',$user->refer_code)->first();
				                		
				                		
				                		if($reffered_user_id){
				                			//check referer can refer only 50 times
				                			$check_refer_count = User::where('refer_code',$user->refer_code)->where('referrer_id',$reffered_user_id->id)->count();
											

				                			if($check_refer_count<=50){
				                				
				                				//cashback entry for referrer(sender)
					                    		$cashback = new CashbackHistory();
								                $cashback->user_id = $reffered_user_id->id;
								                $cashback->type = '1';
								                $cashback->amount =$given_cashback['referred_cashback'];
								                $cashback->referral_user_id =$user->id;
								                $cashback->save();


								                $ref_user = User::where('id',$reffered_user_id->id)->first();

								                if($ref_user->earning!=null && $ref_user->earning!=''){
								                	$ref_user->earning = $ref_user->earning+$given_cashback['referred_cashback'];
								                }else{
								                	$ref_user->earning = $given_cashback['referred_cashback'];
								                }
								                
								                $ref_user->save();

								                $update_user = User::where('id',$user->id)->first();
								                $update_user->referrer_id = $reffered_user_id->id;
								                $update_user->referrer_type = '0';
								
								                $update_user->save();

								               


				                			}else{
				                				// update new user entry

				                				$cashback = new CashbackHistory();
								                $cashback->user_id = $reffered_user_id->id;
								                $cashback->type = '1';
								                $cashback->amount =0;
								                $cashback->referral_user_id =$user->id;
								                $cashback->save();


								                $update_user = User::where('id',$user->id)->first();
								                $update_user->referrer_id = $reffered_user_id->id;
								                $update_user->referrer_type = '0';
								
								                $update_user->save();
				                			}

				           //      			if($reffered_user_id->notification_alert_status == '1'){
				           //      				$message = 'Thank you for referring '.$user->name.' He/She has just signed up with Beutics and you both will receive AED '.$given_cashback['referred_cashback'].' each in your wallet. Congratulations! Keep it going, you can refer upto 50 friends & Earn! Shop smart by earning more';
											    // $message_title = 'Your referral has joined! ';
								          		
									      //     	Notification::saveNotification($reffered_user_id->id,$user->id,'','REFERRAL_JOINED',$message,$message_title,'customer');
				           //      			}
				                			
				                		}

				                		$check_sp_refer_code = SpUser::where('share_code',$user->refer_code)->first();

				                		if($check_sp_refer_code){
				                			// $check_refer_count = User::where('refer_code',$request->refer_code)->count();

				                			// if($check_refer_count<=50){
								                //update user entry

								                $update_user = User::where('id',$user->id)->first();
								                $update_user->referrer_id = $check_sp_refer_code->id;
								                $update_user->referrer_type = '1';
								                $update_user->save();

								        //         if($check_sp_refer_code->notification_alert_status == '1'){
				            //         				$message = $user->name." has signed up on Beutics with your code. Keep sharing with others and it's all yours!";
												    // $message_title = 'Your referral has joined Beutics! ';
									          		
										      //     	Notification::saveNotification($check_sp_refer_code->id,$user->id,'','REFERRAL_JOINED',$message,$message_title,'sp');
				            //         			}

				                			// }
				                		}
				                		$this->cashbackWhenSignup($user->id);

				                	}else{
				                		$this->cashbackWhenSignup($user->id);
				                	}

			                 		if($user->registration_type == 'manual'){

			                 			$data['templete'] = "customer_signup_confirmation";
										$data['name'] = $request->full_name;
										$data['email'] = $request->email;
										$data['subject'] = "Sign up Confirmation";
										$data['token'] = $user->verifyToken;
										send($data);
									}
									else{
				                    	$social = new SocialAccount();
				                    	$social->social_id = $request->social_id;
				                    	$social->social_type = $request->social_type;
				                    	$social->user_id = $user->id;
				                    	$social->save();
				                    }
									Device::manageDeviceIdAndToken($user->id, $data['device_token'], $data['device_type'],$request->header('language'), 'add');
				                    
				                    if($user->email!='' && $user->email!=null){
				                    	$data['templete'] = "welcomeCustomerMail";
										$data['name'] = $user->name;
										$data['email'] = $user->email;
										$data['subject'] = "Welcome Onboard Beutics";
										send($data);
				                    }
				                    
									$token = JWTAuth::fromUser($user);
									$user['token'] = $token;
									$setting = Setting::first();
			                        return response()->json(['status' => true ,'message' => __('messages.Signup Successful'),'data' => $user,'referred_amount'=>$setting->referred_cashback,'contact_sync_amount'=>$setting->contact_sync_cashback]);
			                    }
		          			}else{
		          				return response()->json(['status' => false ,'message' => __('messages.Otp is not valid')]);
		          			}
		          		
		          		}
					}//refer else
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function updateReferCode(Request $request){

		if (Input::isMethod('post')) {
	       try
	       {
		      $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'refer_code' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	          	if($request->refer_code){
	          		$refer_code = strtoupper($request->refer_code);
          			$check_refer_code = User::where('share_code',$refer_code)->count();

          			$check_sp_refer_code = SpUser::where('share_code',$refer_code)->count();
          		}

	          	if(isset($check_refer_code) && $check_refer_code == 0 && isset($check_sp_refer_code) && $check_sp_refer_code == 0) 
          		{
          			return response()->json(['status' => false,'message' => __('messages.Refer code is not valid')]);

				}else{
					//cash back when customer use refer code
					$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
            		$given_cashback = Setting::select('referred_cashback')->first();
                	if($request->refer_code)
                	{
                		User::where('id',$user_id)->update(array('refer_code'=>$request->refer_code));
                		$reffered_user_id = User::where('share_code',$request->refer_code)->first();
                		
                		
                		if($reffered_user_id){
                			//check referer can refer only 50 times
                			$check_refer_count = User::where('refer_code',$request->refer_code)->where('referrer_id',$reffered_user_id->id)->count();
							

                			if($check_refer_count<=50){
                				
                				//cashback entry for referrer(sender)
	                    		$cashback = new CashbackHistory();
				                $cashback->user_id = $reffered_user_id->id;
				                $cashback->type = '1';
				                $cashback->amount =$given_cashback['referred_cashback'];
				                $cashback->referral_user_id =$user_id;
				                $cashback->save();


				                $ref_user = User::where('id',$reffered_user_id->id)->first();

				                if($ref_user->earning!=null && $ref_user->earning!=''){
				                	$ref_user->earning = $ref_user->earning+$given_cashback['referred_cashback'];
				                }else{
				                	$ref_user->earning = $given_cashback['referred_cashback'];
				                }
				                
				                $ref_user->save();

				                $update_user = User::where('id',$user_id)->first();
				                $update_user->referrer_id = $reffered_user_id->id;
				                $update_user->referrer_type = '0';
				
				                $update_user->save();

				               


                			}else{
                				// update new user entry

                				// $cashback = new CashbackHistory();
				                // $cashback->user_id = $reffered_user_id->id;
				                // $cashback->type = '1';
				                // $cashback->amount =0;
				                // $cashback->referral_user_id =$user->id;
				                // $cashback->save();


				                $update_user = User::where('id',$user_id)->first();
				                $update_user->referrer_id = $reffered_user_id->id;
				                $update_user->referrer_type = '0';
				
				                $update_user->save();
                			}

                			
                		}

                		$check_sp_refer_code = SpUser::where('share_code',$request->refer_code)->first();

                		if($check_sp_refer_code){

				                $update_user = User::where('id',$user_id)->first();
				                $update_user->referrer_id = $check_sp_refer_code->id;
				                $update_user->referrer_type = '1';
				                $update_user->save();

                		}
                		// $this->cashbackWhenSignup($user->id);

                	}//main if condition ends
                	return response()->json(['status' => true, 'message' => 'Success', 'data' => []]);

				}//end else
	          }//end outer else
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function getLatLong(Request $request){

		try{
			$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

			$user = User::where('id',$user_id)->first();
			if($user->address ==null && $user->address ==''){

				if( $request->lat){
	        		$user->latitude = $request->lat;
		        }
		        if($request->long){
		        	$user->longitude = $request->long;
		        }
		        
		        if(!$request->address){
		        	if($request->lat && $request->long){
		        		$add = $this->getAddress($request->lat,$request->long);
		        		$user->address = $add;
		        	}
		        	
		        }
		        $user->save();
		        return response()->json(['status' => true, 'message' => 'Successful','data'=>[]]);
			}else{
				return response()->json(['status' => true, 'message' => 'Already Done','data'=>[]]);
			}
			
		}catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage().$e->getLine(), 'data' => []]);
	       }

	}
	   public function getAddress($latitude,$longitude){
		$url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($latitude).','.trim($longitude).'&key=AIzaSyCNVV0WGqduJq4EsX8_Y_s8L-hiZrHmrj4';

		$geocodeFromLatLong = $this->get_content($url); 
		// print_r($geocodeFromLatLong);die;
		$output = json_decode($geocodeFromLatLong);


		$status = $output->status;

		$address = ($status=="OK")?$output->results[0]->formatted_address:'';
		return $address;

		//==================

		// $latitude = '26.4577845';
		// $longitude = '74.59509';
		// $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($latitude).','.trim($longitude).'&key=AIzaSyCNVV0WGqduJq4EsX8_Y_s8L-hiZrHmrj4';
	 //    $json = file_get_contents($url);
	 //    $data = json_decode($json);
	 //    $status = $data->status;
	   
	 //    if($status=="OK") {
	 //        //Get address from json data
	 //        for ($j=0;$j<count($data->results[0]->address_components);$j++) {
	 //            $cn=array($data->results[0]->address_components[$j]->types[0]);
	 //            // print_r($cn);die;
	 //            // if(in_array("locality", $cn)) {
	 //                $city[] = $data->results[0]->address_components[$j]->long_name;
	 //            // }
	 //        }
	 //     } else{
	 //       echo 'Location Not Found';
	 //     }
	 //     //Print city 
	 //     print_r($city);
    }
	public function sendSignupNotification(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
	       	  $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'user_id' => 'required',
	            ]);

	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	            $user = User::where('id',$request->user_id)->first();
	            if($user){
					//voucher secured notification for limited time
								
					if($user->notification_alert_status == '1'){

						$message_main = __('messages.Voucher Secured',['name' => $user->name]);
					    $message_title_main = __('messages.Welcome to Onboard');

					    app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');


						$message_other = __('messages.Voucher Secured',['name' => $user->name]);
					    $message_title_other = __('messages.Welcome to Onboard');
		          		
			          	Notification::saveNotification($user->id,'','','VOUCHER_SECURED',$message_main,$message_other,$message_title_main,$message_title_other,'customer');

			          	app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');


					}

					//update user id when gifted user signup or manual booking user signup
                	$user_mobile_no_with_code = $user->country_code.'-'.$user->mobile_no;

                	$check_booking = Booking::where(function ($query) use ($user_mobile_no_with_code) {
		          		$query->where(function ($query) use ($user_mobile_no_with_code) {
					        $query->where('is_gift','1')
					            
					        ->where('receiver_mobile_no',$user_mobile_no_with_code);
					           
					    })
					    ->orWhere(function ($query) use ($user_mobile_no_with_code) {
					        $query
					            ->where('db_user_mobile_no',$user_mobile_no_with_code);
					    });
		          	})

		          	->whereNotNull('user_id')					
					->get();
					

            		if(count($check_booking)>0){
            			foreach($check_booking as $chk_booking){
            				// $b_row = Booking::where('id',$chk_booking['id'])->first();
            			
                // 			$b_row->user_id = $user->id;
                // 			$b_row->save();
            			
            			

            			// receiver information

                			$gift_sender_info = User::where('id',$chk_booking['gifted_by'])->first();
			          		if($chk_booking['user_id']!='' && $chk_booking['user_id']!=null && $chk_booking['gift_send_date'] == null && $chk_booking['is_gift']=='1' && $user->notification_alert_status == '1'){
			          			
				          		// $message = 'Your friend '.$gift_sender_info->name.' has sent you a gift. You are certainly special and your friendship. Cherish it! The Gift has been placed in “New Bookings” section.';
				          		$message_main = __('messages.You have received a Gift',['name' => $gift_sender_info->name]);

				          		$message_title_main = __('messages.You have received a Gift!');

				          		app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');


								$message_other = __('messages.You have received a Gift',['name' => $gift_sender_info->name]);

				          		$message_title_other = __('messages.You have received a Gift!');

				          		// $db_message = 'You have received a gift from ';
						        Notification::saveNotification($chk_booking['user_id'],$chk_booking['sp_id'],$chk_booking['id'],'GIFT_RECEIVED',$message_main,$message_other,$message_title_main,$message_title_other,'customer');
						        app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');


			          		}
			          	}
            		
            		}

            		//ecard notification
            		$current_date = date('Y-m-d');
            		$ecard = ECard::where('receiver_mobile_no',$user_mobile_no_with_code)
            						
                			->Where(function ($query){
						        $query
						        	// ->where('receiver_id','')
						            ->whereNotNull('receiver_id');

						    })
            				->get();
            		if(count($ecard)>0)
            		{
            			foreach($ecard as $card)
            			{
            				// $e_row = ECard::where('id',$card->id)->first();
                // 			$e_row->receiver_id = $user->id;
                // 			// $e_row->status = '1';
                // 			$e_row->save();

                			if(($card['card_send_date'] == '' || (strtotime($current_date) >= strtotime($card['card_send_date']))) && $card->getAssociatedReceiverInfo->notification_alert_status == '1' ){

                				// if($card->receiver_id!='' && $card->receiver_id!=null && $request->is_send_now==1 && $card->getAssociatedReceiverInfo->notification_alert_status == '1'){

					          		// $message = 'Your friend '.$sender_info->name.' has remembered your '.$row->getAssociatedOccasionInfo->name.'. He has sent you a Gift Cash Card. You have a special friendship! Your wallet has been topped up with the cash card. Enjoy shopping';

					          		$message_main = __('messages.You have received a Gift Card',['sender_name' => $card->getAssociatedUserInfo->name,'occasion_name' => $card->getAssociatedOccasionInfo->name]);
					          		// $db_message = 'You have received a e-gift card from ';

					          		$message_title_main = __('messages.You have received a Gift Card!');

					          		app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');


									$message_other = __('messages.You have received a Gift Card',['sender_name' => $card->getAssociatedUserInfo->name,'occasion_name' => $card->getAssociatedOccasionInfo->name]);
					          		$message_title_other = __('messages.You have received a Gift Card!');


							        Notification::saveNotification($card['receiver_id'],$card['user_id'],$card['id'],'E_GIFT_CARD_RECEIVED',$message_main,$message_other,$message_title_main,$message_title_other,'customer');

							        app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');


				          		// }
                			}
                			
            			}

            		}

            		//-------------
            		//cash back when customer use refer code
            		$given_cashback = Setting::select('referred_cashback')->first();
                	if($user->refer_code)
                	{
                		$reffered_user_id = User::where('share_code',$user->refer_code)->first();
                		

                		if($reffered_user_id){
                			$check_refer_count = User::where('refer_code',$user->refer_code)->where('referrer_id',$reffered_user_id->id)->count();
                			if($check_refer_count<=50){
                				$message_main = __('messages.Your referral has joined',['name' => $user->name,'cashback' => $given_cashback['referred_cashback']]);
							    
                			}else{
                				$message_main = __('messages.Your referral has joined exceed 50',['name' => $user->name]);
                			}
                			$message_title_main = __('messages.Your referral has joined!');

                			app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

							
							if($check_refer_count<=50){
                				$message_other = __('messages.Your referral has joined',['name' => $user->name,'cashback' => $given_cashback['referred_cashback']]);
							    
                			}else{
                				$message_other = __('messages.Your referral has joined exceed 50',['name' => $user->name]);
                			}
							$message_title_other = __('messages.Your referral has joined!');
                		
                			if($reffered_user_id->notification_alert_status == '1'){
							    
				          		
					          	Notification::saveNotification($reffered_user_id->id,$user->id,'','REFERRAL_JOINED',$message_main,$message_other,$message_title_main,$message_title_other,'customer');

					          	app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

                			}
                			
                		}

                		$check_sp_refer_code = SpUser::where('share_code',$user->refer_code)->first();

                		if($check_sp_refer_code){
                			// $check_refer_count = User::where('refer_code',$request->refer_code)->count();

				                if($check_sp_refer_code->notification_alert_status == '1'){
                    				// $message = $user->name." has signed up on Beutics with your code. Keep sharing with others and it's all yours!";

                    				$message_main = __('messages.Your referral has joined Beutics',['name' => $user->name]);

								    $message_title_main = __('messages.Your referral has joined Beutics!');

									app()->getLocale() == 'en' ? app()->setLocale('ar'):app()->setLocale('en');

									$message_other = __('messages.Your referral has joined Beutics',['name' => $user->name]);

								    $message_title_other = __('messages.Your referral has joined Beutics!');

					          		
						          	Notification::saveNotification($check_sp_refer_code->id,$user->id,'','REFERRAL_JOINED',$message_main,$message_other,$message_title_main,$message_title_other,'sp');
                    			}

                			// }
                		}
                		

                	}
                	return response()->json(['status' => true]);

				}//end of user condition    	
	          }

	       }catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage().$e->getLine(), 'data' => []]);
	       }
   		}
	}

	public function cashbackWhenSignup($userid)
	{
		$given_cashback = Setting::select('referred_cashback')->first();
		$cashback = new CashbackHistory();
        $cashback->user_id = $userid;
        $cashback->type = '2';
        $cashback->amount = $given_cashback['referred_cashback'];
        $cashback->save();

        $update_signup_reward = User::where('id',$userid)->first();
        $total_earning = $update_signup_reward->earning+$given_cashback['referred_cashback'];
        $update_signup_reward->earning = $total_earning;
        $update_signup_reward->save();
        return;
	}
	public function generateUniqueShareCode()
	{
		// $unique_id = 'BEU'.rand(1000,100);
		$unique_id = substr(str_shuffle(str_repeat("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", 6)), 0, 6);
        
		 $check_no = User::where('share_code',$unique_id)->exists();
      	 if($check_no){
      		$unique_id = $this->generateUniqueShareCode();
      	 }
      	 return $unique_id;
	}
	
	public function resendEmail(){
		$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
    	$user = User::whereId($user_id)->first();
    	if($user)
    	{
    		$data['templete'] = "confirmation_email";
	        $data['name'] = $user->name;
	        $data['token'] = $user->verifyToken;
	        $data['email'] = $user->email;
	        $data['subject'] = "Confirmation Email";
	        send($data);
	        return response()->json(['status' => true ,'message' => __('messages.Mail Sent Successfully')]);
    	}
		
	}

	public function verifyOtp($otp,$mobileNoWithCode){
		$row = OtpNumber::where('complete_mobile_no',$mobileNoWithCode)->where('otp',$otp)->count();
		if($row > 0){
			OtpNumber::where('complete_mobile_no',$mobileNoWithCode)->where('otp',$otp)->delete();
			return true;
		}else{
			return false;
		}
	}

	public function login(Request $request)
    {
    	if (Input::isMethod('post')) {
	       try
	       {
	       	  $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'country_code' => 'required|exists:countries,phonecode',
	              'mobile_no' => 'required|required|regex:/^([0-9\s\-\+\(\)]*)$/|max:16|min:8',
	              'password' => 'required|min:8|max:50',
	              'device_type' => 'required|in:IPHONE,ANDROID,IOS',
	              'device_token' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	            $user = User::where(['mobile_no' => $data['mobile_no'],'country_code' => $data['country_code']])->first();
	            if($user){
					if($user->status == 1)
					{
						if (Hash::check($data['password'], $user->password)){

							$token = JWTAuth::fromUser($user);
							$user_id = $user->id;
							Device::manageDeviceIdAndToken($user->id, $data['device_token'], $data['device_type'],$request->header('language'), 'add');
							$rows = $this->showUserDetails($user);
							$rows['token'] = $token;

							$setting = Setting::first();

							return response()->json(['status' => true, 'message' => __('messages.Login successful'),'data' => $rows,'referred_amount'=>$setting->referred_cashback,'contact_sync_amount'=>$setting->contact_sync_cashback]);
						}
						else
						{
							return response()->json(['status' => false, 'message' => __('messages.Invalid Password'),'data' => []]);
						} 
					}
					else
					{
						$error_message = __('messages.Your account has been deactivated');
						return response()->json(['status'=>false,'message'=>$error_message]);
					}
				}else{
		         	return response()->json(['status' => false, 'message' =>  __('messages.Mobile No. is not registered with Beutics. Please Register.'),'data' => []]);
		         }     	
	          }

	       }catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage().$e->getLine(), 'data' => []]);
	       }
   		}
    }

    public function showUserDetails($row)
	{
		$data = array();
		$data['id'] = $row['id'];
		$data['name'] = $row['name'];
		$data['email'] = $row['email'];
		$data['country_code'] = $row['country_code'];
		$data['mobile_no'] = $row['mobile_no'];
		$data['registration_type'] = $row['registration_type'];
		$data['refer_code'] = $row['refer_code'];
		$data['share_code'] = $row['share_code'];
		$data['gender'] = $row['gender'];
		$data['dob'] = date('d-m-Y',strtotime($row['dob']));
		$data['image'] = $row['image'];
		$data['verifyToken'] = $row['verifyToken'];
		$data['isPassword'] = $row['isPassword'];
		$data['notification_setting'] = $row['notification_alert_status'];
		$data['is_arabic_active'] = $row['is_arabic_selected'];
		return $data;
	}

	public function logout(Request $request)
    {
    	if (Input::isMethod('post')) {
    	   try
	       {
	    		$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'device_type' => 'required|in:IPHONE,ANDROID,IOS',
		              'device_token' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error, 'data' => []]);
		          } 
		          else 
		          {
		          	$token = JWTAuth::getToken();
		          	if($token)
		          	{
		          		$user_id = $user_id =  JWTAuth::toUser(JWTAuth::getToken())->id;		          				          				          	
				        Device::manageDeviceIdAndToken($user_id, $data['device_token'], $data['device_type'],$request->header('language'), 'remove');
				        JWTAuth::setToken($token)->invalidate();

		        		return response()->json(['status' => true, 'message' => __('messages.Successfully Logged Out')]);
		          	}
		          	
		          }
	       }
	       catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
    	}
    	 
    }

    public function changePassword(Request $request)
    {
    	if (Input::isMethod('post')) {
    		try{
    			$validator = Validator::make($request->all(), [
                  //  'old_password'=>'required|min:8|max:50',
                    'new_password'=>'required||min:8|max:50'  
                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{

	        		$user_id = $user_id =  JWTAuth::toUser(JWTAuth::getToken())->id;
		        	$row = User::whereId($user_id)->first();
		        	if($row)
		        	{
		        		if($request->old_password){
		        			if (Hash::check($request->old_password, $row->password)) 
				            {
				                $row->password = bcrypt($request->new_password);
				                $row->save();
				                return response()->json(['status'=>true,'message'=>__('messages.Password Updated Successfully')]);
				            }
				            else
				            {
				            	
				                return response()->json(['status'=>false,'message'=>__('messages.Old password is not correct.')]);
				            }
		        		}else{
		        			$row->password = bcrypt($request->new_password);
		        			$row->isPassword = 1;
			                $row->save();
			                return response()->json(['status'=>true,'message'=>__('messages.Password Updated Successfully')]);
		        		}
		        		
		        	}else{
		        		return response()->json(['status'=>false,'message'=>__('messages.deactivate')]);
		        	}
		            
		        }
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
    	}
    }

    public function editProfile(Request $request)
    {
    	if (Input::isMethod('post')) 
    	{
    		try
    		{
				$user_id = $user_id =  JWTAuth::toUser(JWTAuth::getToken())->id;
    			$validator = Validator::make($request->all(), [
    				'full_name' => 'required',
    				'email' => 'nullable|email|unique:users,email,'.$user_id,
    				'country_code' => 'required',
    				'mobile_no' => 'required|required|regex:/^([0-9\s\-\+\(\)]*)$/|max:16|min:8',
    				'gender' => 'required|in:Male,Female,Other',
    				'dob' => 'required',
    				'image' => 'mimes:jpeg,jpg,png,gif',

                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{
					$email_check = 0;
		        	$country_row = Country::where('phonecode',$request->country_code)->first();
		          	if($country_row)
		          	{
		          		$user_id = $user_id =  JWTAuth::toUser(JWTAuth::getToken())->id;
			        	$user = User::whereId($user_id)->first();
			        	if($user)
			        	{							
			        		$user->name = $request->full_name;
							
			        		if($request->email && $request->email!= $user->email){
			        			$user->verifyToken = strtotime(date('Y-m-d H:i:s')).rand(1111,999999);
			        			$user->email = $request->email;
			        			$email_check = 1;
			        			
			        		}elseif($request->email){
			        			$user->email = $request->email;
			        		}else{
			        			$user->email = null;
			        		}
			        		$user->country_code = $request->country_code;
			        		$user->mobile_no = $request->mobile_no;
			        		$user->gender = $request->gender;
			        		$user->dob = date('Y-m-d',strtotime($request->dob));

			        		if ($request->hasFile('image') && $request->file('image')) {

			                    $file = $request->file('image');
			                    $name = time().'.'.$file->getClientOriginalExtension();
			                    $destinationPath = public_path('uploads/');
			                    $img = $file->move($destinationPath, $name);

			                    $user->image = 'uploads/'.$name;
			                } 
			        		$user->save();
			        		if($user->save() && $email_check == 1)
			        		{
			        			$data['templete'] = "confirmation_email";
						        $data['name'] = $user->name;
						        $data['token'] = $user->verifyToken;
						        $data['email'] = $user->email;
						        $data['subject'] = "Confirmation Email";
						        send($data);
			        		}
			        		$data = $this->showUserDetails($user);
			        		return response()->json(['status' => true, 'message' => __('messages.Profile Updated Successfully'), 'data' => $data]);
			        	}else{
			        		return response()->json(['status'=>false,'message'=>__('messages.deactivate')]);
			        	}
			        }else{
			        	return response()->json(['status' => true, 'message' => __('messages.Country Code is Invalid')]);
			        }
		        	
		        }
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }

    	}
    }

    public function checkSocial(Request $request)
    {

    	if (Input::isMethod('post')) 
    	{
    		try
    		{
    			$validator = Validator::make($request->all(), [
    				//'email' => 'email',
    				'social_id' => 'required',
    				'social_type' => 'required',
    				'device_type' => 'required|in:IPHONE,ANDROID,IOS',
		            'device_token' => 'required',

                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{
		        	$row = SocialAccount::where(['social_id' => $request->social_id,'social_type' => $request->social_type])->first();
		        	// print_r($row);die;

		        	if($row){
		        		$user = User::where('id',$row->user_id)->first();
		        		if($user)
		        		{
		        			$token = JWTAuth::fromUser($user);
		        			$userdata = $this->showUserDetails($user);
		        			$userdata['token'] = $token;
		        			Device::manageDeviceIdAndToken($user->id, $request->device_token, $request->device_type,$request->header('language'), 'add');
		        			
		        			return response()->json(['status'=>true,'exist_status'=> 1,'is_first'=> 0,'data'=> $userdata]);
		        		}else{
		        			return response()->json(['status'=>true,'exist_status'=> 0,'message'=>__('messages.User not exists')]);
		        		}
		        	}elseif($request->email && $request->social_type != 'apple')
		        	{
		        		$user = User::where('email',$request->email)->first();
		        		if($user)
		        		{
		        			$token = JWTAuth::fromUser($user);
		        			$userdata = $this->showUserDetails($user);
		        			$userdata['token'] = $token;
		        			Device::manageDeviceIdAndToken($user->id, $request->device_token, $request->device_type,$request->header('language'), 'add');
		        			
		        			return response()->json(['status'=>true,'exist_status'=> 1,'data'=> $userdata]);
		        		}else{
		        			return response()->json(['status'=>true,'exist_status'=> 0,'message'=>__('messages.Email is not valid')]);
		        		}
		        	}elseif($request->social_type == 'apple')
		        	{
		        		// echo 'in'.$request->email;die;
		        		$user = array();
		        		if($request->has('email') && $request->get('email') != ''){
		        			$user = User::where('email',$request->email)->first();
		        		}

		        		if(!$user)
		        		{

		        			$user = new User();
	        			    if($request->has('email') && $request->get('email') != ''){
                                $user->email = $request->email;
                            }
                            if($request->has('name') && $request->get('name') != ''){
                                $user->name = $request->name;
                            }
                            $user->share_code = $this->generateUniqueShareCode();
                            $user->status = '1';  
                            $user->registration_type = 'apple';
                            if($user->save()){

                            	$social = new SocialAccount();
		                    	$social->social_id = $request->social_id;
		                    	$social->social_type = $request->social_type;
		                    	$social->user_id = $user->id;
		                    	$social->save();

		                    	$this->cashbackWhenSignup($user->id);

	                    		if($user->email!='' && $user->email!=null){
			                    	$data['templete'] = "welcomeCustomerMail";
									$data['name'] = $user->name;
									$data['email'] = $user->email;
									$data['subject'] = "Welcome Onboard Beutics";
									send($data);
			                    }



                            	$token = JWTAuth::fromUser($user);
			        			$userdata = $this->showUserDetails($user);
			        			$userdata['token'] = $token;
			        			Device::manageDeviceIdAndToken($user->id, $request->device_token, $request->device_type,$request->header('language'), 'add');
			        			$setting = Setting::first();
			        			return response()->json(['status'=>true,'exist_status'=> 1,'is_first'=> 1,'data'=> $userdata,'contact_sync_amount'=>$setting->contact_sync_cashback]);

                            }
		        			
		        		}else{
		        			// $social = new SocialAccount();
	            //         	$social->social_id = $request->social_id;
	            //         	$social->social_type = $request->social_type;
	            //         	$social->user_id = $user->id;
	            //         	$social->save();


		        			$token = JWTAuth::fromUser($user);
		        			$userdata = $this->showUserDetails($user);
		        			$userdata['token'] = $token;
		        			Device::manageDeviceIdAndToken($user->id, $request->device_token, $request->device_type,$request->header('language'), 'add');
		        			
		        			return response()->json(['status'=>true,'exist_status'=> 1,'is_first'=> 0,'data'=> $userdata]);
		        		}
		        	}
		        	else{
		        		return response()->json(['status'=>true,'exist_status'=> 0,'message'=>__('messages.Account does not exists')]);
		        	}

		        }
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }

    	}
    }
    
    public function updateMobile(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'country_code' => 'required|exists:countries,phonecode',
		              'mobile_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:16|min:8',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {
	          		$user = User::where('mobile_no',$request->mobile_no)->where('country_code',$request->country_code)->first();
	          		
	          		if($user)
	          		{
	          			return response()->json(['status' => false,'message' => __('messages.Mobile number already exist')]);
	          		}
	          		else{
	          			$mobileNoWithCode = $request->country_code . $request->mobile_no;
	          			$otp = $this->getOpt($mobileNoWithCode);     
                		return response()->json(['status' => true,'otp' => $otp,'message' =>  __('messages.OTP Sent Successfully')]);           		
	          		}
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}
	
	public function userDetails(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'userid' => 'required|exists:users,id',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {
	          		$user = User::where('id',$request->userid)->first();
	          		
	          		if($user)
	          		{
						$userdata = $this->showUserDetails($user);
	          			return response()->json(['status' => true,'message' => 'User Details','data' => $userdata]);
	          		}
	          		else{
                		return response()->json(['status' => false,'message' => 'Record not Found']);           		
	          		}
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}
	
	public function verifyOtpApi(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'country_code' => 'required|exists:countries,phonecode',
		              'mobile_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:16|min:8',
		              'otp' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {
					$mobileNoWithCode = $request->country_code . $request->mobile_no;
	          		$row = OtpNumber::where('complete_mobile_no',$mobileNoWithCode)->where('otp',$request->otp)->count();
					if($row > 0){
						OtpNumber::where('complete_mobile_no',$mobileNoWithCode)->where('otp',$request->otp)->delete();
						return response()->json(['status' => true]);
					}else{
						return response()->json(['status' => false]);
					}
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}
	
	

}
