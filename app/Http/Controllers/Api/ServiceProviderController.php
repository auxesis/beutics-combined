<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use JWTAuth;
use JWTAuthException;

use App\Http\Controllers\Controller;
use App\Model\SpUser;
use App\Model\OtpNumber;
use App\Model\Country;
use App\Model\SpDevice;
use App\Model\Amenity;
use App\Model\SpAmenity;
use App\Model\Category;
use App\Model\ShopTiming;
use App\Model\SpBannerImages;
use App\Model\Tier;
use App\Model\Setting;
use App\Model\SpCountry;
use Config;


class ServiceProviderController extends Controller
{
	function __construct(Request $request)
	{
		parent::__construct($request);
	    Config::set('jwt.user', SpUser::class);
	    Config::set('auth.providers', ['users' => [
	            'driver' => 'eloquent',
	            'model' => SpUser::class,
	        ]]);

	    $this->col_postfix = '';

        $this->header = $request->header('language');
        if($this->header!= "en"){
        	$this->col_postfix = '_ar';
        }
	}
	public function getCountryList()
	{
		try
	    {
	    	$rows = Country::get();
	    	if($rows){

		    	foreach($rows as $row)
				{
					$countries[] = [
						'id'=>$row->id,
						'country_code'=>$row->sortname,
						'country_name'=>$row->name,
						'nationality'=>$row->nationality,
						'phone_code'=>$row->phonecode,
						'flag_url' => asset('public/sp_uploads/flags/'.$row['flag_url'].'.svg')
						];
				}
				return response()->json(['status' => true, 'message' => 'Country Code List','data'=>$countries]);
			}else{
				return response()->json(['status' => true, 'message' => 'No Record Found','data'=>[]]);
			}
	    }
	    catch(\Exception $e){
	       	return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	    }
	}

	public function getAmenities()
	{
		try
	    {
	    	$rows = Amenity::get();
	    	if($rows){

	    		$col = 'name'.$this->col_postfix;
		    	foreach($rows as $row)
				{
					$amenities[] = [
						'id'=>$row->id,
						'amenity_name'=>$row->$col,
						];
				}
				return response()->json(['status' => true, 'message' => 'Amenities List','data'=>$amenities]);
			}else{
				return response()->json(['status' => true, 'message' => 'No Record Found','data'=>[]]);
			}
	    }
	    catch(\Exception $e){
	       	return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	    }
	}

	public function getCategories()
	{
		try
	    {
	    	$rows = Category::where('category_name','!=','bridal')->where('parent_id','0')->get();
	    	if($rows){
	    		$col = 'category_name'.$this->col_postfix;
		    	foreach($rows as $row)
				{
					$categories[] = [
						'id'=>$row->id,
						'category_name'=>$row->$col,
						];
				}
				return response()->json(['status' => true, 'message' => 'Categories List','data'=>$categories]);
			}else{
				return response()->json(['status' => true, 'message' => 'No Record Found','data'=>[]]);
			}
	    }
	    catch(\Exception $e){
	       	return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	    }
	}

	public function getSubCategories()
	{
		try
	    {
	    	$rows = Category::where('category_name','!=','bridal')->where('parent_id','!=','0')->get();
	    	if($rows){
		    	foreach($rows as $row)
				{
					$categories[] = [
						'id'=>$row->id,
						'category_name'=>$row->category_name,
						];
				}
				return response()->json(['status' => true, 'message' => 'Sub Categories List','data'=>$categories]);
			}else{
				return response()->json(['status' => true, 'message' => 'No Record Found','data'=>[]]);
			}
	    }
	    catch(\Exception $e){
	       	return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	    }
	}
	public function registerOtp(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'store_type' => 'required',
		              'category_id' => 'required|exists:categories,id',
		              'store_name' => 'required',
		              'store_number' => 'required',
		              'username' => 'required',
		              'address' => 'required',
		              'lat' => 'required',
		              'long' => 'required',
		              'landmark' => 'required',
		              'email' => 'required|email|unique:sp_users',
		              'country_code' => 'required|exists:countries,phonecode',
		              'mobile_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:16|min:8',
		              'whatsapp_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:16|min:8',
		              'password' => 'required|min:8|max:50',
		              'confirm_password' => 'required|min:8|max:50|same:password',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {
	          		$user = SpUser::where('mobile_no',$request->mobile_no)->where('country_code',$request->country_code)->count();
	          		
	          		if($user > 0)
	          		{
	          			return response()->json(['status' => false,'message' => __('messages.Mobile number already exist')]);
	          		}else{
						
	          			$otp = $this->getOpt($request->country_code . $request->mobile_no);

	     //      			$data['templete'] = "sp_verify_account_mail";
						// $data['name'] = $request->store_name;
						// $data['email'] = $request->email;
						// // $data['mobile_no'] = '+'.$request->country_code.substr($request->mobile_no, 0, 2).substr($request->mobile_no, -1, 2);
						// $data['mobile_no'] = '+'.$request->country_code.$request->mobile_no;
						// $data['subject'] = "Verify your Account";
						// // $data['token'] = $user->verifyToken;
						// $data['otp'] = $otp;
						// send($data);

	          			return response()->json(['status' => true,'otp' => $otp,'message' => __('messages.OTP Sent Successfully')]);
	          		
	          		}
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function getOpt($mobileNoWithCode)
	{
		$row = OtpNumber::where('complete_mobile_no',$mobileNoWithCode)->first();
		if($row){

			$otp = $row->otp;
		}
		else{			
			$otp = mt_rand(100000, 999999);
			// $otp = 12345;
		}

		$username =  env('SMS_COUNTRY_USERNAME');
		$password =  env('SMS_COUNTRY_PASSWORD');
		$message ="Your one time password for registration is ".$otp." don't share this with anyone."; 
		$parami =['User'=>$username,'passwd'=>$password,'mobilenumber'=>$mobileNoWithCode,'message'=>$message,'sid'=>'smscntry','mtype'=>'N'];
		$ponmo = http_build_query($parami);
		$url = "http://api.smscountry.com/SMSCwebservice_bulk.aspx?$ponmo"; // json
		$res = $this->get_content($url);
		if(!$row){
	        $new_row = new OtpNumber();
	      	$new_row->complete_mobile_no = $mobileNoWithCode;
	      	$new_row->otp = $otp;     
	        $new_row->save();
    	}
		return $otp;		
	}


	function get_content($URL)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $URL);
		$data = curl_exec($ch);
		// curl_exec($ch);
		curl_close($ch);
		return $data;
	}

	public function registerUser(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	$validator = Validator::make($data, 
		            [
		              'store_type' => 'required',
		              'category_id' => 'required|exists:categories,id',
		              'store_name' => 'required',
		              'store_number' => 'required',
		              'username' => 'required',
		              'address' => 'required',
		              'lat' => 'required',
		              'long' => 'required',
		              'landmark' => 'required',
		              'email' => 'required|email|unique:sp_users',
		              'country_code' => 'required|exists:countries,phonecode',
		              'mobile_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:16|min:8',
		              'whatsapp_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:16|min:8',
		              'password' => 'required|min:8|max:50',
		              'confirm_password' => 'required|min:8|max:50|same:password',
		              'device_type' => 'required|in:IPHONE,ANDROID,IOS',
		              'device_token' => 'required',
		              'is_individual' => 'required',
		              'country' => 'required',
		              'delivery_mode'=>'required', // online,store,home,
		              'sub_locality' => 'required'
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {
		          	$user = SpUser::where('mobile_no',$request->mobile_no)->where('country_code',$request->country_code)->count();
		          	$country_row = Country::where('phonecode',$request->country_code)->first();
	          		if($user > 0)
	          		{
	          			return response()->json(['status' => false,'message' =>  __('messages.Mobile number already exist')]);
	          		}else{

          				$user = new SpUser();
			          	$user->name = $request->username;
			          	$user->category_id = $request->category_id;
			          	$user->country_id = $country_row->id;
		          		$user->country_code = $country_row->phonecode;
		          		$user->email = $request->email;  
		          		$user->password = bcrypt($request->password);
	                    $user->mobile_no = $request->mobile_no;
	                    $user->whatsapp_no = $request->whatsapp_no;
	                    $user->store_type = $request->store_type;
	                    $user->store_name = $request->store_name;
	                    $user->store_number = $request->store_number;
	                    $user->address = $request->address;
	                    $user->latitude = $request->lat;
	                    $user->longitude = $request->long;
	                    $user->landmark = $request->landmark;
	                    $user->verifyToken = strtotime(date('Y-m-d H:i:s')).rand(1111,999999);
	                    $user->share_code = $this->generateUniqueShareCode();
	                    $user->delivery_mode = $request->delivery_mode;
		                $user->sub_locality = $request->sub_locality;

	                    if($request->store_type == "store"){
	                    	$user->is_individual = 0;
	                    }
	                    if($request->store_type == "individual"){
	                    	$user->is_individual = 1;
	                    } 
	                    $user->save();
	     
	                    if($user->save())
	                    {
	                    	if($request->country)
	                    	{ 
	                    		SpCountry::manageSpCountries($user->id, $request->country,$request->state,$request->city);
	                    	}

	                 		SpDevice::manageDeviceIdAndToken($user->id, $data['device_token'], $data['device_type'],$request->header('language'), 'add');


	                 			$data['templete'] = "sp_confirmation_email";
								$data['name'] = $user->name;
								$data['token'] = $user->verifyToken;
								$data['email'] = $user->email;
								$data['subject'] = "Confirmation Email";
								send($data);
								//function for storing shop opening and closing time
								$this->manageShopTimings($user->id);

								//mail to admin
								$data1['templete'] = "admin_mail";
								$data1['email'] = env("CONTACT_US_EMAIL");
								$data1['subject'] = "New Sp Registration";
								$data1['message'] = "A new Service Provider ".$user->store_name." has requested to join the platform. Kindly review and action the registration.";
								send($data1);

	                        return response()->json(['status' => true ,'message' => __('messages.registration successful'),'data' => $user]);
	                    }
	          		
	          		}
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function generateUniqueShareCode()
	{
		// $unique_id = 'SP'.rand(1000,100);
		$unique_id = $unique_id = substr(str_shuffle(str_repeat("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", 6)), 0, 6);
		 $check_no = SpUser::where('share_code',$unique_id)->exists();
      	 if($check_no){
      		$unique_id = $this->generateUniqueShareCode();
      	 }
      	 return $unique_id;
	}


	public function manageShopTimings($userid)
	{		
		for($i=1;$i<=7;$i++){
			$row = new ShopTiming();
			$row->user_id = $userid;
			$row->day = $i;
			$row->opening_time = '00:00';
			$row->closing_time = '00:00';
			$row->save();

		}
	}

	public function login(Request $request)
    {
    	
    	if (Input::isMethod('post')) {
   //  		Config::set('jwt.user', 'App\Model\SpUser'); 
			// Config::set('auth.providers.users.model', \App\Model\SpUser::class);
	       try
	       {
	       	  $data = $request->all();
	       	  $validator = Validator::make($data, 
	            [
	              'country_code' => 'required|exists:countries,phonecode',
	              'mobile_no' => 'required|required|regex:/^([0-9\s\-\+\(\)]*)$/|max:16|min:8',
	              'password' => 'required',
	              'device_type' => 'required|in:IPHONE,ANDROID,IOS',
	              'device_token' => 'required',
	            ]);
	       	  if ($validator->fails()) 
	          {
	          	
	            $error = $this->validationHandle($validator->messages());
	            return response()->json(['status' => false, 'message' => $error]);
	          } 
	          else 
	          {
	            $user = SpUser::where(['mobile_no' => $data['mobile_no'],'country_code' => $data['country_code']])->first();
	          
	            if($user){
	            	if($user->is_approved!= 1){

						return response()->json(['status'=>false,'message'=>__('messages.Your application is under review. Thank you for your patience.')]);
					}
	            	elseif($user->is_approved==1 && $user->status==0)
					{
						return response()->json(['status'=>false,'message'=>__('messages.Your account has been deactivated')]);
					}
					else{
						if($user->is_approved == 1)
						{
							if (Hash::check($data['password'], $user->password)){

								$token = JWTAuth::fromUser($user);
								$user_id = $user->id;
								
								SpDevice::manageDeviceIdAndToken($user->id, $data['device_token'], $data['device_type'],$request->header('language'),'add');
								$rows = $this->showUserDetails($user);
								$rows['token'] = $token;

								return response()->json(['status' => true, 'message' => __('messages.Login successful'),'data' => $rows]);
							}
							else
							{
								return response()->json(['status' => false, 'message' => __('messages.Invalid Password'),'data' => []]);
							} 
						}else{
							$error_message = __('messages.Your account is not approved by admin.');
							return response()->json(['status'=>false,'message'=>$error_message]);
						}
					}
				
				}else{
		         	return response()->json(['status' => false, 'message' => __('messages.Invalid Credentials'),'data' => []]);
		         }     	
	          }

	       }catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
   		}
    }

    public function showUserDetails($row)
	{
		$data = array();
		$data['id'] = $row['id'];
		$data['name'] = $row['name'];
		$data['category_name'] = $row->getAssociatedCategoryName->category_name;
		$data['area_description'] = $row['area_description']!=null ? $row['area_description']:'';
		$data['email'] = $row['email'];
		$data['country_code'] = $row['country_code'];
		$data['mobile_no'] = $row['mobile_no'];
		$data['whatsapp_no'] = $row['whatsapp_no'];
		$data['store_name'] = $row['store_name'];
		if($this->header == 'en'){
			$store = 'store';
			$individual = 'individual';
		}else{
			$store = 'متجر';
			$individual = 'فرد';
		}
		$data['store_type'] = $row['store_type']=='store'?$store:$individual;
		$data['store_number'] = $row['store_number'];
		$data['address'] = $row['address'];
		$data['landmark'] = $row['landmark'];
		$data['latitude'] = $row['latitude'];
		$data['longitude'] = $row['longitude'];
		$data['is_individual'] = $row['is_individual'];
		$data['verifyToken'] = $row['verifyToken'];
		$data['share_code'] = $row['share_code'];
		$data['notification_alert_status'] = $row['notification_alert_status'];
		$data['is_provide_home_service'] = $row['is_provide_home_service'];
		$data['mov'] = $row['mov'];
		if($row['profile_image']){
			$data['profile_image'] = changeImageUrlForFileExist(asset('sp_uploads/profile/'.$row['profile_image']));
		}else{
			$data['profile_image'] = '';
		}
		
		$data['description'] = $row['description'];
		$data['service_criteria'] = $row['service_criteria'];
		$data['shop_timings'] = array();
		foreach($row->userShopTiming as $time){
				$data['shop_timings'][] = $time;
		}

		$data['banner_images'] = array();
		// echo "<pre/>";print_r($row->userBannerImages);die;
		foreach($row->userBannerImages as $img){

				$path = changeImageUrlForFileExist(asset('sp_uploads/banner_images/'.$img['banner_path']));
				$headers = get_headers($path);
				if(stripos($headers[0], "200 OK")){
					$data_1['id'] = $img['id'];
		            $data_1['banner_path'] = changeImageUrlForFileExist(asset('sp_uploads/banner_images/'.$img['banner_path']));
		            $data['banner_images'][]=$data_1;
		            
		         }

				// $data_1['id'] = $img['id'];
				// $data_1['banner_path'] = changeImageUrlForFileExist(asset('sp_uploads/banner_images/'.$img['banner_path']));

				
		}

		$data['amenities'] = array();
		foreach($row->userAmenities as $am){

			$data['amenities'][] = $am->getAmenityName->id;
		}

		$setting = Setting::first();
		
		$data['tax_percentage'] = $setting->percentage;
		$data['referred_amount'] = $setting->referred_cashback;
		$data['delivery_mode'] = ($row->delivery_mode !='') ? $row->delivery_mode : null;
		$data['country'] = (isset($row->getAssociatedCountryInfo[0]->country_isocode)) ? array('iso2' => $row->getAssociatedCountryInfo[0]->country_isocode,'name'=>$row->getAssociatedCountryInfo[0]->country_name) : null;
		$data['state'] = (isset($row->getAssociatedCountryInfo[0]->state_isocode) && $row->getAssociatedCountryInfo[0]->state_isocode !='') ? array('iso2' => $row->getAssociatedCountryInfo[0]->state_isocode,'name'=>$row->getAssociatedCountryInfo[0]->state_name) : null;
	   	$data['city'] = (isset($row->getAssociatedCountryInfo[0]->city_isocode) && $row->getAssociatedCountryInfo[0]->city_isocode !=0) ? array('iso2' => $row->getAssociatedCountryInfo[0]->city_isocode,'name'=>$row->getAssociatedCountryInfo[0]->city_name) : null;
	   	$data['sub_locality'] = ($row->sub_locality !="") ? $row->sub_locality : null;
		return $data;
	}
	//not check
	public function verificationApi(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'country_code' => 'required|exists:countries,phonecode',
		              'mobile_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:16|min:8',
		              'otp' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {
					$mobileNoWithCode = $request->country_code . $request->mobile_no;
	          		$row = OtpNumber::where('complete_mobile_no',$mobileNoWithCode)->where('otp',$request->otp)->count();
					if($row > 0){
						OtpNumber::where('complete_mobile_no',$mobileNoWithCode)->where('otp',$request->otp)->delete();
						return response()->json(['status' => true]);
					}else{
						return response()->json(['status' => false]);
					}
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function changePassword(Request $request)
    {
    	if (Input::isMethod('post')) {
    		try{
    			$validator = Validator::make($request->all(), [
                    'old_password'=>'required|min:8|max:50',
                    'new_password'=>'required||min:8|max:50'  
                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{

	        		$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
		        	$row = SpUser::whereId($user_id)->first();
		        	if($row)
		        	{
	        			if (Hash::check($request->old_password, $row->password)) 
			            {
			                $row->password = bcrypt($request->new_password);
			                $row->save();
			                return response()->json(['status'=>true,'message'=>__('messages.Password Updated Successfully')]);
			            }
			            else
			            {
			            	
			                return response()->json(['status'=>false,'message'=>__('messages.Old password is not correct.')]);
			            }
		        		
		        	}else{
		        		return response()->json(['status'=>false,'message'=>__('messages.deactivate')]);
		        	}
		            
		        }
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
    	}
    }

    public function forgotPassword(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'country_code' => 'required|exists:countries,phonecode',
		              'mobile_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:16|min:8',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {
	          		$user = SpUser::where('mobile_no',$request->mobile_no)->where('country_code',$request->country_code)->first();
	          		
	          		if($user)
	          		{
	          			$otp = $this->getOpt($request->country_code . $request->mobile_no);
	          			$rows = $this->showUserDetails($user);
                		return response()->json(['status' => true,'otp' => $otp,'data' => $rows]);	
	          		}
	          		else{
	          			
                		return response()->json(['status' => false,'message' => __('messages.Please enter valid mobile no')]);
	          		}
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function forgotResetPassword(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'country_code' => 'required|exists:countries,phonecode',
		              'mobile_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:16|min:8',
		              'password' => 'required|min:8|max:50',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {
	          		$user = SpUser::where('mobile_no',$request->mobile_no)->where('country_code',$request->country_code)->first();
	          		
	          		if($user)
	          		{
	          			$user->password = bcrypt($request->password);
	          			$user->save();
                		return response()->json(['status' => true,'message' => __('messages.Password Updated Successfully')]);	
	          		}
	          		else{
	          			
                		return response()->json(['status' => false,'message' => __('messages.Please enter valid mobile no')]);
	          		}
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function logout(Request $request)
    {
    	if (Input::isMethod('post')) {
    	   try
	       {
	    		$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'device_type' => 'required|in:IPHONE,ANDROID,IOS',
		              'device_token' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error, 'data' => []]);
		          } 
		          else 
		          {
		          	$token = JWTAuth::getToken();
		          	if($token)
		          	{
		          		$user_id =  JWTAuth::toUser(JWTAuth::getToken())->id;		          				          				          	
				        SpDevice::manageDeviceIdAndToken($user_id, $data['device_token'], $data['device_type'], $request->header('language'),'remove');
				        JWTAuth::setToken($token)->invalidate();

		        		return response()->json(['status' => true, 'message' => __('messages.Successfully Logged Out')]);
		          	}
		          	
		          }
	       }
	       catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
    	}
    	 
    }

    public function resendEmail(){
		$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
    	$user = SpUser::whereId($user_id)->first();
    	if($user)
    	{
    		$data['templete'] = "sp_confirmation_email";
	        $data['name'] = $user->name;
	        $data['token'] = $user->verifyToken;
	        $data['email'] = $user->email;
	        $data['subject'] = "Confirmation Email";
	        send($data);
	        return response()->json(['status' => true ,'message' => __('messages.Mail Sent Successfully')]);
    	}
		
	}

    public function updateMobile(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'country_code' => 'required|exists:countries,phonecode',
		              'mobile_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:16|min:8',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {
	          		$user = SpUser::where('mobile_no',$request->mobile_no)->where('country_code',$request->country_code)->first();
	          		
	          		if($user)
	          		{
	          			return response()->json(['status' => false,'message' => __('messages.Mobile number already exist')]);
	          		}
	          		else{
	          			$mobileNoWithCode = $request->country_code . $request->mobile_no;
	          			$otp = $this->getOpt($mobileNoWithCode);     
                		return response()->json(['status' => true,'otp' => $otp,'message' => __('messages.OTP Sent Successfully')]);           		
	          		}
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function userDetails(Request $request)
	{
		if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'userid' => 'required|exists:sp_users,id',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {
	          		$user = SpUser::where('id',$request->userid)->first();
	          		
	          		if($user)
	          		{
						$userdata = $this->showUserDetails($user);
	          			return response()->json(['status' => true,'message' => 'User Details','data' => $userdata]);
	          		}
	          		else{
                		return response()->json(['status' => false,'message' => 'Record not Found']);           		
	          		}
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
	}

	public function editProfile(Request $request)
    {
    	if (Input::isMethod('post')) 
    	{
    		try
    		{

    			$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

    			$validator = Validator::make($request->all(), [
	              'store_name' => 'required',
	              'store_number' => 'required',
	              'username' => 'required',
	              'email' => 'email|unique:sp_users,email,'.$user_id,
	              'country_code' => 'required',
	              'mobile_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:16|min:8',
	              'whatsapp_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|max:16|min:8',
	              'amenities' => 'required',
	              'profile_image' => 'mimes:jpeg,jpg,png,gif',
	              'description' => 'required',
	              'service_criteria' => 'required',
	              'address' => 'required',
	              // 'landmark' => 'required',
	              'lat' => 'required',
	              'long' => 'required',
	              'country' => 'required',
	              //'delivery_mode'=>'required' // online,store,home,
                 ]);
    			if ($validator->fails()) 
		        {
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status'=>false,'message'=>$error]);
		        }else{
					$email_check = 0;
		        	$country_row = Country::where('phonecode',$request->country_code)->first();
		          	if($country_row)
		          	{
		          		
			        	$user = SpUser::whereId($user_id)->first();
			        	
			        	if($user)
			        	{							
			        		$user->name = $request->username;
			        		$amenities = json_decode($request->amenities, true);
			        		foreach($amenities as $am){
			        			SpAmenity::manageAmenities($user_id, $am,'remove');
			        		}
			        		foreach($amenities as $am){
			        			SpAmenity::manageAmenities($user_id, $am,'add');
			        		}
			        		

			        		if($request->email!= $user->email){
			        			$user->verifyToken = strtotime(date('Y-m-d H:i:s')).rand(1111,999999);
			        			$user->email = $request->email;
			        			$email_check = 1;
			        			
			        		}else{
			        			$user->email = $request->email;
			        		}
			        		
			        		$user->country_code = $request->country_code;
			        		$user->mobile_no = $request->mobile_no;
			        		$user->whatsapp_no = $request->whatsapp_no;
			        		$user->store_name = $request->store_name;
			        		$user->store_number = $request->store_number;
			        		$user->landmark = $request->landmark;
			        		$user->service_criteria = $request->service_criteria;
			        		$user->description = $request->description;
			        		$user->address = $request->address;
		                    $user->latitude = $request->lat;
		                    $user->longitude = $request->long;
		                    $user->area_description = $request->area_description;
		                    $user->delivery_mode = $request->delivery_mode;
		                    $user->sub_locality = $request->sub_locality;

			        		if ($request->hasFile('profile_image') && $request->file('profile_image'))
			        		{

			                    $file = $request->file('profile_image');
			                    $name = time().'.'.$file->getClientOriginalExtension();
			                    $destinationPath = public_path('sp_uploads/profile/');
			                    $img = $file->move($destinationPath, $name);

			                   // $user->profile_image = 'sp_uploads/profile/'.$name;
			                    $user->profile_image = $name;
			                }
			                if($request->bannerimagecount){

			                	$count = $request->bannerimagecount;

			                	for($i=1;$i<= $count;$i++){
			           
			                		$img = 'bannerimages'.$i;

			                		if ($request->$img)
					        		{
					                    $file = $request->$img;
					                    $name = time().str_random(2).'.'.$file->getClientOriginalExtension();
					                    $destinationPath = public_path('sp_uploads/banner_images/');
					                    $img = $file->move($destinationPath, $name);

					                    $row = new SpBannerImages();

					                    $row->user_id = $user_id;
					                    //$row->banner_path = 'sp_uploads/banner_images/'.$name;
					                    $row->banner_path = $name;
					                    $row->save();
					                }
			                	}
			                } 

			                if($request->country)
			                {
			                	SpCountry::manageSpCountries($user_id, json_decode($request->country,true),json_decode($request->state,true),json_decode($request->city,true));
			                }

			        		$user->save();
			        		if($user->save() && $email_check == 1)
			        		{
			        			$data['templete'] = "sp_confirmation_email";
								$data['name'] = $user->name;
								$data['token'] = $user->verifyToken;
								$data['email'] = $user->email;
								$data['subject'] = "Confirmation Email";
								send($data);
			        		}
			        		$data = $this->showUserDetails($user);
			        		return response()->json(['status' => true, 'message' => __('messages.Profile Updated Successfully'), 'data' => $data]);
			        	}else{
			        		return response()->json(['status'=>false,'message'=>__('messages.deactivate')]);
			        	}
			        }else{
			        	return response()->json(['status' => true, 'message' => __('messages.Country Code is Invalid')]);
			        }
		        	
		        }
    		}
    		catch(\Exception $e){
	       		return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }

    	}
    }

   public function shopTimings(Request $request)
    {
    	if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'timings' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {
		          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;
		          	
		          	$timing_arr = json_decode($request->timings,true);
		        

		          	foreach($timing_arr as $day => $val){
		          		$row = ShopTiming::where('user_id',$user_id)->where('day',$val['day'])->first();
		          		$row->isOpen = $val['isOpen'];
		          		$row->opening_time = $val['opening_time'];
		          		$row->closing_time = $val['closing_time'];
		          		$row->save();
		          	}

		          	$user = SpUser::where('id',$user_id)->first();
					$userdata = $this->showUserDetails($user);
	          			return response()->json(['status' => true,'message' => 'User Details','data' => $userdata]);		          
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
    }

    public function deleteBannerImage(Request $request)
    {
    	if (Input::isMethod('post')) {
	       try
	       {
		       	$data = $request->all();
		       	  $validator = Validator::make($data, 
		            [
		              'image_id' => 'required',
		            ]);
		       	  if ($validator->fails()) 
		          {
		          	
		            $error = $this->validationHandle($validator->messages());
		            return response()->json(['status' => false, 'message' => $error]);
		          } 
		          else 
		          {
		          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

		          	$row = SpBannerImages::where('user_id',$user_id)->where('id',$request->image_id)->first();
		          	if($row){
		          		$image_path = 'public/sp_uploads/banner_images/'.$row->banner_path;
		          		// echo $image_path;die;
		          		unlink($image_path);
		          		$row->delete();
		          	}
		          	$user = SpUser::where('id',$user_id)->first();

					$userdata = $this->showUserDetails($user);
	          		return response()->json(['status' => true,'message' => 'User Details','data' => $userdata]);		          
		          }
	       }
	       catch(\Exception $e){
	       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
	       }
		}
    }

    public function viewAgreement()
    {
       try
       {  
          	$user_id = JWTAuth::toUser(JWTAuth::getToken())->id;

          	$row = SpUser::where('id',$user_id)->first();
          	$tier = Tier::where('id',$row->tier_id)->first();
          	$data = array();
          	if($row){

          		$data['user_id'] = $row->id;
          		$data['agreement'] = changeImageUrlForFileExist(asset('public/sp_uploads/approve_pdf/'.$row->agreement));
          		$data['is_provide_home_service'] = $row->is_provide_home_service;
          		$data['mov'] = $row->mov;
          		$data['commission'] = $tier['commission'];
          		$data['redeem'] = $tier['redeem'];
          		$data['earning'] = $tier['earning'];

          	}

      		return response()->json(['status' => true,'message' => 'Agreement Detail','data' => $data]);		          
	          
       }
       catch(\Exception $e){
       	 return response()->json(['status' => false, 'message' => $e->getMessage(), 'data' => []]);
       }
   }
	

}
  