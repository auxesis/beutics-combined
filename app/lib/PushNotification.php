<?php

namespace App\Lib;

class PushNotification {

    public static function Notify($user, $ref_user_id, $ref_id, $ref_type,$message, $message_ar,$title,$title_ar,$user_type, $dic = []) {

        // echo $message.'---';
        // echo $message_ar.'---';
        // echo $title.'---';
        // echo $title_ar.'---';die;
       
        $ios_device_arr = [];
        if($user_type == 'customer'){
            
        $ios_device_arr = \App\Model\Device::select('devices.*')
            ->leftJoin('users', 'users.id', '=', 'devices.user_id')
          //  ->where('users.is_notification_on', '1')
            ->whereIn('device_type', array('IPHONE','IOS','ANDROID'))
            ->where('device_token', '!=', 'simulator')
            ->where('user_id', $user)
            ->groupBy('device_token')
            ->select('device_token','language')
            ->get();
        }else{
            
            $ios_device_arr = \App\Model\SpDevice::select('sp_devices.*')
            ->leftJoin('sp_users', 'sp_users.id', '=', 'sp_devices.user_id')
          //  ->where('users.is_notification_on', '1')
            ->whereIn('device_type', array('IPHONE','IOS','ANDROID'))
            ->where('device_token', '!=', 'simulator')
            ->where('user_id', $user)
            ->groupBy('device_token')
            ->select('device_token','language')
            ->get();
        }
// print_r($ios_device_arr);die;
        if (!empty($ios_device_arr)) {
            
            $notification_info = array('ref_id'=>$ref_id,'ref_type'=>$ref_type);

            PushNotification::sendFcmNotify($ios_device_arr, $message,$message_ar,$title,$title_ar,$user_type,$notification_info);
        }
        
    }

    public static function sendFcmNotify($user_devices, $message,$message_ar,$title,$title_ar,$user_type, $notification_info, $dictionary = array(), $type = '' , $sound = '',$notification_id = '' ,$object_id ='', $unread = '', $unread_activities = '')
    {



        $url = 'https://fcm.googleapis.com/fcm/send';
        if($user_type == 'customer')          
        {
            $server_key = 'AIzaSyCgQLrUA2VZVhwJmvi63S9mtzvkABRkhOc';
            // $server_key = 'AIzaSyANgESj4qJqOHZqKzSESx8GhuM3KV8Qp24';
        }else{
            $server_key = 'AIzaSyAA_cqFMF89oItkXyqS1-KrCGVOGaR2MyI';
        }

        //$ttl = 86400;
        //$randomNum = rand(10, 100);
        //$user_devices = ['czNxA4VAnwk:APA91bHwHUX7rMF4ec6eaohQb9q2cqGEyKEjexrEuLpcYRIQNs4KiHLBQ1sXMJ8XANv-t7QluToptOJmrfS_Cb_bZOoT7VM1icZPYJ0lzJ1GwrI7sTVffUDlBHRIuW3VdIIXy1UaMj29'];
        // print_r($user_devices);die;
        foreach($user_devices as $row){
                $device_id = $row->device_token;
                $msg = "";
                $msg_title = "";
                $fields = array();

                if($user_type == 'customer')          
                {
                    $badge_count = \App\Model\Device::getAndUpdateBadgecount($device_id);
                    // $server_key = 'AIzaSyANgESj4qJqOHZqKzSESx8GhuM3KV8Qp24';
                }else{
                    $badge_count = \App\Model\SpDevice::getAndUpdateBadgecount($device_id);
                }
                if(empty($row->language) || $row->language== "en"){
                        $msg = $message;
                        $msg_title = $title;
                    
                }else{
                     $msg = $message_ar;
                     $msg_title = $title_ar;
                }
                
                // echo $msg.')))'.$msg_title;
                $fields = array
                (
                    "priority" => "high",
                    "data" => array( 
                        "title"=>"$msg_title", 
                        "body" =>"$msg",
                        "sound" => "default",
                        "type"=>"$type",
                        "content_available" => 1,
                        "force-start" => 1,
                        "notification_id"=>"$notification_id",
                        "object_id"=>"$object_id",
                        "dictionary" => (object) $dictionary,
                        "unread_activities" => "$unread_activities",
                        "click_action" => "FCM_PLUGIN_ACTIVITY",
                        "notification_detail" => $notification_info
                    ),
                    "notification"  => array(
                        "title"=>"$msg_title",
                        "body"=>"$msg",
                        "sound" => "default",
                        "type"=>"$type",
                        "content_available" => 1,
                        "force-start" => 1,
                        "notification_id"=>"$notification_id",
                        "object_id"=>"$object_id",
                        "dictionary" =>(object) $dictionary,
                        "badge" => $badge_count
                    ),
                    "unread_activities" => "$unread_activities",
                    "click_action" => "FCM_PLUGIN_ACTIVITY",
                    "notification_detail" => $notification_info,
                    'to'=> $device_id
                );

                $headers = array(
                'Content-Type:application/json',
                'Authorization:key='.$server_key
            );
        
        
            $js = json_encode($fields);
        
            // echo $js;die;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $js);
            $result = curl_exec($ch);
            curl_close($ch);
            // echo '<pre>'; print_r($fields);
 
        }
        
        // die;
        // $this->load->model('setting/notification');
        
        
        
        

        // echo '<pre>'; print_r($fields);
        // echo '<pre>'; print_r($user_devices->toArray());die('test');
        /*
        if ($result === FALSE) {
           die('Problem occurred: ' . curl_error($ch));
        }
        */

    }
}
