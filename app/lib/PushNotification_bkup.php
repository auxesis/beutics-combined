<?php

namespace App\Lib;

class PushNotification {

    public static function Notify($user, $ref_user_id, $ref_id, $ref_type,$message, $message_title,$user_type, $dic = []) {
       
        $ios_device_arr = [];
        if($user_type == 'customer'){
            
            $ios_device_arr = \App\Model\Device::select('devices.*')
                ->leftJoin('users', 'users.id', '=', 'devices.user_id')
              //  ->where('users.is_notification_on', '1')
                ->whereIn('device_type', array('IPHONE','ANDROID'))
                ->where('device_token', '!=', 'simulator')
                ->where('user_id', $user)
                ->pluck('device_token')->toArray();
            }else{
                
                $ios_device_arr = \App\Model\SpDevice::select('sp_devices.*')
                ->leftJoin('sp_users', 'sp_users.id', '=', 'sp_devices.user_id')
              //  ->where('users.is_notification_on', '1')
                ->whereIn('device_type', array('IPHONE','ANDROID'))
                ->where('device_token', '!=', 'simulator')
                ->where('user_id', $user)
                ->pluck('device_token')->toArray();
            }

        // @mail("mahaveer@ninehertzindia.com","My subject mm 2 ",$user);
        // @mail("mahaveer@ninehertzindia.com","My subject mm 3 ",json_encode($ios_device_arr));
       
        if (!empty($ios_device_arr)) {
            
            $notification_info = array('ref_id'=>$ref_id,'ref_type'=>$ref_type);
            PushNotification::sendFcmNotify($ios_device_arr, $message,$message_title,$user_type,$notification_info);
        }
        
    }

    public static function sendFcmNotify($user_devices, $message,$message_title,$user_type, $notification_info, $dictionary = array(), $type = '' , $sound = '',$notification_id = '' ,$object_id ='', $unread = '', $unread_activities = '')
    {
        
        // $this->load->model('setting/notification');
        $url = 'https://fcm.googleapis.com/fcm/send';
        if($user_type == 'customer')          
        {
            $server_key = 'AIzaSyCgQLrUA2VZVhwJmvi63S9mtzvkABRkhOc';
            // $server_key = 'AIzaSyANgESj4qJqOHZqKzSESx8GhuM3KV8Qp24';
        }else{
            $server_key = 'AIzaSyAA_cqFMF89oItkXyqS1-KrCGVOGaR2MyI';
        }
        
        $ttl = 86400;
        $randomNum = rand(10, 100);

        $fields = array
        (
            "priority"             => "high",
            "data"         => array( 
                "title"=>"$message_title", 
                "body" =>"$message",
                "sound" => "default",
                "type"=>"$type",
                "content_available" => 1,
                "force-start" => 1,
                "notification_id"=>"$notification_id",
                "object_id"=>"$object_id",
                "dictionary" => (object) $dictionary,
                "badge" => '1',
                "unread_activities" => "$unread_activities",
                "click_action" => "FCM_PLUGIN_ACTIVITY",
                "notification_detail" => $notification_info
            ),
            "notification"  => array( 
                "title"=>"$message_title",
                "body"=>"$message",
                "sound" => "default",
                "type"=>"$type",
                "content_available" => 1,
                "force-start" => 1,
                "notification_id"=>"$notification_id",
                "object_id"=>"$object_id",
                "dictionary" =>(object) $dictionary),
                "badge" => '1',
                "unread_activities" => "$unread_activities",
                "click_action" => "FCM_PLUGIN_ACTIVITY",
                "notification_detail" => $notification_info

        ); 

        //echo '<pre>'; print_r($fields);
        // echo '<pre>'; print_r($user_devices);die('test');
        

        if($user_devices){
            if(count($user_devices)>1)
            {
                    $fields['registration_ids'] = $user_devices;
            }
            else
            {
                    $fields['to'] = $user_devices[0];
            }
        }

        $headers = array(
            'Content-Type:application/json',
            'Authorization:key='.$server_key
        );
        
        
        $js = json_encode($fields);
        
        // echo $js;die;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $js);
        $result = curl_exec($ch);
        curl_close($ch);

        // echo $result;
        // die;
        if ($result === FALSE) {
           die('Problem occurred: ' . curl_error($ch));
        }

    }
}
