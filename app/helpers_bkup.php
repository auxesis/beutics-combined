<?php
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Model\User;
use App\Model\Admin;
use App\Model\SpUser;
use Mail;

if (!function_exists('send')) {


    function send($data) {
        $from_mail = 'oliver7415@googlemail.com';
        $site_title = 'Beutics';
        // Send email
        Mail::send('email.'.$data['templete'], ['data' => $data], function ($m) use ($data, $from_mail, $site_title) {
          $m->from($from_mail, $site_title);
          $m->to($data['email'], $data['name'])->subject($data['subject']);
        });
    }

}

if (!function_exists('buttonHtml')) {

    function buttonHtml($key, $link) {
        $array = [
            "edit" => "<span class='f-left margin-r-5'><a data-toggle='tooltip'  class='btn btn-primary btn-xs' title='Edit' href='" . $link . "'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a></span>",
            "Active" => '<span class="f-left margin-r-5"> <a data-toggle="tooltip" class="btn btn-success btn-xs" title="Active" href="' . $link . '"><i class="fa fa-check" aria-hidden="true"></i></a></span>',
            "Inactive" => '<span class="f-left margin-r-5"> <a data-toggle="tooltip" class="btn btn-warning btn-xs" title="Inactive" href="' . $link . '"><i class="fa fa-times" aria-hidden="true"></i></a></span>',
           "Complete" => '<span class="f-left margin-r-5"> <a data-toggle="tooltip" class="btn btn-success btn-xs" title="Complete" href="' . $link . '"><i class="fa fa-check" aria-hidden="true"></i></a></span>',
            "delete" => '<form method="post" action="' . $link . '" accept-charset="UTF-8" style="display:inline" onsubmit="return deleteRow(this)"><input name="_method" value="DELETE" type="hidden">
            ' . csrf_field() . '<span><button data-toggle="tooltip"  title="Delete" type="submit" class="btn btn-danger btn-xs delete-action"><i class="fa fa-trash-o" aria-hidden="true"></i></button></span></form>',

            "change-password" => '<span class="f-left margin-r-5"><a data-toggle="tooltip"  class="btn btn-danger btn-xs" title="Change Password" href="' . $link . '"><i class="fa fa-key" aria-hidden="true"></i></a></span>',
            "view" => '<span class="f-left margin-r-5"><a data-toggle="tooltip"  class="btn btn-info btn-xs" title="View Profile" href="' . $link . '"><i class="fa fa-eye" aria-hidden="true"></i></a></span>',

            "staff" => '<span class="f-left margin-r-5"><a data-toggle="tooltip"  class="btn btn-info btn-xs" title="View Staff" href="' . $link . '"><i class="fa fa-users" aria-hidden="true"></i></a></span>',

            "product" => '<span class="f-left margin-r-5"><a data-toggle="tooltip"  class="btn btn-info btn-xs" title="View Product" href="' . $link . '"><i class="fa fa-product-hunt" aria-hidden="true"></i></a></span>',

            "nominate" => '<span class="f-left margin-r-5"><a data-toggle="tooltip"  class="btn btn-info btn-xs" title="Nominate" href="' . $link . '"><i class="fa fa-trophy" aria-hidden="true"></i></a></span>',

            "services" => '<span class="f-left margin-r-5"><a data-toggle="tooltip"  class="btn btn-info btn-xs" title="Services" href="' . $link . '"><i class="fa fa-cogs" aria-hidden="true"></i></a></span>',
            "banner" => '<span class="f-left margin-r-5"><a data-toggle="tooltip"  class="btn btn-info btn-xs" title="Banner Images" href="' . $link . '"><i class="fa fa-file-image-o" aria-hidden="true"></i></a></span>',
            "chat" => '<span class="f-left margin-r-5"><a data-toggle="tooltip"  class="btn btn-info btn-xs" title="chat" href="' . $link . '"><i class="fa fa-comments-o" aria-hidden="true"></i></a></span>'
        ];

        if (isset($array[$key])) {
            return $array[$key];
        }
        return '';
    }
}

/*
 * * Button With Html
 */
if (!function_exists('getButtons')) {


    function getButtons($array = []) {
        $html = '';
        foreach($array as $arr)
        {
            $html  .= buttonHtml($arr['key'],$arr['link']);
        }
        return $html;
      
    }

}

if (!function_exists('loginData')) {


    function loginData() {
        if(Session::get('AdminLoggedIn')['user_id']){            
            $user_id = Session::get('AdminLoggedIn')['user_id'];
        }
        $row =  Admin::whereId($user_id)->first();
        return $row;
      
    }

}

if (!function_exists('spLoginData')) {


    function spLoginData() {
        if(Session::get('SpAdminLoggedIn')['user_id']){            
            $user_id = Session::get('SpAdminLoggedIn')['user_id'];
        }
        $row =  SpUser::whereId($user_id)->first();
        return $row;
      
    }

}

if (!function_exists('getStatus')) {

    function getStatus($current_status,$id) {
       $html = '';
      switch ($current_status) {
          case '1':
                $html =  '<span class="f-left margin-r-5" id = "status_'.$id.'"><a data-toggle="tooltip"  class="btn btn-success btn-xs" title="Active" onClick="changeStatus('.$id.')" >Active</a></span>';
         
             
              break;
               case '0':
                $html =  '<span class="f-left margin-r-5" id = "status_'.$id.'"><a data-toggle="tooltip"  class="btn btn-danger btn-xs" title="Inactive" onClick="changeStatus('.$id.')" >Inactive</a></span>';

             
              break;
          
          default:
            
              break;
      }

      return $html;
      
    }

}

if (!function_exists('getApprovalStatus')) {

    function getApprovalStatus($current_status,$id) {
       $html = '';
      switch ($current_status) {
          case '1':
                $html =  '<span class="f-left margin-r-5" id = "approvalStatus_'.$id.'"><a data-toggle="tooltip"  class="btn btn-success btn-xs" title="Active" onClick="changeApproval('.$id.')" >Yes</a></span>';
         
             
              break;
               case '0':
                $html =  '<span class="f-left margin-r-5" id = "approvalStatus_'.$id.'"><a data-toggle="tooltip"  class="btn btn-danger btn-xs" title="Inactive" onClick="changeApproval('.$id.')" >No</a></span>';

             
              break;
          
          default:
            
              break;
      }
      return $html;  
    }
}

if (!function_exists('returnColorCode')) {


    function returnColorCode($rating)
    {
       $color_arr = array();

        if($rating > 2.3 && $rating < 5){

            $color_arr = array("Needs Improvement","#f3001c");//red

        }if($rating > 4.9 && $rating <= 6){

            $color_arr = array("Average","#f7aa25");//orange

        }if($rating > 6 && $rating <= 7){

             $color_arr = array("Good","#60db57");//light green

        }if($rating > 7 && $rating <= 8){

            $color_arr = array("Very Good","#078229");//dark green

        }if($rating > 8 && $rating <= 9){

            $color_arr = array("Superb","#8bb9ff");//blue green

        }if($rating > 9 && $rating <= 10){
            $color_arr = array("Exceptional","#800080");//bright purple
        }
        return $color_arr;
    }

}

// if ( ! function_exists('uploadwithresize'))
// {
//     function uploadwithresize($file,$path)
//     { 
//         $h=522;
//         $w= 522;
//         $orig_h = 630;
//         $orig_w = 1242;
//         $fileName = time().rand(111111111,9999999999).'.'.$file->getClientOriginalExtension();
//         $destinationPath    = 'public/sp_uploads/'.$path.'/';
//         $image_resize = Image::make($file->getRealPath())
//         ->resize($orig_w, $orig_h)
//         // original
//         ->save($destinationPath.$fileName)

//         // thumbnail
//         ->resize($w, $h)
//         ->save($destinationPath.'thumb/'.$fileName)
//         ->destroy();
//         return $fileName;
//     }
// }





















