<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class WalletCashHistory extends Model
{
    public function getAssociatedBookingDetails()
    {
        return $this->belongsTo('App\Model\Booking','trans_ref_id');
    }

    public function getAssociatedECardDetails()
    {
        return $this->belongsTo('App\Model\ECard','trans_ref_id');
    }

    public function getAssociatedUserDetails()
    {
        return $this->belongsTo('App\Model\User','user_id');
    }
}
