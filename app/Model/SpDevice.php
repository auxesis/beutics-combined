<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SpDevice extends Model
{
    protected $fillable = array('user_id', 'device_token','device_type','language');
    
    
    public static function manageDeviceIdAndToken($user_id, $device_token, $device_type, $methodName, $lang = '') {

        if ($methodName == 'add') {
            SpDevice::updateOrCreate(
                    ['user_id' => $user_id, 'device_token' => $device_token, 'device_type' => $device_type, 'language' => $lang]
            );
        }
        if ($methodName == 'remove') {
            SpDevice::where('user_id', $user_id)
                    ->where('device_token', $device_token)
                    ->where('device_type', $device_type)
                    ->delete();
        }
    }


     public static function getAndUpdateBadgecount($device_id){
        $badge_count = 0;
        $device_row = SpDevice::where('device_token', $device_id)->first();
        if(!empty($device_row)){
            $badge_count = !empty($device_row->badge_count) ? $device_row->badge_count : 0;
            $badge_count += 1;
            $device_row->badge_count = $badge_count;
            $device_row->save();
        }
        return $badge_count;
    }
}
