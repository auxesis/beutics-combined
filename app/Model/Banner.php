<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
class Banner extends Model
{
    public static function getBannerData($pageName)
    {
    	$rows = DB::table('banners')->select('id','title','sub_title','image','page_section_name','page_name','description','sub_description')->where('page_name', $pageName)->where('status','1')->orderBy('sort_order', 'asc')->get();
	   return $rows;
	}
}
