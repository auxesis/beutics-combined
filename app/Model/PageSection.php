<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
class PageSection extends Model
{
    public static function getPageSectionData($pageName)
    {
    	$rows = DB::table('page_sections')->select('title','image','page_section_name','page_name','description','created_at')->where('page_name', $pageName)->where('status','1')->orderBy('created_at', 'asc')->get();
	   return $rows;
	}
}
