<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class QuickFact extends Model
{
    public function getAssociatedSubcategoryName()
    {
        return $this->belongsTo('App\Model\Category','subcategory_id');
    }

    public function getAssociatedcategoryName()
    {
        return $this->belongsTo('App\Model\Category','category_id');
    }

    public function getAssociatedServiceName()
    {
        return $this->belongsTo('App\Model\Service','service_id');
    }

    public function getAssociatedBannerImages()
    {
        return $this->hasMany('App\Model\QuickfactBannerImage','quick_fact_id');
    }
}