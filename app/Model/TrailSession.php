<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TrailSession extends Model
{
   public function getAssociatedSessionQuestions()
    {
        return $this->hasMany('App\Model\TrailSessionQuestion','trail_session_id');
    }
}