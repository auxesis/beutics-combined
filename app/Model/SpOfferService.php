<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SpOfferService extends Model
{
    public function getAssociatedOfferServicesName()
    {
        return $this->belongsTo('App\Model\SpService','sp_service_id');
    }

    public function getAssociatedOfferServicesPrices()
    {
        return $this->hasMany('App\Model\SpServicePrice','sp_service_id','sp_service_id');
    }
     public function getAssociatedOffer() {
        return $this->belongsTo('App\Model\SpOffer','sp_offer_id')->where('is_deleted', '!=','1');
    }

}
