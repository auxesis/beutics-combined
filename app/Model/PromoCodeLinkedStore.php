<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PromoCodeLinkedStore extends Model
{
    public function getAssociatedStorename()
    {
        return $this->belongsTo('App\Model\SpUser','sp_id');
    }
}
