<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Product extends Model
{
    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'product_name',
                'onUpdate' => true
            ]
        ];
    }

    public function getServiceProInfo()
    {
        return $this->belongsTo('App\Model\SpUser','sp_id');
    }
}
