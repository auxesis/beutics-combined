<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CashbackHistory extends Model
{
    public function getAssociatedUsrName()
    {
        return $this->belongsTo('App\Model\User','user_id');
    }

    public function getAssociatedReferrerUsrName()
    {
        return $this->belongsTo('App\Model\User','referrer_user_id');
    }

    public function getAssociatedReferralUsrName()
    {
        return $this->belongsTo('App\Model\User','referral_user_id');
    }

     public function getAssociatedReferrerSpName()
    {
        return $this->belongsTo('App\Model\SpUser','referrer_user_id');
    }
}