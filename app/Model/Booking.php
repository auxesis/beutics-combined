<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    public function getAssociatedSpInformation()
    {
        return $this->belongsTo('App\Model\SpUser','sp_id');
    }
    public function getAssociatedUserInfo()
    {
        return $this->belongsTo('App\Model\User','user_id');
    }

    public function getAssociatedGiftByUserInfo()
    {
        return $this->belongsTo('App\Model\User','gifted_by');
    }

    public function getAssociatedStaffInformation()
    {
        return $this->belongsTo('App\Model\Staff','staff_id');
    }

    public function getAssociatedBookingItems()
    {
        return $this->hasMany('App\Model\BookingItem','booking_id');
    }

    public function getAssociatedRescheduleOrderHistory()
    {
        return $this->hasMany('App\Model\RescheduleOrderHistory','booking_id')->orderBy('created_at', 'DESC');
    }

    public function getAssociatedPromoCodeInformation()
    {
        return $this->belongsTo('App\Model\PromoCode','promo_code_id');
    }

    public function getAssociatedBookingPaymentDetail()
    {
        return $this->hasMany('App\Model\BookingPaymentDetail','booking_id')->where('payment_option','wallet');
    }
    public function getAssociatedBookingPaymentDetailAll()
    {
        return $this->hasMany('App\Model\BookingPaymentDetail','booking_id');
    }
}
