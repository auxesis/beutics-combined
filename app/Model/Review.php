<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    public function getAssociatedReviewQuestions()
    {
    	return $this->hasMany('App\Model\ReviewQuestionRating','review_id');
    }

    public function getAssociatedReviewServices()
    {
    	return $this->hasMany('App\Model\ReviewSpService','review_id');
    }

    public function getAssociatedSpName()
    {
    	return $this->belongsTo('App\Model\SpUser','sp_id');
    }
    public function getAssociatedUser()
    {
        return $this->belongsTo('App\Model\User','user_id');
    }

    public function getAssociatedStaffName()
    {
        return $this->belongsTo('App\Model\Staff','staff_id');
    }
}
