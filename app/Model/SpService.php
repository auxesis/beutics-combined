<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Service;
use DB;
class SpService extends Model
{
    public $timestamps = false;
    public function getRelatedPrices(){
        return $this->hasMany('App\Model\SpServicePrice','sp_service_id');
    }

    public function getRelatedGoals(){
        return $this->hasMany('App\Model\SpServiceGoal','sp_service_id');
    }

    public function getRelatedTrainer(){
        return $this->hasMany('App\Model\SpServiceTrainer','sp_service_id');
    }

    public function getServiceSubcategory(){
        return $this->belongsTo('App\Model\Service','service_id');
    }

    public function getAssociatedService()
    {
        return $this->belongsTo('App\Model\Service','service_id');
    }
    
    public function getAssociatedSubService()
    {
       return $this->belongsTo('App\Model\Service','service_id')->where('parent_service_id','!=',0);
    }

    public function getParentServiceId()
    {
        return $this->belongsTo('App\Model\Service','service_id')->select(array('parent_service_id as id'));
    }

    public function getAssociatedGroupService(){
        return $this->hasMany('App\Model\SpGroupService','sp_service_id')->where('is_deleted','!=','1');;
    }

    public function getAssociatedUsername()
    {
        return $this->belongsTo('App\Model\SpUser','user_id');
    }

    public function getAssociatedOffers()
    {
        return $this->hasMany('App\Model\SpOfferService','sp_service_id')
        ->join('sp_offers', 'sp_offers.id', '=', 'sp_offer_services.sp_offer_id')
        ->where('sp_offers.is_deleted','!=','1');
    }

    public function getAssociatedServiceType()
    {
        return $this->hasMany('App\Model\ServiceType','service_type_id');
    }

    public function getAssociatedParentService($serviceId)
    {
        return Service::where('id',$serviceId)->first();
    }

    public function getAssociatedFitnessType(){
        return $this->belongsTo('App\Model\FitnessType','service_type_id');
    }

    public function getAssociatedFitnessTypeService(){
        return $this->belongsTo('App\Model\FitnessService','service_title_id');
    }
}
