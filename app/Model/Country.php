<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //

    public function getAssociatedNationality()
    {
        return $this->hasMany('App\Model\Country','nationality');
    }
}
