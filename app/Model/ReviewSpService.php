<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReviewSpService extends Model
{
    public function getAssociatedReviewService()
    {
        return $this->belongsTo('App\Model\SpService','service_id');
    }
}