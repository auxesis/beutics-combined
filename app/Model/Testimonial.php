<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
class Testimonial extends Model
{
    public static function getTestimonialData()
    {
    	$rows = DB::table('testimonials')->where('testimonials.status','1')->orderBy('id', 'desc')->get()->toArray();
	   return $rows;
	}
}
