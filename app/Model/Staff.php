<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    public function getStaffLeave()
    {
        return $this->hasMany('App\Model\StaffLeave','staff_id');
    }
    

    public function getServiceProInfo()
    {
        return $this->belongsTo('App\Model\SpUser','sp_id');
    }

    public function getStaffImagesVideos()
    {
        return $this->hasMany('App\Model\StaffImagesVideos','staff_id');
    }

    public function getNationalityInfo()
    {
        return $this->belongsTo('App\Model\Country','nationality');
    }
}
