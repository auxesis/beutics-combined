<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\lib\PushNotification;

class Notification extends Model
{
	protected $fillable = array('user_id', 'device_token','device_type');
    
    
    public static function saveNotification($user_id,$ref_user_id, $ref_id, $ref_type, $message_main,$message_other,$message_title_main,$message_title_other,$user_type)
    {
        $notification = new Notification();
        // echo app()->getLocale();die;
        if(app()->getLocale() == 'en')
        {
            $notification->message = $message_other;
            $notification->message_ar = $message_main;
            
            $notification->notification_title = $message_title_other;
            $notification->notification_title_ar = $message_title_main;

            $message =  $message_other;
            $message_ar =  $message_main;

            $title =  $message_title_other;
            $title_ar =  $message_title_main;

        }else{
            $notification->message = $message_main;
            $notification->message_ar = $message_other;

            $notification->notification_title = $message_title_main;
            $notification->notification_title_ar = $message_title_other;

            $message =  $message_main;
            $message_ar =  $message_other;

            $title =  $message_title_main;
            $title_ar =  $message_title_other;

        }
    	
        $notification->user_id = $user_id;
    	$notification->ref_user_id = $ref_user_id;
    	$notification->ref_id = $ref_id;
        $notification->ref_type = $ref_type;
        $notification->user_type = $user_type;
    	
    	$notification->save();

    	PushNotification::Notify($notification['user_id'],$notification['ref_user_id'], $notification['ref_id'], $notification['ref_type'],$message,$message_ar,$title,$title_ar,$user_type, $dic = []);

    }

    public function getAssociatedRefSpInfo()
    {
        return $this->belongsTo('App\Model\SpUser','ref_user_id');
    }

    public function getAssociatedRefUserInfo()
    {
        return $this->belongsTo('App\Model\User','ref_user_id');
    }

    public function getAssociatedForumCommentDetail()
    {
        return $this->belongsTo('App\Model\ForumComment','ref_id');
    }

    public function getAssociatedBookingDetail()
    {
        return $this->belongsTo('App\Model\Booking','ref_id');
    }
}