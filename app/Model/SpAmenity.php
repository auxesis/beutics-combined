<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SpAmenity extends Model
{
    protected $fillable = array('user_id', 'amenity_id');
    
    
    public static function manageAmenities($user_id, $amenity_id,$methodName) {

        if ($methodName == 'add') {
            SpAmenity::updateOrCreate(
                    ['user_id' => $user_id, 'amenity_id' => $amenity_id]
            );
        }
        if ($methodName == 'remove') {
            SpAmenity::where('user_id', $user_id)
                    ->delete();
        }
    }

    public function getAmenityName()
    {
        return $this->belongsTo('App\Model\Amenity','amenity_id');
    }
}	