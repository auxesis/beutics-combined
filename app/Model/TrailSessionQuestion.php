<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TrailSessionQuestion extends Model
{
   
	public function getAssociatedFaqs()
    {
        return $this->hasMany('App\Model\TrailSessionQuestionsFaqs','question_id');
    }
}