<?php
namespace App\Model;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Cviebrock\EloquentSluggable\Sluggable;
use DB;
class SpUser extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use Sluggable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //joined shoptiming table
    public function userShopTiming()
    {
        return $this->hasMany('App\Model\ShopTiming','user_id')->select(array('day', 'isOpen','opening_time','closing_time'));
    }

    public function userAmenities()
    {
        return $this->hasMany('App\Model\SpAmenity','user_id');
    }

    public function userBannerImages()
    {
        return $this->hasMany('App\Model\SpBannerImages','user_id')->select(array('id', 'banner_path'));
    }

    public function getAssociatedCategoryName()
    {
        return $this->belongsTo('App\Model\Category','category_id')->select(array('category_name'));
    }

    public function getSpService()
    {
        return $this->hasMany('App\Model\SpService','user_id');
    }
    public function getSpOffers()
    {
        return $this->hasMany('App\Model\SpOffer','user_id');
    }

    public function getAssociatedReviews()
    {
        return $this->hasMany('App\Model\Review','sp_id')->where('is_approved','=','1');
    }

    public function getAssociatedProducts()
    {
        return $this->hasMany('App\Model\Product','sp_id');
    }

    public function getAssociatedStaff()
    {
        return $this->hasMany('App\Model\Staff','sp_id')->where('is_deleted','!=','1');
    }

    public function getAssociatedSpTags()
    {
        return $this->hasMany('App\Model\SpTags','sp_id');
    }

    public function getAssociatedCountryInfo()
    {
        return $this->hasMany('App\Model\SpCountry','user_id');
    }

    public function getAssociatedSessionName()
    {
        return $this->belongsTo('App\Model\TrailSession','trail_session_id')->select(array('name'));
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name',
                'onUpdate' => true
            ]
        ];
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public static function getStoreData($typeId,$isFeatured = '0')
    {
        $rows = DB::table('sp_users')->select('sp_users.id','store_name','store_type','description','price','store_image','address','store_url','is_virtual_tour','is_free_trial')
        ->leftjoin('categories','categories.id','=','sp_users.category_id')
        ->where('categories.type_id','=',$typeId)
        ->where('is_featured','=',$isFeatured)
        ->limit('10')
        ->get();
        return $rows;
    }
}
