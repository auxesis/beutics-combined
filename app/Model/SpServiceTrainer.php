<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SpServiceTrainer extends Model
{
    public $timestamps = false;
}
