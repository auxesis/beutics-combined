<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ForumComment extends Model
{

    protected $fillable = ['forum_id', 'user_id', 'sp_id','message'];

    public function getAssociatedForumServiceProvider()
    {
        return $this->belongsTo('App\Model\SpUser','sp_id');
    }

    public function getAssociatedForumCustomer()
    {
        return $this->belongsTo('App\Model\User','user_id');
    }

    public function getAssociatedLikeDislike()
    {
        return $this->hasMany('App\Model\ForumCommentLikeDislike','comment_id');
    }

    public function getAssociatedCommentBannerImages()
    {
        return $this->hasMany('App\Model\ForumReplyBannerImage','comment_id');
    }
}