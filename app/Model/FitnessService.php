<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FitnessService extends Model
{
    public function getAssociatedFitnessType()
    {
        return $this->belongsTo('App\Model\FitnessType','fitness_type_id');
    }
}
