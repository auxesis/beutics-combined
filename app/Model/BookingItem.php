<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BookingItem extends Model
{
    public function getAssociatedOfferDetail()
    {
        return $this->belongsTo('App\Model\SpOffer','booking_item_id');
    }

    public function getAssociatedServiceDetail()
    {
        return $this->belongsTo('App\Model\SpService','booking_item_id');
    }

    public function getAssociatedBookingOfferServices()
    {
        return $this->hasMany('App\Model\SpOfferService','sp_offer_id');
    }
}
