<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
class Forum extends Model
{
    public function getAssociatedForumBannerImages()
    {
        return $this->hasMany('App\Model\ForumBannerImage','forum_id');
    }

    public function getAssociatedUserInformation()
    {
        return $this->belongsTo('App\Model\User','user_id');
    }

    public function getAssociatedForumComments()
    {
        return $this->hasMany('App\Model\ForumComment','forum_id');
    }
    public function getAssociatedForumCategories()
    {
        return $this->hasMany('App\Model\ForumCategory','forum_id');
    }

    public static function getForumData($typeId)
    {
        $rows = DB::table('forums')
        ->select(array(DB::raw('COUNT(forum_comments.forum_id) as count_response'),'categories.category_name','forums.message','forums.created_at'))
        ->leftjoin('forum_categories','forum_categories.forum_id','=','forums.id')
        ->leftjoin('forum_comments','forum_comments.forum_id','=','forums.id')
        ->leftjoin('categories','categories.id','=','forum_categories.category_id')
        ->where('categories.type_id','=',$typeId)
        ->where('categories.parent_id','=','0')
        ->groupBy('forum_comments.forum_id')
        ->orderBy('forums.created_at','DESC')
        ->limit('6')
        ->get()->toArray();
        return $rows;
    }
}