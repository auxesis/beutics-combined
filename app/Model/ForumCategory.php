<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ForumCategory extends Model
{
    public function getAssociatedForums()
    {
        return $this->hasMany('App\Model\forums','forum_id');
    }
}
