<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PromoCodeLinkedService extends Model
{
    public function getAssociatedPromoServiceName()
    {
        return $this->belongsTo('App\Model\Service','service_id');
    }
}
