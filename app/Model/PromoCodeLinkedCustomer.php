<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PromoCodeLinkedCustomer extends Model
{
    public function getAssociatedUsername()
    {
        return $this->belongsTo('App\Model\User','user_id');
    }
}
