<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;


class HomeModel extends Model
{

    public function getCategoryInfo()
    {        
     $row= \DB::table('categories')
        ->select(
            'categories.id',
            'category_types.type_name',
            'categories.parent_id',
            'categories.type_id',
            'categories.category_name',
            'categories.icon'
        )

      ->leftjoin('category_types', 'categories.type_id', '=', 'category_types.id')
      ->where('categories.parent_id', '!=', 0)
      ->orderBy('category_types.id','ASC')
      ->get()->toArray();
      return $row;
    }
  
   public function getoffers($url)
   {
         $current_date = date('Y-m-d');
         $row= \DB::table('category_types')
         ->select(
             'sp_offers.id',
             'offers.title',
            'sp_offers.service_type',
            'sp_offers.total_price',
            'sp_offers.offer_details',
            'sp_offers.nominate_thumb_image',
            'sp_offers.nominate_banner_image'
         )
         ->leftjoin('categories', 'category_types.id', '=', 'categories.type_id')
         ->leftjoin('sp_users', 'categories.id', '=', 'sp_users.category_id')
         ->leftjoin('sp_offers', 'sp_users.id', '=', 'sp_offers.user_id')
         ->leftjoin('offers', 'sp_offers.offer_id', '=', 'offers.id')
         ->where('sp_offers.is_deleted','!=','1')
         ->where('sp_offers.spectacular_offer','1')
         ->where('sp_offers.is_nominated','1')
         ->where('sp_offers.is_featured','1')
         ->where('categories.parent_id', '=', 0)
         ->where('category_types.type_name', '=',strtoupper($url))
        ->where('expiry_date','>=',$current_date)
        ->orderBy('sp_offers.id', 'desc') 
         ->get();

          //echo '<pre>';
       //print_r($row); die;
   return $row;
   }

   public function get_forums($url)
   {
     $row= \DB::table('category_types')
     ->select(
            'forums.id',
            'forums.subject',
            'forums.message',
            'forums.created_at',
            'categories.category_name'  
        )

      ->leftjoin('categories', 'category_types.id', '=', 'categories.type_id')
      ->leftjoin('forum_categories', 'categories.type_id', '=', 'forum_categories.category_id')
      ->leftjoin('forums', 'forum_categories.forum_id', '=', 'forums.id')
      ->where('categories.parent_id', '=', 0)
      ->where('category_types.type_name', '=',strtoupper($url)) 
      ->limit('6')
      ->get()->toArray();
      return $row;
   }


   public function get_service_store()
   {
     $row= \DB::table('sp_users')
     ->select(
            'sp_users.id',
            'sp_users.store_name',
            'sp_users.store_type',
            'sp_users.created_at',
            'sp_users.address'  
        )
      
      ->paginate(6);
      return $row;
   }


   public function getsubcategory($url)
   {
         $row= \DB::table('categories')
         ->select(
             'categories.id',
             'categories.category_name',
            'categories.icon',
            'category_types.type_name'
            )
         ->leftjoin('category_types', 'categories.type_id', '=', 'category_types.id')
         ->where('categories.parent_id', '!=', 0)
         ->where('categories.icon', '!=', '')
         ->where('category_types.type_name', '=',strtoupper($url))
        ->orderBy('categories.id', 'desc') 
        ->limit('20')
         ->get();

          //echo '<pre>';
       //print_r($row); die;
   return $row;
   }
      
}
