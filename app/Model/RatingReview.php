<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RatingReview extends Model
{
    protected $table = 'rating_reviews';
}
