<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
   
   public $timestamps = false;
   
   protected $table = 'chat'; 
}