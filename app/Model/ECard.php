<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ECard extends Model
{
    public function getAssociatedOccasionInfo()
    {
        return $this->belongsTo('App\Model\Occasion','occasion_id');
    }

    public function getAssociatedOccasionThemeInfo()
    {
        return $this->belongsTo('App\Model\GiftTheme','theme_id');
    }

    public function getAssociatedUserInfo()
    {
        return $this->belongsTo('App\Model\User','user_id');
    }

    public function getAssociatedReceiverInfo()
    {
        return $this->belongsTo('App\Model\User','receiver_id');
    }
    public function getAssociatedBookingPaymentDetail()
    {
        return $this->hasMany('App\Model\BookingPaymentDetail','booking_id')->where('booking_type', 'ecard');
    }
}