<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SpGroupService extends Model
{
	public function getServiceSubcategory(){
        return $this->belongsTo('App\Model\Service','service_id');
    }

    public function getAssociatedService()
    {
        return $this->belongsTo('App\Model\Service','service_id');
    }

    public function getAssociatedOffers()
    {
        return $this->hasMany('App\Model\SpOfferService','sp_service_id')
        ->join('sp_offers', 'sp_offers.id', '=', 'sp_offer_services.sp_offer_id')
        ->where('sp_offers.is_deleted','!=','1');
    }
    
    public function getParentServiceId()
    {
        return $this->belongsTo('App\Model\Service','service_id')->select(array('parent_service_id as id'));
    }

    public function getAssociatedParentService($serviceId)
    {
        return Service::where('id',$serviceId)->first();
    }
}
