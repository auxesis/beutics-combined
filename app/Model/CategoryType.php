<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CategoryType extends Model
{
    public function getCategoryInfo()
    {
        return $this->hasMany('App\Model\Category','type_id');
    }
}
