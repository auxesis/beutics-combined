<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PromoCode extends Model
{
    public function getAssociatedServiceProviders()
    {
        return $this->hasMany('App\Model\PromoCodeLinkedStore','promo_code_id');
    }

    public function getAssociatedCustomers()
    {
        return $this->hasMany('App\Model\PromoCodeLinkedCustomer','promo_code_id');
    }

    public function getAssociatedPromoServices()
    {
        return $this->hasMany('App\Model\PromoCodeLinkedService','promo_code_id');
    }

    public function getAssociatedSPromoSubcategories()
    {
        return $this->hasMany('App\Model\PromoCodeLinkedSubcategory','promo_code_id');
    }

    public function getAssociatedPromoCategories()
    {
        return $this->hasMany('App\Model\PromoCodeLinkedCategory','promo_code_id');
    }
}
