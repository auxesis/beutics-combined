<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;
class Service extends Model
{
    
    public function getSubcat()
    {
        return $this->belongsTo('App\Model\Category','subcat_id');
    }

    public function getAssociatedcategoryName()
    {
        return $this->belongsTo('App\Model\Category','cat_id');
    }

    public function getAssociatedSpServices()
    {
        return $this->hasMany('App\Model\SpService','service_id');
    }
}
