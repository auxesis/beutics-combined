<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SpTags extends Model
{
   public function getAssociatedSpInfo()
    {
        return $this->belongsTo('App\Model\SpUser','sp_id');
    }
}
