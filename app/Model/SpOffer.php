<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SpOffer extends Model
{
    public function getAssociatedOfferServices()
    {
        return $this->hasMany('App\Model\SpOfferService','sp_offer_id');
    }

    public function getAssociatedOfferProviderName()
    {
        return $this->belongsTo('App\Model\SpUser','user_id');
    }

    public function getAssociatedOfferName()
    {
        return $this->belongsTo('App\Model\Offer','offer_id');
    }

    public function getAssociatedOfferLikes()
    {
        return $this->hasMany('App\Model\SpectacularOfferLike','offer_id');
    }

    public function getAssociatedSpectOfferName()
    {
        return $this->belongsTo('App\Model\SpectacularOfferType','spectacular_offer_type_id');
    }

    
}
