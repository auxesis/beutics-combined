<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PromoCodeLinkedSubcategory extends Model
{
    public function getAssociatedPromoSubcategoryName()
    {
        return $this->belongsTo('App\Model\Category','subcategory_id');
    }
}
