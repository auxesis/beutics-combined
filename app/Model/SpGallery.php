<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SpGallery extends Model
{
    protected $table = 'sp_gallery';

    public function getServiceProInfo()
    {
        return $this->belongsTo('App\Model\SpUser','sp_id');
    }

    public function getTagsInfo()
    {
        return $this->belongsTo('App\Model\SpTags','tag_id');
    }
}
