<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

// use Illuminate\Database\Eloquent\SoftDeletes;

class SpCountry extends Model
{
    protected $fillable = array('user_id', 'country_isocode', 'country_name','state_isocode','state_name','city_isocode','city_name');

	public static function manageSpCountries($user_id, $country, $state = '', $city = '') {

        $country_code = $country_name = $state_code = $state_name = $city_name = ''; 
        $city_code = 0;      
        $get_country = $country;
        $country_code = $get_country['iso2'];
        $country_name = $get_country['name'];

        if(isset($state) && $state !='' && $state !=null)
        {
            $get_state = $state;
            $state_code = $get_state['iso2'];
            $state_name = $get_state['name'];
        }
        
        if(isset($city) && $city !='' && $city !=null)
        {
            $get_city = $city;
            $city_code = $get_city['iso2'];
            $city_name = $get_city['name'];
        }
        
        $attribute = ['user_id' => $user_id];
        $values = ['country_isocode' => $country_code,'country_name'=> $country_name, 'state_isocode' => $state_code,'state_name' => $state_name,'city_isocode' => $city_code,'city_name' =>$city_name];

        $maprow = SpCountry::where('user_id', $user_id);
        if($maprow->count() !=0)
        {
            $maprow->update($values);
        }
        else
        {
            SpCountry::updateOrCreate($attribute,$values);
        }
    }

    public function getServiceCountryInfo()
    {
        return $this->belongsTo('App\Model\SpCountry','user_id');
    }
}
