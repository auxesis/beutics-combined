<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PromoCodeLinkedCategory extends Model
{
    public function getAssociatedPromoCategoryName()
    {
        return $this->belongsTo('App\Model\Category','category_id');
    }
}
