<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
class Category extends Model
{
    public function getServiceInfo()
    {
        return $this->hasMany('App\Model\Service','subcat_id');
    }

    public static function getsubcategory($typeId)
    {
    	$row = DB::table('categories')->select('categories.id','categories.category_name','categories.icon','category_types.type_name')
    	->leftjoin('category_types', 'categories.type_id', '=', 'category_types.id')
        ->where('categories.type_id', '=', $typeId)
    	->where('categories.parent_id', '!=', 0)
    	->where('categories.icon', '!=', '')
    	->orderBy('categories.id', 'desc') 
    	->limit('20')
    	->get();
	   return $row;
	}
}
