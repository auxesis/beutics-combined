<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AddOnService extends Model
{
	protected $fillable = array('service_id', 'addon_service_id');

    public static function manageAddOnServices($service_id, $addon_service_id) {
     
        AddOnService::updateOrCreate(
            ['service_id' => $service_id, 'addon_service_id' => $addon_service_id]
        );
        
    }
}