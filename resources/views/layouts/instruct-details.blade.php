<!DOCTYPE html>
<html>
    @include('components.head')
<body>
    @include('components.search-header')

    @section('idetails')
    @show

    @include('components.footer')
    @include('components.scripts')
</body>
</html>