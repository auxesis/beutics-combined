<!DOCTYPE html>
<html>
    @include('components.head')
<body>
    @include('components.search-header')

    @section('adetails')
    @show

    @include('components.footer')
    @include('components.scripts')
</body>
</html>