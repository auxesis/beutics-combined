<!DOCTYPE html>
<html>
    @include('components.head')
<body>
    @include('components.search-header')

    @section('beauty-listing')
    @show

    @include('components.footer')
    @include('components.scripts')
</body>
</html>