<!DOCTYPE html>
<html>
    @include('components.head')
<body>
    @include('components.search-header')

    @section('fitness')
    @show

    @include('components.footer')
    @include('components.scripts')
</body>
</html>