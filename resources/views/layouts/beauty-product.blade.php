<!DOCTYPE html>
<html>
    @include('components.head')
<body>
    @include('components.search-header')

    @section('bpdetails')
    @show

    @include('components.footer')
    @include('components.scripts')
</body>
</html>