<!DOCTYPE html>
<html>
    @include('includes.head')
<body>
    @include('includes.search-header')

    @section('content')
    @show
    
    @include('includes.footer')
    @include('includes.scripts')
</body>
</html>