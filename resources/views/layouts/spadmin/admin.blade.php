<!DOCTYPE html>
<html>
	@include('spadmin.includes.head')

  <?php $user_data = spLoginData();//helper function?>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script>
  <script>

    var host_url = "<?php echo env('WEB_URL'); ?>:<?php echo env('NODEPORT'); ?>";
    var userId ='<?php echo $user_data['id'];  ?>';

    console.log('host_url SU',userId+' '+host_url);
    
    var socket = io.connect(host_url,{query:"user_id=SU"+userId+"&user_id_tbl=sp_users&browser_id={{ csrf_token() }}"});
    socket.on('connect', function () {
      
        console.log('connected');

        socket.on('admin_unread_count', function(data){
          console.log('admin_unread_count',data);

          document.getElementById(data.table_name).innerHTML =data.total_unread;

        });

        socket.on('disconnect', function () {
            console.log('disconnected');
        });

    });
  </script>

	<body class="  " style="background-color:#333 !important;">
    <!-- <div  class="bg-dark dk" id="wrap"> -->
    	@include('spadmin.includes.header')

        @include('spadmin.includes.sidebar')
      
        <!-- page content -->

            <div id="content">

                <div class="outer">
                  @include('spadmin.includes.breadcum')
                    <div class="inner bg-light lter" style="min-height:1000px;">
                       
				     	  @yield('content')
				     
           				</div>
       				</div>
      			</div>       
      
      @include('spadmin.includes.footer')
      @include('spadmin.includes.scripts')
      <!-- </div> -->


    <div id="right" class="onoffcanvas is-right is-fixed bg-light" aria-expanded=false>
          <a class="onoffcanvas-toggler" href="#right" data-toggle=onoffcanvas aria-expanded=false></a>
          <br>
          <br>
          <div class="alert alert-danger">
              <button type="button" class="close" data-dismiss="alert">&times;</button>
              <strong>Warning!</strong> Best check yo self, you're not looking too good.
          </div>
          <!-- .well well-small -->
          <div class="well well-small dark">
              <ul class="list-unstyled">
                  <li>Visitor <span class="inlinesparkline pull-right">1,4,4,7,5,9,10</span></li>
                  <li>Online Visitor <span class="dynamicsparkline pull-right">Loading..</span></li>
                  <li>Popularity <span class="dynamicbar pull-right">Loading..</span></li>
                  <li>New Users <span class="inlinebar pull-right">1,3,4,5,3,5</span></li>
              </ul>
          </div>
          <!-- /.well well-small -->
          <!-- .well well-small -->
          <div class="well well-small dark">
              <button class="btn btn-block">Default</button>
              <button class="btn btn-primary btn-block">Primary</button>
              <button class="btn btn-info btn-block">Info</button>
              <button class="btn btn-success btn-block">Success</button>
              <button class="btn btn-danger btn-block">Danger</button>
              <button class="btn btn-warning btn-block">Warning</button>
              <button class="btn btn-inverse btn-block">Inverse</button>
              <button class="btn btn-metis-1 btn-block">btn-metis-1</button>
              <button class="btn btn-metis-2 btn-block">btn-metis-2</button>
              <button class="btn btn-metis-3 btn-block">btn-metis-3</button>
              <button class="btn btn-metis-4 btn-block">btn-metis-4</button>
              <button class="btn btn-metis-5 btn-block">btn-metis-5</button>
              <button class="btn btn-metis-6 btn-block">btn-metis-6</button>
          </div>
          <!-- /.well well-small -->
          <!-- .well well-small -->
          <div class="well well-small dark">
              <span>Default</span><span class="pull-right"><small>20%</small></span>
          
              <div class="progress xs">
                  <div class="progress-bar progress-bar-info" style="width: 20%"></div>
              </div>
              <span>Success</span><span class="pull-right"><small>40%</small></span>
          
              <div class="progress xs">
                  <div class="progress-bar progress-bar-success" style="width: 40%"></div>
              </div>
              <span>warning</span><span class="pull-right"><small>60%</small></span>
          
              <div class="progress xs">
                  <div class="progress-bar progress-bar-warning" style="width: 60%"></div>
              </div>
              <span>Danger</span><span class="pull-right"><small>80%</small></span>
          
              <div class="progress xs">
                  <div class="progress-bar progress-bar-danger" style="width: 80%"></div>
              </div>
          </div>
      </div>
      <!-- /#right -->
    </div>
      <!-- #helpModal -->
    <div id="helpModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Modal title</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
                        et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                        aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                        culpa qui officia deserunt mollit anim id est laborum.
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- /#helpModal --> 
  </body>
</html>