<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="x-apple-disable-message-reformatting" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta name="format-detection" content="telephone=no" />
	<title>Beutics</title>
</head>
<body style="margin:0px;">
	<style>
		@import url('https://fonts.googleapis.com/css?family=Montserrat:400,500,600&display=swap');
		*{font-family: 'Montserrat', sans-serif;}
		body{background: #fff5f5;}
		table{border-spacing: 0px;}
		tbody{}
		table td {border-collapse: collapse;}
		td{padding:0px;}
		img{max-width:100%;}
		@media only screen and (max-width: 600px){
			table {
				/*width: auto !important;*/
			    min-width: 600px;
				margin-left: auto !important;
				margin-right: auto !important;
			}
		}
	</style>

	@yield('content')
</body>
</html>