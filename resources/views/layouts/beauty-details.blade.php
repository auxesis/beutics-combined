<!DOCTYPE html>
<html>
    @include('components.head')
<body>
    @include('components.search-header')

    @section('bdetails')
    @show

    @include('components.footer')
    @include('components.scripts')
</body>
</html>