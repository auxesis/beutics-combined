@extends('layouts.layout')
@section('title','Fitness Listing')
@section('content')
<div class="header_height header_bottom_p"></div>
<!-- breadcrumbs_section -->
<section class="breadcrumbs_section">
	<div class="container">
		<div id="block-breadcrumbs" class="block-breadcrumbs">
		  <nav role="navigation" aria-label="breadcrumb">
		    <ol class="breadcrumb">
		        <li class="breadcrumb-item"><a href="#">Home</a></li>
		        <li class="breadcrumb-item active">Fitness</li>
		    </ol>
		  </nav>
		</div>
	</div>
</section>
<!-- breadcrumbs_section END-->
<!-- page_filter_section -->
<section class="page_filter_section">
	<div class="container">
		<div id="block_page_filter" class="block_page_filter">
		 <h3 class="page_filter_title">Fitness</h3>
		 <div class="page_filter_wrap">
			 <div class="service_on_wrap">
		      	<div class="form-check-inline">
				  <label class="form-check-label">
				    <input type="radio" class="form-check-input  radio_top_fit " name="optradio" value="option1">All
				  </label>
				</div>
				<div class="form-check-inline">
				  <label class="form-check-label">
				    <input type="radio" class="form-check-input  radio_top_fit " name="optradio" value="option2">Artist
				  </label>
				</div>
				<div class="form-check-inline">
				  <label class="form-check-label">
				    <input type="radio" class="form-check-input  radio_top_fit " name="optradio" value="option3">In Store
				  </label>
				</div>
		    </div>
<div class="filter_shortby_wrap" id="mybutton" style="display: inline-grid;">
		    	<button class="filter_wrapper" style="background-color: transparent; border: none; padding-left: 15px;">
		    		<div class="filter_btn filt_btn"><i class="fal fa-sliders-v"></i>&nbsp; Filter</div>
		    		<div class="filter_options_wrap"></div>
				</button>
		    	<button class="shortby_wrapper" style="background-color: transparent; border: none;">
		    		<div class="shortby_btn filt_btn"><i class="fal fa-sort-amount-down"></i>&nbsp; Sort by</div>
		    		<div class="shortby_options_wrap"></div>
				</button>
		    </div>
		</div>
		</div>
	</div>
</section>
<!-- page_filter_section END-->

<!-- page_main_content_section -->
<!-- <section class="page_main_content_section">
	<div class="container">
		<div class="row no_padding page_main_content_wrapper">
			<div class="col-md-9 no_padding">
				<div class="page_main_content_wrap">
					
				</div>
			</div>
			<div class="col-md-3 no_padding">
				<div class="page_sidebar_wrap">
					
				</div>
			</div>
		</div>
	</div>
</section> -->
<section class="page_main_content_section">
	<div class="container">
		<div class="row no_padding page_main_content_wrapper">
			<div class="col-lg-9 col-sm-12 main_content_wrap">
				<div class="page_main_content_wrap listing_items listing_fitness_items">
					<div class="listing_fitness_item listing_item_wrap">
						<div class="col-lg-3 col-sm-12 no_padding image_gallery_wrap">
							<div class="product_image full_image_slider slider">
							  <div class="img_wrap"><img src="images/fitness_item1.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item2.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item3.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item4.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item5.jpg" /></div>
							</div>
							<div class="product_image_thumb thumb_image_slider slider">
							  <div class="img_wrap"><img src="images/fitness_item1.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item2.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item3.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item4.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item5.jpg" /></div>
							</div>
						</div>
						<div class="col-lg-9 col-sm-12 no_padding item_details_wrap">
							<div class="item_title_wrap">
								<h3 class="item_title">Reach - The Fitness Store</h3>
								<div class="review_details"><i class="fas fa-star"></i> 6.5 <span class="review_text good">Good</span> <span class="reviews_count">(7 Reviews)</span></div>
							</div>
							<div class="item_detail_wrap">
								<div class="detail_wrap">
									<div class="service_address"><i class="fas fa-map-marker-alt"></i>&nbsp; 17, Boulevard Plaza Tower Downtown Dubai, UAE</div>
									<div class="service_distance"><i class="fas fa-map-marker-alt"></i>&nbsp; 2.3 km away</div>
									<div class="service_kinds"><span class="btn btn-primary">Men</span><span class="btn btn-primary">Kids</span></div>
									<div class="service_features">
										<div class="feature With_fitne_small"><a href="#"><span class="item_icon v_tour "><img src="images/plane-ticket.png"></span>Virtual tour</a></div>
										<div class="feature With_fitne_small"><a href="#"><span class="item_icon f_friends"><img src="images/group.png"></span>Find Friends</a></div>
										<div class="feature With_fitne_small"><a href="#"><span class="item_icon f_trial"><img src="images/free.png"></span>Free Trial</a></div>
									</div>
									<div class="service_offers">
										<div class="feature  With_fitne_small"><a href="#"><span class="item_icon offers"><img src="images/price-label.png"></span>5 Offers</a></div>
										<div class="feature"><a href="#"><span class="item_icon cashback"><img src="images/wallet_cash.png"></span>Cashback AED 50</a></div>
									</div>
									<div class="service_activity">
										<h2>Activity</h2>
										<div class="discription">Beutics is a marketplace for Beauty, Fitness & Wellness providers. With Beutics App the world of salons, spas, fitness   read more....</div>
									</div>
									<div class="trainer_speaks"><span class="Title">Trainer Speak:</span> English, Franch, Japenes</div>
									<div class="service_available"><span class="btn btn-primary">Dancing</span><span class="btn btn-primary">Zumba</span> <span class="btn btn-primary">Swimming</span> <span class="btn btn-primary">Walking</span></div>
								</div>
								<div class="price_details_wrap">
									<div class="pricing_item">
										<div class="title">Online</div>
										<div class="pricing">
											<div class="icon"><img src="images/video-chat.png"></div>
											<div class="price">
												<div class="s_label">Starting from</div>
												AED 100
											</div>
										</div>
									</div>
									<div class="pricing_item">
										<div class="title">Studio</div>
										<div class="pricing">
											<div class="icon"><img src="images/fitness-gym.png"></div>
											<div class="price">
												<div class="s_label">Starting from</div>
												AED 100
											</div>
										</div>
									</div>
									<div class="pricing_item">
										<div class="title">Home</div>
										<div class="pricing">
											<div class="icon"><img src="images/room.png"></div>
											<div class="price">
												<div class="s_label">Starting from</div>
												AED 100
											</div>
										</div>
									</div>
									<div class="pricing_item">
										<div class="btn btn-primary explore_btn">Explore</div>	
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="listing_fitness_item listing_item_wrap">
						<div class="col-lg-3 col-sm-12 no_padding image_gallery_wrap">
							<div class="product_image full_image_slider slider">
							  <div class="img_wrap"><img src="images/fitness_item1.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item2.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item3.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item4.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item5.jpg" /></div>
							</div>
							<div class="product_image_thumb thumb_image_slider slider">
							  <div class="img_wrap"><img src="images/fitness_item1.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item2.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item3.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item4.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item5.jpg" /></div>
							</div>
						</div>
						<div class="col-lg-9 col-sm-12  no_padding item_details_wrap">
							<div class="item_title_wrap">
								<h3 class="item_title">Reach - The Fitness Store</h3>
								<div class="review_details"><i class="fas fa-star"></i> 6.5 <span class="review_text good">Good</span> <span class="reviews_count">(7 Reviews)</span></div>
							</div>
							<div class="item_detail_wrap">
								<div class="detail_wrap">
									<div class="service_address"><i class="fas fa-map-marker-alt"></i>&nbsp; 17, Boulevard Plaza Tower Downtown Dubai, UAE</div>
									<div class="service_distance"><i class="fas fa-map-marker-alt"></i>&nbsp; 2.3 km away</div>
									<div class="service_kinds"><span class="btn btn-primary">Men</span><span class="btn btn-primary">Kids</span></div>
									<div class="service_features">
										<div class="feature With_fitne_small"><a href="#"><span class="item_icon v_tour"><img src="images/plane-ticket.png"></span>Virtual tour</a></div>
										<div class="feature With_fitne_small"><a href="#"><span class="item_icon f_friends"><img src="images/group.png"></span>Find Friends</a></div>
										<div class="feature With_fitne_small"><a href="#"><span class="item_icon f_trial"><img src="images/free.png"></span>Free Trial</a></div>
									</div>
									<div class="service_offers">
										<div class="feature With_fitne_small"><a href="#"><span class="item_icon offers"><img src="images/price-label.png"></span>5 Offers</a></div>
										<div class="feature"><a href="#"><span class="item_icon cashback"><img src="images/wallet_cash.png"></span>Cashback AED 50</a></div>
									</div>
									<div class="service_activity">
										<h2>Activity</h2>
										<div class="discription">Beutics is a marketplace for Beauty, Fitness & Wellness providers. With Beutics App the world of salons, spas, fitness   read more....</div>
									</div>
									<div class="trainer_speaks"><span class="Title">Trainer Speak:</span> English, Franch, Japenes</div>
									<div class="service_available"><span class="btn btn-primary">Dancing</span><span class="btn btn-primary">Zumba</span> <span class="btn btn-primary">Swimming</span> <span class="btn btn-primary">Walking</span></div>
								</div>
								<div class="price_details_wrap">
									<div class="pricing_item">
										<div class="title">Online</div>
										<div class="pricing">
											<div class="icon"><img src="images/video-chat.png"></div>
											<div class="price">
												<div class="s_label">Starting from</div>
												AED 100
											</div>
										</div>
									</div>
									<div class="pricing_item">
										<div class="title">Studio</div>
										<div class="pricing">
											<div class="icon"><img src="images/fitness-gym.png"></div>
											<div class="price">
												<div class="s_label">Starting from</div>
												AED 100
											</div>
										</div>
									</div>
									<div class="pricing_item">
										<div class="title">Home</div>
										<div class="pricing">
											<div class="icon"><img src="images/room.png"></div>
											<div class="price">
												<div class="s_label">Starting from</div>
												AED 100
											</div>
										</div>
									</div>
									<div class="pricing_item">
										<div class="btn btn-primary explore_btn">Explore</div>	
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="features_category_row landing_page_features_category">
						<div class="feature_category">
							<h3 class="feature_title">Beauty . Fitness . Wellness</h3>
							<div class="feature_detail">Various choices in the market for Beauty, Fitness & Wellness providers</div>
						</div>
					</div>

					<div class="listing_fitness_item listing_item_wrap">
						<div class="col-lg-3 col-sm-12 no_padding image_gallery_wrap">
							<div class="product_image full_image_slider slider">
							  <div class="img_wrap"><img src="images/fitness_item1.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item2.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item3.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item4.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item5.jpg" /></div>
							</div>
							<div class="product_image_thumb thumb_image_slider slider">
							  <div class="img_wrap"><img src="images/fitness_item1.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item2.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item3.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item4.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item5.jpg" /></div>
							</div>
						</div>
						<div class="col-lg-9 col-sm-12  no_padding item_details_wrap">
							<div class="item_title_wrap">
								<h3 class="item_title">Reach - The Fitness Store</h3>
								<div class="review_details"><i class="fas fa-star"></i> 6.5 <span class="review_text good">Good</span> <span class="reviews_count">(7 Reviews)</span></div>
							</div>
							<div class="item_detail_wrap">
								<div class="detail_wrap">
									<div class="service_address"><i class="fas fa-map-marker-alt"></i>&nbsp; 17, Boulevard Plaza Tower Downtown Dubai, UAE</div>
									<div class="service_distance"><i class="fas fa-map-marker-alt"></i>&nbsp; 2.3 km away</div>
									<div class="service_kinds"><span class="btn btn-primary">Men</span><span class="btn btn-primary">Kids</span></div>
									<div class="service_features">
										<div class="feature With_fitne_small"><a href="#"><span class="item_icon v_tour"><img src="images/plane-ticket.png"></span>Virtual tour</a></div>
										<div class="feature With_fitne_small"><a href="#"><span class="item_icon f_friends"><img src="images/group.png"></span>Find Friends</a></div>
										<div class="feature With_fitne_small"><a href="#"><span class="item_icon f_trial"><img src="images/free.png"></span>Free Trial</a></div>
									</div>
									<div class="service_offers">
										<div class="feature With_fitne_small"><a href="#"><span class="item_icon offers"><img src="images/price-label.png"></span>5 Offers</a></div>
										<div class="feature"><a href="#"><span class="item_icon cashback"><img src="images/wallet_cash.png"></span>Cashback AED 50</a></div>
									</div>
									<div class="service_activity">
										<h2>Activity</h2>
										<div class="discription">Beutics is a marketplace for Beauty, Fitness & Wellness providers. With Beutics App the world of salons, spas, fitness   read more....</div>
									</div>
									<div class="trainer_speaks"><span class="Title">Trainer Speak:</span> English, Franch, Japenes</div>
									<div class="service_available"><span class="btn btn-primary">Dancing</span><span class="btn btn-primary">Zumba</span> <span class="btn btn-primary">Swimming</span> <span class="btn btn-primary">Walking</span></div>
								</div>
								<div class="price_details_wrap">
									<div class="pricing_item">
										<div class="title">Online</div>
										<div class="pricing">
											<div class="icon"><img src="images/video-chat.png"></div>
											<div class="price">
												<div class="s_label">Starting from</div>
												AED 100
											</div>
										</div>
									</div>
									<div class="pricing_item">
										<div class="title">Studio</div>
										<div class="pricing">
											<div class="icon"><img src="images/fitness-gym.png"></div>
											<div class="price">
												<div class="s_label">Starting from</div>
												AED 100
											</div>
										</div>
									</div>
									<div class="pricing_item">
										<div class="title">Home</div>
										<div class="pricing">
											<div class="icon"><img src="images/room.png"></div>
											<div class="price">
												<div class="s_label">Starting from</div>
												AED 100
											</div>
										</div>
									</div>
									<div class="pricing_item">
										<div class="btn btn-primary explore_btn">Explore</div>	
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="listing_fitness_item listing_item_wrap">
						<div class="col-lg-3 col-sm-12 no_padding image_gallery_wrap">
							<div class="product_image full_image_slider slider">
							  <div class="img_wrap"><img src="images/fitness_item1.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item2.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item3.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item4.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item5.jpg" /></div>
							</div>
							<div class="product_image_thumb thumb_image_slider slider">
							  <div class="img_wrap"><img src="images/fitness_item1.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item2.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item3.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item4.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item5.jpg" /></div>
							</div>
						</div>
						<div class="col-lg-9 col-sm-12  no_padding item_details_wrap">
							<div class="item_title_wrap">
								<h3 class="item_title">Reach - The Fitness Store</h3>
								<div class="review_details"><i class="fas fa-star"></i> 6.5 <span class="review_text good">Good</span> <span class="reviews_count">(7 Reviews)</span></div>
							</div>
							<div class="item_detail_wrap">
								<div class="detail_wrap">
									<div class="service_address"><i class="fas fa-map-marker-alt"></i>&nbsp; 17, Boulevard Plaza Tower Downtown Dubai, UAE</div>
									<div class="service_distance"><i class="fas fa-map-marker-alt"></i>&nbsp; 2.3 km away</div>
									<div class="service_kinds"><span class="btn btn-primary">Men</span><span class="btn btn-primary">Kids</span></div>
									<div class="service_features">
										<div class="feature With_fitne_small"><a href="#"><span class="item_icon v_tour"><img src="images/plane-ticket.png"></span>Virtual tour</a></div>
										<div class="feature With_fitne_small"><a href="#"><span class="item_icon f_friends"><img src="images/group.png"></span>Find Friends</a></div>
										<div class="feature With_fitne_small"><a href="#"><span class="item_icon f_trial"><img src="images/free.png"></span>Free Trial</a></div>
									</div>
									<div class="service_offers">
										<div class="feature With_fitne_small"><a href="#"><span class="item_icon offers"><img src="images/price-label.png"></span>5 Offers</a></div>
										<div class="feature"><a href="#"><span class="item_icon cashback"><img src="images/wallet_cash.png"></span>Cashback AED 50</a></div>
									</div>
									<div class="service_activity">
										<h2>Activity</h2>
										<div class="discription">Beutics is a marketplace for Beauty, Fitness & Wellness providers. With Beutics App the world of salons, spas, fitness   read more....</div>
									</div>
									<div class="trainer_speaks"><span class="Title">Trainer Speak:</span> English, Franch, Japenes</div>
									<div class="service_available"><span class="btn btn-primary">Dancing</span><span class="btn btn-primary">Zumba</span> <span class="btn btn-primary">Swimming</span> <span class="btn btn-primary">Walking</span></div>
								</div>
								<div class="price_details_wrap">
									<div class="pricing_item">
										<div class="title">Online</div>
										<div class="pricing">
											<div class="icon"><img src="images/video-chat.png"></div>
											<div class="price">
												<div class="s_label">Starting from</div>
												AED 100
											</div>
										</div>
									</div>
									<div class="pricing_item">
										<div class="title">Studio</div>
										<div class="pricing">
											<div class="icon"><img src="images/fitness-gym.png"></div>
											<div class="price">
												<div class="s_label">Starting from</div>
												AED 100
											</div>
										</div>
									</div>
									<div class="pricing_item">
										<div class="title">Home</div>
										<div class="pricing">
											<div class="icon"><img src="images/room.png"></div>
											<div class="price">
												<div class="s_label">Starting from</div>
												AED 100
											</div>
										</div>
									</div>
									<div class="pricing_item">
										<div class="btn btn-primary explore_btn">Explore</div>	
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="listing_fitness_item listing_item_wrap">
						<div class="col-lg-3 col-sm-12  no_padding image_gallery_wrap">
							<div class="product_image full_image_slider slider">
							  <div class="img_wrap"><img src="images/fitness_item1.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item2.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item3.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item4.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item5.jpg" /></div>
							</div>
							<div class="product_image_thumb thumb_image_slider slider">
							  <div class="img_wrap"><img src="images/fitness_item1.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item2.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item3.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item4.jpg" /></div>
							  <div class="img_wrap"><img src="images/fitness_item5.jpg" /></div>
							</div>
						</div>
						<div class="col-lg-9 col-sm-12  no_padding item_details_wrap">
							<div class="item_title_wrap">
								<h3 class="item_title">Reach - The Fitness Store</h3>
								<div class="review_details"><i class="fas fa-star"></i> 6.5 <span class="review_text good">Good</span> <span class="reviews_count">(7 Reviews)</span></div>
							</div>
							<div class="item_detail_wrap">
								<div class="detail_wrap">
									<div class="service_address"><i class="fas fa-map-marker-alt"></i>&nbsp; 17, Boulevard Plaza Tower Downtown Dubai, UAE</div>
									<div class="service_distance"><i class="fas fa-map-marker-alt"></i>&nbsp; 2.3 km away</div>
									<div class="service_kinds"><span class="btn btn-primary">Men</span><span class="btn btn-primary">Kids</span></div>
									<div class="service_features">
										<div class="feature With_fitne_small"><a href="#"><span class="item_icon v_tour"><img src="images/plane-ticket.png"></span>Virtual tour</a></div>
										<div class="feature With_fitne_small"><a href="#"><span class="item_icon f_friends"><img src="images/group.png"></span>Find Friends</a></div>
										<div class="feature With_fitne_small"><a href="#"><span class="item_icon f_trial"><img src="images/free.png"></span>Free Trial</a></div>
									</div>
									<div class="service_offers">
										<div class="feature With_fitne_small"><a href="#"><span class="item_icon offers"><img src="images/price-label.png"></span>5 Offers</a></div>
										<div class="feature"><a href="#"><span class="item_icon cashback"><img src="images/wallet_cash.png"></span>Cashback AED 50</a></div>
									</div>
									<div class="service_activity">
										<h2>Activity</h2>
										<div class="discription">Beutics is a marketplace for Beauty, Fitness & Wellness providers. With Beutics App the world of salons, spas, fitness   read more....</div>
									</div>
									<div class="trainer_speaks"><span class="Title">Trainer Speak:</span> English, Franch, Japenes</div>
									<div class="service_available"><span class="btn btn-primary">Dancing</span><span class="btn btn-primary">Zumba</span> <span class="btn btn-primary">Swimming</span> <span class="btn btn-primary">Walking</span></div>
								</div>
								<div class="price_details_wrap">
									<div class="pricing_item">
										<div class="title">Online</div>
										<div class="pricing">
											<div class="icon"><img src="images/video-chat.png"></div>
											<div class="price">
												<div class="s_label">Starting from</div>
												AED 100
											</div>
										</div>
									</div>
									<div class="pricing_item">
										<div class="title">Studio</div>
										<div class="pricing">
											<div class="icon"><img src="images/fitness-gym.png"></div>
											<div class="price">
												<div class="s_label">Starting from</div>
												AED 100
											</div>
										</div>
									</div>
									<div class="pricing_item">
										<div class="title">Home</div>
										<div class="pricing">
											<div class="icon"><img src="images/room.png"></div>
											<div class="price">
												<div class="s_label">Starting from</div>
												AED 100
											</div>
										</div>
									</div>
									<div class="pricing_item">
										<div class="btn btn-primary explore_btn">Explore</div>	
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="pagination_wrapper">
					<ul class="pagination pagination-lg">
					  <li class="page-item"><a class="page-link" href="#"><i class="fas fa-long-arrow-alt-left"></i></a></li>
					  <li class="page-item active"><a class="page-link" href="#">1</a></li>
					  <li class="page-item"><a class="page-link" href="#">2</a></li>
					  <li class="page-item"><a class="page-link" href="#">3</a></li>
					  <li class="page-item"><a class="page-link" href="#"><i class="fas fa-long-arrow-alt-right"></i></a></li>
					</ul>
				</div>
			</div>
			<div class="col-lg-3 col-sm-12 page_sidebar">
				<div class="page_sidebar_wrap">
					<div class="related_blog_items_wrap">
					<div class="side_bar_heads">Why Buy through Beutics</div>
					<div class="related_blog_items">
						<div class="blog_item">
							<div class="blog_title">Sufjan Stevens: the Spokesman for Sanity</div>
							<div class="published_on"><i class="far fa-alarm-clock"></i> 2 Years ago</div>
						</div>
						<div class="blog_item">
							<div class="blog_title">Sufjan Stevens: the Spokesman for Sanity</div>
							<div class="published_on"><i class="far fa-alarm-clock"></i> 2 Years ago</div>
						</div>
						<div class="blog_item">
							<div class="blog_title">Sufjan Stevens: the Spokesman for Sanity</div>
							<div class="published_on"><i class="far fa-alarm-clock"></i> 2 Years ago</div>
						</div>
						<div class="blog_item">
							<div class="blog_title">Sufjan Stevens: the Spokesman for Sanity</div>
							<div class="published_on"><i class="far fa-alarm-clock"></i> 2 Years ago</div>
						</div>
					</div>
					</div>

					<div class="feature_blogs_section">
						<div class="side_feature_blogs_slider slider">
						    <div class="cu_slider_item">
						    	<a  href="#!" class="category_link_wrap">
							      <div class="image-wrap">
							        <img src="images/blog_img1.png" />
							      </div>
							      <div class="slider-caption-wrap">
							          <div class="caption_title">
							          	Lorem Ipsum is simply dummy text of the printing 
							          </div>
							      </div>
							    </a>
						    </div>
						    <div class="cu_slider_item">
						    	<a  href="#!" class="category_link_wrap">
							      <div class="image-wrap">
							        <img src="images/blog_img1.png" />
							      </div>
							      <div class="slider-caption-wrap">
							          <div class="caption_title">
							          	Lorem Ipsum dummy text of the printing  of the printing 
							          </div>
							      </div>
							    </a>
						    </div>
						    <div class="cu_slider_item">
						    	<a  href="#!" class="category_link_wrap">
							      <div class="image-wrap">
							        <img src="images/blog_img1.png" />
							      </div>
							      <div class="slider-caption-wrap">
							          <div class="caption_title">
							          	Lorem Ipsum y text of the primmy text of the printing 
							          </div>
							      </div>
							    </a>
						    </div>
						    
					  	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- page_main_content_section end -->

<!-- beutics_blog_section -->
<section class="beutics_blog_section padding_tb_60">
	<div class="container-fluid">
		<!-- blogs -->
		<div class="blog_section row">
			<div class="blog_slider_section col-md-9 offset-md-3 no_padding">
				<div class="blog_slider slider">
				    <div class="cu_slider_item">
				    	<div class="blog_wrap">
					      <div class="slider-caption-wrap">
					      	<div class="caption_title">
					          	<h2>What is Lorem Ipsum ?</h2>
					          </div>
					      	<div class="discription">
					      		Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					      	</div>
					          
					      </div>
					    </div>
				    </div>
				    <div class="cu_slider_item">
				    	<div class="blog_wrap">
					      <div class="slider-caption-wrap">
					      	<div class="caption_title">
					          	<h2>What is Lorem Ipsum ?</h2>
					          </div>
					      	<div class="discription">
					      		Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					      	</div>
					          
					      </div>
					    </div>
				    </div>
				    <div class="cu_slider_item">
				    	<div class="blog_wrap">
					      <div class="slider-caption-wrap">
					      	<div class="caption_title">
					          	<h2>What is Lorem Ipsum ?</h2>
					          </div>
					      	<div class="discription">
					      		Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					      	</div>
					          
					      </div>
					    </div>
				    </div>
				    <div class="cu_slider_item">
				    	<div class="blog_wrap">
					      <div class="slider-caption-wrap">
					      	<div class="caption_title">
					          	<h2>What is Lorem Ipsum ?</h2>
					          </div>
					      	<div class="discription">
					      		Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					      	</div>
					          
					      </div>
					    </div>
				    </div>
				    <div class="cu_slider_item">
				    	<div class="blog_wrap">
					      <div class="slider-caption-wrap">
					      	<div class="caption_title">
					          	<h2>What is Lorem Ipsum ?</h2>
					          </div>
					      	<div class="discription">
					      		Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					      	</div>
					          
					      </div>
					    </div>
				    </div>
				    
			  	</div>
			</div>
		</div>
	</div>
</section>
<!-- beutics_blog_section End-->
@endsection