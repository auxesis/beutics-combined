@extends('layouts.layout')
@section('title','Beauty Listing')
@section('content')
<div class="header_height header_bottom_p"></div>
<!-- breadcrumbs_section -->
<section class="breadcrumbs_section">
	<div class="container">
		<div id="block-breadcrumbs" class="block-breadcrumbs">
		  <nav role="navigation" aria-label="breadcrumb">
		    <ol class="breadcrumb">
		        <li class="breadcrumb-item"><a href="#">Home</a></li>
		        <li class="breadcrumb-item active">Beauty</li>
		    </ol>
		  </nav>
		</div>
	</div>
</section>
<!-- breadcrumbs_section END-->
<!-- page_filter_section -->
<section class="page_filter_section">
	<div class="container">
		<div id="block_page_filter" class="block_page_filter">
		 <h3 class="page_filter_title">Beauty</h3>
		 <div class="page_filter_wrap">
			 <div class="service_on_wrap">
		      	<div class="form-check-inline">
				  <label class="form-check-label">
				    <input type="radio" class="form-check-input radio_top_fit" name="optradio" value="option1">All
				  </label>
				</div>
				<div class="form-check-inline">
				  <label class="form-check-label">
				    <input type="radio" class="form-check-input radio_top_fit " name="optradio" value="option2">Artist
				  </label>
				</div>
				<div class="form-check-inline">
				  <label class="form-check-label">
				    <input type="radio" class="form-check-input radio_top_fit" name="optradio" value="option3">In Store
				  </label>
				</div>
		    </div>
<div class="filter_shortby_wrap" id="mybutton" style="display: inline-grid;">
		    	<button class="filter_wrapper" style="background-color: transparent; border: none; padding-left: 15px;">
		    		<div class="filter_btn filt_btn"><i class="fal fa-sliders-v"></i>&nbsp; Filter</div>
		    		<div class="filter_options_wrap"></div>
				</button>
		    	<button class="shortby_wrapper" style="background-color: transparent; border: none;">
		    		<div class="shortby_btn filt_btn"><i class="fal fa-sort-amount-down"></i>&nbsp; Sort by</div>
		    		<div class="shortby_options_wrap"></div>
				</button>
		    </div>
		</div>
		</div>
	</div>
</section>
<!-- page_filter_section END-->

<!-- page_main_content_section -->
<!-- <section class="page_main_content_section">
	<div class="container">
		<div class="row no_padding page_main_content_wrapper">
			<div class="col-md-9 no_padding">
				<div class="page_main_content_wrap">
					
				</div>
			</div>
			<div class="col-md-3 no_padding">
				<div class="page_sidebar_wrap">
					
				</div>
			</div>
		</div>
	</div>
</section> -->
@if(!empty($SpStore))
<section class="page_main_content_section">
	<div class="container">
		<div class="row no_padding page_main_content_wrapper">
			<div class="col-lg-9 col-sm-12 main_content_wrap">
				<div class="page_main_content_wrap listing_items listing_beauty_wellness_items">
					<div class="filter_selected_wrap">
						<ul class="filter_selected">
							<li class="filter_item"><span>Hair Color <i class="fal fa-times remove"></i></span></li>
							<li class="filter_item"><span>Facial <i class="fal fa-times remove"></i></span></li>
							<li class="filter_item"><span>Make Up <i class="fal fa-times remove"></i></span></li>
						</ul>
					</div>


					@foreach($SpStore as $key => $data)
					<div class="listing_beauty_wellness_item listing_item_wrap">
						<div class="col-lg-4 col-sm-12 no_padding image_gallery_wrap">
                            
							<div class="product_image full_image_slider slider">
								@foreach($data->Gallerys as $key => $Gallery)
							  <div class="img_wrap"><img src="{{asset('sp_uploads/gallery/image/'.$Gallery['file_name'])}}"/></div>
							  @endforeach
							</div>
                           
                          
							<div class="product_image_thumb thumb_image_slider slider">
								 @foreach($data->Gallerys as $key => $Gallery)
							  <div class="img_wrap"><img src="{{asset('sp_uploads/gallery/image/'.$Gallery['file_name'])}}"/></div>
							  @endforeach
							</div>
							
						</div>
						<div class="col-lg-9 col-sm-12 no_padding item_details_wrap">
							<div class="item_title_wrap">
								<div class="featured_btn"><span class="featured">Featured</span></div>
								<h3 class="item_title">{{$data->store_name}}</h3>
								
								<div class="review_details"><i class="fas fa-star"></i>{{$data->average_rating}} <span class="review_text good"> Good</span> <span class="reviews_count">({{$data->total_reviewers}}  Reviews)</span></div>
								
							</div>
							<div class="item_detail_wrap">
								<div class="detail_wrap">
									<div class="service_address"><i class="fas fa-map-marker-alt"></i>&nbsp; {{$data->address}}</div>
									<div class="service_distance"><i class="fas fa-map-marker-alt"></i>&nbsp; 2.3 km away</div>
									
									<div class="service_features">
										@if($data->is_virtual_tour == '1'))
										<div class="feature"><span class="item_icon v_tour"><img src="images/plane-ticket.png"></span>Virtual tour</div>
										@endif
										@if($data->is_free_trial == '1'))
										<div class="feature"><span class="item_icon f_trial"><img src="images/free.png"></span>Free Trial</div>
										@endif
									</div>
									
									<div class="staff_nationality">
										<div class="sec_title">Staff Nationality</div>
										<div class="staff_nati_flags">

											<div class="staff_nati_flags_wrap">
                                            @foreach($data->StaffNationality as $key => $StaffNationalitys)
											<div class="flag united_kingdom"><img src="{{asset('sp_uploads/flags/'.$StaffNationalitys['flag_url'].'.svg')}}"></div>
											@endforeach
                                            
											

											<!-- <div class="flag australia"><img src="images/australia.png"></div> -->

											<!-- <div class="flag usa"><img src="images/united-states-of-america.png"></div> -->

											</div>

										</div>
									</div>
									<div class="service_offers offers_coupon">
										<div class="feature"><span class="item_icon cashback"><img src="images/wallet_cash.png"></span>Cashback AED 50</div>
										<div class="feature coupon"><span class="coupon_code">Promo Code : &nbsp;</span> {{$data->PromoCode}}</div>
									</div>
								</div>
								<div class="price_details_wrap">
									<div class="services_matched">3 of services match</div>
									<div class="pricing_item">
										<div class="services">Hair color,  Facial, Make-up, Hair Cutting  Hair color, Facial, Make-up, Hair Trimming ,,,</div>
										<div class="pricing">
											<div class="price">
												<span class="del">AED 120</span>
												AED 100
											</div>
										</div>
									</div>
									<div class="pricing_item">
										<div class="btn btn-primary explore_btn"><a href="{{$data->store_url}}">Explore</a></div>	
									</div>
								</div>
							</div>
						</div>
					</div>
                    @endforeach

                    <div class="features_category_row landing_page_features_category">
						<div class="feature_category">
							<h3 class="feature_title">Beauty . Fitness . Wellness</h3>
							<div class="feature_detail">Various choices in the market for Beauty, Fitness & Wellness providers</div>
						</div>
					</div>
             </div>

                <!-- <div class="pagination_wrapper">
					<ul class="pagination pagination-lg">
					  <li class="page-item"><a class="page-link" href="#"><i class="fas fa-long-arrow-alt-left"></i></a></li>
					  <li class="page-item active"><a class="page-link" href="#">1</a></li>
					  <li class="page-item"><a class="page-link" href="#">2</a></li>
					  <li class="page-item"><a class="page-link" href="#">3</a></li>
					  <li class="page-item"><a class="page-link" href="#"><i class="fas fa-long-arrow-alt-right"></i></a></li>
					</ul>
				</div> -->

           

               
					

				
			</div>
            @if(!empty($pagesectionArr['Why Buy Through Beutics Content']))
			<div class="col-lg-3 col-sm-12 page_sidebar">
				<div class="page_sidebar_wrap">
					<div class="related_blog_items_wrap">
					<div class="side_bar_heads">Why Buy through Beutics</div>
					<div class="related_blog_items">

						@foreach($pagesectionArr['Why Buy Through Beutics Content'] as $WhyBuyBeutics)
						<div class="blog_item">
							<div class="blog_title">{{$WhyBuyBeutics->title}}: {{$WhyBuyBeutics->description}}</div>
							<div class="published_on"><i class="far fa-alarm-clock"></i> {{time_elapsed_string($WhyBuyBeutics->created_at)}}</div>
						</div>
						@endforeach
						
						
					</div>
					</div>
                    @if(!empty($pagesectionArr['Why Buy Through Beutics Banner']))
					<div class="feature_blogs_section">
						<div class="side_feature_blogs_slider slider">
                             @foreach($pagesectionArr['Why Buy Through Beutics Banner'] as $WhyBuyBeuticsBanner)
						    <div class="cu_slider_item">
						    	<a  href="#!" class="category_link_wrap">
							      <div class="image-wrap">
							        <img src="{{asset('sp_uploads/pages/'.$WhyBuyBeuticsBanner->page_name.'/'.$WhyBuyBeuticsBanner->image)}}" />
							      </div>
							      <div class="slider-caption-wrap">
							          <div class="caption_title">
							          	{{$WhyBuyBeuticsBanner->title}}
							          </div>
							      </div>
							    </a>
						    </div>
						    @endforeach
						      
					  	</div>
					</div>
                    @endif

				</div>
			</div>
         @endif

		</div>
	</div>
</section>
@endif
<!-- page_main_content_section end -->

<!-- beutics_blog_section -->
@if(isset($pagesectionArr['Social Media Content']) && !empty($pagesectionArr['Social Media Content']))
<section class="beutics_blog_section padding_tb_60">
	<div class="container-fluid">
		<!-- blogs -->
		<div class="blog_section row">
			<div class="blog_slider_section col-md-9 offset-md-3 no_padding">
				<div class="blog_slider slider">
				    <div class="cu_slider_item">
				    	@foreach($pagesectionArr['Social Media Content'] as $mediaContent)
				    	<div class="blog_wrap">
					      <div class="slider-caption-wrap">
					      	<div class="caption_title">
					          	<h2>{{$mediaContent->title}}</h2>
					          </div>
					      	<div class="discription">
					      		{{$mediaContent->description}}
					      	</div>
					          
					      </div>
					    </div>
					    @endforeach
				    </div>
				     
			  	</div>
			</div>
		</div>
	</div>
</section>
@endif
<!-- beutics_blog_section End-->
@endsection

<?php
function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

?>