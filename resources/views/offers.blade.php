@extends('layouts.offers')
@section('offers')
<div class="header_bottom_p offer_listing_header"></div>
<!-- breadcrumbs_section -->
<section class="breadcrumbs_section">
	<div class="container">
		<div id="block-breadcrumbs" class="block-breadcrumbs">
		  <nav role="navigation" aria-label="breadcrumb">
		    <ol class="breadcrumb">
		        <li class="breadcrumb-item"><a href="#">Home</a></li>
		        <li class="breadcrumb-item active">Client Listing</li>
		    </ol>
		  </nav>
		</div>
	</div>
</section>
<!-- breadcrumbs_section END-->
<!-- page_filter_section -->
<section class="page_filter_section">
	<div class="container">
		<div id="block_page_filter" class="block_page_filter">
		 <!-- <h3 class="page_filter_title">Fitness</h3> -->
		 <div class="page_filter_wrap">
			   <div class="service_on_wrap">
		     	   <ul class="Beaty_tabs_animation">
                        <li>
                            <a href="#"><img src="images/Beauty_at_home.png" />
                              <span class="Beaty_at_home_tab_text">Beauty At Home</span>
                            </a>
                        </li>

                        <li>
                            <a href="#"><img src="images/lipstick.png" />
                              <span class="Beaty_at_home_tab_text">Beauty</span>
                            </a>
                        </li>

                        <li>
                            <a href="#"><img src="images/dumbble.png" />
                              <span class="Beaty_at_home_tab_text">Fittness</span>
                            </a>
                        </li>
                        <li>
                            <a href="#"><img src="images/yoga_master.png" />
                              <span class="Beaty_at_home_tab_text">Wellness</span>
                            </a>
                        </li>
                    </ul>

                    <ul class="saff_form mt-4">                   
                        <li class="pr-3"><input type="radio" id="test1" name="radio-group">
                         <label for="test1"><b>Salone</b></label>
                        </li>            
                         <li><input type="radio" id="test3" name="radio-group" checked="checked">
                         <label for="test3"><b>Clinical Beauty</b></label>                     
                        </li>
                        </ul>
		       </div>
               <div class="filter_shortby_wrap" id="mybutton" style="display: inline-grid;">
		    	<button class="filter_wrapper" style="background-color: transparent; border: none; padding-left: 15px;">
		    		<div class="filter_btn filt_btn"><i class="fal fa-sliders-v"></i>&nbsp; Filter</div>
		    		<div class="filter_options_wrap"></div>
				</button>
		    	<button class="shortby_wrapper" style="background-color: transparent; border: none;">
		    		<div class="shortby_btn filt_btn"><i class="fal fa-sort-amount-down"></i>&nbsp; Sort by</div>
		    		<div class="shortby_options_wrap"></div>
				</button>
		    </div>
		</div>
		</div>
	</div>
</section>
<!-- page_filter_section END-->

<!-- Bundle Off Section -->
<section class="Bundle_section">
 <div class="container">
     <div class="title_name_section">
        Bundle Off
        <span class="see_all_top"> <a href="#">See All</a> </span>
     </div>
    <!-- Start Row -->
     <div class="row">
         <!-- Bundel Section_one -->
         <div class="col-lg-3 col-sm-6">
           <div class="bundle_section_one">
               <div class="Bundle_image">
                   <div class="off_image">
                       80%
                       <span class="off_small_text_image">Off</span>
                   </div>
               <img src="images/bundle_one.png" />
               </div>
               <div class="bottom_details_section">
                   <h5 class="more_bottom_heading">More Than Looks & Beauty 
                    Salon</h5>
                    <div class="price_bundle">
                        <span class="bundle_cut_price"> AED 800  </span>
                        <span class="without_cut_price"> AED 800 </span>
                    </div>
                    <div class="tags_for_coupon mt-2">
                        <span class="tag_coupon_one"> Hair cutting </span>
                        <span class="tag_coupon_one"> Hair color </span>
                        <span class="tag_coupon_one"> Trimming </span>
                    </div>
                    <div class="tags_for_coupon mt-1">
                        <span class="tag_coupon_one"> Hair cutting </span>
                        <span class="tag_coupon_one"> Hair color </span>
                        <span class="tag_coupon_one"> Trimming </span>
                    </div>
               </div>
           </div>
         </div>

         <!-- End Here section -->
          <!-- Bundel Section_one -->
          <div class="col-lg-3 col-sm-6">
            <div class="bundle_section_one">
                <div class="Bundle_image">
                    <div class="off_image">
                        80%
                        <span class="off_small_text_image">Off</span>
                    </div>
                <img src="images/bundle_two.png" />
                </div>
                <div class="bottom_details_section">
                    <h5 class="more_bottom_heading">More Than Looks & Beauty 
                     Salon</h5>
                     <div class="price_bundle">
                         <span class="bundle_cut_price"> AED 800  </span>
                         <span class="without_cut_price"> AED 800 </span>
                     </div>
                     <div class="tags_for_coupon mt-2">
                         <span class="tag_coupon_one"> Hair cutting </span>
                         <span class="tag_coupon_one"> Hair color </span>
                         <span class="tag_coupon_one"> Trimming </span>
                     </div>
                     <div class="tags_for_coupon mt-1">
                         <span class="tag_coupon_one"> Hair cutting </span>
                         <span class="tag_coupon_one"> Hair color </span>
                         <span class="tag_coupon_one"> Trimming </span>
                     </div>
                </div>
            </div>
          </div>
 
          <!-- End Here section -->
           <!-- Bundel Section_one -->
         <div class="col-lg-3 col-sm-6">
            <div class="bundle_section_one">
                <div class="Bundle_image">
                    <div class="off_image">
                        80%
                        <span class="off_small_text_image">Off</span>
                    </div>
                <img src="images/bundle_three.png" />
                </div>
                <div class="bottom_details_section">
                    <h5 class="more_bottom_heading">More Than Looks & Beauty 
                     Salon</h5>
                     <div class="price_bundle">
                         <span class="bundle_cut_price"> AED 800  </span>
                         <span class="without_cut_price"> AED 800 </span>
                     </div>
                     <div class="tags_for_coupon mt-2">
                         <span class="tag_coupon_one"> Hair cutting </span>
                         <span class="tag_coupon_one"> Hair color </span>
                         <span class="tag_coupon_one"> Trimming </span>
                     </div>
                     <div class="tags_for_coupon mt-1">
                         <span class="tag_coupon_one"> Hair cutting </span>
                         <span class="tag_coupon_one"> Hair color </span>
                         <span class="tag_coupon_one"> Trimming </span>
                     </div>
                </div>
            </div>
          </div>
 
          <!-- End Here section -->
           <!-- Bundel Section_one -->
         <div class="col-lg-3 col-sm-6">
            <div class="bundle_section_one">
                <div class="Bundle_image">
                    <div class="off_image">
                        80%
                        <span class="off_small_text_image">Off</span>
                    </div>
                <img src="images/bundle_four.png" />
                </div>
                <div class="bottom_details_section">
                    <h5 class="more_bottom_heading">More Than Looks & Beauty 
                     Salon</h5>
                     <div class="price_bundle">
                         <span class="bundle_cut_price"> AED 800  </span>
                         <span class="without_cut_price"> AED 800 </span>
                     </div>
                     <div class="tags_for_coupon mt-2">
                         <span class="tag_coupon_one"> Hair cutting </span>
                         <span class="tag_coupon_one"> Hair color </span>
                         <span class="tag_coupon_one"> Trimming </span>
                     </div>
                     <div class="tags_for_coupon mt-1">
                         <span class="tag_coupon_one"> Hair cutting </span>
                         <span class="tag_coupon_one"> Hair color </span>
                         <span class="tag_coupon_one"> Trimming </span>
                     </div>
                </div>
            </div>
          </div>
 
          <!-- End Here section -->
     </div>
     <!-- End Row -->
     <div class="row mt-4">
        <!-- Bundel Section_one -->
        <div class="col-lg-3 col-sm-6">
          <div class="bundle_section_one">
              <div class="Bundle_image">
                  <div class="off_image">
                      80%
                      <span class="off_small_text_image">Off</span>
                  </div>
              <img src="images/bundle_9.png" />
              </div>
              <div class="bottom_details_section">
                  <h5 class="more_bottom_heading">More Than Looks & Beauty 
                   Salon</h5>
                   <div class="price_bundle">
                       <span class="bundle_cut_price"> AED 800  </span>
                       <span class="without_cut_price"> AED 800 </span>
                   </div>
                   <div class="tags_for_coupon mt-2">
                       <span class="tag_coupon_one"> Hair cutting </span>
                       <span class="tag_coupon_one"> Hair color </span>
                       <span class="tag_coupon_one"> Trimming </span>
                   </div>
                   <div class="tags_for_coupon mt-1">
                       <span class="tag_coupon_one"> Hair cutting </span>
                       <span class="tag_coupon_one"> Hair color </span>
                       <span class="tag_coupon_one"> Trimming </span>
                   </div>
              </div>
          </div>
        </div>

        <!-- End Here section -->
         <!-- Bundel Section_one -->
         <div class="col-lg-3 col-sm-6">
           <div class="bundle_section_one">
               <div class="Bundle_image">
                   <div class="off_image">
                       80%
                       <span class="off_small_text_image">Off</span>
                   </div>
               <img src="images/bundle_10.png" />
               </div>
               <div class="bottom_details_section">
                   <h5 class="more_bottom_heading">More Than Looks & Beauty 
                    Salon</h5>
                    <div class="price_bundle">
                        <span class="bundle_cut_price"> AED 800  </span>
                        <span class="without_cut_price"> AED 800 </span>
                    </div>
                    <div class="tags_for_coupon mt-2">
                        <span class="tag_coupon_one"> Hair cutting </span>
                        <span class="tag_coupon_one"> Hair color </span>
                        <span class="tag_coupon_one"> Trimming </span>
                    </div>
                    <div class="tags_for_coupon mt-1">
                        <span class="tag_coupon_one"> Hair cutting </span>
                        <span class="tag_coupon_one"> Hair color </span>
                        <span class="tag_coupon_one"> Trimming </span>
                    </div>
               </div>
           </div>
         </div>

         <!-- End Here section -->
          <!-- Bundel Section_one -->
        <div class="col-lg-3 col-sm-6">
           <div class="bundle_section_one">
               <div class="Bundle_image">
                   <div class="off_image">
                       80%
                       <span class="off_small_text_image">Off</span>
                   </div>
               <img src="images/bundle_12.png" />
               </div>
               <div class="bottom_details_section">
                   <h5 class="more_bottom_heading">More Than Looks & Beauty 
                    Salon</h5>
                    <div class="price_bundle">
                        <span class="bundle_cut_price"> AED 800  </span>
                        <span class="without_cut_price"> AED 800 </span>
                    </div>
                    <div class="tags_for_coupon mt-2">
                        <span class="tag_coupon_one"> Hair cutting </span>
                        <span class="tag_coupon_one"> Hair color </span>
                        <span class="tag_coupon_one"> Trimming </span>
                    </div>
                    <div class="tags_for_coupon mt-1">
                        <span class="tag_coupon_one"> Hair cutting </span>
                        <span class="tag_coupon_one"> Hair color </span>
                        <span class="tag_coupon_one"> Trimming </span>
                    </div>
               </div>
           </div>
         </div>

         <!-- End Here section -->
          <!-- Bundel Section_one -->
        <div class="col-lg-3 col-sm-6">
           <div class="bundle_section_one">
               <div class="Bundle_image">
                   <div class="off_image">
                       80%
                       <span class="off_small_text_image">Off</span>
                   </div>
               <img src="images/bundle_13.png" />
               </div>
               <div class="bottom_details_section">
                   <h5 class="more_bottom_heading">More Than Looks & Beauty 
                    Salon</h5>
                    <div class="price_bundle">
                        <span class="bundle_cut_price"> AED 800  </span>
                        <span class="without_cut_price"> AED 800 </span>
                    </div>
                    <div class="tags_for_coupon mt-2">
                        <span class="tag_coupon_one"> Hair cutting </span>
                        <span class="tag_coupon_one"> Hair color </span>
                        <span class="tag_coupon_one"> Trimming </span>
                    </div>
                    <div class="tags_for_coupon mt-1">
                        <span class="tag_coupon_one"> Hair cutting </span>
                        <span class="tag_coupon_one"> Hair color </span>
                        <span class="tag_coupon_one"> Trimming </span>
                    </div>
               </div>
           </div>
         </div>

         <!-- End Here section -->
    </div>
 </div>
</section>

<!-- End Here Section -->

<!-- Bundle Off Section -->
<section class="Bundle_section mt-4">
    <div class="container">
        <div class="title_name_section">
            Combo Off
           <span class="see_all_top"> <a href="#">See All</a> </span>
        </div>
       <!-- Start Row -->
        <div class="row">
            <!-- Bundel Section_one -->
            <div class="col-lg-3 col-sm-6">
              <div class="bundle_section_one">
                  <div class="Bundle_image">
                      <div class="off_image">
                          80%
                          <span class="off_small_text_image">Off</span>
                      </div>
                  <img src="images/bundle_five.png" />
                  </div>
                  <div class="bottom_details_section">
                      <h5 class="more_bottom_heading">More Than Looks & Beauty 
                       Salon</h5>
                       <div class="price_bundle">
                           <span class="bundle_cut_price"> AED 800  </span>
                           <span class="without_cut_price"> AED 800 </span>
                       </div>
                       <div class="tags_for_coupon mt-2">
                           <span class="tag_coupon_one"> Hair cutting </span>
                           <span class="tag_coupon_one"> Hair color </span>
                           <span class="tag_coupon_one"> Trimming </span>
                       </div>
                       <div class="tags_for_coupon mt-1">
                           <span class="tag_coupon_one"> Hair cutting </span>
                           <span class="tag_coupon_one"> Hair color </span>
                           <span class="tag_coupon_one"> Trimming </span>
                       </div>
                  </div>
              </div>
            </div>
   
            <!-- End Here section -->
             <!-- Bundel Section_one -->
             <div class="col-lg-3 col-sm-6">
               <div class="bundle_section_one">
                   <div class="Bundle_image">
                       <div class="off_image">
                           80%
                           <span class="off_small_text_image">Off</span>
                       </div>
                   <img src="images/bundle_six.png" />
                   </div>
                   <div class="bottom_details_section">
                       <h5 class="more_bottom_heading">More Than Looks & Beauty 
                        Salon</h5>
                        <div class="price_bundle">
                            <span class="bundle_cut_price"> AED 800  </span>
                            <span class="without_cut_price"> AED 800 </span>
                        </div>
                        <div class="tags_for_coupon mt-2">
                            <span class="tag_coupon_one"> Hair cutting </span>
                            <span class="tag_coupon_one"> Hair color </span>
                            <span class="tag_coupon_one"> Trimming </span>
                        </div>
                        <div class="tags_for_coupon mt-1">
                            <span class="tag_coupon_one"> Hair cutting </span>
                            <span class="tag_coupon_one"> Hair color </span>
                            <span class="tag_coupon_one"> Trimming </span>
                        </div>
                   </div>
               </div>
             </div>
    
             <!-- End Here section -->
              <!-- Bundel Section_one -->
            <div class="col-lg-3 col-sm-6">
               <div class="bundle_section_one">
                   <div class="Bundle_image">
                       <div class="off_image">
                           80%
                           <span class="off_small_text_image">Off</span>
                       </div>
                   <img src="images/bundle_seven.png" />
                   </div>
                   <div class="bottom_details_section">
                       <h5 class="more_bottom_heading">More Than Looks & Beauty 
                        Salon</h5>
                        <div class="price_bundle">
                            <span class="bundle_cut_price"> AED 800  </span>
                            <span class="without_cut_price"> AED 800 </span>
                        </div>
                        <div class="tags_for_coupon mt-2">
                            <span class="tag_coupon_one"> Hair cutting </span>
                            <span class="tag_coupon_one"> Hair color </span>
                            <span class="tag_coupon_one"> Trimming </span>
                        </div>
                        <div class="tags_for_coupon mt-1">
                            <span class="tag_coupon_one"> Hair cutting </span>
                            <span class="tag_coupon_one"> Hair color </span>
                            <span class="tag_coupon_one"> Trimming </span>
                        </div>
                   </div>
               </div>
             </div>
    
             <!-- End Here section -->
              <!-- Bundel Section_one -->
            <div class="col-lg-3 col-sm-6">
               <div class="bundle_section_one">
                   <div class="Bundle_image">
                       <div class="off_image">
                           80%
                           <span class="off_small_text_image">Off</span>
                       </div>
                   <img src="images/bundle_8.png" />
                   </div>
                   <div class="bottom_details_section">
                       <h5 class="more_bottom_heading">More Than Looks & Beauty 
                        Salon</h5>
                        <div class="price_bundle">
                            <span class="bundle_cut_price"> AED 800  </span>
                            <span class="without_cut_price"> AED 800 </span>
                        </div>
                        <div class="tags_for_coupon mt-2">
                            <span class="tag_coupon_one"> Hair cutting </span>
                            <span class="tag_coupon_one"> Hair color </span>
                            <span class="tag_coupon_one"> Trimming </span>
                        </div>
                        <div class="tags_for_coupon mt-1">
                            <span class="tag_coupon_one"> Hair cutting </span>
                            <span class="tag_coupon_one"> Hair color </span>
                            <span class="tag_coupon_one"> Trimming </span>
                        </div>
                   </div>
               </div>
             </div>
    
             <!-- End Here section -->
        </div>
        <!-- End Row -->
        <div class="row mt-4">
           <!-- Bundel Section_one -->
           <div class="col-lg-3 col-sm-6">
             <div class="bundle_section_one">
                 <div class="Bundle_image">
                     <div class="off_image">
                         80%
                         <span class="off_small_text_image">Off</span>
                     </div>
                 <img src="images/bundle_9.png" />
                 </div>
                 <div class="bottom_details_section">
                     <h5 class="more_bottom_heading">More Than Looks & Beauty 
                      Salon</h5>
                      <div class="price_bundle">
                          <span class="bundle_cut_price"> AED 800  </span>
                          <span class="without_cut_price"> AED 800 </span>
                      </div>
                      <div class="tags_for_coupon mt-2">
                          <span class="tag_coupon_one"> Hair cutting </span>
                          <span class="tag_coupon_one"> Hair color </span>
                          <span class="tag_coupon_one"> Trimming </span>
                      </div>
                      <div class="tags_for_coupon mt-1">
                          <span class="tag_coupon_one"> Hair cutting </span>
                          <span class="tag_coupon_one"> Hair color </span>
                          <span class="tag_coupon_one"> Trimming </span>
                      </div>
                 </div>
             </div>
           </div>
   
           <!-- End Here section -->
            <!-- Bundel Section_one -->
            <div class="col-lg-3 col-sm-6">
              <div class="bundle_section_one">
                  <div class="Bundle_image">
                      <div class="off_image">
                          80%
                          <span class="off_small_text_image">Off</span>
                      </div>
                  <img src="images/bundle_10.png" />
                  </div>
                  <div class="bottom_details_section">
                      <h5 class="more_bottom_heading">More Than Looks & Beauty 
                       Salon</h5>
                       <div class="price_bundle">
                           <span class="bundle_cut_price"> AED 800  </span>
                           <span class="without_cut_price"> AED 800 </span>
                       </div>
                       <div class="tags_for_coupon mt-2">
                           <span class="tag_coupon_one"> Hair cutting </span>
                           <span class="tag_coupon_one"> Hair color </span>
                           <span class="tag_coupon_one"> Trimming </span>
                       </div>
                       <div class="tags_for_coupon mt-1">
                           <span class="tag_coupon_one"> Hair cutting </span>
                           <span class="tag_coupon_one"> Hair color </span>
                           <span class="tag_coupon_one"> Trimming </span>
                       </div>
                  </div>
              </div>
            </div>
   
            <!-- End Here section -->
             <!-- Bundel Section_one -->
           <div class="col-lg-3 col-sm-6">
              <div class="bundle_section_one">
                  <div class="Bundle_image">
                      <div class="off_image">
                          80%
                          <span class="off_small_text_image">Off</span>
                      </div>
                  <img src="images/bundle_11.png" />
                  </div>
                  <div class="bottom_details_section">
                      <h5 class="more_bottom_heading">More Than Looks & Beauty 
                       Salon</h5>
                       <div class="price_bundle">
                           <span class="bundle_cut_price"> AED 800  </span>
                           <span class="without_cut_price"> AED 800 </span>
                       </div>
                       <div class="tags_for_coupon mt-2">
                           <span class="tag_coupon_one"> Hair cutting </span>
                           <span class="tag_coupon_one"> Hair color </span>
                           <span class="tag_coupon_one"> Trimming </span>
                       </div>
                       <div class="tags_for_coupon mt-1">
                           <span class="tag_coupon_one"> Hair cutting </span>
                           <span class="tag_coupon_one"> Hair color </span>
                           <span class="tag_coupon_one"> Trimming </span>
                       </div>
                  </div>
              </div>
            </div>
   
            <!-- End Here section -->
             <!-- Bundel Section_one -->
           <div class="col-lg-3 col-sm-6">
              <div class="bundle_section_one">
                  <div class="Bundle_image">
                      <div class="off_image">
                          80%
                          <span class="off_small_text_image">Off</span>
                      </div>
                  <img src="images/bundle_one.png" />
                  </div>
                  <div class="bottom_details_section">
                      <h5 class="more_bottom_heading">More Than Looks & Beauty 
                       Salon</h5>
                       <div class="price_bundle">
                           <span class="bundle_cut_price"> AED 800  </span>
                           <span class="without_cut_price"> AED 800 </span>
                       </div>
                       <div class="tags_for_coupon mt-2">
                           <span class="tag_coupon_one"> Hair cutting </span>
                           <span class="tag_coupon_one"> Hair color </span>
                           <span class="tag_coupon_one"> Trimming </span>
                       </div>
                       <div class="tags_for_coupon mt-1">
                           <span class="tag_coupon_one"> Hair cutting </span>
                           <span class="tag_coupon_one"> Hair color </span>
                           <span class="tag_coupon_one"> Trimming </span>
                       </div>
                  </div>
              </div>
            </div>
   
            <!-- End Here section -->
       </div>
    </div>
   </section>
   
   <!-- End Here Section -->

   <!-- Choose any Section -->
<section class="Bundle_section mt-4">
    <div class="container-fluid">
        <div class="title_name_section">
            Choose any
           <span class="see_all_top"> <a href="#">See All</a> </span>
        </div>
       <!-- Start Row -->
        <div class="row">
            <!-- Bundel Section_one -->
            <div class="col-lg-3 col-sm-6">
              <div class="bundle_section_one">
                  <div class="Bundle_image">
                      <div class="off_image">
                          80%
                          <span class="off_small_text_image">Off</span>
                      </div>
                  <img src="images/bundle_one.png" />
                  </div>
                  <div class="bottom_details_section">
                      <h5 class="more_bottom_heading">More Than Looks & Beauty 
                       Salon</h5>
                       <div class="price_bundle">
                           <span class="bundle_cut_price"> AED 800  </span>
                           <span class="without_cut_price"> AED 800 </span>
                       </div>
                       <div class="tags_for_coupon mt-2">
                           <span class="tag_coupon_one"> Hair cutting </span>
                           <span class="tag_coupon_one"> Hair color </span>
                           <span class="tag_coupon_one"> Trimming </span>
                       </div>
                       <div class="tags_for_coupon mt-1">
                           <span class="tag_coupon_one"> Hair cutting </span>
                           <span class="tag_coupon_one"> Hair color </span>
                           <span class="tag_coupon_one"> Trimming </span>
                       </div>
                  </div>
              </div>
            </div>
   
            <!-- End Here section -->
             <!-- Bundel Section_one -->
             <div class="col-lg-3 col-sm-6">
               <div class="bundle_section_one">
                   <div class="Bundle_image">
                       <div class="off_image">
                           80%
                           <span class="off_small_text_image">Off</span>
                       </div>
                   <img src="images/bundle_two.png" />
                   </div>
                   <div class="bottom_details_section">
                       <h5 class="more_bottom_heading">More Than Looks & Beauty 
                        Salon</h5>
                        <div class="price_bundle">
                            <span class="bundle_cut_price"> AED 800  </span>
                            <span class="without_cut_price"> AED 800 </span>
                        </div>
                        <div class="tags_for_coupon mt-2">
                            <span class="tag_coupon_one"> Hair cutting </span>
                            <span class="tag_coupon_one"> Hair color </span>
                            <span class="tag_coupon_one"> Trimming </span>
                        </div>
                        <div class="tags_for_coupon mt-1">
                            <span class="tag_coupon_one"> Hair cutting </span>
                            <span class="tag_coupon_one"> Hair color </span>
                            <span class="tag_coupon_one"> Trimming </span>
                        </div>
                   </div>
               </div>
             </div>
    
             <!-- End Here section -->
              <!-- Bundel Section_one -->
            <div class="col-lg-3 col-sm-6">
               <div class="bundle_section_one">
                   <div class="Bundle_image">
                       <div class="off_image">
                           80%
                           <span class="off_small_text_image">Off</span>
                       </div>
                   <img src="images/bundle_three.png" />
                   </div>
                   <div class="bottom_details_section">
                       <h5 class="more_bottom_heading">More Than Looks & Beauty 
                        Salon</h5>
                        <div class="price_bundle">
                            <span class="bundle_cut_price"> AED 800  </span>
                            <span class="without_cut_price"> AED 800 </span>
                        </div>
                        <div class="tags_for_coupon mt-2">
                            <span class="tag_coupon_one"> Hair cutting </span>
                            <span class="tag_coupon_one"> Hair color </span>
                            <span class="tag_coupon_one"> Trimming </span>
                        </div>
                        <div class="tags_for_coupon mt-1">
                            <span class="tag_coupon_one"> Hair cutting </span>
                            <span class="tag_coupon_one"> Hair color </span>
                            <span class="tag_coupon_one"> Trimming </span>
                        </div>
                   </div>
               </div>
             </div>
    
             <!-- End Here section -->
              <!-- Bundel Section_one -->
            <div class="col-lg-3 col-sm-6">
               <div class="bundle_section_one">
                   <div class="Bundle_image">
                       <div class="off_image">
                           80%
                           <span class="off_small_text_image">Off</span>
                       </div>
                   <img src="images/bundle_four.png" />
                   </div>
                   <div class="bottom_details_section">
                       <h5 class="more_bottom_heading">More Than Looks & Beauty 
                        Salon</h5>
                        <div class="price_bundle">
                            <span class="bundle_cut_price"> AED 800  </span>
                            <span class="without_cut_price"> AED 800 </span>
                        </div>
                        <div class="tags_for_coupon mt-2">
                            <span class="tag_coupon_one"> Hair cutting </span>
                            <span class="tag_coupon_one"> Hair color </span>
                            <span class="tag_coupon_one"> Trimming </span>
                        </div>
                        <div class="tags_for_coupon mt-1">
                            <span class="tag_coupon_one"> Hair cutting </span>
                            <span class="tag_coupon_one"> Hair color </span>
                            <span class="tag_coupon_one"> Trimming </span>
                        </div>
                   </div>
               </div>
             </div>
    
             <!-- End Here section -->
        </div>
        <!-- End Row -->
        <div class="row mt-4">
           <!-- Bundel Section_one -->
           <div class="col-lg-3 col-sm-6">
             <div class="bundle_section_one">
                 <div class="Bundle_image">
                     <div class="off_image">
                         80%
                         <span class="off_small_text_image">Off</span>
                     </div>
                 <img src="images/bundle_9.png" />
                 </div>
                 <div class="bottom_details_section">
                     <h5 class="more_bottom_heading">More Than Looks & Beauty 
                      Salon</h5>
                      <div class="price_bundle">
                          <span class="bundle_cut_price"> AED 800  </span>
                          <span class="without_cut_price"> AED 800 </span>
                      </div>
                      <div class="tags_for_coupon mt-2">
                          <span class="tag_coupon_one"> Hair cutting </span>
                          <span class="tag_coupon_one"> Hair color </span>
                          <span class="tag_coupon_one"> Trimming </span>
                      </div>
                      <div class="tags_for_coupon mt-1">
                          <span class="tag_coupon_one"> Hair cutting </span>
                          <span class="tag_coupon_one"> Hair color </span>
                          <span class="tag_coupon_one"> Trimming </span>
                      </div>
                 </div>
             </div>
           </div>
   
           <!-- End Here section -->
            <!-- Bundel Section_one -->
            <div class="col-lg-3 col-sm-6">
              <div class="bundle_section_one">
                  <div class="Bundle_image">
                      <div class="off_image">
                          80%
                          <span class="off_small_text_image">Off</span>
                      </div>
                  <img src="images/bundle_10.png" />
                  </div>
                  <div class="bottom_details_section">
                      <h5 class="more_bottom_heading">More Than Looks & Beauty 
                       Salon</h5>
                       <div class="price_bundle">
                           <span class="bundle_cut_price"> AED 800  </span>
                           <span class="without_cut_price"> AED 800 </span>
                       </div>
                       <div class="tags_for_coupon mt-2">
                           <span class="tag_coupon_one"> Hair cutting </span>
                           <span class="tag_coupon_one"> Hair color </span>
                           <span class="tag_coupon_one"> Trimming </span>
                       </div>
                       <div class="tags_for_coupon mt-1">
                           <span class="tag_coupon_one"> Hair cutting </span>
                           <span class="tag_coupon_one"> Hair color </span>
                           <span class="tag_coupon_one"> Trimming </span>
                       </div>
                  </div>
              </div>
            </div>
   
            <!-- End Here section -->
             <!-- Bundel Section_one -->
           <div class="col-lg-3 col-sm-6">
              <div class="bundle_section_one">
                  <div class="Bundle_image">
                      <div class="off_image">
                          80%
                          <span class="off_small_text_image">Off</span>
                      </div>
                  <img src="images/bundle_12.png" />
                  </div>
                  <div class="bottom_details_section">
                      <h5 class="more_bottom_heading">More Than Looks & Beauty 
                       Salon</h5>
                       <div class="price_bundle">
                           <span class="bundle_cut_price"> AED 800  </span>
                           <span class="without_cut_price"> AED 800 </span>
                       </div>
                       <div class="tags_for_coupon mt-2">
                           <span class="tag_coupon_one"> Hair cutting </span>
                           <span class="tag_coupon_one"> Hair color </span>
                           <span class="tag_coupon_one"> Trimming </span>
                       </div>
                       <div class="tags_for_coupon mt-1">
                           <span class="tag_coupon_one"> Hair cutting </span>
                           <span class="tag_coupon_one"> Hair color </span>
                           <span class="tag_coupon_one"> Trimming </span>
                       </div>
                  </div>
              </div>
            </div>
   
            <!-- End Here section -->
             <!-- Bundel Section_one -->
           <div class="col-lg-3 col-sm-6">
              <div class="bundle_section_one">
                  <div class="Bundle_image">
                      <div class="off_image">
                          80%
                          <span class="off_small_text_image">Off</span>
                      </div>
                  <img src="images/bundle_13.png" />
                  </div>
                  <div class="bottom_details_section">
                      <h5 class="more_bottom_heading">More Than Looks & Beauty 
                       Salon</h5>
                       <div class="price_bundle">
                           <span class="bundle_cut_price"> AED 800  </span>
                           <span class="without_cut_price"> AED 800 </span>
                       </div>
                       <div class="tags_for_coupon mt-2">
                           <span class="tag_coupon_one"> Hair cutting </span>
                           <span class="tag_coupon_one"> Hair color </span>
                           <span class="tag_coupon_one"> Trimming </span>
                       </div>
                       <div class="tags_for_coupon mt-1">
                           <span class="tag_coupon_one"> Hair cutting </span>
                           <span class="tag_coupon_one"> Hair color </span>
                           <span class="tag_coupon_one"> Trimming </span>
                       </div>
                  </div>
              </div>
            </div>
   
            <!-- End Here section -->
       </div>
    </div>
   </section>
   
   <!-- End Here Section -->
   
   <!-- Bundle Off Section -->
   <section class="Bundle_section mt-4 ">
       <div class="container-fluid">
           <div class="title_name_section">
                Flat off
             <span class="see_all_top"> <a href="#">See All</a> </span>
           </div>
          <!-- Start Row -->
           <div class="row">
               <!-- Bundel Section_one -->
               <div class="col-lg-3 col-sm-6">
                 <div class="bundle_section_one">
                     <div class="Bundle_image">
                         <div class="off_image">
                             80%
                             <span class="off_small_text_image">Off</span>
                         </div>
                     <img src="images/bundle_five.png" />
                     </div>
                     <div class="bottom_details_section">
                         <h5 class="more_bottom_heading">More Than Looks & Beauty 
                          Salon</h5>
                          <div class="price_bundle">
                              <span class="bundle_cut_price"> AED 800  </span>
                              <span class="without_cut_price"> AED 800 </span>
                          </div>
                          <div class="tags_for_coupon mt-2">
                              <span class="tag_coupon_one"> Hair cutting </span>
                              <span class="tag_coupon_one"> Hair color </span>
                              <span class="tag_coupon_one"> Trimming </span>
                          </div>
                          <div class="tags_for_coupon mt-1">
                              <span class="tag_coupon_one"> Hair cutting </span>
                              <span class="tag_coupon_one"> Hair color </span>
                              <span class="tag_coupon_one"> Trimming </span>
                          </div>
                     </div>
                 </div>
               </div>
      
               <!-- End Here section -->
                <!-- Bundel Section_one -->
                <div class="col-lg-3 col-sm-6">
                  <div class="bundle_section_one">
                      <div class="Bundle_image">
                          <div class="off_image">
                              80%
                              <span class="off_small_text_image">Off</span>
                          </div>
                      <img src="images/bundle_six.png" />
                      </div>
                      <div class="bottom_details_section">
                          <h5 class="more_bottom_heading">More Than Looks & Beauty 
                           Salon</h5>
                           <div class="price_bundle">
                               <span class="bundle_cut_price"> AED 800  </span>
                               <span class="without_cut_price"> AED 800 </span>
                           </div>
                           <div class="tags_for_coupon mt-2">
                               <span class="tag_coupon_one"> Hair cutting </span>
                               <span class="tag_coupon_one"> Hair color </span>
                               <span class="tag_coupon_one"> Trimming </span>
                           </div>
                           <div class="tags_for_coupon mt-1">
                               <span class="tag_coupon_one"> Hair cutting </span>
                               <span class="tag_coupon_one"> Hair color </span>
                               <span class="tag_coupon_one"> Trimming </span>
                           </div>
                      </div>
                  </div>
                </div>
       
                <!-- End Here section -->
                 <!-- Bundel Section_one -->
               <div class="col-lg-3 col-sm-6">
                  <div class="bundle_section_one">
                      <div class="Bundle_image">
                          <div class="off_image">
                              80%
                              <span class="off_small_text_image">Off</span>
                          </div>
                      <img src="images/bundle_seven.png" />
                      </div>
                      <div class="bottom_details_section">
                          <h5 class="more_bottom_heading">More Than Looks & Beauty 
                           Salon</h5>
                           <div class="price_bundle">
                               <span class="bundle_cut_price"> AED 800  </span>
                               <span class="without_cut_price"> AED 800 </span>
                           </div>
                           <div class="tags_for_coupon mt-2">
                               <span class="tag_coupon_one"> Hair cutting </span>
                               <span class="tag_coupon_one"> Hair color </span>
                               <span class="tag_coupon_one"> Trimming </span>
                           </div>
                           <div class="tags_for_coupon mt-1">
                               <span class="tag_coupon_one"> Hair cutting </span>
                               <span class="tag_coupon_one"> Hair color </span>
                               <span class="tag_coupon_one"> Trimming </span>
                           </div>
                      </div>
                  </div>
                </div>
       
                <!-- End Here section -->
                 <!-- Bundel Section_one -->
               <div class="col-lg-3 col-sm-6">
                  <div class="bundle_section_one">
                      <div class="Bundle_image">
                          <div class="off_image">
                              80%
                              <span class="off_small_text_image">Off</span>
                          </div>
                      <img src="images/bundle_8.png" />
                      </div>
                      <div class="bottom_details_section">
                          <h5 class="more_bottom_heading">More Than Looks & Beauty 
                           Salon</h5>
                           <div class="price_bundle">
                               <span class="bundle_cut_price"> AED 800  </span>
                               <span class="without_cut_price"> AED 800 </span>
                           </div>
                           <div class="tags_for_coupon mt-2">
                               <span class="tag_coupon_one"> Hair cutting </span>
                               <span class="tag_coupon_one"> Hair color </span>
                               <span class="tag_coupon_one"> Trimming </span>
                           </div>
                           <div class="tags_for_coupon mt-1">
                               <span class="tag_coupon_one"> Hair cutting </span>
                               <span class="tag_coupon_one"> Hair color </span>
                               <span class="tag_coupon_one"> Trimming </span>
                           </div>
                      </div>
                  </div>
                </div>
       
                <!-- End Here section -->
           </div>
           <!-- End Row -->
           <div class="row mt-4">
              <!-- Bundel Section_one -->
              <div class="col-lg-3 col-sm-6">
                <div class="bundle_section_one">
                    <div class="Bundle_image">
                        <div class="off_image">
                            80%
                            <span class="off_small_text_image">Off</span>
                        </div>
                    <img src="images/bundle_9.png" />
                    </div>
                    <div class="bottom_details_section">
                        <h5 class="more_bottom_heading">More Than Looks & Beauty 
                         Salon</h5>
                         <div class="price_bundle">
                             <span class="bundle_cut_price"> AED 800  </span>
                             <span class="without_cut_price"> AED 800 </span>
                         </div>
                         <div class="tags_for_coupon mt-2">
                             <span class="tag_coupon_one"> Hair cutting </span>
                             <span class="tag_coupon_one"> Hair color </span>
                             <span class="tag_coupon_one"> Trimming </span>
                         </div>
                         <div class="tags_for_coupon mt-1">
                             <span class="tag_coupon_one"> Hair cutting </span>
                             <span class="tag_coupon_one"> Hair color </span>
                             <span class="tag_coupon_one"> Trimming </span>
                         </div>
                    </div>
                </div>
              </div>
      
              <!-- End Here section -->
               <!-- Bundel Section_one -->
               <div class="col-lg-3 col-sm-6">
                 <div class="bundle_section_one">
                     <div class="Bundle_image">
                         <div class="off_image">
                             80%
                             <span class="off_small_text_image">Off</span>
                         </div>
                     <img src="images/bundle_10.png" />
                     </div>
                     <div class="bottom_details_section">
                         <h5 class="more_bottom_heading">More Than Looks & Beauty 
                          Salon</h5>
                          <div class="price_bundle">
                              <span class="bundle_cut_price"> AED 800  </span>
                              <span class="without_cut_price"> AED 800 </span>
                          </div>
                          <div class="tags_for_coupon mt-2">
                              <span class="tag_coupon_one"> Hair cutting </span>
                              <span class="tag_coupon_one"> Hair color </span>
                              <span class="tag_coupon_one"> Trimming </span>
                          </div>
                          <div class="tags_for_coupon mt-1">
                              <span class="tag_coupon_one"> Hair cutting </span>
                              <span class="tag_coupon_one"> Hair color </span>
                              <span class="tag_coupon_one"> Trimming </span>
                          </div>
                     </div>
                 </div>
               </div>
      
               <!-- End Here section -->
                <!-- Bundel Section_one -->
              <div class="col-lg-3 col-sm-6">
                 <div class="bundle_section_one">
                     <div class="Bundle_image">
                         <div class="off_image">
                             80%
                             <span class="off_small_text_image">Off</span>
                         </div>
                     <img src="images/bundle_11.png" />
                     </div>
                     <div class="bottom_details_section">
                         <h5 class="more_bottom_heading">More Than Looks & Beauty 
                          Salon</h5>
                          <div class="price_bundle">
                              <span class="bundle_cut_price"> AED 800  </span>
                              <span class="without_cut_price"> AED 800 </span>
                          </div>
                          <div class="tags_for_coupon mt-2">
                              <span class="tag_coupon_one"> Hair cutting </span>
                              <span class="tag_coupon_one"> Hair color </span>
                              <span class="tag_coupon_one"> Trimming </span>
                          </div>
                          <div class="tags_for_coupon mt-1">
                              <span class="tag_coupon_one"> Hair cutting </span>
                              <span class="tag_coupon_one"> Hair color </span>
                              <span class="tag_coupon_one"> Trimming </span>
                          </div>
                     </div>
                 </div>
               </div>
      
               <!-- End Here section -->
                <!-- Bundel Section_one -->
              <div class="col-lg-3 col-sm-6">
                 <div class="bundle_section_one">
                     <div class="Bundle_image">
                         <div class="off_image">
                             80%
                             <span class="off_small_text_image">Off</span>
                         </div>
                     <img src="images/bundle_one.png" />
                     </div>
                     <div class="bottom_details_section">
                         <h5 class="more_bottom_heading">More Than Looks & Beauty 
                          Salon</h5>
                          <div class="price_bundle">
                              <span class="bundle_cut_price"> AED 800  </span>
                              <span class="without_cut_price"> AED 800 </span>
                          </div>
                          <div class="tags_for_coupon mt-2">
                              <span class="tag_coupon_one"> Hair cutting </span>
                              <span class="tag_coupon_one"> Hair color </span>
                              <span class="tag_coupon_one"> Trimming </span>
                          </div>
                          <div class="tags_for_coupon mt-1">
                              <span class="tag_coupon_one"> Hair cutting </span>
                              <span class="tag_coupon_one"> Hair color </span>
                              <span class="tag_coupon_one"> Trimming </span>
                          </div>
                     </div>
                 </div>
               </div>
      
               <!-- End Here section -->
          </div>
       </div>
      </section>
      
      <!-- End Here Section -->
      <section class="Bundle_section mt-4">
        <div class="container-fluid">
            <div class="title_name_section">
                Buy X Get Y
               <span class="see_all_top"> <a href="#">See All</a> </span>
            </div>
           <!-- Start Row -->
            <div class="row">
                <!-- Bundel Section_one -->
                <div class="col-lg-3 col-sm-6">
                  <div class="bundle_section_one">
                      <div class="Bundle_image">
                          <div class="off_image">
                              80%
                              <span class="off_small_text_image">Off</span>
                          </div>
                      <img src="images/bundle_one.png" />
                      </div>
                      <div class="bottom_details_section">
                          <h5 class="more_bottom_heading">More Than Looks & Beauty 
                           Salon</h5>
                           <div class="price_bundle">
                               <span class="bundle_cut_price"> AED 800  </span>
                               <span class="without_cut_price"> AED 800 </span>
                           </div>
                           <div class="tags_for_coupon mt-2">
                               <span class="tag_coupon_one"> Hair cutting </span>
                               <span class="tag_coupon_one"> Hair color </span>
                               <span class="tag_coupon_one"> Trimming </span>
                           </div>
                           <div class="tags_for_coupon mt-1">
                               <span class="tag_coupon_one"> Hair cutting </span>
                               <span class="tag_coupon_one"> Hair color </span>
                               <span class="tag_coupon_one"> Trimming </span>
                           </div>
                      </div>
                  </div>
                </div>
       
                <!-- End Here section -->
                 <!-- Bundel Section_one -->
                 <div class="col-lg-3 col-sm-6">
                   <div class="bundle_section_one">
                       <div class="Bundle_image">
                           <div class="off_image">
                               80%
                               <span class="off_small_text_image">Off</span>
                           </div>
                       <img src="images/bundle_two.png" />
                       </div>
                       <div class="bottom_details_section">
                           <h5 class="more_bottom_heading">More Than Looks & Beauty 
                            Salon</h5>
                            <div class="price_bundle">
                                <span class="bundle_cut_price"> AED 800  </span>
                                <span class="without_cut_price"> AED 800 </span>
                            </div>
                            <div class="tags_for_coupon mt-2">
                                <span class="tag_coupon_one"> Hair cutting </span>
                                <span class="tag_coupon_one"> Hair color </span>
                                <span class="tag_coupon_one"> Trimming </span>
                            </div>
                            <div class="tags_for_coupon mt-1">
                                <span class="tag_coupon_one"> Hair cutting </span>
                                <span class="tag_coupon_one"> Hair color </span>
                                <span class="tag_coupon_one"> Trimming </span>
                            </div>
                       </div>
                   </div>
                 </div>
        
                 <!-- End Here section -->
                  <!-- Bundel Section_one -->
                <div class="col-lg-3 col-sm-6">
                   <div class="bundle_section_one">
                       <div class="Bundle_image">
                           <div class="off_image">
                               80%
                               <span class="off_small_text_image">Off</span>
                           </div>
                       <img src="images/bundle_three.png" />
                       </div>
                       <div class="bottom_details_section">
                           <h5 class="more_bottom_heading">More Than Looks & Beauty 
                            Salon</h5>
                            <div class="price_bundle">
                                <span class="bundle_cut_price"> AED 800  </span>
                                <span class="without_cut_price"> AED 800 </span>
                            </div>
                            <div class="tags_for_coupon mt-2">
                                <span class="tag_coupon_one"> Hair cutting </span>
                                <span class="tag_coupon_one"> Hair color </span>
                                <span class="tag_coupon_one"> Trimming </span>
                            </div>
                            <div class="tags_for_coupon mt-1">
                                <span class="tag_coupon_one"> Hair cutting </span>
                                <span class="tag_coupon_one"> Hair color </span>
                                <span class="tag_coupon_one"> Trimming </span>
                            </div>
                       </div>
                   </div>
                 </div>
        
                 <!-- End Here section -->
                  <!-- Bundel Section_one -->
                <div class="col-lg-3 col-sm-6">
                   <div class="bundle_section_one">
                       <div class="Bundle_image">
                           <div class="off_image">
                               80%
                               <span class="off_small_text_image">Off</span>
                           </div>
                       <img src="images/bundle_four.png" />
                       </div>
                       <div class="bottom_details_section">
                           <h5 class="more_bottom_heading">More Than Looks & Beauty 
                            Salon</h5>
                            <div class="price_bundle">
                                <span class="bundle_cut_price"> AED 800  </span>
                                <span class="without_cut_price"> AED 800 </span>
                            </div>
                            <div class="tags_for_coupon mt-2">
                                <span class="tag_coupon_one"> Hair cutting </span>
                                <span class="tag_coupon_one"> Hair color </span>
                                <span class="tag_coupon_one"> Trimming </span>
                            </div>
                            <div class="tags_for_coupon mt-1">
                                <span class="tag_coupon_one"> Hair cutting </span>
                                <span class="tag_coupon_one"> Hair color </span>
                                <span class="tag_coupon_one"> Trimming </span>
                            </div>
                       </div>
                   </div>
                 </div>
        
                 <!-- End Here section -->
            </div>
            <!-- End Row -->
            <div class="row mt-4">
               <!-- Bundel Section_one -->
               <div class="col-lg-3 col-sm-6">
                 <div class="bundle_section_one">
                     <div class="Bundle_image">
                         <div class="off_image">
                             80%
                             <span class="off_small_text_image">Off</span>
                         </div>
                     <img src="images/bundle_9.png" />
                     </div>
                     <div class="bottom_details_section">
                         <h5 class="more_bottom_heading">More Than Looks & Beauty 
                          Salon</h5>
                          <div class="price_bundle">
                              <span class="bundle_cut_price"> AED 800  </span>
                              <span class="without_cut_price"> AED 800 </span>
                          </div>
                          <div class="tags_for_coupon mt-2">
                              <span class="tag_coupon_one"> Hair cutting </span>
                              <span class="tag_coupon_one"> Hair color </span>
                              <span class="tag_coupon_one"> Trimming </span>
                          </div>
                          <div class="tags_for_coupon mt-1">
                              <span class="tag_coupon_one"> Hair cutting </span>
                              <span class="tag_coupon_one"> Hair color </span>
                              <span class="tag_coupon_one"> Trimming </span>
                          </div>
                     </div>
                 </div>
               </div>
       
               <!-- End Here section -->
                <!-- Bundel Section_one -->
                <div class="col-lg-3 col-sm-6">
                  <div class="bundle_section_one">
                      <div class="Bundle_image">
                          <div class="off_image">
                              80%
                              <span class="off_small_text_image">Off</span>
                          </div>
                      <img src="images/bundle_10.png" />
                      </div>
                      <div class="bottom_details_section">
                          <h5 class="more_bottom_heading">More Than Looks & Beauty 
                           Salon</h5>
                           <div class="price_bundle">
                               <span class="bundle_cut_price"> AED 800  </span>
                               <span class="without_cut_price"> AED 800 </span>
                           </div>
                           <div class="tags_for_coupon mt-2">
                               <span class="tag_coupon_one"> Hair cutting </span>
                               <span class="tag_coupon_one"> Hair color </span>
                               <span class="tag_coupon_one"> Trimming </span>
                           </div>
                           <div class="tags_for_coupon mt-1">
                               <span class="tag_coupon_one"> Hair cutting </span>
                               <span class="tag_coupon_one"> Hair color </span>
                               <span class="tag_coupon_one"> Trimming </span>
                           </div>
                      </div>
                  </div>
                </div>
       
                <!-- End Here section -->
                 <!-- Bundel Section_one -->
               <div class="col-lg-3 col-sm-6">
                  <div class="bundle_section_one">
                      <div class="Bundle_image">
                          <div class="off_image">
                              80%
                              <span class="off_small_text_image">Off</span>
                          </div>
                      <img src="images/bundle_12.png" />
                      </div>
                      <div class="bottom_details_section">
                          <h5 class="more_bottom_heading">More Than Looks & Beauty 
                           Salon</h5>
                           <div class="price_bundle">
                               <span class="bundle_cut_price"> AED 800  </span>
                               <span class="without_cut_price"> AED 800 </span>
                           </div>
                           <div class="tags_for_coupon mt-2">
                               <span class="tag_coupon_one"> Hair cutting </span>
                               <span class="tag_coupon_one"> Hair color </span>
                               <span class="tag_coupon_one"> Trimming </span>
                           </div>
                           <div class="tags_for_coupon mt-1">
                               <span class="tag_coupon_one"> Hair cutting </span>
                               <span class="tag_coupon_one"> Hair color </span>
                               <span class="tag_coupon_one"> Trimming </span>
                           </div>
                      </div>
                  </div>
                </div>
       
                <!-- End Here section -->
                 <!-- Bundel Section_one -->
               <div class="col-lg-3 col-sm-6">
                  <div class="bundle_section_one">
                      <div class="Bundle_image">
                          <div class="off_image">
                              80%
                              <span class="off_small_text_image">Off</span>
                          </div>
                      <img src="images/bundle_13.png" />
                      </div>
                      <div class="bottom_details_section">
                          <h5 class="more_bottom_heading">More Than Looks & Beauty 
                           Salon</h5>
                           <div class="price_bundle">
                               <span class="bundle_cut_price"> AED 800  </span>
                               <span class="without_cut_price"> AED 800 </span>
                           </div>
                           <div class="tags_for_coupon mt-2">
                               <span class="tag_coupon_one"> Hair cutting </span>
                               <span class="tag_coupon_one"> Hair color </span>
                               <span class="tag_coupon_one"> Trimming </span>
                           </div>
                           <div class="tags_for_coupon mt-1">
                               <span class="tag_coupon_one"> Hair cutting </span>
                               <span class="tag_coupon_one"> Hair color </span>
                               <span class="tag_coupon_one"> Trimming </span>
                           </div>
                      </div>
                  </div>
                </div>
       
                <!-- End Here section -->
           </div>
        </div>
       </section>

       <section class="Bundle_section mt-4 mb-5">
        <div class="container-fluid">
            <div class="title_name_section">
                Subscribe & Save
              <span class="see_all_top"> <a href="#">See All</a> </span>
            </div>
           <!-- Start Row -->
            <div class="row">
                <!-- Bundel Section_one -->
                <div class="col-lg-3 col-sm-6">
                  <div class="bundle_section_one">
                      <div class="Bundle_image">
                          <div class="off_image">
                              80%
                              <span class="off_small_text_image">Off</span>
                          </div>
                      <img src="images/bundle_five.png" />
                      </div>
                      <div class="bottom_details_section">
                          <h5 class="more_bottom_heading">More Than Looks & Beauty 
                           Salon</h5>
                           <div class="price_bundle">
                               <span class="bundle_cut_price"> AED 800  </span>
                               <span class="without_cut_price"> AED 800 </span>
                           </div>
                           <div class="tags_for_coupon mt-2">
                               <span class="tag_coupon_one"> Hair cutting </span>
                               <span class="tag_coupon_one"> Hair color </span>
                               <span class="tag_coupon_one"> Trimming </span>
                           </div>
                           <div class="tags_for_coupon mt-1">
                               <span class="tag_coupon_one"> Hair cutting </span>
                               <span class="tag_coupon_one"> Hair color </span>
                               <span class="tag_coupon_one"> Trimming </span>
                           </div>
                      </div>
                  </div>
                </div>
       
                <!-- End Here section -->
                 <!-- Bundel Section_one -->
                 <div class="col-lg-3 col-sm-6">
                   <div class="bundle_section_one">
                       <div class="Bundle_image">
                           <div class="off_image">
                               80%
                               <span class="off_small_text_image">Off</span>
                           </div>
                       <img src="images/bundle_six.png" />
                       </div>
                       <div class="bottom_details_section">
                           <h5 class="more_bottom_heading">More Than Looks & Beauty 
                            Salon</h5>
                            <div class="price_bundle">
                                <span class="bundle_cut_price"> AED 800  </span>
                                <span class="without_cut_price"> AED 800 </span>
                            </div>
                            <div class="tags_for_coupon mt-2">
                                <span class="tag_coupon_one"> Hair cutting </span>
                                <span class="tag_coupon_one"> Hair color </span>
                                <span class="tag_coupon_one"> Trimming </span>
                            </div>
                            <div class="tags_for_coupon mt-1">
                                <span class="tag_coupon_one"> Hair cutting </span>
                                <span class="tag_coupon_one"> Hair color </span>
                                <span class="tag_coupon_one"> Trimming </span>
                            </div>
                       </div>
                   </div>
                 </div>
        
                 <!-- End Here section -->
                  <!-- Bundel Section_one -->
                <div class="col-lg-3 col-sm-6">
                   <div class="bundle_section_one">
                       <div class="Bundle_image">
                           <div class="off_image">
                               80%
                               <span class="off_small_text_image">Off</span>
                           </div>
                       <img src="images/bundle_seven.png" />
                       </div>
                       <div class="bottom_details_section">
                           <h5 class="more_bottom_heading">More Than Looks & Beauty 
                            Salon</h5>
                            <div class="price_bundle">
                                <span class="bundle_cut_price"> AED 800  </span>
                                <span class="without_cut_price"> AED 800 </span>
                            </div>
                            <div class="tags_for_coupon mt-2">
                                <span class="tag_coupon_one"> Hair cutting </span>
                                <span class="tag_coupon_one"> Hair color </span>
                                <span class="tag_coupon_one"> Trimming </span>
                            </div>
                            <div class="tags_for_coupon mt-1">
                                <span class="tag_coupon_one"> Hair cutting </span>
                                <span class="tag_coupon_one"> Hair color </span>
                                <span class="tag_coupon_one"> Trimming </span>
                            </div>
                       </div>
                   </div>
                 </div>
        
                 <!-- End Here section -->
                  <!-- Bundel Section_one -->
                <div class="col-lg-3 col-sm-6">
                   <div class="bundle_section_one">
                       <div class="Bundle_image">
                           <div class="off_image">
                               80%
                               <span class="off_small_text_image">Off</span>
                           </div>
                       <img src="images/bundle_8.png" />
                       </div>
                       <div class="bottom_details_section">
                           <h5 class="more_bottom_heading">More Than Looks & Beauty 
                            Salon</h5>
                            <div class="price_bundle">
                                <span class="bundle_cut_price"> AED 800  </span>
                                <span class="without_cut_price"> AED 800 </span>
                            </div>
                            <div class="tags_for_coupon mt-2">
                                <span class="tag_coupon_one"> Hair cutting </span>
                                <span class="tag_coupon_one"> Hair color </span>
                                <span class="tag_coupon_one"> Trimming </span>
                            </div>
                            <div class="tags_for_coupon mt-1">
                                <span class="tag_coupon_one"> Hair cutting </span>
                                <span class="tag_coupon_one"> Hair color </span>
                                <span class="tag_coupon_one"> Trimming </span>
                            </div>
                       </div>
                   </div>
                 </div>
        
                 <!-- End Here section -->
            </div>
            <!-- End Row -->
            <div class="row mt-4">
               <!-- Bundel Section_one -->
               <div class="col-lg-3 col-sm-6">
                 <div class="bundle_section_one">
                     <div class="Bundle_image">
                         <div class="off_image">
                             80%
                             <span class="off_small_text_image">Off</span>
                         </div>
                     <img src="images/bundle_9.png" />
                     </div>
                     <div class="bottom_details_section">
                         <h5 class="more_bottom_heading">More Than Looks & Beauty 
                          Salon</h5>
                          <div class="price_bundle">
                              <span class="bundle_cut_price"> AED 800  </span>
                              <span class="without_cut_price"> AED 800 </span>
                          </div>
                          <div class="tags_for_coupon mt-2">
                              <span class="tag_coupon_one"> Hair cutting </span>
                              <span class="tag_coupon_one"> Hair color </span>
                              <span class="tag_coupon_one"> Trimming </span>
                          </div>
                          <div class="tags_for_coupon mt-1">
                              <span class="tag_coupon_one"> Hair cutting </span>
                              <span class="tag_coupon_one"> Hair color </span>
                              <span class="tag_coupon_one"> Trimming </span>
                          </div>
                     </div>
                 </div>
               </div>
       
               <!-- End Here section -->
                <!-- Bundel Section_one -->
                <div class="col-lg-3 col-sm-6">
                  <div class="bundle_section_one">
                      <div class="Bundle_image">
                          <div class="off_image">
                              80%
                              <span class="off_small_text_image">Off</span>
                          </div>
                      <img src="images/bundle_10.png" />
                      </div>
                      <div class="bottom_details_section">
                          <h5 class="more_bottom_heading">More Than Looks & Beauty 
                           Salon</h5>
                           <div class="price_bundle">
                               <span class="bundle_cut_price"> AED 800  </span>
                               <span class="without_cut_price"> AED 800 </span>
                           </div>
                           <div class="tags_for_coupon mt-2">
                               <span class="tag_coupon_one"> Hair cutting </span>
                               <span class="tag_coupon_one"> Hair color </span>
                               <span class="tag_coupon_one"> Trimming </span>
                           </div>
                           <div class="tags_for_coupon mt-1">
                               <span class="tag_coupon_one"> Hair cutting </span>
                               <span class="tag_coupon_one"> Hair color </span>
                               <span class="tag_coupon_one"> Trimming </span>
                           </div>
                      </div>
                  </div>
                </div>
       
                <!-- End Here section -->
                 <!-- Bundel Section_one -->
               <div class="col-lg-3 col-sm-6">
                  <div class="bundle_section_one">
                      <div class="Bundle_image">
                          <div class="off_image">
                              80%
                              <span class="off_small_text_image">Off</span>
                          </div>
                      <img src="images/bundle_11.png" />
                      </div>
                      <div class="bottom_details_section">
                          <h5 class="more_bottom_heading">More Than Looks & Beauty 
                           Salon</h5>
                           <div class="price_bundle">
                               <span class="bundle_cut_price"> AED 800  </span>
                               <span class="without_cut_price"> AED 800 </span>
                           </div>
                           <div class="tags_for_coupon mt-2">
                               <span class="tag_coupon_one"> Hair cutting </span>
                               <span class="tag_coupon_one"> Hair color </span>
                               <span class="tag_coupon_one"> Trimming </span>
                           </div>
                           <div class="tags_for_coupon mt-1">
                               <span class="tag_coupon_one"> Hair cutting </span>
                               <span class="tag_coupon_one"> Hair color </span>
                               <span class="tag_coupon_one"> Trimming </span>
                           </div>
                      </div>
                  </div>
                </div>
       
                <!-- End Here section -->
                 <!-- Bundel Section_one -->
               <div class="col-lg-3 col-sm-6">
                  <div class="bundle_section_one">
                      <div class="Bundle_image">
                          <div class="off_image">
                              80%
                              <span class="off_small_text_image">Off</span>
                          </div>
                      <img src="images/bundle_one.png" />
                      </div>
                      <div class="bottom_details_section">
                          <h5 class="more_bottom_heading">More Than Looks & Beauty 
                           Salon</h5>
                           <div class="price_bundle">
                               <span class="bundle_cut_price"> AED 800  </span>
                               <span class="without_cut_price"> AED 800 </span>
                           </div>
                           <div class="tags_for_coupon mt-2">
                               <span class="tag_coupon_one"> Hair cutting </span>
                               <span class="tag_coupon_one"> Hair color </span>
                               <span class="tag_coupon_one"> Trimming </span>
                           </div>
                           <div class="tags_for_coupon mt-1">
                               <span class="tag_coupon_one"> Hair cutting </span>
                               <span class="tag_coupon_one"> Hair color </span>
                               <span class="tag_coupon_one"> Trimming </span>
                           </div>
                      </div>
                  </div>
                </div>
       
                <!-- End Here section -->
           </div>
        </div>
       </section>
<!-- beutics_blog_section -->
<section class="beutics_blog_section padding_tb_60">
	<div class="container-fluid">
		<!-- blogs -->
		<div class="blog_section row">
			<div class="blog_slider_section col-md-9 offset-md-3 no_padding">
				<div class="blog_slider slider">
				    <div class="cu_slider_item">
				    	<div class="blog_wrap">
					      <div class="slider-caption-wrap">
					      	<div class="caption_title">
					          	<h2>What is Lorem Ipsum ?</h2>
					          </div>
					      	<div class="discription">
					      		Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					      	</div>
					          
					      </div>
					    </div>
				    </div>
				    <div class="cu_slider_item">
				    	<div class="blog_wrap">
					      <div class="slider-caption-wrap">
					      	<div class="caption_title">
					          	<h2>What is Lorem Ipsum ?</h2>
					          </div>
					      	<div class="discription">
					      		Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					      	</div>
					          
					      </div>
					    </div>
				    </div>
				    <div class="cu_slider_item">
				    	<div class="blog_wrap">
					      <div class="slider-caption-wrap">
					      	<div class="caption_title">
					          	<h2>What is Lorem Ipsum ?</h2>
					          </div>
					      	<div class="discription">
					      		Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					      	</div>
					          
					      </div>
					    </div>
				    </div>
				    <div class="cu_slider_item">
				    	<div class="blog_wrap">
					      <div class="slider-caption-wrap">
					      	<div class="caption_title">
					          	<h2>What is Lorem Ipsum ?</h2>
					          </div>
					      	<div class="discription">
					      		Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					      	</div>
					          
					      </div>
					    </div>
				    </div>
				    <div class="cu_slider_item">
				    	<div class="blog_wrap">
					      <div class="slider-caption-wrap">
					      	<div class="caption_title">
					          	<h2>What is Lorem Ipsum ?</h2>
					          </div>
					      	<div class="discription">
					      		Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
					      	</div>
					          
					      </div>
					    </div>
				    </div>
				    
			  	</div>
			</div>
		</div>
	</div>
</section>
<!-- beutics_blog_section End-->
@endsection