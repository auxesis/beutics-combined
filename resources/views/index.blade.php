@extends('layouts.default')
@section('title','Beutics')
@section('content')
<body class="home_page front_page page_body">
<div class="header_height"></div>
<!-- Home Page Banner Slider -->
@if(!empty($BannersectionArr['Top Banner']))
<div class="home_banner">
	<!-- <div class="home_banner_caption_area"></div> -->
	<div class="home_banner_slider_area">
		<section class="slideshow__slides slider">

            @foreach($BannersectionArr['Top Banner'] as $topBanners)
            <div class="slider-image">
            <div class="image-wrap">
                <img src="{{asset('sp_uploads/banner_images/'.$topBanners->page_name.'/'.$topBanners->image)}}" />
            </div>
            <div class="slider-caption-wrap">
                <div class="caption-details">
                <div class="caption_title">
                	{{$topBanners->title}}  <span>{{$topBanners->sub_title}} </span>  
                </div>
                </div>
            </div>
            </div>
     @endforeach 
            
    	</section>
  	<div class="slick_custom_paginginfo"></div>
	</div>
</div>
@endif
<!-- Home Page Banner Slider End-->
<!-- service category section -->
@if(!empty($categoryInfo))
<section id="service_category_section" class="service_category_section padding_tb_60" style="padding:60px 0 20px 0 !important;">
	<div class="container">
		<div class="section_header">
			<h2 class="sec_head">Service <br>category</h2>
		</div>
		<!-- Beauty -->
		 
		 @foreach($categoryInfo as $key => $data)
		   <div class="<?php echo strtolower($key)?>_category_section category_section  padding_b_90 mb-5">
		   	
		   <div class="visit_categoy"><a href="#!">{{$key}} <i class="far fa-long-arrow-alt-right"></i></a></div>
		   <div class="category_slider_section" @if($key == 'Fitness') dir="rtl" @endif>
		   <div class="<?php echo strtolower($key)?>_category slider">
		   
                   @foreach($data as $key => $data1)
                   
				    <div class="cu_slider_item">
				    	<a  href="#!" class="category_link_wrap">
					      <div class="image-wrap">
					        <img src="{{asset('sp_uploads/sub_categories/'.$data1->icon)}}" />
					        
					      </div>
					      <div class="slider-caption-wrap">
					          <div class="caption_title">
					          	{{$data1->category_name}}
					          </div>
					      </div>
					    </a>
				    </div>
                      @endforeach
				    
			  	</div>
			</div>
		</div>
		  
     @endforeach

		
	</div>
</section>
@endif
<!-- service category section end -->

<!-- beutics_features_section  -->

<section class="beutics_features_section padding_tb_60" style="padding:20px 0 !important;">
	<div class="container  bottom-banner-img">
	@if(!empty($BannersectionArr['Facts Banner']))
		<div class="row features_row">
            @foreach($BannersectionArr['Facts Banner'] as $FactsBannerdata)
			<div class="col-lg-3 col-sm-6">
				<div class="feature_item">
					<img src="{{asset('sp_uploads/banner_images/'.$FactsBannerdata->page_name.'/'.$FactsBannerdata->image)}}" />
					<h3 class="feature_title">{{$FactsBannerdata->title}}</h3>
					<div class="feature_detail">{{strip_tags($FactsBannerdata->description)}}</div>
				</div>
			</div>
        @endforeach
		</div>
		@endif
		@if($MiddleBanner) 
		<div class="row features_category_row">
		<div class="col-md-2"></div>
			<div class="col-md-10">
				<div class="feature_category">
					<h3 class="feature_title">{{$MiddleBanner->title}}</h3>
					<div class="feature_detail">{{strip_tags($MiddleBanner->description)}}</div>
				</div>
			</div>
		</div>
		
	</div>
	<img src="{{asset('sp_uploads/banner_images/'.$MiddleBanner->page_name.'/'.$MiddleBanner->image)}}" style="width:100%;">
	@endif
</section>



<!-- beutics_features_section End-->

<!-- beutics_achievment_section -->
@if($MiddleLowerBanner) 
<section class="beutics_achievment_section padding_tb_60">
	<div class="container">
		<div class="row achivements">

			<div class="col-md-6">
				<div class="achivement_disc">
					<h3 class="achivement_title">{{$MiddleLowerBanner->title}}</h3>
					<div class="achivement_detail">{{strip_tags($MiddleLowerBanner->description)}}
					</div>
				</div>
			</div>

			<div class="col-md-6">
				<div class="achivement_counts">
                    @foreach($MiddleLowerBanner->image as $key => $MiddleLowerBannerdata)
					<div class="achive_count_item">
						<div class="achive_count_item_wrap">
							<div class="achive_icon"><img src="{{asset('sp_uploads/banner_images/'.$MiddleLowerBannerdata['page_name'].'/'.$MiddleLowerBannerdata['image'])}}" /></div>
							<div class="achive_name_count">
								<div class="count">{{$MiddleLowerBannerdata['sub_title']}}</div>
								<div class="name">{{strip_tags($MiddleLowerBannerdata['sub_description'])}}</div>
							</div>
						</div>
					</div>
					@endforeach

					
				</div>
			</div>
		</div>
	</div>
</section>
@endif
<!-- beutics_achievment_section End -->

<!-- refer section start -->
@if(!empty($BannersectionArr['How To Earn Banner']))
<section class="refer_earn_section padding_tb_60">
	@foreach($BannersectionArr['How To Earn Banner'] as $Howtoearnbanner)
	<img src="{{asset('sp_uploads/banner_images/'.$Howtoearnbanner->page_name.'/'.$Howtoearnbanner->image)}}" style="width:100%;">
	@endforeach	
</section>
@endif
<!-- refer section end -->

<!-- beutics_testimonial_section -->
@if(!empty($Testimonial))
<section class="beutics_testimonial_section padding_tb_60">
	<div class="container">
		<div class="section_header">
			<h2 class="sec_head">Testimonials</h2>
		</div>
		<!-- testimonial -->
		<div class="testimonial_section padding_b_40">
			<div class="testimonial_slider_section">
				<div class="testimonial_slider slider">
                
				     @foreach($Testimonial as $key => $Testimonialdata)
				    <div class="cu_slider_item">
				    	<div class="testi_wrap">
					      <div class="image-wrap">
					        <img src="{{asset('sp_uploads/testimonial'.'/'.$Testimonialdata->image)}}" />
					      </div>
					      <div class="slider-caption-wrap">
					      	<div class="discription">
					      		{{strip_tags($Testimonialdata->description)}}
					      	</div>
					          <div class="caption_title">
					          	<h2>{{$Testimonialdata->name}}</h2>
					          	<h3>{{$Testimonialdata->designation}}</h3>
					          </div>
					      </div>
					    </div>
				    </div>
                 @endforeach
				    
			  	</div>
			</div>
		</div>
	</div>
</section>
@endif
<!-- beutics_testimonial_section End-->

<!-- beutics_blog_section -->
@if(!empty($BannersectionArr['Social Media Content']))
<section class="beutics_blog_section padding_tb_60">
	<div class="container-fluid">
		<!-- blogs -->
		<div class="blog_section row">
			<div class="blog_slider_section col-md-9 offset-md-3 no_padding">
				<div class="blog_slider slider">
                  @foreach($BannersectionArr['Social Media Content'] as $Social_Media_Content_Data)
                    <div class="cu_slider_item">
                        <div class="blog_wrap read-more-less" data-id="300">
                          <div class="slider-caption-wrap">
                            <div class="caption_title">
                                <h2>{{$Social_Media_Content_Data->title}}</h2>
                              </div>
                            <div class="discription read-toggle" data-id='0'>
                                {{$Social_Media_Content_Data->description}}
								<!-- <button onclick="readMoreFunction()" id="buttonReadMore">Click to Read More About the Blog</button> -->
                            </div>
                              
                          </div>
                        </div>
                    </div>
                    @endforeach
				    
				    
			  	</div>
			</div>
		</div>
	</div>
</section>
@endif
<!-- beutics_blog_section End-->
@endsection