@extends('layouts.layout')
@section('title','Beauty Landing')
@section('content')
<div class="header_height header_bottom_landing"></div>

@if(isset($pagesectionArr['Top Banner Content']) && !empty($pagesectionArr['Top Banner Content']))
<div class="page_banner landing_page_banner" style="z-index:9999 !important;">
	<div class="page_banner_area Modern-Slider">
	@foreach($pagesectionArr['Top Banner Content'] as $topBanners)
	<div class="item">
    <div class="img-fill">
      <img src="{{asset('sp_uploads/banner_images/'.$topBanners->page_name.'/'.$topBanners->image)}}" alt="">
    </div>
  </div>
  @endforeach
</div>
@endif

<!-- Landing Page Banner Slider End-->
<!-- Landing page beutics_blog_section -->
@if(isset($pagesectionArr['Top Text Content']) && !empty($pagesectionArr['Top Text Content']))
<section class="landing_page_blog_section padding_tb_60">
	<div class="container">
		<!-- blogs -->
		<div class="landing_blog_section row">
			<div class="landing_blog_slider_section col-md-12 no_padding">
				<div class="landing_blog_slider slider">
					@foreach($pagesectionArr['Top Text Content'] as $textBanners)
				    <div class="cu_slider_item">
				    	<div class="blog_wrap">
					      <div class="slider-caption-wrap">
					      	<div class="caption_title">
					          	<h2>{{ $textBanners->title }}</h2>
					          </div>
					      	<div class="discription">
					      		{{ $textBanners->description }}
					      	</div>
					          
					      </div>
					    </div>
				    </div>
				    @endforeach
			  	</div>
			</div>
		</div>
	</div>
</section>
@endif
<!-- Landing page beutics_blog_section End-->


<!-- service category section -->
<section id="landing_offers_section" class="landing_offers_section padding_tb_60">
	<div class="container">
		<div class="offers_category_section_wrapper-1">	
			<div class="section_header">
				<h2 class="sec_head">Offers</h2>
			</div>
			<!-- Beauty -->
			<div class="offers_category_section">
			<ul class="nav nav-tabs offers_category_tabs" role="tablist">
				@foreach($offerdata as $key => $data)
				<?php if($key == 'home') {?>
			    <li class="nav-item">
			      <a class="nav-link  active" data-toggle="tab" href="#{{$key}}">{{strtoupper($key)}}</a>
			    </li>
			    <?php } else {?>
			    <li class="nav-item">
			      <a class="nav-link " data-toggle="tab" href="#{{$key}}">{{strtoupper($key)}}</a>
			    </li>
			    <?php }?>
			    @endforeach
			    <!-- <li class="nav-item">
			      <a class="nav-link" data-toggle="tab" href="#trainer">Trainer at home</a>
			    </li>
			    <li class="nav-item">
			      <a class="nav-link" data-toggle="tab" href="#studio">Studio</a>
			    </li> -->
			</ul>
			</div>
			<!-- Tab panes -->
			<div class="tab-content offers_category_tab_content">
              @foreach($offerdata as $key => $data)
                <?php if($key == 'home') {?>
				<div id="{{$key}}" class="container tab-pane active">
					<div class="landing_offers_slider trainer_offers_slider slider">
                 <?php } else {?>
				<div id="{{$key}}" class="container tab-pane fade">
					<div class="landing_offers_slider studio_offers_slider slider">
                 <?php }?>

                  
						@foreach($data as $key => $data1)
						  <div class="cu_slider_item">
							  <div class="blog_wrap">
								  <div class="image-wrap">
								  	
									  <img src="sp_uploads/nominated_offers/thumb/{{$data1->nominate_thumb_image}}" />
								  </div>
								<div class="slider-caption-wrap">
									<div class="caption_wrap">
										<div class="caption_title">
											<h2>{{$data1->title}} </h2>
										</div>
										<div class="discription">
											{{$data1->offer_details}} 
										</div>
								  </div>
								  <div class="pricing_wrap">
									  <div class="price">${{$data1->total_price}}</div>
									  <div class="button_wrap"><a href="#" class="btn">Book Now</a></div>
								  </div>  
								</div>
							  </div>
						  </div>
						  @endforeach
						</div>
				  </div>
				  @endforeach


				<!-- <div id="trainer" class="container tab-pane fade">
				  <div class="landing_offers_slider trainer_offers_slider slider">
					    <div class="cu_slider_item">
					    	<div class="blog_wrap">
					    		<div class="image-wrap">
							        <img src="images/offer2.jpg" />
							    </div>
						      <div class="slider-caption-wrap">
						      	<div class="caption_wrap">
							      	<div class="caption_title">
							          	<h2>What is trainer1 ?</h2>
							          </div>
							      	<div class="discription">
							      		Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
							      	</div>
						        </div>
						        <div class="pricing_wrap">
						        	<div class="price">$50.00</div>
						        	<div class="button_wrap"><a href="#" class="btn">Book Now</a></div>
						        </div>  
						      </div>
						    </div>
					    </div>
					    <div class="cu_slider_item">
					    	<div class="blog_wrap">
					    		<div class="image-wrap">
							        <img src="images/offer1.jpg" />
							    </div>
						      <div class="slider-caption-wrap">
						      	<div class="caption_wrap">
							      	<div class="caption_title">
							          	<h2>What is trainer2 ?</h2>
							          </div>
							      	<div class="discription">
							      		Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
							      	</div>
						        </div>
						        <div class="pricing_wrap">
						        	<div class="price">$50.00</div>
						        	<div class="button_wrap"><a href="#" class="btn">Book Now</a></div>
						        </div>  
						      </div>
						    </div>
					    </div>
					    <div class="cu_slider_item">
					    	<div class="blog_wrap">
					    		<div class="image-wrap">
							        <img src="images/offer2.jpg" />
							    </div>
						      <div class="slider-caption-wrap">
						      	<div class="caption_wrap">
							      	<div class="caption_title">
							          	<h2>What is trainer3 ?</h2>
							          </div>
							      	<div class="discription">
							      		Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
							      	</div>
						        </div>
						        <div class="pricing_wrap">
						        	<div class="price">$50.00</div>
						        	<div class="button_wrap"><a href="#" class="btn">Book Now</a></div>
						        </div>  
						      </div>
						    </div>
					    </div>
					    
				  	</div>
				</div> -->
				<!-- <div id="studio" class="container tab-pane fade">
				  <div class="landing_offers_slider studio_offers_slider slider">
					    <div class="cu_slider_item">
					    	<div class="blog_wrap">
					    		<div class="image-wrap">
							        <img src="images/offer1.jpg" />
							    </div>
						      <div class="slider-caption-wrap">
						      	<div class="caption_wrap">
							      	<div class="caption_title">
							          	<h2>What is studio1 ?</h2>
							          </div>
							      	<div class="discription">
							      		Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
							      	</div>
						        </div>
						        <div class="pricing_wrap">
						        	<div class="price">$50.00</div>
						        	<div class="button_wrap"><a href="#" class="btn">Book Now</a></div>
						        </div>  
						      </div>
						    </div>
					    </div>
					    <div class="cu_slider_item">
					    	<div class="blog_wrap">
					    		<div class="image-wrap">
							        <img src="images/offer2.jpg" />
							    </div>
						      <div class="slider-caption-wrap">
						      	<div class="caption_wrap">
							      	<div class="caption_title">
							          	<h2>What is studio2 ?</h2>
							          </div>
							      	<div class="discription">
							      		Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
							      	</div>
						        </div>
						        <div class="pricing_wrap">
						        	<div class="price">$50.00</div>
						        	<div class="button_wrap"><a href="#" class="btn">Book Now</a></div>
						        </div>  
						      </div>
						    </div>
					    </div>
					    
					    
				  	</div>
				</div> -->
			</div>
		</div>
	</div>
</section>

<!-- Mode of Delivery -->
@if(isset($pagesectionArr['Mode Of Delivery Content']) && !empty($pagesectionArr['Mode Of Delivery Content']))
<section id="mode_of_delivery_section" class="mode_of_delivery_section padding_tb_60">
	<div class="container">
		<div class="section_header">
			<h3 class="sub_head">Mode of</h3>
			<h2 class="sec_head">Delivery</h2>
		</div>
		<!-- Beauty -->
		<div class="landing_delivery_section">
			<div id="delivery_mode" class="delivery_mode">
				<div class="delivery_mode_wrapper">
			  	@foreach($pagesectionArr['Mode Of Delivery Content'] as $deliveryBanners)
				    <div class="delivery_mode_item">
				    	<div class="blog_wrap">
				    		<div class="image-wrap">
						        <img src="{{asset('sp_uploads/pages/'.$deliveryBanners->page_name.'/'.$deliveryBanners->image)}}" />
						    </div>
					      <div class="slider-caption-wrap">
					      	<div class="caption_wrap">
						      	<div class="caption_title">
						          	<h2><a href="#">{{$deliveryBanners->title}}</a></h2>
						          </div>
						      	<div class="discription">
						      		{{$deliveryBanners->description}}
						      	</div>
					        </div>
					      </div>
					    </div>
				    </div>
				@endforeach
			  	</div>
			</div>
		</div>
	</div>
</section>
@endif
<!-- Mode of delivery end -->

<!-- Add Categories section start -->
@if(!empty($subCategory))
<section class="landing_page_blog_section padding_tb_60">
 <div class="container">
   <div class="section_header_all_category">
    <h3 class="sub_head_all_category">All</h3>
    <h2 class="sec_head">Category</h2>
    </div>
    <!-- blogs -->
    <div class="landing_blog_section row">
       <div class="landing_blog_slider_section col-md-12 no_padding">
          <div class="landing_blog_slider_all_category slider slick-slider-wrapper">
             @foreach($subCategory as $key => $subcategorydata)
             <div class="cu_slider_item_all_category">
                <img src="{{asset('sp_uploads/sub_categories/'.$subcategorydata->icon)}}">
                <!-- <img src="images/Detail_page_about_banner.jpg"> -->
                <p style="background-color: #ef2252;">{{$subcategorydata->category_name}}</p>
             </div>
              @endforeach
             
          </div>
       </div>
    </div>
 </div>
</section>
@endif
<!-- Add Categories section end -->

<!-- service category section -->
@if(!empty($programsCategory))
<section id="studio_center_section" class="studio_center_section padding_tb_60">
   <div class="container">
   <div class="section_header">
      <h2 class="sec_head">Clinical Beauty</h2>
   </div>
   <!-- Beauty -->
   <div class="weight_loss_prog_section category_section ">
      <div class="visit_categoy-1">
         <div class="caption_wrapper" style="padding-top:1.5em; text-align:right;">
            <div class="sld__content">
            	@foreach($programsCategory as $category)
               	<h2>{{$category->category_name}}</h2>
               @endforeach
            </div>
         </div>
         <div class="category_slider_section">
            <div class="weight_loss_prog slider sld__images">
            	@foreach($programsCategory as $category)
               <div class="cu_slider_item">
                  <a  href="#!" class="category_link_wrap">
                     <div class="image-wrap">
                        <img src="{{asset('sp_uploads/sub_categories/'.$subcategorydata->icon)}}" />
                     </div>
                  </a>
               </div>
               @endforeach
            </div>
         </div>
      </div>
   </div>
</section>
@endif
<!-- Studio/Center Blog -->

@if(!empty($forumContent))
<section id="studio_center_blog_section" class="studio_center_blog_section padding_tb_60 col-">
	<div class="container">
	    <div class="section_header">
	       <!-- <h3 class="sub_head">Mode of</h3> -->
	       <h2 class="sec_head">Studio / Center</h2>
	    </div>
	    <!-- Beauty -->
	    @foreach($forumContent as $key => $contentData)
	    <?php 
	    	$class = ($key == 0) ? 'margin-bottom:2em; margin-top: 6em;' : '';
	    ?>
	    <div class="col-lg-12 row" style="display: flex; {{ $class }}">
	    	@foreach($contentData as $key => $data)
	    	<?php
	    		$styleclass = ($key == 0) ? 'margin-right: 2em;' : (($key == 1) ? 'margin-right: 2em; bottom: 2em;' : 'top:');
	    	?>
	    	<div class="col-md-3 blog_wrap blog_wrap_new my-position" style="{{$styleclass}}">
	          <div class="date_category">{{ date("M-d-Y", strtotime($data->created_at)).' - '.$data->category_name }} </div>
	          <div class="discription" style="color: #151515;">
	             {{ $data->message }} 
	          </div>
	          <div class="response_on">*{{ $data->count_response }} response</div>
	          <div class="readmore_wrap"><a href="#!">Read more <i class="far fa-long-arrow-alt-right"></i></a></div>
	      	</div>
	      	@endforeach
	    </div>
	    @endforeach
	</div>
</section>
@endif
<!-- Studio/Center Blog end -->

<!-- Gift card section slider start -->
@if(isset($ecardThemeContent) && !empty($ecardThemeContent))
<section id="gift_card_section" class="gift_card_section padding_tb_100">
	<div class="container">
		<div class="section_header">
			<h2 class="sec_head">Gift Card</h2>
		</div>
		
		<div class="gift_card_section_wrap" id="slider">
			@foreach($ecardThemeContent as $key => $ecardData)
			<input type="radio" name="slider" id="s{{$key}}" checked>
			@endforeach
			@foreach($ecardThemeContent as $key => $ecardData)
			<label for="s{{$key}}" id="slide{{$key}}"><img src="{{asset('public/eCardThemes/'.$ecardData['image'])}}"></label>
			@endforeach		
		</div>
	</div>
</section>	
@endif
<!-- Gift card section slider end -->


<!-- Featured section -->
@if(!empty($featureSectionArr))
<section id="featured_offers_section" class="featured_offers_section padding_tb_60">
	<div class="container">
		<div class="featured_offers_section_wrapper-1">	
			<div class="section_header">
				<h2 class="sec_head">Featured</h2>
			</div>
			<!-- Beauty -->
			<div class="featured_offers_category_section">
			<ul class="nav nav-tabs offers_category_tabs" role="tablist">
			    <li class="nav-item">
			      <a class="nav-link active" data-toggle="tab" href="#store">Store</a>
			    </li>
			    <li class="nav-item">
			      <a class="nav-link" data-toggle="tab" href="#individual">Individual</a>
			    </li>
			</ul>

			<!-- Tab panes -->
			<div class="tab-content featured_offers_category_tab_content">
				@foreach($featureSectionArr as $mainkey => $featuredData)
				<div id="{{$mainkey}}" class=" tab-pane <?php echo ($mainkey == 'store') ? 'active' : '' ?>">
					<div class="featured_offers featured_{{$mainkey}}_offers">
						@foreach($featuredData as $data)
					    <div class="cu_slider_item">
					    	<div class="blog_wrap">
					    		<div class="image-wrap">
							        <img src="{{asset('sp_uploads/store/'.$data->store_image)}}" />
							    </div>
						      <div class="slider-caption-wrap">
						      	<div class="caption_wrap">
							      	<div class="caption_title">
							          	<h2>{{$data->store_name}}</h2>
							          </div>
							      	<div class="discription">
							      		{{$data->description}} 
							      	</div>
						        </div>
						        <div class="pricing_wrap">
						        	<div class="price"><div class="starting_from">Starting from</div>AED {{$data->price}}</div>
						        	<div class="button_wrap"><a href="beauty-listing" class="btn">Explore</a></div>
						        </div>
						      </div>
						    </div>
					    </div>
					    @endforeach
				  	</div>
				</div>
				@endforeach
			</div>		
			</div>
		</div>
	</div>
</section>
@endif
<!-- landing_contact_section -->
@include('contact');
<!-- landing_contact_section end -->

<!-- beutics_blog_section -->
@if(isset($pagesectionArr['Social Media Content']) && !empty($pagesectionArr['Social Media Content']))
<section class="beutics_blog_section padding_tb_60">
	<div class="container-fluid">
		<!-- blogs -->
		<div class="blog_section row">
			<div class="blog_slider_section col-md-9 offset-md-3 no_padding">
				<div class="blog_slider slider">
					@foreach($pagesectionArr['Social Media Content'] as $mediaBanners)
				    <div class="cu_slider_item">
				    	<div class="blog_wrap read-more-less" data-id="300">
					      <div class="slider-caption-wrap">
					      	<div class="caption_title">
					          	<h2>{{$mediaBanners->title}}</h2>
					          </div>
					      	<div class="discription read-toggle" data-id='0'>
					      		{{$mediaBanners->description}}
					      	</div>
					          
					      </div>
					    </div>
				    </div>
				    @endforeach
			  	</div>
			</div>
		</div>
	</div>
</section>
@endif
<!-- beutics_blog_section End-->
<button type="button">
	<a href="beauty-product">Detail page</a>
</button>
@endsection