@extends('layouts.layout')
@section('title','Detail')
@section('content')
<div class="header_height height_fitness_one"></div>
<!-- Home Page Banner Slider -->
<!-- Page Banner  -->
<div class="section-baner-full-width">
   <div class="container-fluid">
      <div class="container">
         <div class="row">
            <!-- breadcrumbs_section -->
            <!-- End breadcrumbs_section -->
            <!-- banner Left Side -->
            <div class="col-lg-4 col-sm-12 padding_remove">
               <div class="height-breadcrumbs">
                  <section class="breadcrumbs_section">
                     <div id="block-breadcrumbs" class="block-breadcrumbs">
                        <nav role="navigation" aria-label="breadcrumb">
                           <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="#">Home</a></li>
                              <li class="breadcrumb-item"><a  href="#">Client Listing</a></li>
                           </ol>
                        </nav>
                     </div>
                  </section>
                  <div class="rating-banner">
                     <span class="start-rating">
                     <i class="fas fa-star"></i></span> 
                     <span class="rating-ration">
                        4.5 
                        <spane class="total-rating">(123)</spane>
                     </span>
                  </div>
                  <h1 class="fitness-heading-banner">
                     The Fitness Culture
                  </h1>
                  <!-- time List in banner -->
                  <ul class="time_table_list">
                     <li>
                        <span class="clock_icon"><i class="fas fa-clock"></i></span>
                        <a href="#"> Open Now : 08.00 AM - 20.00 PM </a>
                     </li>
                     <li>
                        <span class="clock_icon"><i class="fas fa-map-marker-alt"></i>
                        </span>
                        <a href="#">Celesta Fiesta, 161, Sector 27, Gurugram, <br>
                        Haryana 122009
                        </a>
                     </li>
                     <li>
                        <span class="clock_icon"><i class="fas fa-map-marker-alt"></i>
                        </span>
                        <span class="km_road"> 2.3 km away <a href="#" class="view-map"> View Map </a> </span>
                     </li>
                  </ul>
                  <!-- End Section here -->
                  <!-- button listing  -->
                  <ul class="Gender_listing">
                     <li> <a href="#" class="Male_Button"> Male </a></li>
                     <li> <a href="#" class="Male_Button"> Female </a></li>
                     <li> <a href="#" class="Male_Button"> Kid </a></li>
                  </ul>
                  <!-- End here Sectionn -->
                  <!-- Flow Button Banner -->
                  <ul class="Flow_social_button">
                     <li> <a href="#" class="bookmark">Bookmark</a></li>
                     <li>
                        <a href="#" class="bookmark">
                           Share 
                           <spane class="share_icon"><img src="images/share_icon.png"></spane>
                        </a>
                     </li>
                     <li> <a href="#" class="bookmark">Add Review </a></li>
                  </ul>
                  <!-- End Here section-->
                  <!-- Start Tag Line -->
                  <div class="Tag_line_banner_bottom">
                     <p> <span class="Trainer_tag"> Trainer Speak :</span> English , Franch , Japenes</p>
                  </div>
                  <!-- End Here section -->
               </div>
            </div>
            <!-- End Here -->
            <!-- Right Side Banner Image -->
            <div class="col-lg-8 col-sm-12">
               <div class="Right_side_banner_image">
                  <img src="images/Detail_page_about_banner.jpg" class="" />
               </div>
            </div>
            <!-- End Here Section -->
         </div>
      </div>
   </div>
   <!-- aminities Section banner bottom -->
   <div class="banner_bottom_aminities">
      <div class="container">
         <h4 class="aminities">Aminities</h4>
         <ul class="aminities_list">
            <li> <span> <img src="images/weather_icon.png" class="img-fluid icon_block" /></span> 
               <a href="#" class="text-upper">Ac</a>
            </li>
            <li> <span> <img src="images/television_icon.png" class="img-fluid icon_block" /></span> 
               <a href="#">Television</a>
            </li>
            <li> <span> <img src="images/first-aid-kit_icon.png" class="img-fluid icon_block" /></span> 
               <a href="#">Television</a>
            </li>
            <li> <span> <img src="images/wifi-router_icon.png" class="img-fluid icon_block" /></span> 
               <a href="#">Television</a>
            </li>
         </ul>
      </div>
   </div>
   <!-- End Here section -->
</div>
<!-- End Here -->
<!-- Tabs Section -->
<section class="details_about_page_body">
   <div class="container">
      <div class="row">
         <!-- left Side body content  -->
         <div class="col-lg-8 col-sm-12">
            <!-- commone tab -->
            <div class="compone_tab_top">
               <ul class="top_link_tab">
                  <li><a href="beauty-details" class="link_tab_one">Activities</a></li>
                  <li><a href="instruct-details" class="link_tab_one">Instructor</a></li>
                  <li><a href="about-details" class="link_tab_one active">About Us</a></li>
                  <li><a href="review-details" class="link_tab_one">7 Reviews</a>
                     <span class="tab_rating_on_btn"> <a href=""> <i class="fas fa-star"></i> 6.5-Good </a></span>
                  </li>
               </ul>
            </div>
            <!-- End Here -->
            <!-- Start Activities_section -->
            <div class="Activities_section">
               <div class="container-fluid">
                  <div class="Activities_heading">
                     <h2>Activities</h2>
                  </div>
                  <div class="Activities_section_box">
                     <div class="Activities_section_box_heading">
                        <img src="images/member-card_icon.png" class="img-fluid">
                        <span class="yoga_text">Yoga Membership </span>
                     </div>
                     <div class="yoga_coupon">
                        <p class="yoga_coupon_left">
                           Yoga Membership Coupon
                        </p>
                        <ul class="yoga_group_right">
                           <li> <a href="#">Group</a></li>
                           <span class="divinder_Tab">|</span>
                           <li> <a href="#">Personal</a></li>
                        </ul>
                     </div>
                     <div class="calories_section">
                        <div class="calories_section_left">
                           <span class="calories_text">Calories : 215kcal </span>
                        </div>
                        <div class="calories_section_middel">
                           <span class="calories_text">Intensity :  <img src="images/level_img.png"/></span>
                        </div>
                        <ul class="calories_section_right">
                           <li> <a href="#">Level : Beginner</a></li>
                           <span class="divinder_Tab">|</span>
                           <li> <a href="#">Expert</a></li>
                        </ul>
                     </div>
                     <ul class="nav nav-tabs traning_tabs_about_details" id="myTab" role="tablist">
                        <li class="custom_tab_item">
                           <a class="custom_tab_one active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Online</a>
                        </li>
                        <li class="custom_tab_item">
                           <a class="custom_tab_one" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Trainer At Home</a>
                        </li>
                        <li class="custom_tab_item">
                           <a class="custom_tab_one" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Studio</a>
                        </li>
                     </ul>
                     <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active onine_tab" id="home" role="tabpanel" aria-labelledby="home-tab">
                           <div class="row">
                              <div class="col-md-5">
                                 <div class="tab_left_pera">
                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. dignissim qui blandit praesent luptatum <a href="#" class="read_more_btn">Read More...</a></p>
                                 </div>
                              </div>
                              <div class="col-md-7 all_padding_remove">
                                 <ul class="right_tab_date pt-3">
                                    <li> <span class="month_block">1 Month</span>
                                       Cashback : AED 50
                                    </li>
                                    <li> <span class="month_block_two">UEA 799
                                       </span>
                                       <del class="delete">UEA 899</del>
                                    </li>
                                    <li> <a href="#" class="add_button_tab"> Add </a> </li>
                                 </ul>
                                 <ul class="right_tab_date pt-2">
                                    <li> <span class="month_block">3 Month</span>
                                       Cashback : AED 50
                                    </li>
                                    <li> <span class="month_block_two">UEA 799
                                       </span>
                                       <del class="delete">UEA 899</del>
                                    </li>
                                    <li>
                                       <div class="number">
                                          <span class="minus"><i class="fas fa-minus"></i></span>
                                          <input type="text" value="1" class="add_tab_flied" />
                                          <span class="plus"><i class="fas fa-plus"></i></span>
                                       </div>
                                    </li>
                                 </ul>
                                 <ul class="right_tab_date pt-2">
                                    <li> <span class="month_block">6 Month</span>
                                       Cashback : AED 50
                                    </li>
                                    <li> <span class="month_block_two">UEA 799
                                       </span>
                                       <del class="delete">UEA 899</del>
                                    </li>
                                    <li> <a href="#" class="add_button_tab"> Add </a> </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">ssss.</div>
                        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">.sddsfsdf..</div>
                     </div>
                  </div>
                  <a href="#" class="view_activity_btn">View All Activity</a>
                  <div class="diver_section_bottom_line"></div>
               </div>
            </div>
            <!-- End Here Activities_section -->
            <!-- About Gym Section Start -->
            <div class="About_gym_section mt-5 pt-5 pb-5">
               <div class="container-fluid">
                  <div class="Activities_heading">
                     <h2>About Gym</h2>
                  </div>
                  <div class="row">
                     <div class="col-md-12">
                        <div class="about_gym_peragraph">
                           <p>
                              Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. 
                           </p>
                           <p>
                              Beutics is a marketplace for Beauty, Fitness & Wellness providers. With Beutics App the world of salons, spas, fitness centers, dental & dermatology clinics, trainers, instructors and many more are all at your finger tips. Beutics drives consumer value by offering unparalleled convenience and opportunities to save and earn.
                           </p>
                           <p>
                              Beutics is your companion App with two distinct instructions: Spoil you with convenience you ever imagined possible AND find more ways than one to help you save, Period! Embracing change, discovering talents, earning and saving have all been a distant dream in the past. 
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="diver_section_bottom_line"></div>
               </div>
            </div>
            <!-- About Gym Section End -->
            <!-- instrutor_section Start -->
            <div class="instrutor_section pb-5">
               <div class="container-fluid">
                  <div class="heading_instrutor">
                     <h2>Instructor</h2>
                  </div>
                  <div class="pt-5"></div>
                  <div class="instrutor_container">
                     <div class="instrutor_one col-md-3">
                        <div class="instrutor_img">
                           <img src="images/instrutor_one.png"/>
                        </div>
                        <div class="instrutor_detail">
                           <div class="instrutor_name">Luka Sabbat</div>
                           <div class="instrutor_work">Gym Expert</div>
                           <div class="instrutor_country">American</div>
                        </div>
                     </div>
                     <div class="instrutor_two col-md-3">
                        <div class="instrutor_img">
                           <img src="images/instrutor_two.png"/>
                        </div>
                        <div class="instrutor_detail">
                           <div class="instrutor_name">Sean O'Pry</div>
                           <div class="instrutor_work">Yoga Expert</div>
                           <div class="instrutor_country">Candian</div>
                        </div>
                     </div>
                     <div class="instrutor_one col-md-3">
                        <div class="instrutor_img">
                           <img src="images/instrutor_three.png"/>
                        </div>
                        <div class="instrutor_detail">
                           <div class="instrutor_name">Zach Miko</div>
                           <div class="instrutor_work">Dance Expert</div>
                           <div class="instrutor_country">American</div>
                        </div>
                     </div>
                     <div class="instrutor_two col-md-3">
                        <div class="instrutor_img">
                           <img src="images/instrutor_four.png"/>
                        </div>
                        <div class="instrutor_detail">
                           <div class="instrutor_name">Luka Sabbat</div>
                           <div class="instrutor_work">Gym Expert</div>
                           <div class="instrutor_country">American</div>
                        </div>
                     </div>
                  </div>
                  <div class="pt-4">
                     <a href="#" class="view_activity_btn">View All Activity</a>
                  </div>
               </div>
            </div>
            <!-- End Here instrutor_section -->
            <div class="Review_about_details_page">
               <div class="container-fluid">
                  <div class="heading_instrutor heading_Review ">
                     <h2>Review</h2>
                     <p>Tap to rate your experience</p>
                  </div>
                  <div class="star_review_bottom_section">
                     <span class="one"><i class="fas fa-star"></i></span>
                     <span class="one"><i class="fas fa-star"></i></span>
                     <span class="one"><i class="fas fa-star"></i></span>
                     <span class="one"><i class="fas fa-star"></i></span>
                     <span class="last_star"><i class="fas fa-star"></i></span>
                  </div>
                  <div class="write_review_bottom">
                     <p>Write your review</p>
                  </div>
                  <div class="diver_section_bottom_line"></div>
                  <div class="row pt-4 pb-5">
                     <div class="col-md-2">
                        <div class="img_review_left">
                           <img src="images/review_one.png" />
                        </div>
                     </div>
                     <div class="col-md-10">
                        <div class="review_text_right_main">
                           <h4>Angela Doe 
                              <span class="start_into_review_Section">
                              <span class="one"><i class="fas fa-star"></i></span>
                              <span class="one"><i class="fas fa-star"></i></span>
                              <span class="one"><i class="fas fa-star"></i></span>
                              <span class="one"><i class="fas fa-star"></i></span>
                              <span class="last_star"><i class="fas fa-star"></i></span>
                              </span>
                           </h4>
                           <p class="review_date_maine">November 16,2015, at 9:00 pm</p>
                           <p class="review_main_text_bottom">Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</p>
                        </div>
                     </div>
                  </div>
                  <div class="row pt-4 pb-5">
                     <div class="col-md-2">
                        <div class="img_review_left">
                           <img src="images/review_two.png" />
                        </div>
                     </div>
                     <div class="col-md-10">
                        <div class="review_text_right_main">
                           <h4>Jordi Prims
                              <span class="start_into_review_Section">
                              <span class="one"><i class="fas fa-star"></i></span>
                              <span class="one"><i class="fas fa-star"></i></span>
                              <span class="one"><i class="fas fa-star"></i></span>
                              <span class="one"><i class="fas fa-star"></i></span>
                              <span class="last_star"><i class="fas fa-star"></i></span>
                              </span>
                           </h4>
                           <p class="review_date_maine">November 16,2015, at 8:00 pm</p>
                           <p class="review_main_text_bottom">Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</p>
                        </div>
                     </div>
                  </div>
                  <div class="pt-3 pb-5">
                     <a href="#" class="view_activity_btn">View All Activity</a>
                  </div>
               </div>
            </div>
            <div class="gtm_slider_footee_up">
               <div class="heading_smiller_gym">
                  <h3>Similar Gym</h3>
               </div>
               <div class="similar_gym_slider">
                  <div class="item smillar_slider_item">
                     <div class="smillar_gym">
                        <div class="smillar_image-wrap">
                           <img src="images/featured_offer1.jpg">
                        </div>
                        <div class="slider-caption-wrap">
                           <div class="caption_wrap">
                              <div class="smillar_caption_title">
                                 <h4>CHISEL GURGAON</h4>
                              </div>
                              <div class="discription">
                                 Our branded microfiber pouches are excellent way to keep your frames.
                              </div>
                           </div>
                           <div class="pricing_wrap">
                              <div class="price">
                                 <div class="starting_from">Starting from</div>
                                 AED 50.00
                              </div>
                              <div class="button_wrap"><a href="#" class="btn">Explore</a></div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="item smillar_slider_item">
                     <div class="smillar_gym">
                        <div class="smillar_image-wrap">
                           <img src="images/featured_offer2.jpg">
                        </div>
                        <div class="slider-caption-wrap">
                           <div class="caption_wrap">
                              <div class="smillar_caption_title">
                                 <h4>CHISEL GURGAON</h4>
                              </div>
                              <div class="discription">
                                 Our branded microfiber pouches are excellent way to keep your frames.
                              </div>
                           </div>
                           <div class="pricing_wrap">
                              <div class="price">
                                 <div class="starting_from">Starting from</div>
                                 AED 50.00
                              </div>
                              <div class="button_wrap"><a href="#" class="btn">Explore</a></div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="item smillar_slider_item">
                     <div class="smillar_gym">
                        <div class="smillar_image-wrap">
                           <img src="images/featured_offer3.jpg">
                        </div>
                        <div class="slider-caption-wrap">
                           <div class="caption_wrap">
                              <div class="smillar_caption_title">
                                 <h4>CHISEL GURGAON</h4>
                              </div>
                              <div class="discription">
                                 Our branded microfiber pouches are excellent way to keep your frames.
                              </div>
                           </div>
                           <div class="pricing_wrap">
                              <div class="price">
                                 <div class="starting_from">Starting from</div>
                                 AED 50.00
                              </div>
                              <div class="button_wrap"><a href="#" class="btn">Explore</a></div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="item smillar_slider_item">
                     <div class="smillar_gym">
                        <div class="smillar_image-wrap">
                           <img src="images/featured_offer4.jpg">
                        </div>
                        <div class="slider-caption-wrap">
                           <div class="caption_wrap">
                              <div class="smillar_caption_title">
                                 <h4>CHISEL GURGAON</h4>
                              </div>
                              <div class="discription">
                                 Our branded microfiber pouches are excellent way to keep your frames.
                              </div>
                           </div>
                           <div class="pricing_wrap">
                              <div class="price">
                                 <div class="starting_from">Starting from</div>
                                 AED 50.00
                              </div>
                              <div class="button_wrap"><a href="#" class="btn">Explore</a></div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="item smillar_slider_item">
                     <div class="smillar_gym">
                        <div class="smillar_image-wrap">
                           <img src="images/featured_offer4.jpg">
                        </div>
                        <div class="slider-caption-wrap">
                           <div class="caption_wrap">
                              <div class="smillar_caption_title">
                                 <h4>CHISEL GURGAON</h4>
                              </div>
                              <div class="discription">
                                 Our branded microfiber pouches are excellent way to keep your frames.
                              </div>
                           </div>
                           <div class="pricing_wrap">
                              <div class="price">
                                 <div class="starting_from">Starting from</div>
                                 AED 50.00
                              </div>
                              <div class="button_wrap"><a href="#" class="btn">Explore</a></div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="item smillar_slider_item">
                     <div class="smillar_gym">
                        <div class="smillar_image-wrap">
                           <img src="images/featured_offer4.jpg">
                        </div>
                        <div class="slider-caption-wrap">
                           <div class="caption_wrap">
                              <div class="smillar_caption_title">
                                 <h4>CHISEL GURGAON</h4>
                              </div>
                              <div class="discription">
                                 Our branded microfiber pouches are excellent way to keep your frames.
                              </div>
                           </div>
                           <div class="pricing_wrap">
                              <div class="price">
                                 <div class="starting_from">Starting from</div>
                                 AED 50.00
                              </div>
                              <div class="button_wrap"><a href="#" class="btn">Explore</a></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- End Here col_md_8 -->
         <!-- Start Col-md-4 here -->
         <div class="col-lg-4 col-sm-12">
            <div class="trail_maine_section">
               <div class="trail_sessions">
                  <div class="slider-container">
                     <div class="slider-for">
                        <div class="item-slick play_button_style">
                           <img src="images/workout_image_1.png" alt="Alt">
                           <a class="button is-play" href="#">
                              <div class="button-outer-circle has-scale-animation"></div>
                              <div class="button-outer-circle has-scale-animation has-delay-short"></div>
                              <div class="button-icon is-play">
                                 <svg height="100%" width="100%" fill="#373737">
                                    <polygon class="triangle" points="5,0 30,15 5,30" viewBox="0 0 30 15"></polygon>
                                    <path class="path" d="M5,0 L30,15 L5,30z" fill="none" stroke="#373737" stroke-width="1"></path>
                                 </svg>
                              </div>
                           </a>
                        </div>
                        <div class="item-slick">
                           <img src="images/workout_image_1.png" alt="Alt">
                           <a class="button is-play" href="#">
                              <div class="button-outer-circle has-scale-animation"></div>
                              <div class="button-outer-circle has-scale-animation has-delay-short"></div>
                              <div class="button-icon is-play">
                                 <svg height="100%" width="100%" fill="#373737">
                                    <polygon class="triangle" points="5,0 30,15 5,30" viewBox="0 0 30 15"></polygon>
                                    <path class="path" d="M5,0 L30,15 L5,30z" fill="none" stroke="#373737" stroke-width="1"></path>
                                 </svg>
                              </div>
                           </a>
                        </div>
                        <div class="item-slick">
                           <img src="images/workout_image_1.png" alt="Alt">
                           <a class="button is-play" href="#">
                              <div class="button-outer-circle has-scale-animation"></div>
                              <div class="button-outer-circle has-scale-animation has-delay-short"></div>
                              <div class="button-icon is-play">
                                 <svg height="100%" width="100%" fill="#373737">
                                    <polygon class="triangle" points="5,0 30,15 5,30" viewBox="0 0 30 15"></polygon>
                                    <path class="path" d="M5,0 L30,15 L5,30z" fill="none" stroke="#373737" stroke-width="1"></path>
                                 </svg>
                              </div>
                           </a>
                        </div>
                        <div class="item-slick">
                           <img src="images/workout_image_1.png" alt="Alt">
                           <a class="button is-play" href="#">
                              <div class="button-outer-circle has-scale-animation"></div>
                              <div class="button-outer-circle has-scale-animation has-delay-short"></div>
                              <div class="button-icon is-play">
                                 <svg height="100%" width="100%" fill="#373737">
                                    <polygon class="triangle" points="5,0 30,15 5,30" viewBox="0 0 30 15"></polygon>
                                    <path class="path" d="M5,0 L30,15 L5,30z" fill="none" stroke="#373737" stroke-width="1"></path>
                                 </svg>
                              </div>
                           </a>
                        </div>
                     </div>
                     <div class="slider-nav thumbnail_slick_slider_image"> 
                        <a href="#"  data-toggle="modal" data-target="#Gallery_pop_Up"><img class="item-slick" src="images/workout_image_1_thum.png" alt="Alt"></a>
                        <a href="#"  data-toggle="modal" data-target="#Gallery_pop_Up"><img class="item-slick" src="images/workout_image_2_thum.png" alt="Alt"></a>
                        <a href="#"  data-toggle="modal" data-target="#Gallery_pop_Up"><img class="item-slick" src="images/workout_image_3_thum.png" alt="Alt"></a> 
                        <a href="#"  data-toggle="modal" data-target="#Gallery_pop_Up"><img class="item-slick" src="images/workout_image_1_thum.png" alt="Alt"></a>
                     </div>
                     <div class="modal fade" id="Gallery_pop_Up" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog booking_gallery_popup" role="document">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <button type="button" class="close photo_gallery_popup" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true"><i class="fas fa-times"></i></span>
                                 </button>
                              </div>
                              <div class="modal-body">
                                 <div class="row">
                                    <div class="col-md-5">
                                       <div class="product-carousel">
                                          <div class="product">
                                             <div class="item">
                                                <img class="product-image" src="images/booking_gallery_popup.png" />
                                             </div>
                                          </div>
                                          <div class="product">
                                             <div class="item">
                                                <img class="product-image" src="images/booking_gallery_popup.png" />
                                             </div>
                                          </div>
                                          <div class="product">
                                             <div class="item">
                                                <img class="product-image" src="images/booking_gallery_popup.png" />
                                             </div>
                                          </div>
                                          <div class="product">
                                             <div class="item">
                                                <img class="product-image" src="images/booking_gallery_popup.png" />
                                             </div>
                                          </div>
                                          <div class="product">
                                             <div class="item">
                                                <img class="product-image" src="images/booking_gallery_popup.png" />
                                             </div>
                                          </div>
                                          <div class="product">
                                             <div class="item">
                                                <img class="product-image" src="images/booking_gallery_popup.png" />
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-md-7">
                                       <div class="right_side_gallery_popup">
                                          <h3 class="popup_heading_photo">Photo (34)</h3>
                                          <p class="fliter_tab_gallery_popup">Filter by Facilites</p>
                                          <ul class="popup_gallery_listing">
                                             <li><a href="#" class="popup_tabs_gallery">All (34)</a></li>
                                             <li><a href="#" class="popup_tabs_gallery">Stretching Area (7)</a></li>
                                             <li><a href="#" class="popup_tabs_gallery">Weight Section (11)</a></li>
                                             <li><a href="#" class="popup_tabs_gallery">Cardio Section (4)</a></li>
                                             <li><a href="#" class="popup_tabs_gallery">Spinning Cycling Room (4)</a></li>
                                             <li><a href="#" class="popup_tabs_gallery">Cafe / Nutrition Outlet (2)</a></li>
                                             <li><a href="#" class="popup_tabs_gallery">Miscellaneous (3)</a></li>
                                             <li><a href="#" class="popup_tabs_gallery">Steam / Sauna / Jacuzzi (3)</a></li>
                                             <li><a href="#" class="popup_tabs_gallery">Changing Room (2)	</a></li>
                                          </ul>
                                          <p class="fliter_tab_gallery_popup pt-2 pb-3">Filter by Offering</p>
                                          <ul class="popup_gallery_listing">
                                             <li><a href="#" class="popup_tabs_gallery">All (34)</a></li>
                                             <li><a href="#" class="popup_tabs_gallery">Gym Membership + Group Activites (17)</a></li>
                                          </ul>
                                          <div class="diver_section_bottom_line"></div>
                                          <div class="footer_popup_gallery">
                                             <div class="experince_text">Experience The Studio</div>
                                             <div class="booking_popup_btn"><a href="#">Book A Session</a></div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="all_sessions_btn"><a href="#">Trail Sessions</a></div>
                  <div class="free_trail_text"><a href="#">30 days free trail</a></div>
                  <div class="trai_peragraph">
                     <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam. </p>
                  </div>
               </div>
            </div>
            <div class="your_cart_new">
               <div class="all_sessions_btn_new"><a href="#">Your Cart</a></div>
               <div class="whole_sidebar_cart-section_bg">
                  <div class="table_section-right_sidebar">
                     <div class="membership_text">
                        <div class="name_gym"> Gym membership   <span class="multiplue_x"> x 1</span></div>
                     </div>
                     <div class="Code_name_sidebar">
                        <div class="price_amount_bar">AED 20</div>
                     </div>
                  </div>
                  <div class="table_section-right_sidebar">
                     <div class="membership_text">
                        <div class="name_gym"> Gym membership   <span class="multiplue_x"> x 1</span></div>
                     </div>
                     <div class="Code_name_sidebar">
                        <div class="price_amount_bar">AED 20</div>
                     </div>
                  </div>
               </div>
               <div class="whole_sidebar_cart-section_bg">
                  <div class="table_section-right_sidebar">
                     <div class="membership_text">
                        <div class="name_gym"> Subtotal</div>
                     </div>
                     <div class="Code_name_sidebar">
                        <div class="price_amount_bar">AED 20</div>
                     </div>
                  </div>
                  <div class="table_section-right_sidebar">
                     <div class="membership_text">
                        <div class="name_gym">Discount allowed</div>
                     </div>
                     <div class="Code_name_sidebar">
                        <div class="price_amount_bar">AED 20</div>
                     </div>
                  </div>
                  <div class="blue_line_devider"></div>
                  <div class="apply_coupon_code_section">
                     <div class="apply_coupon_text">
                        Apply Coupon
                     </div>
                     <div class="coupon_code_all_here">
                        <div class="coupon_code_first">
                           <div class="coupon_code_left_Side">
                              <a href="#" class="btn_coupon_code_border">BEUTICS10</a>
                              <div class="coupon_date_about">
                                 <p>Lorem ipsum dolor sit amet</p>
                                 <p>Expires : 31st Dec 2020  |   11:03 A M</p>
                              </div>
                           </div>
                           <div class="coupon_code_discount">
                              30%
                              <span class="off_coupon_code">Off</span>
                           </div>
                        </div>
                        <div class="coupon_code_first mt-5">
                           <div class="coupon_code_left_Side">
                              <a href="#" class="btn_coupon_code_border">BEUTICS10</a>
                              <div class="coupon_date_about">
                                 <p>Lorem ipsum dolor sit amet</p>
                                 <p>Expires : 31st Dec 2020  |   11:03 A M</p>
                              </div>
                           </div>
                           <div class="coupon_code_discount">
                              50%
                              <span class="off_coupon_code">Off</span>
                           </div>
                        </div>
                        <div class="coupon_code_first mt-5 mb-5">
                           <div class="coupon_code_left_Side">
                              <a href="#" class="btn_coupon_code_border">BEUTICS10</a>
                              <div class="coupon_date_about">
                                 <p>Lorem ipsum dolor sit amet</p>
                                 <p>Expires : 31st Dec 2020  |   11:03 A M</p>
                              </div>
                           </div>
                           <div class="coupon_code_discount">
                              30%
                              <span class="off_coupon_code">Off</span>
                           </div>
                        </div>
                        <div class="coupon_code_first mt-5 mb-5">
                           <div class="coupon_code_left_Side">
                              <a href="#" class="btn_coupon_code_border">BEUTICS10</a>
                              <div class="coupon_date_about">
                                 <p>Lorem ipsum dolor sit amet</p>
                                 <p>Expires : 31st Dec 2020  |   11:03 A M</p>
                              </div>
                           </div>
                           <div class="coupon_code_discount">
                              30%
                              <span class="off_coupon_code">Off</span>
                           </div>
                        </div>
                        <div class="coupon_code_first mt-5 mb-5">
                           <div class="coupon_code_left_Side">
                              <a href="#" class="btn_coupon_code_border">BEUTICS10</a>
                              <div class="coupon_date_about">
                                 <p>Lorem ipsum dolor sit amet</p>
                                 <p>Expires : 31st Dec 2020  |   11:03 A M</p>
                              </div>
                           </div>
                           <div class="coupon_code_discount">
                              30%
                              <span class="off_coupon_code">Off</span>
                           </div>
                        </div>
                     </div>
                     <!-- All Coupon end here -->
                     <div class="coupon_input_filed">
                        <input  class="enter_code" type="text" Placeholder="Enter Coupon Code" />
                        <button class="apply_coupon_code_btn" type="button">Apply</button>
                     </div>
                  </div>
               </div>
               <div class="whole_sidebar_cart-section_bg">
                  <div class="table_section-right_sidebar">
                     <div class="membership_text">
                        <div class="name_gym">Total</div>
                     </div>
                     <div class="Code_name_sidebar">
                        <div class="price_amount_bar">AED 20</div>
                     </div>
                  </div>
                  <div class="blue_line_devider"></div>
                  <div class="table_section-right_sidebar pb-5">
                     <div class="membership_text">
                        <div class="name_gym save_AED">
                           You are saving 
                           <spane class="code_UEA799">AED 50</spane>
                        </div>
                        <div class="name_gym  save_AED"> Cashback earned  AED 30
                        </div>
                     </div>
                     <!-- <div class="Code_name_sidebar">
                        <div class="price_amount_bar">AED 20</div>
                        </div> -->
                  </div>
               </div>
               <button class="apply_coupon_code_btn Checkout_btn_sidebar" type="button">Checkout</button>
            </div>
         </div>
         <!-- End Here Col-md-4 -->
      </div>
   </div>
</section>
<!-- End Here -->
<!-- beutics_blog_section -->
<section class="beutics_blog_section padding_tb_60">
   <div class="container-fluid">
      <!-- blogs -->
      <div class="blog_section row">
         <div class="blog_slider_section col-md-9 offset-md-3 no_padding">
            <div class="blog_slider slider">
               <div class="cu_slider_item">
                  <div class="blog_wrap">
                     <div class="slider-caption-wrap">
                        <div class="caption_title">
                           <h2>What is Lorem Ipsum ?</h2>
                        </div>
                        <div class="discription">
                           Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </div>
                     </div>
                  </div>
               </div>
               <div class="cu_slider_item">
                  <div class="blog_wrap">
                     <div class="slider-caption-wrap">
                        <div class="caption_title">
                           <h2>What is Lorem Ipsum ?</h2>
                        </div>
                        <div class="discription">
                           Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </div>
                     </div>
                  </div>
               </div>
               <div class="cu_slider_item">
                  <div class="blog_wrap">
                     <div class="slider-caption-wrap">
                        <div class="caption_title">
                           <h2>What is Lorem Ipsum ?</h2>
                        </div>
                        <div class="discription">
                           Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </div>
                     </div>
                  </div>
               </div>
               <div class="cu_slider_item">
                  <div class="blog_wrap">
                     <div class="slider-caption-wrap">
                        <div class="caption_title">
                           <h2>What is Lorem Ipsum ?</h2>
                        </div>
                        <div class="discription">
                           Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </div>
                     </div>
                  </div>
               </div>
               <div class="cu_slider_item">
                  <div class="blog_wrap">
                     <div class="slider-caption-wrap">
                        <div class="caption_title">
                           <h2>What is Lorem Ipsum ?</h2>
                        </div>
                        <div class="discription">
                           Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- beutics_blog_section End-->
@endsection