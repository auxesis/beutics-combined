@extends('layouts.layout')
@section('title','Detail')
@section('content')
<div class="header_height  height_fitness_one"></div>
    <!-- Home Page Banner Slider -->
    <!-- Page Banner  -->
    <div class="section-baner-full-width">
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <!-- breadcrumbs_section -->
                    <!-- End breadcrumbs_section -->
                    <!-- banner Left Side -->
                    <div class="col-lg-4 col-sm-12 padding_remove">
                        <div class="height-breadcrumbs">
                            <section class="breadcrumbs_section">
                                <div id="block-breadcrumbs" class="block-breadcrumbs">
                                    <nav role="navigation" aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                                            <li class="breadcrumb-item"><a href="#">Client Listing</a></li>
                                        </ol>
                                    </nav>
                                </div>
                            </section>
                            <div class="rating-banner"><span class="start-rating">
                            <i class="fas fa-star"></i></span> <span class="rating-ration">4.5 
                               <spane class="total-rating">(123)</spane>
                             </span> </div>
                            <h1 class="fitness-heading-banner">
                    The Fitness Culture
                </h1>
                            <!-- time List in banner -->
                            <ul class="time_table_list">
                                <li> <span class="clock_icon"><i class="fas fa-clock"></i></span> <a href="#"> Open Now : 08.00 AM - 20.00 PM </a> </li>
                                <li> <span class="clock_icon"><i class="fas fa-map-marker-alt"></i>
                            </span> <a href="#">Celesta Fiesta, 161, Sector 27, Gurugram, <br>
                                        Haryana 122009
                            </a> </li>
                                <li> <span class="clock_icon"><i class="fas fa-map-marker-alt"></i>
                            </span> <span class="km_road"> 2.3 km away <a href="#" class="view-map"> View Map </a> </span> </li>
                            </ul>
                            <!-- End Section here -->
                            <!-- button listing  -->
                            <ul class="Gender_listing">
                                <li> <a href="#" class="Male_Button"> Male </a></li>
                                <li> <a href="#" class="Male_Button"> Female </a></li>
                                <li> <a href="#" class="Male_Button"> Kid </a></li>
                            </ul>
                            <!-- End here Sectionn -->
                            <!-- Flow Button Banner -->
                            <ul class="Flow_social_button">
                                <li> <a href="#" class="bookmark">Bookmark</a></li>
                                <li> <a href="#" class="bookmark">Share <spane class="share_icon"><img src="images/share_icon.png"></spane> </a></li>
                                <li> <a href="#" class="bookmark">Add Review </a></li>
                            </ul>
                            <!-- End Here section-->
                            <!-- Start Tag Line -->
                            <div class="Tag_line_banner_bottom">
                                <p> <span class="Trainer_tag"> Trainer Speak :</span> English , Franch , Japenes</p>
                            </div>
                            <!-- End Here section -->
                        </div>
                    </div>
                    <!-- End Here -->
                    <!-- Right Side Banner Image -->
                    <div class="col-lg-8 col-sm-12">
                        <div class="Right_side_banner_image"> <img src="images/Detail_page_about_banner.jpg" class="" /> </div>
                    </div>
                    <!-- End Here Section -->
                </div>
            </div>
        </div>
        <!-- aminities Section banner bottom -->
        <div class="banner_bottom_aminities">
            <div class="container no_padding">
                <h4 class="aminities">Aminities</h4>
                <ul class="aminities_list">
                    <li> <span> <img src="images/weather_icon.png" class="img-fluid icon_block" /></span> <a href="#" class="text-upper">Ac</a> </li>
                    <li> <span> <img src="images/television_icon.png" class="img-fluid icon_block" /></span> <a href="#">Television</a> </li>
                    <li> <span> <img src="images/first-aid-kit_icon.png" class="img-fluid icon_block" /></span> <a href="#">Television</a> </li>
                    <li> <span> <img src="images/wifi-router_icon.png" class="img-fluid icon_block" /></span> <a href="#">Television</a> </li>
                </ul>
            </div>
        </div>
        <!-- End Here section -->
    </div>
    <!-- End Here -->
    <!-- Tabs Section -->
    <section class="details_about_page_body">
        <div class="container">
            <div class="row">
                <!-- left Side body content  -->
                <div class="col-lg-8 col-sm-12">
                    <!-- commone tab -->
                    <div class="compone_tab_top">
                        <ul class="top_link_tab Product_tabs_link"> 
                            <li><a href="#" class="link_tab_one Product_tabs_one">Choices</a></li>
                            <li><a href="#" class="link_tab_one Product_tabs_one">Price</a></li>
                            <li><a href="instruct-details" class="link_tab_one Product_tabs_one">Staff</a></li>
                            <li><a href="beauty-product" class="link_tab_one Product_tabs_one">Products</a></li>
                            
                            <li><a href="review-details" class="link_tab_one Product_tabs_one">7 Reviews</a>
                            <span class="new_rating_review"> <a href=""> <i class="fas fa-star"></i> 6.5-Good </a></span>
                            </li>
                            <li><a href="about-details" class="link_tab_one Product_tabs_one">About</a></li>
                        </ul>
                        
                    </div>
                    <!-- End Here -->
                    <!-- Start Activities_section -->
                    <div class="Activities_section">
                        <div class="container no_padding">
                            <div class="nav nav-tabs nav-fill activity_tabs_section">
                                <a data-toggle="tab" href="#all" aria-controls="all"  role="tab" aria-selected="true">All</a> 
                                <a data-toggle="tab" href="#yoga" aria-controls="yoga"  role="tab" aria-selected="false">YOGA</a> 
                                <a data-toggle="tab" href="#gym" aria-controls="gym"  role="tab" aria-selected="false">GYM</a> 
                                <a data-toggle="tab" href="#Dance" aria-controls="Dance"  role="tab" aria-selected="false">DANCE</a> 
                                <a data-toggle="tab" href="#boxing" aria-controls="boxing"  role="tab" aria-selected="false">BOXING</a> 
                            </div>
                            <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                            <div id="all" class="tab-pane fade custome_none show active" >
                                <h3 class="heading_store_page">Hair</h3>
                                   <div class="product_in_store_border_box">
                                         <div class="hair_box_tab_heading">
                                             <span class="hair_heading_box">Hair Colouring</span>
                                             <ul class="hair_tabs_in_box">
                                                 <li><a href="" class="maie_tabs_box">Male</a></li>
                                                 <li><a href="" class="maie_tabs_box">Female</a></li>
                                                 <li><a href="" class="maie_tabs_box">Kids</a></li>

                                             </ul>
                                         </div>   

                                         <ul class="booking_tab_in_box">
                                              <li><a href="#" class="live_at_home_tab">Live At Home</a></li>
                                              <li><a href="#" class="live_at_home_tab">Trainer At Home</a></li>
                                              <li><a href="#" class="live_at_home_tab">Store</a></li>

                                         </ul>

                                         <div class="book_your_hair_treatment">
                                                <div class="hair_pera_about_box pt-3">
                                                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy 
                                                        nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. 
                                                        Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper 
                                                        <spane class="read_more">Read more..</spane></p>
                                               </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="title">
                                                            Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                            <span href="#" data-toggle="tooltip" data-placement="top" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                            <div class="read_more uea_price">UEA 799</div>
                                                            <div class="texes_inc">Inc. of all taxes</div>
                                                            <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                            <a href="#" class="add_button_tab"> Add </a>
                                                        </div>
                                                    </div>
                                                   
                                                    <div class="col-md-4">
                                                        <div class="title">
                                                            Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                            <span href="#" data-toggle="tooltip" data-placement="top" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                            <div class="read_more uea_price">UEA 799</div>
                                                            <div class="texes_inc">Inc. of all taxes</div>
                                                            <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                            <div class="number">
                                                                <span class="minus"><i class="fas fa-minus"></i></span>
                                                                <input type="text" value="1" class="add_tab_flied">
                                                                <span class="plus"><i class="fas fa-plus"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="title">
                                                            Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                            <span href="#" data-toggle="tooltip" data-placement="top" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                            <div class="read_more uea_price">UEA 799</div>
                                                            <div class="texes_inc">Inc. of all taxes</div>
                                                            <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                            <a href="#" class="add_button_tab"> Add </a>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row pt-5">
                                                    <div class="col-md-4">
                                                        <div class="title">
                                                            Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                            <span href="#" data-toggle="tooltip" data-placement="top" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                            <div class="read_more uea_price">UEA 799</div>
                                                            <div class="texes_inc">Inc. of all taxes</div>
                                                            <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                            <a href="#" class="add_button_tab"> Add </a>
                                                        </div>
                                                    </div>
                                                   
                                                    <div class="col-md-4">
                                                        <div class="title">
                                                            Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                            <span href="#" data-toggle="tooltip" data-placement="top" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                            <div class="read_more uea_price">UEA 799</div>
                                                            <div class="texes_inc">Inc. of all taxes</div>
                                                            <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                            <div class="number">
                                                                <span class="minus"><i class="fas fa-minus"></i></span>
                                                                <input type="text" value="1" class="add_tab_flied">
                                                                <span class="plus"><i class="fas fa-plus"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="title">
                                                            Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                            <span href="#" data-toggle="tooltip" data-placement="top" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                            <div class="read_more uea_price">UEA 799</div>
                                                            <div class="texes_inc">Inc. of all taxes</div>
                                                            <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                            <a href="#" class="add_button_tab"> Add </a>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row pt-5">
                                                    <div class="col-md-4">
                                                        <div class="title">
                                                            Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                            <span href="#" data-toggle="tooltip" data-placement="top" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                            <div class="read_more uea_price">UEA 799</div>
                                                            <div class="texes_inc">Inc. of all taxes</div>
                                                            <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                            <a href="#" class="add_button_tab"> Add </a>
                                                        </div>
                                                    </div>
                                                   
                                                    <div class="col-md-4">
                                                        <div class="title">
                                                            Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                            <span href="#" data-toggle="tooltip" data-placement="top" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                            <div class="read_more uea_price">UEA 799</div>
                                                            <div class="texes_inc">Inc. of all taxes</div>
                                                            <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                            <div class="number">
                                                                <span class="minus"><i class="fas fa-minus"></i></span>
                                                                <input type="text" value="1" class="add_tab_flied">
                                                                <span class="plus"><i class="fas fa-plus"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="title">
                                                            Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                            <span href="#" data-toggle="tooltip" data-placement="top" title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                            <div class="read_more uea_price">UEA 799</div>
                                                            <div class="texes_inc">Inc. of all taxes</div>
                                                            <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                            <a href="#" class="add_button_tab"> Add </a>
                                                        </div>
                                                    </div>

                                                </div>

                                         </div>
                                         
                                   </div>

                                   <!-- next section -->
                                   <div class="product_in_store_border_box mt-5">
                                    <div class="hair_box_tab_heading">
                                        <span class="hair_heading_box">Hair Treatment</span>
                                        <ul class="hair_tabs_in_box">
                                            <li><a href="" class="maie_tabs_box">Male</a></li>
                                            <li><a href="" class="maie_tabs_box">Female</a></li>
                                            <li><a href="" class="maie_tabs_box">Kids</a></li>

                                        </ul>
                                        <a href="#" class="view_all_hair">View All</a>
                                    </div>   

                                    <ul class="booking_tab_in_box">
                                         <li><a href="#" class="live_at_home_tab">Live At Home</a></li>
                                         <li><a href="#" class="live_at_home_tab">Trainer At Home</a></li>
                                         <li><a href="#" class="live_at_home_tab">Store</a></li>

                                    </ul>

                                    <div class="hair_pera_about_box pt-3">
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy 
                                            nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. 
                                            Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper 
                                            <spane class="read_more">Read more..</spane></p>
                                   </div>
                                   <div class="starting_box_flex">
                                   <div class="starting_price_display">
                                   <div class="title">
                                    Small <span class="duration_time"> ( Duration Time : 15min, 20min, 30min, 45min, 1hrs, 2hr, 3 hrs ) </span></div>
                                    <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div> 
                                </div>
                                <div class="right_starting_from">
                                    <p>Starting From</p>
                                    <p class="code_UEA799">UEA 799</p>
                                    
                                </div>
                            </div>
                                </div>
                                   
                                   <!-- End Here  -->
                                    <!-- next section -->
                                    <div class="product_in_store_border_box mt-5">
                                        <div class="hair_box_tab_heading">
                                            <span class="hair_heading_box">Offer</span>
                                            <ul class="hair_tabs_in_box">
                                                <li><a href="" class="maie_tabs_box">Male</a></li>
                                                <li><a href="" class="maie_tabs_box">Female</a></li>
                                                <li><a href="" class="maie_tabs_box">Kids</a></li>
    
                                            </ul>
                                            <a href="#" class="view_all_hair">View All</a>
                                        </div>   
    
                                        <ul class="booking_tab_in_box">
                                             <li><a href="#" class="live_at_home_tab">Live At Home</a></li>
                                             <li><a href="#" class="live_at_home_tab">Trainer At Home</a></li>
                                             <li><a href="#" class="live_at_home_tab">Store</a></li>
    
                                        </ul>
    
                                        <div class="hair_pera_about_box pt-3">
                                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy 
                                                nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. 
                                                Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper 
                                                <spane class="read_more">Read more..</spane></p>
                                       </div>
                                       <div class="starting_box_flex">
                                       <div class="starting_price_display">
                                       <div class="title">
                                        Small <span class="duration_time"> ( Duration Time : 15min, 20min, 30min, 45min, 1hrs, 2hr, 3 hrs ) </span></div>
                                        <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div> 
                                    </div>
                                    <div class="right_starting_from">
                                        <p><del>UEA 655</del></p>
                                        <p class="code_UEA799">UEA 799</p>
                                        
                                    </div>
                                </div>
                                    </div>
                                       
                                       <!-- End Here  -->
                                   
                                
                            </div>
                            <div id="yoga" class="tab-pane fade custome_none">
                                
                            </div>
                            <div id="gym" class="tab-pane fade custome_none">
                                <h3>Menu 12</h3>
                                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            </div>
                            <div id="Dance" class="tab-pane fade custome_none">
                                <h3>Menu 13</h3>
                                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            </div>
                            <div id="boxing" class="tab-pane fade">
                                <h3>Menu 14</h3>
                                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            </div>
                            </div>
                            <a href="#" class="view_activity_btn">View All Activity</a>
                            <div class="diver_section_bottom_line"></div>
                        </div>
                    </div>
                    <!-- End Here Activities_section -->
            
                    <!-- Collapse Here For FAQ -->
                    <div id="accordion" class="myaccordion">
                        <div class="card">
                          <div class="card-header" id="headingOne">
                            <h2 class="mb-0">
                              <button class="d-flex align-items-center justify-content-between btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                Colouring
                                <span class="fa-stack fa-sm">
                                 
                                  <i class="fas fa-chevron-right "></i>
                                </span>
                              </button>
                            </h2>
                          </div>
                          <div id="collapseOne" class="collapse faq" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                <div class="product_in_store_border_box">
                                    <div class="hair_box_tab_heading">
                                        <span class="hair_heading_box">Hair Colouring</span>
                                        <ul class="hair_tabs_in_box">
                                            <li><a href="" class="maie_tabs_box">Male</a></li>
                                            <li><a href="" class="maie_tabs_box">Female</a></li>
                                            <li><a href="" class="maie_tabs_box">Kids</a></li>

                                        </ul>
                                    </div>   

                                    <ul class="booking_tab_in_box">
                                         <li><a href="#" class="live_at_home_tab">Live At Home</a></li>
                                         <li><a href="#" class="live_at_home_tab">Trainer At Home</a></li>
                                         <li><a href="#" class="live_at_home_tab">Store</a></li>

                                    </ul>

                                    <div class="book_your_hair_treatment">
                                           <div class="hair_pera_about_box pt-3">
                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy 
                                                   nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. 
                                                   Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper 
                                                   <spane class="read_more">Read more..</spane></p>
                                          </div>
                                           <div class="row">
                                               <div class="col-md-4">
                                                   <div class="title">
                                                       Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                       <span href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                       <div class="read_more uea_price">UEA 799</div>
                                                       <div class="texes_inc">Inc. of all taxes</div>
                                                       <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                       <a href="#" class="add_button_tab"> Add </a>
                                                   </div>
                                               </div>
                                              
                                               <div class="col-md-4">
                                                   <div class="title">
                                                       Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                       <span href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                       <div class="read_more uea_price">UEA 799</div>
                                                       <div class="texes_inc">Inc. of all taxes</div>
                                                       <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                       <div class="number">
                                                           <span class="minus"><i class="fas fa-minus"></i></span>
                                                           <input type="text" value="1" class="add_tab_flied">
                                                           <span class="plus"><i class="fas fa-plus"></i></span>
                                                       </div>
                                                   </div>
                                               </div>

                                               <div class="col-md-4">
                                                   <div class="title">
                                                       Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                       <span href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                       <div class="read_more uea_price">UEA 799</div>
                                                       <div class="texes_inc">Inc. of all taxes</div>
                                                       <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                       <a href="#" class="add_button_tab"> Add </a>
                                                   </div>
                                               </div>

                                           </div>

                                           <div class="row pt-5">
                                               <div class="col-md-4">
                                                   <div class="title">
                                                       Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                       <span href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                       <div class="read_more uea_price">UEA 799</div>
                                                       <div class="texes_inc">Inc. of all taxes</div>
                                                       <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                       <a href="#" class="add_button_tab"> Add </a>
                                                   </div>
                                               </div>
                                              
                                               <div class="col-md-4">
                                                   <div class="title">
                                                       Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                       <span href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                       <div class="read_more uea_price">UEA 799</div>
                                                       <div class="texes_inc">Inc. of all taxes</div>
                                                       <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                       <div class="number">
                                                           <span class="minus"><i class="fas fa-minus"></i></span>
                                                           <input type="text" value="1" class="add_tab_flied">
                                                           <span class="plus"><i class="fas fa-plus"></i></span>
                                                       </div>
                                                   </div>
                                               </div>

                                               <div class="col-md-4">
                                                   <div class="title">
                                                       Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                       <span href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                       <div class="read_more uea_price">UEA 799</div>
                                                       <div class="texes_inc">Inc. of all taxes</div>
                                                       <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                       <a href="#" class="add_button_tab"> Add </a>
                                                   </div>
                                               </div>

                                           </div>
                                           <div class="row pt-5">
                                               <div class="col-md-4">
                                                   <div class="title">
                                                       Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                       <span href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                       <div class="read_more uea_price">UEA 799</div>
                                                       <div class="texes_inc">Inc. of all taxes</div>
                                                       <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                       <a href="#" class="add_button_tab"> Add </a>
                                                   </div>
                                               </div>
                                              
                                               <div class="col-md-4">
                                                   <div class="title">
                                                       Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                       <span href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                       <div class="read_more uea_price">UEA 799</div>
                                                       <div class="texes_inc">Inc. of all taxes</div>
                                                       <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                       <div class="number">
                                                           <span class="minus"><i class="fas fa-minus"></i></span>
                                                           <input type="text" value="1" class="add_tab_flied">
                                                           <span class="plus"><i class="fas fa-plus"></i></span>
                                                       </div>
                                                   </div>
                                               </div>

                                               <div class="col-md-4">
                                                   <div class="title">
                                                       Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                       <span href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                       <div class="read_more uea_price">UEA 799</div>
                                                       <div class="texes_inc">Inc. of all taxes</div>
                                                       <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                       <a href="#" class="add_button_tab"> Add </a>
                                                   </div>
                                               </div>

                                           </div>

                                    </div>
                                    
                              </div>
                                </div>
                            </div>
                         
                        </div>
                        <div class="card">
                          <div class="card-header" id="headingTwo">
                            <h2 class="mb-0">
                              <button class="d-flex align-items-center justify-content-between btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Cut & Style
                                <span class="fa-stack fa-sm">                                
                                    <i class="fas fa-chevron-right "></i>
                                  </span>
                              </button>
                            </h2>
                          </div>
                          <div id="collapseTwo" class="collapse faq" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body"><div class="product_in_store_border_box">
                                <div class="hair_box_tab_heading">
                                    <span class="hair_heading_box">Hair Colouring</span>
                                    <ul class="hair_tabs_in_box">
                                        <li><a href="" class="maie_tabs_box">Male</a></li>
                                        <li><a href="" class="maie_tabs_box">Female</a></li>
                                        <li><a href="" class="maie_tabs_box">Kids</a></li>

                                    </ul>
                                </div>   

                                <ul class="booking_tab_in_box">
                                     <li><a href="#" class="live_at_home_tab">Live At Home</a></li>
                                     <li><a href="#" class="live_at_home_tab">Trainer At Home</a></li>
                                     <li><a href="#" class="live_at_home_tab">Store</a></li>

                                </ul>

                                <div class="book_your_hair_treatment">
                                       <div class="hair_pera_about_box pt-3">
                                           <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy 
                                               nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. 
                                               Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper 
                                               <spane class="read_more">Read more..</spane></p>
                                      </div>
                                       <div class="row">
                                           <div class="col-md-4">
                                               <div class="title">
                                                   Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                   <span href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                   <div class="read_more uea_price">UEA 799</div>
                                                   <div class="texes_inc">Inc. of all taxes</div>
                                                   <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                   <a href="#" class="add_button_tab"> Add </a>
                                               </div>
                                           </div>
                                          
                                           <div class="col-md-4">
                                               <div class="title">
                                                   Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                   <span href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                   <div class="read_more uea_price">UEA 799</div>
                                                   <div class="texes_inc">Inc. of all taxes</div>
                                                   <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                   <div class="number">
                                                       <span class="minus"><i class="fas fa-minus"></i></span>
                                                       <input type="text" value="1" class="add_tab_flied">
                                                       <span class="plus"><i class="fas fa-plus"></i></span>
                                                   </div>
                                               </div>
                                           </div>

                                           <div class="col-md-4">
                                               <div class="title">
                                                   Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                   <span href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                   <div class="read_more uea_price">UEA 799</div>
                                                   <div class="texes_inc">Inc. of all taxes</div>
                                                   <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                   <a href="#" class="add_button_tab"> Add </a>
                                               </div>
                                           </div>

                                       </div>

                                       <div class="row pt-5">
                                           <div class="col-md-4">
                                               <div class="title">
                                                   Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                   <span href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                   <div class="read_more uea_price">UEA 799</div>
                                                   <div class="texes_inc">Inc. of all taxes</div>
                                                   <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                   <a href="#" class="add_button_tab"> Add </a>
                                               </div>
                                           </div>
                                          
                                           <div class="col-md-4">
                                               <div class="title">
                                                   Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                   <span href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                   <div class="read_more uea_price">UEA 799</div>
                                                   <div class="texes_inc">Inc. of all taxes</div>
                                                   <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                   <div class="number">
                                                       <span class="minus"><i class="fas fa-minus"></i></span>
                                                       <input type="text" value="1" class="add_tab_flied">
                                                       <span class="plus"><i class="fas fa-plus"></i></span>
                                                   </div>
                                               </div>
                                           </div>

                                           <div class="col-md-4">
                                               <div class="title">
                                                   Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                   <span href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                   <div class="read_more uea_price">UEA 799</div>
                                                   <div class="texes_inc">Inc. of all taxes</div>
                                                   <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                   <a href="#" class="add_button_tab"> Add </a>
                                               </div>
                                           </div>

                                       </div>
                                       <div class="row pt-5">
                                           <div class="col-md-4">
                                               <div class="title">
                                                   Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                   <span href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                   <div class="read_more uea_price">UEA 799</div>
                                                   <div class="texes_inc">Inc. of all taxes</div>
                                                   <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                   <a href="#" class="add_button_tab"> Add </a>
                                               </div>
                                           </div>
                                          
                                           <div class="col-md-4">
                                               <div class="title">
                                                   Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                   <span href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                   <div class="read_more uea_price">UEA 799</div>
                                                   <div class="texes_inc">Inc. of all taxes</div>
                                                   <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                   <div class="number">
                                                       <span class="minus"><i class="fas fa-minus"></i></span>
                                                       <input type="text" value="1" class="add_tab_flied">
                                                       <span class="plus"><i class="fas fa-plus"></i></span>
                                                   </div>
                                               </div>
                                           </div>

                                           <div class="col-md-4">
                                               <div class="title">
                                                   Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                   <span href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                   <div class="read_more uea_price">UEA 799</div>
                                                   <div class="texes_inc">Inc. of all taxes</div>
                                                   <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                   <a href="#" class="add_button_tab"> Add </a>
                                               </div>
                                           </div>

                                       </div>

                                </div>
                                </div>
                                
                          </div>
                            
                          </div>
                        </div>
                        <div class="card">
                          <div class="card-header" id="headingThree">
                            <h2 class="mb-0">
                              <button class="d-flex align-items-center justify-content-between btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Special
                                <span class="fa-stack fa-sm">                                
                                    <i class="fas fa-chevron-right "></i>
                                  </span>
                              </button>
                            </h2>
                          </div>
                          <div id="collapseThree" class="collapse faq" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                <div class="product_in_store_border_box">
                                    <div class="hair_box_tab_heading">
                                        <span class="hair_heading_box">Hair Colouring</span>
                                        <ul class="hair_tabs_in_box">
                                            <li><a href="" class="maie_tabs_box">Male</a></li>
                                            <li><a href="" class="maie_tabs_box">Female</a></li>
                                            <li><a href="" class="maie_tabs_box">Kids</a></li>

                                        </ul>
                                    </div>   

                                    <ul class="booking_tab_in_box">
                                         <li><a href="#" class="live_at_home_tab">Live At Home</a></li>
                                         <li><a href="#" class="live_at_home_tab">Trainer At Home</a></li>
                                         <li><a href="#" class="live_at_home_tab">Store</a></li>

                                    </ul>

                                    <div class="book_your_hair_treatment">
                                           <div class="hair_pera_about_box pt-3">
                                               <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy 
                                                   nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. 
                                                   Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper 
                                                   <spane class="read_more">Read more..</spane></p>
                                          </div>
                                           <div class="row">
                                               <div class="col-md-4">
                                                   <div class="title">
                                                       Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                       <span href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                       <div class="read_more uea_price">UEA 799</div>
                                                       <div class="texes_inc">Inc. of all taxes</div>
                                                       <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                       <a href="#" class="add_button_tab"> Add </a>
                                                   </div>
                                               </div>
                                              
                                               <div class="col-md-4">
                                                   <div class="title">
                                                       Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                       <span href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                       <div class="read_more uea_price">UEA 799</div>
                                                       <div class="texes_inc">Inc. of all taxes</div>
                                                       <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                       <div class="number">
                                                           <span class="minus"><i class="fas fa-minus"></i></span>
                                                           <input type="text" value="1" class="add_tab_flied">
                                                           <span class="plus"><i class="fas fa-plus"></i></span>
                                                       </div>
                                                   </div>
                                               </div>

                                               <div class="col-md-4">
                                                   <div class="title">
                                                       Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                       <span href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                       <div class="read_more uea_price">UEA 799</div>
                                                       <div class="texes_inc">Inc. of all taxes</div>
                                                       <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                       <a href="#" class="add_button_tab"> Add </a>
                                                   </div>
                                               </div>

                                           </div>

                                           <div class="row pt-5">
                                               <div class="col-md-4">
                                                   <div class="title">
                                                       Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                       <span href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                       <div class="read_more uea_price">UEA 799</div>
                                                       <div class="texes_inc">Inc. of all taxes</div>
                                                       <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                       <a href="#" class="add_button_tab"> Add </a>
                                                   </div>
                                               </div>
                                              
                                               <div class="col-md-4">
                                                   <div class="title">
                                                       Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                       <span href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                       <div class="read_more uea_price">UEA 799</div>
                                                       <div class="texes_inc">Inc. of all taxes</div>
                                                       <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                       <div class="number">
                                                           <span class="minus"><i class="fas fa-minus"></i></span>
                                                           <input type="text" value="1" class="add_tab_flied">
                                                           <span class="plus"><i class="fas fa-plus"></i></span>
                                                       </div>
                                                   </div>
                                               </div>

                                               <div class="col-md-4">
                                                   <div class="title">
                                                       Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                       <span href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                       <div class="read_more uea_price">UEA 799</div>
                                                       <div class="texes_inc">Inc. of all taxes</div>
                                                       <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                       <a href="#" class="add_button_tab"> Add </a>
                                                   </div>
                                               </div>

                                           </div>
                                           <div class="row pt-5">
                                               <div class="col-md-4">
                                                   <div class="title">
                                                       Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                       <span href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                       <div class="read_more uea_price">UEA 799</div>
                                                       <div class="texes_inc">Inc. of all taxes</div>
                                                       <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                       <a href="#" class="add_button_tab"> Add </a>
                                                   </div>
                                               </div>
                                              
                                               <div class="col-md-4">
                                                   <div class="title">
                                                       Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                       <span href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                       <div class="read_more uea_price">UEA 799</div>
                                                       <div class="texes_inc">Inc. of all taxes</div>
                                                       <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                       <div class="number">
                                                           <span class="minus"><i class="fas fa-minus"></i></span>
                                                           <input type="text" value="1" class="add_tab_flied">
                                                           <span class="plus"><i class="fas fa-plus"></i></span>
                                                       </div>
                                                   </div>
                                               </div>

                                               <div class="col-md-4">
                                                   <div class="title">
                                                       Small <span class="duration_time"> ( Duration Time : 1hr )</span>
                                                       <span href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore"><i class="fas fa-info-circle"></i></span>
                                                       <div class="read_more uea_price">UEA 799</div>
                                                       <div class="texes_inc">Inc. of all taxes</div>
                                                       <div class="cashback"><span class="walle_icon"><img src="images/wallet_icon.png"></span> Cashback AED 50 </div>
                                                       <a href="#" class="add_button_tab"> Add </a>
                                                   </div>
                                               </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                              </div>
                            </div>
                          
                        </div>
                      </div>                    
                    <!-- End Here Section -->

                </div>
                <!-- End Here col_md_8 -->
                <!-- Start Col-md-4 here -->
                <div class="col-lg-4 col-sm-12">
                    <div class="trail_maine_section">
                        <div class="trail_sessions">
                            <div class="slider-container">
                                <div class="slider-for">
                                    <div class="item-slick play_button_style"> <img src="images/workout_image_1.png" alt="Alt">
                                        <a class="button is-play" href="#">
                                            <div class="button-outer-circle has-scale-animation"></div>
                                            <div class="button-outer-circle has-scale-animation has-delay-short"></div>
                                            <div class="button-icon is-play">
                                                <svg height="100%" width="100%" fill="#373737">
                                                    <polygon class="triangle" points="5,0 30,15 5,30" viewBox="0 0 30 15"></polygon>
                                                    <path class="path" d="M5,0 L30,15 L5,30z" fill="none" stroke="#373737" stroke-width="1"></path>
                                                </svg>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item-slick"> <img src="images/workout_image_1.png" alt="Alt">
                                        <a class="button is-play" href="#">
                                            <div class="button-outer-circle has-scale-animation"></div>
                                            <div class="button-outer-circle has-scale-animation has-delay-short"></div>
                                            <div class="button-icon is-play">
                                                <svg height="100%" width="100%" fill="#373737">
                                                    <polygon class="triangle" points="5,0 30,15 5,30" viewBox="0 0 30 15"></polygon>
                                                    <path class="path" d="M5,0 L30,15 L5,30z" fill="none" stroke="#373737" stroke-width="1"></path>
                                                </svg>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item-slick"> <img src="images/workout_image_1.png" alt="Alt">
                                        <a class="button is-play" href="#">
                                            <div class="button-outer-circle has-scale-animation"></div>
                                            <div class="button-outer-circle has-scale-animation has-delay-short"></div>
                                            <div class="button-icon is-play">
                                                <svg height="100%" width="100%" fill="#373737">
                                                    <polygon class="triangle" points="5,0 30,15 5,30" viewBox="0 0 30 15"></polygon>
                                                    <path class="path" d="M5,0 L30,15 L5,30z" fill="none" stroke="#373737" stroke-width="1"></path>
                                                </svg>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="item-slick"> <img src="images/workout_image_1.png" alt="Alt">
                                        <a class="button is-play" href="#">
                                            <div class="button-outer-circle has-scale-animation"></div>
                                            <div class="button-outer-circle has-scale-animation has-delay-short"></div>
                                            <div class="button-icon is-play">
                                                <svg height="100%" width="100%" fill="#373737">
                                                    <polygon class="triangle" points="5,0 30,15 5,30" viewBox="0 0 30 15"></polygon>
                                                    <path class="path" d="M5,0 L30,15 L5,30z" fill="none" stroke="#373737" stroke-width="1"></path>
                                                </svg>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="slider-nav thumbnail_slick_slider_image"> 
                                    <a href="#"  data-toggle="modal" data-target="#Gallery_pop_Up"><img class="item-slick" src="images/workout_image_1_thum.png" alt="Alt"></a>
                                    

                                    <a href="#"  data-toggle="modal" data-target="#Gallery_pop_Up"><img class="item-slick" src="images/workout_image_2_thum.png" alt="Alt"></a>
                                    <a href="#"  data-toggle="modal" data-target="#Gallery_pop_Up"><img class="item-slick" src="images/workout_image_3_thum.png" alt="Alt"></a> 
                                    <a href="#"  data-toggle="modal" data-target="#Gallery_pop_Up"><img class="item-slick" src="images/workout_image_1_thum.png" alt="Alt"></a>
                                </div>
                                <div class="modal fade" id="Gallery_pop_Up" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog booking_gallery_popup" role="document">
                                      <div class="modal-content">
                                        <div class="modal-header">
                                      
                                          <button type="button" class="close photo_gallery_popup" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true"><i class="fas fa-times"></i></span>
                                          </button>
                                        </div>
                                        <div class="modal-body">
                                           <div class="row">
                                               <div class="col-md-5">
                                                <div class="product-carousel">
                                                  
                                                    <div class="product">
                                                      <div class="item">
                                                        <img class="product-image" src="images/booking_gallery_popup.png" />
                                                        
                                                      </div>
                                                    
                                                    </div>
                                                    <div class="product">
                                                      <div class="item">
                                                        <img class="product-image" src="images/booking_gallery_popup.png" />
                                                       
                                                      
                                                      </div>
                                                     
                                                    </div>
                                                    <div class="product">
                                                      <div class="item">
                                                        <img class="product-image" src="images/booking_gallery_popup.png" />
                                                        
                                                      </div>
                                                     
                                                    </div>
                                                    <div class="product">
                                                      <div class="item">
                                                        <img class="product-image" src="images/booking_gallery_popup.png" />
                                                          
                                                      </div>
                                                    
                                                    </div>
                                                      <div class="product">
                                                      <div class="item">
                                                        <img class="product-image" src="images/booking_gallery_popup.png" />
                                                    
                                                      </div>
                                                    
                                                    </div>
                                                    <div class="product">
                                                      <div class="item">
                                                        <img class="product-image" src="images/booking_gallery_popup.png" />
                                                        
                                                      </div>
                                                    
                                                    </div>
                                                    
                                                   
                                                    
                                                  </div>

                                               </div>
                                               <div class="col-md-7">
                                                   <div class="right_side_gallery_popup">
                                                        <h3 class="popup_heading_photo">Photo (34)</h3>
                                                        <p class="fliter_tab_gallery_popup">Filter by Facilites</p>
                                                        <ul class="popup_gallery_listing">
                                                            <li><a href="#" class="popup_tabs_gallery">All (34)</a></li>
                                                            <li><a href="#" class="popup_tabs_gallery">Stretching Area (7)</a></li>
                                                            <li><a href="#" class="popup_tabs_gallery">Weight Section (11)</a></li>
                                                            <li><a href="#" class="popup_tabs_gallery">Cardio Section (4)</a></li>
                                                            <li><a href="#" class="popup_tabs_gallery">Spinning Cycling Room (4)</a></li>
                                                            <li><a href="#" class="popup_tabs_gallery">Cafe / Nutrition Outlet (2)</a></li>
                                                            <li><a href="#" class="popup_tabs_gallery">Miscellaneous (3)</a></li>
                                                            <li><a href="#" class="popup_tabs_gallery">Steam / Sauna / Jacuzzi (3)</a></li>
                                                            <li><a href="#" class="popup_tabs_gallery">Changing Room (2)    </a></li>
                                                        </ul>
                                                        <p class="fliter_tab_gallery_popup pt-2 pb-3">Filter by Offering</p>

                                                        <ul class="popup_gallery_listing">
                                                            <li><a href="#" class="popup_tabs_gallery">All (34)</a></li>
                                                            <li><a href="#" class="popup_tabs_gallery">Gym Membership + Group Activites (17)</a></li>
                                                        </ul>

                                                        <div class="diver_section_bottom_line"></div>
                                                        <div class="footer_popup_gallery">
                                                            <div class="experince_text">Experience The Studio</div>
                                                            <div class="booking_popup_btn"><a href="#">Book A Session</a></div>
                                                        </div>
                                                        
                                                   </div>
                                               </div>
                                           </div>

                                        </div>
                                       
                                      </div>
                                    </div>
                                  </div>
                            </div>
                            <div class="all_sessions_btn"><a href="#">Trail Sessions</a></div>
                            <div class="free_trail_text"><a href="#">30 days free trail</a></div>
                            <div class="trai_peragraph">
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam. </p>
                            </div>
                        </div>
                    </div>
                    <div class="your_cart_new">
                        <div class="all_sessions_btn_new"><a href="#">Your Cart</a></div>
                        <div class="whole_sidebar_cart-section_bg">
                          <div class="table_section-right_sidebar">
                                <div class="membership_text">
                                   <div class="name_gym"> Gym membership   <span class="multiplue_x"> x 1</span></div>
                                </div>
                                <div class="Code_name_sidebar">
                                    <div class="price_amount_bar">AED 20</div>
                                </div>
                          </div>
                          <div class="table_section-right_sidebar">
                            <div class="membership_text">
                               <div class="name_gym"> Gym membership   <span class="multiplue_x"> x 1</span></div>
                            </div>
                            <div class="Code_name_sidebar">
                                <div class="price_amount_bar">AED 20</div>
                            </div>
                      </div>
                    </div>

                    <div class="whole_sidebar_cart-section_bg">
                        <div class="table_section-right_sidebar">
                              <div class="membership_text">
                                 <div class="name_gym"> Subtotal</div>
                              </div>
                              <div class="Code_name_sidebar">
                                  <div class="price_amount_bar">AED 20</div>
                              </div>
                        </div>
                        <div class="table_section-right_sidebar">
                          <div class="membership_text">
                             <div class="name_gym">Discount allowed</div>
                          </div>
                          <div class="Code_name_sidebar">
                              <div class="price_amount_bar">AED 20</div>
                          </div>
                    </div>
                    <div class="blue_line_devider"></div>
                    <div class="apply_coupon_code_section">
                        <div class="apply_coupon_text">
                          Apply Coupon
                        </div>
                         <div class="coupon_code_all_here">
                        <div class="coupon_code_first">
                            <div class="coupon_code_left_Side">
                             <a href="#" class="btn_coupon_code_border">BEUTICS10</a>
                             <div class="coupon_date_about">
                             <p>Lorem ipsum dolor sit amet</p>
                             <p>Expires : 31st Dec 2020  |   11:03 A M</p>
                            </div>
                            </div>
                            <div class="coupon_code_discount">
                                30%
                                <span class="off_coupon_code">Off</span>
                            </div>

                        </div>

                        <div class="coupon_code_first mt-5">
                            <div class="coupon_code_left_Side">
                             <a href="#" class="btn_coupon_code_border">BEUTICS10</a>
                             <div class="coupon_date_about">
                             <p>Lorem ipsum dolor sit amet</p>
                             <p>Expires : 31st Dec 2020  |   11:03 A M</p>
                            </div>
                            </div>
                            <div class="coupon_code_discount">
                                50%
                                <span class="off_coupon_code">Off</span>
                            </div>

                        </div>
                        
                        <div class="coupon_code_first mt-5 mb-5">
                            <div class="coupon_code_left_Side">
                             <a href="#" class="btn_coupon_code_border">BEUTICS10</a>
                             <div class="coupon_date_about">
                             <p>Lorem ipsum dolor sit amet</p>
                             <p>Expires : 31st Dec 2020  |   11:03 A M</p>
                            </div>
                            </div>
                            <div class="coupon_code_discount">
                                30%
                                <span class="off_coupon_code">Off</span>
                            </div>

                        </div>
                        <div class="coupon_code_first mt-5 mb-5">
                            <div class="coupon_code_left_Side">
                             <a href="#" class="btn_coupon_code_border">BEUTICS10</a>
                             <div class="coupon_date_about">
                             <p>Lorem ipsum dolor sit amet</p>
                             <p>Expires : 31st Dec 2020  |   11:03 A M</p>
                            </div>
                            </div>
                            <div class="coupon_code_discount">
                                30%
                                <span class="off_coupon_code">Off</span>
                            </div>

                        </div>
                        <div class="coupon_code_first mt-5 mb-5">
                            <div class="coupon_code_left_Side">
                             <a href="#" class="btn_coupon_code_border">BEUTICS10</a>
                             <div class="coupon_date_about">
                             <p>Lorem ipsum dolor sit amet</p>
                             <p>Expires : 31st Dec 2020  |   11:03 A M</p>
                            </div>
                            </div>
                            <div class="coupon_code_discount">
                                30%
                                <span class="off_coupon_code">Off</span>
                            </div>

                        </div>
                    </div>
                        <!-- All Coupon end here -->
                        <div class="coupon_input_filed">
                            <input  class="enter_code" type="text" Placeholder="Enter Coupon Code" />
                            <button class="apply_coupon_code_btn" type="button">Apply</button>
                        </div>

                    </div>
                  </div>

                  <div class="whole_sidebar_cart-section_bg">
                    <div class="table_section-right_sidebar">
                          <div class="membership_text">
                             <div class="name_gym">Total</div>
                          </div>
                          <div class="Code_name_sidebar">
                              <div class="price_amount_bar">AED 20</div>
                          </div>
                    </div>
                    <div class="blue_line_devider"></div>
                    <div class="table_section-right_sidebar pb-5">
                      <div class="membership_text">
                         <div class="name_gym save_AED"> You are saving <spane class="code_UEA799">AED 50</spane>
                            </div>
                            <div class="name_gym  save_AED"> Cashback earned  AED 30
                            </div>
                      </div>
                      <!-- <div class="Code_name_sidebar">
                          <div class="price_amount_bar">AED 20</div>
                      </div> -->
                </div>
              </div>
                        
                        <button class="apply_coupon_code_btn Checkout_btn_sidebar" type="button">Checkout</button>
               

                    </div>
                </div>
                <!-- End Here Col-md-4 -->
            </div>
        </div>
    </section>
    <!-- End Here -->
    <!-- beutics_blog_section -->
    <section class="beutics_blog_section padding_tb_60">
        <div class="container-fluid">
            <!-- blogs -->
            <div class="blog_section row">
                <div class="blog_slider_section col-md-9 offset-md-3 no_padding">
                    <div class="blog_slider slider">
                        <div class="cu_slider_item">
                            <div class="blog_wrap">
                                <div class="slider-caption-wrap">
                                    <div class="caption_title">
                                        <h2>What is Lorem Ipsum ?</h2> </div>
                                    <div class="discription"> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </div>
                                </div>
                            </div>
                        </div>
                        <div class="cu_slider_item">
                            <div class="blog_wrap">
                                <div class="slider-caption-wrap">
                                    <div class="caption_title">
                                        <h2>What is Lorem Ipsum ?</h2> </div>
                                    <div class="discription"> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </div>
                                </div>
                            </div>
                        </div>
                        <div class="cu_slider_item">
                            <div class="blog_wrap">
                                <div class="slider-caption-wrap">
                                    <div class="caption_title">
                                        <h2>What is Lorem Ipsum ?</h2> </div>
                                    <div class="discription"> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </div>
                                </div>
                            </div>
                        </div>
                        <div class="cu_slider_item">
                            <div class="blog_wrap">
                                <div class="slider-caption-wrap">
                                    <div class="caption_title">
                                        <h2>What is Lorem Ipsum ?</h2> </div>
                                    <div class="discription"> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </div>
                                </div>
                            </div>
                        </div>
                        <div class="cu_slider_item">
                            <div class="blog_wrap">
                                <div class="slider-caption-wrap">
                                    <div class="caption_title">
                                        <h2>What is Lorem Ipsum ?</h2> </div>
                                    <div class="discription"> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- beutics_blog_section End-->
@endsection