@extends('layouts.layout')
@section('title','Detail')
@section('content')
<div class="header_height height_fitness_one"></div>
<!-- Home Page Banner Slider -->

<!-- Page Banner  -->
  <div class="section-baner-full-width">
        <div class="container-fluid">
            <div class="container">
                <div class="row">
<!-- breadcrumbs_section -->
   
<!-- End breadcrumbs_section -->

                <!-- banner Left Side -->
         <div class="col-lg-4 col-sm-12 padding_remove">
            <div class="height-breadcrumbs">
                <section class="breadcrumbs_section">
                    <div id="block-breadcrumbs" class="block-breadcrumbs">
                        <nav role="navigation" aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item"><a  href="#">Client Listing</a></li>
                            </ol>
                        </nav>
                    </div>

                </section>

                    <div class="rating-banner"><span class="start-rating">
                            <i class="fas fa-star"></i></span> 
                              <span class="rating-ration">4.5 
                               <spane class="total-rating">(123)</spane>
                             </span>
                    </div>
                <h1 class="fitness-heading-banner">
                    The Fitness Culture
                </h1>

                <!-- time List in banner -->

                     <ul class="time_table_list">
                        <li>
                            <span class="clock_icon"><i class="fas fa-clock"></i></span>
                            <a href="#"> Open Now : 08.00 AM - 20.00 PM </a>
                        </li>
                        <li>
                            <span class="clock_icon"><i class="fas fa-map-marker-alt"></i>
                            </span>
                            <a href="#">Celesta Fiesta, 161, Sector 27, Gurugram, <br>
                                        Haryana 122009
                            </a>
                        </li>

                        <li>
                            <span class="clock_icon"><i class="fas fa-map-marker-alt"></i>
                            </span>
                            <span class="km_road"> 2.3 km away <a href="#" class="view-map"> View Map </a> </span>
                        </li>

                     </ul>

                <!-- End Section here -->

                <!-- button listing  -->
                   <ul class="Gender_listing">
                         <li> <a href="#" class="Male_Button"> Male </a></li>
                         <li> <a href="#" class="Male_Button"> Female </a></li>
                         <li> <a href="#" class="Male_Button"> Kid </a></li>
                   </ul>
                <!-- End here Sectionn -->


                <!-- Flow Button Banner -->
                  <ul class="Flow_social_button">
                     <li> <a href="#" class="bookmark">Bookmark</a></li>
                      <li> <a href="#" class="bookmark">Share <spane class="share_icon"><img src="images/share_icon.png"></spane> </a></li>
                      <li> <a href="#" class="bookmark">Add Review </a></li>
                  </ul>

                <!-- End Here section-->

                  <!-- Start Tag Line -->
                    <div class="Tag_line_banner_bottom">
                          <p> <span class="Trainer_tag"> Trainer Speak :</span> English , Franch , Japenes</p>
                     </div>
                  <!-- End Here section -->

             </div>
        </div>

                 <!-- End Here -->

  <!-- Right Side Banner Image -->
   <div class="col-lg-8 col-sm-12">
    <div class="Right_side_banner_image">
        <img src="images/Detail_page_about_banner.jpg" class="" />
    </div>
   </div>

  <!-- End Here Section -->





                </div>

            </div>
            
        </div>
        <!-- aminities Section banner bottom -->
                <div class="banner_bottom_aminities">
                    <div class="container">

                    <h4 class="aminities">Aminities</h4>
                     <ul class="aminities_list">
                         <li> <span> <img src="images/weather_icon.png" class="img-fluid icon_block" /></span> 
                         <a href="#" class="text-upper">Ac</a>
                         </li>

                         <li> <span> <img src="images/television_icon.png" class="img-fluid icon_block" /></span> 
                         <a href="#">Television</a>
                         </li>

                         <li> <span> <img src="images/first-aid-kit_icon.png" class="img-fluid icon_block" /></span> 
                         <a href="#">Television</a>
                         </li>

                         <li> <span> <img src="images/wifi-router_icon.png" class="img-fluid icon_block" /></span> 
                         <a href="#">Television</a>
                         </li>
                     </ul>

                  </div>
                 </div>
    <!-- End Here section -->
</div>

<!-- End Here -->
<!-- Tabs Section -->

<section class="details_about_page_body">
     <div class="container">
        <div class="row">
            <!-- left Side body content  -->
            <div class="col-md-8">
             <!-- commone tab -->
              <div class="compone_tab_top">
                <ul class="top_link_tab Product_tabs_link"> 
                    <li><a href="#" class="link_tab_one Product_tabs_one">Choices</a></li>
                    <li><a href="beauty-in-store" class="link_tab_one Product_tabs_one">At Store</a></li>
                      <li><a href="#" class="link_tab_one Product_tabs_one">At Home</a></li>
                      <li><a href="instruct-details" class="link_tab_one Product_tabs_one">Staff</a></li>
                      <li><a href="beauty-product" class="link_tab_one Product_tabs_one active">Products</a></li>
                      
                    <li><a href="review-details" class="link_tab_one Product_tabs_one">7 Reviews</a>
                      <span class="tab_rating_on_btn tab_rating_on_btn_new"> <a href=""> <i class="fas fa-star"></i> 6.5-Good </a></span>
                      </li>
                      <li><a href="about-details" class="link_tab_one Product_tabs_one">About</a></li>
                </ul>
              </div>
             <!-- End Here -->
              <!-- Start Activities_section -->
        

    <!-- About Gym Section Start -->
         <div class="About_gym_section">
            <div class="container-fluid">
               <div class="row">
                    <div class="col-md-4 col-sm">
                          <div class="product_one">
                              <img src="images/product_1.png">
                              <div class="product_heading">Neo Nude Foundation</div>
                          </div>
                     </div>
                     <div class="col-md-4 col-sm">
                        <div class="product_one">
                            <img src="images/product_two.png">
                            <div class="product_heading">Neo Nude Foundation</div>
                        </div>
                   </div>
                   <div class="col-md-4 col-sm">
                    <div class="product_one">
                        <img src="images/product_3.png">
                        <div class="product_heading">Neo Nude Foundation</div>
                    </div>
               </div>
                 </div>
                 
                 <div class="row mt-4 mb-4">
                    <div class="col-md-4 col-sm">
                         <div class="product_one">
                             <img src="images/product_4.png">
                             <div class="product_heading">Neo Nude Foundation</div>
                         </div>
                    </div>
                    <div class="col-md-4 col-sm">
                       <div class="product_one">
                           <img src="images/product_5.png">
                           <div class="product_heading">Neo Nude Foundation</div>
                       </div>
                  </div>
                  <div class="col-md-4 col-sm">
                   <div class="product_one">
                       <img src="images/product_6.png">
                       <div class="product_heading">Neo Nude Foundation</div>
                   </div>
              </div>
                </div>

                <div class="row mb-4 pd-5">
                    <div class="col-md-4 col-sm">
                         <div class="product_one">
                             <img src="images/product_7.png">
                             <div class="product_heading">Neo Nude Foundation</div>
                         </div>
                    </div>
                    <div class="col-md-4 col-sm">
                       <div class="product_one">
                           <img src="images/product_8.png">
                           <div class="product_heading">Neo Nude Foundation</div>
                       </div>
                  </div>
                  <div class="col-md-4 col-sm">
                   <div class="product_one">
                       <img src="images/product_9.png">
                       <div class="product_heading">Neo Nude Foundation</div>
                   </div>
              </div>
                </div>

                 <div class="pagination_wrapper pt-5 mt-5 mb-5 pd-5">
                    <ul class="pagination pagination-lg">
                      <!-- <li class="page-item"><a class="page-link" href="#"><i class="fas fa-long-arrow-alt-left"></i></a></li> -->
                      <li class="page-item active"><a class="page-link" href="#">1</a></li>
                      <li class="page-item"><a class="page-link" href="#">2</a></li>
                      <li class="page-item"><a class="page-link" href="#">3</a></li>
                      <li class="page-item"><a class="page-link" href="#"><i class="fas fa-long-arrow-alt-right"></i></a></li>
                    </ul>
                </div>
            </div>
         </div>
</div>

            <!-- End Here col_md_8 -->
            <!-- Start Col-md-4 here -->
             <div class="col-md-4">
                <div class="side_all_frent_page">
                <div class="trail_maine_section">
                    <div class="trail_sessions">
                        <div class="slider-container">
                            <div class="slider-for">
                                <div class="item-slick play_button_style"> <img src="images/workout_image_1.png" alt="Alt">
                                    <a class="button is-play" href="#">
                                        <div class="button-outer-circle has-scale-animation"></div>
                                        <div class="button-outer-circle has-scale-animation has-delay-short"></div>
                                        <div class="button-icon is-play">
                                            <svg height="100%" width="100%" fill="#373737">
                                                <polygon class="triangle" points="5,0 30,15 5,30" viewBox="0 0 30 15"></polygon>
                                                <path class="path" d="M5,0 L30,15 L5,30z" fill="none" stroke="#373737" stroke-width="1"></path>
                                            </svg>
                                        </div>
                                    </a>
                                </div>
                                <div class="item-slick"> <img src="images/workout_image_1.png" alt="Alt">
                                    <a class="button is-play" href="#">
                                        <div class="button-outer-circle has-scale-animation"></div>
                                        <div class="button-outer-circle has-scale-animation has-delay-short"></div>
                                        <div class="button-icon is-play">
                                            <svg height="100%" width="100%" fill="#373737">
                                                <polygon class="triangle" points="5,0 30,15 5,30" viewBox="0 0 30 15"></polygon>
                                                <path class="path" d="M5,0 L30,15 L5,30z" fill="none" stroke="#373737" stroke-width="1"></path>
                                            </svg>
                                        </div>
                                    </a>
                                </div>
                                <div class="item-slick"> <img src="images/workout_image_1.png" alt="Alt">
                                    <a class="button is-play" href="#">
                                        <div class="button-outer-circle has-scale-animation"></div>
                                        <div class="button-outer-circle has-scale-animation has-delay-short"></div>
                                        <div class="button-icon is-play">
                                            <svg height="100%" width="100%" fill="#373737">
                                                <polygon class="triangle" points="5,0 30,15 5,30" viewBox="0 0 30 15"></polygon>
                                                <path class="path" d="M5,0 L30,15 L5,30z" fill="none" stroke="#373737" stroke-width="1"></path>
                                            </svg>
                                        </div>
                                    </a>
                                </div>
                                <div class="item-slick"> <img src="images/workout_image_1.png" alt="Alt">
                                    <a class="button is-play" href="#">
                                        <div class="button-outer-circle has-scale-animation"></div>
                                        <div class="button-outer-circle has-scale-animation has-delay-short"></div>
                                        <div class="button-icon is-play">
                                            <svg height="100%" width="100%" fill="#373737">
                                                <polygon class="triangle" points="5,0 30,15 5,30" viewBox="0 0 30 15"></polygon>
                                                <path class="path" d="M5,0 L30,15 L5,30z" fill="none" stroke="#373737" stroke-width="1"></path>
                                            </svg>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="slider-nav thumbnail_slick_slider_image"> 
                                <a href="#"  data-toggle="modal" data-target="#Gallery_pop_Up"><img class="item-slick" src="images/workout_image_1_thum.png" alt="Alt"></a>
                                

                                <a href="#"  data-toggle="modal" data-target="#Gallery_pop_Up"><img class="item-slick" src="images/workout_image_2_thum.png" alt="Alt"></a>
                                <a href="#"  data-toggle="modal" data-target="#Gallery_pop_Up"><img class="item-slick" src="images/workout_image_3_thum.png" alt="Alt"></a> 
                                <a href="#"  data-toggle="modal" data-target="#Gallery_pop_Up"><img class="item-slick" src="images/workout_image_1_thum.png" alt="Alt"></a>
                            </div>
                            <div class="modal fade" id="Gallery_pop_Up" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog booking_gallery_popup" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                  
                                      <button type="button" class="close photo_gallery_popup" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                       <div class="row">
                                           <div class="col-md-5">
                                            <div class="product-carousel">
                                              
                                                <div class="product">
                                                  <div class="item">
                                                    <img class="product-image" src="images/booking_gallery_popup.png" />
                                                    
                                                  </div>
                                                
                                                </div>
                                                <div class="product">
                                                  <div class="item">
                                                    <img class="product-image" src="images/booking_gallery_popup.png" />
                                                   
                                                  
                                                  </div>
                                                 
                                                </div>
                                                <div class="product">
                                                  <div class="item">
                                                    <img class="product-image" src="images/booking_gallery_popup.png" />
                                                    
                                                  </div>
                                                 
                                                </div>
                                                <div class="product">
                                                  <div class="item">
                                                    <img class="product-image" src="images/booking_gallery_popup.png" />
                                                      
                                                  </div>
                                                
                                                </div>
                                                  <div class="product">
                                                  <div class="item">
                                                    <img class="product-image" src="images/booking_gallery_popup.png" />
                                                
                                                  </div>
                                                
                                                </div>
                                                <div class="product">
                                                  <div class="item">
                                                    <img class="product-image" src="images/booking_gallery_popup.png" />
                                                    
                                                  </div>
                                                
                                                </div>
                                                
                                               
                                                
                                              </div>

                                           </div>
                                           <div class="col-md-7">
                                               <div class="right_side_gallery_popup">
                                                    <h3 class="popup_heading_photo">Photo (34)</h3>
                                                    <p class="fliter_tab_gallery_popup">Filter by Facilites</p>
                                                    <ul class="popup_gallery_listing">
                                                        <li><a href="#" class="popup_tabs_gallery">All (34)</a></li>
                                                        <li><a href="#" class="popup_tabs_gallery">Stretching Area (7)</a></li>
                                                        <li><a href="#" class="popup_tabs_gallery">Weight Section (11)</a></li>
                                                        <li><a href="#" class="popup_tabs_gallery">Cardio Section (4)</a></li>
                                                        <li><a href="#" class="popup_tabs_gallery">Spinning Cycling Room (4)</a></li>
                                                        <li><a href="#" class="popup_tabs_gallery">Cafe / Nutrition Outlet (2)</a></li>
                                                        <li><a href="#" class="popup_tabs_gallery">Miscellaneous (3)</a></li>
                                                        <li><a href="#" class="popup_tabs_gallery">Steam / Sauna / Jacuzzi (3)</a></li>
                                                        <li><a href="#" class="popup_tabs_gallery">Changing Room (2)    </a></li>
                                                    </ul>
                                                    <p class="fliter_tab_gallery_popup pt-2 pb-3">Filter by Offering</p>

                                                    <ul class="popup_gallery_listing">
                                                        <li><a href="#" class="popup_tabs_gallery">All (34)</a></li>
                                                        <li><a href="#" class="popup_tabs_gallery">Gym Membership + Group Activites (17)</a></li>
                                                    </ul>

                                                    <div class="diver_section_bottom_line"></div>
                                                    <div class="footer_popup_gallery">
                                                        <div class="experince_text">Experience The Studio</div>
                                                        <div class="booking_popup_btn"><a href="#">Book A Session</a></div>
                                                    </div>
                                                    
                                               </div>
                                           </div>
                                       </div>

                                    </div>
                                   
                                  </div>
                                </div>
                              </div>
                        </div>
                        <div class="all_sessions_btn"><a href="#">Trail Sessions</a></div>
                        <div class="free_trail_text"><a href="#">30 days free trail</a></div>
                        <div class="trai_peragraph">
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam. </p>
                        </div>
                    </div>
                </div>
                <div class="your_cart_new">
                    <div class="all_sessions_btn_new"><a href="#">Your Cart</a></div>
                    <div class="whole_sidebar_cart-section_bg">
                      <div class="table_section-right_sidebar">
                            <div class="membership_text">
                               <div class="name_gym"> Gym membership   <span class="multiplue_x"> x 1</span></div>
                            </div>
                            <div class="Code_name_sidebar">
                                <div class="price_amount_bar">AED 20</div>
                            </div>
                      </div>
                      <div class="table_section-right_sidebar">
                        <div class="membership_text">
                           <div class="name_gym"> Gym membership   <span class="multiplue_x"> x 1</span></div>
                        </div>
                        <div class="Code_name_sidebar">
                            <div class="price_amount_bar">AED 20</div>
                        </div>
                  </div>
                </div>

                <div class="whole_sidebar_cart-section_bg">
                    <div class="table_section-right_sidebar">
                          <div class="membership_text">
                             <div class="name_gym"> Subtotal</div>
                          </div>
                          <div class="Code_name_sidebar">
                              <div class="price_amount_bar">AED 20</div>
                          </div>
                    </div>
                    <div class="table_section-right_sidebar">
                      <div class="membership_text">
                         <div class="name_gym">Discount allowed</div>
                      </div>
                      <div class="Code_name_sidebar">
                          <div class="price_amount_bar">AED 20</div>
                      </div>
                </div>
                <div class="blue_line_devider"></div>
                <div class="apply_coupon_code_section">
                    <div class="apply_coupon_text">
                      Apply Coupon
                    </div>
                     <div class="coupon_code_all_here">
                    <div class="coupon_code_first">
                        <div class="coupon_code_left_Side">
                         <a href="#" class="btn_coupon_code_border">BEUTICS10</a>
                         <div class="coupon_date_about">
                         <p>Lorem ipsum dolor sit amet</p>
                         <p>Expires : 31st Dec 2020  |   11:03 A M</p>
                        </div>
                        </div>
                        <div class="coupon_code_discount">
                            30%
                            <span class="off_coupon_code">Off</span>
                        </div>

                    </div>

                    <div class="coupon_code_first mt-5">
                        <div class="coupon_code_left_Side">
                         <a href="#" class="btn_coupon_code_border">BEUTICS10</a>
                         <div class="coupon_date_about">
                         <p>Lorem ipsum dolor sit amet</p>
                         <p>Expires : 31st Dec 2020  |   11:03 A M</p>
                        </div>
                        </div>
                        <div class="coupon_code_discount">
                            50%
                            <span class="off_coupon_code">Off</span>
                        </div>

                    </div>
                    
                    <div class="coupon_code_first mt-5 mb-5">
                        <div class="coupon_code_left_Side">
                         <a href="#" class="btn_coupon_code_border">BEUTICS10</a>
                         <div class="coupon_date_about">
                         <p>Lorem ipsum dolor sit amet</p>
                         <p>Expires : 31st Dec 2020  |   11:03 A M</p>
                        </div>
                        </div>
                        <div class="coupon_code_discount">
                            30%
                            <span class="off_coupon_code">Off</span>
                        </div>

                    </div>
                    <div class="coupon_code_first mt-5 mb-5">
                        <div class="coupon_code_left_Side">
                         <a href="#" class="btn_coupon_code_border">BEUTICS10</a>
                         <div class="coupon_date_about">
                         <p>Lorem ipsum dolor sit amet</p>
                         <p>Expires : 31st Dec 2020  |   11:03 A M</p>
                        </div>
                        </div>
                        <div class="coupon_code_discount">
                            30%
                            <span class="off_coupon_code">Off</span>
                        </div>

                    </div>
                    <div class="coupon_code_first mt-5 mb-5">
                        <div class="coupon_code_left_Side">
                         <a href="#" class="btn_coupon_code_border">BEUTICS10</a>
                         <div class="coupon_date_about">
                         <p>Lorem ipsum dolor sit amet</p>
                         <p>Expires : 31st Dec 2020  |   11:03 A M</p>
                        </div>
                        </div>
                        <div class="coupon_code_discount">
                            30%
                            <span class="off_coupon_code">Off</span>
                        </div>

                    </div>
                </div>
                    <!-- All Coupon end here -->
                    <div class="coupon_input_filed">
                        <input  class="enter_code" type="text" Placeholder="Enter Coupon Code" />
                        <button class="apply_coupon_code_btn" type="button">Apply</button>
                    </div>

                </div>
              </div>

              <div class="whole_sidebar_cart-section_bg">
                <div class="table_section-right_sidebar">
                      <div class="membership_text">
                         <div class="name_gym">Total</div>
                      </div>
                      <div class="Code_name_sidebar">
                          <div class="price_amount_bar">AED 20</div>
                      </div>
                </div>
                <div class="blue_line_devider"></div>
                <div class="table_section-right_sidebar pb-5">
                  <div class="membership_text">
                     <div class="name_gym save_AED"> You are saving <spane class="code_UEA799">AED 50</spane>
                        </div>
                        <div class="name_gym  save_AED"> Cashback earned  AED 30
                        </div>
                  </div>
                  <!-- <div class="Code_name_sidebar">
                      <div class="price_amount_bar">AED 20</div>
                  </div> -->
            </div>
          </div>
                    
                    <button class="apply_coupon_code_btn Checkout_btn_sidebar" type="button">Checkout</button>
           

                </div>
                </div>
            </div>
 <!-- End Here Col-md-4 -->

     </div>
 </div>
</section>

<!-- End Here -->


<!-- beutics_blog_section -->
<section class="beutics_blog_section padding_tb_60">
    <div class="container-fluid">
        <!-- blogs -->
        <div class="blog_section row">
            <div class="blog_slider_section col-md-9 offset-md-3 no_padding">
                <div class="blog_slider slider">
                    <div class="cu_slider_item">
                        <div class="blog_wrap">
                          <div class="slider-caption-wrap">
                            <div class="caption_title">
                                <h2>What is Lorem Ipsum ?</h2>
                              </div>
                            <div class="discription">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </div>
                              
                          </div>
                        </div>
                    </div>
                    <div class="cu_slider_item">
                        <div class="blog_wrap">
                          <div class="slider-caption-wrap">
                            <div class="caption_title">
                                <h2>What is Lorem Ipsum ?</h2>
                              </div>
                            <div class="discription">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </div>
                              
                          </div>
                        </div>
                    </div>
                    <div class="cu_slider_item">
                        <div class="blog_wrap">
                          <div class="slider-caption-wrap">
                            <div class="caption_title">
                                <h2>What is Lorem Ipsum ?</h2>
                              </div>
                            <div class="discription">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </div>
                              
                          </div>
                        </div>
                    </div>
                    <div class="cu_slider_item">
                        <div class="blog_wrap">
                          <div class="slider-caption-wrap">
                            <div class="caption_title">
                                <h2>What is Lorem Ipsum ?</h2>
                              </div>
                            <div class="discription">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </div>
                              
                          </div>
                        </div>
                    </div>
                    <div class="cu_slider_item">
                        <div class="blog_wrap">
                          <div class="slider-caption-wrap">
                            <div class="caption_title">
                                <h2>What is Lorem Ipsum ?</h2>
                              </div>
                            <div class="discription">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </div>
                              
                          </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
<!-- beutics_blog_section End-->
@endsection