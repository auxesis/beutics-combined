@extends('layouts.layout')
@section('title','Detail')
@section('content')
<div class="header_height height_fitness_one"></div>
<!-- Home Page Banner Slider -->
<!-- Page Banner  -->
<div class="section-baner-full-width">
   <div class="container-fluid">
      <div class="container">
         <div class="row">
            <!-- breadcrumbs_section -->
            <!-- End breadcrumbs_section -->
            <!-- banner Left Side -->
            <div class="col-lg-4 col-sm-12 padding_remove">
               <div class="height-breadcrumbs">
                  <section class="breadcrumbs_section">
                     <div id="block-breadcrumbs" class="block-breadcrumbs">
                        <nav role="navigation" aria-label="breadcrumb">
                           <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="#">Home</a></li>
                              <li class="breadcrumb-item"><a  href="#">Client Listing</a></li>
                           </ol>
                        </nav>
                     </div>
                  </section>
                  <div class="rating-banner">
                     <span class="start-rating">
                     <i class="fas fa-star"></i></span> 
                     <span class="rating-ration">
                        4.5 
                        <spane class="total-rating">(123)</spane>
                     </span>
                  </div>
                  <h1 class="fitness-heading-banner">
                     The Fitness Culture
                  </h1>
                  <!-- time List in banner -->
                  <ul class="time_table_list">
                     <li>
                        <span class="clock_icon"><i class="fas fa-clock"></i></span>
                        <a href="#"> Open Now : 08.00 AM - 20.00 PM </a>
                     </li>
                     <li>
                        <span class="clock_icon"><i class="fas fa-map-marker-alt"></i>
                        </span>
                        <a href="#">Celesta Fiesta, 161, Sector 27, Gurugram, <br>
                        Haryana 122009
                        </a>
                     </li>
                     <li>
                        <span class="clock_icon"><i class="fas fa-map-marker-alt"></i>
                        </span>
                        <span class="km_road"> 2.3 km away <a href="#" class="view-map"> View Map </a> </span>
                     </li>
                  </ul>
                  <!-- End Section here -->
                  <!-- button listing  -->
                  <ul class="Gender_listing">
                     <li> <a href="#" class="Male_Button"> Male </a></li>
                     <li> <a href="#" class="Male_Button"> Female </a></li>
                     <li> <a href="#" class="Male_Button"> Kid </a></li>
                  </ul>
                  <!-- End here Sectionn -->
                  <!-- Flow Button Banner -->
                  <ul class="Flow_social_button">
                     <li> <a href="#" class="bookmark">Bookmark</a></li>
                     <li>
                        <a href="#" class="bookmark">
                           Share 
                           <spane class="share_icon"><img src="images/share_icon.png"></spane>
                        </a>
                     </li>
                     <li> <a href="#" class="bookmark">Add Review </a></li>
                  </ul>
                  <!-- End Here section-->
                  <!-- Start Tag Line -->
                  <div class="Tag_line_banner_bottom">
                     <p> <span class="Trainer_tag"> Trainer Speak :</span> English , Franch , Japenes</p>
                  </div>
                  <!-- End Here section -->
               </div>
            </div>
            <!-- End Here -->
            <!-- Right Side Banner Image -->
            <div class="col-lg-8 col-sm-12">
               <div class="Right_side_banner_image">
                  <img src="images/Detail_page_about_banner.jpg" class="" />
               </div>
            </div>
            <!-- End Here Section -->
         </div>
      </div>
   </div>
   <!-- aminities Section banner bottom -->
   <div class="banner_bottom_aminities">
      <div class="container no_padding">
         <h4 class="aminities">Aminities</h4>
         <ul class="aminities_list">
            <li> <span> <img src="images/weather_icon.png" class="img-fluid icon_block" /></span> 
               <a href="#" class="text-upper">Ac</a>
            </li>
            <li> <span> <img src="images/television_icon.png" class="img-fluid icon_block" /></span> 
               <a href="#">Television</a>
            </li>
            <li> <span> <img src="images/first-aid-kit_icon.png" class="img-fluid icon_block" /></span> 
               <a href="#">Television</a>
            </li>
            <li> <span> <img src="images/wifi-router_icon.png" class="img-fluid icon_block" /></span> 
               <a href="#">Television</a>
            </li>
         </ul>
      </div>
   </div>
   <!-- End Here section -->
</div>
<!-- End Here -->
<!-- Tabs Section -->
<section class="details_about_page_body">
   <div class="container">
      <div class="row">
         <!-- left Side body content  -->
         <div class="col-lg-8 col-sm-12">
            <!-- commone tab -->
            <div class="compone_tab_top">
               <ul class="top_link_tab">
               <li><a href="beauty-details" class="link_tab_one">Activities</a></li>
                  <li><a href="instruct-details" class="link_tab_one">Instructor</a></li>
                  <li><a href="about-details" class="link_tab_one">About Us</a></li>
                  <li><a href="review-details" class="link_tab_one active">7 Reviews</a>
                     <span class="tab_rating_on_btn"> <a href=""> <i class="fas fa-star"></i> 6.5-Good </a></span>
                  </li>
               </ul>
            </div>
            <div class="Activities_heading">
               <h2>Add Your Review </h2>
               <p class="tap_experience">Tap to rate your experience</p>
               <div class="star_review_bottom_section padding-left_remove">
                  <span class="one"><i class="fas fa-star"></i></span>
                  <span class="one"><i class="fas fa-star"></i></span>
                  <span class="one"><i class="fas fa-star"></i></span>
                  <span class="one"><i class="fas fa-star"></i></span>
                  <span class="last_star"><i class="fas fa-star"></i></span>
               </div>
               <div class="write_review_bottom ">
                  <p class="padding-left_remove">Write your review</p>
               </div>
               <div class="diver_section_bottom_line"></div>
               <p class="tap_experience">Review Statics</p>
               <div class="rating_bar row">
                  <div class="col-md-2">
                     <div class="rating_big_section">
                        <h1>4.5 <span class="Star_big_section"><i class="fas fa-star"></i></span></h1>
                        <p class="all_review">25 reviews</p>
                        <div class="good_btn">
                           Good
                        </div>
                     </div>
                  </div>
                  <div class="col-md-1"></div>
                  <div class="col-md-4">
                     <div class="bar_one_middle">
                        <span class="value_money">Value of money</span> <span class="total_number_bar">(8/10)</span>
                        <div class="progress" style="height:6px; width:100%">
                           <div class="progress-bar" style="width:70%;height:20px"></div>
                        </div>
                     </div>
                     <div class="bar_one_middle">
                        <span class="value_money">Cleanliness</span> <span class="total_number_bar">(8/10)</span>
                        <div class="progress" style="height:6px; width:100%">
                           <div class="progress-bar" style="width:70%;height:20px"></div>
                        </div>
                     </div>
                     <div class="bar_one_middle">
                        <span class="value_money">Core & Attention</span> <span class="total_number_bar">(8/10)</span>
                        <div class="progress" style="height:6px; width:100%">
                           <div class="progress-bar" style="width:70%;height:20px"></div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-1"></div>
                  <div class="col-md-4">
                     <div class="bar_one_middle">
                        <span class="value_money">Staff Expertise</span> <span class="total_number_bar">(8/10)</span>
                        <div class="progress" style="height:6px; width:100%">
                           <div class="progress-bar" style="width:60%;height:20px"></div>
                        </div>
                     </div>
                     <div class="bar_one_middle">
                        <span class="value_money">Location</span> <span class="total_number_bar">(8/10)</span>
                        <div class="progress" style="height:6px; width:100%">
                           <div class="progress-bar" style="width:60%;height:20px"></div>
                        </div>
                     </div>
                     <div class="bar_one_middle">
                        <span class="value_money">Facilites / Activites</span> <span class="total_number_bar">(8/10)</span>
                        <div class="progress" style="height:6px; width:100%">
                           <div class="progress-bar" style="width:60%;height:20px"></div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="Activities_heading mt-5">
                  <h2>Review </h2>
               </div>
               <div class="row pt-4 pb-5">
                  <div class="col-md-2">
                     <div class="img_review_left">
                        <img src="images/review_one.png">
                     </div>
                  </div>
                  <div class="col-md-10">
                     <div class="review_text_right_main">
                        <span class="yoga_tag_testimonial">Yoga</span>
                        <h4>Angela Doe 
                           <span class="start_into_review_Section">
                           <span class="one"><i class="fas fa-star"></i></span>
                           <span class="one"><i class="fas fa-star"></i></span>
                           <span class="one"><i class="fas fa-star"></i></span>
                           <span class="one"><i class="fas fa-star"></i></span>
                           <span class="last_star"><i class="fas fa-star"></i></span>
                           </span>
                        </h4>
                        <p class="review_date_maine">November 16,2015, at 9:00 pm</p>
                        <p class="review_main_text_bottom">Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</p>
                     </div>
                  </div>
               </div>
               <div class="row pt-4 pb-5">
                  <div class="col-md-2">
                     <div class="img_review_left">
                        <img src="images/review_two.png">
                     </div>
                  </div>
                  <div class="col-md-10">
                     <div class="review_text_right_main">
                        <span class="yoga_tag_testimonial">Yoga</span> <span class="yoga_tag_testimonial">Gym</span>
                        <h4>Jordi Prims 
                           <span class="start_into_review_Section">
                           <span class="one"><i class="fas fa-star"></i></span>
                           <span class="one"><i class="fas fa-star"></i></span>
                           <span class="one"><i class="fas fa-star"></i></span>
                           <span class="one"><i class="fas fa-star"></i></span>
                           <span class="last_star"><i class="fas fa-star"></i></span>
                           </span>
                        </h4>
                        <p class="review_date_maine">November 16,2015, at 9:00 pm</p>
                        <p class="review_main_text_bottom">Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</p>
                     </div>
                  </div>
               </div>
               <div class="row pt-4 pb-5">
                  <div class="col-md-2">
                     <div class="img_review_left">
                        <img src="images/review_three.png">
                     </div>
                  </div>
                  <div class="col-md-10">
                     <div class="review_text_right_main">
                        <span class="yoga_tag_testimonial">Yoga</span>
                        <h4>Jack Doe
                           <span class="start_into_review_Section">
                           <span class="one"><i class="fas fa-star"></i></span>
                           <span class="one"><i class="fas fa-star"></i></span>
                           <span class="one"><i class="fas fa-star"></i></span>
                           <span class="one"><i class="fas fa-star"></i></span>
                           <span class="last_star"><i class="fas fa-star"></i></span>
                           </span>
                        </h4>
                        <p class="review_date_maine">November 16,2015, at 9:00 pm</p>
                        <p class="review_main_text_bottom">Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</p>
                     </div>
                  </div>
               </div>
               <div class="row pt-4 pb-5">
                  <div class="col-md-2">
                     <div class="img_review_left">
                        <img src="images/review_four.png">
                     </div>
                  </div>
                  <div class="col-md-10">
                     <div class="review_text_right_main">
                        <span class="yoga_tag_testimonial">Yoga</span>     <span class="yoga_tag_testimonial">Gym</span>
                        <h4>Nick Prims
                           <span class="start_into_review_Section">
                           <span class="one"><i class="fas fa-star"></i></span>
                           <span class="one"><i class="fas fa-star"></i></span>
                           <span class="one"><i class="fas fa-star"></i></span>
                           <span class="one"><i class="fas fa-star"></i></span>
                           <span class="last_star"><i class="fas fa-star"></i></span>
                           </span>
                        </h4>
                        <p class="review_date_maine">November 16,2015, at 9:00 pm</p>
                        <p class="review_main_text_bottom">Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</p>
                     </div>
                  </div>
               </div>
               <div class="row pt-4 pb-5">
                  <div class="col-md-2">
                     <div class="img_review_left">
                        <img src="images/review_one.png">
                     </div>
                  </div>
                  <div class="col-md-10">
                     <div class="review_text_right_main">
                        <span class="yoga_tag_testimonial">Yoga</span>
                        <h4>Angela Doe 
                           <span class="start_into_review_Section">
                           <span class="one"><i class="fas fa-star"></i></span>
                           <span class="one"><i class="fas fa-star"></i></span>
                           <span class="one"><i class="fas fa-star"></i></span>
                           <span class="one"><i class="fas fa-star"></i></span>
                           <span class="last_star"><i class="fas fa-star"></i></span>
                           </span>
                        </h4>
                        <p class="review_date_maine">November 16,2015, at 9:00 pm</p>
                        <p class="review_main_text_bottom">Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</p>
                     </div>
                  </div>
               </div>
               <div class="row pt-4 pb-5">
                  <div class="col-md-2">
                     <div class="img_review_left">
                        <img src="images/review_two.png">
                     </div>
                  </div>
                  <div class="col-md-10">
                     <div class="review_text_right_main">
                        <span class="yoga_tag_testimonial">Yoga</span> <span class="yoga_tag_testimonial">Gym</span>
                        <h4>Jordi Prims 
                           <span class="start_into_review_Section">
                           <span class="one"><i class="fas fa-star"></i></span>
                           <span class="one"><i class="fas fa-star"></i></span>
                           <span class="one"><i class="fas fa-star"></i></span>
                           <span class="one"><i class="fas fa-star"></i></span>
                           <span class="last_star"><i class="fas fa-star"></i></span>
                           </span>
                        </h4>
                        <p class="review_date_maine">November 16,2015, at 9:00 pm</p>
                        <p class="review_main_text_bottom">Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</p>
                     </div>
                  </div>
               </div>
               <div class="row pt-4 pb-5">
                  <div class="col-md-2">
                     <div class="img_review_left">
                        <img src="images/review_one.png">
                     </div>
                  </div>
                  <div class="col-md-10">
                     <div class="review_text_right_main">
                        <span class="yoga_tag_testimonial">Yoga</span>
                        <h4>Angela Doe 
                           <span class="start_into_review_Section">
                           <span class="one"><i class="fas fa-star"></i></span>
                           <span class="one"><i class="fas fa-star"></i></span>
                           <span class="one"><i class="fas fa-star"></i></span>
                           <span class="one"><i class="fas fa-star"></i></span>
                           <span class="last_star"><i class="fas fa-star"></i></span>
                           </span>
                        </h4>
                        <p class="review_date_maine">November 16,2015, at 9:00 pm</p>
                        <p class="review_main_text_bottom">Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</p>
                     </div>
                  </div>
               </div>
               <div class="row pt-4 pb-5">
                  <div class="col-md-2">
                     <div class="img_review_left">
                        <img src="images/review_two.png">
                     </div>
                  </div>
                  <div class="col-md-10">
                     <div class="review_text_right_main">
                        <span class="yoga_tag_testimonial">Yoga</span> <span class="yoga_tag_testimonial">Gym</span>
                        <h4>Jordi Prims 
                           <span class="start_into_review_Section">
                           <span class="one"><i class="fas fa-star"></i></span>
                           <span class="one"><i class="fas fa-star"></i></span>
                           <span class="one"><i class="fas fa-star"></i></span>
                           <span class="one"><i class="fas fa-star"></i></span>
                           <span class="last_star"><i class="fas fa-star"></i></span>
                           </span>
                        </h4>
                        <p class="review_date_maine">November 16,2015, at 9:00 pm</p>
                        <p class="review_main_text_bottom">Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum.</p>
                     </div>
                  </div>
               </div>
            </div>
            <!-- End Here -->
            <!-- Review Submit Here Section start  -->
            <!-- End Here Section Review  -->
         </div>
         <!-- End Here col_md_8 -->
         <!-- Start Col-md-4 here -->
         <div class="col-lg-4 col-sm-12">
            <div class="trail_maine_section">
               <div class="trail_sessions">
                  <div class="slider-container">
                     <div class="slider-for">
                        <div class="item-slick play_button_style">
                           <img src="images/workout_image_1.png" alt="Alt">
                           <a class="button is-play" href="#">
                              <div class="button-outer-circle has-scale-animation"></div>
                              <div class="button-outer-circle has-scale-animation has-delay-short"></div>
                              <div class="button-icon is-play">
                                 <svg height="100%" width="100%" fill="#373737">
                                    <polygon class="triangle" points="5,0 30,15 5,30" viewBox="0 0 30 15"></polygon>
                                    <path class="path" d="M5,0 L30,15 L5,30z" fill="none" stroke="#373737" stroke-width="1"></path>
                                 </svg>
                              </div>
                           </a>
                        </div>
                        <div class="item-slick">
                           <img src="images/workout_image_1.png" alt="Alt">
                           <a class="button is-play" href="#">
                              <div class="button-outer-circle has-scale-animation"></div>
                              <div class="button-outer-circle has-scale-animation has-delay-short"></div>
                              <div class="button-icon is-play">
                                 <svg height="100%" width="100%" fill="#373737">
                                    <polygon class="triangle" points="5,0 30,15 5,30" viewBox="0 0 30 15"></polygon>
                                    <path class="path" d="M5,0 L30,15 L5,30z" fill="none" stroke="#373737" stroke-width="1"></path>
                                 </svg>
                              </div>
                           </a>
                        </div>
                        <div class="item-slick">
                           <img src="images/workout_image_1.png" alt="Alt">
                           <a class="button is-play" href="#">
                              <div class="button-outer-circle has-scale-animation"></div>
                              <div class="button-outer-circle has-scale-animation has-delay-short"></div>
                              <div class="button-icon is-play">
                                 <svg height="100%" width="100%" fill="#373737">
                                    <polygon class="triangle" points="5,0 30,15 5,30" viewBox="0 0 30 15"></polygon>
                                    <path class="path" d="M5,0 L30,15 L5,30z" fill="none" stroke="#373737" stroke-width="1"></path>
                                 </svg>
                              </div>
                           </a>
                        </div>
                        <div class="item-slick">
                           <img src="images/workout_image_1.png" alt="Alt">
                           <a class="button is-play" href="#">
                              <div class="button-outer-circle has-scale-animation"></div>
                              <div class="button-outer-circle has-scale-animation has-delay-short"></div>
                              <div class="button-icon is-play">
                                 <svg height="100%" width="100%" fill="#373737">
                                    <polygon class="triangle" points="5,0 30,15 5,30" viewBox="0 0 30 15"></polygon>
                                    <path class="path" d="M5,0 L30,15 L5,30z" fill="none" stroke="#373737" stroke-width="1"></path>
                                 </svg>
                              </div>
                           </a>
                        </div>
                     </div>
                     <div class="slider-nav thumbnail_slick_slider_image"> 
                        <a href="#"  data-toggle="modal" data-target="#Gallery_pop_Up"><img class="item-slick" src="images/workout_image_1_thum.png" alt="Alt"></a>
                        <a href="#"  data-toggle="modal" data-target="#Gallery_pop_Up"><img class="item-slick" src="images/workout_image_2_thum.png" alt="Alt"></a>
                        <a href="#"  data-toggle="modal" data-target="#Gallery_pop_Up"><img class="item-slick" src="images/workout_image_3_thum.png" alt="Alt"></a> 
                        <a href="#"  data-toggle="modal" data-target="#Gallery_pop_Up"><img class="item-slick" src="images/workout_image_1_thum.png" alt="Alt"></a>
                     </div>
                     <div class="modal fade" id="Gallery_pop_Up" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog booking_gallery_popup" role="document">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <button type="button" class="close photo_gallery_popup" data-dismiss="modal" aria-label="Close">
                                 <span aria-hidden="true"><i class="fas fa-times"></i></span>
                                 </button>
                              </div>
                              <div class="modal-body">
                                 <div class="row">
                                    <div class="col-md-5">
                                       <div class="product-carousel">
                                          <div class="product">
                                             <div class="item">
                                                <img class="product-image" src="images/booking_gallery_popup.png" />
                                             </div>
                                          </div>
                                          <div class="product">
                                             <div class="item">
                                                <img class="product-image" src="images/booking_gallery_popup.png" />
                                             </div>
                                          </div>
                                          <div class="product">
                                             <div class="item">
                                                <img class="product-image" src="images/booking_gallery_popup.png" />
                                             </div>
                                          </div>
                                          <div class="product">
                                             <div class="item">
                                                <img class="product-image" src="images/booking_gallery_popup.png" />
                                             </div>
                                          </div>
                                          <div class="product">
                                             <div class="item">
                                                <img class="product-image" src="images/booking_gallery_popup.png" />
                                             </div>
                                          </div>
                                          <div class="product">
                                             <div class="item">
                                                <img class="product-image" src="images/booking_gallery_popup.png" />
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-md-7">
                                       <div class="right_side_gallery_popup">
                                          <h3 class="popup_heading_photo">Photo (34)</h3>
                                          <p class="fliter_tab_gallery_popup">Filter by Facilites</p>
                                          <ul class="popup_gallery_listing">
                                             <li><a href="#" class="popup_tabs_gallery">All (34)</a></li>
                                             <li><a href="#" class="popup_tabs_gallery">Stretching Area (7)</a></li>
                                             <li><a href="#" class="popup_tabs_gallery">Weight Section (11)</a></li>
                                             <li><a href="#" class="popup_tabs_gallery">Cardio Section (4)</a></li>
                                             <li><a href="#" class="popup_tabs_gallery">Spinning Cycling Room (4)</a></li>
                                             <li><a href="#" class="popup_tabs_gallery">Cafe / Nutrition Outlet (2)</a></li>
                                             <li><a href="#" class="popup_tabs_gallery">Miscellaneous (3)</a></li>
                                             <li><a href="#" class="popup_tabs_gallery">Steam / Sauna / Jacuzzi (3)</a></li>
                                             <li><a href="#" class="popup_tabs_gallery">Changing Room (2)	</a></li>
                                          </ul>
                                          <p class="fliter_tab_gallery_popup pt-2 pb-3">Filter by Offering</p>
                                          <ul class="popup_gallery_listing">
                                             <li><a href="#" class="popup_tabs_gallery">All (34)</a></li>
                                             <li><a href="#" class="popup_tabs_gallery">Gym Membership + Group Activites (17)</a></li>
                                          </ul>
                                          <div class="diver_section_bottom_line"></div>
                                          <div class="footer_popup_gallery">
                                             <div class="experince_text">Experience The Studio</div>
                                             <div class="booking_popup_btn"><a href="#">Book A Session</a></div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="all_sessions_btn"><a href="#">Trail Sessions</a></div>
                  <div class="free_trail_text"><a href="#">30 days free trail</a></div>
                  <div class="trai_peragraph">
                     <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam. </p>
                  </div>
               </div>
            </div>
            <div class="Voice_review_sidebar">
               <div class="all_sessions_btn"><a href="#">Voice of Reviews</a></div>
               <div class="free_trail_text"><a href="#">Top 10 services recommended by reviews
                  </a>
               </div>
               <div class="servie_voice_review">
                  <p class="servie-1_lebal">Service 1</p>
                  <span> <img src="images/like_thumb.png" /> <span class="review_one_thum">(1 Reviewers) </span></span>
                  <span class="thum_down_voice_review"> <img src="images/thum_down.png" /> <span class="review_one_thum">(1 Reviewers) </span></span>
               </div>
               <div class="servie_voice_review">
                  <p class="servie-1_lebal">Service 2</p>
                  <span> <img src="images/like_thumb.png" /> <span class="review_one_thum">(1 Reviewers) </span></span>
                  <span class="thum_down_voice_review"> <img src="images/thum_down.png" /> <span class="review_one_thum">(1 Reviewers) </span></span>
               </div>
               <div class="servie_voice_review">
                  <p class="servie-1_lebal">Service 1</p>
                  <span> <img src="images/like_thumb.png" /> <span class="review_one_thum">(1 Reviewers) </span></span>
                  <span class="thum_down_voice_review"> <img src="images/thum_down.png" /> <span class="review_one_thum">(1 Reviewers) </span></span>
               </div>
               <div class="servie_voice_review">
                  <p class="servie-1_lebal">Service 2</p>
                  <span> <img src="images/like_thumb.png" /> <span class="review_one_thum">(1 Reviewers) </span></span>
                  <span class="thum_down_voice_review"> <img src="images/thum_down.png" /> <span class="review_one_thum">(1 Reviewers) </span></span>
               </div>
            </div>
         </div>
         <!-- End Here Col-md-4 -->
      </div>
   </div>
</section>
<!-- End Here -->
<!-- beutics_blog_section -->
<section class="beutics_blog_section padding_tb_60">
   <div class="container-fluid">
      <!-- blogs -->
      <div class="blog_section row">
         <div class="blog_slider_section col-md-9 offset-md-3 no_padding">
            <div class="blog_slider slider">
               <div class="cu_slider_item">
                  <div class="blog_wrap">
                     <div class="slider-caption-wrap">
                        <div class="caption_title">
                           <h2>What is Lorem Ipsum ?</h2>
                        </div>
                        <div class="discription">
                           Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </div>
                     </div>
                  </div>
               </div>
               <div class="cu_slider_item">
                  <div class="blog_wrap">
                     <div class="slider-caption-wrap">
                        <div class="caption_title">
                           <h2>What is Lorem Ipsum ?</h2>
                        </div>
                        <div class="discription">
                           Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </div>
                     </div>
                  </div>
               </div>
               <div class="cu_slider_item">
                  <div class="blog_wrap">
                     <div class="slider-caption-wrap">
                        <div class="caption_title">
                           <h2>What is Lorem Ipsum ?</h2>
                        </div>
                        <div class="discription">
                           Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </div>
                     </div>
                  </div>
               </div>
               <div class="cu_slider_item">
                  <div class="blog_wrap">
                     <div class="slider-caption-wrap">
                        <div class="caption_title">
                           <h2>What is Lorem Ipsum ?</h2>
                        </div>
                        <div class="discription">
                           Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </div>
                     </div>
                  </div>
               </div>
               <div class="cu_slider_item">
                  <div class="blog_wrap">
                     <div class="slider-caption-wrap">
                        <div class="caption_title">
                           <h2>What is Lorem Ipsum ?</h2>
                        </div>
                        <div class="discription">
                           Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- beutics_blog_section End-->
@endsection