@extends('layouts.emails.welcomeEmail')

@section('content')

<table style="width:600px; max-width:100%; margin:0px auto; background-color:#ffffff;" cellspacing="0">
		<tr>
			<td>
				<table style="width: 100%;">
					<tr>
						<td style="text-align:center; padding:5px 0px 7px;"><img src="{{ url('/') }}/public/welcome_images/logo.jpg" alt="Logo" width="107" /></td>
					</tr>
				</table>
				<table style="width: 100%;">
					<tr>
						<td style="text-align:center;"><img src="{{ url('/') }}/public/welcome_images/banner-9.jpg" width="600" alt="Banner Image" /></td> 
					</tr>
				</table>
				<table style="width: 100%;">
					<tr>
						<td style="padding: 15px 12%;">
							<p style="color: #b33775; font-weight: 600; font-size: 13px; margin: 20px 0px; line-height: 24px;">Hi {{$data['name']}},</p>
							<p style="color: #b33775; font-weight: 500; font-size: 13px; margin: 20px 0px; line-height: 24px; text-align: justify;">Thanks for using Beutics!</p>
							<p style="color: #b33775; font-weight: 500; font-size: 13px; margin: 20px 0px; line-height: 24px; text-align: justify;">As per your request booking order number ({{$data['order_no']}}) with {{$data['store_name']}} is cancelled. We regret this experience and hope to see you make an appointment again soon.</p>
							<p style="color: #b33775; font-weight: 500; font-size: 13px; margin: 20px 0px; line-height: 24px; text-align: justify;">We have initiated the refund to your account and the same will reflect in your wallet asap. If you think this was a mistake,</p>
						</td>
					</tr>
				</table>
				<table style="width: 100%;background:#b33775">
					<tr>
						<td style="padding: 15px 12%;">
							<p style="color: #ffffff; font-weight: 400; font-size: 13px; margin: 20px 0px; line-height: 24px; text-align: justify;">Please contact our Support team in chat with below details :</p>
							<table style="width: 100%;">
								<tr>
									<td style="padding:0px 0px 10px;">
										<table style="width: 100%;">
											<tr>
												<td style="width:53px; height:53px;"><img src="{{ url('/') }}/public/welcome_images/icon1.png" width="53" alt="Email Image"></td>
												<td><p style="font-size:13px; font-weight:600; line-height:24px; color:#ffffff; padding: 0px 20px;">Order Number</p></td>
											</tr>
										</table>
									</td>
									<td style="padding:0px 0px 10px;">
										<table style="width: 100%;">
											<tr>
												<td style="width:53px; height:53px;"><img src="{{ url('/') }}/public/welcome_images/icon2.png" width="53" alt="Email Image"></td>
												<td><p style="font-size:13px; font-weight:600; line-height:24px; color:#ffffff; padding: 0px 10px 0px 20px;">Service Provider Name</p></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td style="padding:10px 0px 15px;">
										<table style="width: 100%;">
											<tr>
												<td style="width:53px; height:53px;"><img src="{{ url('/') }}/public/welcome_images/icon3.png" width="53" alt="Email Image"></td>
												<td><p style="font-size:13px; font-weight:600; line-height:24px; color:#ffffff; padding: 0px 20px;">Cancellation Date & Time</p></td>
											</tr>
										</table>
									</td>
									<td style="padding:10px 0px 15px;">
										<table style="width: 100%;">
											<tr>
												<td style="width:53px; height:53px;"><img src="{{ url('/') }}/public/welcome_images/icon4.png" width="53" alt="Email Image"></td>
												<td><p style="font-size:13px; font-weight:600; line-height:24px; color:#ffffff; padding: 0px 20px;">Payment Method & Amount</p></td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<table style="width: 100%; background-color:#ffffff;" >
					<tr>
						<td style="text-align:left; padding: 15px 12%;">
							<p style="font-weight:500; color: #b43675; font-size: 13px; margin: 20px 0px 10px;">Cheers to your great success!</p>
							<p style="font-weight:600; color: #b43675; font-size: 14px; margin: 10px 0px 20px;">The Beutics team.</p>
						</td>
					</tr>
				</table>
				<table style="width: 100%; background-color:#4B96B3; padding: 20px 0px;" >
					<tr>
						<td style="text-align:center;"><p style="font-weight:500; color: #ffffff; font-size: 13px; margin: 15px 0px;">Follow us for updates, discounts, and more.</p></td>
					</tr>
					<tr>
						<td style="text-align:center;">
							<p style="margin:0px;">
								<a href="https://www.facebook.com/beuticsapp/" target="_blank"><img src="{{ url('/') }}/public/welcome_images/facebook.png" alt="Facebook Logo" width="40" style="margin:10px 0px;" /></a>
								<a href="https://www.instagram.com/beuticsapp" target="_blank" ><img src="{{ url('/') }}/public/welcome_images/instagram.png" alt="Instagram Logo" width="40" style="margin:10px 0px;" /></a>
								<a href="https://twitter.com/beuticsapp" target="_blank"><img src="{{ url('/') }}/public/welcome_images/twitter.png" alt="Twitter Logo" width="40" style="margin:10px 0px;" /></a>
							</p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	@endsection