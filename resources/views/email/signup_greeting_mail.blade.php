@extends('layouts.emails.email')

@section('content')
<h1 align="center" style="font-size:40px;color:#cb7da4:margin-top:60px;">Beutics</h1>
<h3 style="letter-spacing:0.3px; color:#000000;margin-bottom:30px; margin-top:60px;"> We have received your application. The team will reach out to review and help you onboard Beutics marketplace as soon as possible.<br/>
Thank you and look forward to helping you succeed.<br/></br></br>
Should you wish to talk to Beutics Sales please call us at +971 58 5807599 or write to us at support@beutics.com
</h3>
@endsection