@extends('layouts.emails.welcomeEmail')

@section('content')

<table style="width:600px; max-width:100%; margin:0px auto; background-color:#ffffff;" cellspacing="0">
		<tr>
			<td>
				<table style="width: 100%;">
					<tr>
						<td style="text-align:center; padding:5px 0px 7px;"><img src="{{ url('/') }}/public/welcome_images/logo.jpg" alt="Logo" width="107" /></td>
					</tr>
				</table>
				<table style="width: 100%;">
					<tr>
						<td style="text-align:center;"><img src="{{ url('/') }}/public/welcome_images/banner-10.jpg" width="600" alt="Banner Image" /></td> 
					</tr>
				</table>
				<table style="width: 100%;">
					<tr>
						<td style="padding: 15px 12%;">
							<p style="color: #b33775; font-weight: 600; font-size: 13px; margin: 20px 0px; line-height: 24px;">Dear {{$data['customer_name']}},</p>
							<p style="color: #b33775; font-weight: 500; font-size: 13px; margin: 20px 0px; line-height: 24px; text-align: justify;">Time to rejoice! You are our select few to share this moment of joy of Beutics app launch. Thanks for downloading the App and signing up. As a token of appreciation, we are offering vouchers that you can use with the promo code while booking services on Beutics APP at salons, spas, fitness centers or if you need a dental or laser treatment.</p>
							<table style="width: 100%;">
								<tr><td style="padding:15px ; text-align:center;"></td></tr>
								<tr>
									<td valign="top" style="padding:0px 0px 10px; text-align:center;">
										<img src="{{ url('/') }}/public/welcome_images/title_image1.jpg" width="184" alt="Email Image" style="margin: 0 auto; display: block;">
										<p style="font-size:13px; font-weight:600; line-height:24px; color:#000000; width:100%; margin: 0px; text-align:center; display:block;">{{$data['code_limit']}} vouchers</p>
										<p style="font-size:13px; font-weight:600; line-height:24px; color:#000000; width:100%; margin: 0px; text-align:center; display:block;">@ {{$data['discount'].' '.$data['discount_label']}} OFF</p>
									</td>
									<td valign="top" style="padding:0px 0px 10px; text-align:center;">
										<img src="{{ url('/') }}/public/welcome_images/title_image2.jpg" width="184" alt="Email Image" style="margin: 0 auto; display: block;">
										<p style="font-size:13px; font-weight:600; line-height:24px; color:#000000; width:100%; margin: 0px; text-align:center; display:block;">Voucher expires after</p>
										<p style="font-size:13px; font-weight:600; line-height:24px; color:#000000; width:100%; margin: 0px; text-align:center; display:block;">{{$data['code_limit']}} Usages</p>
									</td>
								</tr>
								<tr><td style="padding:15px ; text-align:center;"></td></tr>
								<tr>
									<td valign="top" style="padding:0px 0px 10px; text-align:center;">
										<img src="{{ url('/') }}/public/welcome_images/title_image3.jpg" width="184" alt="Email Image" style="margin: 0 auto; display: block;">
										<p style="font-size:13px; font-weight:600; line-height:24px; color:#000000; width:100%; margin: 0px; text-align:center; display:block;">Only One(1) voucher</p>
										<p style="font-size:13px; font-weight:600; line-height:24px; color:#000000; width:100%; margin: 0px; text-align:center; display:block;">can be applied</p>
										<p style="font-size:13px; font-weight:600; line-height:24px; color:#000000; width:100%; margin: 0px; text-align:center; display:block;">per Transaction/Order.</p>
									</td>
									<td valign="top" style="padding:0px 0px 10px; text-align:center;">
										<img src="{{ url('/') }}/public/welcome_images/title_image4.jpg" width="184" alt="Email Image" style="margin: 0 auto; display: block;">
										<p style="font-size:13px; font-weight:600; line-height:24px; color:#000000; width:100%; margin: 0px; text-align:center; display:block;">{{$data['total_days']}} Days</p>
										<p style="font-size:13px; font-weight:600; line-height:24px; color:#000000; width:100%; margin: 0px; text-align:center; display:block;">from today</p>
									</td>
								</tr>
								<tr><td style="padding:15px ; text-align:center;"></td></tr>
							</table>
						</td>
					</tr>
				</table>
				<table style="width: 100%;background:#b33775">
					<tr>
						<td style="padding: 15px 12%;">
							<p style="color: #ffffff; font-weight: 600; font-size: 13px; margin: 20px 0px 0px; line-height: 24px;">How does it work?</p>
							<p style="color: #ffffff; font-weight: 400; font-size: 13px; margin: 0px 0px 20px; line-height: 24px; text-align: justify;">Your personalized Promo codes are securely placed in your Promo Code Roster(can be found in booking section). Simply book services at any store and apply the relevant Promo code. You begin your saving journey with Beutics. After all we are all about Convenience and Saving. Enjoy!</p>
							<p style="color: #ffffff; font-weight: 600; font-size: 13px; margin: 20px 0px 0px; line-height: 24px;">Can I gift the vouchers?</p>
							<p style="color: #ffffff; font-weight: 400; font-size: 13px; margin: 0px 0px 20px; line-height: 24px; text-align: justify;">You have earned these vouchers and therefore it’s only applicable for your use. However, you can certainly gift services to your friends and apply these discounted vouchers. Trust we brought a smile on your face! </p>
							<p style="text-align:center; width:100%; margin: 25px 0px;"><a href="http://api.beutics.com" style="font-weight: 600; background:#ffffff; color:#b33775; width:260px; height:48px; margin:0px auto; display:inline-block; text-transform: uppercase; text-decoration: none; line-height: 48px; font-size: 14px;">Take me to the voucher!</a></p>
						</td>
					</tr>
				</table>
				<table style="width: 100%; background-color:#ffffff;" >
					<tr>
						<td style="text-align:left; padding: 15px 12%;">
							<p style="font-weight:500; color: #b43675; font-size: 13px; margin: 20px 0px 10px;">Cheers to your great success!</p>
							<p style="font-weight:600; color: #b43675; font-size: 14px; margin: 10px 0px 20px;">The Beutics team.</p>
						</td>
					</tr>
				</table>
				<table style="width: 100%; background-color:#4B96B3; padding: 20px 0px;" >
					<tr>
						<td style="text-align:center;"><p style="font-weight:500; color: #ffffff; font-size: 13px; margin: 15px 0px;">Follow us for updates, discounts, and more.</p></td>
					</tr>
					<tr>
						<td style="text-align:center;">
							<p style="margin:0px;">
								<a href="https://www.facebook.com/beuticsapp/" target="_blank"><img src="{{ url('/') }}/public/welcome_images/facebook.png" alt="Facebook Logo" width="40" style="margin:10px 0px;" /></a>
								<a href="https://www.instagram.com/beuticsapp" target="_blank" ><img src="{{ url('/') }}/public/welcome_images/instagram.png" alt="Instagram Logo" width="40" style="margin:10px 0px;" /></a>
								<a href="https://twitter.com/beuticsapp" target="_blank"><img src="{{ url('/') }}/public/welcome_images/twitter.png" alt="Twitter Logo" width="40" style="margin:10px 0px;" /></a>
							</p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	@endsection