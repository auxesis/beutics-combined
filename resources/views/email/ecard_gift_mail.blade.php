@extends('layouts.emails.welcomeEmail')

@section('content')

<table style="width:600px; max-width:100%; margin:0px auto; background-color:#ffffff;" cellspacing="0">
		<tr>
			<td>
				<table style="width: 100%;">
					<tr>
						<td style="text-align:center; padding:5px 0px 7px;"><img src="{{ url('/') }}/public/welcome_images/logo.jpg" alt="Logo" width="107" /></td>
					</tr>
				</table>
				<table style="width: 100%;">
					<tr>
						<td style="text-align:center;"><img src="{{ $data['theme'] }}" width="600" alt="Banner Image" /></td> 
					</tr>
				</table>
				<table style="width: 100%;">
					<tr>
						<td style="padding: 15px 12%;">
							<p style="color: #b33775; font-weight: 600; font-size: 13px; margin: 20px 0px; line-height: 24px;">Dear {{$data['receiver_name'] }},</p>
							<p style="color: #b33775; font-weight: 500; font-size: 13px; margin: 20px 0px; line-height: 24px; text-align: justify;">Happy {{$data['occasion'] }}! Your friend {{$data['sender_name'] }} has send across a surprise gift Card worth AED  {{$data['gift_card_amount'] }} from Beutics app.</p>
							<p style="text-align:center; width:100%; margin: 25px 0px;"><a href="{{route('app_redirect')}}" style="font-weight: 600; background:#b33775; color:#ffffff; width:190px; height:48px; margin:0px auto; display:inline-block; text-transform: uppercase; text-decoration: none; line-height: 48px; font-size: 14px;">Download Now</a></p>
							<p style="color: #b33775; font-weight: 500; font-size: 13px; margin: 20px 0px; line-height: 24px; text-align: justify;">{{$data['sender_name'] }} has also shared personalized wishes. Take a look: {{$data['receiver_message'] }}.</p>
						</td>
					</tr>
				</table>
				<table style="width: 100%;background:#b33775">
					<tr>
						<td style="padding: 15px 12%;">
							<p style="color: #ffffff; font-weight: 400; font-size: 13px; margin: 20px 0px; line-height: 24px; text-align: justify;">If already installed, check your Beutics Wallet.</p>
							<p style="color: #ffffff; font-weight: 600; font-size: 13px; margin: 20px 0px 0px; line-height: 24px;">How to obtain the gift card?</p>
							<p style="color: #ffffff; font-weight: 400; font-size: 13px; margin: 0px 0px 20px; line-height: 24px; text-align: justify;">Well first and foremost, you need to download the Beutics App from Google or Apple play store. As soon as you sign up, your newly created Beutics wallet with be credited with gifted cash card amount. It’s that simple! Happily shop for any service and use wallet to pay.</p>
							<p style="color: #ffffff; font-weight: 600; font-size: 13px; margin: 20px 0px 0px; line-height: 24px;">Did you know?</p>
							<p style="color: #ffffff; font-weight: 400; font-size: 13px; margin: 0px 0px 20px; line-height: 24px; text-align: justify;">Gift card share has another practical application to be used to top up your friends or family members wallet securely. This is a convenient alternative for someone without access to credit or debit cards.</p>
							<p style="color: #ffffff; font-weight: 400; font-size: 13px; margin: 20px 0px; line-height: 24px; text-align: justify;">We really hope that you enjoy the gift and derive full pleasure and benefit out of it. Just to let you know, if you do need to reciprocate, you can gift a card using Beutics App.</p>
						</td>
					</tr>
				</table>
				<table style="width: 100%; background-color:#ffffff;" >
					<tr>
						<td style="text-align:left; padding: 15px 12%;">
							<p style="font-weight:500; color: #b43675; font-size: 13px; margin: 20px 0px 10px;">Cheers to your great success!</p>
							<p style="font-weight:600; color: #b43675; font-size: 14px; margin: 10px 0px 20px;">The Beutics team.</p>
						</td>
					</tr>
				</table>
				<table style="width: 100%; background-color:#4B96B3; padding: 20px 0px;" >
					<tr>
						<td style="text-align:center;"><p style="font-weight:500; color: #ffffff; font-size: 13px; margin: 15px 0px;">Follow us for updates, discounts, and more.</p></td>
					</tr>
					<tr>
						<td style="text-align:center;">
							<p style="margin:0px;">
								<a href="https://www.facebook.com/beuticsapp/" target="_blank"><img src="{{ url('/') }}/public/welcome_images/facebook.png" alt="Facebook Logo" width="40" style="margin:10px 0px;" /></a>
								<a href="https://www.instagram.com/beuticsapp" target="_blank" ><img src="{{ url('/') }}/public/welcome_images/instagram.png" alt="Instagram Logo" width="40" style="margin:10px 0px;" /></a>
								<a href="https://twitter.com/beuticsapp" target="_blank"><img src="{{ url('/') }}/public/welcome_images/twitter.png" alt="Twitter Logo" width="40" style="margin:10px 0px;" /></a>
							</p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	@endsection