@extends('layouts.emails.welcomeEmail')

@section('content')

<table style="width:600px; max-width:100%; margin:0px auto; background-color:#ffffff;" cellspacing="0">
		<tr>
			<td>
				<table style="width: 100%;">
					<tr>
						<td style="text-align:center; padding:5px 0px 7px;"><img src="{{ url('/') }}/public/welcome_images/logo.jpg" alt="Logo" width="107" /></td>
					</tr>
				</table>
				<table style="width: 100%;">
					<tr>
						<td style="text-align:center;"><img src="{{ url('/') }}/public/welcome_images/banner-7.jpg" width="600" alt="Banner Image" /></td> 
					</tr>
				</table>
				<table style="width: 100%;">
					<tr>
						<td style="padding: 15px 12%;">
							<p style="color: #b33775; font-weight: 600; font-size: 13px; margin: 20px 0px; line-height: 24px;">Hello {{$data['name']}},</p>
							<p style="color: #b33775; font-weight: 500; font-size: 13px; margin: 20px 0px; line-height: 24px; text-align: justify;">Welcome to Beutics! Thank you for installing the must-have Beauty, Fitness, Wellness companion App.</p>
							<p style="color: #b33775; font-weight: 500; font-size: 13px; margin: 20px 0px; line-height: 24px; text-align: justify;">You have joined us alongside many others who use and love Beutics. This comprehensive application has been designed keeping you and your needs at the prime. Beutics aspires to elevate your daily experience with unmatched convenience and value. Your quest to look good, stay fit and maintain wellness will never be the same.</p>
							<p style="color: #b33775; font-weight: 500; font-size: 13px; margin: 20px 0px; line-height: 24px; text-align: justify;">Throughout your journey with Beutics we will use your phone and email to communicate and notify and for this reason we request you to verify your Beutics account.</p>
						</td>
					</tr>
				</table>
				<table style="width: 100%;background:#b33775">
					<tr>
						<td style="padding: 15px 12%;">
		

							<p style="color: #ffffff; font-weight: 600; font-size: 13px; margin: 20px 0px 0px; line-height: 24px;">into your App sign up.</p>
							<p style="color: #ffffff; font-weight: 400; font-size: 13px; margin: 0px; line-height: 24px; text-align: justify;">Please click the link below within the next 30 days</p>
							<p style="color: #ffffff; font-weight: 400; font-size: 13px; margin: 0px 0px 20px; line-height: 24px; text-align: justify;">
								<a href="{!! route('front.confirm.email',['token'=>$data['token']]) !!}" target="_blank" style="color: #fff600;text-decoration: none;">https://www.beutics.com/user-email-confirmation</a>
							</p>
							
							<p style="color: #ffffff; font-weight: 400; font-size: 13px; margin: 20px 0px; line-height: 24px; text-align: justify;">If you have any questions regarding your Beutics account, please contact us at <a href="mailto:contact@beutics.com" target="_blank" style="color: #fff600;text-decoration: none;">contact@beutics.com</a> Or chat with us through the App. Our technical support team will assist you with anything you need.</p>
							<p style="color: #ffffff; font-weight: 400; font-size: 13px; margin: 20px 0px; line-height: 24px; text-align: justify;">Enjoy yourself, and welcome to Beutics.</p>
						</td>
					</tr>
				</table>
				<table style="width: 100%; background-color:#ffffff;" >
					<tr>
						<td style="text-align:left; padding: 15px 12%;">
							<p style="font-weight:500; color: #b43675; font-size: 13px; margin: 20px 0px 10px;">Cheers to your great success!</p>
							<p style="font-weight:600; color: #b43675; font-size: 14px; margin: 10px 0px 20px;">The Beutics team.</p>
						</td>
					</tr>
				</table>
				<table style="width: 100%; background-color:#4B96B3; padding: 20px 0px;" >
					<tr>
						<td style="text-align:center;"><p style="font-weight:500; color: #ffffff; font-size: 13px; margin: 15px 0px;">Follow us for updates, discounts, and more.</p></td>
					</tr>
					<tr>
						<td style="text-align:center;">
							<p style="margin:0px;">
								<a href="#" target="_blank"><img src="{{ url('/') }}/public/welcome_images/facebook.png" alt="Facebook Logo" width="40" style="margin:10px 0px;" /></a>
								<a href="#" target="_blank" ><img src="{{ url('/') }}/public/welcome_images/instagram.png" alt="Instagram Logo" width="40" style="margin:10px 0px;" /></a>
								<a href="#" target="_blank"><img src="{{ url('/') }}/public/welcome_images/twitter.png" alt="Twitter Logo" width="40" style="margin:10px 0px;" /></a>
							</p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	@endsection