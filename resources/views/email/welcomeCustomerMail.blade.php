@extends('layouts.emails.welcomeEmail')

@section('content')

<table style="width:600px; max-width:100%; margin:0px auto; background-color:#ffffff;" cellspacing="0">
		<tr>
			<td>
				<table style="width: 100%;">
					<tr>
						<td style="text-align:center; padding:5px 0px 7px;"><img src="{{ url('/') }}/public/welcome_images/logo.jpg" alt="Logo" width="107" /></td>
					</tr>
				</table>
				<table style="width: 100%;">
					<tr>
						<td style="text-align:center;"><img src="{{ url('/') }}/public/welcome_images/banner-13.jpg" width="600" alt="Banner Image" /></td> 
					</tr>
				</table>
				<table style="width: 100%;background:#F5FBF2">
					<tr>
						<td style="padding: 20px 12%;">
							<p style="color: #75C14E; font-weight: 600; font-size: 13px; margin: 0px; line-height: 24px; text-align: center;">Your companion App that promises</p>
							<p style="color: #707070; font-weight: 400; font-size: 13px; margin: 0px; line-height: 24px; text-align: center;">Convenience like never before.</p>
							<p style="color: #707070; font-weight: 400; font-size: 13px; margin: 0px; line-height: 24px; text-align: center;">Simply Save and Earn.</p>
							<p style="color: #707070; font-weight: 400; font-size: 13px; margin: 0px; line-height: 24px; text-align: center;">One stop source for your Beauty, Fitness</p>
							<p style="color: #707070; font-weight: 400; font-size: 13px; margin: 0px; line-height: 24px; text-align: center;">and Wellness Services.</p>
						</td>
					</tr>
				</table>
				<table style="width: 100%;">
					<tr>
						<td style="padding: 15px 12%;">
							<p style="color: #717171; font-weight: 600; font-size: 13px; margin: 20px 0px; line-height: 24px;">Hello {{$data['name']}},</p>
							<p style="color: #717171; font-weight: 500; font-size: 13px; margin: 20px 0px 30px; line-height: 24px; text-align: justify;">To get started with, we like you to read through the following to understand this initiative.</p>
							<p style="color: #4C95B3; font-weight: 600; font-size: 24px; margin: 30px 0px 20px; line-height: 24px; text-align: center;">Quick tips to using the App</p>
							<p style="color: #717171; font-weight: 500; font-size: 13px; margin: 20px 0px; line-height: 24px; text-align: center;">This App promises to be power-packed, feature-rich, multiple Earn & Save avenues! Wanna make the best of it…</p>
							<table style="width: 100%;">
								<tr><td style="padding:15px ; text-align:center;"></td></tr>
								<tr>
									<td style="padding:0px 15px 10px; text-align:center;">
										<img src="{{ url('/') }}/public/welcome_images/title_image5.jpg" width="184" alt="Email Image" style="margin: 0 auto; display: block;">
										<p style="font-size:13px; font-weight:500; line-height:24px; color:#717171; width:100%; margin: 15px 0px; text-align:center; display:block;">Should you need, don’t shy from selecting multiple services at a time!</p>
									</td>
									<td style="padding:0px 15px 10px; text-align:center;">
										<img src="{{ url('/') }}/public/welcome_images/title_image6.jpg" width="184" alt="Email Image" style="margin: 0 auto; display: block;">
										<p style="font-size:13px; font-weight:500; line-height:24px; color:#717171; width:100%; margin: 15px 0px; text-align:center; display:block;">Earn cash back upto AED 2500 by referring Beutics App with your friends and family.</p>
									</td>
								</tr>
								<tr><td style="padding:15px ; text-align:center;"></td></tr>
								<tr>
									<td style="padding:0px 15px 10px; text-align:center;">
										<img src="{{ url('/') }}/public/welcome_images/title_image7.jpg" width="184" alt="Email Image" style="margin: 0 auto; display: block;">
										<p style="font-size:13px; font-weight:500; line-height:24px; color:#717171; width:100%; margin: 15px 0px; text-align:center; display:block;">It cannot get any further detailed and insightful. Feedback on all relevant aspects of the store to help make your decision quicker.</p>
									</td>
									<td style="padding:0px 15px 10px; text-align:center;">
										<img src="{{ url('/') }}/public/welcome_images/title_image8.jpg" width="184" alt="Email Image" style="margin: 0 auto; display: block;">
										<p style="font-size:13px; font-weight:500; line-height:24px; color:#717171; width:100%; margin: 15px 0px; text-align:center; display:block;">Gifting a service to your near and dear ones is a simplified affair. Further imagine, sharing a Gift card and topping up your friends wallet!</p>
									</td>
								</tr>
							</table>
							<table style="width: 100%;">
								<tr><td style="padding:15px ; text-align:center;"></td></tr>
								<tr>
									<td style="padding:0px 15px 10px; text-align:center;">
										<img src="{{ url('/') }}/public/welcome_images/title_image9.jpg" width="184" alt="Email Image" style="margin: 0 auto; display: block;">
										<p style="font-size:13px; font-weight:500; line-height:24px; color:#717171; width:100%; margin: 15px 0px; text-align:center; display:block;">Earn cashback on every order that you process online. This rule is in exception only if you make a purchase with clinics in Aesthetics category, but you still earn, just provide us proof of invoice with relevant beutics order ID. “Imagine a scenario: You spend and earn cashback at a Gym and redeem the earned cashback for any purchase at other stores (Salon, spa, aesthetics)”.</p>
									</td>
								</tr>
							</table>
							<table style="width: 100%;">
								<tr><td style="padding:15px ; text-align:center;"></td></tr>
								<tr>
									<td style="padding:0px 15px 10px; text-align:center;">
										<img src="{{ url('/') }}/public/welcome_images/title_image10.jpg" width="184" alt="Email Image" style="margin: 0 auto; display: block;">
									</td>
									<td style="padding:0px 15px 10px; text-align:center;">
										<img src="{{ url('/') }}/public/welcome_images/title_image11.jpg" width="184" alt="Email Image" style="margin: 0 auto; display: block;">
									</td>
								</tr>
								<tr><td style="padding:15px ; text-align:center;"></td></tr>
								<tr>
									<td style="padding:0px 15px 10px; text-align:center;">
										<img src="{{ url('/') }}/public/welcome_images/title_image12.jpg" width="184" alt="Email Image" style="margin: 0 auto; display: block;">
									</td>
									<td style="padding:0px 15px 10px; text-align:center;">
										<img src="{{ url('/') }}/public/welcome_images/title_image13.jpg" width="184" alt="Email Image" style="margin: 0 auto; display: block;">
									</td>
								</tr>
								<tr><td style="padding:15px ; text-align:center;"></td></tr>
							</table>
						</td>
					</tr>
				</table>
				<table style="width: 100%;background:#b33775">
					<tr>
						<td style="padding: 15px 12%;">
							<p style="color: #ffffff; font-weight: 600; font-size: 13px; margin: 20px 0px; line-height: 24px;">Our Objective</p>
							<p style="color: #ffffff; font-weight: 400; font-size: 13px; margin: 20px 0px; line-height: 24px; text-align: justify;">At Beutics, we are driven by just one objective, find ways to offer you Value. We created Beutics keeping you in the center and developed ways to make your lifestyle convenient and filled with rich experience. We believe, we have managed to derive value through this app in following ways</p>
							<p style="color: #ffffff; font-weight: 400; font-size: 13px; margin: 20px 0px; line-height: 24px; text-align: justify;">1 You can now earn and redeem Cashback with Beutics. It’s worth imagining the amount you can earn annually every time you have spent at a salon, spa, gym or at clinics! </p>
							<p style="color: #ffffff; font-weight: 400; font-size: 13px; margin: 20px 0px; line-height: 24px; text-align: justify;">2 You will certainly find the best offers and prices in town, We Bet! Beutics ecosystem is brimming with partners that are excited to welcome you with best value for money.</p>
							<p style="color: #ffffff; font-weight: 400; font-size: 13px; margin: 20px 0px; line-height: 24px; text-align: justify;">3 You can now read feedback of your friends/others or share your reviews and contribute to mutual development. Now you know upfront what to expect at a store and no more surprises.</p>
							<p style="color: #ffffff; font-weight: 400; font-size: 13px; margin: 20px 0px; line-height: 24px; text-align: justify;">4 All the above along with sheer convenience to find and book appointments 24 x 7 with your favourite staff at any of your desired stores.</p>
						</td>
					</tr>
				</table>
				<table style="width: 100%; background-color:#4B96B3; padding: 20px 0px;" >
					<tr>
						<td style="text-align:center;"><p style="font-weight:500; color: #ffffff; font-size: 13px; margin: 15px 0px;">Follow us for updates, discounts, and more.</p></td>
					</tr>
					<tr>
						<td style="text-align:center;">
							<p style="margin:0px;">
								<a href="https://www.facebook.com/beuticsapp/" target="_blank"><img src="{{ url('/') }}/public/welcome_images/facebook.png" alt="Facebook Logo" width="40" style="margin:10px 0px;" /></a>
								<a href="https://www.instagram.com/beuticsapp" target="_blank" ><img src="{{ url('/') }}/public/welcome_images/instagram.png" alt="Instagram Logo" width="40" style="margin:10px 0px;" /></a>
								<a href="https://twitter.com/beuticsapp" target="_blank"><img src="{{ url('/') }}/public/welcome_images/twitter.png" alt="Twitter Logo" width="40" style="margin:10px 0px;" /></a>
							</p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	@endsection