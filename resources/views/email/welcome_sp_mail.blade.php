@extends('layouts.emails.welcomeEmail')

@section('content')

<table style="width:600px; max-width:100%; margin:0px auto; background-color:#ffffff;" cellspacing="0">
		<tr>
			<td>
				<table style="width: 100%;">
					<tr>
						<td style="text-align:center; padding:5px 0px 7px;"><img src="{{ url('/') }}/public/welcome_images/logo.jpg" alt="Logo" width="107" /></td>
					</tr>
				</table>
				<table style="width: 100%;">
					<tr>
						<td style="text-align:center;"><img src="{{ url('/') }}/public/welcome_images/banner-12.jpg" width="600" alt="Banner Image" /></td> 
					</tr>
				</table>
				<table style="width: 100%;">
					<tr>
						<td style="padding: 15px 12%;">
							<p style="color: #b33775; font-weight: 600; font-size: 13px; margin: 10px 0px 20px; line-height: 24px;">Dear {{$data['name']}},</p>
							<p style="color: #b33775; font-weight: 600; font-size: 13px; margin: 10px 0px; line-height: 24px;">Your {{$data['store_name']}} has been successfully validated.</p>
							<p style="color: #b33775; font-weight: 500; font-size: 13px; margin: 10px 0px 20px; line-height: 24px;">We heartily welcome you to our world of great opportunities. We are really excited having you on Beutics because we believe our app users are going to keep you busy. Just make sure you have listed your service under the right category so that our app users can easily find you.</p>
						</td>
					</tr>
				</table>
				<table style="width: 100%;">
					<tr>
						<td style="padding: 15px 12% 0px;">
							<p style="color: #4C95B3;font-weight: 600;font-size: 28px;margin: 0px 0px;line-height: 34px;width: 100%;text-align: center;">CHECKOUT</p>						
							<p style="color: #717171; font-weight: 400;font-size: 13px;margin: 0px 0px 20px;width: 100%;text-align: center;line-height: 24px;">You have been granted full access to the Beutics provider app.</p>						
							<table style="width: 100%; padding:20px 0px 0px;">
								<tr>
									<td style="padding:0px 0px 10px;">
										<table style="width: 100%;">
											<tr>
												<td style="width:52px; height:52px;"><img src="{{ url('/') }}/public/welcome_images/icon5.png" width="52" alt="Email Image"></td>
												<td><p style="font-size:13px; font-weight:600; line-height:24px; color:#717171; padding: 0px 20px;">Add Your Service</p></td>
											</tr>
										</table>
									</td>
									<td style="padding:0px 0px 10px;">
										<table style="width: 100%;">
											<tr>
												<td style="width:52px; height:52px;"><img src="{{ url('/') }}/public/welcome_images/icon6.png" width="52" alt="Email Image"></td>
												<td><p style="font-size:13px; font-weight:600; line-height:24px; color:#717171; padding: 0px 10px 0px 20px;">Create Offers</p></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td style="padding:10px 0px 15px;">
										<table style="width: 100%;">
											<tr>
												<td style="width:52px; height:52px;"><img src="{{ url('/') }}/public/welcome_images/icon7.png" width="52" alt="Email Image"></td>
												<td><p style="font-size:13px; font-weight:600; line-height:24px; color:#717171; padding: 0px 20px;">Add your Staff members</p></td>
											</tr>
										</table>
									</td>
									<td style="padding:10px 0px 15px;">
										<table style="width: 100%;">
											<tr>
												<td style="width:52px; height:52px;"><img src="{{ url('/') }}/public/welcome_images/icon8.png" width="52" alt="Email Image"></td>
												<td><p style="font-size:13px; font-weight:600; line-height:24px; color:#717171; padding: 0px 20px;">Manage your Appointments calendar</p></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td style="padding:10px 0px 15px;">
										<table style="width: 100%;">
											<tr>
												<td style="width:52px; height:52px;"><img src="{{ url('/') }}/public/welcome_images/icon9.png" width="52" alt="Email Image"></td>
												<td><p style="font-size:13px; font-weight:600; line-height:24px; color:#717171; padding: 0px 20px;">Respond to user queries in Forum</p></td>
											</tr>
										</table>
									</td>
									<td style="padding:10px 0px 15px;">
										<table style="width: 100%;">
											<tr>
												<td style="width:52px; height:52px;"><img src="{{ url('/') }}/public/welcome_images/icon10.png" width="52" alt="Email Image"></td>
												<td><p style="font-size:13px; font-weight:600; line-height:24px; color:#717171; padding: 0px 20px;">Update your account information</p></td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<table style="width: 100%; background-color:#ffffff;" >
					<tr>
						<td style="text-align:left; padding: 15px 12%;">
							<p style="font-weight:500; color: #b43675; font-size: 13px; margin: 20px 0px 10px;">Cheers to your great success!</p>
							<p style="font-weight:600; color: #b43675; font-size: 14px; margin: 10px 0px 20px;">The Beutics team.</p>
						</td>
					</tr>
				</table>
				<table style="width: 100%; background-color:#4B96B3; padding: 20px 0px;" >
					<tr>
						<td style="text-align:center;"><p style="font-weight:500; color: #ffffff; font-size: 13px; margin: 15px 0px;">Follow us for updates, discounts, and more.</p></td>
					</tr>
					<tr>
						<td style="text-align:center;">
							<p style="margin:0px;">
								<a href="https://www.facebook.com/beuticsapp/" target="_blank"><img src="{{ url('/') }}/public/welcome_images/facebook.png" alt="Facebook Logo" width="40" style="margin:10px 0px;" /></a>
								<a href="https://www.instagram.com/beuticsapp" target="_blank" ><img src="{{ url('/') }}/public/welcome_images/instagram.png" alt="Instagram Logo" width="40" style="margin:10px 0px;" /></a>
								<a href="https://twitter.com/beuticsapp" target="_blank"><img src="{{ url('/') }}/public/welcome_images/twitter.png" alt="Twitter Logo" width="40" style="margin:10px 0px;" /></a>
							</p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	@endsection