@extends('layouts.emails.welcomeEmail')

@section('content')

<table style="width:600px; max-width:100%; margin:0px auto; background-color:#ffffff;" cellspacing="0">
		<tr>
			<td>
				<table style="width: 100%;">
					<tr>
						<td style="text-align:center; padding:5px 0px 7px;"><img src="images/logo.jpg" alt="Logo" width="107" /></td>
					</tr>
				</table>
				<table style="width: 100%;">
					<tr>
						<td style="text-align:center;"><img src="images/banner-11.jpg" width="600" alt="Banner Image" /></td> 
					</tr>
				</table>
				<table style="width: 100%;">
					<tr>
						<td style="padding: 15px 12% 0px;">
							<p style="color: #b33775; font-weight: 500; font-size: 13px; margin: 20px 0px; line-height: 24px;"><span style="font-weight: 600;">{sender name}</span> uses Beutics to Look Good and Stay Fit and recommends you this App.</p>
						</td>
					</tr>
				</table>
				<table style="width: 100%;">
					<tr>
						<td style="padding: 0px 12%;">
							<p style="background:#FFF5F5; margin: 10px 0px 30px; width:100%; padding: 20px 10px;">
								<span style="color:#717171; width:100%; text-align:center;display: block;font-size: 20px;font-weight: 600; line-height: 34px;">REFERRAL CODE</span>
								<span style="color:#B33775; width:100%; text-align:center;display: block;font-size: 20px;font-weight: 600; line-height: 34px;">{CODE XXXXX}</span>
							</p>
							<p style="color: #b33775; font-weight: 500; font-size: 13px; margin: 20px 0px; line-height: 24px;"><span style="font-weight: 600;font-size: 15px;">AED 50</span> awaits you when you sign up using the above code.</p>
							<p style="color: #b33775; font-weight: 500; font-size: 13px; margin: 20px 0px; line-height: 24px;">Salon, Spa, Fitness centers, Cosmetic clinics & Home Services are now at your fingertips. True experience of convenience and savings packed into a single App.</p>
							<p style="color: #b33775; font-weight: 500; font-size: 13px; margin: 20px 0px; line-height: 24px;">Oh! by the way, you too can refer your friends and earn upto <span style="font-weight: 600;font-size: 15px;">AED 2500</span>. Spread this inspiration!</p>
							<p style="text-align:center; width:100%; margin: 25px 0px;"><a href="{{route('app_redirect')}}" style="font-weight: 600; background:#b33775; color:#ffffff; width:190px; height:48px; margin:0px auto; display:inline-block; text-transform: uppercase; text-decoration: none; line-height: 48px; font-size: 14px;">Download Now</a></p>
							<p style="color: #b33775; font-weight: 500; font-size: 13px; margin: 20px 0px 0px; line-height: 24px; text-align:center;">for more info <a href="https://beutics.com/" target="_blank" style="color:#b33775;">www.beutics.com.</a></p>
						</td>
					</tr>
				</table>
				<table style="width: 100%; background-color:#ffffff;" >
					<tr>
						<td style="text-align:left; padding: 15px 12%;">
							<p style="font-weight:500; color: #b43675; font-size: 13px; margin: 20px 0px 10px;">Cheers to your great success!</p>
							<p style="font-weight:600; color: #b43675; font-size: 14px; margin: 10px 0px 20px;">The Beutics team.</p>
						</td>
					</tr>
				</table>
				<table style="width: 100%; background-color:#4B96B3; padding: 20px 0px;" >
					<tr>
						<td style="text-align:center;"><p style="font-weight:500; color: #ffffff; font-size: 13px; margin: 15px 0px;">Follow us for updates, discounts, and more.</p></td>
					</tr>
					<tr>
						<td style="text-align:center;">
							<p style="margin:0px;">
								<a href="https://www.facebook.com/beuticsapp/" target="_blank"><img src="{{ url('/') }}/public/welcome_images/facebook.png" alt="Facebook Logo" width="40" style="margin:10px 0px;" /></a>
								<a href="https://www.instagram.com/beuticsapp" target="_blank" ><img src="{{ url('/') }}/public/welcome_images/instagram.png" alt="Instagram Logo" width="40" style="margin:10px 0px;" /></a>
								<a href="https://twitter.com/beuticsapp" target="_blank"><img src="{{ url('/') }}/public/welcome_images/twitter.png" alt="Twitter Logo" width="40" style="margin:10px 0px;" /></a>
							</p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	@endsection