@extends('layouts.emails.welcomeEmail')

@section('content')

<table style="width:600px; max-width:100%; margin:0px auto; background-color:#ffffff;" cellspacing="0">
		<tr>
			<td>
				<table style="width: 100%;">
					<tr>
						<td style="text-align:center; padding:5px 0px 7px;"><img src="{{ url('/') }}/public/welcome_images/logo.jpg" alt="Logo" width="107" /></td>
					</tr>
				</table>
				<table style="width: 100%;">
					<tr>
						<td style="text-align:center;"><img src="{{ url('/') }}/public/welcome_images/banner-5.jpg" width="600" alt="Banner Image" /></td> 
					</tr>
				</table>
				<table style="width: 100%;">
					<tr>
						<td style="padding: 15px 12%;">
							<p style="color: #b33775; font-weight: 600; font-size: 13px; margin: 20px 0px; line-height: 24px;">Dear {{$data['name']}},</p>
							<p style="color: #b33775; font-weight: 500; font-size: 13px; margin: 20px 0px; line-height: 24px; text-align: justify;">Great news! You have just received a new booking from {{$data['customer_name']}} for your {{$data['services']}}.</p>
							<p style="color: #b33775; font-weight: 500; font-size: 13px; margin: 20px 0px; line-height: 24px; text-align: justify;">For more details, you can contact {{$data['customer_name']}} through chat. All the best. We are sure you will do a great job.</p>
						</td>
					</tr>
				</table>
				<table style="width: 100%; background-color:#ffffff;" >
					<tr>
						<td style="text-align:left; padding: 15px 12%;">
							<p style="font-weight:500; color: #b43675; font-size: 13px; margin: 0px 0px 10px;">Cheers to your great success!</p>
							<p style="font-weight:600; color: #b43675; font-size: 14px; margin: 10px 0px 20px;">The Beutics team.</p>
						</td>
					</tr>
				</table>
				<table style="width: 100%; background-color:#4B96B3; padding: 20px 0px;" >
					<tr>
						<td style="text-align:center;"><p style="font-weight:500; color: #ffffff; font-size: 13px; margin: 15px 0px;">Follow us for updates, discounts, and more.</p></td>
					</tr>
					<tr>
						<td style="text-align:center;">
							<p style="margin:0px;">
								<a href="https://www.facebook.com/beuticsapp/" target="_blank"><img src="{{ url('/') }}/public/welcome_images/facebook.png" alt="Facebook Logo" width="40" style="margin:10px 0px;" /></a>
								<a href="https://www.instagram.com/beuticsapp" target="_blank" ><img src="{{ url('/') }}/public/welcome_images/instagram.png" alt="Instagram Logo" width="40" style="margin:10px 0px;" /></a>
								<a href="https://twitter.com/beuticsapp" target="_blank"><img src="{{ url('/') }}/public/welcome_images/twitter.png" alt="Twitter Logo" width="40" style="margin:10px 0px;" /></a>
							</p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	@endsection