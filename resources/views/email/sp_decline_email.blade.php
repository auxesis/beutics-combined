@extends('layouts.emails.email')

@section('content')

<h3 style="letter-spacing:0.3px; color:#000000;margin-bottom:30px; margin-top:75px;">Your account has been declined by admin</h3>
      <p style="padding:0 30px;line-height:1.5"><b>Reason of Decline is - </b>{{$data['decline_reason']}}</p>

@endsection