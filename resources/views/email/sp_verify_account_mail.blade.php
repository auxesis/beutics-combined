@extends('layouts.emails.welcomeEmail')

@section('content')

<table style="width:600px; max-width:100%; margin:0px auto; background-color:#ffffff;" cellspacing="0">
		<tr>
			<td>
				<table style="width: 100%;">
					<tr>
						<td style="text-align:center; padding:5px 0px 7px;"><img src="{{ url('/') }}/public/welcome_images/logo.jpg" alt="Logo" width="107" /></td>
					</tr>
				</table>
				<table style="width: 100%;">
					<tr>
						<td style="text-align:center;"><img src="{{ url('/') }}/public/welcome_images/banner-8.jpg" width="600" alt="Banner Image" /></td> 
					</tr>
				</table>
				<table style="width: 100%;">
					<tr>
						<td style="padding: 15px 12%;">
							<p style="color: #b33775; font-weight: 600; font-size: 13px; margin: 20px 0px; line-height: 24px;">Dear {{$data['name']}},</p>
							<p style="color: #b33775; font-weight: 500; font-size: 13px; margin: 20px 0px; line-height: 24px; text-align: justify;">You have requested to be registered as a service provider in the Beutics app - one of the fastest-growing on-demand service platforms. It has been specifically designed and promoted to generate maximum response for your business. There’s one quick step you need to complete before creating your Beutics account. </p>
						</td>
					</tr>
				</table>
				<table style="width: 100%;background:#b33775">
					<tr>
						<td style="padding: 15px 12%;">
							<p style="color: #ffffff; font-weight: 400; font-size: 13px; margin: 20px 0px; line-height: 24px; text-align: justify;">Let’s make sure this is the right mobile number of yours, please confirm this is the right mobile number {{$data['mobile_no']}} to use for your new account.</p>
							<p style="color: #ffffff; font-weight: 400; font-size: 13px; margin: 20px 0px; line-height: 24px; text-align: justify;">Please enter this verification code in your app to get started on Beutics:</p>
							<p style="color: #ffffff; font-weight: 400; font-size: 13px; margin: 20px 0px; line-height: 24px; text-align: justify;"><span style="font-weight: 600; padding: 8px 48px 8px 8px; color: #b33775; background: #ffffff;">{{$data['otp']}}</span></p>
						</td>
					</tr>
				</table>
				<table style="width: 100%; background-color:#ffffff;" >
					<tr>
						<td style="text-align:left; padding: 15px 12%;">
							<p style="font-weight:500; color: #b43675; font-size: 13px; margin: 20px 0px 10px;">Cheers to your great success!</p>
							<p style="font-weight:600; color: #b43675; font-size: 14px; margin: 10px 0px 20px;">The Beutics team.</p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	@endsection