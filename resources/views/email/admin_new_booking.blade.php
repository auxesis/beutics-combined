@extends('layouts.emails.email')

@section('content')
<h1 align="center" style="font-size:40px;color:#cb7da4:margin-top:60px;">Hello Beutics Admin,</h1>
<h3 style="letter-spacing:0.3px; color:#000000;margin-bottom:30px; margin-top:60px;"> {{$data['message']}}
</h3>
<p>Best Regards</p>
<p><b>Beutics</b></p>
@endsection


