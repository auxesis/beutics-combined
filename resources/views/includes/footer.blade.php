<footer class="page-footer">

  <div class="container text-center text-md-left">

    <div class="row">

      <div class="col-md-4 mx-auto footer_1">
       <div class="footer_item_wrap contact_details">
			<div class="contact_details_list">	<i class="fas fa-map-marker-alt"></i>
				<div class="field field--name-field-address- field--type-string field--label-above">
					<div class="field__item">Reg. Tech Drive Computer FZE Suite 1702, Level 17, Boulevard Plaza Tower 1, Sheikh Mohammed Bin Rashid Boulevard, PO Box, - 416654, Downtown Dubai, UAE</div>
				</div>
			</div>
			<div class="contact_details_list">	<i class="fas fa-phone-alt"></i>
				<div class="field field--name-field-phone-no- field--type-string field--label-above">
					<div class="field__items">
						<div class="field__item">+7 998 71 150 30 20</div>
					</div>
				</div>
			</div>
			<div class="contact_details_list">	<i class="fas fa-envelope"></i>
				<div class="field field--name-field-email- field--type-email field--label-above">
					<div class="field__item">contact@beutics.com</div>
				</div>
			</div>
		</div>
      </div>

      <div class="col-md-4 mx-auto footer_2">
        <div class="footer_item_wrap">
        	<div class="footer_menu hover-underline-animation">
		        <ul class="list-unstyled">
		          <li><a href="#!">Home</a></li>
		          <li><a href="#!">About Us</a></li>
		          <li><a href="#!">Service</a></li>
		          <li><a href="#!">Gift Card</a></li>
		          <li><a href="#!">Salon</a></li>
		          <li><a href="#!">Spa</a></li>
		          <li><a href="#!">Blog</a></li>
		          <li><a href="#!">Contact Us</a></li>
		        </ul>
		    </div>
		    <div class="footer_menu hover-underline-animation">
		        <ul class="list-unstyled">
		          <li><a href="#!">Home</a></li>
		          <li><a href="#!">About Us</a></li>
		          <li><a href="#!">Service</a></li>
		          <li><a href="#!">Gift Card</a></li>
		          <li><a href="#!">Salon</a></li>
		          <li><a href="#!">Spa</a></li>
		          <li><a href="#!">Blog</a></li>
		          <li><a href="#!">Contact Us</a></li>
		        </ul>
		    </div>
	    </div>
      </div>

      <div class="col-md-4 mx-auto footer_3">
      	<div class="footer_item_wrap">
      		<div class="image-wrap">
		        <img src="images/logo-foot.png" />
		    </div>
		    <div class="caption-wrap">
		      	<div class="discription">
		      		Lorem ipsum dolor sit amet, onsectetur adipiscing elit. Morbi at egestas magna. Eget lectus molestie, pharetra sem sed, scelerisque felis. mi ut, ullamcorper nibh.  mauris venenatis eu.
		      	</div>
		      	<h3>Get Download</h3>
		      	<div class="dowload_buttons">
		      		<a href="#!" class="btn android">&nbsp;</a><a href="#!" class="btn ios">&nbsp;</a>
		      	</div>
		    </div>
      	</div>
    </div>
  </div>
</div>

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">
  	<div class="container">
  		<div class="row">
	  		<div class="copy_right_item col-md-4">© 2020 Beutics. All Right Reserved.. </div>
	  		<div class="copy_right_item col-md-4" style="display: flex; align-items: center;">
				  <div class="col-md-6" style="padding: 0;">
					<a href="index.html">Terms & Conditions</a>
				  </div>
				  <div class="col-md-6" style="padding: 0;">
					  <a href="index.html"><span>|</span>Privacy Policy</a>
				  </div>
			  </div>
	  		<div class="copy_right_item col-md-4">
	  			<ul class="social_icons">
	  				<li class="icons"><a href="#!"><i class="fab fa-twitter"></i></a></li>
	  				<li class="icons"><a href="#!"><i class="fab fa-facebook-f"></i></a></li>
	  				<li class="icons"><a href="#!"><i class="fab fa-google-plus-g"></i></a></li>
	  				<li class="icons"><a href="#!"><i class="fab fa-youtube"></i></a></li>
	  				<li class="icons"><a href="#!"><i class="fab fa-linkedin"></i></a></li>
	  			</ul>
	  		</div>
  		</div>
  	</div>
  </div>
  <!-- Copyright -->

</footer>