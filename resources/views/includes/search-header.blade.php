<header class="header">
	<nav class="navbar navbar-expand-md navbar-dark fixed-top top_menu_bar main_menu" id="header_main">
		<div class="container">
		<div class="main_menu_wrapper">
			<div class="brand_wrapper">
		  <!-- Brand -->
		  <a class="navbar-brand" href="/"><img src="images/logo.jpg" alt="Beutics"></a>
		  	</div>
		  <!-- Main Menu Wrap -->

		  <div class="menu_wrapper">
		<div class="main_category_menu_wrap">
			<div class="navbar-nav category_menu" id="category_menu">
		    <ul class="navbar-nav">
		      <li class="nav-item">
		        <a class="nav-link" href="beauty">
		        	<span class="menu_item_icon beauty">&nbsp;</span>
					<div class="menu_item_text">
						<div class="whiteText">Beauty</div>
					</div>
				</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="fitness">
		        	<span class="menu_item_icon fitness">&nbsp;</span>
					<div class="menu_item_text">
						<div class="whiteText">Fitness</div>
					</div>
				</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="wellness">
		        	<span class="menu_item_icon wellness">&nbsp;</span>
					<div class="menu_item_text">
						<div class="whiteText">Wellness</div>
					</div>
				</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="offers">
		        	<span class="menu_item_icon offers">&nbsp;</span>
					<div class="menu_item_text">
						<div class="whiteText">Offers</div>
					</div>
				</a>
		      </li>
		      
         <!-- Dropdown -->
		    <li class="nav-item dropdown more">
		      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
		        <span class="menu_item_icon more">&nbsp;</span>
					<div class="menu_item_text">
						<div class="whiteText">More</div>
					</div>
				<span class="menu_item_icon drop_arrow">&nbsp;</span>
		      </a>
		      <div class="dropdown-menu">
		        <a class="dropdown-item" href="#">Demo Category</a>
		        <a class="dropdown-item" href="#">Demo Category</a>
		        <a class="dropdown-item" href="#">Demo Category</a>
		        <a class="dropdown-item" href="#">Demo Category</a>
		        <a class="dropdown-item" href="#">Demo Category</a>
		        <a class="dropdown-item" href="#">Demo Category</a>
		        <a class="dropdown-item" href="#">Demo Category</a>
		        <a class="dropdown-item" href="#">Demo Category</a>
		        <a class="dropdown-item" href="#">Demo Category</a>
		      </div>
		    </li>
		    </ul>
		  </div>
		</div>
		<!-- Main Menu Wrap End-->
		<!-- Search module start -->
		<div class="search_section_module">
			<ul class="nav nav-tabs search_types" role="tablist">
			  <li class="nav-item button-dropdown">
			    <a class="nav-link active find_store" href="#find_store" role="tab" data-toggle="tab">Find A Store</a>
			  </li>
			  <li class="nav-item">
			    <a class="nav-link find_store" href="#single_service" role="tab" data-toggle="tab">Single Service</a>
			  </li>
			  <li class="nav-item">
			    <a class="nav-link find_store" href="#multi_service" role="tab" data-toggle="tab">MultiServices</a>
			  </li>
			</ul>

			<!-- Tab panes -->
			<div class="tab-content close-class main_search_wrapper">
			  <div role="tabpanel" class="find_store_service tab-pane in active" id="find_store">
			  	<div class="beutics_search_wrap">
			  		  <div class="search-container">
					    <form action="/action_page.php">
					      <input type="text" placeholder="Search Store |" name="search">
					      <button type="submit"><i class="fal fa-search"></i></button>
					    </form>
					  </div>
			  	</div>
			  </div>
			  <div role="tabpanel" class="single_service_search tab-pane fade" id="single_service">
			  	<div class="beutics_search_wrap">
			  		  <div class="search-container">
					    <form action="/action_page.php">
					    	<div class="optional_form_wrapper">
						    	<div class="filter_wrap service_on_wrap">
							      	<div class="form-check-inline">
									  <label class="form-check-label">
									    <input type="radio" class="form-check-input" name="optradio" value="option1">At Home
									  </label>
									</div>
									<div class="form-check-inline">
									  <label class="form-check-label">
									    <input type="radio" class="form-check-input" name="optradio" value="option2">Beauty
									  </label>
									</div>
									<div class="form-check-inline">
									  <label class="form-check-label">
									    <input type="radio" class="form-check-input" name="optradio" value="option3">Clinical Beauty
									  </label>
									</div>
							    </div>
							    <div class="filter_wrap service_gender_wrap">
							    	<div class="form-group">
									  <select class="form-control">
									    <option value="" selected disabled>Gender</option>
									    <option value="">Female</option>
	    								<option value="">Male</option>
									  </select>
									</div>
							    </div>
							    <div class="filter_wrap service_locality_wrap">
							    	<div class="form-group">
									  <select class="form-control">
									    <option value="" selected disabled>Locality</option>
									    <option value="">1</option>
	    								<option value="">2</option>
	    								<option value="">3</option>
									  </select>
									</div>
							    </div>
							    <div class="filter_wrap service_search_wrap">
							      <input type="text" placeholder="Search Store |" name="search">
							      <button type="submit"><i class="fal fa-search"></i></button>
							    </div>
					      	</div>
					    </form>
					  </div>
			  	</div>
			  </div>
			  <div role="tabpanel" class="multi_service_search tab-pane fade" id="multi_service">
		  		<div class="beutics_search_wrap">
		  		  <div class="search-container">
				    <form action="/action_page.php">
				    	<div class="optional_form_wrapper">
					    	<div class="filter_wrap service_on_wrap">
						      	<div class="form-check-inline">
								  <label class="form-check-label">
								    <input type="radio" class="form-check-input" name="optradio" value="option1">At Home
								  </label>
								</div>
								<div class="form-check-inline">
								  <label class="form-check-label">
								    <input type="radio" class="form-check-input" name="optradio" value="option2">Beauty
								  </label>
								</div>
								<div class="form-check-inline">
								  <label class="form-check-label">
								    <input type="radio" class="form-check-input" name="optradio" value="option3">Clinical Beauty
								  </label>
								</div>
						    </div>
						    <div class="filter_wrap service_gender_wrap">
						    	<div class="form-group">
								  <select class="form-control">
								    <option value="" selected disabled>Gender</option>
								    <option value="">Female</option>
    								<option value="">Male</option>
								  </select>
								</div>
						    </div>
						    <div class="filter_wrap service_locality_wrap">
						    	<div class="form-group">
								  <select class="form-control">
								    <option value="" selected disabled>Locality</option>
								    <option value="">1</option>
    								<option value="">2</option>
    								<option value="">3</option>
								  </select>
								</div>
						    </div>
						    <div class="filter_wrap service_search_wrap">
						      <input type="text" placeholder="Search Store |" name="search">
						      <button type="submit"><i class="fal fa-search"></i></button>
						    </div>
				      	</div>
				    </form>
				  </div>
		  	</div>
		  	<div class="beutics_search_multiservices">
		  		<div class="beutics_trending_search">
		  			<div class="multi_service_search_headers">
		  				<h3>Trending Search</h3>
		  			</div>
		  			<div class="beutics_trending_search_wrap">
		  				<div class="trending_search_item active_item">
	  						<a class="item_link" href="#">
						        	<span class="item_icon"><i class="fal fa-tooth"></i></span>
									<div class="item_text">
										Dentistry
									</div>
							</a>
		  				</div>
		  				<div class="trending_search_item">
	  						<a class="item_link" href="#">
						        	<span class="item_icon"><i class="fal fa-tooth"></i></span>
									<div class="item_text">
										Dermatology
									</div>
							</a>
		  				</div>
		  				<div class="trending_search_item">
	  						<a class="item_link" href="#">
						        	<span class="item_icon"><i class="fal fa-tooth"></i></span>
									<div class="item_text">
										Plastic Surgery
									</div>
							</a>
		  				</div>
		  				<div class="trending_search_item">
	  						<a class="item_link" href="#">
						        	<span class="item_icon"><i class="fal fa-tooth"></i></span>
									<div class="item_text">
										Laser Removal
									</div>
							</a>
		  				</div>
		  				<div class="trending_search_item">
	  						<a class="item_link" href="#">
						        	<span class="item_icon"><i class="fal fa-tooth"></i></span>
									<div class="item_text">
										Fat Freezing
									</div>
							</a>
		  				</div>
		  				<div class="trending_search_item">
	  						<a class="item_link" href="#">
						        	<span class="item_icon"><i class="fal fa-tooth"></i></span>
									<div class="item_text">
										Dentistry
									</div>
							</a>
		  				</div>
		  				<div class="trending_search_item">
	  						<a class="item_link" href="#">
						        	<span class="item_icon"><i class="fal fa-tooth"></i></span>
									<div class="item_text">
										Dermatology
									</div>
							</a>
		  				</div>
		  				<div class="trending_search_item">
	  						<a class="item_link" href="#">
						        	<span class="item_icon"><i class="fal fa-tooth"></i></span>
									<div class="item_text">
										Plastic Surgery
									</div>
							</a>
		  				</div>
		  				<div class="trending_search_item">
	  						<a class="item_link" href="#">
						        	<span class="item_icon"><i class="fal fa-tooth"></i></span>
									<div class="item_text">
										Laser Removal
									</div>
							</a>
		  				</div>
		  				<div class="trending_search_item">
	  						<a class="item_link" href="#">
						        	<span class="item_icon"><i class="fal fa-tooth"></i></span>
									<div class="item_text">
										Fat Freezing
									</div>
							</a>
		  				</div>
		  				<div class="trending_search_item">
	  						<a class="item_link" href="#">
						        	<span class="item_icon"><i class="fal fa-tooth"></i></span>
									<div class="item_text">
										Fat Freezing
									</div>
							</a>
		  				</div>
		  			</div>
		  		</div>
		  		<div class="beutics_category_services">
		  			<div class="multi_service_search_headers">
		  				<h3>Dentistry</h3>
		  			</div>
		  			<div class="beutics_trending_search_wrap beutics_category_services_wrap">
					    <div class="form-check category_service">
					      <label class="form-check-label" for="check1">
					        <input type="checkbox" class="form-check-input" id="check1" name="option1" value="something" checked>Teeth Whithening (Laser)
					      </label>
					    </div>
					    <div class="form-check category_service">
					      <label class="form-check-label" for="check2">
					        <input type="checkbox" class="form-check-input" id="check2" name="option2" value="something">Teeth Scaling & Polishing (Deep)
					      </label>
					    </div>
					    <div class="form-check category_service">
					      <label class="form-check-label" for="check2">
					        <input type="checkbox" class="form-check-input" id="check3" name="option3" value="something">Root Canal Tooth
					      </label>
					    </div>
					    <div class="form-check category_service">
					      <label class="form-check-label" for="check1">
					        <input type="checkbox" class="form-check-input" id="check4" name="option4" value="something" checked>Deep Tooth Filling
					      </label>
					    </div>
					    <div class="form-check category_service">
					      <label class="form-check-label" for="check2">
					        <input type="checkbox" class="form-check-input" id="check5" name="option5" value="something">AirFlow Stain Removal
					      </label>
					    </div>
					    <div class="form-check category_service">
					      <label class="form-check-label" for="check2">
					        <input type="checkbox" class="form-check-input" id="check6" name="option6" value="something">Teeth Braces
					      </label>
					    </div>
					    <div class="form-check category_service">
					      <label class="form-check-label" for="check1">
					        <input type="checkbox" class="form-check-input" id="check7" name="option6" value="something" checked>Teeth Implant
					      </label>
					    </div>
					    <div class="form-check category_service">
					      <label class="form-check-label" for="check2">
					        <input type="checkbox" class="form-check-input" id="check8" name="option7" value="something">Teeth Whithening (Laser)
					      </label>
					    </div>
					    <div class="form-check category_service">
					      <label class="form-check-label" for="check2">
					        <input type="checkbox" class="form-check-input" id="check9" name="option8" value="something">Teeth Scaling & Polishing (Deep)
					      </label>
					    </div>
					    <div class="form-check category_service">
					      <label class="form-check-label" for="check2">
					        <input type="checkbox" class="form-check-input" id="check9" name="option8" value="something">Teeth Scaling & Polishing (Deep)
					      </label>
					    </div>
					    <div class="form-check category_service">
					      <label class="form-check-label" for="check1">
					        <input type="checkbox" class="form-check-input" id="check7" name="option6" value="something" checked>Teeth Implant
					      </label>
					    </div>
					    <div class="form-check category_service">
					      <label class="form-check-label" for="check2">
					        <input type="checkbox" class="form-check-input" id="check8" name="option7" value="something">Teeth Whithening (Laser)
					      </label>
					    </div>
					    <div class="form-check category_service">
					      <label class="form-check-label" for="check2">
					        <input type="checkbox" class="form-check-input" id="check9" name="option8" value="something">Teeth Scaling & Polishing (Deep)
					      </label>
					    </div>
					    <div class="form-check category_service">
					      <label class="form-check-label" for="check2">
					        <input type="checkbox" class="form-check-input" id="check9" name="option8" value="something">Teeth Scaling & Polishing (Deep)
					      </label>
					    </div>
		  			</div>
		  		</div>
		  	</div>
			  </div>
			</div>
		</div>
<!-- Search module end -->
		  <!-- Top Menu Wrap -->
		  <div class="top_nav_bar_wrap">
		  <!-- Toggler/collapsibe Button -->
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
		    <span class="navbar-toggler-icon"></span>
		  </button>

		  <!-- Navbar links -->
		  <div class="collapse navbar-collapse" id="collapsibleNavbar">
		    <ul class="navbar-nav ml-auto">
		      <li class="nav-item">
		        <a class="nav-link" href="#">
		        	<span class="menu_item_icon earn">&nbsp;</span>
					<div class="menu_item_text">
						<div class="whiteText">Refer & Earn</div>
					</div>
				</a>
		      </li>
		     
		      <li class="nav-item">
		        <a class="nav-link" href="#">
		        	<span class="menu_item_icon support">&nbsp;</span>
					<div class="menu_item_text">
						<div class="whiteText">24x7 Support</div>
					</div>
				</a>
		      </li>
			  <li class="nav-item">
		        <a class="nav-link" href="#">
		        	<span class="menu_item_icon wallet">&nbsp;</span>
					<div class="menu_item_text">
						<div class="whiteText">Earn AED 10</div>
					</div>
				</a>
		      </li>
	
		    </ul>
		  </div>
		  	<div class="user_menu">
			  	<nav class="navbar">
				<!-- Links -->
				  <ul class="navbar-nav">
				    <!-- <li class="nav-item user_login_regis">
				        <a class="nav-link" href="#">
				        	<span class="menu_item_icon usericon">&nbsp;</span>
							<div class="menu_item_text">
								<div class="whiteText">Login or Register</div>
							</div>
						</a>
				      </li> -->
				       <!-- Dropdown -->
				    <li class="nav-item dropdown">
				      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
				        <span class="menu_item_icon flag">&nbsp;</span>
							<div class="menu_item_text">
								<div class="whiteText">IN</div>
							</div>
						<span class="menu_item_icon drop_arrow">&nbsp;</span>
				      </a>
				      <div class="dropdown-menu">
				        <a class="dropdown-item" href="#">IN</a>
				        <a class="dropdown-item" href="#">DEMO</a>
				        <a class="dropdown-item" href="#">DEMO</a>
				      </div>
				    </li>
				    <li class="nav-item get_started_btn">
				      <a class="btn btn-danger" href="##">Get Started</a>
				    </li>
				  </ul>
				</nav>
			</div>
		</div>
		<!-- Top Menu Wrap end -->
	</div>
		</div>
		</div>
	</nav>
</header>