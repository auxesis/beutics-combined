<header class="header">
	<nav class="navbar navbar-expand-md navbar-dark fixed-top top_menu_bar main_menu" id="header_main">
		<div class="container">
		<div class="main_menu_wrapper">
			<div class="brand_wrapper">
		  <!-- Brand -->
		  <a class="navbar-brand" href="/"><img src="images/logo.jpg" alt="Beutics"></a>
		  	</div>
		  <!-- Main Menu Wrap -->

		  <div class="menu_wrapper">
		<div class="main_category_menu_wrap">
			<div class="navbar-nav category_menu" id="category_menu">
		    <ul class="navbar-nav">
		      <li class="nav-item">
		        <a class="nav-link" href="beauty">
		        	<span class="menu_item_icon beauty">&nbsp;</span>
					<div class="menu_item_text">
						<div class="whiteText">Beauty</div>
					</div>
				</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="fitness">
		        	<span class="menu_item_icon fitness">&nbsp;</span>
					<div class="menu_item_text">
						<div class="whiteText">Fitness</div>
					</div>
				</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="wellness">
		        	<span class="menu_item_icon wellness">&nbsp;</span>
					<div class="menu_item_text">
						<div class="whiteText">Wellness</div>
					</div>
				</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="offers">
		        	<span class="menu_item_icon offers">&nbsp;</span>
					<div class="menu_item_text">
						<div class="whiteText">Offers</div>
					</div>
				</a>
			</li>
		      
         <!-- Dropdown -->
		    <li class="nav-item dropdown more">
		      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
		        <span class="menu_item_icon more">&nbsp;</span>
					<div class="menu_item_text">
						<div class="whiteText">More</div>
					</div>
				<span class="menu_item_icon drop_arrow">&nbsp;</span>
		      </a>
		      <div class="dropdown-menu">
		        <a class="dropdown-item" href="#">Demo Category</a>
		        <a class="dropdown-item" href="#">Demo Category</a>
		        <a class="dropdown-item" href="#">Demo Category</a>
		        <a class="dropdown-item" href="#">Demo Category</a>
		        <a class="dropdown-item" href="#">Demo Category</a>
		        <a class="dropdown-item" href="#">Demo Category</a>
		        <a class="dropdown-item" href="#">Demo Category</a>
		        <a class="dropdown-item" href="#">Demo Category</a>
		        <a class="dropdown-item" href="login">Demo Category</a>
		      </div>
		    </li>
		    </ul>
		  </div>
		</div>
		<!-- Main Menu Wrap End-->
		<!-- Search module start -->
	
<!-- Search module end -->
		  <!-- Top Menu Wrap -->
		  <div class="top_nav_bar_wrap">
		  <!-- Toggler/collapsibe Button -->
		  <button class="navbar-toggler custom_responsive" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
		    <span class="navbar-toggler-icon"></span>
		  </button>

		  <!-- Navbar links -->
		  <div class="collapse navbar-collapse" id="collapsibleNavbar">
		    <ul class="navbar-nav ml-auto">
		      <li class="nav-item">
		        <a class="nav-link" href="#">
		        	<span class="menu_item_icon earn">&nbsp;</span>
					<div class="menu_item_text">
						<div class="whiteText">Refer & Earn</div>
					</div>
				</a>
		      </li>
			  <li class="nav-item">
		        <a class="nav-link" href="#">
		        	<span class="menu_item_icon support">&nbsp;</span>
					<div class="menu_item_text">
						<div class="whiteText">24x7 Support</div>
					</div>
				</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="#">
		        	<span class="menu_item_icon wallet">&nbsp;</span>
					<div class="menu_item_text">
						<div class="whiteText">Earn AED 10</div>
					</div>
				</a>
		      </li>
		    
	
		    </ul>
		  </div>
		  	<div class="user_menu">
			  	<nav class="navbar">
				<!-- Links -->
				  <ul class="navbar-nav">
				    <!-- <li class="nav-item user_login_regis">
				        <a class="nav-link" href="#">
				        	<span class="menu_item_icon usericon">&nbsp;</span>
							<div class="menu_item_text">
								<div class="whiteText">Login or Register</div>
							</div>
						</a>
				      </li> -->
				       <!-- Dropdown -->
				    <li class="nav-item dropdown">
				      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
				        <span class="menu_item_icon flag">&nbsp;</span>
							<div class="menu_item_text">
								<div class="whiteText">IN</div>
							</div>
						<span class="menu_item_icon drop_arrow">&nbsp;</span>
				      </a>
				      <div class="dropdown-menu">
				        <a class="dropdown-item" href="#">IN</a>
				        <a class="dropdown-item" href="#">DEMO</a>
				        <a class="dropdown-item" href="#">DEMO</a>
				      </div>
				    </li>
				    <li class="nav-item get_started_btn">
				      <a class="btn btn-danger" href="##">Get Started</a>
				    </li>
				  </ul>
				</nav>
			</div>
		</div>
		<!-- Top Menu Wrap end -->
	</div>
		</div>
		</div>
	</nav>
</header>
@show