@extends('layouts.spadmin.admin')

@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/jquery.gritter/css/jquery.gritter.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/themes/default/css/uniform.default.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/bootstrap/css/bootstrap-select.min.css" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>{{$title}}</h5>
          

            </header>
             @include('message')
            <div id="collapse2" class="body">
                <!-- <form class="form-horizontal" id="popup-validation"> -->
                {!! Form::open(['route' => 'new-services.storemultiple', 'method' => 'POST', 'class' => 'form-horizontal','id' => 'popup-validation','enctype' => 'multipart/form-data']) !!}
                {{ csrf_field() }}

                    <div class="form-group">
                        {{Form::label('', __('messages.Sub Category Name'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::select('sub_category[]', $subcat_arr,null,['class' => 'form-control validate[required] myselect','id'=>'sub_category','data-live-search'=>"true", 'multiple'=>'multiple']) }}
                        </div>
                    </div>
                    <div class="form-group">
                      {{Form::label('service_type_id', __('messages.Fitness Type'), ['class' => 'control-label col-lg-4'])}}
                      <div class="col-lg-4">
                          {{ Form::select('service_type_id', $fitnesstype_arr,null,['class' => 'form-control selectpicker','placeholder'=>__('messages.Select Type'),'id'=>'fitness_type_id']) }}
                      </div>
                    </div>
                    <div class="form-group" style="display: none" id="fitness_service_title_div">
                      {{Form::label('service_title_id', __('messages.Fitness Title'), ['class' => 'control-label col-lg-4'])}}
                      <div class="col-lg-4" id="fitness_title_div">
                          {{ Form::select('service_title_id', [],null,['class' => 'form-control selectpicker validate[required]','placeholder'=>__('messages.Select Title'),'id'=>'fitness_title_id']) }}
                      </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('service_id', __('messages.Service Name'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4" id="service_div">
                            {{ Form::select('service_id[]', [],null,['class' => 'form-control validate[required] myselect','id'=>'services', 'multiple'=>'multiple']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('sub_service_id', __('messages.Sub Service Name'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4" id="sub_service_div">
                            {{ Form::select('sub_service_id[]', [],null,['class' => 'form-control myselect','id'=>'sub_services', 'multiple'=>'multiple']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('description', 'Description', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::textarea('description','', ['class' => 'form-control validate[required]','rows'=>'2']) }}
                        </div>
                    </div>
                    <div id="further_data">
                    </div>
                    <span id="fieldAlert" style="color:red;margin-left:335px;"></span>
                    <div class="form-actions no-margin-bottom" >

                    <div class="form-actions no-margin-bottom" >
                        <input value="{{ __('messages.Submit') }}" class="btn btn-primary"  id="submitBtn" style="width:80px;" autocomplete="off">
                    </div>  

                    <br><br><br><br>
                </form>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
@endsection
@section('uniquescript')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>

     <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/holder/2.4.1/holder.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/jquery.uniform.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>

    <script src="{{ url('/') }}/assets/lib/bootstrap/js/bootstrap-select.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
    <script>
        $(function() {
          Metis.formValidation();
        });
    </script>
    <script type="text/javascript">
      $(".myselect").select2();
    </script>
    <script src="{{ url('/') }}/assets/js/jquery.validate.min.js"></script>
    <script>$(".validate").validate();</script>


    <script type="text/javascript">
        $("#sub_category").change(function()
        {
            var id = $(this).val();
            
            jQuery.ajax({
            url: '{{route('ajax.get.newservices')}}',
            type: 'POST',
            data:{subcat_id:id},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
              $("#service_div").html(response);
              
            	}
            });
        });

        $("body").on('change', '#services', function()
        //$("#services").change(function()
        { 
            var id = $(this).val();
            
            jQuery.ajax({
            url: '{{route('ajax.get.newsubservices')}}',
            type: 'POST',
            data:{service_id:id},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
              $("#sub_service_div").html(response);
              $("#description").val($('#service_desc').val());
              }
            });
              
        });

        $("body").on('change', '#sub_services', function()
        { 
            var id = $(this).val();
            
            jQuery.ajax({
            url: '{{route('ajax.get.newsubservicesinfo')}}',
            type: 'POST',
            data:{service_id:id},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                if(response == '')
                    $("#description").val($('#service_desc').val());
                else
                    $("#description").val(response);
              }
            });
              
        });
        
        $("#fitness_type_id").change(function()
        {
            var id = $(this).val();
            
            jQuery.ajax({
            url: '{{route('ajax.get.newservicesfitnesstype')}}',
            type: 'POST',
            data:{fitness_type_id:id},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {

              $("#fitness_service_title_div").show();
              $("#fitness_title_div").html(response);
              
              }
            });
        });

        $('#submitBtn').click(function(){
         if($("#is_men").is(':checked') || $("#is_women").is(':checked') || $("#is_kids").is(':checked')){
               return true;
         }else{
            $("#fieldAlert").html('<?php echo __('messages.Please select all fields values'); ?>');
             // return false;
             e.preventDefault();
             return false;
           
         }
        });

    </script>

@endsection