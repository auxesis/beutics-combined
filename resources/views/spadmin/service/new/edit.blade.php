@extends('layouts.spadmin.admin')

@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/jquery.gritter/css/jquery.gritter.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/themes/default/css/uniform.default.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/bootstrap/css/bootstrap-select.min.css" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
  <style>
    .hourbox {
        margin: 10px 10px;float: left;
    }
    .hourbox .control-label {
        font-weight: bold;
    }
    .hourbox .hhbox {
        display: inline-block;vertical-align: middle;padding: 0 5px;
    }
    .quantity input {
      width: 100px;height: 42px;line-height: 1.65;float: left;display: block; padding: 0;margin: 0;padding-left: 20px;border: 1px solid #eee;
    }
    .quantity-nav {
      float: left;position: relative;height: 42px;
    }
    .quantity-button.quantity-up {
      position: absolute;height: 50%;top: 0;border: 1px solid #eee;
    }
    .quantity-button.quantity-down {
      position: absolute;bottom: 0px; height: 50%; width: 11px;border: 1px solid #eee;
    }
    .select2-results__option .checkbox {
      margin : 0px;
    }
    .select2-results__option .checkbox input[type="checkbox"] {
      margin : 4px 0px;
    }
  </style>
@endsection

@section('content')
<div class="row">
  <div class="col-lg-12">
    <div class="box">
      <header class="dark">
          <div class="icons"><i class="fa fa-check"></i></div>
          <h5>{{$title}}</h5>
      </header>
      @include('message')
      <div id="collapse2" class="body">
        <!-- <form class="form-horizontal" id="popup-validation"> -->
        {!! Form::model($rows,['route' => ['new-services.update',$rows->id], 'method' => 'PATCH', 'class' => 'form-horizontal','id' => 'popup-validation','enctype' => 'multipart/form-data']) !!}
        {{ csrf_field() }}
        <div class="form-group">
            {{Form::label('', __('messages.Sub Category Name'), ['class' => 'control-label col-lg-4'])}}
            <div class="col-lg-4">
               {{ Form::text('',$catName, ['class' => 'form-control','readonly']) }}
            </div>
        </div>
        <div class="form-group">
            {{Form::label('service_id', __('messages.Service Name'), ['class' => 'control-label col-lg-4'])}}
            <div class="col-lg-4">
               {{ Form::text('',$serviceName, ['class' => 'form-control','readonly']) }}
            </div>
        </div>
        <div class="form-group">
            {{Form::label('service_id', __('messages.Sub Service Name'), ['class' => 'control-label col-lg-4'])}}
            <div class="col-lg-4">
               {{ Form::text('',$subServiceName, ['class' => 'form-control','readonly']) }}
            </div>
        </div>
        <div class="form-group">
              {{Form::label('description', 'Description', ['class' => 'control-label col-lg-4'])}}
              <div class="col-lg-4">
                  {{ Form::textarea('description',$rows->description, ['class' => 'form-control validate[required]','rows'=>'2']) }}
              </div>
        </div>
        <div class="form-group">
          {{Form::label('', __('messages.Gender'), ['class' => 'control-($rows->getAssociatedSubService !=null) ? label col-lg-4'])}}
          <div class="col-lg-5">
            <?php
            $service_criteria = explode(',',$rows->getAssociatedUsername->service_criteria);
            $provider_criteria = array('store'=>'Store Services','home'=>'Home Services','online'=>'Online Services');
            ?>
            @foreach($service_criteria as $sc)
            <?php
              $genderkey = 'is'.ucfirst($sc);
              $check = in_array($genderkey,$selected_gender_arr)?'checked':'';
            ?>
            <div class="checkboxg">
              <label>
                <input type="checkbox" name="is{{ucfirst($sc)}}" id="is_{{$sc}}" onclick="getPriceDiv('<?= $sc."_price";?>',this)" value="true" class="myClass" <?php echo $check;?>> {{ucwords($sc)}}
              </label>
            </div>
            <div class="row">
              <?php 
                $display = $check =='checked'?'display:block;':'display:none;';
              ?>
              <div class="col-lg-12" style="<?= $display;?>margin: 0px 15px;" id="{{$sc}}_price">
              @foreach($provider_criteria as $key => $name)
                @if(in_array($key,$delivery_mode))
                  <?php
                    $servicekey = 'is'.ucfirst($sc).'_'.$key.'_service';
                    $check = in_array($servicekey,$selected_gender_arr)?'checked':'';
                  ?>
                  <label>
                    @if($key == 'home')
                      <input type="checkbox" name="is{{ucfirst($sc)}}_{{$key}}_service" onclick="checkHomeService('<?= $sc."_".$key."_service_section";?>',this)" value="true" class="myClass" id="is_{{$sc}}_{{$key}}" <?php echo $check;?>> {{ __('messages.'.$name) }}
                    @else
                      <input type="checkbox" name="is{{ucfirst($sc)}}_{{$key}}_service" onclick="getPriceDiv('<?= $sc."_".$key."_service_section";?>',this)" value="true" class="myClass" id="is_{{$sc}}_{{$key}}" <?php echo $check;?>> {{ __('messages.'.$name) }}
                    @endif
                  </label>
                  <div class="row">
                    <?php 
                      $display = $check =='checked'?'display:block;':'display:none';
                    ?>
                    <div class="col-lg-12" style="<?= $display;?>" id="{{$sc}}_{{$key}}_service_section">
                        @php
                          $mode_of_trainer = isset($selected_trainer_arr[$sc.'_'.$key.'_individual_trainer']) ? $selected_trainer_arr[$sc.'_'.$key.'_individual_trainer'] : array();
                        @endphp
                        <div class="col-lg-12" class="service_trainer_div">
                          {{
                            Form::select('is'.ucfirst($sc).'_'.$key.'_service_trainer[]', $trainer_arr,$mode_of_trainer,['class' => 'myservicetrainer form-control','id'=>$sc.'_'.$key.'_service_trainer', 'multiple'=>'multiple']) 
                          }}
                        </div>
                        <div class="col-lg-12" id="service_div_section" style="margin: 6px 0px;">
                          @php
                            $keystyle = $sc.'_'.$key.'_'.'service_section_price';
                          @endphp
                          <select class="myselectservice form-control validate[required]" id="{{$keystyle}}" multiple="" name="is{{ucfirst($sc)}}_{{$key}}_service_attribute_id[]">
                            @foreach($attribute_arr as $stylekey => $styleval)
                              @php
                              $selected = (array_key_exists($servicekey,$single_service_prices_arr) && array_key_exists($stylekey,$single_service_prices_arr[$servicekey]))?'selected':'';
                                $name = str_replace(' ','_',strtolower($styleval));
                              @endphp
                              <option value="{{$stylekey}}" data-attrval="{{$name}}" id="is_{{$sc}}_{{$key}}_{{$name}}" <?= $selected ?>>{{$styleval}}</option>
                            @endforeach
                          </select>
                        </div>

                        @foreach($attribute_arr as $stylekey => $styleval)
                          @php
                            $style = str_replace(' ','_',strtolower($styleval));
                            if($key == 'store')
                            {
                              $price_key = $sc.'_originalprice';
                              $discount_key = $sc.'_discount';
                              $bestprice_key = $sc.'_bestprice';
                            }
                            else
                            {
                              $price_key = $sc.'_'.$key.'_originalprice';
                              $discount_key = $sc.'_'.$key.'_discount';
                              $bestprice_key = $sc.'_'.$key.'_bestprice';
                            }
                            if(array_key_exists($servicekey,$single_service_prices_arr) && array_key_exists($stylekey,$single_service_prices_arr[$servicekey]))
                            {
                              $display = 'display:block;';
                              $originalprice = $single_service_prices_arr[$servicekey][$stylekey][$price_key];
                              $discountprice = $single_service_prices_arr[$servicekey][$stylekey][$discount_key];
                              $bestprice = $single_service_prices_arr[$servicekey][$stylekey][$bestprice_key];
                              $desc = $single_service_prices_arr[$servicekey][$stylekey]['description'];
                              $time = explode(':',$single_service_prices_arr[$servicekey][$stylekey]['time']);
                            }
                            else
                            {
                              $display = 'display:none;';
                              $originalprice = $discountprice = $bestprice = $desc = '';
                              $time = array();
                            }
                            
                          @endphp
                          <div class="row" style="<?= $display ?> margin: 0px 0px 10px 0px;" id="{{$sc}}_{{$key}}_service_section_price_{{$style}}">
                            <h5 style="margin: 5px 15px;text-decoration: underline;">{{$styleval}}</h5>
                            <div class="col-lg-4">
                              {{ 
                                Form::text($style.'['.$price_key.']',$originalprice, ['class' => 'form-control '. $sc.'_'.$key.'_'.$style.'_price','placeholder'=>__('messages.Original Price'),'onkeypress'=>'return isNumber(event)','autocomplete'=>'off','id'=>$style.$sc.'_'.$key.'_originalprice','maxlength'=>'10','onkeyup'=>'calcAllValues("'. $style.$sc.'_'.$key.'",this)']) 
                              }}
                            </div>
                            <div class="col-lg-4">
                              {{ 
                                Form::text($style.'['.$discount_key.']',$discountprice, ['class' => 'form-control '. $sc.'_'.$key.'_'.$style.'_price','placeholder'=>__('messages.Discount %'),'onkeypress'=>'return isNumber(event)','autocomplete'=>'off','onkeyup'=>'calcBestPriceValues("'.$style.$sc.'_'.$key.'")','id'=>$style.$sc.'_'.$key.'_discount']) 
                              }}
                            </div>
                            <div class="col-lg-4">
                              {{ 
                                Form::text($style.'['.$bestprice_key.']',$bestprice, ['class' => 'form-control '. $sc.'_'.$key.'_'.$style.'_price','placeholder'=>__('messages.Best Price'),'onkeypress'=>'return isNumber(event)','autocomplete'=>'off','onkeyup'=>'calcDisValues("'.$style.$sc.'_'.$key.'")','id'=>$style.$sc.'_'.$key.'_bestprice','maxlength'=>'10']) 
                              }}
                            </div>
                            <div class="input-group  col-lg-12">
                              <div class="hourbox">
                                  <div class="hhbox control-label">Hours</div>
                                  <div class="hhbox quantity">
                                    <input type="number" name="{{$style}}[is{{ucfirst($sc)}}_{{$key}}_serviceTimeHour]" placeholder="Hours" value='<?= isset($time[0])?$time[0]:0; ?>' min="0" max="23" step="1" class="">
                                  </div>
                              </div>
                              <div class="hourbox">
                                <div class="hhbox control-label">Minutes</div>
                                <div class="hhbox quantity">
                                  <input type="number" name="{{$style}}[is{{ucfirst($sc)}}_{{$key}}_serviceTimeMin]" placeholder="Minutes" value='<?= isset($time[1])?$time[1]:0; ?>' min="0" max="59" step="1" class="">
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-12">
                             {{ Form::textarea($style.'[is'.ucfirst($sc).'_'.$key.'_'.'description'.']',$desc, ['class' => 'form-control','rows'=>'2', 'placeholder'=>'Description']) 
                              }}
                            </div>
                          </div>
                        @endforeach       
                    </div>
                  </div>
                @endif
              @endforeach
              <div style="margin: 0px 0px 0px 0px;"></div>
              <br/>
              </div>
            </div>
            @endforeach 
          </div>
        </div>
      </div>
      <!-- end body -->
      <div class="form-actions no-margin-bottom">
          <input value="{{ __('messages.Submit') }}" class="btn btn-primary"  id="submitBtn" style="width:80px;" autocomplete="off">
      </div>  
      {{Form::close()}}
      <br><br><br><br>
    </div>
  </div>
</div>
  <!-- /.col-lg-12 -->
</div>
@endsection
@section('uniquescript')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>

     <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/holder/2.4.1/holder.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/jquery.uniform.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>
    <script src="{{ url('/') }}/assets/lib/bootstrap/js/bootstrap-select.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
    <script type="text/javascript">
      $(document).ready(function() {
          let branch_all = [];
          
          function formatResult(state) {
              if (!state.id) {
                  var btn = $('<div class="text-right"><button id="all-branch" style="margin-right: 10px;" class="btn btn-default">Select All</button><button id="clear-branch" class="btn btn-default">Clear All</button></div>')
                  return btn;
              }
              
              branch_all.push(state.id);
              var id = 'state' + state.id;
              var checkbox = $('<div class="checkbox"><input id="'+id+'" type="checkbox" '+(state.selected ? 'checked': '')+'><label for="checkbox1">'+state.text+'</label></div>', { id: id });
              return checkbox;   
          }
          
          function arr_diff(a1, a2) {
              var a = [], diff = [];
              for (var i = 0; i < a1.length; i++) {
                  a[a1[i]] = true;
              }
              for (var i = 0; i < a2.length; i++) {
                  if (a[a2[i]]) {
                      delete a[a2[i]];
                  } else {
                      a[a2[i]] = true;
                  }
              }
              for (var k in a) {
                  diff.push(k);
              }
              return diff;
          }
          
          let optionSelect2 = {
              templateResult: formatResult,
              closeOnSelect: false,
              tags: true,
              closeOnSelect : false,
              allowHtml: true,
              allowClear: true,
              width: '100%'
          };
          
          let $select2 = $(".myselectservice").select2(optionSelect2);
          
          var scrollTop;
          
          $select2.on("select2:selecting", function( event ){
              var $pr = $( '#'+event.params.args.data._resultId ).parent();
              scrollTop = $pr.prop('scrollTop');
          });
          
          $select2.on("select2:select", function( event ){
              $(window).scroll();
              
              var $pr = $( '#'+event.params.data._resultId ).parent();
              $pr.prop('scrollTop', scrollTop );
              
              $(this).val().map(function(index) {
                  $("#state"+index).prop('checked', true);
              });
          });
          
          $select2.on("select2:unselecting", function ( event ) {
              var $pr = $( '#'+event.params.args.data._resultId ).parent();
              scrollTop = $pr.prop('scrollTop');
          });
          
          $select2.on("select2:unselect", function ( event ) {
              $(window).scroll();
              
              var $pr = $( '#'+event.params.data._resultId ).parent();
              $pr.prop('scrollTop', scrollTop );
              
              var branch  =   $(this).val() ? $(this).val() : [];
              var branch_diff = arr_diff(branch_all, branch);
              branch_diff.map(function(index) {
                  $("#state"+index).prop('checked', false);
              });
          });
          
          $(document).on("click", "#all-branch",function(){
              $(".myselectservice > option").not(':first').prop("selected", true);// Select All Options
              $(".myselectservice").trigger("change")
              $(".select2-results__option").not(':first').attr("aria-selected", true);
              $("[id^=state]").prop("checked", true);
              $(window).scroll();
          });
          
          $(document).on("click", "#clear-branch", function(){
              $(".myselectservice > option").not(':first').prop("selected", false);
              $(".myselectservice").trigger("change");
              $(".select2-results__option").not(':first').attr("aria-selected", false);
              $("[id^=state]").prop("checked", false);
              $(window).scroll();
          });

          $(document).on("change",".myselectservice", function(){
            var idname = $(this).attr('id');
            $("[id^="+idname+"_]").css('display','none');
            $('option:selected',this).each(function(){
              var name = $(this).attr("data-attrval");
              $("#"+idname+'_'+name).css('display','');
            });
          });
      });
    </script>
    <script>
      $(".myservicetrainer").select2({
          width: '100%',
          placeholder: "Select Trainer",
      });
      
      $(function() {
        Metis.formValidation();
      });
    </script>

    <script>
      function getPriceDiv(id,value)
      {
        if($(value).prop('checked')){
          $("#"+id).css('display','');
          $("#"+id+"_original").css('display','');
        }else{
          $("#"+id).css('display','none');
          $("#"+id+"_original").css('display','none');
        }
      }

      function checkHomeService(id,value)
      {
          var is_provide_home_service = '';
          if($(value).prop('checked')){
              $("#"+id).css('display','');

              is_provide_home_service = '1';
                  jQuery.ajax({
                      url: '{{route('ajax.check.home.service')}}',
                      type: 'POST',
                      data:{is_home_service:is_provide_home_service},
                      headers: {
                      'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                      },
                      success: function (response) {

                          if(response == 'show'){
                              $("#homeservicealert").modal('show');
                              $("#activenow").click(function(){
                                  $("#homeservicealert").modal('hide');
                                  $("#agreement").modal('show');
                              });

                              $("#submitAgreement").click(function(){

                                  var mov = $("#mov").val();
                                  var hs = $("#is_provide_home_service").val();
                                  if(mov!=''){
                                       jQuery.ajax({
                                          url: '{{route('home.service.active')}}',
                                          type: 'POST',
                                          data:{mov:mov,is_home_service:'1'},
                                          headers: {
                                          'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                                          },
                                          success: function (response) {
                                            if(response == 'success'){
                                              $("#agreement").modal('hide');
                                            }
                                           
                                          }
                                          });
                                  }
                                  
                              });
                          }
                        
                      }
                  });
          }else{
              $("#"+id).css('display','none');    
          } 
      }

      $("#submitBtn").click(function(){
        var is_form_submit = 0;
        $('.myClass').each(function (index, value) {
          var str = $(this).attr('id');//id of checkboxes
          var id = str.replace('is_','');

          if($("#"+str).is(":checked")){
            $("#"+id+'_section_price').each(function(){
              $('option:selected',this).each(function(){
                var attrid = $(this).attr('id');

                if(attrid != 'undefined' && $("#"+attrid).length)
                {
                  var id = attrid.replace('is_','');
                  $('.'+id+'_price').each(function (index, value) {               
                    var col_id = $(value).attr('id');
                    if($(value).val()!='')
                    {
                      $("#"+col_id).css("border-color", "green");
                    }
                    else{
                      $("#"+col_id).css("border-color", "red");
                      is_form_submit++;
                    }
                  });
                }
              });
            });
          }   
        });
        
        if(!is_form_submit){
          
          $("#popup-validation").submit();
        }
      });

      function isNumber(evt) {
          var charCode = (evt.which) ? evt.which : event.keyCode
          if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46){
              return false;
          }
        
          return true;
      }

      function calcBestPriceValues(gender)
      {
        var dis = $("#"+gender+"_discount").val();
        if(dis>100){
          $("#"+gender+"_discount").val("");
        }else{

          var total = $("#"+gender+"_originalprice").val();
          if(total!=''){
            var bestprice = total - (total*dis)/100;
              $("#"+gender+"_bestprice").val((bestprice).toFixed(3));
          }else{
            $("#"+gender+"_bestprice").val(0);
          }
          
        }
      }

      function calcDisValues(gender)
      {
        var best_price = $("#"+gender+"_bestprice").val();
         var total = $("#"+gender+"_originalprice").val();

          best_price = parseInt(best_price);
         total = parseInt(total);
        if(best_price>0 && best_price<total){
         
          if(total!=''){
            var bestprice_1 = total - best_price;
            bestprice_2 =(bestprice_1*100)/total;
              $("#"+gender+"_discount").val((bestprice_2).toFixed(3));
          }else{
            $("#"+gender+"_discount").val(0);
          }
          
        }else{
          $("#"+gender+"_bestprice").css("border-color", "red");
          $("#"+gender+"_bestprice").val('');
        }
      }

      function calcAllValues(gender,data)
      {
        // var total = $("#"+gender+"_originalprice").val();
        var total = data.value;
        var best_price = $("#"+gender+"_bestprice").val();
        
        var dis = $("#"+gender+"_discount").val();
   

          if(dis == '' || dis == 0 || dis == '0'){
            $("#"+gender+"_bestprice").val(total);
            $("#"+gender+"_discount").val('0')  ;
          }
          else{
            if(total>0 && dis>0){
              var bestprice = total - (total*dis)/100;
                $("#"+gender+"_bestprice").val((bestprice).toFixed(3));
            }else{
              $("#"+gender+"_bestprice").val(0);
            } 
          }
      }
    </script>
    <script type="text/javascript">
      $(document).ready(function(){
        jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div></div>').insertAfter('.quantity input');
          jQuery('.quantity').each(function() {
            var spinner = jQuery(this),
              input = spinner.find('input[type="number"]'),
              btnUp = spinner.find('.quantity-up'),
              btnDown = spinner.find('.quantity-down'),
              min = input.attr('min'),
              max = input.attr('max');

            btnUp.click(function() {
              var oldValue = parseFloat(input.val());
              if (oldValue >= max) {
                var newVal = oldValue;
              } else {
                var newVal = oldValue + 1;
              }
              spinner.find("input").val(newVal);
              spinner.find("input").trigger("change");
            });

            btnDown.click(function() {
              var oldValue = parseFloat(input.val());
              if (oldValue <= min) {
                var newVal = oldValue;
              } else {
                var newVal = oldValue - 1;
              }
              spinner.find("input").val(newVal);
              spinner.find("input").trigger("change");
            });

          });
      }); 
    </script>
@endsection

<!-- The Modal -->
<div class="modal" id="homeservicealert" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">{{ __('messages.Beutics Provider') }}</h4>
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        {{ __('messages.Please active Home Service and enter MOV from My Agreement section to add Home Service Prices.') }}
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="activenow">{{ __('messages.Active Now') }}</button>
        <!-- <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button> -->
      </div>

    </div>
  </div>
</div>

<div class="modal" id="agreement" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">{{ __('messages.Agreement') }}</h4>
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <center><a class="btn btn-danger" href="{{ asset('sp_uploads/approve_pdf/'.$rows->getAssociatedUsername->agreement) }}" target="_blank">{{ __('messages.View Agreement') }}</a></center><br/><br/>

        <div class="form-group">
            {{Form::label('', '', ['class' => 'control-label col-lg-4 '])}}
            <div class="col-lg-6">
                <input type="checkbox" id="is_provide_home_service"> {{ __('messages.Also Provide Home Services?') }}
            </div>
        </div>
        <div class="form-group">
            {{Form::label('mov', 'MOV', ['class' => 'control-label col-lg-4'])}}
            <div class="col-lg-6">
                {{ Form::text('','', ['class' => 'form-control','placeholder'=>__('messages.minimum order value'),'id'=>'mov']) }}
            </div>
        </div>
        
       
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <a class="btn btn-success" id="submitAgreement">{{ __('messages.Submit') }}</a>
        <!-- <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button> -->

      </div>

    </div>
  </div>
</div>