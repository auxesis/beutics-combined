@if($fitness_service_arr)
{{ Form::select('service_title_id', $fitness_service_arr,null,['class' => 'form-control selectpicker validate[required]','placeholder'=>__('messages.Select Title'),'id'=>'fitness_title_id']) }}
@else
{{ __('messages.No Fitness Services Found') }}
@endif