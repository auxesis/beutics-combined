@if($sub_service_arr)
{{ Form::select('sub_service_id[]',$sub_service_arr,null,['class' => 'form-control myselect','data-live-search'=>"true",'id'=>'sub_services','multiple'=>'multiple']) }}
@else
{{ __('messages.No Sub Services Found') }}
@endif

{{ Form::hidden('service_desc', $serviceDesc, ['id' => 'service_desc']) }}
<script type="text/javascript">
  $(".myselect").select2();
</script>