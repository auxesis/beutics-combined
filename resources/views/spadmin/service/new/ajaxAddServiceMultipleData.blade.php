<style>
	.hourbox {
	    margin: 10px 10px;float: left;
	}
	.hourbox .control-label {
	    font-weight: bold;
	}
	.hourbox .hhbox {
    	display: inline-block;vertical-align: middle;padding: 0 5px;
	}
	.quantity input {
		width: 100px;height: 42px;line-height: 1.65;float: left;display: block; padding: 0;margin: 0;padding-left: 20px;border: 1px solid #eee;
	}
	.quantity-nav {
		float: left;position: relative;height: 42px;
	}
	.quantity-button.quantity-up {
		position: absolute;height: 50%;top: 0;border: 1px solid #eee;
	}
	.quantity-button.quantity-down {
		position: absolute;bottom: 0px; height: 50%; width: 11px;border: 1px solid #eee;
	}
    .select2-results__option .checkbox {
    	margin : 0px;
    }
    .select2-results__option .checkbox input[type="checkbox"] {
    	margin : 4px 0px;
    }

</style>

<link rel="stylesheet" href="{{ url('/') }}/assets/lib/bootstrap/css/bootstrap-select.min.css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
@if($sp_row)

<div class="form-group">
  {{Form::label('', __('messages.Gender'), ['class' => 'control-label col-lg-4'])}}
    <div class="col-lg-5">
      <?php 
      	$service_criteria = explode(',',$sp_row->service_criteria);
      	$provider_criteria = array('store'=>'Store Services','home'=>'Home Services','online'=>'Online Services');
      	$service_level = array('beginner'=>'Beginner','expert'=>'Expert','intermidiate'=>'Intermidiate');
      	$service_sets = array('group'=>'Group','individual'=>'Individual');

      ?>
      @foreach($service_criteria as $sc)
        <div class="checkboxg">
	      <label>
	      	<input type="checkbox" name="is{{ucfirst($sc)}}" id="is_{{$sc}}" onclick="getPriceDiv('<?= $sc."_price";?>',this)" value="true" class="myClass"> {{ucwords($sc)}}
	      </label>
	    </div>
        <div class="row">
	      	<div class="col-lg-12" style="display:none;" id="{{$sc}}_price">
	      		<div id="{{$sc}}_goal_section">
				    <div class="col-lg-4">
	                    {{ 
	                    	Form::select($sc.'_level', $service_level,null,['class' => 'service_level form-control validate[required]','id'=>$sc.'_level', 'placeholder'=>__('messages.Select Level')]) 
	                	}}
				    </div>
				    <div class="col-lg-4">
	      				{{ 
	      					Form::text($sc.'_calories','', ['class' => 'form-control validate[required] '. $sc,'placeholder'=>__('messages.Calories'),'id'=>$sc.'service_calories','maxlength'=>'10']) 
	      				}}
	      			</div>
	      			<div class="col-lg-4">
	      				{{ 
	      					Form::text($sc.'_intensity','', ['class' => 'form-control validate[required] '. $sc,'placeholder'=>__('messages.Intensity %'),'onkeypress'=>'return isNumber(event)','id'=>$sc.'_service_intensity','maxlength'=>'10']) 
	      				}}
	      			</div>
	      		</div>
		        @foreach($provider_criteria as $key => $name)
		        @if(in_array($key,$delivery_mode))
		        <div style="margin: 0px 15px;">
			        <label>
			        	@if($key == 'home')
			      			<input type="checkbox" name="is{{ucfirst($sc)}}_{{$key}}_service" onclick="checkHomeService('<?= $sc."_".$key."_service_section";?>',this)" value="true" class="myClass" id="is_{{$sc}}_{{$key}}"> {{ __('messages.'.$name) }}
			      		@else
			      			<input type="checkbox" name="is{{ucfirst($sc)}}_{{$key}}_service" onclick="getPriceDiv('<?= $sc."_".$key."_service_section";?>',this)" value="true" class="myClass" id="is_{{$sc}}_{{$key}}"> {{ __('messages.'.$name) }}
			      		@endif
			      	</label>
			      	<div class="row">
				      	<div class="col-lg-12" style="display:none;" id="{{$sc}}_{{$key}}_service_section">
				      		<div class="col-lg-12">
	                           {{ 
	                           		Form::textarea($sc.'_'.$key.'_'.'description','', ['class' => 'form-control','rows'=>'2', 'maxlength' => '1000', 'placeholder'=>'Description']) 
	                           	}}
	                        </div>
				      		<div class="col-lg-12" id="is_{{$sc}}_service_category_div">
				      		@foreach($service_sets as $categorykey => $category)
				      			<div class="checkboxg">
						      		<label>
								      	<input type="checkbox" name="is{{ucfirst($sc)}}_{{$key}}_service_{{$categorykey}}" id="{{$sc}}_{{$key}}_price_{{$categorykey}}" value="true" class="myClass" onclick="getPriceDiv('<?= $sc."_".$key."_service_".$categorykey."_section";?>',this)"> {{$category}}
								    </label>
								</div>
								<div class="row" style="display:none;" id="{{$sc}}_{{$key}}_service_{{$categorykey}}_section">
					      			<div class="col-lg-12" class="service_trainer_div">
			                            {{
			                            	Form::select('is'.ucfirst($sc).'_'.$key.'_service_'.$categorykey.'_trainer[]', $trainer_arr,null,['class' => 'myservicetrainer form-control','id'=>$sc.'_'.$key.'_service_'.$categorykey.'_trainer', 'multiple'=>'multiple']) 
			                            }}
			                        </div>
						      		
			                        <div class="col-lg-12" class="service_div_section" style="margin: 6px 0px;"> 
				                        @php
				                        	$keystyle = $sc.'_'.$key.'_'.$categorykey.'_service_section_price';
				                        @endphp
				                        <select class="myselectservice form-control validate[required]" id="{{$keystyle}}" multiple="" name="is{{ucfirst($sc)}}_{{$key}}_service_{{$categorykey}}_attribute_id[]">
				                        	@foreach($attribute_arr as $stylekey => $styleval)
				                        	@php
				                        		$name = str_replace(' ','_',strtolower($styleval));
				                        	@endphp
				                        	<option value="{{$stylekey}}" data-attrval="{{$name}}" id="is_{{$sc}}_{{$key}}_{{$name}}">{{$styleval}}</option>
				                        	@endforeach
				                        </select>
			                        </div>

			                        @foreach($attribute_arr as $stylekey => $styleval)
						      		@php
						      			$style = str_replace(' ','_',strtolower($styleval));
						      			if($key == 'store')
						      			{
						      				$price_key = $categorykey.'['.$style.']['.$sc.'_originalprice]';
						      				$discount_key = $categorykey.'['.$style.']['.$sc.'_discount]';
						      				$bestprice_key = $categorykey.'['.$style.']['.$sc.'_bestprice]';
						      			}
						      			else
						      			{
						      				$price_key = $categorykey.'['.$style.']['.$sc.'_'.$key.'_originalprice]';
						      				$discount_key = $categorykey.'['.$style.']['.$sc.'_'.$key.'_discount]';
						      				$bestprice_key = $categorykey.'['.$style.']['.$sc.'_'.$key.'_bestprice]';
						      			}
						      		@endphp
						      		<div class="row" style="display:none; margin: 0px 0px 10px 0px;" id="{{$sc}}_{{$key}}_{{$categorykey}}_service_section_price_{{$style}}">
						      			<h5 style="margin: 5px 15px;text-decoration: underline;">{{$styleval}}</h5>
						      			<div class="col-lg-4">
						      				{{ 
						      					Form::text($price_key,null, ['class' => 'form-control '. $sc.'_'.$key.'_'.$categorykey.'_'.$style.'_price','placeholder'=>__('messages.Original Price'),'onkeypress'=>'return isNumber(event)','autocomplete'=>'off','id'=>$categorykey.'_'.$style.'_'.$sc.'_'.$key.'_originalprice','maxlength'=>'10','onkeyup'=>'calcAllValues("'.$categorykey.'_'.$style.'_'.$sc.'_'.$key.'",this)']) 
						      				}}
						      			</div>
						      			<div class="col-lg-4">
						      				{{ 
						      					Form::text($discount_key,'', ['class' => 'form-control '. $sc.'_'.$key.'_'.$categorykey.'_'.$style.'_price','placeholder'=>__('messages.Discount %'),'onkeypress'=>'return isNumber(event)','autocomplete'=>'off','onkeyup'=>'calcBestPriceValues("'.$categorykey.'_'.$style.'_'.$sc.'_'.$key.'")','id'=>$categorykey.'_'.$style.'_'.$sc.'_'.$key.'_discount']) 
						      				}}
						      			</div>
						      			<div class="col-lg-4">
						      				{{ 
						      					Form::text($bestprice_key,'', ['class' => 'form-control '. $sc.'_'.$key.'_'.$categorykey.'_'.$style.'_price','placeholder'=>__('messages.Best Price'),'onkeypress'=>'return isNumber(event)','autocomplete'=>'off','onkeyup'=>'calcDisValues("'.$categorykey.'_'.$style.'_'.$sc.'_'.$key.'")','id'=>$categorykey.'_'.$style.'_'.$sc.'_'.$key.'_bestprice','maxlength'=>'10']) 
						      				}}
						      			</div>
				                        <div class="input-group  col-lg-12">
				                            <div class="hourbox">
				                            	<div class="hhbox control-label">Hours</div>
					                            <div class="hhbox quantity">
					                            	<input type="number" name="{{$categorykey}}[{{$style}}][is{{ucfirst($sc)}}_{{$key}}_serviceTimeHour]" placeholder="Hours" value='0' min="0" max="23" step="1" class="">
					                            </div>
					                        </div>
				                           	<div class="hourbox">
				                           		<div class="hhbox control-label">Minutes</div>
				                             	<div class="hhbox quantity">
				                          			<input type="number" name="{{$categorykey}}[{{$style}}][is{{ucfirst($sc)}}_{{$key}}_serviceTimeMin]" placeholder="Minutes" value='0' min="0" max="59" step="1" class="">
				                            	</div>
				                          	</div>
				                        </div>
				                        <div class="col-lg-12">
				                           {{
				                           		Form::textarea($categorykey.'['.$style.'][is'.ucfirst($sc).'_'.$key.'_'.'description'.']',$attr_desc_arr[$stylekey], ['class' => 'form-control','rows'=>'2', 'placeholder'=>'Description','id'=>$sc.'_'.$key.'_'.$categorykey.'_service_section_price_'.$style.'_description']) 
				                           	}}
				                        </div>
						      		</div>
						      		@endforeach
					      		</div>
				      		@endforeach
							</div>   
				    	</div>
			    	</div>
			    </div>
		    	@endif
		    	@endforeach
		    	<div style="margin: 0px 0px 0px 0px;"></div>
		      	<br/>
		    </div>
		</div>
      @endforeach 
    </div>
</div>
<!-- end -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>
<script src="{{ url('/') }}/assets/lib/bootstrap/js/bootstrap-select.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
<script type="text/javascript">
	$(".myservicetrainer").select2({
	    width: '100%',
	    placeholder: "Select Trainer",
	});
	$('.select2-search__field').css('width', '100%');
    $(document).ready(function() {
        let branch_all = [];
        
        function formatResult(state) {
            if (!state.id) {
                var btn = $('<div class="text-right"><button id="all-branch" style="margin-right: 10px;" class="btn btn-default">Select All</button><button id="clear-branch" class="btn btn-default">Clear All</button></div>')
                return btn;
            }
            
            branch_all.push(state.id);
            var id = 'state' + state.id;
            var checkbox = $('<div class="checkbox"><input id="'+id+'" type="checkbox" '+(state.selected ? 'checked': '')+'><label for="checkbox1">'+state.text+'</label></div>', { id: id });
            return checkbox;   
        }
        
        function arr_diff(a1, a2) {
            var a = [], diff = [];
            for (var i = 0; i < a1.length; i++) {
                a[a1[i]] = true;
            }
            for (var i = 0; i < a2.length; i++) {
                if (a[a2[i]]) {
                    delete a[a2[i]];
                } else {
                    a[a2[i]] = true;
                }
            }
            for (var k in a) {
                diff.push(k);
            }
            return diff;
        }
        
        let optionSelect2 = {
            templateResult: formatResult,
            closeOnSelect: false,
            tags: true,
            closeOnSelect : false,
            allowHtml: true,
			allowClear: true,
            width: '100%',
            placeholder: "Select Attribute",
        };
        
        let $select2 = $(".myselectservice").select2(optionSelect2);
        
        var scrollTop;
        
        $select2.on("select2:selecting", function( event ){
            var $pr = $( '#'+event.params.args.data._resultId ).parent();
            scrollTop = $pr.prop('scrollTop');
        });
        
        $select2.on("select2:select", function( event ){
            $(window).scroll();
            
            var $pr = $( '#'+event.params.data._resultId ).parent();
            $pr.prop('scrollTop', scrollTop );
            
            $(this).val().map(function(index) {
                $("#state"+index).prop('checked', true);
            });
        });
        
        $select2.on("select2:unselecting", function ( event ) {
            var $pr = $( '#'+event.params.args.data._resultId ).parent();
            scrollTop = $pr.prop('scrollTop');
        });
        
        $select2.on("select2:unselect", function ( event ) {
            $(window).scroll();
            
            var $pr = $( '#'+event.params.data._resultId ).parent();
            $pr.prop('scrollTop', scrollTop );
            
            var branch  =   $(this).val() ? $(this).val() : [];
            var branch_diff = arr_diff(branch_all, branch);
            branch_diff.map(function(index) {
                $("#state"+index).prop('checked', false);
            });
        });
        
        $(document).on("click", "#all-branch",function(){
            $(".myselectservice > option").not(':first').prop("selected", true);// Select All Options
            $(".myselectservice").trigger("change")
            $(".select2-results__option").not(':first').attr("aria-selected", true);
            $("[id^=state]").prop("checked", true);
            $(window).scroll();
        });
        
        $(document).on("click", "#clear-branch", function(){
            $(".myselectservice > option").not(':first').prop("selected", false);
            $(".myselectservice").trigger("change");
            $(".select2-results__option").not(':first').attr("aria-selected", false);
            $("[id^=state]").prop("checked", false);
            $(window).scroll();
        });

        $(document).on("change",".myselectservice", function(){
        	var idname = $(this).attr('id');
        	$("[id^="+idname+"_]").css('display','none');
        	$('option:selected',this).each(function(){
        		var name = $(this).attr("data-attrval");
        		$("#"+idname+'_'+name).css('display','');
        		$("#"+idname+'_'+name+'_description').css('display','');
        	});
        });
    });
</script>
<script>
	function getPriceDiv(id,value)
	{
		if($(value).prop('checked')){
			$("#"+id).css('display','');
			$("#"+id+"_original").css('display','');
		}else{
			$("#"+id).css('display','none');
			$("#"+id+"_original").css('display','none');
		}
	}
	
	$("#submitBtn").click(function(){
		var is_form_submit = 0;
		$('.myClass').each(function (index, value) {
			var str = $(this).attr('id');//id of checkboxes
			var id = str.replace('is_','');

			if($("#"+str).is(":checked")){
				$("#"+id+'_section_price').each(function(){
					$('option:selected',this).each(function(){
						var attrid = $(this).attr('id');

						if(attrid != 'undefined' && $("#"+attrid).length)
						{
							var id = attrid.replace('is_','');
							$('.'+id+'_price').each(function (index, value) {								
							 	var col_id = $(value).attr('id');
								if($(value).val()!='')
								{
									$("#"+col_id).css("border-color", "green");
								}
								else{
									$("#"+col_id).css("border-color", "red");
									is_form_submit++;
								}
							});
						}
					});
				});
			}		
		});
		
		if(!is_form_submit){
			
			$("#popup-validation").submit();
		}
	});
	

	function checkHomeService(id,value)
	{
		var is_provide_home_service = '';
		if($(value).prop('checked')){
			$("#"+id).css('display','');

			is_provide_home_service = '1';
				jQuery.ajax({
		            url: '{{route('ajax.check.home.newservice')}}',
		            type: 'POST',
		            data:{is_home_service:is_provide_home_service},
		            headers: {
		            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
		            },
		            success: function (response) {

		            	if(response == 'show'){
		            		$("#homeservicealert").modal('show');
		            		$("#cancelnow").click(function(){
		            			$("#homeservicealert").modal('hide');
		            		});
		            		$("#activenow").click(function(){
		            			$("#homeservicealert").modal('hide');
		            			$("#agreement").modal('show');
		            		});

		            		$("#submitAgreement").click(function(){

		            			var mov = $("#mov").val();
		            			var hs = $("#is_provide_home_service").val();
		            			if(mov!='' && hs!=''){
		            				 jQuery.ajax({
							            url: '{{route('home.newservice.active')}}',
							            type: 'POST',
							            data:{mov:mov,is_home_service:'1'},
							            headers: {
							            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
							            },
							            success: function (response) {
							              if(response == 'success'){
							              	$("#agreement").modal('hide');
							              }
							             
							          	}
							            });
		            			}else{
		            				$("#homeCheckValidation").html('<?php echo __('messages.Please select all fields values'); ?>');
		            			}
		            			
		            		});
		            	}
		              
		            }
		        });
		}else{
			$("#"+id).css('display','none');	
		}
		
	} 

	function isNumber(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46){
            return false;
        }
      
        return true;
    }

    function calcBestPriceValues(gender)
    {
    
      var dis = $("#"+gender+"_discount").val();
      if(dis>100){
        $("#"+gender+"_discount").val("");
      }else{

      	var total = $("#"+gender+"_originalprice").val();
      	if(total!=''){
      		var bestprice = total - (total*dis)/100;
            $("#"+gender+"_bestprice").val((bestprice).toFixed(3));
      	}else{
      		$("#"+gender+"_bestprice").val(0);
      	}
        
      }
    }

    function calcDisValues(gender)
    {
      var best_price = $("#"+gender+"_bestprice").val();
       var total = $("#"+gender+"_originalprice").val();


       best_price = parseInt(best_price);
       total = parseInt(total);
      

      if(best_price>0 && best_price<total){
       
        if(total!=''){
        	var bestprice_1 = total - best_price;
        	bestprice_2 =(bestprice_1*100)/total;
            $("#"+gender+"_discount").val((bestprice_2).toFixed(3));
        }else{
        	$("#"+gender+"_discount").val(0);
        }
        
      }else{


      	$("#"+gender+"_bestprice").css("border-color", "red");
      	$("#"+gender+"_bestprice").val('');
      }
    }

    function calcAllValues(gender,data)
    {
    	// var total = $("#"+gender+"_originalprice").val();
    	var total = data.value;
    	var best_price = $("#"+gender+"_bestprice").val();
    	
    	var dis = $("#"+gender+"_discount").val();
 

       	if(dis == '' || dis == 0 || dis == '0'){
       		$("#"+gender+"_bestprice").val(total);
       		$("#"+gender+"_discount").val('0')	;
       	}
       	else{
       		if(total>0 && dis>0){
	        	var bestprice = total - (total*dis)/100;
	            $("#"+gender+"_bestprice").val((bestprice).toFixed(3));
	        }else{
	        	$("#"+gender+"_bestprice").val(0);
	        }	
       	}
    }
</script>
    
<script type="text/javascript">
    $(document).ready(function(){
    	jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div></div>').insertAfter('.quantity input');
	    jQuery('.quantity').each(function() {
	      var spinner = jQuery(this),
	        input = spinner.find('input[type="number"]'),
	        btnUp = spinner.find('.quantity-up'),
	        btnDown = spinner.find('.quantity-down'),
	        min = input.attr('min'),
	        max = input.attr('max');

	      btnUp.click(function() {
	        var oldValue = parseFloat(input.val());
	        if (oldValue >= max) {
	          var newVal = oldValue;
	        } else {
	          var newVal = oldValue + 1;
	        }
	        spinner.find("input").val(newVal);
	        spinner.find("input").trigger("change");
	      });

	      btnDown.click(function() {
	        var oldValue = parseFloat(input.val());
	        if (oldValue <= min) {
	          var newVal = oldValue;
	        } else {
	          var newVal = oldValue - 1;
	        }
	        spinner.find("input").val(newVal);
	        spinner.find("input").trigger("change");
	      });

	    });
	}); 
</script>

<!-- The Modal -->
<div class="modal" id="homeservicealert" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">{{ __('messages.Beutics Provider') }}</h4>
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        {{ __('messages.Please active Home Service and enter MOV from My Agreement section to add Home Service Prices.') }}
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
      	<button type="cancel" class="btn" id="cancelnow">{{ __('messages.Cancel') }}</button>
      	<button type="button" class="btn btn-success" id="activenow">{{ __('messages.Active Now') }}</button>
        <!-- <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button> -->
      </div>

    </div>
  </div>
</div>

<div class="modal" id="agreement" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">{{ __('messages.Agreement') }}</h4>
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <center><a class="btn btn-danger" href="{{ asset('sp_uploads/approve_pdf/'.$sp_row->agreement) }}" target="_blank">{{ __('messages.View Agreement') }}</a></center><br/><br/>

        <div class="form-group">
            {{Form::label('', '', ['class' => 'control-label col-lg-4 '])}}
            <div class="col-lg-6">
                <input type="checkbox" id="is_provide_home_service"> {{ __('messages.Also Provide Home Services?') }}
                
            </div>
        </div>
        <div class="form-group">
            {{Form::label('mov', 'MOV', ['class' => 'control-label col-lg-4'])}}
            <div class="col-lg-6">
                {{ Form::text('','', ['class' => 'form-control','placeholder'=> __('messages.minimum order value'),'id'=>'mov']) }}

            </div>
        </div>
        <span id="homeCheckValidation"></span>
       
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
      	<a class="btn btn-success" id="submitAgreement">{{ __('messages.Submit') }}</a>
        <!-- <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button> -->

      </div>

    </div>
  </div>
</div>

@endif

