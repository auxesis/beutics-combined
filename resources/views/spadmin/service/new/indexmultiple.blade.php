@extends('layouts.spadmin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
 
@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>{{ __('messages.Services') }}</h5>
                <a class="btn btn-primary pull-right" href="{{route('new-services.create')}}" style="margin-top:4px;">{{ __('messages.Create Service') }}</a>
            </header>
            <div id="collapse4" class="body">
              @include('message')
              
               <table id="user_datatable" class="table table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>{{ __('messages.Fitness Type') }}</th>
                        <th>{{ __('messages.Fitness Title') }}</th>
                        <th>{{ __('messages.Service Name') }}</th>
                        <th>{{ __('messages.Sub Category Name') }}</th>
                        <th>{{ __('messages.Created At') }}</th>
                        <th>{{ __('messages.Action') }}</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
                  
            </div>
        </div>
    </div>
</div>
@endsection

@section('uniquescript')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>
<script type="text/javascript">

  function deleteRow(obj, offer_asso)
  {
    event.preventDefault(); // prevent form submit
    var msg = "{{ __('messages.Are you sure?') }}";
    if(offer_asso > 0){
        var msg = "This service is linked with "+offer_asso+" offers on deleting of this service, system will delete all your linked offers.\nAre you sure want to delete this service?";
                    
    }

    swal({
      title: msg,
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {        
       obj.submit();

      } else {
         swal.close();
      }
    });
  }
 
    $(function() {
        var table = $('#user_datatable').DataTable({
           stateSave: true,
        processing: true,
        serverSide: true,
        order: [[0, "desc" ]],
        "ajax":{
        "url": '{!! route('new-services.getdata') !!}',
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{csrf_token()}}"}    
        },
        columns: [
        { data: 'fitness_type', name: 'fitness_type', orderable:true , visible:false},
        { data: 'fitness_title', name: 'fitness_title', orderable:true , visible:false},
        { data: 'service_id', name: 'service_id', orderable:true },
        { data: 'subcat_name', name: 'subcat_name', orderable:true },
        { data: 'created_at', name: 'created_at', orderable:true },
        { data: 'action', name: 'action', orderable:true  },
        ],
        "columnDefs": [
        { "aTargets": [0, 1], "sClass": "invisible"},
        { "searchable": false, "targets": 0 }
        ]
        , language: {
        searchPlaceholder: "{{ __('messages.Search by name') }}"
        },
});
});  


</script>
@endsection

