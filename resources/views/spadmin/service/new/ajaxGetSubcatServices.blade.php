@if($service_arr)
{{ Form::select('service_id',$service_arr,null,['class' => 'form-control validate[required] selectpicker','placeholder'=>__('messages.Select Services'),'data-live-search'=>"true",'id'=>'services']) }}
@else
{{ __('messages.No Services Found') }}
@endif

<script type="text/javascript">
     $("#services").change(function()
        {
            var id = $(this).val();
            var subcat_id = <?php echo json_encode($subcat_id); ?>;
            jQuery.ajax({
            url: '{{route('ajax.add.newservice.data')}}',
            type: 'POST',
            data:{service_id:id,subcat_id:subcat_id},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                $("#further_data").html(response);
              
                }
            });
              
        });
</script>