@if($sub_service_arr)
{{ Form::select('sub_service_id',$sub_service_arr,null,['class' => 'form-control selectpicker','data-live-search'=>"true",'id'=>'sub_services','placeholder'=>__('messages.Select Sub Services')]) }}
@else
{{ __('messages.No Sub Services Found') }}
@endif

{{ Form::hidden('service_desc', $serviceDesc, ['id' => 'service_desc']) }}