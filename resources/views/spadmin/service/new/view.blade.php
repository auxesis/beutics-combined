@extends('layouts.spadmin.admin')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>{{ __('messages.View Service') }}</h5>

            </header>
            <div id="collapse2" class="body">
                <div class="row">
                     @include('message')
                    <div class="col-sm-12 col-lg-12">
                        <div class="col-lg-12 border_details">
                            <div class="col-lg-4"><b>{{ __('messages.Sub Category Name') }}</b></div>
                            <div class="col-lg-8"><?= $catName; ?></div> <div class="clearfix"></div>
                        </div>
                        <div  class="col-lg-12 border_details">
                            <div class="col-lg-4"><b>{{ __('messages.Service Name') }}</b></div>
                            <div class="col-lg-8"><?= $serviceName; ?></div> <div class="clearfix"></div>
                        </div>
                        <br/><br/><br/>
                        @foreach($rows->getRelatedPrices as $prices)
                        <div  class="col-lg-12 border_details">

                            <div class="col-lg-4"><b><?= str_replace("_"," ",$prices['field_key']); ?></b></div>
                            <div class="col-lg-8"><?= $prices['price'].'  <b>AED</b>'; ?></div> <div class="clearfix"></div>
                        </div>
                        @endforeach
                    </div>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>

@endsection