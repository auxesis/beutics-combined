@extends('layouts.spadmin.admin')

@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/jquery.gritter/css/jquery.gritter.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/themes/default/css/uniform.default.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/bootstrap/css/bootstrap-select.min.css" />
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>{{$title}}</h5>
          

            </header>
             @include('message')
            <div id="collapse2" class="body">
                <!-- <form class="form-horizontal" id="popup-validation"> -->
                {!! Form::model($rows,['route' => ['services.update',$rows->id], 'method' => 'PATCH', 'class' => 'form-horizontal','id' => 'popup-validation','enctype' => 'multipart/form-data']) !!}
                {{ csrf_field() }}
                    
                    <div class="form-group">
                        {{Form::label('', __('messages.Sub Category Name'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                           {{ Form::text('',$rows->getServiceSubcategory->getSubcat->$cat_name, ['class' => 'form-control','readonly']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('service_id', __('messages.Service Name'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                           {{ Form::text('',$rows->getAssociatedService->$sr_name, ['class' => 'form-control','readonly']) }}
                        </div>
                    </div>

                    <div class="form-group">
                    {{Form::label('', __('messages.Gender'), ['class' => 'control-label col-lg-4'])}}
                     <div class="col-lg-5">
                        <?php $service_criteria = explode(',',$rows->getAssociatedUsername->service_criteria);
                        ?>
                        @foreach($service_criteria as $sc)
                         <div class="checkboxg">
                          <label>

                            <input 
                            type="checkbox" 
                            name="is{{ucfirst($sc)}}"
                            id="is_{{$sc}}" 
                            class="myClass"
                            onclick="getPriceDiv('<?= $sc."_price";?>',this)" value="true" 
                                <?php
                                $key = 'is'.ucfirst($sc);
                                $check = in_array($key,$selected_gender_arr)?'checked':'';echo $check;
                                ?>> 
                                {{ucwords($sc)}}

                          </label>
                         </div>
                         <div class="row">
                            <?php $display = $check =='checked'?'display:block;':'display:none';?>

                             <div class="col-lg-12" style="<?= $display;?>" id="{{$sc}}_price">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <?php $price = strtoupper($sc.'_originalprice');
                                        $price_row = array_key_exists($price,$prices_arr)?$prices_arr[$price]:'';
                                        ?>
                                       {{ Form::text($sc.'_originalprice',$price_row, ['class' => 'form-control '. $sc.'_price','placeholder'=>__('messages.Original Price'),'onkeypress'=>'return isNumber(event)','autocomplete'=>'off','id'=>$sc.'_originalprice','maxlength'=>'10','onkeyup'=>'calcAllValues("'. $sc.'",this)']) }}
                                    </div>
                                    <div class="col-lg-4">

                                    <?php $price = strtoupper($sc.'_discount');
                                    $price_row = array_key_exists($price,$prices_arr)?$prices_arr[$price]:'';
                                    ?>

                                    {{ Form::text($sc.'_discount',$price_row, ['class' => 'form-control '. $sc.'_price','placeholder'=>__('messages.Discount %'),'onkeypress'=>'return isNumber(event)','autocomplete'=>'off','onkeyup'=>'calcBestPriceValues("'. $sc.'")','id'=>$sc.'_discount']) }}
                                    </div>
                                    <div class="col-lg-4">
                                        <?php $price = strtoupper($sc.'_bestprice');
                                        $price_row = array_key_exists($price,$prices_arr)?$prices_arr[$price]:'';
                                        ?>
                                        {{ Form::text($sc.'_bestprice',$price_row, ['class' => 'form-control '. $sc.'_price','placeholder'=>__('messages.Best Price'),'onkeypress'=>'return isNumber(event)','autocomplete'=>'off','onkeyup'=>'calcDisValues("'. $sc.'")','id'=>$sc.'_bestprice','maxlength'=>'10']) }}
                                    </div>
                                 </div><!--first row end -->
                                 <br/>
                                 <label>
                                    <input 
                                    type="checkbox" 
                                    name="is{{ucfirst($sc)}}_home_service" onclick="checkHomeService('<?= $sc."_home_price";?>',this)" 
                                    value="true"
                                    class="myClass"
                                    id="is_{{$sc}}_home"
                                    <?php
                                    $key = 'is'.ucfirst($sc).'_home_service';
                                    $check_home = in_array($key,$selected_gender_arr)?'checked':'';
                                    echo $check_home;
                                    ?>
                                    > 
                                    {{ __('messages.Also available for Home Services') }}
                                </label>
                                <div class="row">
                                     <?php $display_home = $check_home =='checked'?'display:block;':'display:none';?>
                                    <div class="col-lg-12" style="<?= $display_home;?>" id="{{$sc}}_home_price">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <?php $price = strtoupper($sc.'_home_originalprice');
                                                $price_row = array_key_exists($price,$prices_arr)?$prices_arr[$price]:'';
                                                ?>

                                                {{ Form::text($sc.'_home_originalprice',$price_row, ['class' => 'form-control '. $sc.'_home_price','placeholder'=>__('messages.Original Price'),'onkeypress'=>'return isNumber(event)','autocomplete'=>'off','id'=>$sc.'_home_originalprice','maxlength'=>'10','onkeyup'=>'calcAllValues("'. $sc.'_home",this)']) }}
                                            </div>
                                            <div class="col-lg-4">
                                                <?php $price = strtoupper($sc.'_home_discount');
                                                $price_row = array_key_exists($price,$prices_arr)?$prices_arr[$price]:'';
                                                ?>

                                                {{ Form::text($sc.'_home_discount',$price_row, ['class' => 'form-control '. $sc.'_home_price','placeholder'=>__('messages.Discount %'),'onkeypress'=>'return isNumber(event)','autocomplete'=>'off','onkeyup'=>'calcBestPriceValues("'. $sc.'_home")','id'=>$sc.'_home_discount']) }}
                                            </div>

                                            <div class="col-lg-4">
                                                 <?php $price = strtoupper($sc.'_home_bestprice');
                                                $price_row = array_key_exists($price,$prices_arr)?$prices_arr[$price]:'';
                                                ?>

                                                  {{ Form::text($sc.'_home_bestprice',$price_row, ['class' => 'form-control '. $sc.'_home_price','placeholder'=>__('messages.Best Price'),'onkeypress'=>'return isNumber(event)','autocomplete'=>'off','onkeyup'=>'calcDisValues("'. $sc.'_home")','id'=>$sc.'_home_bestprice','maxlength'=>'10']) }}
                                            </div>

                                        </div>            
                                    </div>
                                </div>
                                <br/>
                             </div>
                         </div>
                        @endforeach
                     </div>
                    </div>
                    
                    </div>
                    <!-- end body -->

                    <div class="form-actions no-margin-bottom">
                        <input value="{{ __('messages.Submit') }}" class="btn btn-primary"  id="submitBtn" style="width:80px;" autocomplete="off">
                    </div>  
                    {{Form::close()}}
                    <br><br><br><br>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
@endsection
@section('uniquescript')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>

     <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/holder/2.4.1/holder.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/jquery.uniform.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>

    <script src="{{ url('/') }}/assets/lib/bootstrap/js/bootstrap-select.min.js"></script>

    <script>
        $(function() {
          Metis.formValidation();
        });
    </script>

    <script>
   
    function getPriceDiv(id,value)
    {
        if($(value).prop('checked')){
            $("#"+id).css('display','');
        }else{
            $("#"+id).css('display','none');    
        }
    }

    function checkHomeService(id,value)
    {
        var is_provide_home_service = '';
        if($(value).prop('checked')){
            $("#"+id).css('display','');

            is_provide_home_service = '1';
                jQuery.ajax({
                    url: '{{route('ajax.check.home.service')}}',
                    type: 'POST',
                    data:{is_home_service:is_provide_home_service},
                    headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (response) {

                        if(response == 'show'){
                            $("#homeservicealert").modal('show');
                            $("#activenow").click(function(){
                                $("#homeservicealert").modal('hide');
                                $("#agreement").modal('show');
                            });

                            $("#submitAgreement").click(function(){

                                var mov = $("#mov").val();
                                var hs = $("#is_provide_home_service").val();
                                if(mov!=''){
                                     jQuery.ajax({
                                        url: '{{route('home.service.active')}}',
                                        type: 'POST',
                                        data:{mov:mov,is_home_service:'1'},
                                        headers: {
                                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                                        },
                                        success: function (response) {
                                          if(response == 'success'){
                                            $("#agreement").modal('hide');
                                          }
                                         
                                        }
                                        });
                                }
                                
                            });
                        }
                      
                    }
                });
        }else{
            $("#"+id).css('display','none');    
        }
        
    }

    $("#submitBtn").click(function(){
    var is_form_submit = 0;
    $('.myClass').each(function (index, value) {
      var str = $(this).attr('id');//id of checkboxes

      var id = str.replace('is_','');

      if($("#"+str).is(":checked")){

        $('.'+id+'_price').each(function (index, value) {
          
          var col_id = $(value).attr('id');
          // alert($(value).val());
          if($(value).val()!='')
          {
            $("#"+col_id).css("border-color", "green");           

          }
          else{
            $("#"+col_id).css("border-color", "red");
            is_form_submit++;
          }
        });
      }   
    });
    
    if(!is_form_submit){
      
      $("#popup-validation").submit();
    }
  });

    function isNumber(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46){
            return false;
        }
      
        return true;
    }

    function calcBestPriceValues(gender)
    {
    
      var dis = $("#"+gender+"_discount").val();
      if(dis>100){
        $("#"+gender+"_discount").val("");
      }else{

        var total = $("#"+gender+"_originalprice").val();
        if(total!=''){
          var bestprice = total - (total*dis)/100;
            $("#"+gender+"_bestprice").val((bestprice).toFixed(3));
        }else{
          $("#"+gender+"_bestprice").val(0);
        }
        
      }
    }

    function calcDisValues(gender)
    {
      var best_price = $("#"+gender+"_bestprice").val();
       var total = $("#"+gender+"_originalprice").val();

        best_price = parseInt(best_price);
       total = parseInt(total);
      if(best_price>0 && best_price<total){
       
        if(total!=''){
          var bestprice_1 = total - best_price;
          bestprice_2 =(bestprice_1*100)/total;
            $("#"+gender+"_discount").val((bestprice_2).toFixed(3));
        }else{
          $("#"+gender+"_discount").val(0);
        }
        
      }else{
        $("#"+gender+"_bestprice").css("border-color", "red");
        $("#"+gender+"_bestprice").val('');
      }
    }

    function calcAllValues(gender,data)
    {
      // var total = $("#"+gender+"_originalprice").val();
      var total = data.value;
      var best_price = $("#"+gender+"_bestprice").val();
      
      var dis = $("#"+gender+"_discount").val();
 

        if(dis == '' || dis == 0 || dis == '0'){
          $("#"+gender+"_bestprice").val(total);
          $("#"+gender+"_discount").val('0')  ;
        }
        else{
          if(total>0 && dis>0){
            var bestprice = total - (total*dis)/100;
              $("#"+gender+"_bestprice").val((bestprice).toFixed(3));
          }else{
            $("#"+gender+"_bestprice").val(0);
          } 
        }

        
     
    }
</script>
@endsection

<!-- The Modal -->
<div class="modal" id="homeservicealert" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">{{ __('messages.Beutics Provider') }}</h4>
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        {{ __('messages.Please active Home Service and enter MOV from My Agreement section to add Home Service Prices.') }}
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="activenow">{{ __('messages.Active Now') }}</button>
        <!-- <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button> -->
      </div>

    </div>
  </div>
</div>

<div class="modal" id="agreement" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">{{ __('messages.Agreement') }}</h4>
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <center><a class="btn btn-danger" href="{{ asset('sp_uploads/approve_pdf/'.$rows->getAssociatedUsername->agreement) }}" target="_blank">{{ __('messages.View Agreement') }}</a></center><br/><br/>

        <div class="form-group">
            {{Form::label('', '', ['class' => 'control-label col-lg-4 '])}}
            <div class="col-lg-6">
                <input type="checkbox" id="is_provide_home_service"> {{ __('messages.Also Provide Home Services?') }}
            </div>
        </div>
        <div class="form-group">
            {{Form::label('mov', 'MOV', ['class' => 'control-label col-lg-4'])}}
            <div class="col-lg-6">
                {{ Form::text('','', ['class' => 'form-control','placeholder'=>__('messages.minimum order value'),'id'=>'mov']) }}
            </div>
        </div>
        
       
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <a class="btn btn-success" id="submitAgreement">{{ __('messages.Submit') }}</a>
        <!-- <button type="button" class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button> -->

      </div>

    </div>
  </div>
</div>