@extends('layouts.spadmin.admin')

@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/jquery.gritter/css/jquery.gritter.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/themes/default/css/uniform.default.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/bootstrap/css/bootstrap-select.min.css" />
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>{{$title}}</h5>
          

            </header>
             @include('message')
            <div id="collapse2" class="body">
                <!-- <form class="form-horizontal" id="popup-validation"> -->
                {!! Form::open(['route' => 'services.store', 'method' => 'POST', 'class' => 'form-horizontal','id' => 'popup-validation','enctype' => 'multipart/form-data']) !!}
                {{ csrf_field() }}
                
                    <div class="form-group">
                        {{Form::label('', __('messages.Sub Category Name'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::select('', $subcat_arr,null,['class' => 'form-control validate[required] selectpicker','placeholder'=>__('messages.Select Sub Categories'),'id'=>'sub_category','data-live-search'=>"true"]) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('service_id', __('messages.Service Name'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4" id="service_div">
                            {{ Form::select('service_id', [],null,['class' => 'form-control validate[required]','placeholder'=>__('messages.Select Services'),'id'=>'services']) }}
                        </div>
                    </div>

                    <div id="further_data">
                    </div>
                    <span id="fieldAlert" style="color:red;margin-left:335px;"></span>
                    <div class="form-actions no-margin-bottom" >

                    <div class="form-actions no-margin-bottom" >
                        <input value="{{ __('messages.Submit') }}" class="btn btn-primary"  id="submitBtn" style="width:80px;" autocomplete="off">
                    </div>  

                    <br><br><br><br>
                </form>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
@endsection
@section('uniquescript')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>

     <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/holder/2.4.1/holder.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/jquery.uniform.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>

    <script src="{{ url('/') }}/assets/lib/bootstrap/js/bootstrap-select.min.js"></script>

    <script>
        $(function() {
          Metis.formValidation();
        });
    </script>

    <script src="{{ url('/') }}/assets/js/jquery.validate.min.js"></script>
    <script>$(".validate").validate();</script>


    <script type="text/javascript">
        $("#sub_category").change(function()
        {
            var id = $(this).val();
            
            jQuery.ajax({
            url: '{{route('ajax.get.services')}}',
            type: 'POST',
            data:{subcat_id:id},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
              $("#service_div").html(response);
              
            	}
            });
              
        });

        $(function() {
		  $('.selectpicker').selectpicker();
		});


        $('#submitBtn').click(function(){
         if($("#is_men").is(':checked') || $("#is_women").is(':checked') || $("#is_kids").is(':checked')){
               return true;
         }else{
            $("#fieldAlert").html('<?php echo __('messages.Please select all fields values'); ?>');
             // return false;
             e.preventDefault();
             return false;
           
         }
        });

    </script>




@endsection