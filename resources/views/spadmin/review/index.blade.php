@extends('layouts.spadmin.admin')
@section('uniquecss')
<style type="text/css">
	
.tab-content select{ display: inline-block; width: auto;      margin-top: 34px;}

.rtabcob h4{ float: left; }

li.disabled span {
    padding: 0px;
}

.pagination > li {
    display: inline-block;
}

.pagination > li span{position: relative;
    float: left;
    padding: 4px 12px!important;
    margin-left: -1px;
    line-height: 1.42857143;
    color: #337ab7;
    text-decoration: none;
    background-color: #fff;
    border: 1px solid #ddd;}

.rtabcob ul li {
    border: none;
}

ul.pagination {
    width: 100%;
    text-align: center;
}

.pagination {
    display: block;
}
	
</style> 
@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>{{ __('messages.My Reviews') }}</h5>
                
            </header>
            <div id="collapse4" class="body">

<?php if(count($customer_reviews)>0){?>
<div class="row">
	<div class="col-md-12 totalRe"> 
		<span style="background-color:<?php $color = returnColorCode($customer_combined_reviews['average_rating']);echo $color[1];?>">{{$customer_combined_reviews['average_rating']}}</span>
		<h4>
		<?php $color = returnColorCode($customer_combined_reviews['average_rating']); ?>
				{{__('messages.'.$color[0])}}
		<?php
			if($customer_combined_reviews['total_reviewers']>1){
				$label = __('messages.Reviews');
			}else{
				$label = __('messages.Review');
			}
		?>
		{{'('.$customer_combined_reviews['total_reviewers'].' '.$label}} )</h4>
	</div>
</div>
<div class="row revewsBars">
	<div class="col-md-4">
		<h4>{{ __('messages.Value for Money') }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;({{$customer_combined_reviews['value_for_money']}}/10)</h4>
		<?php $width = $customer_combined_reviews['value_for_money'].'0';?>
        <div class="review">
			<div class="reviewIn" style="width:{{$width}}%;background-color:<?php $color = returnColorCode($customer_combined_reviews['value_for_money']);echo $color[1];?>;">
				
			</div>
		</div>
	</div>

	<div class="col-md-4">
		<h4>{{ __('messages.Staff Expertise') }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;({{$customer_combined_reviews['staff_expertise']}}/10)</h4>
		<?php $width = $customer_combined_reviews['staff_expertise'].'0';?>
              		<div class="review">
						<div class="reviewIn" style="width:{{$width}}%;background-color:<?php $color = returnColorCode($customer_combined_reviews['staff_expertise']);echo $color[1];?>;">
						
						</div>
					</div>
	</div>


	<div class="col-md-4">
	<h4>{{ __('messages.Cleanliness') }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;({{$customer_combined_reviews['ambience_comfort']}}/10)</h4>
              		<?php $width = $customer_combined_reviews['ambience_comfort'].'0';?>
              		<div class="review">
						<div class="reviewIn" style="width:{{$width}}%;background-color:<?php $color = returnColorCode($customer_combined_reviews['ambience_comfort']);echo $color[1];?>;">
							
						</div>
					</div>
	</div>



</div>


<div class="row revewsBars">
	<div class="col-md-4">
	<h4>{{ __('messages.Location') }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;({{$customer_combined_reviews['location']}}/10)</h4>
  		<?php $width = $customer_combined_reviews['location'].'0';?>
  		<div class="review">
			<div class="reviewIn" style="width:{{$width}}%;background-color:<?php $color = returnColorCode($customer_combined_reviews['location']);echo $color[1];?>;">
			
			</div>
		</div> 
	</div>

	<div class="col-md-4">
	<h4>{{ __('messages.Care & Attention') }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;({{$customer_combined_reviews['hospitality']}}/10) </h4>
  		<?php $width = $customer_combined_reviews['hospitality'].'0';?>
  		<div class="review">
			<div class="reviewIn" style="width:{{$width}}%;background-color:<?php $color = returnColorCode($customer_combined_reviews['hospitality']);echo $color[1];?>;">
			
			</div>
		</div>
	</div>


	<div class="col-md-4">
	<h4>{{ __('messages.Facilities / Products') }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;({{$customer_combined_reviews['products_equipments']}}/10) </h4>
  	<?php $width = $customer_combined_reviews['products_equipments'].'0';?>
  		<div class="review">
			<div class="reviewIn" style="width:{{$width}}%;background-color:<?php $color = returnColorCode($customer_combined_reviews['products_equipments']);echo $color[1];?>;">
				
			</div>
		</div> 
	</div>

</div>

<ul class="nav nav-tabs reviewTabs">
    <li class="active"><a data-toggle="tab" href="#home">{{ __('messages.Reviews') }}</a></li>
    <li><a data-toggle="tab" href="#menu1">{{ __('messages.Voice of Reviewers') }}</a></li>
 </ul>







  <div class="tab-content">
   
   
    <div id="home" class="tab-pane rtabcob fade in active">
	 
	<div class="row">
	 	<div class="col-md-12">
		 	<h4>{{ __('messages.Reviews') }}</h4>
		    <select style="float:right;" onchange="reviewFilter(this)" class="form-control">
		    	<option value="all">{{ __('messages.All') }}</option>
		    	<option value="needs_improvement">{{ __('messages.Needs Improvement') }}</option>
		    	<option value="average">{{ __('messages.Average') }}</option>
		    	<option value="good">{{ __('messages.Good') }}</option>
		    	<option value="very_good">{{ __('messages.Very Good') }}</option>
		    	<option value="superb">{{ __('messages.Superb') }}</option>
		    	<option value="exceptional">{{ __('messages.Exceptional') }}</option>
		    </select>
		</div>
	 </div>
    <div id="reviewDiv">
		@foreach($customer_reviews as $c_reviews)
		<div class="reverss">			  
			<div class="row">
			<div class="col-md-12">
				<span class="reviScore" style="background-color:<?php $color = returnColorCode($c_reviews['rating']);echo $color[1];?>">
					{{$c_reviews['rating']}}
				</span>
				<div class="revrdetail">
					<h5>{{$c_reviews['customer_name']}}</h5>
					<p class="date">
						@if($c_reviews['user_id'] != '' && $c_reviews['user_id'] != null)
						{{$c_reviews['created_at']}}
						@endif
					</p>
				</div>	
			</div>
			</div>
			<div class="row">
				<div class="col-md-12 disc">
					<p>{{$c_reviews['description']}}</p>
				</div>
			</div>
		</div>

	   @endforeach

	   <div class="pagination">
		    {{$customer_reviews->links()}}
		</div>
	</div>



    </div>
    <div id="menu1" class="tab-pane rtabcob fade">
     
		<h4>{{ __('messages.Voice of Reviewers') }}</h4><br/><br/>
		<ul>
		      @foreach($customer_combined_reviews['services'] as $services)
		      <li>
			  	<h5>{{$services['service_name']}}</h5>
		      	<span>
					<img src="{{ url('/') }}/assets/icons/green_thumb.png"/> ({{$services['like_count']}} {{ __('messages.Reviewers') }}) 
				</span>
			  	<span>
				  <img src="{{ url('/') }}/assets/icons/red_thumb.png"/>  ({{$services['dislike_count']}} {{ __('messages.Reviewers') }}) 
					</span>
			  </li>
		      
		      @endforeach
		  </ul>
    </div>
  </div>

            </div>
            <!-- end -->
        <?php } else{ echo __('messages.No Reviews Found'); }?>
        </div>
    </div>
</div>
@endsection

@section('uniquescript')
<script>
	function reviewFilter(filterVal)
	{
		filterVal = filterVal.value;
		jQuery.ajax({
            url: '{{route('sp.ajax.review.filter')}}',
            type: 'POST',
            data:{filter:filterVal},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
             // console.log(response);
             if(response!="nodata"){
             	$("#reviewDiv").html(response);
             }else{
             	
             	var sp = "<span style='color:red;font-size:20px;'>".<?php echo __('messages.No Data Found'); ?>."</span>";
             	$("#reviewDiv").html(sp);
             }
            }
              
          });
	}
</script>
@endsection

