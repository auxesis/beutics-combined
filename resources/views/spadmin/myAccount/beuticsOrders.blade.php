@extends('layouts.spadmin.admin')

@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>{{ __('messages.Online Sales') }}</h5>
                <!-- <a class="btn btn-primary pull-right" href="{{route('staffs.create')}}" style="margin-top:4px;">Online Sales</a> -->
                <!--<a class="btn btn-success pull-right" href="{{route('staffs.staff-calendar')}}" style="margin-top:4px;"> Staff Calendar </a>-->
            </header>
            <div id="collapse4" class="body">
              @include('message')
              
               <table id="user_datatable" class="table table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th width="10%">{{ __('messages.Order Unique ID') }}</th>
                        <th width="15%">{{ __('messages.Customer Name') }}</th>
                        <th width="10%">{{ __('messages.Order Date') }}</th>
                        <th width="35%">{{ __('messages.Services Name') }}</th>
                        <th width="10%">{{ __('messages.Service Closed') }}</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
                  
            </div>
        </div>
    </div>
</div>
@endsection

@section('uniquescript')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script type="text/javascript">

 
    $(function() {
        var table = $('#user_datatable').DataTable({
           stateSave: true,
        processing: true,
        serverSide: true,
        order: [[0, "desc" ]],
        "ajax":{

        "url": '{!! route('my-account.gets.sales') !!}',
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{csrf_token()}}",status:'all'}
        },
        columns: [
        { data: 'order_id', name: 'order_id', orderable:true  },
        { data: 'customer_name', name: 'customer_name', orderable:true  },
        { data: 'order_date', name: 'order_date', orderable:true },
        { data: 'services_name', name: 'services_name', orderable:true  },
        { data: 'service_closed', name: 'service_closed', orderable:true  },
        ],
        "columnDefs": [
        { "searchable": false, "targets": 0 }
        ]
        , language: {
        searchPlaceholder: "Search by name"
        },
});
});  


</script>
@endsection

