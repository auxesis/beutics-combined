@extends('layouts.spadmin.admin')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>{{ __('messages.My Account') }}</h5>
                <a class="btn btn-primary pull-right" href="{{route('myaccount.paid.sales')}}" style="margin-top:4px;">{{ __('messages.Paid Orders') }}</a>
            </header>
            <div id="collapse2" class="body">
                <div class="row">
                    <div class="col-sm-12 col-lg-12"  style="border:1px solid rgb(235,235,235); width: 98%; margin-left: 1%;margin-bottom: 3%;">
                        <ul style="list-style: none;">
                            <li style="margin-bottom:20px;"><h4>{{ __('messages.CURRENT EARNING') }}</h4></li>
                            <li>
                                <a href="{{route('myaccount.online.sales')}}">
                                {{ __('messages.Total Online Sales') }}
                                </a> 
                                <span style="float:right;margin-right:20%;"> AED {{$result_arr['total_online_sales']}}</span></li>
                            <hr>
                           
                            <li>
                                <a href="{{route('myaccount.offline.sales')}}">
                                {{ __('messages.Total Offline Sales') }} 
                                </a>
                                <span style="float:right;margin-right:20%;"> AED {{$result_arr['total_offline_sale']}}</span>
                            </li>
                            <hr>

                            <li>{{ __('messages.Net Store Sales') }} <span style="float:right;margin-right:20%;"> AED {{$result_arr['net_store_sales']}}</span></li>
                            <hr>

                            <li>
                                <a href="{{route('myaccount.all.sales')}}">
                                {{ __('messages.Total Sales (Beutics Order)') }} 
                                </a>
                                <span style="float:right;margin-right:20%;"> AED {{$result_arr['total_sales']}}</span></li>
                            <hr>

                            <li> {{ __('messages.Beutics Commission') }} (@ {{$result_arr['commission']}}%) <span style="float:right;margin-right:20%;"> AED {{$result_arr['beutics_commission']}}</span></li>
                            <hr>

                            <li>{{ __('messages.My Earnings') }} <span style="float:right;margin-right:20%;"> AED {{$result_arr['my_earnings']}}</span></li>

                        </ul>
                    </div>

                    <div class="col-sm-12 col-lg-12"  style="border:1px solid rgb(235,235,235); width: 98%; margin-left: 1%;;margin-bottom: 3%;">
                        <ul style="list-style: none;">
                            <li style="margin-bottom:20px;"><h4>{{ __('messages.OUTSTANDINGS') }}</h4></li>
                            <li><?php echo $result_arr['outstanding'] >0 ?  __('messages.Pay Store') : __('messages.Pay Beutics'); ?> <span style="float:right;margin-right:20%;"> AED {{$result_arr['outstanding']}}</span></li>

                        </ul>
                    </div>

                    <div class="col-sm-12 col-lg-12"  style="border:1px solid rgb(235,235,235); width: 98%; margin-left: 1%;">
                        <ul style="list-style: none;">
                            <li style="margin-bottom:20px;"><h4>{{ __('messages.LIFETIME EARNING') }}</h4></li>

                            <li>{{ __('messages.Total Online Earnings') }}<span style="float:right;margin-right:20%;"> AED {{$result_arr['total_lifetime_online_earning']}}</span></li>
                            <hr>
                            <li>{{ __('messages.Total Offline Earnings') }}<span style="float:right;margin-right:20%;"> AED {{$result_arr['total_lifetime_offline_earning']}}</span></li>

                            <hr>
                            <li>{{ __('messages.Total Earnings') }}<span style="float:right;margin-right:20%;"> AED {{$result_arr['total_lifetime_earning']}}</span></li>


                        </ul>
                    </div>
                </div>
            </div>
        </div>
    <!-- /.col-lg-12 -->
    </div>
</div>

@endsection