@extends('layouts.spadmin.admin')

@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>{{ __('messages.Total Sales (Beutics Order)') }}</h5>
            </header>
            <div id="collapse4" class="body">
              @include('message')
              
               <table id="user_datatable" class="table table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th width="10%">{{ __('messages.Order Unique ID') }}</th>
                        <th width="15%">{{ __('messages.Customer Name') }}</th>
                        <th width="35%">{{ __('messages.Services Name') }}</th>
                        <th width="10%">{{ __('messages.Order Amount') }}</th>
                        <th width="10%">{{ __('messages.Order Date') }}</th>
                        <th width="10%">{{ __('messages.Service Closed') }}</th>
                        <th width="10%">{{ __('messages.Action') }}</th>
                        
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
                  
            </div>
        </div>
    </div>
</div>
@endsection

@section('uniquescript')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script type="text/javascript">

 
    $(function() {
        var table = $('#user_datatable').DataTable({
           stateSave: true,
        processing: true,
        serverSide: true,
        order: [[0, "desc" ]],
        "ajax":{

        "url": '{!! route('my-account.gets.allsales') !!}',
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{csrf_token()}}",status:'online'}
        },
        columns: [
        { data: 'booking_unique_id', name: 'booking_unique_id', orderable:true  },
        { data: 'name', name: 'name', orderable:true  },
        { data: 'booking_items', name: 'booking_items', orderable:true  },
        { data: 'total_item_amount', name: 'total_item_amount', orderable:true  },
        { data: 'created_date', name: 'created_date', orderable:true },
        { data: 'service_end_time', name: 'service_end_time', orderable:true  },
        { data: 'action', name: 'action', orderable:true  },
        ],
        "columnDefs": [
        { "searchable": false, "targets": 0 }
        ]
        , language: {
        searchPlaceholder: "{{ __('messages.Search by Customer Name') }}"
        },
});
});  


</script>
@endsection

