@extends('layouts.spadmin.admin')

@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/jquery.gritter/css/jquery.gritter.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/themes/default/css/uniform.default.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.min.css">


@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>{{ __('messages.Banner Image') }}</h5>
          

            </header>
              @include('message')
            <div id="collapse2" class="body">
                <!-- <form class="form-horizontal" id="popup-validation"> -->
                {!! Form::open(['route' => 'spuser.update-gallery', 'method' => 'POST', 'class' => 'form-horizontal','id' => 'popup-validation','enctype' => 'multipart/form-data']) !!}
                {{ csrf_field() }}
                    <div class="form-group">
                        
                        <div class="row">
                            <div class="col-md-4" style="margin-top: 10px; text-align:right"><label class="control-label" style="margin-left: 10px; padding: 0">{{ __('messages.Banner Image') }}</label></div>
                                <div class="col-md-8" style="margin-top: 10px;">
                                    <input type="file" id="image" name="banner_image">
                                    <input type="submit" value="{{ __('messages.Submit') }}" style="margin-top: 15px" class="btn btn-primary">
                                     <br/>
                                    <span style="color:red;text-align:center;">{{ __('messages.Minimum image size should be 1300 X 800.') }}</span>
                                </div>
                         
                          </div>
                    </div>
                  
                </form>
                @foreach($banner_images as $ban_img)
                <div class="form-group disp-inline" style="padding-top: 30px;">
                    <img src="{{asset('sp_uploads/banner_images/'.$ban_img['banner_path'])}}" width="100" height="100" class="banner_img<?= $ban_img->id; ?>"/>
                    <a onclick="deleteBannerImage('<?= $ban_img->id; ?>');" class="banner_img<?= $ban_img->id; ?>">{{ __('messages.Delete') }}</a>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
@endsection
@section('uniquescript')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/holder/2.4.1/holder.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/jquery.uniform.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>
    <script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.js"></script>

    <script>        
        $(function() {
          Metis.formValidation();
        });

    </script>
<script type="text/javascript">
    
    function deleteBannerImage(id)
        {

          swal({
              title: "Are you sure?",
              type: "warning",
              showCancelButton: "No",
              confirmButtonClass: "btn-danger",
              confirmButtonText: "Yes",
              cancelButtonText: "No",
            },
              function(){
                jQuery.ajax({
                url: '{{route('spuser.banner-image-delete')}}',
                type: 'POST',
                data:{banner_id:id},
                headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    
                    if(response == 'success'){
                        $(".banner_img"+id).hide();
                    }
                }
                });
              }
          );
        }
</script>

@endsection

