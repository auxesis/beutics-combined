@extends('layouts.spadmin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.min.css">

<style type="text/css">
    .hwetimes tr td {
    padding: 6px;
}
</style>

@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>{{ __('messages.Shop Timing') }}</h5>

            </header>
            <div id="collapse2" class="body hwetimes">
                <!-- <form class="form-horizontal" id="popup-validation"> -->

                    @include('message')

                    <table>
                        <tr>
                            <td></td>
                            <td>{{ __('messages.Days') }}</td>
                            <td>{{ __('messages.Open Hrs') }}</td>
                            <td>{{ __('messages.Close Hrs') }}</td>
                        </tr>
                        <?php $days_arr = array('1'=>__('messages.Monday'),'2'=>__('messages.Tuesday'),'3'=>__('messages.Wednesday'),'4'=>__('messages.Thursday'),'5'=>__('messages.Friday'),'6'=>__('messages.Saturday'),'7'=>__('messages.Sunday')); ?>

                        {!! Form::open(['route' => 'spuser.update-shop-timings', 'method' => 'POST', 'id' => 'popup-validation', 'enctype' => 'multipart/form-data']) !!}
                        {{ csrf_field() }}
                        <?php $i=0;?>
                        @foreach($row as $key => $rows)
                        <tr>
                            <td>      
                                {{Form::checkbox('isOpen_'.$rows->day, 1,$rows->isOpen,['class' => 'checkbox'])}}

                            </td>

                            <td>
                                {{$days_arr[$rows->day]}}
                            </td>
                            <td>
                               <div class=" input-group clockpicker">
                               <?php
                               $days=[__('messages.Monday'),__('messages.Tuesday'),__('messages.Wednesday'), __('messages.Thursday'), __('messages.Friday'), __('messages.Saturday'), __('messages.Sunday')]; ?>
                                <input type="text" class="form-control check_open" value="{{date('h:i A',strtotime($rows->opening_time))}}" name="opening_time_{{$days[$i]}}">
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-time"></span>
                                    </span>
                                    
                                </div>
                            </td>
                            <td>
                                <div class="input-group clockpicker">
                                    <input type="text" class="form-control check_close" value="{{date('h:i A',strtotime($rows->closing_time))}}" name="closing_time_{{$days[$i] }}">
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-time"></span>
                                    </span>
                                   
                                </div>
                            </td>
                        </tr>
                        <?php $i++;?>
                        @endforeach

                        
                    </table>
                    <div class="form-actions no-margin-bottom">
                        <input type="submit" value="{{ __('messages.Submit') }}" id="submitBtn" class="btn btn-primary">
                    </div>
                    {{Form::close()}}
                    <br><br><br><br>
                </form>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>

 

@endsection
@section('uniquescript')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>
    
    <script src="{{ url('/') }}/assets/lib/jquery-validation/jquery.validate.js"></script>

     <script src="{{ url('/') }}/assets/lib/bootstrap-clockpicker.js"></script>

    <script type="text/javascript">
        $('.clockpicker').clockpicker({
            'donetext':'Done',
            'twelvehour': true,
        });
    </script>

    <script>
        $(function() {
          Metis.formValidation();
        });

        // $(document).ready(function(){
        //     $("#submitBtn").click(function(){

        //     });
        // });
    </script>


@endsection