@extends('layouts.spadmin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css">
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>{{ __('messages.Change Password') }}</h5>

            </header>
            <div id="collapse2" class="body">
                <!-- <form class="form-horizontal" id="popup-validation"> -->
                {!! Form::open(['route' => 'spuser.update-password', 'method' => 'POST', 'class' => 'form-horizontal', 'id' => 'popup-validation']) !!}
                {{ csrf_field() }}

                    @include('message')

                    
                    <div class="form-group">
                        <label class="control-label col-lg-4">{{ __('messages.Old Password') }}</label>

                        <div class=" col-lg-4">
                            <input class="validate[required] form-control" type="password" name="old_password" id="old_password"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-4">{{ __('messages.New Password') }}</label>

                        <div class=" col-lg-4">
                            <input class="validate[required] form-control" type="password" name="new_password" id="new_password"/>
                        </div>
                    </div>   

                    <div class="form-group">
                        <label class="control-label col-lg-4">{{ __('messages.Confirm Password') }}</label>

                        <div class=" col-lg-4">
                            <input class="validate[required,equals[new_password]] form-control" type="password" name="confirm_password" id="confirm_password"/>
                                   
                        </div>
                    </div>  
              

                    <div class="form-actions no-margin-bottom">
                        <input type="submit" value="{{ __('messages.Submit') }}" class="btn btn-primary">
                    </div>
                    <br><br><br><br>
                </form>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>

 

@endsection
@section('uniquescript')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>
    
    <script src="{{ url('/') }}/assets/lib/jquery-validation/jquery.validate.js"></script>

    
    <script>
        $(function() {
          Metis.formValidation();
        });
    </script>


@endsection