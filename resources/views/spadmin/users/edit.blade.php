@extends('layouts.spadmin.admin')

@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/jquery.gritter/css/jquery.gritter.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/themes/default/css/uniform.default.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.min.css">

  
<style type="text/css">
    

.croppie-container {
    width: 100%;
    height: auto!important;
}
    
</style>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>{{$title}}</h5>
                 <h5 style="float:right; ">
                    <?php if($row->is_individual == 1){ $store_type = __('messages.Individual'); }else{  $store_type = __('messages.Store'); }?>
                    <span class="badge badge-secondary" style="background-color:#B22222;padding:6px;">{{$store_type}}</span></h5>

            </header>
              @include('message')
            <div id="collapse2" class="body">
                <!-- <form class="form-horizontal" id="popup-validation"> -->
                {!! Form::model($row,['route' => 'spuser.update', 'method' => 'POST', 'class' => 'form-horizontal','id' => 'popup-validation','enctype' => 'multipart/form-data']) !!}
                {{ csrf_field() }}
                    <input type="hidden" name="id" value={{$row->id}}>
                    <div class="form-group">
                        {{Form::label('name', __('messages.Name'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('name',ucwords($row->name), ['class' => 'form-control validate[required]','readonly']) }}
                        </div>
                    </div>                    

                    <div class="form-group">
                        {{Form::label('store_name', __('messages.Store Name') , ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('store_name',ucwords($row->store_name), ['class' => 'form-control validate[required]','readonly']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('store_number', __('messages.Store Number') , ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('store_number',null, ['class' => 'form-control validate[required]']) }}
                        </div>
                    </div>
                   
                    <input type="hidden" name="country_code" value="971">
                    <div class="form-group">
                        {{Form::label('email', __('messages.Email') , ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            <div class="inner-addon right-icons-1 right-addon">
                                 <i style="color:red;">
                                    <?php
                                        if($row->verifyToken!=""){
                                    ?>
                                    <a id="resendmail" style="cursor:pointer;">{{ __('messages.Resend (unverified)') }}</a>
                                    <?php
                                        }else

                                        {
                                            echo __('messages.Verified');
                                        }
                                    ?>
                                 </i>
                                {{ Form::text('email',null, ['class' => 'form-control validate[required]']) }}
                                <span id="resend_span" style="color:red;"></span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('mobile_no', __('messages.Mobile No') , ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            <div class="inner-addon right-icons right-addon">
                                 <i data-toggle="modal" data-target="#numberchange" style="color:#0C78B6;" onmouseover="changePointer();" id="mobchange">{{ __('messages.Change') }}</i>
                                <span class="hnumber">+971</span>
                                {{ Form::text('mobile_no',null, ['class' => 'form-control validate[required]','id'=>'main_mobile_no','readonly']) }}
                     
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        {{Form::label('whatsapp_no', __('messages.Whatsapp No') , ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            <div class="inner-addon right-icons right-addon">
                             <span class="hnumber">+971</span>
                            {{ Form::text('whatsapp_no',null, ['class' => 'form-control validate[required]']) }}
                        </div>
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('address', __('messages.Address') , ['class' => 'control-label col-lg-4'])}}
                        <div class="col-sm-4">
                             
                            {!! Form::text('address', null, ['maxlength'=>'255','placeholder' => 'Address', 'required'=>true, 'class'=>'form-control', 'id'=>'autocomplete']) !!} 
                            {!! Form::hidden('latitude', null, ['class'=>'form-control', 'id'=>'latitude']) !!}
                            {!! Form::hidden('longitude', null, ['class'=>'form-control', 'id'=>'longitude']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="country" class="control-label col-lg-4">{{__('messages.Country')}}</label>
                        <div class="col-lg-4">
                            <select class="form-control validate[required] myselect" name="country" id="country">
                                <option value="">Select Country</option>
                                @foreach($country_list as $country)
                                    <option value="{{ $country->iso2.'/'.$country->name }}" data-iso2code="{{$country->iso2}}" {{($country->iso2 == $user_countries['country_isocode']) ? 'selected' : '' }}>{{ $country->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    @php 
                        $showstate = ($user_countries[0]['state_isocode'] !=null) ? 'block' : 'none';
                        $showcity = ($user_countries[0]['city_isocode'] !=null) ? 'block' : 'none';
                    @endphp
                    <div class="form-group" id="statebox" style="display: {{$showstate}};">
                        <label for="state" class="control-label col-lg-4">{{__('messages.State')}}</label>
                        <div class="col-lg-4">
                            <select class="form-control validate[required] myselect" name="state" id="state">
                            </select>
                        </div>
                    </div>
                    <div class="form-group" id="citybox" style="display: {{$showstate}};">
                        <label for="city" class="control-label col-lg-4">{{__('messages.City')}}</label>
                        <div class="col-lg-4">
                            <select class="form-control validate[required] myselect" name="city" id="city">
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('sub_locality', __('messages.Sub Locality'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('sub_locality',null, ['class' => 'form-control validate[required]','maxlength'=>'1000']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('landmark', __('messages.Landmark') , ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('landmark',null, ['class' => 'form-control validate[required]','maxlength'=>'1000']) }}
                        </div>
                    </div>

                     <div class="form-group">
                        {{Form::label('amenity', __('messages.Amenities') , ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            @foreach($amenities as $key => $am)

                               {{Form::checkbox('amenity[]',$key ,false,['checked'=>(in_array($key,$usr_amen))])}}{{ucfirst($am)}}
                            @endforeach
                        </div>
                    </div>
                    
                    <div class="form-group">
                        {{Form::label('service_criteria', __('messages.Service Criteria') , ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            <?php $sc = explode(',',$row->service_criteria);?>

                            {{Form::checkbox('service_criteria[]', 'men' ,false,['checked'=>(in_array('men',$sc))])}}{{ __('messages.Men') }}

                            {{Form::checkbox('service_criteria[]', 'women' ,false,['checked'=>(in_array('women',$sc))])}}{{ __('messages.Women') }}

                            {{Form::checkbox('service_criteria[]', 'kids' ,false,['checked'=>(in_array('kids',$sc))])}}{{ __('messages.Kids') }}

                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('description', __('messages.About Store') , ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::textarea('description',null, ['class' => 'form-control', 'rows' => 3, 'cols' => 40]) }}
                        </div>
                    </div>

                    

                    <div class="form-group">
                        <label class="control-label col-lg-4">{{ __('messages.Profile Image') }}</label>
                        <div class="row">
                            <div class="col-md-4">
                            <div id="ImgView">
                                @if($row['profile_image'])
                                <img src="{{changeImageUrlForFileExist(asset('sp_uploads/profile/'.$row['profile_image']))}}" style="height:200px;width:200px;">
                                @endif
                            </div>
                            <div id="upload-demo" style=" display: none;">
                            </div>
                                <input type="file" id="image" onclick="showHideDiv(this)" name="profile_image">

                            </div>
                           
                       
                    </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('account_title', __('messages.Bank Account Title') , ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('account_title',null, ['class' => 'form-control ']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('bank', __('messages.BANK NAME & BRANCH NAME') , ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('bank',null, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('account_iban', __('messages.BANK ACCOUNT NUMBER') , ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('account_iban',null, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('branch_name', __('messages.IBAN NUMBER') , ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('branch_name',null, ['class' => 'form-control ']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('branch_address', __('messages.Bank Address') , ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('branch_address',null, ['class' => 'form-control ']) }}
                        </div>
                    </div>
                    @if($row->is_provide_home_service == '1')
                    <div class="form-group">
                        {{Form::label('area_description', __('messages.Area Description') , ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::textarea('area_description',null, ['class' => 'form-control ']) }}
                        </div>
                    </div>
                    @endif
                   
                     <!-- <div class="form-group " >
                        <label class="control-label col-lg-4">Banner Images</label>
                        <div class="col-lg-8">
                        
                        <div class="row">
                            <div class="col-md-10">
                                <div class="row">

                                     <div class="col-md-6">
                                        <div class="upload-demo-banner"></div>
                                     <div class="input_fields_container">
                                     <?php $countClass =count($banner_images)+1;
                                     $class='banner_cr_'.$countClass;?>
                                     <?= Form::hidden('banner_cr[]',null, ['class' =>$class]) ?>
                                       <input type="file" name="banner_path[]" accept= "image/*" class="resize_banner" onchange="bannerImgChange(this)" /></div></div>
                                      <div class="col-md-6">
                                          <a class="btn btn-sm btn-primary" onclick="bannerResize(this)"  style="margin-bottom:10px">Ok</a>
                                          <button class="btn btn-sm btn-primary add_more_button" style="display: none;" id="addButton"  style="margin-bottom:10px">Add More</button>

                                      </div>
                                </div>
                               
                            </div>
                        </div>
                        
                        <div id="more_image" style="display: none;">
                            <?= Form::hidden('banner_cr[]',null, ['class' => 'banner_cr']) ?>
                            <div class="row" style="margin-top: 10px;">
                            <div class="upload-demo-banner"></div>
                            <div class="col-md-10">
                                <input type="file" name="banner_path[]"  accept= "image/*"  class="resize_banner" onchange="bannerImgChange(this)" />
                            </div>
                            <div  class="col-md-2">
                                <a href="#" class="remove_field" style="margin-bottom:10px;">Remove</a>
                                 <a class="btn btn-sm btn-primary" onclick="bannerResize(this)"  style="margin-bottom:10px">Ok</a>
                            </div>
                        </div>
                        </div>
                        

                        @foreach($banner_images as $ban_img)
                        <div class="form-group disp-inline" style="padding-top: 30px;">
                            <img src="{{asset('sp_uploads/banner_images/'.$ban_img['banner_path'])}}" width="100" height="100" class="banner_img<?= $ban_img->id; ?>"/>
                            <a onclick="deleteBannerImage('<?= $ban_img->id; ?>');" class="banner_img<?= $ban_img->id; ?>">Delete</a>
                        </div>
                        @endforeach
                        </div>
                    </div> -->
                    <?= Form::hidden('image_cr',null, ['id' => 'image_cr']) ?>
                    <div class="form-actions no-margin-bottom">
                        <input type="submit" value="{{ __('messages.Submit') }}" class="btn btn-primary">
                    </div>
                    <br><br><br><br>
                </form>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
@endsection
@section('uniquescript')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/holder/2.4.1/holder.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/jquery.uniform.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>
    <script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCNVV0WGqduJq4EsX8_Y_s8L-hiZrHmrj4&libraries=places&callback=initialize"
        async defer></script>
<script type="text/javascript">
    function bannerResize()
    {
        var classname='{{ $class}}';
        resize_banner.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (img) {
                $('.'+classname).val(img);
            });
        $('#addButton').show();
        console.log('done');
    }
</script>
        <script type="text/javascript">
            function showHideDiv()
            {
                $('#ImgView').hide();
                $('#upload-demo').show();
            }
            function changePointer()
            {
                $("#mobchange").css('cursor','pointer');
            }
        </script>

    <script>

        $('#popup-validation').submit(function(e)
        {
            e.preventDefault();
            obj = $(this);
            resize.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (img) {
                $('#image_cr').val(img);
               obj.unbind('submit').submit();
            });
        });

        
        $(function() {
          Metis.formValidation();
        });
        // image crop
        var resize = $('#upload-demo').croppie({
            enableExif: true,
            enableOrientation: true,    
            viewport: { // Default { width: 100, height: 100, type: 'square' } 
                width: 200,
                height: 100,
                type: 'square' //square
            },
            boundary: {
                width: 300,
                height: 300
            }
        });

        $('#image').on('change', function () { 
          var reader = new FileReader();
            reader.onload = function (e) {
              resize.croppie('bind',{
                url: e.target.result
              }).then(function(){
                console.log('jQuery bind complete');
              });
            }
            reader.readAsDataURL(this.files[0]);
        });

        var resize_banner = $('#upload-demo-banner').croppie({
            enableExif: true,
            enableOrientation: true,    
            viewport: { // Default { width: 100, height: 100, type: 'square' } 
                width: 200,
                height: 200,
                type: 'square' //square
            },
            boundary: {
                width: 300,
                height: 300
            }
        });


        function bannerImgChange(obj){
                $('#add_more_button').hide();
                resize_banner = $(obj).parent().parent().find('.upload-demo-banner').croppie({
                enableExif: true,
                enableOrientation: true,    
                viewport: { // Default { width: 100, height: 100, type: 'square' } 
                    width: 200,
                    height: 200,
                    type: 'square' //square
                },
                boundary: {
                    width: 300,
                    height: 300
                }
            });
            var reader = new FileReader();
            console.log('here');
            reader.onload = function (e) {
              resize_banner.croppie('bind',{
                url: e.target.result
              }).then(function(){
                console.log('jQuery bind complete');
              });
            }
            reader.readAsDataURL(obj.files[0]);
        }

        // 

        function changeNo()
        {
            var mobile_no = $("#mobile_no").val();
            var country_code = $("#country_code").val();
            jQuery.ajax({
                url: '{{route('ajax.change.mobileno')}}',
                type: 'POST',
                data:{mobile_no:mobile_no,country_code:country_code},
                headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    
                  if(response!='exists'){
                    $("#numberchange").modal('hide');
                    
                    $("#verificationModal").modal('show');
                    $("#moWithNo").val(country_code+','+mobile_no);
                  }else{
                    $(".noexistsvalidation").html('Mobile number already exists');
                  }
                  $("#numberchange").on("hidden.bs.modal", function(){
                            $(".modal-body input").val("");
                            $(".noexistsvalidation").html("");
                        });
                }

            });
        }

        function verify()
        {
            var otp = $("#otp").val();
            var moWithNo = $("#moWithNo").val();
            if(otp!=''){
                jQuery.ajax({
                    url: '{{route('ajax.check.otp')}}',
                    type: 'POST',
                    data:{otp:otp,mobileWithCode:moWithNo},
                    headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (response) {
                      if(response!='not_verified'){
                       
                        $("#verificationModal").modal('hide');
                        $("#main_mobile_no").val(response);
                        
                      }else{
                         $(".noexistsvalidation1").html('Otp is not valid');
                      }

                      $("#verificationModal").on("hidden.bs.modal", function(){
                            $(".modal-body input").val("");
                            $(".noexistsvalidation1").html("");
                        });
                    }
                });
            }else{
                $(".noexistsvalidation1").html('Please enter otp');
            }
        }

        function deleteBannerImage(id)
        {

          swal({
              title: "Are you sure?",
              type: "warning",
              showCancelButton: "No",
              confirmButtonClass: "btn-danger",
              confirmButtonText: "Yes",
              cancelButtonText: "No",
            },
              function(){
                jQuery.ajax({
                url: '{{route('spuser.banner-image-delete')}}',
                type: 'POST',
                data:{banner_id:id},
                headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    
                    if(response == 'success'){
                        $(".banner_img"+id).hide();
                    }
                }
                });
              }
          );
        }

        $(document).ready(function() {
            var max_fields_limit = 10; //maximum input boxes allowed
            
         
            var x = '<?php echo count($banner_images)+1; ?>'; //initlal text box count
            $('.add_more_button').click(function(e){ //on add input button click
                e.preventDefault();
                $('.limit_error').remove();
                if(x < max_fields_limit)
                { //max input box allowed
                    x++; //text box increment
                    var className='banner_cr_'+x;
                    var onclick='bannerResize_'+x;
                    var html='<div id="more_image"><input type="hidden" class="'+className+'" name="banner_cr[]"><div class="row" style="margin-top: 10px;"><div class="upload-demo-banner"></div><div class="col-md-10"><input type="file" name="banner_path[]"   class="resize_banner" onchange="bannerImgChange(this)" /></div><div class="col-md-2"><a href="#" class="remove_field" style="margin-bottom:10px;">Remove</a><a class="btn btn-sm btn-primary" onclick="'+onclick+'"  style="margin-bottom:10px">Ok</a></div></div></div>';
                    $('.input_fields_container').append(html); //add input boxes
                }
                else
                {
                  $(this).after('<span class="limit_error" style="color:red;">You have reached the limit of images uploaded.</span>');
                }
            });
            
            $('.input_fields_container').on("click",".remove_field", function(e){ //user click on remove text
                e.preventDefault(); $(this).parent().parent('div').remove(); 
                x--;
                $('.limit_error').remove();
            })
        });

        $("#resendmail").click(function(){
            jQuery.ajax({
                url: '{{route('spuser.resend.mail')}}',
                type: 'POST',
                data:{},
                headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    
                    if(response == 'success'){
                        $("#resend_span").html('Mail Sent Successfully');
                    }
                }
                });
        });

        //google map 
        $("#autocomplete").change(function(){
          $("#latitude").val('');
          $("#longitude").val('');
          
        });
        function initialize() 
        {
            var input = document.getElementById('autocomplete');
            var options = {};            
           var autocomplete = new google.maps.places.Autocomplete(input, options);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                    var place = autocomplete.getPlace();
                    var lat = place.geometry.location.lat();
                    var lng = place.geometry.location.lng();
                    $("#latitude").val(lat);
                    $("#longitude").val(lng); 
                });
        }
                     
        google.maps.event.addDomListener(window, 'load', initialize);

    </script>

    <script>
        $(document).ready(function(){
            var countryID = "{{ $user_countries['country_isocode'] }}";
            var stateID = "{{ $user_countries['state_isocode'] }}";
            var cityID = "{{ $user_countries['city_isocode'] }}";
            if(stateID !="" || countryID !='')
            {
                getcountryStateCity(countryID,stateID);
            }
            if(cityID !=0 || stateID !="")
            {
                getStateCity(countryID,stateID,cityID);
            }
            $('#country').on('change', function(){
                var countryID = $(this).find(':selected').attr('data-iso2code');
                getcountryStateCity(countryID,'');
                $("#citybox").css("display",'none');
            });
            
            $('#state').on('change', function(){
                var countryID = $("#country").find(':selected').attr('data-iso2code');
                var stateID = $(this).find(':selected').attr('data-iso2code');
                getStateCity(countryID,stateID,'');
            });

            function getcountryStateCity(countryID,stateID)
            {
                if(countryID){
                    $.ajax({
                        url:"{{route('spuser.get-state-list')}}",
                        type: "POST",
                        data: {
                        country_id: countryID,
                        _token: '{{csrf_token()}}' 
                        },
                        dataType : 'json',
                        success:function(html){
                            var inputtype = html.inputtype;
                            if(html.inputtype === 'state' || html.inputtype === 'city')
                            {
                                $("#"+inputtype+"box").show();
                                $("#"+inputtype).empty();
                                $("#"+inputtype).append('<option value="">Select</option>');
                                $.each(html.data,function(key,value){
                                    var isocode = (html.inputtype == 'city') ? value.id : value.iso2;
                                    var optionval = (html.inputtype == 'city') ? value.id+'/'+value.name : value.iso2+'/'+value.name;
                                    var selectedval = (value.iso2 == stateID) ? "selected" : '';
                                    $("#"+inputtype).append('<option value="'+optionval+'" data-iso2code="'+isocode+'" '+selectedval+'>'+value.name+'</option>');
                                });
                            }
                            else
                            {
                                $("#statebox").hide();
                                $("#citybox").css("display",'none');
                            }
                        }
                    }); 
                }else{
                    $('#state').html('<option value="">Select country first</option>');
                    $('#city').html('<option value="">Select state first</option>'); 
                }
            }

            function getStateCity(countryID,stateID,cityID)
            {
                if(stateID){
                    $.ajax({
                        url:"{{route('spuser.get-city-list')}}",
                        type: "POST",
                        data: {
                        country_id: countryID,
                        state_id: stateID,
                        _token: '{{csrf_token()}}' 
                        },
                        dataType : 'json',
                        success:function(html){
                            if(html.data.length != 0)
                            {
                                $("#citybox").show();
                                $("#city").empty();
                                $("#city").append('<option value="">Select</option>');
                                $.each(html.data,function(key,value){
                                    var selectedval = (value.id == cityID) ? "selected" : '';
                                    $("#city").append('<option value="'+value.id+'/'+value.name+'" data-iso2code="'+value.id+'" '+selectedval+'>'+value.name+'</option>');
                                });
                            }
                            else
                            {
                                $("#citybox").hide();
                            }
                        }
                    }); 
                }else{
                    $('#city').html('<option value="">Select state first</option>'); 
                }
            }
        });
    </script>



<div id="numberchange" class="modal fade" role="dialog" data-backdrop="static">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color:red;"><center>{{ __('messages.Change Number') }}</center></h4><br/>
        <p align="center">{{ __('messages.Enter your mobile number below to receive verification code.') }}</p>
      </div>
      <div class="modal-body">
    
        <div class="row" style="margin-left:141px;">
            <div class="col-lg-12">
                 <div class="form-group">
                    <div class="col-lg-8">
                        <div class="inner-addon right-icons right-addon">
                             <span class="hnumber">+971</span>
                             <input type="hidden" id="country_code" value="971">
                            {{ Form::text('','', ['class' => 'form-control validate[required]','placeholder'=>__('messages.New Mobile No'),'id'=>'mobile_no','maxlength'=>'15']) }}
                            <span class="noexistsvalidation"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <center>
        <div class="row">
            <div class="col-lg-12">
                 <div class="form-actions no-margin-bottom">
                    <button class="btn btn-primary" onclick="changeNo()">{{ __('messages.Submit') }}</button>
                </div>
            </div>
        </div>
    </center>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('messages.Close') }}</button>
      </div>
    </div>

  </div>
</div>
 <input type="hidden" id="moWithNo">
<div id="verificationModal" class="modal fade" role="dialog" data-backdrop="static">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color:red;"><center>{{ __('messages.Verification') }}</center></h4><br/>
        <p align="center">{{ __('messages.To Verify your mobile, we have sent an SMS to phone number you have entered.') }}</p>
      </div>
      <div class="modal-body">
    
        <div class="row" style="margin-left:141px;">
            <div class="col-lg-12">
               
                 <div class="form-group">
                    <div class="col-lg-8">
                        <div class="inner-addon right-icons right-addon">
                             
                            {{ Form::text('','', ['class' => 'form-control validate[required]','placeholder'=>__('messages.Enter Verification Code'),'id'=>'otp','maxlength'=>'6']) }}
                            <span class="noexistsvalidation1"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <center>
        <div class="row">
            <div class="col-lg-12">
                 <div class="form-actions no-margin-bottom">
                    <button class="btn btn-primary" onclick="verify()">{{ __('messages.Submit') }}</button>
                </div>
            </div>
        </div>
    </center>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('messages.Close') }}</button>
      </div>
    </div>

  </div>
</div>
@endsection