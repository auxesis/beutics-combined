<!DOCTYPE html>
<html lang="en">

<head>
    <meta property="og:title" content="Beutics" />
      <meta property="og:type" content="website" />
      <meta property="og:url" content="http://app.beutics.com/service-provider" />
      <meta property="og:image" content="{{asset('assets/img/scratch_card.png')}}">
  
    <meta charset="UTF-8">
    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Login</title>
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ url('/') }}/assets/lib/bootstrap/css/bootstrap.css">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ url('/') }}/assets/lib/font-awesome/css/font-awesome.css">
    
    <!-- Metis core stylesheet -->
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/main.css">
    
    <style>
        .error{
            color:red;
        }

        .hhlolgin .hnumber {
            top: 12px;
            left: 14px;
            z-index: 10;
        }

        .hhlolgin input {
            padding-left: 50px !important;
        }

    </style>
</head>

<body class="login">

    <div class="form-signin">
        <div class="text-center">
            <img src="{{ url('/') }}/assets/img/logo.png" height="122px" alt="Metis Logo">
        </div>
        <hr>
    <div class="tab-content">
        <div id="login" class="tab-pane active">
            @include('message')
            <h4 align="center" style="color:#4d855a;">Service Provider</h4>

                {!! Form::open(['route' => 'spadmin.make.login', 'method' => 'POST', 'class'=>'validate login100-form validate-form']) !!}
                {{ csrf_field() }}
                
                <input type="hidden" name="country_code" value="971"/>
                <div class="inner-addon hhlolgin right-addon">
                     <span class="hnumber">+971</span>
                    {{ Form::text('mobile_no','', ['class' => 'form-control top', 'placeholder' => 'Mobile No', 'required' => true]) }}
                </div>

                {{ Form::password('password', array('placeholder' => 'Password', "class" => "form-control", 'required' => true)) }}

                 @if($errors->has('password'))
                <div class="error">{{ $errors->first('password') }}</div>
                @endif

                
                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            </form>
        </div>
    </div>
    <br>
    <div class="text-center">
        <ul class="list-inline">

            <li><a href="{{route('front.sp.forgotpassword')}}" class="text-muted">Forgot Password</a></li>
            <li><a href="{{route('front.sp.signup')}}" class="text-muted">Become a Service Provider</a></li>
        </ul>
    </div>
  </div>

    <!--jQuery -->
    <script src="{{ url('/') }}/assets/lib/jquery/jquery.js"></script>

    <!--Bootstrap -->
    <script src="{{ url('/') }}/assets/lib/bootstrap/js/bootstrap.js"></script>
    <script src="{{ url('/') }}/assets/js/jquery.validate.min.js"></script>
    <script>$(".validate").validate();</script>
</body>

</html>
