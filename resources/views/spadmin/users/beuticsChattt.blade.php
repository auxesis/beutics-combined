@extends('layouts.spadmin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
 
@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
             <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>{{$title}}</h5>
            </header>
            <div id="collapse4" class="body">
              @include('message')
              
               @if(!empty($chatlist))
                <div class="messaging">
                  <div class="inbox_msg">
                    <div class="inbox_people">
                      <div class="headind_srch">
                        <div class="recent_heading">
                          <h4>Recent</h4>
                        </div>
                    
                      </div>
                      <div class="inbox_chat">

                        <div class="listing-chater chat_list active_chat" other-id="1" other-name="Admin" booking-id="0" other-image="https://ptetutorials.com/images/user-profile.png">
                          <div class="chat_people">
                            <div class="chat_img" > 
                              <img src="https://ptetutorials.com/images/user-profile.png" style="border-radius: 50%;height: 33px;" alt="Admin"> 
                            </div>
                            <div class="chat_ib">
                              <h5>Admin @if(!empty($adminChatList))<span class="chat_date">{{date('M d Y', strtotime(@$adminChatList[0]->updated_at))}}</span> @endif</h5>
                              
                              @if(!empty($adminChatList))
                                <p>{{@$adminChatList[0]->message}}</p>
                               @endif
                            </div>
                          </div>
                        </div>

                        @foreach($chatlist as $key=>$row)

                        <?php $other_userimage =($row->other_user_id == $user['id'] && $row->other_user_id_tbl =='sp_users')?$row->user_image:$row->other_user_image; ?>

                        <?php  

                          $booking_id= $row->booking_id;

                          $other_user_id= ($row->other_user_id == $user['id'] && $row->other_user_id_tbl =='sp_users')?$row->user_id:$row->other_user_id;
                          $other_user_image= (!empty($other_userimage))?url('/public/'.$other_userimage):'https://ptetutorials.com/images/user-profile.png';
                          $other_user_name= ucfirst(($row->other_user_id == $user['id'] && $row->other_user_id_tbl =='sp_users')?$row->user_name:$row->other_user_name);

                          // if($key==0){
                          //       $other['id']          = $other_user_id;
                          //       $other['image']       = $other_user_image;
                          //       $other['full_name']   = $other_user_name;
                          //       $other['booking_id']   = $booking_id;
                          // }
                        ?>
                        <div class="listing-chater chat_list" other-id="{{$other_user_id}}" other-name="{{@$other_user_name}}" booking-id="{{$booking_id}}" other-image="{{@$other_user_image}}">
                          <div class="chat_people">
                            <div class="chat_img" > 
                              <img src="{{(!empty($other_userimage))?url('/public/'.$other_userimage):'https://ptetutorials.com/images/user-profile.png'}}" style="border-radius: 50%;height: 33px;" alt="{{ ucfirst(($row->other_user_id == $user['id'] && $row->other_user_id_tbl =='sp_users')?$row->user_name:$row->other_user_name)}} "> 
                            </div>
                            <div class="chat_ib">
                              <h5>{{ ucfirst(($row->other_user_id == $user['id'] && $row->other_user_id_tbl =='sp_users')?$row->user_name:$row->other_user_name)}} <span class="chat_date">{{date('M d Y', strtotime($row->updated_at))}}</span></h5>
                              <p>{{$row->message}}</p>

                            </div>
                          </div>
                        </div>
                        @endforeach
                      </div>
                    </div>
                    
                    <div class="mesgs" id="other-data" other-id="1" other-name="Admin" other-image="https://ptetutorials.com/images/user-profile.png" booking-id="0">
                      <div class="msg_history" id="messages">
                        @foreach($chats as $chatrow)

                          @if($chatrow['other_user_id']==$user['id'] && $chatrow['other_user_id_tbl']=='admins')
                          <div class="incoming_msg">
                              <div class="incoming_msg_img"> 
                                <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> 
                              </div>
                              <div class="received_msg">
                                <div class="received_withd_msg" >
                                  <p>{{$chatrow['message']}}</p>
                                  <span class="time_date"> {{date('h:i A', strtotime($chatrow['user_time']))}}   |    {{date('M d Y', strtotime($chatrow['user_time']))}}</span>
                                </div>
                              </div>
                          </div>
                          @else
                          <div class="outgoing_msg">
                            <div class="sent_msg" >
                              <p>{{$chatrow['message']}}</p>
                              <span class="time_date"> {{date('h:i A', strtotime($chatrow['user_time']))}}    |    {{date('M d Y', strtotime($chatrow['user_time']))}}</span>
                            </div>
                          </div>

                          @endif

                        @endforeach

                      </div>
                      <div class="type_msg">
                        <form id="chatForm" enctype="multipart/form-data" action="">
                          <input type="hidden" name="type" value="TEXT">
                          <div class="input_msg_write">
                            <input id="m" class="emojiable-option write_msg" type="text" placeholder="Type a message" />
                            <button class="msg_send_btn" onclick="$('form#chatForm').submit();" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                          </div>
                        </form>
                      </div>

                    </div>
                  </div>
                </div>
                @endif                  
            </div>
        </div>
    </div>
</div>
@endsection

@section('uniquescript')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>

<script src="{{url('assets/js/moment.min.js')}}"></script>
<!-- <script src="{{url('assets/js/socket.io-1.2.0.js')}}"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script> -->
<!-- <script src="/socket.io/socket.io.js"></script> -->
<script>

    // var host_url = "http://testing.demo2server.com:4242";
    var baseUrl ="<?php echo url(''); ?>";

    var userId    = '<?php echo $user['id']; ?>';
    var userName  = '<?php echo $user['full_name']; ?>'; 
    var userImage = '<?php echo $user['image']; ?>'; 
    var other     =  $("#other-data");
    var otherId   = other.attr("other-id"); 
    var otherName = other.attr("other-name"); 
    var otherImage= other.attr("other-image");
    var booking_id =other.attr("booking-id");

    var groupId   = (userId>otherId)?userId+""+otherId+""+booking_id:otherId+""+userId+""+booking_id;
            
            
    //alert(userId);
    // var socket = io.connect(host_url,{query:"user_id=SU"+userId+"&user_id_tbl=sp_users&browser_id={{ csrf_token() }}"});

    // socket.on('connect', function () {
    //     console.log('connected');
        
    //     socket.on('disconnect', function () {
    //         console.log('disconnected');
    //     });

    // });

    socket.on('multi_user', function(data){
      console.log('multi_user');
      console.log(data);
      var groupIdChat = (data.data.user_id>data.data.other_user_id)?data.data.user_id+""+data.data.other_user_id+""+data.data.booking_id:data.data.other_user_id+""+data.data.user_id+""+data.data.booking_id;
        var dateTime = new Date(data.data.user_time);

        if(userId == data.data.user_id && data.data.user_id_tbl=='sp_users'){
          $('#messages').append('<div class="outgoing_msg"><div class="sent_msg" ><p>'+data.data.message+'</p><span class="time_date">'+moment.utc(dateTime).local().format('MMM DD, YYYY, h:mm A')+'</span></div></div>') ;
        }else{
          $('#messages').append('<div class="incoming_msg"><div class="incoming_msg_img"><img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"></div><div class="received_msg"><div class="received_withd_msg" ><p>'+data.data.message+'</p><span class="time_date"> '+moment.utc(dateTime).local().format('MMM DD, YYYY, h:mm A')+'</span></div></div></div>');

        }
      $("#messages").animate({ scrollTop: $("#messages").prop('scrollHeight') - $("#messages").position().top }, "slow");
    });


    socket.on('receive_message', function(data){
      console.log('receive_message');
      console.log(data);
      var dateTime = new Date(data.data.user_time);
      var groupIdChat = (data.data.user_id>data.data.other_user_id)?data.data.user_id+""+data.data.other_user_id+""+data.data.booking_id:data.data.other_user_id+""+data.data.user_id+""+data.data.booking_id;

      if(userId == data.data.user_id && data.data.user_id_tbl=='sp_users'){
          $('#messages').append('<div class="outgoing_msg"><div class="sent_msg" ><p>'+data.data.message+'</p><span class="time_date">'+moment.utc(dateTime).local().format('MMM DD, YYYY, h:mm A')+'</span></div></div>') ;
        }else{
          $('#messages').append('<div class="incoming_msg"><div class="incoming_msg_img"><img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"></div><div class="received_msg"><div class="received_withd_msg" ><p>'+data.data.message+'</p><span class="time_date"> '+moment.utc(dateTime).local().format('MMM DD, YYYY, h:mm A')+'</span></div></div></div>');

        }

      $("#messages").animate({ scrollTop: $("#messages").prop('scrollHeight') - $("#messages").position().top }, "slow");
    });


    $(function () {

          $('#chatForm').submit(function(event){
           

                if($.trim($('#m').val())==""){
                  $('#m').val('');
                  $('#m').focus();
                  return false;
                }

                var other_tabel =(other.attr('booking-id')>'0')?'users':'admins';
                var log_obj =
                        {
                            "user_id"       : userId,
                            "other_user_id" : other.attr('other-id'),
                            "upload_img"    : "",
                            "message"       : $('#m').val(),
                            "message_type"  : "Text",
                            "other_data"    : "",
                            "user_id_tbl" :'sp_users',
                            "other_user_id_tbl" :other_tabel,
                            "booking_id":other.attr('booking-id')
                        };

                console.log(log_obj);

                socket.emit('send_message', log_obj);
                $('#m').val('');

                event.preventDefault();

      });
              

  });

  // Get previous chat data .
    $(".listing-chater").click(function(){

        // Replace chat header data.
        $(".listing-chater").removeClass('active_chat');
        $(this).addClass('active_chat');

        // Set other data for chat communication.
        other.attr('other-id',$(this).attr('other-id'));
        other.attr('other-name',$(this).attr('other-name'));
        other.attr('other-image',$(this).attr('other-image'));
        other.attr('booking-id',$(this).attr('booking-id'));

        //loader show
        $('.loading').show();
        // Otp generate verificaiton ajax run 
        $.ajax({
            type : "POST",
            url: baseUrl + '/service-provider/get-chat',
            dataType : "JSON",
            //async:false,
            data:{"_token": "{{ csrf_token() }}",'other_id':$(this).attr('other-id'), 'booking_id':$(this).attr('booking-id')},
            success : function(response){
                console.log(response);
                var chats = "";
                $.each(response.data, function( index, chat ) {

                    var dateTime = new Date(chat.user_time);
                    if(userId == chat.user_id){
                      chats +='<div class="outgoing_msg"><div class="sent_msg" ><p>'+chat.message+'</p><span class="time_date">'+moment.utc(dateTime).local().format('MMM DD, YYYY, h:mm A')+'</span></div></div>' ;
                    }else{
                      chats +='<div class="incoming_msg"><div class="incoming_msg_img"><img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"></div><div class="received_msg"><div class="received_withd_msg" ><p>'+chat.message+'</p><span class="time_date"> '+moment.utc(dateTime).local().format('MMM DD, YYYY, h:mm A')+'</span></div></div></div>';

                    }

                });
                
                $("#messages").html(chats);

                  $("#messages").animate({ scrollTop: $("#messages").prop('scrollHeight') - $("#messages").position().top }, "slow");

                $('.loading').hide();
            },
            error: function (request, status, error) {
                //loader hide
                $('.loading').hide();
                return false;
            }
        });
    })
</script>

@endsection

