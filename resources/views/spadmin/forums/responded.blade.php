@extends('layouts.spadmin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
 
@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>{{ __('messages.Responded Queries') }}</h5>
                
            </header>
            <div id="collapse4" class="body">
              @include('message')
              
               <table id="user_datatable" class="table table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <!-- <th>ID </th> -->
                        <th>{{ __('messages.Subject') }} </th>
                        <!-- <th>Message</th>-->
                        <th>{{ __('messages.Date') }}</th>
                        <th>{{ __('messages.Username') }}</th>
                        <th>{{ __('messages.Action') }}</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
                  
            </div>
        </div>
    </div>
</div>
@endsection

@section('uniquescript')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>
<script type="text/javascript"> 
    
     function deleteRow(obj)
      {
        event.preventDefault(); // prevent form submit
        swal({
          title: "{{ __('messages.Are you sure?') }}",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          cancelButtonText: "No",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm){
          if (isConfirm) {        
           obj.submit();

          } else {
             swal.close();
          }
        });
      }

    $(function() {
        var table = $('#user_datatable').DataTable({
           stateSave: true,
        processing: true,
        serverSide: true,
        order: [[0, "desc" ]],
        "ajax":{
        "url": '{!! route('forum.customer.getresponded') !!}',
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{csrf_token()}}",status:'pending'}
        },
        columns: [
        // { data: 'id', name: 'id', orderable:true },
        { data: 'subject', name: 'subject', orderable:true },
        //{ data: 'message', name: 'message', orderable:true  },
        { data: 'date', name: 'date', orderable:true  },
        { data: 'username', name: 'username', orderable:true  },
        { data: 'action', name: 'action', orderable:true  },
        ],
        "columnDefs": [
        { "searchable": false, "targets": 0 }
        ]
        , language: {
        searchPlaceholder: "{{ __('messages.Search Queries') }}"
        },
});
});  


</script>
@endsection

