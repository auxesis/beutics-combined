@extends('layouts.spadmin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $('#addmore').click(function(){
      var count = parseInt($('.append_here').attr('data-count'));
      if(count <= 4){
        count++;
        $('.append_here').append('<input type="file" class="form-control" id="images'+count+'" name="image'+count+'" accept="image/*">');
        $('.append_here').attr('data-count',count);
      }
      else $('#addmore').remove();

      if(count == 5) $('#addmore').remove();
      
    });
  });
</script>
 
@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12 forumcmts">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>{{ __('messages.Forum Detail') }} </h5>
                
            </header>
            <div id="collapse4" class="body">

              <h4>{{$forum['subject']}}</h4>
              <p><i>{{ __('messages.Requested By') }}</i> {{$username}} &nbsp; &nbsp; <i>{{ __('messages.Posted On') }}</i> {{ date('M d Y h:i A', strtotime($posted_on))}}</p>

              @include('message')
                
              @if($response_check == 0)
              <button type="button" class="btn btn-info btn-lg pull-right" style="margin-top:-56px;" data-toggle="modal" data-target="#myModal">{{ __('messages.Reply to Forum') }}</button>
          
              <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">{{ __('messages.Reply to Forum') }}</h4>
                    </div>
                    <div class="modal-body">
                      
                      <form action="" method="post" enctype="multipart/form-data">

                          {{ csrf_field() }}

                          <div class="form-group">
                            <label for="email">{{ __('messages.Message') }}:</label>
                            <textarea class="form-control" width="100%"  rows="4" cols="50" name="message" id="message" required="required"></textarea>
                          </div>
                          <div class="form-group">
                            <label for="pwd">{{ __('messages.Images') }}:</label>
                            <input type="file" class="form-control" id="images1" name="image1" accept="image/*">

                            <div data-count="1" class="append_here">

                            </div>

                          </div>

                          <div class="form-group">
                            <a href="#" id="addmore">{{ __('messages.Add More') }}</a>
                          </div>
                          <button type="submit" class="btn btn-info btn-lg ">{{ __('messages.Submit') }}</button>
                        </form>

                    </div>
                    
                  </div>

                </div>
              </div>              
              @endif

           
               
                      @foreach($banners as $image)
                          @if($image['banner_path'])
                         
                            <div class="imgss">
                           <a href="#"
                              class="thumbnail"
                              data-image-id="" 
                              data-toggle="modal" 
                              data-title=""
                              data-image={{ asset('/public/sp_uploads/forum/'.$image['banner_path']) }}
                              data-target="#image-gallery"
                            >
                              <img src={{ asset('/public/sp_uploads/forum/'.$image['banner_path']) }} width="100%"/>
</a>
                            </div>
                          
                          @endif
                      @endforeach



                
                      <div class="row">
                      <div class="col-lg-12">
                      <p class="descp"><?= $forum['message']; ?></p>
                      </div></div>
                      
                     <h5>{{$responses}} {{ __('messages.Responses') }}</h5>
                     
                      <div>
                        <ul class="fqclist">
                        @foreach($responses_list as $comment_item) 
                        <li>                           
                            <div class="fimages">
                              @if($comment_item['sp_image'])
                                <img src={{ asset('/public/sp_uploads/profile/'.$comment_item['sp_image']) }} width="50" />
                              @endif

                              @if($comment_item['user_image'])
                              <img src={{ asset('/public/'.$comment_item['user_image']) }} width="50" />
                              @endif

                              @if((!$comment_item['sp_image'] || $comment_item['sp_image'] =='') && (!$comment_item['user_image'] || $comment_item['user_image'] ==''))
                              
                              <img src={{ asset('/public/images/defualt.jpeg') }} width="50" />

                              @endif
                              </div>

                              <div class="fqlist-right">
                              <h4>
                                {{$comment_item['sp_username']}}{{$comment_item['username']}}                                
                                @if($comment_item['store_name'])
                                   <span>({{$comment_item['store_name']}})</span>
                                @endif
                              </h4>
                          
                              <p>{{ date('M d Y h:i A', strtotime($comment_item['date']))}}</p>
                            </div>                          
                            <p  class="descp">{{ $comment_item['message']}}</p>
                            
                            @if($comment_item['getAssociatedCommentBannerImages'])
                                 @foreach($comment_item['getAssociatedCommentBannerImages'] as $banner_img)   
                                 
                                 <div class="imgss">                               
                                 <img src={{asset('/public/sp_uploads/forum_reply_banners/'.$banner_img->banner_path)}}  width="100%" />
                                 </div>

              

                                 @endforeach
                            @endif                          

                            <?php 
                              $numberOfLikes = 0;
                              $numberOfDisLikes = 0;

                              $like_detect = 0;
                              $dislike_detect = 0;

                              $lkDslk = $comment_item['getAssociatedLikeDislike'];
                              foreach($lkDslk as $check){
                                if($check->isLike == '1'){
                                  $numberOfLikes++;
                                  if($check->sp_id == $sp_user_id)
                                  $like_detect = 1;
                                }
                                else{
                                  $numberOfDisLikes++;
                                  if($check->sp_id == $sp_user_id)
                                  $dislike_detect = 1;
                                } 
                              }
                            ?>
                            
                       <div class="row nomargin">
                         <div class="col-md-6">
                         <form action="" method="post">
                                  {{ csrf_field() }}
                                  <input type="hidden" name="comment_id" value="<?php echo $comment_item['id']; ?>">
                                  <input type="submit" class="likes <?php if($like_detect != '1') echo 'disable'; ?>" 
                                  <?php if($like_detect == '1') echo ' disabled'; ?>                                   
                                  value="Like" name="like">
                                </form>
                              <?php
                              echo  __('messages.Likes'); 
                              ?> : <?php echo $numberOfLikes; ?>
                         </div>
                         <div class="col-md-6">
                              <form action="" method="post">
                                  {{ csrf_field() }}
                                  <input type="hidden" name="comment_id" value="<?php echo $comment_item['id']; ?>">
                                  <input type="submit" class="dislikes <?php if($dislike_detect != '1') echo 'disable'; ?>"

                                   <?php if($dislike_detect == '1') echo ' disabled'; ?> 

                                   value="DisLike" name="dislike">
                                </form>
                              <?php
                              
                              echo  __('messages.DisLikes'); 
                              ?> : <?php echo $numberOfDisLikes; ?>  
                         </div>
                       </div>
                                                            
                                                              
                             
                                
                        
                                
                   
                                
                          

                           
                          </li>
                        @endforeach
                        </ul>
                      </div>
                    


            
                  
            </div>
        </div>
    </div>

    <div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="image-gallery-title"></h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <img id="image-gallery-image" class="img-responsive" src="">
                    </div>
                    <div class="modal-footer galfooter">
                        <button type="button" class="btn btn-secondary previous" id="show-previous-image"><i class="fa fa-arrow-left"></i>
                        </button>

                        <button type="button" id="show-next-image" class="btn btn-secondary next"><i class="fa fa-arrow-right"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>

<script>
let modalId = $('#image-gallery');

$(document)
  .ready(function () {

    loadGallery(true, 'a.thumbnail');

    //This function disables buttons when needed
    function disableButtons(counter_max, counter_current) {
      $('#show-previous-image, #show-next-image')
        .show();
      if (counter_max === counter_current) {
        $('#show-next-image')
          .hide();
      } else if (counter_current === 1) {
        $('#show-previous-image')
          .hide();
      }
    }

    /**
     *
     * @param setIDs        Sets IDs when DOM is loaded. If using a PHP counter, set to false.
     * @param setClickAttr  Sets the attribute for the click handler.
     */

    function loadGallery(setIDs, setClickAttr) {
      let current_image, selector, counter = 0;
      $('#show-next-image, #show-previous-image')
        .click(function () {
          if ($(this)
            .attr('id') === 'show-previous-image') {
            current_image--;
          } else {
            current_image++;
          }

          selector = $('[data-image-id="' + current_image + '"]');
          updateGallery(selector);
        });

      function updateGallery(selector) {
        let $sel = selector;
        current_image = $sel.data('image-id');
        $('#image-gallery-title').text($sel.data('title'));
        $('#image-gallery-image').attr('src', $sel.data('image'));       
        disableButtons(counter, $sel.data('image-id'));
      }

      if (setIDs == true) {
        $('[data-image-id]')
          .each(function () {
            counter++;
            $(this)
              .attr('data-image-id', counter);
          });
      }
      $(setClickAttr)
        .on('click', function () {
          updateGallery($(this));
        });
    }
  });

// build key actions
$(document)
  .keydown(function (e) {
    switch (e.which) {
      case 37: // left
        if ((modalId.data('bs.modal') || {})._isShown && $('#show-previous-image').is(":visible")) {
          $('#show-previous-image')
            .click();
        }
        break;

      case 39: // right
        if ((modalId.data('bs.modal') || {})._isShown && $('#show-next-image').is(":visible")) {
          $('#show-next-image')
            .click();
        }
        break;

      default:
        return; // exit this handler for other keys
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)
  });

</script>

</div>
@endsection