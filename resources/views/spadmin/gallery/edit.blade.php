@extends('layouts.spadmin.admin')

@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/jquery.gritter/css/jquery.gritter.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/themes/default/css/uniform.default.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.min.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>{{$title}}</h5>
          

            </header>
             @include('message')
            <div id="collapse2" class="body">
                <!-- <form class="form-horizontal" id="popup-validation"> -->
                {!! Form::model($rows,['route' => ['my-gallery.update',$rows->id], 'method' => 'PATCH', 'class' => 'form-horizontal','id' => 'popup-validation','enctype' => 'multipart/form-data']) !!}
                {{ csrf_field() }}
                    
                    <div class="form-group">
                        {{Form::label('tags', __('messages.Tag Name'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            <select name="tags[]" class="form-control validate[required] myselectTags" multiple="multiple" >
                                @foreach ($all_tags as $tag_key => $tag_val) 
                                    <option value="{{ $tag_key }}" {{ in_array($tag_key, explode(', ', $rows->tag_id)) ? 'selected' : '' }}>{{ $tag_val }}</option>   
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('title', __('messages.Title'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('title',null, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    @php
                        $mimetype = substr($rows->mime_type,0,5);
                    @endphp
                    <div class="form-group">
                        <label class="control-label col-lg-4">{{ __('messages.File Name') }}</label>
                        <div class="col-lg-4">
                            <div class="radio-toolbar">
                                <input type="radio" id="radioImage" name="file_name" value="image" data-name="image" {{ ($mimetype == 'image' ? 'checked' : '')}}>
                                <label for="radioImage">Add Image</label>

                                <input type="radio" id="radioVideo" name="file_name" value="video" data-name="video" {{ ($mimetype == 'video' ? 'checked' : '')}}>
                                <label for="radioVideo">Add Video</label>

                                <input type="radio" id="radioUrl" name="file_name" value="url" data-name="url" {{ ($mimetype == 'url' ? 'checked' : '')}}>
                                <label for="radioUrl">Add Youtube url</label> 
                            </div>
                        </div>
                    </div>
                    <div class="form-group file_element" id="inputimage" style=" display: none;">
                        <label class="control-label col-lg-4">{{ __('messages.Image') }}</label>
                        <div class="row">
                            <div class="col-md-4">
                                @if($mimetype == 'image')
                                <div id="ImgView">
                                    @if($rows->file_name!='')
                                      <img src="{{changeImageUrlForFileExist(asset('sp_uploads/gallery/image/'.$rows->file_name))}}" alt="..." height="200" width="200">
                                    @else
                                      <img src="{{asset('images/defualt.jpeg')}}" alt="..." height="200" width="200">
                                    @endif
                                </div><br>
                                @endif
                                <div id="upload-demo" style="display: none;"></div>
                                <input class="form-control" type="file" id="image" onclick="showHideDiv(this)" name="file_image" accept="image/*">
                                <?= Form::hidden('image_cr',null, ['id' => 'image_cr']) ?>
                            </div>   
                        </div>
                    </div>
                    
                    <div class="form-group file_element" id="inputvideo" style=" display: none;">
                        @if($mimetype == 'video')
                        <label class="control-label col-lg-4"></label> 
                        <div class="col-md-4">
                            <a href="{{asset('public/sp_uploads/gallery/video/'.$rows->file_name)}}" download> {{$rows->file_name}} </a>
                        </div>
                        @endif
                        <br>
                        <label for="file_video" class="control-label col-lg-4">{{ __('messages.Video') }}</label>
                        <div class="col-md-4">
                            <input class="form-control" name="file_video" type="file" id="file_video" accept="video/*">
                        </div>
                    </div>
                    <div class="form-group file_element" id="inputurl" style=" display: none;">
                        {{Form::label('video_url', __('messages.Video Url'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            <input class="form-control" name="file_url" type="text" value="{{ ($mimetype == 'url') ? $rows->file_name  : ''}}">
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('file_desc', __('messages.Description'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::textarea('file_desc',null, ['class' => 'form-control validate[required] summary-ckeditor','rows'=>'2']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('sort_order', __('messages.Order'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('sort_order',null, ['class' => 'form-control','onkeypress'=>'return isNumber(event)']) }}
                        </div>
                    </div>
                    <div class="form-actions no-margin-bottom">
                        <input type="submit" value="{{ __('messages.Submit') }}" class="btn btn-primary">
                    </div>
                    <br><br><br><br>
                </form>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
@endsection
@section('uniquescript')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/holder/2.4.1/holder.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/jquery.uniform.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="{{ url('/') }}/assets/lib/ckeditor/ckeditor.js"></script>
    <script type="text/javascript">
        function showHideDiv()
        {
            $('#ImgView').hide();
            $('#upload-demo').show();
        }
        $(function() {
           $(".myselectTags").select2();
        });
        $(document).ready(function() {
            var selectedbox = $("input[name='file_name']:checked").attr('data-name');
            $('#input'+selectedbox).show()

            $("input[name='file_name']").change(function(){
                $('.file_element').hide();
                $("input[name='file_name']:checked").each(function(){
                    $('#input'+$(this).attr('data-name')).show();
                });
            })
        });
    </script>
    <script>
        CKEDITOR.replace( 'file_desc' );
        $(function() {
          Metis.formValidation();
        });

        function isNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
        
        $('#popup-validation').submit(function(e){
            e.preventDefault();
            obj = $(this);
            resize.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (img) {
                $('#image_cr').val(img);
               obj.unbind('submit').submit();
            });
        });

        // image crop
        var resize = $('#upload-demo').croppie({
            enableExif: true,
            enableOrientation: true,    
            viewport: { // Default { width: 100, height: 100, type: 'square' } 
                width: 200,
                height: 200,
                type: 'square' //square
            },
            boundary: {
                width: 300,
                height: 300
            }
        });
        $('#image').on('change', function () { 
          var reader = new FileReader();
            reader.onload = function (e) {
              resize.croppie('bind',{
                url: e.target.result
              }).then(function(){
                console.log('jQuery bind complete');
              });
            }
            reader.readAsDataURL(this.files[0]);
        });
        // 
    </script>


@endsection