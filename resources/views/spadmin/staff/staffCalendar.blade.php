@extends('layouts.spadmin.admin')
@section('uniquecss')
<link rel="stylesheet" href="{{ url('/') }}/assets/lib/datepicker/jquery-ui.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css">
@endsection
@section('content')
<div class="row">
  <div class="col-lg-12">
      <div class="box">
          <header>
            <div class="icons"><i class="fa fa-calendar"></i></div>
            <h5>{{ __('messages.Staff Calendar') }}</h5>
            <a class="btn btn-primary pull-right" href="{{route('orders.add_order')}}" style="margin-top:4px;">{{ __('messages.Add Walk-in Order') }}</a>
          </header>
          <div id="collapse4" class="body">
          @include('message')
              {!! Form::open(['route' => 'staffs.staff-calendar', 'method' => 'post', 'class' => 'form-horizontal','id' => 'popup-validation','enctype' => 'multipart/form-data']) !!}
                {{ csrf_field() }}
                 <div class="form-group">
                    
                    <div class="col-lg-4">
                      {{Form::label('selected_date', __('messages.Appointment Date'), ['class' => 'control-label '])}}
                        {{ Form::text('selected_date',$selected_date, ['class' => 'form-control validate[required]','id'=>'datepicker','autocomplete'=>'off']) }}
                    </div>
                     <div class="col-lg-4">{{Form::label('selected_staff', __('messages.Select Staff'), ['class' => 'control-label'])}}
                      <br/>
                          <?php 
                          $staff_arr = array();
                            foreach($staff_name_rows as $row){
                                $staff_arr[$row['id']] = $row['staff_name'];
                            }
                          ?>
                            {{ Form::select('selected_staff[]', $staff_arr,$selected_staff,['class' => 'form-control validate[required] selectpicker','id'=>'staff_listing','multiple'=>'multiple']) }}
                      </div>
                 
                    <div class="col-lg-2">
                        {{Form::label('&nbsp;', '', ['class' => 'control-label'])}}       
                        <input type="submit" value="{{ __('messages.Submit') }}" class="btn btn-primary form-control">
                    </div> </div>
              </form>
              <style>
              .nw_booking_i { background:rgb(244,245,25);padding: 4px 10px;margin-right: 8px;border: 1px solid rgb(186, 187, 18);}
              .con_booking_i { background:rgb(11,243,244);padding: 4px 10px;margin-right: 8px;border: 1px solid rgb(9, 176, 176);}
              .leave_booking_i { background:rgb(162,157,154);padding: 4px 10px;margin-right: 8px;color:#fff;border: 1px solid rgb(70, 69, 68);}
              .nwh_booking_i { background:rgb(147,247,43);padding: 4px 10px;margin-right: 8px;border: 1px solid rgb(99, 183, 11);}
              .conh_booking_i { background:pink;padding: 4px 10px;margin-right: 8px;border: 1px solid #e14560;}
              .comp_booking_i { background:rgb(26, 26, 26);padding: 4px 10px;margin-right: 8px;color:#fff;border: 1px solid rgb(0, 0, 0);}
              .ava_booking_i { background:white;padding: 4px 10px;margin-right: 8px;border: 1px solid #dfdfdf;}

              table {border-bottom:1px solid #ddd !important;}
              .mytable  tr{/*border:none!important;*/ padding: 0px;  }
              .mytable td{ height: 30px; border:none!important;  padding: 0px !important;   }
              .mytable .new{ background-color: rgb(147,247,43); height:30px;border-top:1px solid rgb(147,247,43) !important;}
              .mytable .confirmed_home{ background-color: pink; height:30px;border-top:1px solid pink !important;}
              .mytable .new_shop{ background-color: rgb(244,245,25); height:30px;border-top:1px solid rgb(244,245,25) !important;}
              .mytable .confirmed_shop{ background-color: rgb(11,243,244); height:30px;border-top:1px solid rgb(11,243,244) !important;}
              .mytable .completed{ background-color: rgb(26, 26, 26); height:30px;border-top:1px solid rgb(26, 26, 26) !important;}
              .mytable .leave{ background-color: rgb(162,157,154); height:30px;border-top:1px solid rgb(162,157,154) !important;}
              .mytable .nocolor{background-color: transparent;height:30px;border-top:1px solid #ddd !important;}
              .cm_tb{cursor: pointer;}
              .cm_tb:hover{opacity: 0.8;border:0px solid #ddd !important;}
              .mytable .user_img{ width: 52px;
                height: 52px;
                border-radius: 100%;
                margin: 0 auto;
                display: block; }
              hr{margin:0px 0px 40px 0px !important;color:#ddd;}
              .mytable .time_td{border-top:2px solid #ddd !important;;border-right:1px solid #ddd !important;text-align: center;min-width: 75px;    width: 75px;color:#6e6e6e;}
              .mytable .time_td_thirty{border-right:1px solid #ddd !important;}
              .nocolor-bottom{border-bottom:1px solid #ddd !important;}
              </style>
              <br>
               <table class="table ">
                    <tbody>
                        <tr>
                          <td><span class="nw_booking_i">{{ __('messages.Yellow') }}</span> {{ __('messages.New Booking') }}</td>
                          <td><span class="con_booking_i">{{ __('messages.Blue') }}</span> {{ __('messages.Confirmed Booking') }}</td>
                          <td><span class="leave_booking_i">{{ __('messages.Grey') }}</span> {{ __('messages.Staff on leave') }}</td>
                          <td><span class="nwh_booking_i">{{ __('messages.Green') }}</span> {{ __('messages.New Booking(At Home)') }}</td>
                        </tr>
                        <tr>
                          <td><span class="conh_booking_i">{{ __('messages.Pink') }}</span> {{ __('messages.Confirmed Booking (At Home)') }}</td>
                          <td><span class="comp_booking_i">{{ __('messages.Black') }}</span> {{ __('messages.Service Completed') }}</td>
                          <td><span class="ava_booking_i">{{ __('messages.White') }}</span> {{ __('messages.Booking Available') }}</td>
                          <td>&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
              <?php if(!empty($final_array)){ ?>
                <div class="table table-responsive">
                <table class="table mytable">
                    <tbody>
                        <tr>
                            <th> &nbsp;</th>
                            <?php foreach($final_array as $key => $value){ ?>                               
                                <th>
                                  <?php if($value['staff_image']){ ?>
                                  <img class="user_img" title="<?= $value['staff_name']; ?>" src="<?= $value['staff_image']; ?>" />
                                <?php } else { ?>
                                  <img src="{{asset('images/avatar-profile.jpg')}}" alt="..." title="<?= $value['staff_name']; ?>" class="user_img">
                                  <?php } ?><!-- <div class="text-center" style="margin-top:5px;color:#6e6e6e;"> <?= $value['staff_name']; ?></div> -->
                                </th>                                                               
                            <?php } ?>
                        </tr>

                        <?php foreach($shoptime_slots as $stley => $slot){ ?>
                        <tr>
                            <?php $last_dgt = explode(':', $slot); ?>
                            
                            <?php if($stley){ ?>
                              <?php if($last_dgt[1] != '30' && $last_dgt[1] != '15' && $last_dgt[1] != '45'){ ?>
                              <td class="time_td"> <b> <?= date('h:i A', strtotime($slot)); ?></b></td>
                              <?php } else { ?>
                                <td class="time_td_thirty">  &nbsp; </td>
                              <?php } ?>
                            <?php } else { ?>
                              <td class="time_td">  <b> <?= date('h:i A', strtotime($slot)); ?></b> </td>
                            <?php } ?>
                            
                            <?php foreach($final_array as $key => $value){ ?>
                                <?php foreach($value['booking'] as $booking){ $id = ($booking['order_detail'])?$booking['order_detail']['id']:'1'; 
                                    $staff_id = $value['id']; ?>
                                    <?php if($slot == $booking['time']){ ?>
                                      <td>
                                        <?php if($booking['status'] == 'new-home-booked'){ ?>
                                         <a href="{{route('orders.order_confirm_view', $id)}}" target="_blank" class="cm_tb"><div class="new"></div></a>
                                        <?php } else if($booking['status'] == 'new-shop-booked'){ ?>
                                         <a href="{{route('orders.order_confirm_view', $id)}}" target="_blank" class="cm_tb"><div class="new_shop"></div></a>
                                        <?php } else if( $booking['status'] == 'confirmed-home-booked'){ ?>
                                        <a href="{{route('orders.reschedule_order', $id)}}" target="_blank" class="cm_tb"><div class="confirmed_home"></div></a>
                                        <?php } else if( $booking['status'] == 'confirmed-shop-booked'){ ?>
                                        <a href="{{route('orders.reschedule_order', $id)}}" target="_blank" class="cm_tb"><div class="confirmed_shop"></div></a>
                                        <?php } else if($booking['status'] == 'completed-home-booked' || $booking['status'] == 'completed-shop-booked'){ ?>
                                        <a href="{{route('orders.show_order', [$id, ' '])}}" target="_blank" class="cm_tb"><div class="completed"></div></a>
                                        <?php } else if($booking['status'] == 'leave'){ ?>
                                        <div class="cm_tb" style="cursor: default;"><div class="leave"></div></div>
                                        <?php } else { ?>
                                        <?php 
                                          $s_date = $selected_date.' '.$slot;
                                          if(strtotime($s_date) > strtotime(date('Y-m-d H:i'))){ ?>
                                          <a href="{{route('orders.add_order', [$staff_id, strtotime($selected_date), strtotime($slot)])}}" target="_blank" class="cm_tb">
                                                                                  
                                            <?php if($last_dgt[1] == '45'){ ?>
                                            <div class="nocolor nocolor-bottom"></div>
                                            <?php }  else { ?>
                                            <div class="nocolor"></div>
                                            <?php } ?>                                          
                                          </a>
                                          <?php } else { ?>
                                            <a href="javascript:void(0)" style="cursor: default;">
                                                                                      
                                            <?php if($last_dgt[1] == '45'){ ?>
                                            <div class="nocolor nocolor-bottom"></div>
                                            <?php }  else { ?>
                                            <div class="nocolor"></div>
                                            <?php } ?>                                          
                                          </a>
                                          <?php } ?>
                                        <?php } ?>
                                      </td>
                                    <?php } ?>       
                                <?php } ?>                          
                            <?php } ?>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table></div>
              <?php } ?>
          </div>
      </div>
  </div>
</div>
@endsection

@section('uniquescript')
<script src="{{ url('/') }}/assets/lib/datepicker/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>
<script>
$("#datepicker").datepicker({ 
            dateFormat: 'yy-mm-dd',
            changeYear: true,
            changeMonth: true,
           
            });


    $(document).ready(function() {
        $('#staff_listing').multiselect();
    });

</script>
@endsection

