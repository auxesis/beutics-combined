@extends('layouts.spadmin.admin')

@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/jquery.gritter/css/jquery.gritter.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/themes/default/css/uniform.default.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css">
   <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/datepicker/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.min.css">


@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>{{$title}}</h5>
          

            </header>
             @include('message')
            <div id="collapse2" class="body">
                <!-- <form class="form-horizontal" id="popup-validation"> -->
                {!! Form::open(['route' => 'staffs.editLeave', 'method' => 'POST', 'class' => 'form-horizontal','id' => 'popup-validation','enctype' => 'multipart/form-data']) !!}
                {{ csrf_field() }}
                    <input type="hidden" name="id" value="<?= $id; ?>" />
                     <input type="hidden" name="staff_id" value="<?= $staff_leave['staff_id']; ?>" />
                    <div class="form-group">
                        {{Form::label('from_date', __('messages.From Date'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('from_date',$staff_leave['from_date'], ['class' => 'form-control datepicker leave_from_date validate[required]','autocomplete'=>'off']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('to_date', __('messages.To Date'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('to_date',$staff_leave['to_date'], ['class' => 'form-control leave_to_date datepicker validate[required]','autocomplete'=>'off']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('from_time', __('messages.From Time'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4" id="appointment_time_div_frm">
                            {{ Form::text('from_time',$staff_leave['from_time'], ['class' => 'form-control check_close validate[required]','autocomplete'=>'off']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('to_time', __('messages.To Time'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4" id="appointment_time_div_to">
                             {{ Form::text('to_time',$staff_leave['to_time'], ['class' => 'form-control check_close validate[required]','autocomplete'=>'off']) }}
                        </div>
                    </div>
                    <div class="form-actions no-margin-bottom">
                        <input type="submit" value="{{ __('messages.Update') }}" class="btn btn-primary">
                    </div>
                    <br><br><br><br>
                </form>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
@endsection
@section('uniquescript')
   <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>

     <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/holder/2.4.1/holder.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/jquery.uniform.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="{{ url('/') }}/assets/lib/datepicker/jquery-ui.js"></script>
        <script src="{{ url('/') }}/assets/lib/bootstrap-clockpicker.js"></script>

    <script type="text/javascript">
        $('.clockpicker').clockpicker({
            'donetext':'Done',
            'twelvehour': true,
        });
    </script>
     <script>    
        $(document).ready(function(){
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();
            if(from_date){
                jQuery.ajax({
                url: '{{route('ajax.get.fromtime')}}',
                type: 'POST',
                data:{selected_date:from_date, selected_time:'<?php echo date('H:i', strtotime($staff_leave['from_time'])) ?>' },
                headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                  $("#appointment_time_div_frm").html(response);
                  
                  }
                });
            }
             if(to_date){
                jQuery.ajax({
                url: '{{route('ajax.get.totime')}}',
                type: 'POST',
                data:{selected_date:to_date, selected_time:'<?php echo date('H:i', strtotime($staff_leave['to_time'])) ?>' },
                headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                  $("#appointment_time_div_to").html(response);
                  
                  }
                });
            }
          
        });
        $(function() {
          Metis.formValidation();
        });
        
         $( function() {
          $(".datepicker").datepicker({ 
            dateFormat: 'yy-mm-dd',
            changeYear: true,
            changeMonth: true,
            minDate: 0,
            });
         
        } );
       
        $(".leave_from_date").on("change",function(){
            var selected = $(this).val();
                      
            jQuery.ajax({
            url: '{{route('ajax.get.fromtime')}}',
            type: 'POST',
            data:{selected_date:selected},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
              $("#appointment_time_div_frm").html(response);
              
              }
            });
        });
        $(".leave_to_date").on("change",function(){
            var selected = $(this).val();
                      
            jQuery.ajax({
            url: '{{route('ajax.get.totime')}}',
            type: 'POST',
            data:{selected_date:selected},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
              $("#appointment_time_div_to").html(response);
              
              }
            });
        });
    </script>


@endsection