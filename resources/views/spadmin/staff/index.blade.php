@extends('layouts.spadmin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
 
@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>{{ __('messages.Staff table') }}</h5>
                <a class="btn btn-primary pull-right" href="{{route('staffs.create')}}" style="margin-top:4px;">{{ __('messages.Create Staff') }}</a>
                <!--<a class="btn btn-success pull-right" href="{{route('staffs.staff-calendar')}}" style="margin-top:4px;"> Staff Calendar </a>-->
            </header>
            <div id="collapse4" class="body">
              @include('message')
              
               <table id="user_datatable" class="table table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th width="20%">{{ __('messages.Staff Name') }}</th>
                        <th width="10%">{{ __('messages.Nationality') }}</th>
                        <th width="20%">{{ __('messages.Experience') }}</th>
                        <th width="25%">{{ __('messages.Speciality') }}</th>
                        <th width="5%">{{ __('messages.Provide Home Service') }}</th>
                        <th width="20%">{{ __('messages.Action') }}</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
                  
            </div>
        </div>
    </div>
</div>
@endsection

@section('uniquescript')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>
<script type="text/javascript">

  function deleteRow(obj)
  {
    event.preventDefault(); // prevent form submit
    swal({
      title: "{{ __('messages.Are you sure?') }}",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {        
       obj.submit();

      } else {
         swal.close();
      }
    });
  }
 
    $(function() {
        var table = $('#user_datatable').DataTable({
           stateSave: true,
        processing: true,
        serverSide: true,
        order: [[0, "desc" ]],
        "ajax":{
        "url": '{!! route('staffs.getdata') !!}',
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{csrf_token()}}"}
        },
        columns: [
        { data: 'staff_name', name: 'name', orderable:true },
        { data: 'nationality', name: 'nationality', orderable:true  },
        { data: 'experience', name: 'experience', orderable:true  },
        { data: 'speciality', name: 'speciality', orderable:true  },
        { data: 'is_provide_home_service', name: 'is_provide_home_service', orderable:true  },
        { data: 'action', name: 'action', orderable:true  },
        ],
        "columnDefs": [
        { "searchable": false, "targets": 0 }
        ]
        , language: {
        searchPlaceholder: "{{ __('messages.Search by name,nationality,experience') }}"
        },
});
});  


</script>
@endsection

