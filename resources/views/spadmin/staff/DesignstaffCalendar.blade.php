@extends('layouts.spadmin.admin')
@section('uniquecss')
@endsection
@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="box">
			<header>
				<div class="icons"><i class="fa fa-calendar"></i></div>
				<h5>Staff Calendar</h5>
			</header>
			<div id="collapse4" class="body">
				@include('message')
				{!! Form::open(['route' => 'staffs.staff-calendar', 'method' => 'post', 'class' => 'form-horizontal','id' => 'popup-validation','enctype' => 'multipart/form-data']) !!}
				{{ csrf_field() }}
				<div class="form-group">
					<div class="col-lg-4">
						{{Form::label('selected_date', 'Appointment Date', ['class' => 'control-label '])}}
						{{ Form::text('selected_date',$selected_date, ['class' => 'form-control validate[required]','id'=>'datepicker','autocomplete'=>'off']) }}
					</div>
					<div class="col-lg-4">{{Form::label('selected_staff', 'Select Staff', ['class' => 'control-label'])}}
						<?php
						$staff_arr = array();
						foreach($staff_name_rows as $row){
						$staff_arr[$row['id']] = $row['staff_name'];
						}
						?>
						{{ Form::select('selected_staff', $staff_arr,$selected_staff,['class' => 'form-control validate[required]','placeholder'=>'Choose Any']) }}
					</div>
					<div class="col-lg-2">
						{{Form::label('&nbsp;', '', ['class' => 'control-label'])}}
						<input type="submit" value="Submit" class="btn btn-primary form-control">
					</div> </div>
				</form>
				
				<!-- Calendar HTML Start here -->
				<style>
				}
					.mytable  tr{/*border:none!important;*/ padding: 0px;  }
				.mytable td{ height: 60px; /*border:none!important;*/  padding: 0px;   }
				.mytable .green{ background-color: #5db17f; }
				.mytable .yellow{ background-color: #d1c362; }
				.mytable .pink{ background-color: #d5869c; }
				.mytable .nocolor{background-color: transparent;}
				.mytable .user_img{ width: 60px;
				height: 60px;
				border-radius: 100%;
				margin: 0 auto;
				display: block; }
				</style>
				<div class="table table-responsive">
					<table class="table mytable">
						<thead>	<tr>
						<th> &nbsp;</td>
						<th> <img src="https://i.pinimg.com/736x/2e/0a/f8/2e0af89dac4dbf2aae5bbca791adb4c6.jpg"  class="user_img"></th>
						<th> <img src="https://i.pinimg.com/736x/2e/0a/f8/2e0af89dac4dbf2aae5bbca791adb4c6.jpg"  class="user_img"></th>
						<th> <img src="https://i.pinimg.com/736x/2e/0a/f8/2e0af89dac4dbf2aae5bbca791adb4c6.jpg"  class="user_img"></th>
						<th> <img src="https://i.pinimg.com/736x/2e/0a/f8/2e0af89dac4dbf2aae5bbca791adb4c6.jpg"  class="user_img"></th>
						<th> <img src="https://i.pinimg.com/736x/2e/0a/f8/2e0af89dac4dbf2aae5bbca791adb4c6.jpg"  class="user_img"></th>
						<th> <img src="https://i.pinimg.com/736x/2e/0a/f8/2e0af89dac4dbf2aae5bbca791adb4c6.jpg"  class="user_img"></th>
						<th> <img src="https://i.pinimg.com/736x/2e/0a/f8/2e0af89dac4dbf2aae5bbca791adb4c6.jpg"  class="user_img"></th>
						
					</tr></thead>
					<tbody>
						
						<tr>
							<td> <b>09:00 AM</b></td>
							<td class="green"></td>
							<td class="nocolor"></td>
							<td class="green"></td>
							<td class="yellow"></td>
							<td class="pink"></td>
							<td class="green"></td>
							<td class="nocolor"></td>
						</tr>
						<tr>
							<td> <b>10:00 AM</b></td>
							<td class="nocolor"></td>
							<td class="green"></td>
							<td class="green"></td>
							<td class="nocolor"></td>
							<td class="pink"></td>
							<td class="nocolor"></td>
							<td class="green"></td>
						</tr>
						<tr>
							<td> <b>11:00 AM</b></td>
							<td class="nocolor"></td>
							<td class="nocolor"></td>
							<td class="nocolor"></td>
							<td class="green"></td>
							<td class="pink"></td>
							<td class="nocolor"></td>
							<td class="nocolor"></td>
						</tr>
						<tr>
							<td> <b>12:00 PM</b></td>
							<td class="green"></td>
							<td class="nocolor"></td>
							<td class="nocolor"></td>
							<td class="nocolor"></td>
							<td class="pink"></td>
							<td class="green"></td>
							<td class="nocolor"></td>
						</tr>
						<tr>
							<td> <b>01:00 PM</b></td>
							<td class="green"></td>
							<td class="green"></td>
							<td class="nocolor"></td>
							<td class="green"></td>
							<td class="pink"></td>
							<td class="green"></td>
							<td class="nocolor"></td>
						</tr>
						<tr>
							<td> <b>02:00 PM</b></td>
							<td class="green"></td>
							<td class="green"></td>
							<td class="pink"></td>
							<td class="green"></td>
							<td class="pink"></td>
							<td class="nocolor"></td>
							<td class="green"></td>
						</tr>
						<tr>
							<td> <b>03:00 PM</b></td>
							<td class="nocolor"></td>
							<td class="nocolor"></td>
							<td class="pink"></td>
							<td class="green"></td>
							<td class="pink"></td>
							<td class="nocolor"></td>
							<td class="green"></td>
						</tr>
						<tr>
							<td> <b>04:00 PM</b></td>
							<td class="nocolor"></td>
							<td class="nocolor"></td>
							<td class="pink	"></td>
							<td class="nocolor"></td>
							<td class="pink"></td>
							<td class="green"></td>
							<td class="green"></td>
						</tr>
						<tr>
							<td> <b>05:00 PM</b></td>
							<td class="green"></td>
							<td class="nocolor"></td>
							<td class="pink"></td>
							<td class="nocolor"></td>
							<td class="pink"></td>
							<td class="green"></td>
							<td class="nocolor"></td>
						</tr>
					</tbody>
				</table>	</div>
				<!-- Calendar HTML End here -->
			</div>
		</div>
	</div>
</div>
@endsection
@section('uniquescript')
@endsection