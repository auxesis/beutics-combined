@if($shoptime_slots)
{{ Form::select('to_time', $shoptime_slots,$selected_time,['class' => 'form-control validate[required]','placeholder'=>__('messages.Select To Time'),'id'=>'to_time']) }}
@else
{{'No times Found'}}
@endif