@if($shoptime_slots)
{{ Form::select('from_time', $shoptime_slots,$selected_time,['class' => 'form-control validate[required]','placeholder'=>__('messages.Select From Time'),'id'=>'from_time']) }}
@else
{{'No times Found'}}
@endif