@extends('layouts.spadmin.admin')

@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/jquery.gritter/css/jquery.gritter.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/themes/default/css/uniform.default.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.min.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>{{$title}}</h5>
          

            </header>
             @include('message')
            <div id="collapse2" class="body">
                <!-- <form class="form-horizontal" id="popup-validation"> -->
                {!! Form::open(['route' => 'staffs.store', 'method' => 'POST', 'class' => 'form-horizontal','id' => 'popup-validation','enctype' => 'multipart/form-data']) !!}
                {{ csrf_field() }}

                    <div class="form-group">
                        {{Form::label('staff_name', __('messages.Staff Name'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('staff_name','', ['class' => 'form-control validate[required]']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('nationality', __('messages.Nationality'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::select('nationality', $nationality_arr,null,['class' => 'form-control myselect','placeholder'=>'Select Nationality']) }}
                        </div>
                    </div>
                   
                    <div class="form-group">
                        {{Form::label('experience', __('messages.Experience'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('experience','', ['class' => 'form-control validate[required]', 'onkeypress'=>'return isNumber(event)']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('speciality', 'Speciality', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::select('speciality[]', $categories,null,['class' => 'form-control validate[required] myselectspeciality','multiple'=>"multiple", 'id' => 'myselectspeciality']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('certificates', __('messages.Certificates'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('certificates','', ['class' => 'form-control validate[required]']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('description', 'Description', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::textarea('description','', ['class' => 'form-control validate[required] summary-ckeditor','rows'=>'2']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('description_ar', 'Description(Arabic)', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::textarea('description_ar','', ['class' => 'form-control validate[required] summary-ckeditor_1','rows'=>'2']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-4">{{ __('messages.Profile Image') }}</label>
                        <div class="row">
                            <div class="col-md-4">
                                <div id="ImgView">
                                    <img src="">
                                </div>
                                <div id="upload-demo" style=" display: none;"></div>
                                <input type="file" id="image" onclick="showHideDiv(this)" name="staff_image">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-4">{{ __('messages.Staff Images') }}</label>
                        <div class="col-lg-8">
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="input_fields_container">
                                                <input type="file" id="image_files" name="staff_images[]" multiple accept="image/*"><p><strong>(Upload Multiple Staff Images)</strong></p><br/>
                                                <div id="selectedFiles"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('video_url', 'Video Url', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('video_url','', ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-4">{{ __('messages.Staff Videos') }}</label>
                        <div class="col-lg-8">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input_fields_container">
                                            <input type="file" id="video_files" name="staff_videos[]" multiple accept="video/*"><br/>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <table class='table table-borderless' style="display: none" id="selectedVideoFiles">
                                        <thead>
                                            <tr>
                                              <th scope="col">Uploaded Files</th>
                                              <th scope="col"></th>
                                            </tr>
                                          </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                     <div class="form-group">
                        {{Form::label('', '', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">

                            {{Form::checkbox('is_provide_home_service', '1')}} {{ __('messages.Is Provide Home Services?') }}
                        </div>
                    </div>


                    <?= Form::hidden('image_cr',null, ['id' => 'image_cr']) ?>
                    <div class="form-actions no-margin-bottom">
                        <input type="submit" value="{{ __('messages.Submit') }}" class="btn btn-primary">
                    </div>
                    <br><br><br><br>
                </form>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
@endsection
@section('uniquescript')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>

     <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/holder/2.4.1/holder.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/jquery.uniform.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="{{ url('/') }}/assets/lib/ckeditor/ckeditor.js"></script>
    <script type="text/javascript">
        function showHideDiv()
        {
            $('#ImgView').hide();
            $('#upload-demo').show();
        }

        $(function() {
           $(".myselect").select2();
           $(".myselectspeciality").select2({
             width: 'resolve'
           });
        });

        $('#myselectspeciality').prepend(new Option('All Rounders', '0'));
    </script>
    <script>
        CKEDITOR.replace( 'description' );
        CKEDITOR.replace( 'description_ar' );
        $(function() {
          Metis.formValidation();
        });
        
        function isNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        $('#popup-validation').submit(function(e){
            e.preventDefault();
            obj = $(this);
            resize.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (img) {
                $('#image_cr').val(img);
               obj.unbind('submit').submit();
            });
        });

        // image crop
        var resize = $('#upload-demo').croppie({
            enableExif: true,
            enableOrientation: true,    
            viewport: { // Default { width: 100, height: 100, type: 'square' } 
                width: 200,
                height: 200,
                type: 'square' //square
            },
            boundary: {
                width: 300,
                height: 300
            }
        });

        $('#image').on('change', function () { 
          var reader = new FileReader();
            reader.onload = function (e) {
              resize.croppie('bind',{
                url: e.target.result
              }).then(function(){
                console.log('jQuery bind complete');
              });
            }
            reader.readAsDataURL(this.files[0]);
        });
        //
        $('#image_files').on('change', function (e) {
            var files = e.target.files
            var filesLength = this.files.length;
            for (i = 0; i < filesLength; i++) 
            {
                var f = files[i];
                var fileReader = new FileReader();
                fileReader.onload = (function(e) {
                    var file = e.target;
                    $('#selectedFiles').append($("<div class='form-group disp-inline' style='margin: 10px 4px!important;''><img src='"+ e.target.result +"' title='"+f.name+"' width='100' height='100' class='staff_img"+i+"'/><a href='#' class='remove_img'>Remove</a></div>"));

                    $('.remove_img').click(function(e){
                       $(this).parent('.disp-inline').remove();
                    });
                });
                fileReader.readAsDataURL(f);
            }
        });
        $('#video_files').on('change', function (e) {
            $('#selectedVideoFiles').show();
            $('#selectedVideoFiles tbody').empty();
            var files = e.target.files
            var filesLength = this.files.length;
            
            for (i = 0; i < filesLength; i++) 
            {
                var f = files[i];
                var fileReader = new FileReader();
                var row = $('<tr><td>' + f.name+ '</td><td><button class="remove_file">X</button></td></tr>');
                $('#selectedVideoFiles').append(row);
                
                $('.remove_file').click(function(e){
                   $(this).parents("tr").remove();
                   var rowCount = $('#selectedVideoFiles tbody tr').length;
                   if(rowCount == 0)
                    $('#selectedVideoFiles').hide();
                });
            }
        });
    </script>


@endsection