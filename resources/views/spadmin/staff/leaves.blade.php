@extends('layouts.spadmin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
 <style type="text/css">.dataTables_filter {
display: none;
} </style>
@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5><?= $staff['staff_name']?> {{ __('messages.Leaves') }}</h5>
                <a class="btn btn-primary pull-right" href="{{route('staffs.add_leave_form', $id)}}" style="margin-top:4px;">{{ __('messages.Add New Leave') }}</a>
            </header>
            <div id="collapse4" class="body">
              @include('message')
              
               <table id="user_datatable" class="table table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>{{ __('messages.From Date') }} </th>
                        <th>{{ __('messages.To Date') }} </th>
                        <th>{{ __('messages.From Time') }} </th>
                        <th>{{ __('messages.To Time') }} </th>
                        <th>{{ __('messages.Created At') }}</th>
                        <th>{{ __('messages.Action') }}</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
                  
            </div>
        </div>
    </div>
</div>
@endsection

@section('uniquescript')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>
<script type="text/javascript">

  function deleteRow(obj)
  {
    event.preventDefault(); // prevent form submit
    swal({
      title: "{{ __('messages.Are you sure?') }}",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {        
       obj.submit();

      } else {
         swal.close();
      }
    });
  }
 
    $(function() {
        var table = $('#user_datatable').DataTable({
           stateSave: true,
        processing: true,
        serverSide: true,
        order: [[0, "desc" ]],
        "ajax":{
        "url": '{!! route('staffs.getleave') !!}',
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{csrf_token()}}",staff_id: <?= $id; ?>}
        },
        columns: [
        { data: 'from_date', name: 'from_date', orderable:true },
        { data: 'to_date', name: 'to_date', orderable:true },
        { data: 'from_time', name: 'from_time', orderable:true },
        { data: 'to_time', name: 'to_time', orderable:true },
        { data: 'created_date', name: 'created_date', orderable:true },
        { data: 'action', name: 'action', orderable:true  },
        ],
        "columnDefs": [
        { "searchable": false, "targets": 0 }
        ]
        ,language: {
        searchPlaceholder: "{{ __('messages.Search by name') }}"
        },
});
});  


</script>
@endsection

