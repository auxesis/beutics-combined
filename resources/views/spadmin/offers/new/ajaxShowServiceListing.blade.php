
@if($service_data)
	
  
    <tr class="parents_tr" data-attr="{{$service_data[0]}}">
      <td style="width: 35%"><span><?= $service_data[1]; ?></span></td>
      <td style="width: 30%">
      <div class="center form-group">
        <span class="input-group-btn">
           
        </span>
        <input type="number" name="services[{{$service_data[0]}}]" class="form-control input-number quantity_numbers" value="1" min="1" max="99" step = "1" data-attr="{{$service_data[0]}}" >
        <span class="input-group-btn">
           
        </span>
      </div>
    </td>
    <input type="hidden" value="<?= $service_data[2];?>" class="priceVal" id = "product_price_{{$service_data[0]}}" />
    <input type="hidden" name="org_price[{{$service_data[0]}}]"  value="<?= $service_data[2];?>" class="priceVal" id = "product_price_{{$service_data[0]}}" />
    <input type="hidden" name="best_price[{{$service_data[0]}}]"  value="<?= $service_data[3];?>" class="priceVal" id = "product_price_{{$service_data[0]}}" />
    <td style="width: 25%"><span>AED <?= $service_data[2];?></span></td>
    <td style="width: 10%"><span><i class="fa fa-times remove-fields" aria-hidden="true"></i></span></td>
    </tr>
 	
  
    <script type="text/javascript">
	  $('.btn-number').click(function(e){
	    e.preventDefault();
	    
	    fieldName = $(this).attr('data-field');
	    type      = $(this).attr('data-type');
	    var input = $("input[name='"+fieldName+"']");
	    var currentVal = parseInt(input.val());
	    if (!isNaN(currentVal)) {
	        if(type == 'minus') {
	            
	            if(currentVal > input.attr('min')) {
	            	
	                input.val(currentVal - 1).change();
	            } 
	            if(parseInt(input.val()) == input.attr('min')) {

	                $(this).attr('disabled', true);
	            }

	        } else if(type == 'plus') {

	            if(currentVal < input.attr('max')) {
	                input.val(currentVal + 1).change();
	            }
	            if(parseInt(input.val()) == input.attr('max')) {
	                $(this).attr('disabled', true);
	            }

	        }
	    } else {
	        input.val(0);
	    }
	});
	$('.input-number').focusin(function(){
	   $(this).data('oldValue', $(this).val());
	});
	$('.input-number').change(function() {
	    
	    minValue =  parseInt($(this).attr('min'));
	    maxValue =  parseInt($(this).attr('max'));
	    valueCurrent = parseInt($(this).val());
	    
	    name = $(this).attr('name');
	    if(valueCurrent >= minValue) {
	        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
	    } else {
	        $(this).val($(this).data('oldValue'));
	    }
	    if(valueCurrent <= maxValue) {
	        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
	    } else {
	        $(this).val($(this).data('oldValue'));
	    }
	    
	    
	});
	$(".input-number").keydown(function (e) {
	        // Allow: backspace, delete, tab, escape, enter and .
	        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
	             // Allow: Ctrl+A
	            (e.keyCode == 65 && e.ctrlKey === true) || 
	             // Allow: home, end, left, right
	            (e.keyCode >= 35 && e.keyCode <= 39)) {
	                 // let it happen, don't do anything
	                 return;
	        }
	        // Ensure that it is a number and stop the keypress
	        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
	            e.preventDefault();
	        }
	    });


	$('.remove-fields').on('click', function() {
		
		var field_id = $(this).parent().parent().parent().attr('data-attr');

		//alert(field_id);
		 var array_elements = $("#rendereddata").attr('data-id').split(",");


		var y = array_elements;
		var remove_Item = field_id;


		y = $.grep(y, function(value) {
		  return value != remove_Item;
		});

		$("#rendereddata").attr('data-id',y.join());


				//enableOption(field_id);
				$(this).parent().parent().parent().remove();
				calculateTotal();
				//alert($("#rendereddata").attr('data-id'));
	});

	$(".quantity_numbers").change(function(){
		calculateTotal();
	});

</script>
@endif