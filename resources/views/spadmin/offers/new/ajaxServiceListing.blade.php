@if($service_row)
	{{Form::label('', 'Services', ['class' => 'control-label col-lg-4','id'=>'my_services'])}}
	<option value="">{{ __('messages.Select Services') }}</option>
	@foreach($service_row as $mainrows)
	@foreach($mainrows as $key => $rows)
	@php
		$service_attr = ($rows['service_attr'] !='') ? '('.$rows['service_attr'].')' : '';
	@endphp
       <option
        data-id="<?= $rows['sp_service_id'].'_'.$key ?>" value="<?= $rows['sp_service_id'].'_'.$key.','.$rows['name'].' '.$service_attr.','.$rows['ORIGINALPRICE'].','.$rows['BESTPRICE'].','.$key; ?>" id="sp_id_{{$rows['sp_service_id']}}_{{$key}}"><?= $rows['name'].' '.$service_attr.' '.$rows['ORIGINALPRICE'].' AED'; ?>
        </option>
	@endforeach
	@endforeach
	<script>
		function addService(servicedata)
		{ console.log(servicedata.value);
			var s = $(servicedata).attr('id');		
			var whole_data = servicedata.value.split(",");

			var rendering_id = whole_data[0];
			
			if($("#rendereddata").attr('data-id') != "")
			{
				var array_elements = $("#rendereddata").attr('data-id').split(",");
				counts = {};
				jQuery.each(array_elements, function(key,value) {
				  if (!counts.hasOwnProperty(value)) {
				    counts[value] = 1;
				  } else {
				    counts[value]++;
				  }
				});
				console.log(counts);
				if(counts[rendering_id]>1)
				{
					return false;
				}
			}
			jQuery.ajax({
	            url: '{{route('ajax.new-offers.show.serviceListing')}}',
	            type: 'POST',
	            data:{servicedata:servicedata.value},
	            headers: {
	            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
	            },
	            success: function (response) {
	            	$(".service-li-list").append(response);
	            	$(".tfoot_show").show();
	             	calculateTotal();
	            } 
	        });
		}

		function calculateTotal()
		{

			var total_price = 0;
			$( ".quantity_numbers" ).each(function( index ) {


				var field_name = $(this);
				var field_id = field_name.attr('data-attr');
				var single_price = $("#product_price_"+field_id).val();

				var qty = field_name.val();

				var single_total_price = single_price*qty;
				total_price = total_price+single_total_price;

	  
			});
			$("#showTotal").html(parseFloat(total_price));
			$("#total_price").val(parseFloat(total_price));	
		}
	</script>
@endif