@extends('layouts.spadmin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
 
@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>{{ __('messages.My Offers') }}</h5>
                <a class="btn btn-primary pull-right" href="{{route('new-offers.create')}}" style="margin-top:4px;">{{ __('messages.Create Offer') }}</a>
            </header>
            <div id="collapse4" class="body">
              @include('message')
              
               <table id="user_datatable" class="table table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th width="15%">{{ __('messages.ID') }} </th>
                        <th width="15%">{{ __('messages.Offer Name') }} </th>
                        <th width="10%">{{ __('messages.Spectacular Offer') }} </th>
                        <th width="10%">{{ __('messages.Nominated Offer') }} </th>
                        <th width="10%">{{ __('messages.Gender') }}</th>
                        <th width="10%">{{ __('messages.Service Type') }}</th>
                        <th width="10%">{{ __('messages.Expiry Date') }}</th>
                        <th width="10%">{{ __('messages.Status') }}</th>
                        <th width="10%">{{ __('messages.Likes') }}</th>
                        <th width="15%">{{ __('messages.Action') }}</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
                  
            </div>
        </div>
    </div>
</div>
@endsection

@section('uniquescript')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>
<script type="text/javascript"> 
    
     function deleteRow(obj)
      {
        event.preventDefault(); // prevent form submit
        swal({
          title: "{{ __('messages.Are you sure?') }}",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          cancelButtonText: "No",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm){
          if (isConfirm) {        
           obj.submit();

          } else {
             swal.close();
          }
        });
      }

    $(function() {
        var table = $('#user_datatable').DataTable({
           stateSave: true,
        processing: true,
        serverSide: true,
        order: [[0, "desc" ]],
        "ajax":{
        "url": '{!! route('new-offers.getdata') !!}',
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{csrf_token()}}",status:'pending'}
        },
        columns: [
        { data: 'id', name: 'id', orderable:true },
        { data: 'offer_id', name: 'offer_id', orderable:true },
        { data: 'spectacular_offer', name: 'spectacular_offer', orderable:true },
        { data: 'is_nominated', name: 'is_nominated', orderable:true },
        { data: 'gender', name: 'gender', orderable:true },
        { data: 'service_type', name: 'service_type', orderable:true },
        { data: 'expiry_date', name: 'expiry_date', orderable:true  },
        { data: 'status', name: 'status', orderable:true  },
        { data: 'likes', name: 'likes', orderable:true  },
        { data: 'action', name: 'action', orderable:true  },
        ],
        "columnDefs": [
        { "searchable": false, "targets": 0 }
        ]
        , language: {
        searchPlaceholder: "{{ __('messages.Search by Name & ID') }}"
        },
});
});  


</script>
@endsection

