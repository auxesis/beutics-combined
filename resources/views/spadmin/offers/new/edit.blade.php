@extends('layouts.spadmin.admin')

@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/jquery.gritter/css/jquery.gritter.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/themes/default/css/uniform.default.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/datepicker/jquery-ui.css">

  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

<style type="text/css">
  
.form-horizontal .radio, .form-horizontal .checkbox {
    min-height: auto;
    display: inline-block;
    margin-right: 5px;
    margin-left: 10px;
}


table.owntable.table.table-borered {
    width: 456px;
    margin: 0 auto;
    position: relative;
    left: 6%;
}


table.owntable th {
    background-color: #337ab7;
    color: #fff;
}

.hweexterfile input {
    max-width: 100%;
    width: 89%;
    margin: 0 auto;
}

.hweexterfile {
    padding: 0;
}

.hweexterfile span {
    position: relative;
    top: -8px;
}

.hwetotal td:last-child {
    text-align: right;
}

.hwetotal td {
    padding: 10px 0;
}
.hwetotal {
    font-weight: bold;
    border-top: 1px solid #ddd;
    margin-top: 19px;
}
#serviceList~.select2 {width: 100% !important}
</style>

@endsection

@section('content')
<div class="row">
  <div class="col-lg-12">
    <div class="box">
      <header class="dark">
        <div class="icons"><i class="fa fa-check"></i></div>
        <h5>{{$title}}</h5>
      </header>
       @include('message')
      <div id="collapse2" class="body">
          <!-- <form class="form-horizontal" id="popup-validation"> -->
        {!! Form::model($rows,['route' => ['new-offers.update',$rows->id], 'method' => 'PATCH', 'class' => 'form-horizontal','id' => 'popup-validation','enctype' => 'multipart/form-data']) !!}
        {{ csrf_field() }}

          <div class="form-group">
              {{Form::label('offer_id', __('messages.Offer Name'), ['class' => 'control-label col-lg-4'])}}
              <div class="col-lg-4">
                <?php 
                $offer_arr = array();
                  foreach($offer_name_rows as $row){
                      $offer_arr[$row['id']] = $row[$titlee];
                  }
                ?>
                  {{ Form::select('offer_id', $offer_arr,null,['class' => 'form-control validate[required]','placeholder'=>__('messages.Choose Any')]) }}
              </div>
          </div>

          <div class="form-group">
            {{Form::label('', '', ['class' => 'control-label col-lg-4'])}}
              <div class="col-lg-4">
                 {{Form::checkbox('spectacular_offer', '1',null,['class' => 'checkbox'])}} {{ __('messages.Spectacular Offer') }}

              </div>
          </div>                           
  
          <div class="form-group">
              {{Form::label('offer_details', __('messages.Offer Details'), ['class' => 'control-label col-lg-4'])}}
              <div class="col-lg-4">
                  {{ Form::textarea('offer_details',null, ['class' => 'form-control validate[required] summary-ckeditor']) }}
              </div>
          </div>

          <div class="form-group">
            {{Form::label('', __('messages.Gender'), ['class' => 'control-label col-lg-4'])}}
              <div class="col-lg-4">
                <?php $service_criteria = explode(',',$sp_row->service_criteria);
                ?>
                @foreach($service_criteria as $sc)
                  {{Form::radio('gender',$sc,'',['class' => 'checkbox service_criteria','onchange'=>'expendDiv(this)'])}} {{ucwords($sc)}}
                @endforeach   
              </div>
          </div>
          <?php 
            $provider_criteria = array('home','store','online');
          ?>
          <div class="form-group" id="serviceTypeDiv">
            {{Form::label('', __('messages.Service Type'), ['class' => 'control-label col-lg-4'])}}
            <div class="col-lg-4">
              @foreach($provider_criteria as $key => $name)
                @if(in_array($name,$delivery_mode))
                  {{Form::radio('service_type',$name,'',['class' => 'checkbox service_type'])}}{{ __('messages.'.ucfirst($name))}}
                @endif
              @endforeach
            </div>
          </div>

          <div class="form-group" id="serviceDiv" >
            {{Form::label('', __('messages.Services'), ['class' => 'control-label col-lg-4'])}}
            <div class="col-lg-4">
                <select data-live-search="true" class="form-control myselect" id="serviceList" onchange="addService(this)" >
                </select>
            </div>
          </div>

          <div class="form-group" id='listing'>
            <div class="col-md-offset-4 col-md-4">
              <ul class="services-list hweexterfile">
                <li >
                <?php $total = 0; ?>  
                <table width="100%" class="service-li-list">
                  @foreach($service_row as $mainrows)
                    @foreach($mainrows as $key => $prows)
                    <tr class="parents_tr" data-attr="{{$prows['sp_service_id']}}_{{$key}}">
                      <td style="width: 35%"><span>{{$prows['name']}} ({{$prows['service_attr']}})</span></td>
                      <td style="width: 30%">
                        <div class="center form-group">
                          <span class="input-group-btn">
                             
                          </span>
                          <input type="number" name="services[{{$prows['sp_service_id']}}_{{$key}}]" class="form-control input-number quantity_numbers" value="{{$prows['quantity']}}" min="1" max="99" step = "1" data-attr="{{$prows['sp_service_id']}}_{{$key}}">
                          <span class="input-group-btn">
                             
                          </span>
                        </div>
                      </td>
                      <?php $total = $total+($prows['ORIGINALPRICE'] * $prows['quantity']);?>
                      <input type="hidden" value="<?= $prows['ORIGINALPRICE'];?>" class="priceVal" id = "product_price_{{$prows['sp_service_id']}}_{{$key}}" />

                      <td style="width: 25%"><span>AED <?= $prows['ORIGINALPRICE'];?></span></td>

                      <td style="width: 10%"><span><i class="fa fa-times remove-fields" aria-hidden="true"></i></span></td>
                    </tr>
                    @endforeach
                  @endforeach
                  <tfoot class="tfoot_show hwetotal">
                    <td colspan="2">{{ __('messages.Total') }}</td>
                    <td id="showTotal">{{$total}}</td>
                    <input type="hidden" name="total_price" id="total_price" value="{{$total}}" />
                  </tfoot>
                  <!-- show only once on page load -->
                  <!-- <tfoot class="tfoot_show hwetotal onlyonce">
                    <td colspan="2">Total</td>
                    <td id="showTotal">{{$total}}</td>
                    <input type="hidden" name="total_price" id="total_price1" />
                  </tfoot> -->
                  <!--  -->
                </table>

                </li>
              </ul>
            </div>
          </div>

          <div class="form-group">
              {{Form::label('discount', __('messages.Discount'), ['class' => 'control-label col-lg-4'])}}
              <div class="col-lg-4">
                  {{ Form::text('discount',null, ['class' => 'form-control validate[required]','id'=>'discount','onkeypress'=>'return isNumber(event)','onkeyup'=>'calcBestPriceValues()']) }}
                  <span id="discount_span" style="color:red;"></span>
              </div>
          </div>

          <div class="form-group">
              {{Form::label('best_price', __('messages.Best Price'), ['class' => 'control-label col-lg-4'])}}
              <div class="col-lg-4">
                <?php $bp = $total-($total*$rows->discount)/100;?>
                  {{ Form::text('best_price',$bp, ['class' => 'form-control validate[required]','id'=>'best_price','onkeypress'=>'return isNumber(event)','onkeyup'=>'calcDisValues()']) }}
                  <span id="bestprice_span" style="color:red;"></span>
              </div>
          </div>

          <div class="form-group">
              {{Form::label('expiry_date', __('messages.Expiry Date'), ['class' => 'control-label col-lg-4'])}}
              <div class="col-lg-4">
                  {{ Form::text('expiry_date',null, ['class' => 'form-control validate[required]','id'=>'datepicker','autocomplete'=>'off']) }}
              </div>
          </div>

          <div class="form-group">
              {{Form::label('offer_terms', __('messages.Offer Terms'), ['class' => 'control-label col-lg-4'])}}
              <div class="col-lg-4">
                  {{ Form::textarea('offer_terms',null, ['class' => 'form-control validate[required] summary-ckeditor']) }}
              </div>
          </div>

          <div class="form-actions no-margin-bottom">
                                  
              <input type="submit" value="{{ __('messages.Submit') }}" class="btn btn-primary">
          </div>
          <br><br><br><br>
        </form>
      </div>
    </div>  
  </div>
    <!-- /.col-lg-12 -->
</div>
<span id="rendereddata" data-id = ''></span>
@endsection
@section('uniquescript')

    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>

     <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/holder/2.4.1/holder.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/jquery.uniform.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="{{ url('/') }}/assets/lib/datepicker/jquery-ui.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
    <script src="{{ url('/') }}/assets/lib/ckeditor/ckeditor.js"></script>
    <script>
      CKEDITOR.replace( 'offer_terms' );
      CKEDITOR.replace( 'offer_details' );
    </script>
    <script>
      

        $(function() {
          Metis.formValidation();
        });

         $( function() {
          $("#datepicker").datepicker({ 
              dateFormat: 'yy-mm-dd',
            changeYear: true,
            changeMonth: true,
            minDate: 1,
            changeMonth: true,
            });
        } );


    </script>

    <script type="text/javascript">
      $(".myselect").select2();

      $(".myselect").change(function(){
        

        var whole_data = $(this).val().split(",");

        if($("#rendereddata").attr('data-id') == "")
        {
          $("#rendereddata").attr('data-id',whole_data[0]);
        }
        else
        {
          var new_data_ids = $("#rendereddata").attr('data-id')+','+whole_data[0];
          $("#rendereddata").attr('data-id',new_data_ids);
        }

      });
    </script>

    <script>
       function isNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        $(document).ready(function(){

          $( ".parents_tr" ).each(function( index ) {

           var ids = $(this).attr('data-attr');

            if($("#rendereddata").attr('data-id') == "")
            {

              $("#rendereddata").attr('data-id',ids);
            }
            else
            {
              var new_data_ids = $("#rendereddata").attr('data-id')+','+ids;
              $("#rendereddata").attr('data-id',new_data_ids);
            }

           
          });

          filterServices();

          $(".service_type").change(function(){
            $("#serviceDiv").show();
            filterServices();
          });

           $(".service_type").change(function(){service_criteria
              if ($(".parents_tr")[0]){
                  $(".parents_tr").remove();
                  $(".tfoot_show").remove();
              }
             // $(".service-li-list").remove();
            });

           
           
         $('.remove-fields').on('click', function() {
    
              var field_id = $(this).parent().parent().parent().attr('data-attr');

              var array_elements = $("#rendereddata").attr('data-id').split(",");

              var y = array_elements;
              var remove_Item = field_id;


              y = $.grep(y, function(value) {
                return value != remove_Item;
              });

              $("#rendereddata").attr('data-id',y.join());

              $(this).parent().parent().parent().remove();
              calculateTotal();
          });

          $(".quantity_numbers").change(function(){

            calculateTotal();
          });
        });
        

        function calcBestPriceValues()
        {
          var dis = $("#discount").val();
          var total = $("#showTotal").html();
          if(dis<100){
            if(total>0){
              var bestprice = total - (total*dis)/100;
              $("#best_price").val((bestprice).toFixed(3));
            }else{
              $("#best_price").val(0);
            }
          }else{
            $("#discount_span").html("<?php echo __('messages.Discount cannot be more than 100.') ?>");
            $("#discount").val('');
          }
        }

        function calcDisValues()
        {

          var best_price = $("#best_price").val();
          var total = $("#showTotal").html();
          best_price = parseInt(best_price);
          total = parseInt(total);

          if(best_price>0 && best_price < total){ 
            if(total>0)         {
              var bestprice_1 = total - best_price;
              bestprice_2 =(bestprice_1*100)/total;
              $("#discount").val((bestprice_2).toFixed(3));
              $("#bestprice_span").html("");
            }else{
              $("#discount").val('');
              $("#bestprice_span").html("");
            }
            
          }else{
            $("#bestprice_span").html("<?php echo __('messages.Best price cannot be greater than original price.') ?>");
            $("#best_price").val('');
          }
        }

      function expendDiv(sc)
      {
        $('#serviceTypeDiv').show();

        if ($(".parents_tr")[0]){
            $(".parents_tr").remove();
            $(".tfoot_show").remove();
           
        }

         $("#serviceList").html('');
         $(".service_type").prop('checked',false);
         $("#rendereddata").attr('data-id','');
      }
        
      

      function filterServices()
      {
        var service_criteria = $("input[name='gender']:checked").val();
        var service_type = $("input[name='service_type']:checked").val();
        // var service_name = serviceVal.value;
   
          jQuery.ajax({
            url: '{{route('ajax.new-offers.get.services')}}',
            type: 'POST',
            data:{gender:service_criteria,service_type:service_type},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {

             $("#serviceList").html(response);
             // $('#serviceList').select2();
            }
              
          });
      }

      $(function() {
        $('.selectpicker').selectpicker();
      });
    </script>


@endsection