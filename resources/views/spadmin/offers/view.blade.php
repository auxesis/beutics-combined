@extends('layouts.spadmin.admin')
@section('uniquecss')

<style type="text/css">
    table.owntable.table.table-borered {
        width: 456px;
        margin: 0 auto;
        position: relative;
        left: -25%;
    }

    table.owntable th {
        background-color: #337ab7;
        color: #fff;
    }
</style>

@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>{{ __('messages.View Offer') }}</h5>

            </header>
            <div id="collapse2" class="body">
              <div class="row">
                 @include('message')
                <div class="col-sm-12 col-lg-12">
                  
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>{{ __('messages.ID') }}</b></div>
                        <div class="col-lg-8"><?= $rows->id; ?></div> <div class="clearfix"></div>
                    </div>
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>{{ __('messages.Service Provider Name') }}</b></div>
                        <div class="col-lg-8"><?= $rows->getAssociatedOfferProviderName->name; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>{{ __('messages.Offer Name') }}</b></div>
                        <div class="col-lg-8"><?= $rows->getAssociatedOfferName->$lang_title; ?></div> <div class="clearfix"></div>
                    </div>
                    <div  class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>{{ __('messages.Spectacular Offer') }}</b></div>
                        <div class="col-lg-8"><?= $rows->spectacular_offer == '1' ? __('messages.Yes'):__('messages.No'); ?></div> <div class="clearfix"></div>
                    </div>

                     <div  class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>{{ __('messages.Gender') }}</b></div>
                        <div class="col-lg-8"><?= $rows->gender; ?></div> <div class="clearfix"></div>
                    </div>

                     <div  class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>{{ __('messages.Service Type') }}</b></div>
                        <div class="col-lg-8"><?= $rows->service_type; ?></div> <div class="clearfix"></div>
                    </div>

                    <div  class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>{{ __('messages.Total Price') }}</b></div>
                        <div class="col-lg-8"><?= round($rows->total_price, 2).' AED'; ?></div> <div class="clearfix"></div>
                    </div>
                    <div  class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>{{ __('messages.Discount') }}</b></div>
                        <div class="col-lg-8"><?= $rows->discount; ?></div> <div class="clearfix"></div>
                    </div>
                     <div  class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>{{ __('messages.Best Price') }}</b></div>
                        <div class="col-lg-8"><?= $rows->best_price.' AED'; ?></div> <div class="clearfix"></div>
                    </div>
                    <div  class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>{{ __('messages.Expiry Date') }}</b></div>
                        <div class="col-lg-8"><?= date('d-M-Y',strtotime($rows->expiry_date)); ?></div> <div class="clearfix"></div>
                    </div>
                    <div  class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>{{ __('messages.Offer Terms') }}</b></div>
                        <div class="col-lg-8"><?= $rows->offer_terms; ?></div> <div class="clearfix"></div>
                    </div>

                     <div  class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>{{ __('messages.Offer Description') }}</b></div>
                        <div class="col-lg-8"><?= $rows->offer_details; ?></div> <div class="clearfix"></div>
                    </div>


                    <div  class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>{{ __('messages.Nominated Offer') }}</b></div>
                        <div class="col-lg-8"><?= $rows->is_nominated == '1' ? __('messages.Yes'):__('messages.No'); ?></div> <div class="clearfix"></div>
                         <br/><br/>
                    </div>

                    <div class="new_section">
                        <table class="owntable table table-borered">
                          <thead>
                            <tr>

                              <th scope="col">{{ __('messages.Service Name') }}</th>
                              <th scope="col">{{ __('messages.QTY') }}</th>
                              <th scope="col">{{ __('messages.Price') }}</th>
                            </tr>
                          </thead>
                          <tbody>
                           <?php $total = 0; ?>
                            @foreach($offers_service as $srow)
                            <tr>
                              <td>{{$srow['name']}}</td>
                              <td>{{$srow['quantity']}}</td>
                              <td><?php
                              if(array_key_exists("ORIGINALPRICE",$srow)){
                                echo $srow['ORIGINALPRICE']; 
                              }else{
                                echo $srow['HOME_ORIGINALPRICE']; 
                              }
                                
                                ?>
                                  
                              </td>
                            
                            </tr>
                             @endforeach
                             
                           <tr>
                              <td>{{ __('messages.Total') }} </td>
                              <td> </td>
                              <td colspan="2" id="totalPrice"><?php echo $total_bestprice;?></td>
                            </tr>
                           
                           
                          </tbody>
                        </table>   
                      </div>

                </div>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>

@endsection