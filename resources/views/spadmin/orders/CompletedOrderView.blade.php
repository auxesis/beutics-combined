@extends('layouts.spadmin.admin')

@section('uniquecss')
<style type="text/css">
    table.owntable th {
        background-color: #337ab7;
        color: #fff;
    }
</style>
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/jquery.gritter/css/jquery.gritter.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/themes/default/css/uniform.default.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css">
   <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/datepicker/jquery-ui.css">


@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>{{$title}}</h5>
            </header>
             @include('message')
            <div id="collapse2" class="body">
                <!-- <form class="form-horizontal" id="popup-validation"> -->
                {!! Form::open(['route' => 'orders.completed', 'method' => 'post', 'class' => 'form-horizontal','id' => 'popup-validation','enctype' => 'multipart/form-data']) !!}
                {{ csrf_field() }}
                    <input type="hidden" name="order_id" value="<?= $result['id']; ?>" />
                    <input type="hidden" name="staff_id" value="<?= $result['staff_id']; ?>" />
                    <input type="hidden" name="appointment_date" value="<?= $result['appointment_date']; ?>" />
                    <input type="hidden" name="appointment_time" value="<?= $result['appointment_time']; ?>" />
                  
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>{{ __('messages.Customer Name') }}</b></div>
                        <div class="col-lg-8">
                            <?= $result['user_name']; ?>
                        </div> <div class="clearfix"></div>
                    </div>

                   

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>{{ __('messages.Staff Name') }}</b></div>
                        <div class="col-lg-8"><?= $result['staff_name']; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>{{ __('messages.Appointment Date') }}</b></div>
                        <div class="col-lg-8"><?= $result['appointment_date']; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>{{ __('messages.Appointment Time') }}</b></div>
                        <div class="col-lg-8"><?= ($result['appointment_time'])?date('h:i A', strtotime($result['appointment_time'])):''; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>{{ __('messages.Service End Time') }}</b></div>
                        <div class="col-lg-8"><?= ($result['service_end_time'])?date('h:i A', strtotime($result['service_end_time'])):''; ?></div>
                        
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>{{ __('messages.Total Amount') }}</b></div>
                        <div class="col-lg-8"><?= $result['total_item_amount'].' AED'; ?></div> <div class="clearfix"></div>
                    </div>
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>{{ __('messages.Order Date and Time') }}</b></div>
                        <div class="col-lg-8"><?= $result['created_date']; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>{{ __('messages.Payment Status') }}</b></div>
                        <div class="col-lg-8"><?= $result['payment_type'] == "0" ? "Paid Online":"Pay At Store"; ?></div> <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>                   <br>
                    <div class="form-group">
                        {{Form::label('unique_order_id', __('messages.Enter Order ID'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                             <?php if($result['db_user_name']){ ?>
                                {{ Form::text('unique_order_id',$result['booking_unique_id'], ['class' => 'form-control validate[required]','autocomplete'=>'off']) }}
                              <?php } else { ?>
                                {{ Form::text('unique_order_id','', ['class' => 'form-control validate[required]','autocomplete'=>'off']) }}
                              <?php } ?>
                            
                        </div>
                    </div>
                    <?php if($user_data['category_id'] == 4){?>
                      <div class="form-group">
                        {{Form::label('invoice', __('messages.Upload Invoice'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::file('invoice', ['class' => 'form-control','autocomplete'=>'off', 'style' => 'height: 100%;', 'accept' => 'application/pdf']) }}
                        </div>
                    </div>
                    <?php } ?>
                    <div class="clearfix"></div>                   
                    <?php  $current_date = date('Y-m-d'); $current_time = date('H:i:s');
                     $app_date = date('Y-m-d',strtotime($result['appointment_date']));
                     if((strtotime($current_date) < strtotime($app_date)) || ((strtotime($current_date) == strtotime($app_date)) && (strtotime($current_time) < strtotime($result['appointment_time'])))) { ?>
                       <div class="form-actions no-margin-bottom">                                            
                        <input type="button" style="opacity:0.5" value="{{ __('messages.Submit') }}" class="btn btn-primary">
                    </div>
                     <?php } else { ?>
                      <div class="form-actions no-margin-bottom">                                            
                        <input type="submit" value="{{ __('messages.Submit') }}" class="btn btn-primary">
                    </div>
                    <?php } ?>
                    
                    
                    <div class="clearfix"></div><br><br>
                  <header class="dark">
                  <div class="icons"><i class="fa fa-list"></i></div>
                    <h5>{{ __('messages.Order Items') }}</h5>

                 </header><br>
                   <div class="new_section">
                        <table class="owntable table table-borered">
                          <thead>
                            <tr>
                              <th width="30%" scope="col">{{ __('messages.Item') }}</th>
                              <th scope="col">{{ __('messages.Type') }}</th>
                              <th scope="col">{{ __('messages.QTY') }}</th>
                              <th scope="col">{{ __('messages.Gender') }}</th>
                              <th scope="col">{{ __('messages.Price') }}</th>
                            </tr>
                          </thead>
                          <tbody>
                           <?php $total = 0;$terms=''; ?>
                            @foreach($result['booking_items'] as $srow)
                            

                            <tr>
                              <td>{{$srow['name']}}
                              <?php if(!empty($srow['services'])){ ?>: <div style="font-size:12px;"><i>
                                  @foreach($srow['services'] as $servrow)
                                   {{$servrow['name']}}(<b> {{$servrow['quantity']}} </b>), 
                                 @endforeach
                               </i>
                               <br/>
                               <?php 
                               $terms=preg_replace('/[^\da-z ]/i', '', $srow['offer_terms']);
                               $details=preg_replace('/[^\da-z ]/i', '', $srow['offer_details']);
                                ?>
                               <span>
                                  <a data-toggle="modal" data-target="#modalTermCondition" onclick="showModal('<?= $srow['name'];?>' , '<?= $terms;?>', '<?= $details;?>')"><b style="color:red;">View {{$srow['name']}} Details</b></a>
                               </span></div>
                                 <?php } ?>
                              </td>
                              <td><b><?= isset($srow['service_id']) ?  __('messages.Service'):__('messages.Offer'); ?></b></td>
                              <td>{{$srow['quantity']}}</td>
                              <td>{{$srow['gender']}}</td>
                              <?php if($result['db_user_name']){ ?>
                                <td><?php echo $srow['original_price'].' AED'; ?></td>  
                              <?php } else { ?>
                                <td><?php echo $srow['best_price'].' AED'; ?></td>  
                              <?php } ?>                   
                            </tr>
                             @endforeach
                          </tbody>
                        </table>   
                      </div>
                </form>
            </div>
        </div>  
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- Modal -->
<div id="modalTermCondition" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="offer_name">
          
          
        </h4>
      </div>
     <div class="modal-body" >
        <h5>Offer Terms & Condition</h5>
        <p id="offer_terms"></p>
        <br/>
        <h5>Offer Details</h5>
        <p id="offer_details"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
@endsection
@section('uniquescript')

    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>

     <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/holder/2.4.1/holder.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/jquery.uniform.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="{{ url('/') }}/assets/lib/datepicker/jquery-ui.js"></script>
        
    <script>    
        $(function() {
          Metis.formValidation();
        });
        function showModal(offer_name,offer_terms,offer_details){
        $("#offer_name").html(offer_name+" Details");
          $("#offer_terms").html(offer_terms);
          $("#offer_details").html(offer_details);
      }
    </script>

@endsection