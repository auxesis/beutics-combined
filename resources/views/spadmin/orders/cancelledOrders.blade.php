@extends('layouts.spadmin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
 
@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>{{ __('messages.My Cancelled Orders') }}</h5>
               
            </header>
            <div id="collapse4" class="body">
              @include('message')
              
               <table id="user_datatable" class="table table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th width="10%">{{ __('messages.Name') }} </th>
<!--                         <th width="10%">Service Provider </th>
 -->                    <th width="10%">{{ __('messages.Appointment Date') }}</th>
                        <!-- <th width="10%">Appointment Time</th> -->
                        <th width="10%">{{ __('messages.Service End Time') }}</th>
                        <th width="10%">{{ __('messages.Service Type') }}</th>
                        <th width="10%">{{ __('messages.Order Date') }}</th>
                        <th width="10%">{{ __('messages.Action') }}</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
                  
            </div>
        </div>
    </div>
</div>
@endsection

@section('uniquescript')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>
<script type="text/javascript"> 
    
     function deleteRow(obj)
      {
        event.preventDefault(); // prevent form submit
        swal({
          title: "{{ __('messages.Are you sure?') }}",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          cancelButtonText: "No",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm){
          if (isConfirm) {        
           obj.submit();

          } else {
             swal.close();
          }
        });
      }

    $(function() {
        var table = $('#user_datatable').DataTable({
           stateSave: true,
        processing: true,
        serverSide: true,
        order: [[0, "desc" ]],
        "ajax":{
        "url": '{!! route('orders.getdataCancelled') !!}',
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{csrf_token()}}",status:'pending'}
        },
        columns: [
        { data: 'name', name: 'name', orderable:true },
        // { data: 'sp_id', name: 'sp_id', orderable:true },
        { data: 'appointment_date', name: 'appointment_date', orderable:true },
        // { data: 'appointment_time', name: 'appointment_time', orderable:true },
        { data: 'service_end_time', name: 'service_end_time', orderable:true },
        { data: 'booking_service_type', name: 'booking_service_type', orderable:true  },
        { data: 'created_date', name: 'created_date', orderable:true  },
        { data: 'action', name: 'action', orderable:true  },
        ],
        "columnDefs": [
        { "searchable": false, "targets": 0 }
        ]
        , language: {
        searchPlaceholder: "{{ __('messages.Search by name & mobile') }}"
        },
});
});  


</script>
@endsection

