@extends('layouts.spadmin.admin')

@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/jquery.gritter/css/jquery.gritter.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/themes/default/css/uniform.default.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css">
   <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/datepicker/jquery-ui.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

<style type="text/css">
  
.form-horizontal .radio, .form-horizontal .checkbox {
    min-height: auto;
    display: inline-block;
    margin-right: 5px;
    margin-left: 10px;
}


table.owntable.table.table-borered {
    width: 456px;
    margin: 0 auto;
    position: relative;
    left: 6%;
}


table.owntable th {
    background-color: #337ab7;
    color: #fff;
}

.hweexterfile input {
    max-width: 100%;
    width: 89%;
    margin: 0 auto;
}

.hweexterfile {
    padding: 0;
}

.hweexterfile span {
    position: relative;
    top: -8px;
}

.hwetotal td:last-child {
    text-align: right;
}

.hwetotal td {
    padding: 10px 0;
}
.hwetotal {
    font-weight: bold;
    border-top: 1px solid #ddd;
    margin-top: 19px;
}

#serviceList~.select2 {width: 100% !important}


.btn-group.bootstrap-select.form-control {
    width: 149px;
    display: inline-block;
}

input#customer_mobile_no {
    display: inline-block;
    width: 170px;
}

</style>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>{{$title}}</h5>
            </header>
             @include('message')
            <div id="collapse2" class="body">
                <!-- <form class="form-horizontal" id="popup-validation"> -->
                {!! Form::open(['route' => 'orders.new_order', 'method' => 'post', 'class' => 'form-horizontal','id' => 'popup-validation','enctype' => 'multipart/form-data']) !!}
                {{ csrf_field() }}
                   
                    <div class="form-group">
                         {{Form::label('customer_name', __('messages.Customer Name'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('customer_name','', ['class' => 'form-control validate[required]','autocomplete'=>'off']) }}
                        </div>
                    </div>

                    <div class="form-group">
                         {{Form::label('customer_mobile_no', __('messages.Customer Mobile No'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">

                            {{ Form::select('country_code', $country_codes,'',['class' => 'form-control selectpicker validate[required]','data-live-search'=>"true",'style' => 'width: 65px;float: left;']) }}

                            {{ Form::text('customer_mobile_no','', ['class' => 'form-control validate[required]','autocomplete'=>'off', 'onkeypress'=>'return isNumber(event)']) }}
                        </div>
                    </div>

                   <div class="form-group">
                        {{Form::label('staff_id', __('messages.Select Staff'), ['class' => 'control-label col-lg-4'])}}
                        
                         <div class="col-lg-4">
                            <?php 
                            $staff_arr = array();
                              foreach($staff_name_rows as $row){
                                  $staff_arr[$row['id']] = $row['staff_name'];
                              }
                            ?>
                             {{ Form::select('staff_id', $staff_arr,$staff_id,['class' => 'form-control validate[required]','placeholder'=>__('messages.Choose Any')]) }}
                        </div> <div class="clearfix"></div>
                    </div>
                   <div class="form-group">
                        {{Form::label('appointment_date', __('messages.Appointment Date'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('appointment_date',$date, ['class' => 'form-control app_date1 validate[required]','id'=>'datepicker','autocomplete'=>'off']) }}
                        </div>
                    </div>
                     <div class="form-group">
                        {{Form::label('appointment_time', __('messages.Appointment Time'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4" id="appointment_time_div">
                            {{ Form::select('appointment_time', [],null,['class' => 'form-control validate[required]','placeholder'=>__('messages.Select Appointment Time'),'id'=>'appointment_time']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('hours', __('messages.Select Hours'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            <?php for($i = 0; $i <= 12; $i++): 
                            if($i==0){
                              $hours[$i] = $i.'hour';
                            } else if($i==1){
                              $hours[$i] = '0'.$i.'hour';
                            } else if($i<10){
                              $hours[$i] = '0'.$i.'hours';
                            } else {
                              $hours[$i] = $i.'hours';
                            }  
                            
                           endfor ?>
                            {{ Form::select('hours', $hours,null,['class' => 'form-control validate[required]','placeholder'=>__('messages.Select Hours'),'id'=>'hours']) }}
                        </div>
                        <div class="col-lg-4">
                           <?php $minutes = array('0' => '0 '.'minute', '15' => '15 '.'minutes','30' => '30 '.'minutes', '45' => '45 '.'minutes'); ?>
                            {{ Form::select('minutes', $minutes,null,['class' => 'form-control validate[required]','placeholder'=>__('messages.Select Minutes'),'id'=>'hours']) }}
                        </div>
                    </div>                    
                  
                    <div class="form-group">
                      {{Form::label('', __('messages.Gender'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                          <?php $service_criteria = explode(',',$sp_row->service_criteria);
                          ?>
                          @foreach($service_criteria as $sc)
                           
                            {{Form::radio('gender',$sc,'',['class' => 'checkbox service_criteria','onchange'=>'expendDiv(this)'])}} {{ucwords($sc)}}

                          @endforeach   
                        </div>
                    </div>


                    <div class="form-group" id="serviceTypeDiv" style="display:none;">
                      {{Form::label('', __('messages.Service Type'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">

                          {{Form::radio('service_type', 
                          'home','',['class' => 'checkbox service_type'])}} {{ __('messages.Home') }} 

                          {{Form::radio('service_type', 
                          'shop','',['class' => 'checkbox service_type'])}} {{ __('messages.Shop') }}

                        </div>
                    </div>

                    <div class="form-group" id="serviceDiv" style="display:none;">
                        {{Form::label('', __('messages.Services'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            <select data-live-search="true" class="form-control myselect" id="serviceList" onchange="addService(this)" >
                            </select>
                        </div>
                    </div>

                    <div class="form-group" id='listing'>
                      <div class="col-md-offset-4 col-md-4">
                        <ul class="services-list hweexterfile">
                          <li >

                          <table width="100%" class="service-li-list">
                            <tfoot class="tfoot_show hwetotal" style="display:none;">
                              <td colspan="2">{{ __('messages.Total') }} </td>
                              <td id="showTotal">0</td>
                              <input type="hidden" name="total_price" id="total_price" />
                            </tfoot>

                          </table>

                          </li>
                        </ul>
                      </div>
                    </div>

                    <!-- <div class="form-group" id='service_list_div'></div> -->

                    <div class="form-actions no-margin-bottom">
                                            
                        <input type="submit" value="{{ __('messages.Submit') }}" class="btn btn-primary">
                    </div>
                    
                </form>
            </div>
        </div>  
    </div>
    <!-- /.col-lg-12 -->
</div>
<span id="rendereddata" data-id = ''></span>
@endsection
@section('uniquescript')

    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>

     <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/holder/2.4.1/holder.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/jquery.uniform.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="{{ url('/') }}/assets/lib/datepicker/jquery-ui.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
     
    <script type="text/javascript">
      var app = $(".app_date1").val();
      if(app!=''){

          jQuery.ajax({
            url: '{{route('ajax.get.time')}}',
            type: 'POST',
            data:{selected_date:app},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
              $("#appointment_time_div").html(response);
              
              }
            });
          }

      $(".myselect").select2();

      $(".myselect").change(function(){
        

        var whole_data = $(this).val().split(",");

        if($("#rendereddata").attr('data-id') == "")
        {
          $("#rendereddata").attr('data-id',whole_data[0]);
        }
        else
        {
          var new_data_ids = $("#rendereddata").attr('data-id')+','+whole_data[0];
          $("#rendereddata").attr('data-id',new_data_ids);
        }

      });
    </script>

    <script>
      

        $(function() {
          Metis.formValidation();
        });

        <?php if($date){ ?>
          jQuery.ajax({
            url: '{{route('ajax.get.time')}}',
            type: 'POST',
            data:{selected_date:'<?= $date ?>', selected_time:'<?= $time ?>'},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
              $("#appointment_time_div").html(response);
              
              }
            });
        <?php } ?>

         $( function() {
          $("#datepicker").datepicker({ 
            dateFormat: 'yy-mm-dd',
            changeYear: true,
            changeMonth: true,
            minDate: 0,
            });
        } );


    </script>
    <script>
       function isNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        function calcBestPriceValues()
        {
          var dis = $("#discount").val();
          var total = $("#showTotal").html();
          if(dis<100){
            if(total>0){
              var bestprice = total - (total*dis)/100;
              $("#best_price").val((bestprice).toFixed(3));
            }else{
              $("#best_price").val(0);
            }
          }else{
            $("#discount_span").html("Discount cannot be more than 100.");
            $("#discount").val('');
          }
        }

        function calcDisValues()
        {

          var best_price = $("#best_price").val();
          var total = $("#showTotal").html();
          best_price = parseInt(best_price);
          total = parseInt(total);
          if(best_price>0 && best_price < total){ 
            if(total>0)         {
              var bestprice_1 = total - best_price;
              bestprice_2 =(bestprice_1*100)/total;
              $("#discount").val((bestprice_2).toFixed(3));
            }else{
              $("#discount").val('');
            }
            
          }else{
            $("#bestprice_span").html("Best price cannot be greater than original price.");
            $("#best_price").val('');
          }
        }

      function expendDiv(sc)
      {

        $('#serviceTypeDiv').show();

        if ($(".parents_tr")[0]){
            $(".parents_tr").remove();
            $(".tfoot_show").remove();
                        
        }
        $("#serviceList").html('');
        $(".service_type").prop('checked',false);
      
      }
        
      $(".service_type").change(function(){

        $("#serviceDiv").show();
        filterServices();
      });

    

      $(".service_type").change(function(){
        if ($(".parents_tr")[0]){
            $(".parents_tr").remove();
            $(".tfoot_show").remove();
        }
       // $(".service-li-list").remove();
      });
      
      function filterServices()
      {
        var service_criteria = $("input[name='gender']:checked").val();
        var service_type = $("input[name='service_type']:checked").val();
        // var service_name = serviceVal.value;
           
          jQuery.ajax({
            url: '{{route('ajax.offers.get.services')}}',
            type: 'POST',
            data:{gender:service_criteria,service_type:service_type},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
             // console.log(response);
             $("#serviceList").html(response);
            }
              
          });
      }

      $(function() {
        $('.selectpicker').selectpicker();
      });
    </script>
   
    <script>    
       
       
         $( function() {
          $("#datepicker").datepicker({ 
            dateFormat: 'yy-mm-dd',
            changeYear: true,
            changeMonth: true,
            minDate: 0,
            });
          $("#datepicker").on("change",function(){
            var selected = $(this).val();
                      
            jQuery.ajax({
            url: '{{route('ajax.get.time')}}',
            type: 'POST',
            data:{selected_date:selected},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
              $("#appointment_time_div").html(response);
              
              }
            });
        });
        } );

         function isNumber(evt) {
              evt = (evt) ? evt : window.event;
              var charCode = (evt.which) ? evt.which : evt.keyCode;
              if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                  return false;
              }
              return true;
          }

    </script>

@endsection