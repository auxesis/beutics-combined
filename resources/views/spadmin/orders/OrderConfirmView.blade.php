@extends('layouts.spadmin.admin')

@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/jquery.gritter/css/jquery.gritter.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/themes/default/css/uniform.default.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css">
   <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/datepicker/jquery-ui.css">


@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>{{$title}}</h5>
            </header>
             @include('message')
            <div id="collapse2" class="body">
                <!-- <form class="form-horizontal" id="popup-validation"> -->
                {!! Form::open(['route' => 'orders.order_confirm', 'method' => 'post', 'class' => 'form-horizontal','id' => 'popup-validation','enctype' => 'multipart/form-data']) !!}
                {{ csrf_field() }}
                    <input type="hidden" name="order_id" value="<?= $result['id']; ?>" />
                    
                    <div class="form-group">
                        <label for="appointment_date" class="control-label col-lg-4">{{ __('messages.Customer Name') }}</label>
                        <div class="col-lg-4" style="margin-top:5px;">
                            <?= $result['user_name']; ?>
                        </div>
                    </div>

                   <div class="form-group">
                        {{Form::label('staff_id', __('messages.Select Staff') , ['class' => 'control-label col-lg-4'])}}
                        
                         <div class="col-lg-4">
                            <?php 
                            $staff_arr = array();
                              foreach($staff_name_rows as $row){
                                  $staff_arr[$row['id']] = $row['staff_name'];
                              }
                            ?>
                              {{ Form::select('staff_id', $staff_arr,$result['staff_id'],['class' => 'form-control validate[required]','placeholder'=>__('messages.Choose Any') ]) }}
                        </div> <div class="clearfix"></div>
                    </div>
                   <div class="form-group">
                        {{Form::label('appointment_date', __('messages.Appointment Date'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('appointment_date',$result['appointment_date'], ['class' => 'form-control validate[required]','id'=>'datepicker','autocomplete'=>'off']) }}
                        </div>
                    </div>
                     <div class="form-group">
                        {{Form::label('appointment_time', __('messages.Appointment Time') , ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4" id="appointment_time_div">
                            {{ Form::select('appointment_time', [],null,['class' => 'form-control validate[required]','placeholder'=>__('messages.Select Appointment Time'),'id'=>'appointment_time']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('hours', __('messages.Select Hours') , ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                             <?php for($i = 0; $i <= 12; $i++): 
                           if($i==0){
                              $hours[$i] = $i.'hour';
                            } else if($i==1){
                              $hours[$i] = '0'.$i.'hour';
                            } else if($i<10){
                              $hours[$i] = '0'.$i.'hours';
                            } else {
                              $hours[$i] = $i.'hours';
                            }  
                            
                           endfor ?>
                            {{ Form::select('hours', $hours,null,['class' => 'form-control validate[required]','placeholder'=>__('messages.Select Hours'),'id'=>'hours']) }}
                        </div>
                        <div class="col-lg-4">
                            <?php $minutes = array('0' => '0 '.'minute', '15' => '15 '.'minutes','30' => '30 '.'minutes', '45' => '45 '.'minutes'); ?>
                            {{ Form::select('minutes', $minutes,null,['class' => 'form-control validate[required]','placeholder'=>__('messages.Select Minutes') ,'id'=>'hours']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="appointment_date" class="control-label col-lg-4">{{ __('messages.Total Amount') }}</label>
                        <div class="col-lg-4" style="margin-top:5px;">
                            <?= $result['total_item_amount'].' AED'; ?>
                        </div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>{{ __('messages.Payment Status') }}</b></div>
                        <div class="col-lg-8"><?= $result['payment_type'] == "0" ? "Paid Online":"Pay At Store"; ?></div> <div class="clearfix"></div>
                    </div>


                  
                    <div class="clearfix"></div>
                  <header class="dark">
                  <div class="icons"><i class="fa fa-list"></i></div>
                    <h5>{{ __('messages.Orderd Items') }}</h5>

                 </header><br>
                   <div class="new_section">
                        <table class="owntable table table-borered">
                          <thead>
                            <tr>
                              <th scope="col">{{ __('messages.Item') }}</th>
                              <th scope="col">{{ __('messages.Type') }}</th>
                              <th scope="col">{{ __('messages.QTY') }}</th>
                              <th scope="col">{{ __('messages.Gender') }}</th>
                              <th scope="col">{{ __('messages.Price') }}</th>
                            </tr>
                          </thead>
                          <tbody>
                           <?php $total = 0;$terms=''; ?>
                            @foreach($result['booking_items'] as $srow)
                            

                            <tr>
                              <td>{{$srow['name']}}
                              <?php if(!empty($srow['services'])){ ?>: <div style="font-size:12px;"><i>
                                  @foreach($srow['services'] as $servrow)
                                   {{$servrow['name']}}(<b> {{$servrow['quantity']}} </b>), 
                                 @endforeach
                               </i>
                               <br/>
                               <?php 
                               $terms=preg_replace('/[^\da-z ]/i', '', $srow['offer_terms']);
                               $details=preg_replace('/[^\da-z ]/i', '', $srow['offer_details']);
                                ?>
                               <span>
                                  <a data-toggle="modal" data-target="#modalTermCondition" onclick="showModal('<?= $srow['name'];?>' , '<?= $terms;?>', '<?= $details;?>')"><b style="color:red;">View {{$srow['name']}} Details</b></a>
                               </span></div>
                                 <?php } ?>
                              </td>
                              <td><b><?= isset($srow['service_id']) ?  __('messages.Service'):__('messages.Offer'); ?></b></td>
                              <td>{{$srow['quantity']}}</td>
                              <td>{{$srow['gender']}}</td>
                              <?php if($result['db_user_name']){ ?>
                                <td><?php echo $srow['original_price'].' AED'; ?></td>  
                              <?php } else { ?>
                                <td><?php echo $srow['best_price'].' AED'; ?></td>  
                              <?php } ?>                   
                            </tr>
                             @endforeach
                          </tbody>
                        </table>   
                      </div>
                    <div class="form-actions no-margin-bottom">
                                            
                        <input type="submit" value="{{ __('messages.Submit') }}" class="btn btn-primary">
                    </div>
                    
                </form>
            </div>
        </div>  
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- Modal -->
<div id="modalTermCondition" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="offer_name">
          
          
        </h4>
      </div>
      <div class="modal-body" >
        <h5>Offer Terms & Condition</h5>
        <p id="offer_terms"></p>
        <br/>
        <h5>Offer Details</h5>
        <p id="offer_details"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
@endsection
@section('uniquescript')

    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>

     <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/holder/2.4.1/holder.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/jquery.uniform.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="{{ url('/') }}/assets/lib/datepicker/jquery-ui.js"></script>
        
    <script>    
      function showModal(offer_name,offer_terms,offer_details){
        $("#offer_name").html(offer_name+" Details");
          $("#offer_terms").html(offer_terms);
          $("#offer_details").html(offer_details);
      }
        $(function() {
          Metis.formValidation();
        });

        <?php if($result['appointment_date']){ ?>
          jQuery.ajax({
            url: '{{route('ajax.get.time')}}',
            type: 'POST',
            data:{selected_date:'<?= $result['appointment_date'] ?>', selected_time:'<?= date('H:i', strtotime($result['appointment_time'])); ?>'},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
              $("#appointment_time_div").html(response);
              
              }
            });
        <?php } ?>

         $( function() {
          var a = new Date();
            var timeZoneOffset = +4*60;
            a.setMinutes(a.getMinutes() + a.getTimezoneOffset() + timeZoneOffset );
          $("#datepicker").datepicker({ 
            dateFormat: 'yy-mm-dd',
            changeYear: true,
            changeMonth: true,
            minDate: 0,a,
            });
          $("#datepicker").on("change",function(){
            var selected = $(this).val();
                      
            jQuery.ajax({
            url: '{{route('ajax.get.time')}}',
            type: 'POST',
            data:{selected_date:selected},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
              $("#appointment_time_div").html(response);
              
              }
            });
        });
        } );
    </script>

@endsection