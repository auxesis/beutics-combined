@extends('layouts.spadmin.admin')
@section('uniquecss')

<style type="text/css">
    table.owntable th {
        background-color: #337ab7;
        color: #fff;
    }
</style>

@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>{{ __('messages.View Order Details') }}</h5>

            </header>
            <div id="collapse2" class="body">
              <div class="row">
                 @include('message')
                <div class="col-sm-12 col-lg-12">
                  <?php if($result['status'] == '5'){ ?>
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>{{ __('messages.Order ID') }}</b></div>
                        <div class="col-lg-8"><?= $result['booking_unique_id']; ?></div> <div class="clearfix"></div>
                    </div>
                    <?php } ?>
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>{{ __('messages.Customer Name') }}</b></div>
                        <div class="col-lg-8"><?= $result['user_name']; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>{{ __('messages.Staff Name') }}</b></div>
                        <div class="col-lg-8"><?= $result['staff_name']; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>{{ __('messages.Appointment Date') }}</b></div>
                        <div class="col-lg-8"><?= $result['appointment_date']; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>{{ __('messages.Appointment Time') }}</b></div>
                        <div class="col-lg-8"><?= ($result['appointment_time'])?date('h:i A', strtotime($result['appointment_time'])):''; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>{{ __('messages.Service End Time') }}</b></div>
                        <div class="col-lg-8"><?= ($result['service_end_time'])?date('h:i A', strtotime($result['service_end_time'])):''; ?></div>
                        
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>{{ __('messages.Total Amount') }}</b></div>
                        <div class="col-lg-8"><?= $result['total_item_amount'].' AED'; ?></div> <div class="clearfix"></div>
                    </div>

                  
                    <?php if($result['invoice']){ ?>
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>{{ __('messages.Invoice') }}</b></div>
                        <div class="col-lg-8"><a href="{{ asset('sp_uploads/invoice/'.$result['invoice']) }}" class="" target="_blank">View Invoice</a></div> <div class="clearfix"></div>
                    </div>
                    <?php } ?>
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>{{ __('messages.Order Date and Time') }}</b></div>
                        <div class="col-lg-8"><?= $result['created_date']; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>{{ __('messages.Payment Status') }}</b></div>
                        <div class="col-lg-8"><?= $result['payment_type'] == "0" ? "Paid Online":"Pay At Store"; ?></div> <div class="clearfix"></div>
                    </div>


                    <div class="clearfix"></div><br><br>
                  <header class="dark">
                  <div class="icons"><i class="fa fa-list"></i></div>
                    <h5>{{ __('messages.Order Items') }}</h5>

                 </header><br>
                   <div class="new_section">
                        <table class="owntable table table-borered">
                          <thead>
                            <tr>
                              <th width="30%" scope="col">{{ __('messages.Item') }}</th>
                              <th scope="col">{{ __('messages.Type') }}</th>
                              <th scope="col">{{ __('messages.QTY') }}</th>
                              <th scope="col">{{ __('messages.Gender') }}</th>
                              <th scope="col">{{ __('messages.Price') }}</th>
                            </tr>
                          </thead>
                          <tbody>
                           <?php $total = 0;$terms=''; ?>
                            @foreach($result['booking_items'] as $srow)
                            

                            <tr>
                              <td>{{$srow['name']}}
                              <?php if(!empty($srow['services'])){ ?>: <div style="font-size:12px;"><i>
                                  @foreach($srow['services'] as $servrow)
                                   {{$servrow['name']}}(<b> {{$servrow['quantity']}} </b>), 
                                 @endforeach
                               </i>
                               <br/>
                               <?php 
                               $terms=preg_replace('/[^\da-z ]/i', '', $srow['offer_terms']);
                               $details=preg_replace('/[^\da-z ]/i', '', $srow['offer_details']);
                                ?>
                               <span>
                                  <a data-toggle="modal" data-target="#modalTermCondition" onclick="showModal('<?= $srow['name'];?>' , '<?= $terms;?>', '<?= $details;?>')"><b style="color:red;">View {{$srow['name']}} Details</b></a>
                               </span></div>
                                 <?php } ?>
                              </td>
                              <td><b><?= isset($srow['service_id']) ?  __('messages.Service'):__('messages.Offer'); ?></b></td>
                              <td>{{$srow['quantity']}}</td>
                              <td>{{$srow['gender']}}</td>
                              <?php if($result['db_user_name']){ ?>
                                <td><?php echo $srow['original_price'].' AED'; ?></td>  
                              <?php } else { ?>
                                <td><?php echo $srow['best_price'].' AED'; ?></td>  
                              <?php } ?>                   
                            </tr>
                             @endforeach
                          </tbody>
                        </table>   
                      </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- Modal -->
<div id="modalTermCondition" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="offer_name">
          
          
        </h4>
      </div>
      <div class="modal-body" >
        <h5>Offer Terms & Condition</h5>
        <p id="offer_terms"></p>
        <br/>
        <h5>Offer Details</h5>
        <p id="offer_details"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
@endsection
@section('uniquescript')
<script type="text/javascript">
  function showModal(offer_name,offer_terms,offer_details){
        $("#offer_name").html(offer_name+" Details");
          $("#offer_terms").html(offer_terms);
          $("#offer_details").html(offer_details);
      }
  
</script>
@endsection