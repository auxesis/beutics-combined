@if($shoptime_slots)
{{ Form::select('appointment_time', $shoptime_slots,$selected_time,['class' => 'form-control validate[required]','placeholder'=>__('messages.Select Appointment Time'),'id'=>'appointment_time']) }}
@else
{{ __('messages.No times Found') }}
@endif