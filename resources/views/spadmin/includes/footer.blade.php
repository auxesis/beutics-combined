<footer class="Footer bg-dark dker">
    <p>{{ __('messages.Copyright') }} &copy; {{ date('Y') }} {{ __('messages.Beutics All rights reserved') }}. </p>
</footer>
<!-- /#footer -->
