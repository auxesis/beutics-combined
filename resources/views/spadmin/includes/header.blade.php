<div  class="bg-dark dk" id="wrap"> 
<div id="top">
    <!-- .navbar -->
    <nav class="navbar navbar-inverse navbar-static-top">
        <div class="container-fluid">
    
    
            <!-- Brand and toggle get grouped for better mobile display -->
            <header class="navbar-header">
    
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="index.html" class="navbar-brand"><img src="{{ url('/') }}/assets/img/logo.png" alt="" style="width:40px;height:45px; !important"></a>
    
            </header>
    
    
    
            <div class="topnav">
                <?php $user_data = spLoginData();//helper function?>
               <div class="btn-group">
                    <a data-placement="bottom" href="{{url('service-provider/beutics-chat')}}" data-original-title="{{ __('messages.Customer Chat Notification') }}" data-toggle="tooltip"
                       class="btn btn-default btn-sm">
                        <i class="fa fa-comments-o" aria-hidden="true"></i> {{ __('messages.Customer Messages') }}
                        <span style="position: absolute;right: -5px;top: -6px;background-color: #449d44;width: 18px;color: #fff;border-radius: 50%;" id="tbn_users">{{unreadChat($user_data['id'],'sp_users','users')}}</span>
                    </a>
                </div>

                <div class="btn-group">
                    <a data-placement="bottom" href="{{url('service-provider/beutics-chat')}}" data-original-title="{{ __('messages.Admin Chat Notification') }}" data-toggle="tooltip"
                       class="btn btn-default btn-sm">
                        <i class="fa fa-comments-o" aria-hidden="true"></i> {{ __('messages.Admin Messages') }}
                        <span style="position: absolute;right: -5px;top: -6px;background-color: #449d44;width: 18px;color: #fff;border-radius: 50%;" id="tbn_admins">{{unreadChat($user_data['id'],'sp_users','admins')}}</span>
                    </a>
                </div>
                <div class="btn-group">
                    <?php if(app()->getLocale() == 'en'):
                        $arr_class = 'btn-default';
                        $en_class = 'btn-success';
                    else:
                        $arr_class = 'btn-success';
                        $en_class = 'btn-default';
                    endif;
                    ?>
                    <!-- <a class="btn <?php echo $en_class; ?> btn-sm" href="{{ url('locale/en') }}" ><i class="fa fa-language"></i> EN</a> -->

                   <!--  <a class="btn <?php echo $arr_class; ?> btn-sm" href="{{ url('locale/ar') }}" ><i class="fa fa-language"></i> AR</a> -->
                </div>
                <div class="btn-group">
                    <a data-placement="bottom" data-original-title="Fullscreen" data-toggle="tooltip"
                       class="btn btn-default btn-sm" id="toggleFullScreen">
                        <i class="glyphicon glyphicon-fullscreen"></i>
                    </a>
                </div>
                <div class="btn-group">
                    <a href="{{route('spadmin.logout')}}" data-toggle="tooltip" data-original-title="Logout" data-placement="bottom"
                       class="btn btn-metis-1 btn-sm">
                        <i class="fa fa-power-off"></i>
                    </a>
                </div>
                <div class="btn-group">
                    <a data-placement="bottom" data-original-title="Show / Hide Left" data-toggle="tooltip"
                       class="btn btn-primary btn-sm toggle-left" id="menu-toggle">
                        <i class="fa fa-bars"></i>
                    </a>
                </div>
    
            </div>
    
    
    
    
            <div class="collapse navbar-collapse navbar-ex1-collapse">
    
                <!-- .nav -->
                <ul class="nav navbar-nav">
                    <li class="active"><a href="{{route('spadmin.dashboard')}}">{{ __('messages.Dashboard') }}</a></li>
                    <!-- <li><a href="table.html">Tables</a></li> -->
                    <li class='dropdown '>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ __('messages.Settings') }}
                             <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{route('spuser.change-password')}}">{{ __('messages.Change Password') }}</a></li>

                            <li><a href="{{route('spuser.edit-profile')}}">{{ __('messages.Profile') }}</a></li>

                            <li><a href="{{route('spuser.gallery')}}">{{ __('messages.Banner Images') }}</a></li>

                            <li><a href="{{route('spuser.shop-timings')}}">{{ __('messages.Shop Timings') }}</a></li>
                            
                        </ul>
                    </li>
                </ul>
                <!-- /.nav -->
            </div>
        </div>
        <!-- /.container-fluid -->
    </nav>
    <!-- /.navbar -->
       <!--  <header class="head">
                 <div class="search-bar">
                   <form class="main-search" action="">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Live Search ...">
                            <span class="input-group-btn">
                                <button class="btn btn-primary btn-sm text-muted" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    <!-- /.main-search -->        
                 </div>
                <!-- /.search-bar -->
           <!--  <div class="main-bar">
                <h3>
                    <i class="fa fa-dashboard"></i>&nbsp;
                    Dashboard
                </h3>
            </div> -->
            <!-- /.main-bar -->
        <!-- </header> --> 
        <!-- /.head -->
</div>
<!-- /#top -->
</div>