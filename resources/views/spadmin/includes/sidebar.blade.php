<div id="left">
    <div class="media user-media bg-dark dker">
        <div class="user-media-toggleHover">
            <span class="fa fa-user"></span>
        </div>
        <?php
          $user_data = spLoginData(); 
          $name =  $user_data->name;
          $profile =  $user_data->profile_image;

          ?>
        <div class="user-wrapper bg-dark">
            <a class="user-link" href="">
                <!-- <img class="media-object img-thumbnail user-img" alt="User Picture" src="{{ url('/').'/'.$profile }}" width="70" height="70"> -->
                @if($profile)
                <?php $url = changeImageUrlForFileExist(url('/').'/'.'public/sp_uploads/profile/'.$profile);?>
                <img class="media-object img-thumbnail user-img" alt="User Picture" src="{{ $url}}" style="width:70px; height:70px;">
                @else
                <img class="media-object img-thumbnail user-img" alt="User Picture" src="{{ url('/').'/' }}public/images/defualt.jpeg" style="width:70px; height:70px;">
                @endif
            </a>
    
            <div class="media-body">
                <h5 class="media-heading"> </h5>
                <ul class="list-unstyled user-info">
                    <li><a href="">{{ucfirst($name)}}</a></li>
                    <li>{{ __('messages.Service Provider') }}<br>
                        <small>&nbsp;</small>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- #menu -->
    <ul id="menu" class="bg-blue dker" style="background-color:#333 !important;">
        <li class="nav-header">{{ __('messages.Menu') }}</li>
        <li class="nav-divider"></li>
        <li class="">
          <a href="{{route('spadmin.dashboard')}}">
            <i class="fa fa-dashboard"></i><span class="link-title">&nbsp;{{ __('messages.Dashboard') }}</span>
          </a>
        </li>

        <li class="">
          <a href="{{route('my-accounts')}}">
            <i class="fa fa-user"></i><span class="link-title">&nbsp;{{ __('messages.My Account') }}</span>
          </a>
        </li>

        <li class="">
          <a href="{{route('my-gallery.index')}}">
            <i class="fa fa-user"></i><span class="link-title">&nbsp;{{ __('messages.My Gallery') }}</span>
          </a>
        </li>

        <li class="">
          <a href="{{route('staffs.index')}}">
            <i class="fa fa-user"></i><span class="link-title">&nbsp;{{ __('messages.My Staff') }}</span>
          </a>
        </li>

        <li class="">
          <a href="{{route('products.index')}}">
            <i class="fa fa-product-hunt"></i><span class="link-title">&nbsp;{{ __('messages.My Products') }}</span>
          </a>
        </li>

         <li class="">
          <a href="{{route('spuser.agreement')}}">
            <i class="fa fa-file"></i><span class="link-title">&nbsp;{{ __('messages.My Agreement') }}</span>
          </a>
        </li>

        <li class="">
          <a href="{{route('services.index')}}">
            <i class="fa fa-cogs"></i><span class="link-title">&nbsp;{{ __('messages.My Services') }}</span>
          </a>
        </li>

        <li class="">
          <a href="{{route('new-services.index')}}">
            <i class="fa fa-cogs"></i><span class="link-title">&nbsp;{{ __('messages.My New Services') }}</span>
          </a>
        </li>

        <li class="">
          <a href="{{route('offers.index')}}">
            <i class="fa fa-gift"></i><span class="link-title">&nbsp;{{ __('messages.My Offers') }}</span>
          </a>
        </li>

        <li class="">
          <a href="{{route('new-offers.index')}}">
            <i class="fa fa-gift"></i><span class="link-title">&nbsp;{{ __('messages.My New Offers') }}</span>
          </a>
        </li>

        <li class="">
            <a href="javascript:;">
              <i class="fa fa-gift "></i>
              <span class="link-title">{{ __('messages.My Forums') }}</span>
              <span class="fa arrow"></span>
            </a>
            <ul class="collapse" aria-expanded="false">
              <li class="@if(isset($module) && $module == 'forums') active @endif" >
                <a href="{{route('forum.customer.queries')}}">
                  <i class="fa fa-gift"></i>&nbsp; {{ __('messages.Queries') }} </a>
              </li>

               <li class="@if(isset($module) && $module == 'forums') active @endif" >
                <a href="{{route('forum.customer.responded.queries')}}">
                  <i class="fa fa-gift"></i>&nbsp; {{ __('messages.Responded') }} </a>
              </li>
            </ul>
        </li> 

        <li class="">
            <a href="javascript:;">
              <i class="fa fa-gift "></i>
              <span class="link-title">{{ __('messages.My Orders') }}</span>
              <span class="fa arrow"></span>
            </a>
            <ul class="collapse" aria-expanded="false">
              <li class="@if(isset($module) && $module == 'orders') active @endif" >
                <a href="{{route('orders.index')}}">
                  <i class="fa fa-gift"></i>&nbsp; {{ __('messages.All Open Orders') }} </a>
              </li>
              <li class="@if(isset($module) && $module == 'orders') active @endif" >
                <a href="{{route('orders.confirmed_orders')}}">
                  <i class="fa fa-gift"></i>&nbsp; {{ __('messages.All Confirmed Orders') }} </a>
              </li>
              <li class="@if(isset($module) && $module == 'orders') active @endif" >
                <a href="{{route('orders.completed_orders')}}">
                  <i class="fa fa-gift"></i>&nbsp; {{ __('messages.All Completed Orders') }} </a>
              </li>
              <li class="@if(isset($module) && $module == 'orders') active @endif" >
                <a href="{{route('orders.cancelled_order')}}">
                  <i class="fa fa-gift"></i>&nbsp; {{ __('messages.All Cancelled Orders') }} </a>
              </li>
            </ul>
        </li> 

        <li class="">
          <a href="{{route('my-reviews')}}">
            <i class="fa fa-gift"></i><span class="link-title">&nbsp;{{ __('messages.My Reviews') }}</span>
          </a>
        </li>

        <li class="">
          <a href="{{route('spuser.beutics-chat')}}">
            <i class="fa fa-comments-o"></i><span class="link-title">&nbsp;{{ __('messages.Beutics Chat') }}</span>
          </a>
        </li>  
        <li class="">
          <a href="{{route('staffs.staff-calendar')}}">
            <i class="fa fa-calendar"></i><span class="link-title">&nbsp;{{ __('messages.Staff Calendar') }}</span>
          </a>
        </li>  
    </ul>
    <!-- /#menu -->
</div>
<!-- /#left -->