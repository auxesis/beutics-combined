@extends('layouts.spadmin.admin')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>{{ __('messages.View Agreement') }}</h5>

            </header>
            <div id="collapse2" class="body">
                <div class="row">
                     @include('message')
                    <div class="col-sm-12 col-lg-12">
                        <div class="col-lg-12 border_details">
                            <div>
                                <b><p align="center">{{__('messages.Hello')}} {{$row->store_name}},</p></b>
                                <!-- <b><p align="center" style="color:red;">Agreed Revenue share with Beutics {{round($tier->commission,0)}}% of the Order value.</p></b> -->

                                <b><p align="center" style="color:red;">{{__('messages.Agreement Title',['comm' => round($tier->commission,0)])}}</p></b>
                            </div>
                            <div class="col-lg-8" style="margin-left:385px;">
                                 <a href="{{ asset('sp_uploads/approve_pdf/'.$row->agreement) }}" class="btn btn-primary" target="_blank">{{ __('messages.View Agreement') }}</a>
                              
                                 <br/><br/>
                            </div> 

                            <div class="clearfix"></div>
                        </div>
                        </br></br>
                        {!! Form::open(['route' => 'spuser.mov', 'method' => 'POST', 'id' => 'popup-validation', 'enctype' => 'multipart/form-data']) !!}
                        {{ csrf_field() }}

                        <div  class="col-lg-12">
                            <div class="form-group">
                                <div class="col-lg-4">
                                    {{Form::checkbox('is_provide_home_service', '1',$row->is_provide_home_service,['class' => 'checkbox','style'=>'display:inline;'])}} {{ __('messages.Is Provide Home Services?') }}

                                </div>
                            </div>                          
                            <br/><br/>
                        </div>
                        <?php 
                        $style_name = ($row->is_provide_home_service == 1) ? '' : 'display:none;'; 
                        ?>
                           
                        <div  class="col-lg-12 shownDiv" style="<?= $style_name ?>">
                            <div class="form-group">
                                <div class="col-lg-4">

                                    {{ Form::text('mov',$row->mov, ['class' => 'form-control validate[required]','placeholder' => __('messages.Enter Minimum Order Value'),'maxlength'=>'10']) }}
                                </div>
                            </div>                          
                            <br/><br/>

                            
                        </div> 
                        <div class="form-actions no-margin-bottom">
                                <input type="submit" value="{{ __('messages.Submit') }}" class="btn btn-primary">
                            </div>
                                      
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>

@endsection
@section('uniquescript')
    <script>
       $(document).ready(function() {
            $('.checkbox').on('change', function() {
                if($('.checkbox').prop("checked") == true){
                    $('.shownDiv').show();
                }else{
                    $('.shownDiv').hide();
                }
            });
        });
    </script>
@endsection

