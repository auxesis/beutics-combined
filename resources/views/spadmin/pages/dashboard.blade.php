@extends('layouts.spadmin.admin')

@section('content')
    <?php
    $user_data = spLoginData(); 
    $name =  $user_data->name;
    $profile =  $user_data->profile_image;

    ?>
@include('message')
 <div class="row">
  <div class="col-md-12"><div class="hbox hbox-1 text-center">
    <h3><?php echo ucfirst($result_arr['store_name']); ?></h3>
    <p><?php echo $result_arr['address']; ?></p>
     <p> @if($result_arr['today_shop_opening_time'] == '00:00:00'){{ __('messages.Closed Today') }} @else {{ __('messages.Today ').$result_arr['today_shop_opening_time']}} @endif</p>
  </div></div> 
  <div class="col-md-3"><div class="hbox hbox-2">
    <h3>{{ __('messages.ORDERS') }}</h3>
    <hr>
    <ul>
     <a href="{{route('orders.index')}}" target="_blank" style="color: inherit;"> <li>{{ __('messages.New (In Store)') }}<span>{{$result_arr['orders']['todays_new_shop_orders']}}</span></li></a>
      <a href="{{route('orders.index')}}" target="_blank" style="color: inherit;"><li>{{ __('messages.New (At Home)') }}<span>{{$result_arr['orders']['todays_new_home_orders']}}</span></li></a>

      <li>{{ __('messages.Sales (Online)') }}<span>AED {{($result_arr['orders']['todays_online_sales'] != ''?$result_arr['orders']['todays_online_sales']:'0')}}</span></li>

      <li>{{ __('messages.Sales (Offline)') }}<span>AED {{($result_arr['orders']['todays_offline_sales']!=''?$result_arr['orders']['todays_offline_sales']:'0')}}</span></li>
      <li>{{ __('messages.Confirmed') }} <span>{{$result_arr['orders']['todays_confirmed_orders']}} {{ __('messages.of') }} {{$result_arr['orders']['todays_new_shop_orders'] + $result_arr['orders']['todays_new_home_orders']}}</span></li>
      <li>{{ __('messages.Closed') }}<span>{{$result_arr['orders']['todays_closed_orders']}}</span></li>
      <li></li>
    </ul>
  </div></div>
  <div class="col-md-3"><div class="hbox hbox-3">
    <h3>{{ __('messages.OPERATIONS') }}</h3>
    <hr>
    <a href="{{route('staffs.staff-calendar')}}" target="_blank" style="color: inherit;">
      <ul>
      <li>{{ __('messages.Appointments (In Store)') }}<span>{{$result_arr['operations']['todays_shop_appointments']}}</span></li>
      <li>{{ __('messages.Appointments (At Home)') }}<span>{{$result_arr['operations']['todays_home_appointments']}}</span></li>
      <li>{{ __('messages.Staff Strength') }}<span>{{$result_arr['operations']['available_staff']}} {{ __('messages.of') }} {{$result_arr['operations']['total_staff']}}</span></li>
      <li>{{ __('messages.Cancellations') }}<span>{{$result_arr['operations']['todays_cancellations']}}</span></li>
      <li></li>
    </ul>
  </a>
  </div></div>
  <div class="col-md-3"><div class="hbox hbox-4">
    <h3>{{ __('messages.Reality Check') }}</h3>
    <hr>
    <a href="{{route('my-reviews')}}" target="_blank" style="color: inherit;">
    <ul>
      <li>Beutics {{ __('messages.Score') }} 
        <?php if(isset($result_arr['reality_check']['beutics_score'])){ ?>
        <span>{{$result_arr['reality_check']['beutics_score']}}<b> {{__('messages.'.$result_arr['reality_check']['beutics_score_label'][0])}}</b></span>
      <?php }?>
      </li>

      <?php if($result_arr['reality_check']['staff_preferred']['staff_name'] != ''){ ?>
      <li>{{$result_arr['reality_check']['staff_preferred']['staff_name']}}<span>{{ __('messages.Likes') }} ({{$result_arr['reality_check']['staff_preferred']['total_like']}})</span></li>
      <?php }?>

      <?php if($result_arr['reality_check']['offer_preferred']['offer_name'] != ''){ ?>
      <li>{{$result_arr['reality_check']['offer_preferred']['offer_name']}}<span>{{ __('messages.Likes') }}  ({{$result_arr['reality_check']['offer_preferred']['total_like']}})</span></li>
       <?php }?>

      <?php if($result_arr['reality_check']['service_preferred']['name'] != ''){ ?>
      <li>{{$result_arr['reality_check']['service_preferred']['name']}}<span>{{ __('messages.Likes') }}  ({{$result_arr['reality_check']['service_preferred']['total_like']}})</span></li>
      <?php }?>
      <li></li>
    </ul>
  </a>
  </div></div>
  <div class="col-md-3"><div class="hbox hbox-5">
    <h3>{{ __('messages.CUSTOMERS') }}</h3>
    <hr>
    <ul>
      <li>{{ __('messages.Online') }}<span>{{$result_arr['customers']['total_online_customer']}}</span></li>
      <li>{{ __('messages.Referred') }}<span>{{$result_arr['customers']['total_referred_customer']}}</span></li>
      <li>{{ __('messages.Forum Status') }}<span>{{$result_arr['customers']['pending_response']}} {{ __('messages.of') }} {{$result_arr['customers']['todays_ask_received']}}</span></li>
     
    </ul>
  </div></div>
 </div>
                       
@endsection

@section('uniquescript')
  <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.18.4/js/jquery.tablesorter.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-sparklines/2.1.2/jquery.sparkline.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/flot/0.8.3/jquery.flot.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/flot/0.8.3/jquery.flot.selection.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/flot/0.8.3/jquery.flot.resize.min.js"></script>

  <script>
    $(function() {
      Metis.dashboard();
    });
</script>
@endsection
