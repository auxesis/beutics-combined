<section id="landing_contact_section" class="landing_contact_section padding_tb_60">
	<div class="container">
		<!-- Beauty -->
		<div id="show_msg" style="text-align: center"></div>
		<div class="landing_contact_section_wrap" id="landing_contact_section_wrap">
			<form id="contactForm" action="{{ route('contact-form') }}">
				<input type="hidden" id="token" value="{{ @csrf_token() }}">
			  	<div class="landing_contact_wrapper">
				    <div class="landing_contact_item">
				    	<div class="section_header">
							<h2 class="sec_head">List your Business on Beautics</h2>
						</div>
						<div class="discription">Various choices in the market for Beauty, Fitness & Wellness providers</div>
				    </div>
				    <div class="landing_contact_item">
				    	<div class="form-group">
					      <label for="name">Full Name</label>
					      <span id="userName-info" class="info"></span><br/>
					      <input type="text" class="form-control" id="name" name="name">
					    </div>
					    <div class="form-group">
					      <label for="email">Email Address</label>
					      <input type="email" class="form-control" id="email" name="email">
					    </div>
					    <div class="form-group">
					      <label for="phone">Phone no.</label>
					      <input type="tel" class="form-control" id="phonenumber" name="phonenumber">
					    </div>
				    </div>
				    <div class="landing_contact_item">
				    	<div class="form-group">
						  <label for="Specialization">Specialization</label>
						  <select class="form-control" id="specialization" name="specialization">
						    <option>1</option>
						    <option>2</option>
						    <option>3</option>
						    <option>4</option>
						  </select>
						</div>
						<div class="form-group">
						  <label for="message ">YOUR MESSAGE</label>
						  <textarea class="form-control" rows="3" id="message" name="message"></textarea>
						</div>
						<div class="form-group text_align_right">
						  <button type="submit" class="btn btn-primary">Contact me </button>
						</div>
				    </div>
			  	</div>
		  	</form>
		</div>
	</div>
</section>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	$('#contactForm').on('submit',function(e){
		e.preventDefault();
		var url = $(this).attr('action');
		$.post(url, {
			'_token': $('#token').val(),
			name:$('#name').val(),
            email:$('#email').val(),
            phonenumber:$('#phonenumber').val(),
            specialization:$('#specialization').val(),
            message:$('#message').val()
		}, function (response){
			if(response.code === 400)
			{
				$(response.msg).each(function(index, key){
					$('#'+key).css('border-color','#ff0808');
				});
				return false;
			}else if(response.code === 200){
				var success = '<span class="alert">'+response.msg+'</span>';
				$('#show_msg').html(success);
				$("#contactForm")[0].reset();
			}
		});
	});

});
</script>