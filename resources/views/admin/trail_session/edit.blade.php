@extends('layouts.admin.admin')

@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/jquery.gritter/css/jquery.gritter.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/themes/default/css/uniform.default.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.min.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>{{__('messages.'.$title)}}</h5>
            </header>
             @include('message')
            <div id="collapse2" class="body">
                <!-- <form class="form-horizontal" id="popup-validation"> -->
                {!! Form::model($rows,['route' => ['trail-session.update',$rows->id], 'method' => 'PATCH', 'class' => 'form-horizontal','id' => 'trail-session','enctype' => 'multipart/form-data']) !!}
                {{ csrf_field() }}

                    <div class="form-group">
                        {{Form::label('name', __('messages.Session Name'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::select('trail_session_id', $session_list,$rows['id'],['class' => 'form-control validate[required] myselect','placeholder'=>'Select Session']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('question', __('messages.Question'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                          @foreach($questionList as $id => $list)
                            {{ Form::text('question['.$id.']',$list, ['class' => 'form-control validate[required]']) }}
                            <br>
                          @endforeach
                          <div id="items"></div>
                        </div>
                    </div>
                    <!--<div class="form-group">
                        {{Form::label('question', __('messages.Question'), ['class' => 'control-label col-lg-4'])}}
                          <div class="col-lg-4">
                            @php
                            $count = 0;
                              //$count = $questionList['count'];
                              unset($questionList['count']);
                            @endphp
                            @foreach($questionList as $id => $list)
                            <div class="input-group">
                              {{ Form::text('question['.$id.']',$list, ['class' => 'form-control validate[required]']) }}
                              <div class="input-group-addon">
                                <a style="padding: 0px 4px;" class="btn btn-danger btn-sm delete-question" data-id ="{{ $id }}" data-count="{{ $count }}" href="{{ route('trail-session.delete-question', ['id' => $id]) }}" data-method="DELETE"> <i class="fa fa-trash-o" aria-hidden="true"></i></a>  
                              </div>
                            </div>
                            <br>
                            @endforeach
                            <div id="items"></div>
                          </div>
                    </div>-->
                    <div class="form-group" style="text-align: right">
                      <label for="addmore" class="control-label col-lg-4"></label>
                      <div class="col-lg-4" >
                        <a href="#" id="addmore">{{ __('messages.Add More') }}</a>
                      </div>
                    </div>

                    <div class="form-actions no-margin-bottom">
                        <input type="submit" value="Submit" class="btn btn-primary">
                    </div>
                    <br><br><br><br>
                </form>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
@endsection
@section('uniquescript')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>

     <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/holder/2.4.1/holder.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/jquery.uniform.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>

    <script type="text/javascript">
      var numItems = $('.delete-question').length;
      if(numItems == 1){
        $(".delete-question").attr("href", "#");
        $("a").removeClass("delete-question");
      }
      $(document).on('click', '.delete-question', function (e)
      {
        var url = $(this).attr('href');
        var id = $(this).attr('data-id');
        var asso = $(this).attr('data-count');
        console.log(url);
        e.preventDefault(); // prevent form submit
        var msg = "Are you sure ?";
        if(asso > 0){
            var msg = "You cannot delete this Question because this is associated with User Faq.";
        }
        if(asso > 0){ 
          swal({
              title: msg,
              type: "warning",
              showCancelButton: true,
              showConfirmButton: false,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes",
              cancelButtonText: "Ok",
              closeOnConfirm: false,
              closeOnCancel: false,

          },
          function(isConfirm){
            if (isConfirm) {        
              //obj.submit();

            } else {
               swal.close();
            }
          });
        }
        else{
          swal({
              title: msg,
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes",
              cancelButtonText: "No",
              closeOnConfirm: false,
              closeOnCancel: false,

          },
          function(isConfirm){
            if (isConfirm) 
            {
              $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': '{{csrf_token()}}'
                  }
              });
              $.ajax({
                type: "POST",
                url: url,
                dataType: "JSON",
                data: {"id":id},
                success: function (response) {
                  swal({
                      title: "Deleted", 
                      text: response.message, 
                      type: response.status
                  },function() {
                      location.reload();
                  });
                },
                error: function(xhr) {
                  console.log(xhr.responseText);
                }   
              })
            } else {
               swal.close();
            }
          });
        }
      });
    </script>
    <script>
        $(function() {
          Metis.formValidation();
        });

        $(document).ready(function() {
            var max_fields = 20; //maximum input boxes allowed
            var wrapper = $("#items"); //Fields wrapper
            var add_button = $("#addmore"); //Add button ID

            var x = 1; //initlal text box count
            $(add_button).click(function(e)
            { //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div class="input-group"><input class="form-control" name="question[new'+x+']" type="text" value=""><div class="input-group-addon"><button type="button" style="padding: 0px 4px;" class="btn btn-danger btn-sm remove_field"><i class="fa fa-close" aria-hidden="true"></i></button></div></div><br>');
            }
            });
             
            $(wrapper).on("click",".remove_field", function(e){ //user click on remove field
              e.preventDefault(); 
              $(this).parent().closest('.input-group').next('br').remove(); 
              $(this).parent().closest('.input-group').remove();
              x--;
            })
        });
    </script>
@endsection