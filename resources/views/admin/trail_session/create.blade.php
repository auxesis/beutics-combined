@extends('layouts.admin.admin')

@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/jquery.gritter/css/jquery.gritter.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/themes/default/css/uniform.default.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>{{__('messages.'.$title)}}</h5>
          

            </header>
             @include('message')
            <div id="collapse2" class="body">
                <!-- <form class="form-horizontal" id="popup-validation"> -->
                {!! Form::open(['route' => 'trail-session.store', 'method' => 'POST', 'class' => 'form-horizontal','id' => 'trail-session','enctype' => 'multipart/form-data']) !!}
                {{ csrf_field() }}
                    
                    <div class="form-group">
                        {{Form::label('name', __('messages.Session Name'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('name','', ['class' => 'form-control validate[required]','maxlength'=>'50']) }}
                        </div>
                    </div>
                    
                    <div class="form-group">
                        {{Form::label('question', __('messages.Question'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            <div class="input-group">
                                {{ Form::text('question[]','', ['class' => 'form-control validate[required]']) }}
                                <div class="input-group-addon"></div>
                            </div>
                            <br>
                            <div id="items"></div>
                        </div>
                    </div>
                    <div class="form-group" style="text-align: right">
                      <label for="addmore" class="control-label col-lg-4"></label>
                      <div class="col-lg-4" >
                        <a href="#" id="addmore">{{ __('messages.Add More') }}</a>
                      </div>
                    </div>
                    <div class="form-actions no-margin-bottom">
                        <input type="submit" value="Submit" class="btn btn-primary">
                    </div>
                    <br><br><br><br>
                </form>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
@endsection
@section('uniquescript')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>

     <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/holder/2.4.1/holder.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/jquery.uniform.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    
    <script>
        $(function() {
          Metis.formValidation();
        });

        $(document).ready(function() {
            var max_fields = 20; //maximum input boxes allowed
            var wrapper = $("#items"); //Fields wrapper
            var add_button = $("#addmore"); //Add button ID

            var x = 1; //initlal text box count
            $(add_button).click(function(e)
            { //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div class="input-group question_group"><input class="form-control" name="question[]" type="text"><div class="input-group-addon"><button type="button" style="padding: 0px 4px;" class="btn btn-danger btn-sm remove_field"><i class="fa fa-close" aria-hidden="true"></i></button></div></div><br>');
            }
            });
             
            $(wrapper).on("click",".remove_field", function(e){ //user click on remove field
              e.preventDefault(); 
              $(this).parent().closest('.question_group').next('br').remove(); 
              $(this).parent().closest('.question_group').empty();
              x--;
            })
        });
    </script>
@endsection