@extends('layouts.admin.admin')
@section('uniquecss')

  <style>
    #percent-sign {
        top: 8px;
        left: 126px;
        color: #555;
        position: absolute;
        z-index: 1;
    }
  </style>
   <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">

@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>View Life Time Earning</h5>

            </header>
            <div id="collapse2" class="body">
                <div class="row">
                     @include('message')
                    <div class="col-sm-12 col-lg-12">
                        
                        <div class="col-lg-12 border_details">
                            <div class="col-lg-4"><b>Total Online Earnings</b></div>
                            <div class="col-lg-8"><?php echo round($total_online,2).' AED'; ?></div> <div class="clearfix"></div>
                        </div>
                        <div  class="col-lg-12 border_details">
                            <div class="col-lg-4"><b>Total Offline Earnings</b></div>
                            <div class="col-lg-8"><?php echo round($total_offline,2).' AED'; ?></div> <div class="clearfix"></div>
                        </div>
                        <div  class="col-lg-12 border_details">
                            <div class="col-lg-4"><b>Total Lifetime Earnings</b></div>
                            <div class="col-lg-8"><?php echo round($life_time_earning,2).' AED'; ?></div> <div class="clearfix"></div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>

@endsection
@section('uniquescript')
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>

@endsection