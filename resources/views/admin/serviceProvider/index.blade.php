@extends('layouts.admin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
  
 
@endsection

@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>Service Providers</h5>
                <!-- <a class="btn btn-primary pull-right" href="" style="margin-top:4px;">Create User</a> -->
            </header>
            <div id="collapse4" class="body">
              @include('message')

               <table id="user_datatable" class="table table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th width="15%">Name</th>
                        <th width="15%">Email</th>
                        <th width="10%">Store Name</th>
                        <th width="10%">Mobile No</th>
                        <th width="5%">Created At</th>
                        <th width="5%">Status</th>
                        <th width="5%">Featured</th>
                        <th width="5%">Tax Applied</th>
                        <th width="35%">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
                  
            </div>
        </div>
    </div>
</div>
@endsection

@section('uniquescript')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>
<script type="text/javascript">
  function callNative(){

    $("#user_datatable").DataTable().draw();
  }
  function loadStatusFilter()
  {

    jQuery("#user_datatable_filter").parent().parent().
      append('<div id="user_datatable_status_filter" class="col-sm-12 col-md-2"><label>Filter:</label><select name="columns[testtest]" class="form-control input-sm" id="custom-filter" onchange="callNative()"><option value="">All</option><option value="pending">Pending</option><option value="approved">Approved</option></select></div><div id="user_datatable_visibility_filter" class="col-sm-12 col-md-2"><label>Status:</label><select name="columns[testtest]" class="form-control input-sm" id="custom-visibility-filter" onchange="callNative()"><option value="">All</option><option value="active">Active</option><option value="inactive">Inactive</option></select></div><div id="user_datatable_verfication_filter" class="col-sm-12 col-md-2"><label>Email Filter:</label><select name="columns[testtest]" class="form-control input-sm" id="custom-verfication-filter" onchange="callNative()"><option value="">All</option><option value="verified">Verified Email</option><option value="unverified">Unverified Email</option></select></div>');

    jQuery('#user_datatable_length').parent().closest("div.col-sm-12").removeClass("col-md-12");  
    jQuery('#user_datatable_length').parent().closest("div.col-sm-12").addClass("col-md-12");  

    jQuery('#user_datatable_filter').parent().closest("div.col-sm-12").removeClass("col-md-12");  
    jQuery('#user_datatable_filter').parent().closest("div.col-sm-12").addClass("col-md-12");

  
  }
  function deleteRow(obj)
  {
    event.preventDefault(); // prevent form submit
    swal({
      title: "Are you sure?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {        
       obj.submit();

      } else {
         swal.close();
      }
    });
  }
    
    function changeStatus(id)
    {
      swal({
          title: "Are you sure?",
          type: "warning",
          showCancelButton: "No",
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Yes",
          cancelButtonText: "No",
        },
          function(){
            jQuery.ajax({
            url: '{{route('sp.users.status.update')}}',
            type: 'POST',
            data:{user_id:id},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
              $("#status_"+id).html(response);
            }
            });
          }
      );
    }

    function changeApproval(id)
    {
      swal({
          title: "Are you sure?",
          type: "warning",
          showCancelButton: "No",
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Yes",
          cancelButtonText: "No",
        },
          function(){
            jQuery.ajax({
            url: '{{route('sp.users.tax.visibility.update')}}',
            type: 'POST',
            data:{user_id:id},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
              $("#approvalStatus_"+id).html(response);
            }
            });
          }
      );
    }
    
    function changeFeaturedStatus(id)
    {
      swal({
          title: "Are you sure?",
          type: "warning",
          showCancelButton: "No",
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Yes",
          cancelButtonText: "No",
        },
          function(){
            jQuery.ajax({
            url: '{{route('sp.users.featuredstatus.update')}}',
            type: 'POST',
            data:{user_id:id},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
              $("#featuredstatus_"+id).html(response);
            }
            });
          }
      );
    }
 
    $(function() {
        var table = $('#user_datatable').DataTable({
           stateSave: true,
        processing: true,
        serverSide: true,
        order: [[0, "desc" ]],
        "ajax":{
        "url": '{!! route('sp.getdata') !!}',
                "dataType": "json",
                "type": "POST",
                 data: function(d) {
                    d._token= "{{csrf_token()}}";
                    d.status_filter = $('#custom-filter').val();
                    d.visibility_filter = $('#custom-visibility-filter').val();
                    d.verfication_filter = $('#custom-verfication-filter').val();
                }
        },
        columns: [
        { data: 'name', name: 'name', orderable:true },
        { data: 'email', name: 'email', orderable:true  },
        { data: 'store_name', name: 'store_name', orderable:true  },
        { data: 'mobile_no', name: 'mobile_no', orderable:true  },
        { data: 'created_at', name: 'created_at', orderable:true  },
        { data: 'status', name: 'status', orderable:true  },
        { data: 'is_featured', name: 'is_featured', orderable:true  },
        { data: 'is_tax_applied', name: 'is_tax_applied', orderable:true  },
        { data: 'action', name: 'action', orderable:true  },
        ],
        "columnDefs": [
        { "searchable": false, "targets": 0 }
        ]
        , language: {
        searchPlaceholder: "Search by name, email, mobile"
        },
        initComplete:function(){

          loadStatusFilter();
        }
});
});  


</script>
@endsection

