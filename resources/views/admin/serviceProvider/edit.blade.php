@extends('layouts.admin.admin')

@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/jquery.gritter/css/jquery.gritter.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/themes/default/css/uniform.default.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.min.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
  <style type="text/css">

    .croppie-container {
        width: 100%;
        height: auto!important;
    }
        
</style>

@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>{{$title}}</h5>
          

            </header>
              @include('message')
            <div id="collapse2" class="body">
                <!-- <form class="form-horizontal" id="popup-validation"> -->
                {!! Form::model($row,['route' => 'sp.update', 'method' => 'POST', 'class' => 'form-horizontal','id' => 'popup-validation','enctype' => 'multipart/form-data']) !!}
                {{ csrf_field() }}
                    <input type="hidden" name="id" value={{$row->id}}>
                    <div class="form-group">
                        {{Form::label('name', __('messages.Name'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('name',null, ['class' => 'form-control validate[required]']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('is_individual', __('messages.Store Type'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::radio('is_individual', '0', true, ['checked'=>($row->is_individual== 0)]) }}Store
                            {{ Form::radio('is_individual', '1', true, ['checked'=>($row->is_individual==1)]) }}Individual
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('is_verified', __('messages.Is Verified'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::radio('is_verified', '1', true, ['checked'=>($row->is_verified== 1)]) }}Yes
                            {{ Form::radio('is_verified', '0', true, ['checked'=>($row->is_verified==0)]) }}No
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('is_virtual_tour', __('messages.Is Virtual Tour'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::radio('is_virtual_tour', '1', true, ['checked'=>($row->is_virtual_tour== 1)]) }}Yes
                            {{ Form::radio('is_virtual_tour', '0', true, ['checked'=>($row->is_virtual_tour==0)]) }}No
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('is_free_trial', __('messages.Is Free Trial'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::radio('is_free_trial', '1', true, ['checked'=>($row->is_free_trial== 1)]) }}Yes
                            {{ Form::radio('is_free_trial', '0', true, ['checked'=>($row->is_free_trial==0)]) }}No
                        </div>
                    </div>


                    <div class="form-group">
                        {{Form::label('service_criteria', __('messages.Service Criteria'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            <?php $sc = explode(',',$row->service_criteria);?>

                            {{Form::checkbox('service_criteria[]','men' ,false,['checked'=>(in_array('men',$sc))])}}Men

                            {{Form::checkbox('service_criteria[]','women' ,false,['checked'=>(in_array('women',$sc))])}}Women

                            {{Form::checkbox('service_criteria[]','kids' ,false,['checked'=>(in_array('kids',$sc))])}}Kids

                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('amenity', __('messages.Amenities'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            @foreach($amenities as $key => $am)

                               {{Form::checkbox('amenity[]',$key ,false,['checked'=>(in_array($key,$usr_amen))])}}{{$am}}
                            @endforeach
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('category_id', __('messages.Category'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::select('category_id', $categories,null,['class' => 'form-control validate[required]']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('tier_id', __('messages.Tier'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::select('tier_id', $tiers,null,['class' => 'form-control validate[required]']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('store_name', __('messages.Store Name'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('store_name',null, ['class' => 'form-control validate[required]']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('store_number', __('messages.Store Number'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('store_number',null, ['class' => 'form-control validate[required]']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('trail_session', __('messages.Session Name'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::select('trail_session', $session_list,[$row->trail_session_id],['class' => 'form-control myselect','placeholder'=>'Select Session']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('tags', __('messages.Tag Name'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::select('tags[]', $tags_arr,$selected_tags,['class' => 'form-control validate[required] myselectTags','multiple'=>"multiple"]) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('description', __('messages.About Store'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::textarea('description',null, ['class' => 'form-control', 'rows' => 3, 'cols' => 40]) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('session_description', __('messages.Store Description'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::textarea('session_description',null, ['class' => 'form-control', 'rows' => 3, 'cols' => 40]) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('session_sub_description', __('messages.Store Sub Description'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::textarea('session_sub_description',null, ['class' => 'form-control', 'rows' => 3, 'cols' => 40]) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-4">{{__('messages.Store Image')}}</label>
                            <div class="col-md-3">
                            <div id="ImgView">
                                @if($row->store_image)
                                <img style="height: 150px; width: 186px;" src="{{changeImageUrlForFileExist(asset('sp_uploads/store/'.$row->store_image))}}">
                                @endif
                            </div>
                            <div id="upload-demo" style=" display: none;">
                            </div>
                            <input type="file" id="image" onclick="showHideDiv(this)" name="store_image">
                            </div>
                            <div class="col-md-3" style="margin-top: 10px;"></div>
                    
                    </div>
                    <div class="form-group">
                        {{Form::label('price', __('messages.Price'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('price',null, ['class' => 'form-control','id'=>'best_price','onkeypress'=>'return isNumber(event)']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('country_code', __('messages.Country Code'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('country_code',null, ['class' => 'form-control validate[required]','readonly']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('mobile_no', __('messages.Mobile No'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('mobile_no',null, ['class' => 'form-control validate[required]']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('whatsapp_no', __('messages.Whatsapp No'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('whatsapp_no',null, ['class' => 'form-control validate[required]']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('email', __('messages.Email'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('email',null, ['class' => 'form-control validate[required]']) }}
                        </div>
                    </div>

              <!--       <div class="form-group">
                        {{Form::label('address', 'Address', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('address',null, ['class' => 'form-control validate[required]']) }}
                        </div>
                    </div> -->

                    <div class="form-group">
                        {{Form::label('address', __('messages.Address'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-sm-4">
                             
                            {!! Form::text('address', null, ['maxlength'=>'255','placeholder' => 'Address', 'required'=>true, 'class'=>'form-control', 'id'=>'autocomplete']) !!} 
                            {!! Form::hidden('latitude', null, ['class'=>'form-control', 'id'=>'latitude']) !!}
                            {!! Form::hidden('longitude', null, ['class'=>'form-control', 'id'=>'longitude']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="country" class="control-label col-lg-4">{{__('messages.Country')}}</label>
                        <div class="col-lg-4">
                            <select class="form-control validate[required] myselect" name="country" id="country">
                                <option value="">Select Country</option>
                                @foreach($country_list as $country)
                                    <option value="{{ $country->iso2.'/'.$country->name }}" data-iso2code="{{$country->iso2}}" {{($country->iso2 == $user_countries['country_isocode']) ? 'selected' : '' }}>{{ $country->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    @php 
                        $showstate = ($user_countries[0]['state_isocode'] !=null) ? 'block' : 'none';
                        $showcity = ($user_countries[0]['city_isocode'] !=null) ? 'block' : 'none';
                    @endphp
                    <div class="form-group" id="statebox" style="display: {{$showstate}};">
                        <label for="state" class="control-label col-lg-4">{{__('messages.State')}}</label>
                        <div class="col-lg-4">
                            <select class="form-control validate[required] myselect" name="state" id="state">
                            </select>
                        </div>
                    </div>
                    <div class="form-group" id="citybox" style="display: {{$showstate}};">
                        <label for="city" class="control-label col-lg-4">{{__('messages.City')}}</label>
                        <div class="col-lg-4">
                            <select class="form-control validate[required] myselect" name="city" id="city">
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('sub_locality', __('messages.Sub Locality'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('sub_locality',null, ['class' => 'form-control validate[required]','maxlength'=>'1000']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('landmark', __('messages.Landmark'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('landmark',null, ['class' => 'form-control validate[required]','maxlength'=>'1000']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-4">{{__('messages.Profile Image')}}</label>
                            <div class="col-md-3">
                            <div id="ImgView">
                                @if($row->profile_image)
                                <img style="height: 150px; width: 186px;" src="{{changeImageUrlForFileExist(asset('sp_uploads/profile/'.$row->profile_image))}}">
                                @endif
                            </div>
                            <div id="upload-demo" style=" display: none;">
                            </div>
                            <input type="file" id="image" onclick="showHideDiv(this)" name="profile_image">
                            </div>
                            <div class="col-md-3" style="margin-top: 10px;"></div>
                    
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-4">{{__('messages.Agreement')}}</label>
                        <div class="col-lg-8">
                            <input style="display: inline-block;" type="file" name="agreement"/>

                            @if($row->agreement!='')
                            <a style="display: inline-block;" href="{{ changeImageUrlForFileExist(asset('sp_uploads/approve_pdf/'.$row->agreement)) }}" target="_blank"><i class="fa fa-download" aria-hidden="true"></i></a> 
                            @endif
                          <span style="display: block;">(Supports only PDF)</span>
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('modes_of_delivery', __('messages.Modes Of Delivery'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            <?php $sc = explode(',',$row->delivery_mode);?>

                            {{Form::checkbox('delivery_mode[]','online' ,false,['checked'=>(in_array('online',$sc))])}}Online

                            {{Form::checkbox('delivery_mode[]','store' ,false,['checked'=>(in_array('store',$sc))])}}In-Stores

                            {{Form::checkbox('delivery_mode[]','home' ,false,['checked'=>(in_array('home',$sc))])}}At Home
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('account_title', __('messages.Bank Account Title'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('account_title',null, ['class' => 'form-control ']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('bank', __('messages.BANK NAME & BRANCH NAME'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('bank',null, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('account_iban', __('messages.BANK ACCOUNT NUMBER'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('account_iban',null, ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('branch_name', __('messages.IBAN NUMBER'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('branch_name',null, ['class' => 'form-control ']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('branch_address', __('messages.Bank Address'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('branch_address',null, ['class' => 'form-control ']) }}
                        </div>
                    </div>
                     <!-- <div class="form-group ">
                        <label class="control-label col-lg-4">Banner Images</label>
                        <div class="col-lg-8">

                        <div class="row">
                            <div class="col-md-10">
                                <div class="row">
                                     <div class="col-md-6"><div class="input_fields_container">
                                       <input type="file" name="banner_path[]" accept= "image/*"/></div></div>
                                      <div class="col-md-6">
                                          <button class="btn btn-sm btn-primary add_more_button" style="margin-bottom:10px">Add More Images</button>
                                      </div>
                                </div>
                               
                            </div>
                        </div>

                        
                        <div id="more_image" style="display: none;">
                            <div class="row" style="margin-top: 10px;">
                            <div class="col-md-10">
                                <input type="file" name="banner_path[]"  accept= "image/*" />
                            </div>
                            <div  class="col-md-2">
                                <a href="#" class="remove_field" style="margin-bottom:10px;">Remove</a>
                            </div>
                        </div>
                        </div>
                        


                        @foreach($banner_images as $ban_img)
                        <div class="form-group disp-inline" >
                            <img src="{{asset('sp_uploads/banner_images/'.$ban_img['banner_path'])}}" width="100" height="100" class="banner_img<?= $ban_img->id; ?>"/>
                            <a onclick="deleteBannerImage('<?= $ban_img->id; ?>');" class="banner_img<?= $ban_img->id; ?>">Delete</a>
                        </div>
                        @endforeach
                        </div>
                    </div> -->
                    <div class="form-actions no-margin-bottom">
                        <input type="submit" value="Submit" class="btn btn-primary">
                    </div>
                    <?= Form::hidden('image_cr',null, ['id' => 'image_cr']) ?>
                    <br><br><br><br>
                </form>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
@endsection
@section('uniquescript')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/holder/2.4.1/holder.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/jquery.uniform.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>
    <script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCNVV0WGqduJq4EsX8_Y_s8L-hiZrHmrj4&libraries=places&callback=initialize"
        async defer></script>

<script type="text/javascript">
    function showHideDiv()
    {
        $('#ImgView').hide();
        $('#upload-demo').show();
    }
    $(function() {
       $(".myselectTags").select2({
        tags: true,
       });
    });
    </script>
    <script>

        function isNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        $(function() {
          Metis.formValidation();
        });
        $('#popup-validation').submit(function(e){
            e.preventDefault();
            obj = $(this);
            resize.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (img) {
                $('#image_cr').val(img);
               obj.unbind('submit').submit();
            });
        });

        // image crop
        var resize = $('#upload-demo').croppie({
            enableExif: true,
            enableOrientation: true,    
            viewport: { // Default { width: 100, height: 100, type: 'square' } 
                width: 200,
                height: 200,
                type: 'square' //square
            },
            boundary: {
                width: 300,
                height: 300
            }
        });

        $('#image').on('change', function () { 
          var reader = new FileReader();
            reader.onload = function (e) {
              resize.croppie('bind',{
                url: e.target.result
              }).then(function(){
                console.log('jQuery bind complete');
              });
            }
            reader.readAsDataURL(this.files[0]);
        });
        // 



        function deleteBannerImage(id)
        {

          swal({
              title: "Are you sure?",
              type: "warning",
              showCancelButton: "No",
              confirmButtonClass: "btn-danger",
              confirmButtonText: "Yes",
              cancelButtonText: "No",
            },
              function(){
                jQuery.ajax({
                url: '{{route('sp.banner-image-delete')}}',
                type: 'POST',
                data:{banner_id:id},
                headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    
                    if(response == 'success'){
                        $(".banner_img"+id).hide();
                    }
                }
                });
              }
          );
        }

        //google map 
        $("#autocomplete").change(function(){
          $("#latitude").val('');
          $("#longitude").val('');
          
        });

        $(document).ready(function() {
            var max_fields_limit = 10; //maximum input boxes allowed
            
         
            var x = 1; //initlal text box count
            $('.add_more_button').click(function(e){ //on add input button click
                e.preventDefault();
                $('.limit_error').remove();
                if(x < max_fields_limit){ //max input box allowed
                    x++; //text box increment
                    $('.input_fields_container').append($("#more_image").html()); //add input box
                }
                else
                {
                  $(this).after('<span class="limit_error" style="color:red;">You have reached the limit of images uploaded.</span>');
                }
            });
            
            $('.input_fields_container').on("click",".remove_field", function(e){ //user click on remove text
                e.preventDefault(); $(this).parent().parent('div').remove(); x--;
                  $('.limit_error').remove();
            })
        });

        function initialize() 
        {
            var input = document.getElementById('autocomplete');
            var options = {};            
           var autocomplete = new google.maps.places.Autocomplete(input, options);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                    var place = autocomplete.getPlace();
                    var lat = place.geometry.location.lat();
                    var lng = place.geometry.location.lng();
                    $("#latitude").val(lat);
                    $("#longitude").val(lng); 
                });
        }
                     
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>

    <script>
        $(document).ready(function(){
            var countryID = "{{ $user_countries['country_isocode'] }}";
            var stateID = "{{ $user_countries['state_isocode'] }}";
            var cityID = "{{ $user_countries['city_isocode'] }}";
            if(stateID !="" || countryID !='')
            {
                getcountryStateCity(countryID,stateID);
            }
            if(cityID !=0 || stateID !="")
            {
                getStateCity(countryID,stateID,cityID);
            }
            $('#country').on('change', function(){
                var countryID = $(this).find(':selected').attr('data-iso2code');
                getcountryStateCity(countryID,'');
                $("#citybox").css("display",'none');
            });
            
            $('#state').on('change', function(){
                var countryID = $("#country").find(':selected').attr('data-iso2code');
                var stateID = $(this).find(':selected').attr('data-iso2code');
                getStateCity(countryID,stateID,'');
            });

            function getcountryStateCity(countryID,stateID)
            {
                if(countryID){
                    $.ajax({
                        url:"{{route('sp.get-state-list')}}",
                        type: "POST",
                        data: {
                        country_id: countryID,
                        _token: '{{csrf_token()}}' 
                        },
                        dataType : 'json',
                        success:function(html){
                            var inputtype = html.inputtype;
                            if(html.inputtype === 'state' || html.inputtype === 'city')
                            {
                                $("#"+inputtype+"box").show();
                                $("#"+inputtype).empty();
                                $("#"+inputtype).append('<option value="">Select</option>');
                                $.each(html.data,function(key,value){
                                    var isocode = (html.inputtype == 'city') ? value.id : value.iso2;
                                    var optionval = (html.inputtype == 'city') ? value.id+'/'+value.name : value.iso2+'/'+value.name;
                                    var selectedval = (value.iso2 == stateID) ? "selected" : '';
                                    $("#"+inputtype).append('<option value="'+optionval+'" data-iso2code="'+isocode+'" '+selectedval+'>'+value.name+'</option>');
                                });
                            }
                            else
                            {
                                $("#statebox").hide();
                                $("#citybox").css("display",'none');
                            }
                        }
                    }); 
                }else{
                    $('#state').html('<option value="">Select country first</option>');
                    $('#city').html('<option value="">Select state first</option>'); 
                }
            }

            function getStateCity(countryID,stateID,cityID)
            {
                if(stateID){
                    $.ajax({
                        url:"{{route('sp.get-city-list')}}",
                        type: "POST",
                        data: {
                        country_id: countryID,
                        state_id: stateID,
                        _token: '{{csrf_token()}}' 
                        },
                        dataType : 'json',
                        success:function(html){
                            if(html.data.length != 0)
                            {
                                $("#citybox").show();
                                $("#city").empty();
                                $("#city").append('<option value="">Select</option>');
                                $.each(html.data,function(key,value){
                                    var selectedval = (value.id == cityID) ? "selected" : '';
                                    $("#city").append('<option value="'+value.id+'/'+value.name+'" data-iso2code="'+value.id+'" '+selectedval+'>'+value.name+'</option>');
                                });
                            }
                            else
                            {
                                $("#citybox").hide();
                            }
                        }
                    }); 
                }else{
                    $('#city').html('<option value="">Select state first</option>'); 
                }
            }
        });
    </script>
@endsection