@extends('layouts.admin.admin')
@section('uniquecss')

  <style>
    #percent-sign {
        top: 8px;
        left: 126px;
        color: #555;
        position: absolute;
        z-index: 1;
    }
  </style>
   <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">

@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>View Profile</h5>

            </header>
            <div id="collapse2" class="body">
                <div class="row">
                     @include('message')
                    <div class="col-sm-12 col-lg-12">
                        <div class="col-lg-12 border_details">
                            <div class="col-lg-8">
                                @if($row->profile_image!='' && $row->profile_image!=null)
                                <img src="<?= changeImageUrlForFileExist(asset('sp_uploads/profile/'.$row->profile_image)); ?>" width="150" height="150"/>
                                @else
                                <img src="<?= asset('images/defualt.jpeg'); ?>" width="150" height="150"/>
                                @endif
                                 <br/><br/>
                            </div> 

                            <div class="clearfix"></div>
                        </div>

                        <div class="col-lg-12 border_details">
                            <div class="col-lg-4"><b>Name</b></div>
                            <div class="col-lg-8"><?= $row->name; ?></div> <div class="clearfix"></div>
                        </div>
                        <div  class="col-lg-12 border_details">
                            <div class="col-lg-4"><b>Email</b></div>
                            <div class="col-lg-8"><?= $row->email; ?></div> <div class="clearfix"></div>
                        </div>
                        <div  class="col-lg-12 border_details">
                            <div class="col-lg-4"><b>Mobile No</b></div>
                            <div class="col-lg-8"><?= $row->mobile_no; ?></div> <div class="clearfix"></div>
                        </div>

                        <div  class="col-lg-12 border_details">
                            <div class="col-lg-4"><b>Watsapp No</b></div>
                            <div class="col-lg-8"><?= $row->whatsapp_no; ?></div> <div class="clearfix"></div>
                        </div>

                        <div  class="col-lg-12 border_details">
                            <div class="col-lg-4"><b>Store Name</b></div>
                            <div class="col-lg-8"><?= $row->store_name; ?></div> <div class="clearfix"></div>
                        </div>
                        <div  class="col-lg-12 border_details">
                            <div class="col-lg-4"><b>Session Name</b></div>
                            <div class="col-lg-8"><?= $row->name; ?></div> <div class="clearfix"></div>
                        </div>

                        <div  class="col-lg-12 border_details">
                            <div class="col-lg-4"><b>Store Type</b></div>
                            <div class="col-lg-8">
                                @if($row->is_individual!=1)
                                {{"Store"}}
                                @else
                                {{"Individual"}}
                                @endif
                                </div> <div class="clearfix"></div>
                        </div>

                        <div  class="col-lg-12 border_details">
                            <div class="col-lg-4"><b>Store Number</b></div>
                            <div class="col-lg-8"><?= $row->store_number; ?></div> <div class="clearfix"></div>
                        </div>

                        <div  class="col-lg-12 border_details">
                            <div class="col-lg-4"><b>Address</b></div>
                            <div class="col-lg-8"><?= $row->address; ?></div> <div class="clearfix"></div>
                        </div>

                        <div  class="col-lg-12 border_details">
                            <div class="col-lg-4"><b>Country</b></div>
                            <div class="col-lg-8"><?= $row->country_name; ?></div> <div class="clearfix"></div>
                        </div>

                        <div  class="col-lg-12 border_details">
                            <div class="col-lg-4"><b>State</b></div>
                            <div class="col-lg-8"><?= $row->state_name; ?></div> <div class="clearfix"></div>
                        </div>

                        <div  class="col-lg-12 border_details">
                            <div class="col-lg-4"><b>City</b></div>
                            <div class="col-lg-8"><?= $row->city_name; ?></div> <div class="clearfix"></div>
                        </div>

                        <div  class="col-lg-12 border_details">
                            <div class="col-lg-4"><b>Locality</b></div>
                            <div class="col-lg-8"><?= $row->sub_locality; ?></div> <div class="clearfix"></div>
                        </div>

                        <div  class="col-lg-12 border_details">
                            <div class="col-lg-4"><b>Landmark</b></div>
                            <div class="col-lg-8"><?= $row->landmark; ?></div> <div class="clearfix"></div>
                            <br/>
                        </div>
                        {!! Form::open(['route' => 'sp.user.approve', 'method' => 'POST', 'id' => 'popup-validation', 'enctype' => 'multipart/form-data']) !!}
                        {{ csrf_field() }}

                       
                        <input type="hidden" name="slug" value="<?= $row->slug; ?>"/>

                        <div  class="col-lg-12 border_details">
                            <div class="form-group">
                                <label class="control-label col-lg-4">Tier</label>
                                <div class="col-lg-4">
                                    @if($row->is_approved!=1)

                                    <select name="tier" class="form-control" required="true">
                                        <option value="">Select Tier</option>
                                        @foreach($tiers as $key => $val)
                                        <?php if($key == $row->tier_id) {
                                            $selected = 'selected = "selected"';
                                        }else{
                                           $selected = ''; 
                                        }
                                        ?>

                                        <option value="{{$key}}" {{$selected}}>{{$val}}</option>
                                        @endforeach
                                    </select>

                                    @else

                                        @if(array_key_exists($row->tier_id,$tiers))
                                            {{$tiers[$row->tier_id]}}
                                        @endif
                                    @endif  
                                </div>
                            </div>                            
                            <br/><br/>
                        </div>

                        <div  class="col-lg-12 border_details">
                            <div class="form-group">
                                <label class="control-label col-lg-4">Agreement</label>
                                <div class="col-lg-8">
                                    @if($row->is_approved!=1)
                                    <input type="file" name="agreement"/><span>(Supports only PDF)</span>
                                    @else
                                    <a  href="{{ asset('sp_uploads/approve_pdf/'.$row->agreement) }}" target="_blank">View PDF</a>
                                    @endif
                                </div>
                            </div>                            
                            <br/><br/>
                        </div>

                        <div  class="col-lg-12 border_details">
                            <div class="col-lg-4"></div>
                            <div class="col-lg-8">
                                @if($row->is_approved!=1)
                                <button class="btn btn-success">Approve</button>
                                
                           {{ form::close() }}
                                <a class="btn btn-danger" onclick="changeStatus('<?php echo $row->slug;?>')">Decline</a>
                                @endif
                            </div> 
                            <div class="clearfix"></div>
                        </div>
                        
                         <div  class="col-lg-12 border_details">
                            <div class="col-lg-4"><b>Bank Account Title</b></div>
                            <div class="col-lg-8"><?= $row->account_title; ?></div> <div class="clearfix"></div>
                        </div>
                         <div  class="col-lg-12 border_details">
                            <div class="col-lg-4"><b>BANK NAME & BRANCH NAME</b></div>
                            <div class="col-lg-8"><?= $row->bank; ?></div> <div class="clearfix"></div>
                        </div>
                         <div  class="col-lg-12 border_details">
                            <div class="col-lg-4"><b>BANK ACCOUNT NUMBER</b></div>
                            <div class="col-lg-8"><?= $row->account_iban; ?></div> <div class="clearfix"></div>
                        </div>
                         <div  class="col-lg-12 border_details">
                            <div class="col-lg-4"><b>IBAN NUMBER</b></div>
                            <div class="col-lg-8"><?= $row->branch_name; ?></div> <div class="clearfix"></div>
                        </div>
                         <div  class="col-lg-12 border_details">
                            <div class="col-lg-4"><b>Bank Address</b></div>
                            <div class="col-lg-8"><?= $row->branch_address; ?></div> <div class="clearfix"></div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>

@endsection
@section('uniquescript')
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>
<script>
    function changeStatus(slug)
    {
      swal({
          title: "Decline!",
          text: "Write Reason of Decline",
          type: "input",
          showCancelButton: true,
          closeOnConfirm: false,
          animation: "slide-from-top",
          inputPlaceholder: "Enter Reason"
        },
        function(inputValue){
          if (inputValue === false) return false;

          if (inputValue === "") {
            swal.showInputError("You need to write something!");
            return false
          }
          jQuery.ajax({
            url: '{{route('sp.user.decline')}}',
            type: 'POST',
            data:{slug:slug,description:inputValue},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
      
              swal({
                    title: response,
                }, function() {
                    window.location = '{!! route('sp.index') !!}';
                });
            }
            });
          //swal("Nice!", "You wrote: " + inputValue, "success");
        });
    }
</script>
@endsection