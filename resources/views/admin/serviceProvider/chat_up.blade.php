@extends('layouts.admin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
 
@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>Beutics Chat</h5>
            </header>
            <div id="collapse4" class="body">
              @include('message')

              <div class="messaging">
                <div class="inbox_msg">
                  <div style="width: 100%">
                    <div class="headind_srch" style="background: #f8f8f8 none repeat scroll 0 0;">
                      <div class="recent_heading">
                        <h4>{{$title}}</h4>
                      </div>
                  
                    </div>

                    <div class="mesgs" id="other-data" other-id="{{$other['id']}}" other-name="{{@$other['full_name']}}" other-image="{{@$other['image']}}" style="width: 100%">
                        <div class="msg_history" id="messages">
                          @foreach($chats as $chatrow)

                            @if($chatrow['other_user_id']=='1' && $chatrow['other_user_id_tbl']=='admins')
                            <div class="incoming_msg">
                                <div class="incoming_msg_img"> 
                                  <img src="{{@$other['image']}}"  style="border-radius: 50%;height: 50px;" alt="sunil"> 
                                </div>
                                <div class="received_msg">
                                  <div class="received_withd_msg" >
                                    <p>{{$chatrow['message']}}</p>
                                    <span class="time_date"> {{date('h:i A', strtotime($chatrow['user_time']))}}   |    {{date('M d Y', strtotime($chatrow['user_time']))}}</span>
                                  </div>
                                </div>
                            </div>
                            @else
                            <div class="outgoing_msg">
                              <div class="sent_msg" >
                                <p>{{$chatrow['message']}}</p>
                                <span class="time_date"> {{date('h:i A', strtotime($chatrow['user_time']))}}    |    {{date('M d Y', strtotime($chatrow['user_time']))}}</span>
                              </div>
                            </div>

                            @endif

                          @endforeach

                        </div>
                        <div class="type_msg">
                          <form id="chatForm" enctype="multipart/form-data" action="">
                            <input type="hidden" name="type" value="TEXT">
                            <div class="input_msg_write">
                              <input id="m" class="emojiable-option write_msg" type="text" placeholder="Type a message" />
                              <button class="msg_send_btn" onclick="$('form#chatForm').submit();" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                            </div>
                          </form>
                        </div>

                    </div>
                  </div>
                  
                  
                </div>
              </div>

            </div>
        </div>
    </div>
</div>

@endsection

@section('uniquescript')

<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>

<script src="{{url('assets/js/moment.min.js')}}"></script>
<!-- <script src="{{url('assets/js/socket.io-1.2.0.js')}}"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script>
<!-- <script src="/socket.io/socket.io.js"></script> -->
<script>

    var host_url = "http://198.74.62.156:4219";

    var userId    = 1; 
    var userName  = "Admin"; 
    var userImage = "";
    var otherId   = "<?php echo $other['id']; ?>"; 
    var otherName = "<?php echo $other['full_name']; ?>";
    var otherImage= "<?php echo $other['image']; ?>"

    var booking_id =0;

    var groupId   = (userId>otherId)?userId+""+otherId+""+booking_id:otherId+""+userId+""+booking_id;
            
            
    //alert(userId);
    var socket = io.connect(host_url,{query:"user_id=A1&user_id_tbl=admins&browser_id={{ csrf_token() }}"});

    socket.on('connect', function () {
        console.log('connected');
        socket.on('multi_user', function(data){
          console.log('multi_user');
          console.log(data);
          var groupIdChat = (data.data.user_id>data.data.other_user_id)?data.data.user_id+""+data.data.other_user_id+""+data.data.booking_id:data.data.other_user_id+""+data.data.user_id+""+data.data.booking_id;
          var dateTime = new Date(data.data.user_time);
            if(groupId==groupIdChat){
              
              if(userId == data.data.user_id && data.data.user_id_tbl=="admins"){
                $('#messages').append('<div class="outgoing_msg"><div class="sent_msg" ><p>'+data.data.message+'</p><span class="time_date">'+moment.tz(dateTime,"Asia/Kolkata").local().format('MMM DD, YYYY, h:mm A')+'</span>') ;
              }else{
                $('#messages').append('<div class="incoming_msg"><div class="incoming_msg_img"><img style="border-radius: 50%;height: 50px;" src="'+otherImage+'"></div><div class="received_msg"><div class="received_withd_msg" ><p>'+data.data.message+'</p><span class="time_date"> '+moment.tz(dateTime,"Asia/Kolkata").local().format('MMM DD, YYYY, h:mm A')+'</span></div></div></div>');

              }
          }
          $("#messages").animate({ scrollTop: $("#messages").prop('scrollHeight') - $("#messages").position().top }, "slow");
        });


        socket.on('receive_message', function(data){
          console.log('receive_message');
          console.log(data);
          var dateTime = new Date(data.data.user_time);
          var groupIdChat = (data.data.user_id>data.data.other_user_id)?data.data.user_id+""+data.data.other_user_id+""+data.data.booking_id:data.data.other_user_id+""+data.data.user_id+""+data.data.booking_id;

          console.log('groupIdChat',groupId);
          console.log('groupIdChat',groupIdChat);

          if(groupId==groupIdChat){
            
            if(userId == data.data.user_id && data.data.user_id_tbl=="admins"){
                $('#messages').append('<div class="outgoing_msg"><div class="sent_msg" ><p>'+data.data.message+'</p><span class="time_date">'+moment.tz(dateTime,"Asia/Kolkata").local().format('MMM DD, YYYY, h:mm A')+'</span>') ;
              }else{
                $('#messages').append('<div class="incoming_msg"><div class="incoming_msg_img"><img src="'+otherImage+'" style="border-radius: 50%;height: 50px;"></div><div class="received_msg"><div class="received_withd_msg" ><p>'+data.data.message+'</p><span class="time_date"> '+moment.tz(dateTime,"Asia/Kolkata").local().format('MMM DD, YYYY, h:mm A')+'</span></div></div></div>');

              }
          }
          $("#messages").animate({ scrollTop: $("#messages").prop('scrollHeight') - $("#messages").position().top }, "slow");
        });





        socket.on('disconnect', function () {
            console.log('disconnected');
        });

    });

    $(function () {
          $('#chatForm').submit(function(event){
           

                if($.trim($('#m').val())==""){
                  $('#m').val('');
                  $('#m').focus();
                  return false;
                }
                var log_obj =
                        {
                            "user_id"       : userId,
                            "other_user_id" : otherId,
                            "upload_img"    : "",
                            "message"       : $('#m').val(),
                            "message_type"  : "Text",
                            "other_data"    : "",
                            "user_id_tbl" :"admins",
                            "other_user_id_tbl" :"sp_users",
                            "booking_id":booking_id
                        };

                console.log(log_obj);

                socket.emit('send_message', log_obj);
                $('#m').val('');

                event.preventDefault();

      });
              

});

    
</script>
@endsection