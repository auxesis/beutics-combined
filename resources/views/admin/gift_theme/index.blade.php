@extends('layouts.admin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
 
@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>Gift Card Themes</h5>
                <a class="btn btn-primary pull-right" href="{{route('gift-theme.create')}}" style="margin-top:4px;">Create Theme</a>
            </header>
            <div id="collapse4" class="body">
              @include('message')
              
               <table id="user_datatable" class="table table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th></th>
                        <th>ID</th>
                        <th>Occasion Name</th>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
                  
            </div>
        </div>
    </div>
</div>
@endsection

@section('uniquescript')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>
<script type="text/javascript">

  var selected_gifts = 
  $(document).on('click', '.pdr_checkbox', function(){
    console.log($(this).val())
  });
  function deleteRow(obj,asso)
  {

    event.preventDefault(); // prevent form submit
    var msg = "Are you sure?";
    if(asso > 0){
        var msg = "You cannot delete this theme because this is associated with Ecards.";
                           
   
    }
    if(asso > 0){ 
      swal({
          title: msg,
          type: "warning",
          showCancelButton: true,
          showConfirmButton: false,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          cancelButtonText: "Ok",
          closeOnConfirm: false,
          closeOnCancel: false,

        },
        function(isConfirm){
          if (isConfirm) {        
           obj.submit();

          } else {
             swal.close();
          }
        });
    }
    else{
      swal({
          title: msg,
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          cancelButtonText: "No",
          closeOnConfirm: false,
          closeOnCancel: false,

        },
        function(isConfirm){
          if (isConfirm) {        
           obj.submit();

          } else {
             swal.close();
          }
        });
      }
    }
    
    function makePublish(id)
    {
      swal({
          title: "Are you sure ?",
          type: "warning",
          showCancelButton: "No",
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Yes",
          cancelButtonText: "No",
        },
          function(){
            jQuery.ajax({
            url: '{{route('gifttheme.publish.update')}}',
            type: 'POST',
            data:{ecard_id:id},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
              $("#status_"+id).html(response);
            }
            });
          }
      );
    }

    $(function() {
        var table = $('#user_datatable').DataTable({
          stateSave: true,
        processing: true,
        serverSide: true,
        order: [[0, "desc" ]],
        "ajax":{
        "url": '{!! route('gifttheme.getdata') !!}',
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{csrf_token()}}"}
        },
        columns: [
        { data: "checkbox", name:'checkbox', orderable:true, searchable:false},
        { data: 'id', name: 'id', orderable:true },
        { data: 'name', name: 'name', orderable:true },
        { data: 'image', name: 'image', orderable:false },
        { data: 'action', name: 'action', orderable:true  },
        ],
        "columnDefs": [
        { "searchable": false, "targets": 0 }
        ]
        , language: {
        searchPlaceholder: "Search by occasion name"
        },
});
});  


</script>
@endsection

