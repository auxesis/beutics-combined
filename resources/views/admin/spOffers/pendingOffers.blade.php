@extends('layouts.admin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
 
@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>Service Provider Pending Offers</h5>
            </header>
            <div id="collapse4" class="body">
              @include('message')
              
               <table id="user_datatable" class="table table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th width="15%">ID </th>
                        <th>Offer Name </th>
                        <th>Store Name </th>
                        <th>Spectacular Offer </th>
                        <th>Discount </th>      
                        <th>Created At</th>
                        <th>Expiry Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
                  
            </div>
        </div>
    </div>
</div>
@endsection

@section('uniquescript')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>
<script type="text/javascript">
  function callNative(){

    $("#user_datatable").DataTable().draw();
  }
  function loadStatusFilter()
  {

    jQuery("#user_datatable_filter").parent().parent().
      append('<div id="user_datatable_status_filter" class="col-sm-12 col-md-6"><label>Spectacular Offer:</label><select name="columns[testtest]" class="form-control input-sm" id="custom-filter" onchange="callNative()"><option value="">All</option><option value="Yes">Yes</option><option value="No">No</option></select></div><div id="user_datatable_category_filter" class="col-sm-12 col-md-2"><label>Categories:</label><select name="columns[testtest]" class="form-control input-sm" id="category-custom-filter" onchange="callNative()"><option value="">All Categories</option><?php foreach($p_categories as $key => $cat){ ?><option value="<?= $key?>"><?= $cat?></option><?php } ?></select></div><div id="user_datatable_discount_filter" class="col-sm-12 col-md-6"><label>Discount Filter:</label><select name="columns[testtest]" class="form-control input-sm" id="discount-custom-filter" onchange="callNative()"><option value="">Recently Added</option><option value="high">Hign To Low</option><option value="low">Low To High</option></select></div>');

    jQuery('#user_datatable_length').parent().closest("div.col-sm-12").removeClass("col-md-12");  
    jQuery('#user_datatable_length').parent().closest("div.col-sm-12").addClass("col-md-12");  
    jQuery('#user_datatable_filter').parent().closest("div.col-sm-12").removeClass("col-md-12");  
    jQuery('#user_datatable_filter').parent().closest("div.col-sm-12").addClass("col-md-12");
    jQuery('#user_datatable_status_filter').closest("div.col-sm-12").removeClass("col-md-2");  
    jQuery('#user_datatable_status_filter').closest("div.col-sm-12").addClass("col-md-2");

    jQuery('#user_datatable_category_filter').closest("div.col-sm-12").removeClass("col-md-2");  
    jQuery('#user_datatable_category_filter').closest("div.col-sm-12").addClass("col-md-2");

    jQuery('#user_datatable_discount_filter').closest("div.col-sm-12").removeClass("col-md-2");  
    jQuery('#user_datatable_discount_filter').closest("div.col-sm-12").addClass("col-md-2");
  }
  </script>
<script type="text/javascript"> 
 
    $(function() {
        var table = $('#user_datatable').DataTable({
            stateSave: true,
        processing: true,
        serverSide: true,
        order: [[0, "desc" ]],
        "ajax":{
        "url": '{!! route('sp.offer.getdata') !!}',
                "dataType": "json",
                "type": "POST",
                data: function(d) {
                    d._token= "{{csrf_token()}}";
                    d.status_filter = $('#custom-filter').val();
                    d.status='pending';
                    d.category_filter = $('#category-custom-filter').val();
                    d.discount_filter = $('#discount-custom-filter').val();
                }
               
        },
        columns: [
        { data: 'id', name: 'id', orderable:true },
        { data: 'offer_id', name: 'offer_id', orderable:true },
        { data: 'user_id', name: 'user_id', orderable:true },
        { data: 'spectacular_offer', name: 'spectacular_offer', orderable:true },
        { data: 'discount', name: 'discount', orderable:true },
        { data: 'created_at', name: 'created_at', orderable:true  },
        { data: 'expiry_date', name: 'expiry_date', orderable:true  },
        { data: 'action', name: 'action', orderable:true  },
        ],
        "columnDefs": [
        { "searchable": false, "targets": 0 }
        ]
        , language: {
        searchPlaceholder: "Search by name"
        },initComplete:function()
        {
          loadStatusFilter();
        }
});
});  


</script>
@endsection

