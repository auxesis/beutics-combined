@extends('layouts.admin.admin')

@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/jquery.gritter/css/jquery.gritter.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/themes/default/css/uniform.default.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/datepicker/jquery-ui.css">


<style type="text/css">
  
.form-horizontal .radio, .form-horizontal .checkbox {
    min-height: auto;
    display: inline-block;
    margin-right: 5px;
    margin-left: 10px;
}


table.owntable.table.table-borered {
    width: 456px;
    margin: 0 auto;
    position: relative;
    left: 6%;
}


table.owntable th {
    background-color: #337ab7;
    color: #fff;
}



</style>

@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>{{$title}}</h5>
          

            </header>
             @include('message')
            <div id="collapse2" class="body">
                <!-- <form class="form-horizontal" id="popup-validation"> -->
                {!! Form::model($rows,['route' => ['sp.offer.update.nominate',$rows->id], 'method' => 'POST', 'class' => 'form-horizontal','id' => 'popup-validation','enctype' => 'multipart/form-data']) !!}
                {{ csrf_field() }}
                  <input type="hidden" value="{{$status_name}}" name="status_name">
                    <div class="form-group">
                        {{Form::label('', 'Service Provider Name', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                      
                            {{ Form::text('user_id',null, ['class' => 'form-control validate[required]','readonly']) }}
                        </div>
                    </div>

                      <div class="form-group">
                        {{Form::label('offer_id', 'Offer Name', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                          <?php 
                          $offer_arr = array();
                            foreach($offer_name_rows as $row){
                                $offer_arr[$row['id']] = $row['title'];
                            }
                          ?>
                            {{ Form::select('offer_id', $offer_arr,null,['class' => 'form-control validate[required]','placeholder'=>'Combo Choose Any']) }}
                        </div>
                    </div>


                    <div class="form-group">
                      {{Form::label('', '', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{Form::checkbox('spectacular_offer', '1',null,['class' => 'checkbox'])}} SpectaCular Offer

                        </div>
                    </div>                          
            
                    
                    <div class="form-group">
                        {{Form::label('offer_details', 'Offer Details', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::textarea('offer_details',null, ['class' => 'form-control validate[required] summary-ckeditor']) }}
                        </div>
                    </div>


                    <div class="form-group">
                      {{Form::label('', 'Gender', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                          <?php $service_criteria = explode(',',$rows->getAssociatedOfferProviderName->service_criteria);
                          ?>
                          @foreach($service_criteria as $sc)
                            <?php $checked = ($sc == $rows->gender) ? true: false; ?>
                            {{Form::radio('gender', $sc,['class' => 'checkbox', 'checked'=>$checked])}} {{ucwords($sc)}}

                          @endforeach   
                        </div>
                    </div>


                    <div class="form-group">
                      {{Form::label('', 'Service Type', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">

                          {{Form::radio('service_type', 
                          'home',['class' => 'checkbox','checked'=> ('home' == $rows->service_type) ? true: false])}}Home

                          {{Form::radio('service_type', 
                          'store',['class' => 'checkbox','checked'=> ('store' == $rows->service_type) ? true: false])}}Store

                        </div>
                    </div>

                      <div class="new_section">
                        <table class="owntable table table-borered">
                          <thead>
                            <tr>

                              <th scope="col">Service Name</th>
                              <th scope="col">qty</th>
                              <th scope="col">price</th>
                            </tr>
                          </thead>
                          <tbody>

                            <?php 
                             $key_arr = [
                              'men' => [
                                'store' => ['MEN_ORIGINALPRICE','MEN_DISCOUNT','MEN_BESTPRICE'],
                                'home' => ['MEN_HOME_ORIGINALPRICE','MEN_HOME_DISCOUNT','MEN_HOME_BESTPRICE'],
                              ],
                              'women' => [
                                'store' => ['WOMEN_ORIGINALPRICE','WOMEN_DISCOUNT','WOMEN_BESTPRICE'],
                                'home' => ['WOMEN_HOME_ORIGINALPRICE','WOMEN_HOME_DISCOUNT','WOMEN_HOME_BESTPRICE'],
                              ],
                              'kids' => [
                                'store' => ['KIDS_ORIGINALPRICE','KIDS_DISCOUNT','KIDS_BESTPRICE'],
                                'home' => ['KIDS_HOME_ORIGINALPRICE','KIDS_HOME_DISCOUNT','KIDS_HOME_BESTPRICE'],
                              ],
                              
                            ];
                            $total = 0;
                          ?>
                            @foreach($rows->getAssociatedOfferServices as $srow)
                            <tr>
                              <td>{{$srow->getAssociatedOfferServicesName->getAssociatedService->name}}</td>
                              <td>{{$srow->quantity}}</td>

                              @foreach($srow->getAssociatedOfferServicesPrices as $pricerow)
                              <?php 
                                if(in_array($pricerow['field_key'],$key_arr[$rows->gender][$rows->service_type] ))
                                {
                                  $key_name = explode('_',$pricerow['field_key']);
                              
                                  $service_row_current['sp_service_id'] = $rows['id'];
                                  $service_row_current[$key_name[count($key_name) - 1]] = $pricerow['price'];
                                    
                                }
                              ?>
                              @endforeach
                              <td><?php 
                                $total = $total+$service_row_current['ORIGINALPRICE']*$srow->quantity;
                                echo $service_row_current['ORIGINALPRICE']; 
                                ?>
                                  
                              </td>

                            </tr>
                             @endforeach
                           <tr>
                              <td>Total </td>
                              <td> </td>
                              <td colspan="2" id="totalPrice"><?php echo $total;?></td>
                            </tr>
                           
                           
                          </tbody>
                        </table>   
                      </div>
                      <br/>
                    <div class="form-group">
                        {{Form::label('discount', 'Discount(%)', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('discount',round($rows->discount,2), ['class' => 'form-control validate[required]','id'=>'discount','onkeypress'=>'return isNumber(event)','onkeyup'=>'calcBestPriceValues()']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('best_price', 'Best Price', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('best_price',$total - ($total*$rows->discount)/100, ['class' => 'form-control validate[required]','id'=>'best_price','onkeypress'=>'return isNumber(event)','onkeyup'=>'calcDisValues()']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('expiry_date', 'Expiry Date', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('expiry_date',null, ['class' => 'form-control validate[required]','id'=>'datepicker']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('offer_terms', 'Offer Terms', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::textarea('offer_terms',null, ['class' => 'form-control validate[required] summary-ckeditor']) }}
                        </div>
                    </div>
                    @if($rows->status!=1)
                      <div class="form-group">
                        {{Form::label('', 'Service Type', ['class' => 'control-label col-lg-4'])}}
                          <div class="col-lg-4">

                            {{Form::radio('status', 
                            'approve',['class' => 'checkbox'])}}Approve

                            {{Form::radio('status', 
                            'decline',['class' => 'checkbox'])}}Decline

                          </div>
                      </div>
                      @else

                      <div class="form-group">
                        {{Form::label('spectacular_offer_type_id', 'Spectacular Offer Type', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                          <?php 
                          $offer_arr = array();
                            foreach($spect_offer_name_rows as $row){
                                $offer_arr[$row['id']] = $row['title'];
                            }
                          ?>
                            {{ Form::select('spectacular_offer_type_id', $offer_arr,null,['class' => 'form-control validate[required]','placeholder'=>'Choose Any']) }}
                        </div>
                    </div>

                      <div class="form-group">
                        <label class="control-label col-lg-4">Thumbnail Image<br/><i style="color:red;">Minimum Size <br/>(300 X 350)</i></label>
                        <div class="col-lg-3">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                     @if($rows->nominate_thumb_image!='')
                                      <img src="{{asset('sp_uploads/nominated_offers/thumb/'.$rows->nominate_thumb_image)}}" alt="..."/>
                                      @endif
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                    <div>
                                      <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="nominate_thumb_image"></span>
                                      <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                    </div>
                                </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-4">Banner Image<br/><i style="color:red;">Minimum Size <br/>(1242 X 630)</i></label>
                        <div class="col-lg-3">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                    @if($rows->nominate_banner_image!='')
                                      <img src="{{asset('sp_uploads/nominated_offers/'.$rows->nominate_banner_image)}}" alt="..."/>
                                      @endif
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                    <div>
                                      <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="nominate_banner_image"></span>
                                      <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                    </div>
                                </div>
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('remarks', 'Remarks', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::textarea('remarks',null, ['class' => 'form-control']) }}
                        </div>
                    </div>

                    @endif

                    
                    <div class="form-actions no-margin-bottom">
                        <input type="submit" value="Submit" class="btn btn-primary">
                   
                    </div>
                    <br><br><br><br>
                </form>
            </div>
        </div>  
    </div>
    <!-- /.col-lg-12 -->
</div>
@endsection
@section('uniquescript')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>

     <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/holder/2.4.1/holder.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/jquery.uniform.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>
    <script src="{{ url('/') }}/assets/lib/datepicker/jquery-ui.js"></script>
    <script src="{{ url('/') }}/assets/lib/ckeditor/ckeditor.js"></script>
    <script>
      CKEDITOR.replace( 'offer_terms' );
      CKEDITOR.replace( 'offer_details' );
    </script>
    <script>
        $(function() {
          Metis.formValidation();
        });

         $( function() {
          $( "#datepicker" ).datepicker({ 
            dateFormat: 'yy-mm-dd',
            changeYear: true,
            changeMonth: true,
            minDate: 0,
            });
        } );


    </script>
    <script>
       function isNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        function calcBestPriceValues()
        {
          var dis = $("#discount").val();
          if(dis>0){
            var total = $("#totalPrice").html();
            var bestprice = total - (total*dis)/100;
              $("#best_price").val((bestprice).toFixed(3));
          }
        }

        function calcDisValues()
        {
          var best_price = $("#best_price").val();
          if(best_price>0){
            var total = $("#totalPrice").html();
            var bestprice_1 = total - best_price;
            bestprice_2 =(bestprice_1*100)/total;
              $("#discount").val((bestprice_2).toFixed(3));
          }
        }

    </script>


@endsection