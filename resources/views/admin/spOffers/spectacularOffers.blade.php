@extends('layouts.admin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/datepicker/jquery-ui.css">
@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>Service Provider Spectacular Offers</h5>
            </header>
            <div id="collapse4" class="body">
              @include('message')
              
               <table id="user_datatable" class="table table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th width="5%">ID </th>
                        <th width="15%">Offer Name </th>
                        <th width="20%">Store Name</th>
                        <th width="15%">Spectacular Offer Type </th>
                        <th width="5%">Discount </th>                        
                        <th width="5%">Gender </th>                        
                        <th width="5%">Service Type </th>                        
                        <th width="10%">Expiry Date</th>
                        <th width="10%">Featured Offer</th>
                        <th width="10%">Status</th>
                        <th width="20%">Created At</th>
                        <th width="15%">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
                  
            </div>
        </div>
    </div>
</div>
@endsection

@section('uniquescript')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>
<script src="{{ url('/') }}/assets/lib/datepicker/jquery-ui.js"></script>
<script type="text/javascript">
  function callNative(){

    $("#user_datatable").DataTable().draw();
  }
  function loadStatusFilter()
  {

    jQuery("#user_datatable_filter").parent().parent().
      append('<div id="user_datatable_expiry_filter" class="col-sm-12 col-md-2"><label>Expiry Date Filter:</label><input type="text" class="form-control" id="expiry-custom-filter" onchange="callNative()" autocomplete="off"></div><div id="user_datatable_category_filter" class="col-sm-12 col-md-2"><label>Categories:</label><select name="columns[testtest]" class="form-control input-sm" id="custom-filter" onchange="callNative()"><option value="">All Categories</option><?php foreach($p_categories as $key => $cat){ ?><option value="<?= $key?>"><?= $cat?></option><?php } ?></select></div><div id="user_datatable_discount_filter" class="col-sm-12 col-md-6"><label>Discount Filter:</label><select name="columns[testtest]" class="form-control input-sm" id="discount-custom-filter" onchange="callNative()"><option value="">Recently Added</option><option value="high">Hign To Low</option><option value="low">Low To High</option></select></div>');

    jQuery('#user_datatable_length').parent().closest("div.col-sm-12").removeClass("col-md-12");  
    jQuery('#user_datatable_length').parent().closest("div.col-sm-12").addClass("col-md-12");  
    jQuery('#user_datatable_filter').parent().closest("div.col-sm-12").removeClass("col-md-12");  
    jQuery('#user_datatable_filter').parent().closest("div.col-sm-12").addClass("col-md-12");
    jQuery('#user_datatable_status_filter').closest("div.col-sm-12").removeClass("col-md-4");  
    jQuery('#user_datatable_status_filter').closest("div.col-sm-12").addClass("col-md-4");

    jQuery('#user_datatable_expiry_filter').closest("div.col-sm-12").removeClass("col-md-4");  
    jQuery('#user_datatable_expiry_filter').closest("div.col-sm-12").addClass("col-md-4");

    jQuery('#user_datatable_discount_filter').closest("div.col-sm-12").removeClass("col-md-2");  
    jQuery('#user_datatable_discount_filter').closest("div.col-sm-12").addClass("col-md-2");

    jQuery('#expiry-custom-filter').addClass("datepicker");
    jQuery( ".datepicker" ).datepicker({ 
        dateFormat: 'yy-mm-dd',
        changeYear: true,
        changeMonth: true,
        minDate: 0,
        });
  }
  </script>

<script type="text/javascript"> 

    function deleteRow(obj)
    {
      event.preventDefault(); // prevent form submit
      swal({
        title: "Are you sure?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm){
        if (isConfirm) {        
         obj.submit();

        } else {
           swal.close();
        }
      });
    }

    function changeStatus(id)
    {
      swal({
          title: "Are you sure?",
          type: "warning",
          showCancelButton: "No",
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Yes",
          cancelButtonText: "No",
        },
          function(){
            jQuery.ajax({
            url: '{{route('sp.offer.status.update')}}',
            type: 'POST',
            data:{offer_id:id},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
              $("#status_"+id).html(response);
            }
            });
          }
      );
    }
    
    function changeFeaturedStatus(id)
    {
      swal({
          title: "Are you sure?",
          type: "warning",
          showCancelButton: "No",
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Yes",
          cancelButtonText: "No",
        },
          function(){
            jQuery.ajax({
            url: '{{route('sp.offer.featuredstatus.update')}}',
            type: 'POST',
            data:{offer_id:id},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
              $("#featuredstatus_"+id).html(response);
            }
            });
          }
      );
    }
    $(function() {
        var table = $('#user_datatable').DataTable({
            stateSave: true,
        processing: true,
        serverSide: true,
        order: [[0, "desc" ]],
        "ajax":{
        "url": '{!! route('sp.offer.getdata') !!}',
                "dataType": "json",
                "type": "POST",
                
                data: function(d) {
                    d._token= "{{csrf_token()}}";
                    d.expiry_filter = $('#expiry-custom-filter').val();
                    d.status='spectacular';
                    d.category_filter = $('#custom-filter').val();
                    d.discount_filter = $('#discount-custom-filter').val();
                }
        },
        columns: [
        { data: 'id', name: 'id', orderable:true },
        { data: 'offer_id', name: 'offer_id', orderable:true },
        { data: 'user_id', name: 'user_id', orderable:true },
        { data: 'spectacular_offer_type_id', name: 'spectacular_offer_type_id', orderable:true },
        { data: 'discount', name: 'discount', orderable:true },
        { data: 'gender', name: 'gender', orderable:true },
        { data: 'service_type', name: 'service_type', orderable:true },
        
        { data: 'expiry_date', name: 'expiry_date', orderable:true  },
        { data: 'is_featured', name: 'is_featured', orderable:true  },
        { data: 'spectacular_status', name: 'spectacular_status', orderable:true },
        { data: 'created_at', name: 'created_at', orderable:true  },
        { data: 'action', name: 'action', orderable:true  },
        ],
        "columnDefs": [
        { "searchable": false, "targets": 0 }
        ]
        , language: {
        searchPlaceholder: "Search by Name & ID"
        },
        initComplete:function()
        {
          loadStatusFilter();
        }
});
});  


</script>
@endsection

