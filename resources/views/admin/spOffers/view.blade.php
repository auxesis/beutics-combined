@extends('layouts.admin.admin')
@section('uniquecss')

<style type="text/css">
    table.owntable.table.table-borered {
        width: 456px;
        margin: 0 auto;
        position: relative;
        left: -25%;
    }

    table.owntable th {
        background-color: #337ab7;
        color: #fff;
    }
</style>

@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>View Offer</h5>

            </header>
            <div id="collapse2" class="body">
              <div class="row">
                 @include('message')

                   <?php 
                             $key_arr = [
                              'men' => [
                                'store' => ['MEN_ORIGINALPRICE','MEN_DISCOUNT','MEN_BESTPRICE'],
                                'home' => ['MEN_HOME_ORIGINALPRICE','MEN_HOME_DISCOUNT','MEN_HOME_BESTPRICE'],
                              ],
                              'women' => [
                                'store' => ['WOMEN_ORIGINALPRICE','WOMEN_DISCOUNT','WOMEN_BESTPRICE'],
                                'home' => ['WOMEN_HOME_ORIGINALPRICE','WOMEN_HOME_DISCOUNT','WOMEN_HOME_BESTPRICE'],
                              ],
                              'kids' => [
                                'store' => ['KIDS_ORIGINALPRICE','KIDS_DISCOUNT','KIDS_BESTPRICE'],
                                'home' => ['KIDS_HOME_ORIGINALPRICE','KIDS_HOME_DISCOUNT','KIDS_HOME_BESTPRICE'],
                              ],
                              
                            ];
                            $total = 0;
                          ?>
                            @foreach($rows->getAssociatedOfferServices as $srow)

                              @foreach($srow->getAssociatedOfferServicesPrices as $pricerow)
                              <?php 
                                if(in_array($pricerow['field_key'],$key_arr[$rows->gender][$rows->service_type] ))
                                {
                                  $changecase = strtoupper($rows->gender).'_';
                                  $replace = str_replace($changecase, '', $pricerow['field_key']);
                                  $replace_arr = explode('_',$replace);

                                  $service_row_current['sp_service_id'] = $rows['id'];
                                  $service_row_current[$replace_arr[count($replace_arr)-1]] = $pricerow['price'];
                                    
                                }
                              ?>
                              @endforeach
                              <?php 
                                $total = $total+($service_row_current['ORIGINALPRICE']*$srow->quantity);
                                ?>
                                  

                             @endforeach

                <div class="col-sm-12 col-lg-12">

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>ID</b></div>
                        <div class="col-lg-8"><?= $rows->id; ?></div> <div class="clearfix"></div>
                    </div>
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Service Provider Name</b></div>
                        <div class="col-lg-8"><?= $rows->getAssociatedOfferProviderName->store_name; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Offer Name</b></div>
                        <div class="col-lg-8"><?= $rows->getAssociatedOfferName->title; ?></div> <div class="clearfix"></div>
                    </div>
                    @if($rows->is_nominated != '1')
                    <div  class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Spectacular Offer</b></div>
                        <div class="col-lg-8"><?= $rows->spectacular_offer == '1' ? 'Yes':'No'; ?></div> <div class="clearfix"></div>
                    </div>
                    @else

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Spectacular Offer Type</b></div>
                        <div class="col-lg-8"><?= isset($rows->getAssociatedSpectOfferName->title)? $rows->getAssociatedSpectOfferName->title:''; ?></div> <div class="clearfix"></div>
                    </div>
                    @endif

                     <div  class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Gender</b></div>
                        <div class="col-lg-8"><?= $rows->gender; ?></div> <div class="clearfix"></div>
                    </div>

                     <div  class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Service Type</b></div>
                        <div class="col-lg-8"><?= $rows->service_type; ?></div> <div class="clearfix"></div>
                    </div>

                    <div  class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Total Price</b></div>
                        <div class="col-lg-8"><?= $total.' AED'; ?></div> <div class="clearfix"></div>
                    </div>
                    <div  class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Discount</b></div>
                        <div class="col-lg-8"><?= $rows->discount; ?></div> <div class="clearfix"></div>
                    </div>
                     <div  class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Best Price</b></div>
                        <div class="col-lg-8"><?= $total - (($total*$rows->discount)/100).' AED'; ?></div> <div class="clearfix"></div>
                    </div>
                    <div  class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Expiry Date</b></div>
                        <div class="col-lg-8"><?= date('d-M-Y',strtotime($rows->expiry_date)); ?></div> <div class="clearfix"></div>
                    </div>
                    <div  class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Offer Terms</b></div>
                        <div class="col-lg-8"><?= $rows->offer_terms; ?></div> <div class="clearfix"></div>
                    </div>

                    <div  class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Offer Details</b></div>
                        <div class="col-lg-8"><?= $rows->offer_details; ?></div> <div class="clearfix"></div>
                    </div>


                    <div  class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Nominated Offer</b></div>
                        <div class="col-lg-8"><?= $rows->is_nominated == '1' ? 'Yes':'No'; ?></div> <div class="clearfix"></div>
                         <br/>
                    </div>
                    @if($rows->is_nominated == '1')
                     <div  class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Remarks</b></div>
                        <div class="col-lg-8"><?= $rows->remarks; ?></div> <div class="clearfix"></div>
                         <br/><br/>
                    </div>
                    @endif
                    <div class="new_section">
                        <table class="owntable table table-borered">
                          <thead>
                            <tr>

                              <th scope="col">Service Name</th>
                              <th scope="col">qty</th>
                              <th scope="col">price</th>
                            </tr>
                          </thead>
                          <tbody>

                            <?php 
                         
                            $total = 0;
                          ?>
                            @foreach($rows->getAssociatedOfferServices as $srow)
                            <tr>
                              <td>{{$srow->getAssociatedOfferServicesName->getAssociatedService->name}}</td>
                              <td>{{$srow->quantity}}</td>

                              @foreach($srow->getAssociatedOfferServicesPrices as $pricerow)
                              <?php 
                                if(in_array($pricerow['field_key'],$key_arr[$rows->gender][$rows->service_type] ))
                                {
                                  $changecase = strtoupper($rows->gender).'_';
                                  $replace = str_replace($changecase, '', $pricerow['field_key']);
                                  $replace_arr = explode('_',$replace);

                                  $service_row_current['sp_service_id'] = $rows['id'];
                                  $service_row_current[$replace_arr[count($replace_arr)-1]] = $pricerow['price'];
                                    
                                }
                              ?>
                              @endforeach
                              <td><?php 
                                $total = $total+($service_row_current['ORIGINALPRICE']*$srow->quantity);
                                echo $service_row_current['ORIGINALPRICE']; 
                                ?>
                                  
                              </td>

                            </tr>
                             @endforeach
                           <tr>
                              <td>Total </td>
                              <td> </td>
                              <td colspan="2" id="totalPrice"><?php echo $total;?></td>
                            </tr>
                           
                           
                          </tbody>
                        </table>   
                      </div>

                </div>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>

@endsection