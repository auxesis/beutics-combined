@extends('layouts.admin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
 
@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>Service Provider Expired Offers</h5>
            </header>
            <div id="collapse4" class="body">
              @include('message')
              
               <table id="user_datatable" class="table table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th width="15%">ID </th>
                        <th>Offer Name </th>
                        <th>Store Name</th>
                        <th>Spectacular Offer </th>
                        <th>Created At</th>
                        <th>Expiry Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
                  
            </div>
        </div>
    </div>
</div>
@endsection

@section('uniquescript')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>
<script type="text/javascript"> 
 
    $(function() {
        var table = $('#user_datatable').DataTable({
            stateSave: true,
        processing: true,
        serverSide: true,
        order: [[0, "desc" ]],
        "ajax":{
        "url": '{!! route('sp.offer.getdata') !!}',
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{csrf_token()}}",status:'expired'}
        },
        columns: [
        { data: 'id', name: 'id', orderable:true },
        { data: 'offer_id', name: 'offer_id', orderable:true },
        { data: 'user_id', name: 'user_id', orderable:true },
        { data: 'spectacular_offer', name: 'spectacular_offer', orderable:true },
        { data: 'created_at', name: 'created_at', orderable:true  },
        { data: 'expiry_date', name: 'expiry_date', orderable:true  },
        { data: 'action', name: 'action', orderable:true  },
        ],
        "columnDefs": [
        { "searchable": false, "targets": 0 }
        ]
        , language: {
        searchPlaceholder: "Search by name"
        },
});
});  


</script>
@endsection

