@extends('layouts.admin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
 
@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>{{ $title }}</h5>
                
            </header>
            <div id="collapse4" class="body">
               @include('message')
              
               <table id="page_datatable" class="table table-bordered" style="width:100%">
                <thead>
                    <tr> 
                        <th>Title</th>       
                        <th>Created At</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
                  
            </div>
        </div>
    </div>
</div>
@endsection

@section('uniquescript')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>
<script type="text/javascript">

    // function changeStatus(id)
    // {
    //   swal({
    //       title: "Are you sure?",
    //       type: "warning",
    //       showCancelButton: "No",
    //       confirmButtonClass: "btn-danger",
    //       confirmButtonText: "Yes",
    //       cancelButtonText: "No",
    //     },
    //       function(){
    //         jQuery.ajax({
    //         url: '{{route('admin.users.status.update')}}',
    //         type: 'POST',
    //         data:{user_id:id},
    //         headers: {
    //         'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    //         },
    //         success: function (response) {
    //           $("#status_"+id).html(response);
    //         }
    //         });
    //       }
    //   );
    // }
 
    $(function() {
        var table = $('#page_datatable').DataTable({
        processing: true,
        serverSide: true,
        order: [[0, "desc" ]],
        "ajax":{
        "url": '{!! route('custompages.getdata') !!}',
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{csrf_token()}}"}
        },
        columns: [
        { data: 'title', name: 'title', orderable:true },
        { data: 'created_at', name: 'created_at', orderable:true  },
        { data: 'action', name: 'action', orderable:true  },
        ],
        "columnDefs": [
        { "searchable": false, "targets": 0 }
        ]
        , language: {
        searchPlaceholder: "Search"
        },
});
});  


</script>
@endsection

