@extends('layouts.admin.admin')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>{{ $title }}</h5>
            

            </header>
            <div id="collapse2" class="body">
                <!-- <form class="form-horizontal" id="popup-validation"> -->
                {!! Form::open(['route' => 'custompages.update', 'method' => 'POST', 'class' => 'form-horizontal', 'id' => 'popup-validation']) !!}
                {{ csrf_field() }}
                	<input type="hidden" name="slug" value="<?= $row->slug;?>">
                    <div class="form-group">
                        <label class="control-label col-lg-3">Title</label>
                        <div class="col-lg-7">
                            <input type="text" class="validate[required] form-control" id="req" name="title" value="<?= $row->title;?>" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-3">Content</label>
                        <div class="col-lg-7">
                           <textarea id="messageArea" name="content" rows="7" class="form-control ckeditor" placeholder="Write your message.."><?= $row->content;?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-3">Content(Arabic)</label>
                        <div class="col-lg-7">
                           <textarea id="messageArea_1" name="content_ar" rows="7" class="form-control ckeditor" placeholder="Write your message.."><?= $row->content_ar;?></textarea>
                        </div>
                    </div>


                    <div class="form-actions no-margin-bottom">
                        <input type="submit" value="Submit" class="btn btn-primary">
                    </div>
                    <br><br><br><br>
                </form>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
@endsection
@section('uniquescript')

   <script src="{{ url('/') }}/assets/lib/ckeditor/ckeditor.js"></script>
 	<script type="text/javascript">
         CKEDITOR.replace( 'messageArea',
         {
          customConfig : 'config.js',
          toolbar : 'simple'
          });

         CKEDITOR.replace( 'messageArea_1',
         {
          customConfig : 'config.js',
          toolbar : 'simple'
          });
	</script> 
@endsection
