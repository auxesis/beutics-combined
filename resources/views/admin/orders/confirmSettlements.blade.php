@extends('layouts.admin.admin')

@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/jquery.gritter/css/jquery.gritter.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/themes/default/css/uniform.default.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{ url('/') }}/assets/lib/datepicker/jquery-ui.css">
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>{{$title}}</h5>
          

            </header>
             @include('message')
            <div id="collapse2" class="body">
                <!-- <form class="form-horizontal" id="popup-validation"> -->
                {!! Form::open(['route' => 'orders.save-settlements', 'method' => 'POST', 'class' => 'form-horizontal','id' => 'popup-validation','enctype' => 'multipart/form-data']) !!}
                {{ csrf_field() }}
                    {{ Form::hidden('order_ids',$ids) }}
                    {{ Form::hidden('total_amount',abs($total_sp_cost)) }}
                    {{ Form::hidden('sp_id',$sp_id) }}
                    <!--<div class="form-group">
                        {{Form::label('name', 'Total Billing Amount (To Pay)', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{Form::label('name', $total_sp_cost.' AED', ['class' => 'control-label col-lg-6'])}}
                        </div>
                    </div>-->
                    <div class="form-group">
                        {{Form::label('name', 'Total Pay', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{Form::label('name', abs($total_sp_cost).' AED', ['class' => 'control-label col-lg-6'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('paid_by', 'Paid By', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            <?php if($total_sp_cost > 0){ $sel_val = 'Admin'; } else { $sel_val = 'Service Provider'; } ?>
                            {{Form::label('name', $sel_val, ['class' => 'control-label col-lg-6'])}}
                            {{ Form::hidden('paid_by',$sel_val) }}    
                            <!-- {{ Form::select('paid_by', ['Admin' => 'Admin', 'Service Provider' => 'Service Provider'],$sel_val,['class' => 'form-control validate[required]','placeholder'=>'Select']) }} -->
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('payment_mode', 'Payment Mode', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::select('payment_mode', ['Online Banking' => 'Online Banking', 'Cash' => 'Cash', 'Cheque' =>'Cheque', 'Others' =>'Others'],null,['class' => 'form-control','placeholder'=>'Select']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('payment_date', 'Payment Date', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('payment_date','', ['id'=>'datepicker', 'class' => 'form-control validate[required]']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('payment_transaction_id', 'Payment Transaction ID (if any)', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('payment_transaction_id','', ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('notes', 'Notes (if any)', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::textarea('notes','', ['class' => 'form-control']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('receipt', 'Upload Receipt (if any)', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::file('receipt', ['class' => 'form-control','autocomplete'=>'off', 'style' => 'height: 100%;']) }}
                        </div>
                    </div>
                    <div class="form-actions no-margin-bottom">
                        <input type="submit" value="Submit" class="btn btn-primary">
                    </div>
                    <br><br><br><br>
                </form>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
@endsection
@section('uniquescript')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>

     <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/holder/2.4.1/holder.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/jquery.uniform.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="{{ url('/') }}/assets/lib/datepicker/jquery-ui.js"></script>
    <script>
        $(function() {
          Metis.formValidation();
        });
        $( function() {
          $("#datepicker").datepicker({ 
            dateFormat: 'yy-mm-dd',
            changeYear: true,
            changeMonth: true,
            });
        } );
    </script>


@endsection