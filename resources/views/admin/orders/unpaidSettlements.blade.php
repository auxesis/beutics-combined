@extends('layouts.admin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
   <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>All Unpaid Settlements</h5>
               
            </header>
            <div id="collapse4" class="body">
              @include('message')
              {!! Form::open(['route' => 'orders.confirm-settlements', 'method' => 'post', 'class' => 'form-horizontal','id' => 'popup-validation','enctype' => 'multipart/form-data']) !!}
                {{ csrf_field() }}
               <table id="user_datatable" class="table table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th width="10%">All </th>
                        <th width="10%">Customer Name </th>
                        <th width="10%">Service Provider </th>
                        <th width="10%">Order ID</th>
                        <th width="10%">Billing Amount (To Pay)</th>
                        <th width="10%">Pay to Beutics</th>
                        <th width="10%">Pay to SP</th>
                        <th width="10%">Order Date</th>
                        <th width="10%">Order Completed Date</th>
                        <th width="10%">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <div class="form-actions no-margin-bottom">                                            
                <input type="submit" value="Mark As Paid" id="submit_paid" class="btn btn-primary">
                <p class="error_msg error"></p>
            </div>
            </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('uniquescript')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script type="text/javascript">

  function callNative(){

    $("#user_datatable").DataTable().draw();
  }
  function loadStatusFilter()
  {

    jQuery("#user_datatable_filter").parent().parent().
      append("<div id='user_datatable_payment_filter' class='col-sm-12 col-md-6'><label>Payment Mode:</label><br><input type='radio' checked onchange='callNative()' name='payment_type' value='0' class='payment-filter' /><label>Billable &nbsp;&nbsp;&nbsp;</label><input type='radio'  onchange='callNative()' name='payment_type' value='1' class='payment-filter' /><label>Non-Billable</label></div>");
       $(function() {
               $(".myselect").select2();

               $('input[type=radio][name=payment_type]').change(function() {
                  if (this.value == '1') {
                    $('#popup-validation').attr('action', '{!! route('orders.offline-settlements') !!}');
                  }
                  else if (this.value == '0') {
                    $('#popup-validation').attr('action', '{!! route('orders.confirm-settlements') !!}');
                  }
              });

               $("#submit_paid").click(function () {
                  $('.error_msg').html('');
                  //Reference the CheckBoxes and determine Total Count of checked CheckBoxes.
                  var checked = $("#user_datatable input[type=checkbox]:checked").length;
       
                  if (checked > 0) {
                      //alert(checked + " CheckBoxe(s) are checked.");
                      return true;
                  } else {
                      //alert("Please select CheckBoxe(s).");
                      $('.error_msg').html('Please select Service Provider or atleast one CheckBoxe(s).');
                      return false;
                  }
              });

            });
  }
  </script>
<script type="text/javascript"> 
    
     function deleteRow(obj)
      {
        event.preventDefault(); // prevent form submit
        swal({
          title: "Are you sure?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          cancelButtonText: "No",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm){
          if (isConfirm) {        
           obj.submit();

          } else {
             swal.close();
          }
        });
      }

    $(function() {
        var table = $('#user_datatable').DataTable({
           stateSave: true,
        processing: true,
        serverSide: true,
        order: [[0, "desc" ]],
        "ajax":{
        "url": '{!! route('orders.getdataUnpaid') !!}',
                "dataType": "json",
                "type": "POST",
                data: function(d) {
                    d._token= "{{csrf_token()}}";
                    d.payment_type = $("input[name='payment_type']:checked"). val();
                    d.sp_id = <?php echo $sp_id ?>;
                }
        },
        columns: [
        { data: 'input_el', input_el: 'input_el', orderable:true },
        { data: 'name', name: 'name', orderable:true },
        { data: 'sp_id', name: 'sp_id', orderable:true },
        { data: 'booking_unique_id', name: 'booking_unique_id', orderable:true },
        { data: 'sp_cost', name: 'sp_cost', orderable:true },
        { data: 'beutics_comm', name: 'beutics_comm', orderable:true },
        { data: 'sp_total_to_pay', name: 'sp_total_to_pay', orderable:true },
        { data: 'created_date', name: 'created_date', orderable:true },
        { data: 'completed_date_time', name: 'completed_date_time', orderable:true },
        { data: 'action', name: 'action', orderable:true  },
        ],
        "columnDefs": [
        { "searchable": false, "targets": 0 }
        ]
        , language: {
        searchPlaceholder: "Search by name & mobile"
        },
        initComplete:function()
        {
          loadStatusFilter();
        }
});
});  


</script>
<style>
  .dataTables_filter {
display: none;
}
</style>style>
@endsection

