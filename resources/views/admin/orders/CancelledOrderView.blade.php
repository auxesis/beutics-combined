@extends('layouts.admin.admin')

@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/jquery.gritter/css/jquery.gritter.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/themes/default/css/uniform.default.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css">
   <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/datepicker/jquery-ui.css">


@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>{{$title}}</h5>
            </header>
             @include('message')
            <div id="collapse2" class="body">
                <!-- <form class="form-horizontal" id="popup-validation"> -->
                {!! Form::open(['route' => 'orders.cancel', 'method' => 'post', 'class' => 'form-horizontal','id' => 'popup-validation','enctype' => 'multipart/form-data']) !!}
                {{ csrf_field() }}
                    <input type="hidden" name="order_id" value="<?= $result['id']; ?>" />
                    <input type="hidden" name="description" value="Refund" />
                                      
                    <!-- <div class="form-group">
                        <label class="control-label col-lg-4">Booking Amount</label>
                        <div class="col-lg-4" style="margin-top:5px;">
                            <?= $result['paid_amount'].' AED' ; ?>
                        </div>
                    </div> -->
                    <div class="col-sm-12 col-lg-12">
                    
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Payment Mode</b></div>
                        <div class="col-lg-8"><?= ($result['payment_type'] == '0')?'Paid':'To Pay at Store'; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Booking Item Amount</b></div>
                        <div class="col-lg-8"><b>
                          <?php if($result['final_settlement_amount']){
                            echo $item_amt = $result['final_settlement_amount'];
                          } else {
                            echo $item_amt = $result['total_item_amount'];
                          } echo ' AED';?></b>
                        </div> <div class="clearfix"></div>
                    </div>

                     <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Promo Code Discount</b></div>
                        <div class="col-lg-8">(<b>-</b>) <?= ($result['promo_code_amount'])?$result['promo_code_amount'].' AED':'0 AED'; ?></div> <div class="clearfix"></div>
                    </div>
                     <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Redeem Points</b></div>
                        <div class="col-lg-8">(<b>-</b>) <?= ($result['redeemed_reward_points'])?$result['redeemed_reward_points']:'0 AED'; ?></div> <div class="clearfix"></div>
                    </div>

                     <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Total Amount</b></div>
                        <div class="col-lg-8"><b><?php echo ($item_amt-($result['promo_code_amount']+$result['redeemed_reward_points'])).' AED'; ?></b></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Total Tax</b></div>
                        <div class="col-lg-8">(<b>+</b>) 
                          <?php if($result['final_settlement_tax']){
                            echo $result['final_settlement_tax'].' AED';
                          } else {
                            echo $result['tax_amount'].' AED';
                          } ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Total Paid</b></div>
                        <div class="col-lg-8"><b>
                          <?php if($result['final_settlement_amount']){
                            echo $result['total_final_settlement_amount'].' AED';
                          } else {
                            echo $result['paid_amount'].' AED';
                          } ?></b></div> <div class="clearfix"></div>
                    </div>                    


                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Total Cashback</b></div>
                        <div class="col-lg-8">
                          <?php if($result['final_settlement_cashback']){
                            echo $result['final_settlement_cashback'].' AED';
                          } else {
                            echo $result['total_cashback'].' AED';
                          } ?>
                          </div> <div class="clearfix"></div>
                    </div>
                    <?php if($result['invoice']){ ?>
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Invoice</b></div>
                        <div class="col-lg-8"><a href="{{ asset('sp_uploads/invoice/'.$result['invoice']) }}" class="" target="_blank">View Invoice</a></div> <div class="clearfix"></div>
                    </div>
                    <?php } ?>

                    </div>
                    <?php if($result['promo_code_amount']){ ?>
                    <div class="clearfix"></div><br><br>
                    <div class="col-sm-12 col-lg-12">
                      <h3><u>Promocode Details</u></h3>
                      <div class="col-lg-12 border_details">
                          <div class="col-lg-4"><b>Promo Code</b></div>
                          <div class="col-lg-8"><?= $result['promo_code_title']; ?></div> <div class="clearfix"></div>
                      </div>
                      <div class="col-lg-12 border_details">
                          <div class="col-lg-4"><b>Amount</b></div>
                          <div class="col-lg-8"><?= $result['promo_code_amount'].' AED'; ?></div> <div class="clearfix"></div>
                      </div>
                    </div>
                    <?php } ?>

                  <div class="clearfix"></div><br><br>
                  <?php if($result['payment_type'] == '0'){ ?>
                   <div class="form-group">
                        {{Form::label('cancellation_refunded_amount', 'Enter Refund Amount', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('cancellation_refunded_amount','', ['class' => 'form-control validate[required]','autocomplete'=>'off', 'maxlength' => '8']) }}
                        </div>
                    </div>
                  <?php } else { ?>
                    {{ Form::hidden('cancellation_refunded_amount','0') }}
                  <?php } ?>
                   <div class="form-group">
                        {{Form::label('cancellation_reason', 'Cancellation Reason', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::textarea('cancellation_reason','', ['class' => 'form-control','autocomplete'=>'off', 'maxlength' => '150']) }}
                            <small><i>Max 150 characters are allowed.</i></small>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                 
                    <div class="form-actions no-margin-bottom">
                                            
                        <input type="submit" value="Submit" class="btn btn-primary">
                    </div>
                    
                </form>
            </div>
        </div>  
    </div>
    <!-- /.col-lg-12 -->
</div>

@endsection
@section('uniquescript')

    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>

     <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/holder/2.4.1/holder.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/jquery.uniform.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="{{ url('/') }}/assets/lib/datepicker/jquery-ui.js"></script>
        
    <script>    
        $(function() {
          Metis.formValidation();
        });
    </script>

@endsection