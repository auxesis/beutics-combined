@extends('layouts.admin.admin')
@section('uniquecss')

<style type="text/css">
    table.owntable th {
        background-color: #337ab7;
        color: #fff;
    }
</style>

@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>View Transaction Details</h5>

            </header>
            <div id="collapse2" class="body">
              <div class="row">
                 @include('message')
                <div class="col-sm-12 col-lg-12">
                    <h3><u>Transaction Details</u></h3>
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Service Provider</b></div>
                        <div class="col-lg-8"><?= $result['store_name']; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Payment Transaction ID</b></div>
                        <div class="col-lg-8"><?= $result['payment_transaction_id']; ?></div> <div class="clearfix"></div>
                    </div>
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Paid By</b></div>
                        <div class="col-lg-8"><?= $result['paid_by']; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Payment Mode</b></div>
                        <div class="col-lg-8"><?= $result['payment_mode']; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Total Amount</b></div>
                        <div class="col-lg-8"><?= $result['total_amount']; ?></div> <div class="clearfix"></div>
                    </div>
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Notes</b></div>
                        <div class="col-lg-8"><?= $result['notes']; ?></div> <div class="clearfix"></div>
                    </div>
                    <?php if($result['receipt']){ ?>
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Receipt</b></div>
                        <div class="col-lg-8"><a href="{{ asset('public/receipts/'.$result['receipt']) }}" class="" target="_blank">View Receipt</a></div> <div class="clearfix"></div>
                    </div>
                    <?php } ?>
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Payment Date</b></div>
                        <div class="col-lg-8"><?= ($result['payment_date']); ?></div> <div class="clearfix"></div>
                    </div>
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Transaction Date & Time</b></div>
                        <div class="col-lg-8"><?= ($result['transaction_date_time'])?date('Y-m-d h:i A', strtotime($result['transaction_date_time'])):''; ?></div> <div class="clearfix"></div>
                    </div>
                  </div>
                 
                      <?php if($result['PaymentSettlementOrder']){ ?>
                       <div class="clearfix"></div><br><br>
                      <div class="col-sm-12 col-lg-12">
                        <h3><u>Orders List </u></h3><br>
                        
                        <div class="new_section">
                        <table class="owntable table table-borered">
                          <thead>
                            <tr>
                              <th scope="col">Customer Name</th>
                              <th scope="col">Appointment Date & Time</th>
                              <th scope="col">Service End Time</th>
                              <th scope="col">Order Date</th>
                              <th scope="col">Order Completed Date</th>
                              <th scope="col">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($result['PaymentSettlementOrder'] as $srrow)
                             <tr>
                              <td><?= $srrow['name']; ?></td>
                              <td><?= $srrow['appointment_date']; ?></td>                         
                              <td><?= $srrow['service_end_time']; ?></td>   
                              <td><?= $srrow['b_created_at']; ?></td>       
                              <td><?= $srrow['completed_date_time']; ?></td>       
                              <td><?= $srrow['action']; ?></td>       
                            </tr>
                            @endforeach
                          </tbody>
                        </table>   
                      </div>
                </div><br><br>
                <?php } ?>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>

@endsection