@extends('layouts.admin.admin')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>View Orders</h5>

            </header>
            <div id="collapse2" class="body">
                <div class="row">
                    <div class="col-sm-12 col-lg-12">
                         <div class="col-lg-12 border_details">
                            <div class="col-lg-4"><b>Order Id</b></div>
                            <div class="col-lg-8"><?= $row->booking_unique_id; ?></div> <div class="clearfix"></div>
                        </div>

                        <div class="clearfix"></div><br><br>
                    </div>
                </div>
            </div>
        </div>
    <!-- /.col-lg-12 -->
    </div>
</div>

@endsection
