@extends('layouts.admin.admin')
@section('uniquecss')
<style type="text/css">
.second_msgh p {
    background: #05728f none repeat scroll 0 0;
    border-radius: 3px;
    font-size: 14px;
    margin: 0;
    color: #fff;
    padding: 15px 16px 15px 22px;
    width: 100%;
}
.incoming_msg{margin-top:8px;}
.time_date {   margin: 2px 0 0;}
.incoming_msg_img { margin-top: 6px;text-align: center;}
</style>
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
 
@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>Order Chat History</h5>
            </header>
            <div id="collapse4" class="body">
              @include('message')

              <div class="messaging">
                <div class="inbox_msg">
                  <div style="width: 100%">
                    <div class="headind_srch" style="background: #f8f8f8 none repeat scroll 0 0;">
                      <div class="recent_heading">
                        <h4>{{$title}}</h4>
                      </div>
                  
                    </div>

                    <div class="mesgs" id="other-data" style="width: 100%">
                        <div class="msg_history" id="messages">
                          @if($data)
                          @foreach($data as $chatrow)

                            @if($chatrow['user_id_tbl']=='users')
                            <div class="incoming_msg">
                                <div class="incoming_msg_img"> 
                                 <b> <?php echo $chatrow['name']; ?></b>
                                </div>
                                <div class="received_msg">
                                  <div class="received_withd_msg" >
                                    <pre>{{$chatrow['message']}}</pre>
                                    <span class="time_date"> {{date('h:i A', strtotime($chatrow['user_time'].'+4 hours'))}}   |    {{date('M d Y', strtotime($chatrow['user_time'].'+4 hours'))}}</span>
                                  </div>
                                </div>
                            </div>
                            @else

                            <div class="incoming_msg">
                                <div class="incoming_msg_img"> 
                                  <b><?php echo $chatrow['name']; ?></b>
                                </div>
                                <div class="received_msg">
                                  <div class="received_withd_msg second_msgh" >
                                    <pre>{{$chatrow['message']}}</pre>
                                    <span class="time_date"> {{date('h:i A', strtotime($chatrow['user_time'].'+4 hours'))}}   |    {{date('M d Y', strtotime($chatrow['user_time'].'+4 hours'))}}</span>
                                  </div>
                                </div>
                            </div>

                            @endif

                          @endforeach
                          @endif

                        </div>
                        </div>

              
                  </div>
                  
                  
                </div>
              </div>

            </div>
        </div>
    </div>
</div>

@endsection

@section('uniquescript')

<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>

<script src="{{url('assets/js/moment.min.js')}}"></script>
<!-- <script src="{{url('assets/js/socket.io-1.2.0.js')}}"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script>
<!-- <script src="/socket.io/socket.io.js"></script> -->

@endsection