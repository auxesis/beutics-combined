@extends('layouts.admin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
 <link rel="stylesheet" href="{{ url('/') }}/assets/lib/datepicker/jquery-ui.css">
@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>All Confirmed Orders</h5>
               
            </header>
            <div id="collapse4" class="body">
              @include('message')
              
               <table id="user_datatable" class="table table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th width="10%">Order ID </th>
                        <th width="10%">Customer Name </th>
                        <th width="10%">Service Provider </th>
                        <th width="10%">Appointment Date & Time</th>
                        <th width="10%">Service End Date</th>
                        <th width="10%">Staff Name</th>
                        <th width="10%">Payment Type</th>
                        <th width="10%">Order Date</th>
                        <th width="10%">Confirmed Date & Time</th>
                        <th width="10%">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
                  
            </div>
        </div>
    </div>
</div>
@endsection

@section('uniquescript')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>
<script src="{{ url('/') }}/assets/lib/datepicker/jquery-ui.js"></script>
<script type="text/javascript">
  function callNative(){

    $("#user_datatable").DataTable().draw();
  }
  function loadStatusFilter()
  {

    jQuery("#user_datatable_filter").parent().parent().
      append('<div id="user_datatable_from_appointment_date" class="col-sm-12 col-md-3"><label>From Appointment Date:</label><input type="text" class="form-control" id="from_app_date-custom-filter" onchange="callNative()" autocomplete="off"></div><div id="user_datatable_to_appointment_date" class="col-sm-12 col-md-3"><label>To Appointment Date:</label><input type="text" class="form-control" id="to_app_date-custom-filter" onchange="callNative()" autocomplete="off"></div>');

    // jQuery('#from_app_date-custom-filter').addClass("datepicker");
    // jQuery('#to_app_date-custom-filter').addClass("datepicker");
    jQuery( "#to_app_date-custom-filter" ).attr('disabled',true);
    jQuery( "#from_app_date-custom-filter" ).datepicker({ 
        dateFormat: 'yy-mm-dd',
        changeYear: true,
        changeMonth: true,
        // minDate: 0,
        onSelect: function(dateText, inst){
           $("#to_app_date-custom-filter").datepicker("option","minDate",
           $("#from_app_date-custom-filter").datepicker("getDate"));
           jQuery( "#to_app_date-custom-filter" ).attr('disabled',false);
        }
        });

    jQuery( "#to_app_date-custom-filter" ).datepicker({ 
        dateFormat: 'yy-mm-dd',
        changeYear: true,
        changeMonth: true,
       
        });
  }
  </script>

<script type="text/javascript"> 
    
     function deleteRow(obj)
      {
        event.preventDefault(); // prevent form submit
        swal({
          title: "Are you sure?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          cancelButtonText: "No",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm){
          if (isConfirm) {        
           obj.submit();

          } else {
             swal.close();
          }
        });
      }

    $(function() {
        var table = $('#user_datatable').DataTable({
           stateSave: true,
        processing: true,
        serverSide: true,
        order: [[0, "desc" ]],
        "ajax":{
        "url": '{!! route('orders.getdataConfirm') !!}',
                "dataType": "json",
                "type": "POST",
                 data: function(d) {
                    d._token= "{{csrf_token()}}";
                    d.from_appointment_date = $('#from_app_date-custom-filter').val();
                    d.to_appointment_date = $('#to_app_date-custom-filter').val();
                }
        },
        columns: [
        { data: 'booking_unique_id', name: 'booking_unique_id', orderable:true },
        { data: 'name', name: 'name', orderable:true },
        { data: 'sp_id', name: 'sp_id', orderable:true },
        { data: 'appointment_date', name: 'appointment_date', orderable:true },
        { data: 'service_end_time', name: 'service_end_time', orderable:true },
        { data: 'staff_name', name: 'staff_name', orderable:true },
        { data: 'payment_type', name: 'payment_type', orderable:true },
        { data: 'created_date', name: 'created_date', orderable:true  },
        { data: 'confirmed_date_time', name: 'confirmed_date_time', orderable:true  },
        { data: 'action', name: 'action', orderable:true  },
        ],
        "columnDefs": [
        { "searchable": false, "targets": 0 }
        ]
        , language: {
        searchPlaceholder: "Search by name & mobile"
        },
        initComplete:function()
        {
          loadStatusFilter();
        }
});
});  


</script>
@endsection

