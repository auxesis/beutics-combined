@extends('layouts.admin.admin')
@section('uniquecss')

<style type="text/css">
    table.owntable th {
        background-color: #337ab7;
        color: #fff;
    }
</style>

@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>View Order Details</h5>

            </header>
            <div id="collapse2" class="body">
              <div class="row">
                 @include('message')
                <div class="col-sm-12 col-lg-12">
                    <h3><u>Order Details</u></h3>
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Order ID</b></div>
                        <div class="col-lg-8"><?= $result['booking_unique_id']; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Order Service Type</b></div>
                        <?php if($result['payment_type'] == "0"){ $type = "Online"; }else{ $type = "Offline"; } ?>
                        <div class="col-lg-8"><?= $result['booking_service_type'].' ('.$type.')'; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Customer Name</b></div>
                        <div class="col-lg-8"><?= $result['user_name']; ?></div> <div class="clearfix"></div>
                    </div>
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Customer Type</b></div>
                        <div class="col-lg-8"><b>
                          <?php if(($result['user_refer_code'] && $result['sp_share_code']) && $result['user_refer_code'] == $result['sp_share_code']){ 
                              echo ' SP Referred'; 
                            } elseif(($result['user_id'] == null && $result['db_user_name'])) {
                               echo 'Walk-in'; 
                           } else {
                               echo 'Beutics'; 
                           } ?></b>
                        </div> <div class="clearfix"></div>
                    </div>
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Service Provider</b></div>
                        <div class="col-lg-8"><?= $result['sp_store_name']; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Staff Name</b></div>
                        <div class="col-lg-8"><?= $result['staff_name']; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Appointment Date</b></div>
                        <div class="col-lg-8"><?= $result['appointment_date']; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Appointment Time</b></div>
                        <div class="col-lg-8"><?= ($result['appointment_time'])?date('h:i A', strtotime($result['appointment_time'])):''; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Service End Date</b></div>
                        <div class="col-lg-8"><?= $result['appointment_date']; ?> &nbsp;<?= ($result['service_end_time'])?date('h:i A', strtotime($result['service_end_time'])):''; ?></div>
                        
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Order Placed Date</b></div>
                        <div class="col-lg-8"><?= $result['created_date']; ?></div> <div class="clearfix"></div>
                    </div>
                  </div>
                  <div class="clearfix"></div><br><br>
                  <div class="col-sm-12 col-lg-12">
                    <h3><u>Payment Details</u></h3>
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Payment Mode</b></div>
                        <div class="col-lg-8"><?= ($result['payment_type'] == '0')?'Paid':'To Pay at Store'; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Booking Item Amount</b></div>
                        <div class="col-lg-8"><b>
                          <?php if($result['final_settlement_amount']){
                            echo $item_amt = $result['final_settlement_amount'];
                          } else {
                            echo $item_amt = $result['total_item_amount'];
                          } echo ' AED';?></b>
                        </div> <div class="clearfix"></div>
                    </div>

                     <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Promo Code Discount</b></div>
                        <div class="col-lg-8">(<b>-</b>) <?= ($result['promo_code_amount'])?$result['promo_code_amount'].' AED':'0 AED'; ?></div> <div class="clearfix"></div>
                    </div>
                     <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Redeem Points</b></div>
                        <div class="col-lg-8">(<b>-</b>) <?= ($result['redeemed_reward_points'])?$result['redeemed_reward_points']:'0 AED'; ?></div> <div class="clearfix"></div>
                    </div>

                     <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Total Amount</b></div>
                        <div class="col-lg-8"><b><?php echo ($item_amt-($result['promo_code_amount']+$result['redeemed_reward_points'])).' AED'; ?></b></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Total Tax</b></div>
                        <div class="col-lg-8">(<b>+</b>) 
                          <?php if($result['final_settlement_tax']){
                            echo $result['final_settlement_tax'].' AED';
                          } else {
                            echo $result['tax_amount'].' AED';
                          } ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Total Paid</b></div>
                        <div class="col-lg-8"><b>
                          <?php if($result['final_settlement_amount']){
                            echo $result['total_final_settlement_amount'].' AED';
                          } else {
                            echo $result['paid_amount'].' AED';
                          } ?></b></div> <div class="clearfix"></div>
                    </div>                    

                    <?php if($result['payment_details']){ ?>
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Paid By</b></div>
                        <div class="col-lg-8"><b><?php echo $result['payment_details']; ?></b></div> <div class="clearfix"></div>
                    </div>
                   <?php } ?>
                    <?php if($result['pt_transaction_id']){ ?>
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Paytabs Transaction ID</b></div>
                        <div class="col-lg-8"><?php echo $result['pt_transaction_id']; ?></div> <div class="clearfix"></div>
                    </div>
                   <?php } ?>
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Total Cashback</b></div>
                        <div class="col-lg-8">
                          <?php if($result['final_settlement_cashback']){
                            echo $result['final_settlement_cashback'].' AED';
                          } else {
                            echo $result['total_cashback'].' AED';
                          } ?>
                          </div> <div class="clearfix"></div>
                    </div>
                    <?php if($result['invoice']){ ?>
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Invoice</b></div>
                        <div class="col-lg-8"><a href="{{ asset('sp_uploads/invoice/'.$result['invoice']) }}" class="" target="_blank">View Invoice</a></div> <div class="clearfix"></div>
                    </div>
                    <?php } ?>

                    </div>
                    <?php if($result['promo_code_amount']){ ?>
                    <div class="clearfix"></div><br><br>
                    <div class="col-sm-12 col-lg-12">
                      <h3><u>Promocode Details</u></h3>
                      <div class="col-lg-12 border_details">
                          <div class="col-lg-4"><b>Promo Code</b></div>
                          <div class="col-lg-8"><?= $result['promo_code_title']; ?></div> <div class="clearfix"></div>
                      </div>
                      <div class="col-lg-12 border_details">
                          <div class="col-lg-4"><b>Amount</b></div>
                          <div class="col-lg-8"><?= $result['promo_code_amount'].' AED'; ?></div> <div class="clearfix"></div>
                      </div>
                    </div>
                    <?php } ?>

                  <div class="clearfix"></div><br><br>
                  <div class="col-sm-12 col-lg-12">
                    <h3><u>Order Status</u></h3>
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Status</b></div>
                        <div class="col-lg-8">
                          <?php if($result['status'] == 1){
                            echo 'Confirmed on '.date('Y-m-d h:i A', strtotime($result['confirmed_date_time']));
                          } else if($result['status'] == 2){
                            echo 'Cancelled on '.date('Y-m-d h:i A', strtotime($result['cancelled_date_time']));
                          } else if($result['status'] == 3){
                            echo 'Refunded';
                          } else if($result['status'] == 4){
                            echo 'Expired';
                          } else if($result['status'] == 5){
                            echo 'Completed on '.date('Y-m-d h:i A', strtotime($result['completed_date_time']));
                          } else {
                            echo 'New';
                          }?>
                        </div> <div class="clearfix"></div>
                    </div>
                    <?php if($result['status'] == 2){ ?>
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Cancellation Reason</b></div>
                        <div class="col-lg-8"><?= $result['cancellation_reason']; ?></div> <div class="clearfix"></div>
                    </div>
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Refund Amount</b></div>
                        <div class="col-lg-8"><?= $result['cancellation_refunded_amount'].' AED'; ?></div> <div class="clearfix"></div>
                    </div>
                  <?php } ?>
                  </div>

                    <div class="clearfix"></div><br><br>
                    <div class="col-sm-12 col-lg-12">
                      <h3><u>Order Item Details</u></h3>
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Order Type</b></div>
                        <div class="col-lg-8"><?= ($result['is_gift'])?'Gift Order':'Self Order'; ?></div> <div class="clearfix"></div>
                        <?php if($result['is_gift']){ ?><br>
                          <div class="col-lg-4"><b>Sender Name</b></div>
                          <div class="col-lg-8"><?= $result['gifted_by_name']; ?></div> <div class="clearfix"></div>
                          <br>                        
                          <h4>Gift Details</h4>

                        
                        <div class="new_section">
                        <table class="owntable table table-borered">
                          <thead>
                            <tr>
                              <th scope="col">Receiver</th>
                              <th scope="col">Receiver Email</th>
                              <th scope="col">Gifted By</th>
                              <th scope="col">Message</th>
                              <th scope="col">Gift Send Date</th>
                            </tr>
                          </thead>
                          <tbody>
                             <tr>
                              <td><?= $result['receiver_name'].'<br>( '.$result['receiver_mobile_no'].' )'; ?></td>
                              <td><?= $result['receiver_email']; ?></td>                         
                              <td><?= $result['gifted_by_name']; ?></td>                                        
                              <td><?= $result['receiver_message']; ?></td>  
                              <td><?= $result['gift_send_date']; ?></td>       
                            </tr>
                            
                          </tbody>
                        </table>   
                      </div>
                      <?php } ?>
                    </div>
                    <div class="clearfix"></div><br><br>
                  
                   <div class="new_section">
                        <table class="owntable table table-borered">
                          <thead>
                            <tr>
                              <th width="30%" scope="col">Item</th>
                              <th scope="col">Type</th>
                              <th scope="col">QTY</th>
                              <th scope="col">Gender</th>
                              <th scope="col">Original Price</th>
                              <th scope="col">Best Price</th>
                            </tr>
                          </thead>
                          <tbody>
                           <?php $total = 0;$terms=''; ?>
                            @foreach($result['booking_items'] as $srow)
                            

                            <tr>
                              <td>{{$srow['name']}}
                              <?php if(!empty($srow['services'])){ ?>: <div style="font-size:12px;"><i>
                                  @foreach($srow['services'] as $servrow)
                                   {{$servrow['name']}}(<b> {{$servrow['quantity']}} </b>), 
                                 @endforeach
                               </i>
                               <br/>
                               <?php 
                               $terms=preg_replace('/[^\da-z ]/i', '', $srow['offer_terms']);
                               $details=preg_replace('/[^\da-z ]/i', '', $srow['offer_details']);
                                ?>
                               <span>
                                  <a data-toggle="modal" data-target="#modalTermCondition" onclick="showModal('<?= $srow['name'];?>' , '<?= $terms;?>', '<?= $details;?>')"><b style="color:red;">View {{$srow['name']}} Details</b></a>
                               </span></div>
                                 <?php } ?>
                              </td>
                              <td><b><?= isset($srow['service_id']) ?  __('messages.Service'):__('messages.Offer'); ?></b></td>
                              <td>{{$srow['quantity']}}</td>
                              <td>{{$srow['gender']}}</td>
                              <!-- change on28 sept -->
                             <!--  <?php if($result['db_user_name']){ ?>
                                <td><?php echo $srow['original_price'].' AED'; ?></td>  
                              <?php } else { ?>
                                <td><?php echo $srow['best_price'].' AED'; ?></td>  
                              <?php } ?>   -->
                                <!--  -->

                                <td><?php echo $srow['original_price'].' AED'; ?></td>  
                                <td><?php echo $srow['best_price'].' AED'; ?></td>  
                            </tr>
                             @endforeach
                          </tbody>
                        </table>   
                      </div>
                      </div>
                 
                      <?php if($result['reschedule_history']){ ?>
                       <div class="clearfix"></div><br><br>
                      <div class="col-sm-12 col-lg-12">
                        <h3><u>Reschedule Order Log </u></h3><br>
                        
                        <div class="new_section">
                        <table class="owntable table table-borered">
                          <thead>
                            <tr>
                              <th scope="col">Appointment Date</th>
                              <th scope="col">Appointment Time</th>
                              <th scope="col">Appointment End Time</th>
                              <th scope="col">Rescheduled On</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($result['reschedule_history'] as $srrow)
                             <tr>
                              <td><?= $srrow['appointment_date']; ?></td>
                              <td><?= $srrow['appointment_time']; ?></td>                         
                              <td><?= $srrow['service_end_time']; ?></td>   
                              <td><?= $srrow['created_at']; ?></td>       
                            </tr>
                            @endforeach
                          </tbody>
                        </table>   
                      </div>
                </div><br><br>
                <?php } ?>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- Modal -->
<div id="modalTermCondition" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="offer_name">
          
          
        </h4>
      </div>
      <div class="modal-body" >
        <h5>Offer Terms & Condition</h5>
        <p id="offer_terms"></p>
        <br/>
        <h5>Offer Details</h5>
        <p id="offer_details"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

@endsection
@section('uniquescript')
<script type="text/javascript">
  function showModal(offer_name,offer_terms,offer_details){
        $("#offer_name").html(offer_name+" Details");
          $("#offer_terms").html(offer_terms);
          $("#offer_details").html(offer_details);
      }
  
</script>
@endsection