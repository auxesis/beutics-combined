@extends('layouts.admin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
   <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
   <link rel="stylesheet" href="{{ url('/') }}/assets/lib/datepicker/jquery-ui.css">
@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>Transaction History</h5>
               
            </header>
            <div id="collapse4" class="body">
              @include('message')
              
               <table id="user_datatable" class="table table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th width="10%">Service Provider </th>
                        <th width="10%">Payment Transaction ID</th>
                        <th width="10%">Paid By</th>
                        <th width="10%">Payment Mode</th>
                        <th width="10%">Total Amount</th>
                        <th width="10%">Payment Date</th>
                        <th width="10%">Transaction Date</th>
                        <th width="10%">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            
            </div>
        </div>
    </div>
</div>
@endsection

@section('uniquescript')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="{{ url('/') }}/assets/lib/datepicker/jquery-ui.js"></script>
<script type="text/javascript">

  function callNative(){

    $("#user_datatable").DataTable().draw();
  }
  function loadStatusFilter()
  {

    jQuery("#user_datatable_filter").parent().parent().
      append("<div class='col-md-12' style='margin-bottom: 10px;'><div id='user_datatable_category_filter' class='col-sm-12 col-md-3'><label>Service Provider:</label><select name='columns[sp_users]' class='myselect form-control input-sm' id='custom-filter' onchange='callNative()'><option value=''>Select</option><?php foreach($sp_list as $key => $cat){ ?><option value='<?= $key?>'><?= $cat?></option><?php } ?></select></div><div id='user_datatable_from_appointment_date' class='col-sm-12 col-md-3'><label>From Payment Date:</label><input type='text' class='form-control' id='from_app_date-custom-filter' onchange='callNative()' autocomplete='off'></div><div id='user_datatable_to_appointment_date' class='col-sm-12 col-md-3'><label>To Payment Date:</label><input type='text' class='form-control' id='to_app_date-custom-filter' onchange='callNative()' autocomplete='off'></div><p></p></div>");
       $(function() {
               $(".myselect").select2();               
            });
        jQuery( "#to_app_date-custom-filter" ).attr('disabled',true);
        jQuery( "#from_app_date-custom-filter" ).datepicker({ 
            dateFormat: 'yy-mm-dd',
            changeYear: true,
            changeMonth: true,
            // minDate: 0,
            onSelect: function(dateText, inst){
               $("#to_app_date-custom-filter").datepicker("option","minDate",
               $("#from_app_date-custom-filter").datepicker("getDate"));
               jQuery( "#to_app_date-custom-filter" ).attr('disabled',false);
            }
            });

        jQuery( "#to_app_date-custom-filter" ).datepicker({ 
            dateFormat: 'yy-mm-dd',
            changeYear: true,
            changeMonth: true,
           
            });
  }
  </script>
<script type="text/javascript"> 
    
     function deleteRow(obj)
      {
        event.preventDefault(); // prevent form submit
        swal({
          title: "Are you sure?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          cancelButtonText: "No",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm){
          if (isConfirm) {        
           obj.submit();

          } else {
             swal.close();
          }
        });
      }

    $(function() {
        var table = $('#user_datatable').DataTable({
           stateSave: true,
        processing: true,
        serverSide: true,
        order: [[0, "desc" ]],
        "ajax":{
        "url": '{!! route('orders.getdataHistory') !!}',
                "dataType": "json",
                "type": "POST",
                data: function(d) {
                    d._token= "{{csrf_token()}}";
                    d.from_appointment_date = $('#from_app_date-custom-filter').val();
                    d.to_appointment_date = $('#to_app_date-custom-filter').val();
                    d.sp_id = $('#custom-filter').val();
                }
        },
        columns: [
        { data: 'store_name', name: 'store_name', orderable:true },
        { data: 'payment_transaction_id', name: 'payment_transaction_id', orderable:true },
        { data: 'paid_by', name: 'paid_by', orderable:true },
        { data: 'payment_mode', name: 'payment_mode', orderable:true },
        { data: 'total_amount', name: 'total_amount', orderable:true },
        { data: 'payment_date', name: 'payment_date', orderable:true },
        { data: 'created_at', name: 'created_at', orderable:true },
        { data: 'action', name: 'action', orderable:true  },
        ],
        "columnDefs": [
        { "searchable": false, "targets": 0 }
        ]
        , language: {
        searchPlaceholder: "Search by name & mobile"
        },
        initComplete:function()
        {
          loadStatusFilter();
        }
});
});  


</script>
<style>
  .dataTables_filter {
display: none;
}
</style>style>
@endsection

