@extends('layouts.admin.admin')
@section('uniquecss')

<style type="text/css">
    table.owntable th {
        background-color: #337ab7;
        color: #fff;
    }
</style>

@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>View E-Card</h5>

            </header>
            <div id="collapse2" class="body">
              <div class="row">
                 @include('message')
                <div class="col-sm-12 col-lg-12">

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Card Unique ID</b></div>
                        <div class="col-lg-8"><?= $data['card_unique_id']; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Occasion</b></div>
                        <div class="col-lg-8"><?= $data['occasion']; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Theme</b></div>
                        <div class="col-lg-8"><?= $data['theme']; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Sender</b></div>
                        <div class="col-lg-8"><?= $data['sender']; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Receiver</b></div>
                        <div class="col-lg-8"><?= $data['receiver']; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Receiver Message</b></div>
                        <div class="col-lg-8"><?= $data['receiver_message']; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Amount</b></div>
                        <div class="col-lg-8"><?= $data['amount']; ?></div> <div class="clearfix"></div>
                    </div>
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Payment Mode</b></div>
                        <div class="col-lg-8"><?= $data['payment_details']; ?></div> <div class="clearfix"></div>
                    </div>

                    <?php if($data['card_send_date']){ ?>
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Received Date</b></div>
                        <div class="col-lg-8"><?= $data['card_send_date']; ?></div> <div class="clearfix"></div>
                    </div>
                    <?php } ?>
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Status</b></div>
                        <div class="col-lg-8"><?= $data['status']; ?></div> <div class="clearfix"></div>
                    </div>
                    <?php if($data['status'] == 'Cancelled'){ ?>
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Cancelled On</b></div>
                        <div class="col-lg-8"><?= $data['cancelled_date_time']; ?></div> 
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Card Refund Amount</b></div>
                        <div class="col-lg-8"><?= $data['cancellation_refunded_amount']; ?></div> <div class="clearfix"></div>
                    </div>
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Card Cancellation Reason</b></div>
                        <div class="col-lg-8"><?= $data['cancellation_reason']; ?></div> <div class="clearfix"></div>
                    </div>
                    <?php } ?>
                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Card Gifted</b></div>
                        <div class="col-lg-8"><?= $data['created_at']; ?></div> <div class="clearfix"></div>
                    </div>

                  <div class="clearfix"></div><br><br>
                </div>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>

@endsection