@extends('layouts.admin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
 
@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>E-Cards</h5>
            </header>
            <div id="collapse4" class="body">
              @include('message')
              
               <table id="user_datatable" class="table table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Card Unique ID</th>
                        <!-- <th>Occasion</th> -->
                        <th>Sender</th>
                        <th>Receiver</th>
                        <th>Amount</th>
                        <th>Status</th>
                        <th>Card Gifted</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
                  
            </div>
        </div>
    </div>
</div>
@endsection

@section('uniquescript')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>
<script type="text/javascript">
  function callNative(){

    $("#user_datatable").DataTable().draw();
  }
  function loadStatusFilter()
  {

    jQuery("#user_datatable_filter").parent().parent().
      append("<div id='user_datatable_category_filter' class='col-sm-12 col-md-2'><label>Occasions:</label><select name='columns[testtest]' class='form-control input-sm' id='custom-filter' onchange='callNative()''><option value=''>All Categories</option><?php foreach($occasions as $key => $cat){ ?><option value='<?= $key?>''><?= $cat?></option><?php } ?></select></div><div id='user_datatable_status_filter' class='col-sm-12 col-md-2'><label>Status:</label><select name='columns[testtest]' class='form-control input-sm' id='status-filter' onchange='callNative()''><option value=''>All status</option><option value='pending'>Pending</option><option value='received'>Received</option><option value='cancelled'>Cancelled</option></select></div>");
  }
  </script>

<script type="text/javascript">

  function deleteRow(obj)
  {
    event.preventDefault(); // prevent form submit
    swal({
      title: "Are you sure?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {        
       obj.submit();

      } else {
         swal.close();
      }
    });
  }
 
    $(function() {
        var table = $('#user_datatable').DataTable({
          stateSave: true,
        processing: true,
        serverSide: true,
        order: [[0, "desc" ]],
        "ajax":{
        "url": '{!! route('e-card.getdata') !!}',
                "dataType": "json",
                "type": "POST",
                data: function(d) {
                    d._token= "{{csrf_token()}}";
                    d.category_filter = $('#custom-filter').val();
                    d.status_filter = $('#status-filter').val();
                }
        },
        columns: [
        { data: 'id', name: 'id', orderable:true },
        { data: 'card_unique_id', name: 'card_unique_id', orderable:true },
        // { data: 'occasion', name: 'occasion', orderable:false },
        { data: 'sender', name: 'sender', orderable:false },
        { data: 'receiver', name: 'receiver', orderable:false },
        { data: 'amount', name: 'amount', orderable:false },
        { data: 'status', name: 'status', orderable:false },
        { data: 'created_at', name: 'created_at', orderable:false },
        { data: 'action', name: 'action', orderable:true  },
        ],
        "columnDefs": [
        { "searchable": false, "targets": 0 }
        ]
        , language: {
        searchPlaceholder: "Search here"
        },
        initComplete:function()
        {
          loadStatusFilter();
        },
});
});  


</script>
@endsection

