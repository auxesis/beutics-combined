@extends('layouts.admin.admin')

@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/jquery.gritter/css/jquery.gritter.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/themes/default/css/uniform.default.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.min.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>{{$title}}</h5>
          

            </header>
             @include('message')
            <div id="collapse2" class="body">
                <!-- <form class="form-horizontal" id="popup-validation"> -->
                {!! Form::open(['route' => 'banners.store', 'method' => 'POST', 'class' => 'form-horizontal','id' => 'popup-validation','enctype' => 'multipart/form-data']) !!}
                {{ csrf_field() }}

                    <div class="form-group">
                        {{Form::label('page_name', 'Page Name', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::select('page_name', $page_name_array,null,['class' => 'form-control validate[required] myselect', 'id'=>"page_name",'placeholder' => 'Select Page Name']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('page_section_name', __('messages.Section Name'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::select('page_section_name', $all_page_sections,null,['class' => 'form-control validate[required] myselectTags']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('title', __('messages.Title'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('title','', ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('sub_title', __('messages.Sub Title'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('sub_title','', ['class' => 'form-control']) }}
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-lg-4">{{ __('messages.Image') }}</label>
                        <div class="row">
                            <div class="col-md-4">
                                <div id="ImgView">
                                    <img src="">
                                </div>
                                <div id="upload-demo" style=" display: none;"></div>
                            
                                <input class="form-control" type="file" id="image" onclick="showHideDiv(this)" name="image" accept="image/*">
                                <?= Form::hidden('image_cr',null, ['id' => 'image_cr']) ?>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        {{Form::label('description', __('messages.Description'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::textarea('description','', ['class' => 'form-control validate[required] summary-ckeditor','rows'=>'2']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('sub_description', __('messages.Sub Description'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::textarea('sub_description','', ['class' => 'form-control summary-ckeditor','rows'=>'2']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('link', __('messages.Link'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('link','', ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('sort_order', __('messages.Order'), ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('sort_order','', ['class' => 'form-control','onkeypress'=>'return isNumber(event)']) }}
                        </div>
                    </div>
                    <div class="form-actions no-margin-bottom">
                        <input type="submit" value="{{ __('messages.Submit') }}" class="btn btn-primary">
                    </div>
                    <br><br><br><br>
                </form>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
@endsection
@section('uniquescript')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/holder/2.4.1/holder.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/jquery.uniform.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script src="{{ url('/') }}/assets/lib/ckeditor/ckeditor.js"></script>
    <script type="text/javascript">
        function showHideDiv()
        {
            $('#ImgView').hide();
            $('#upload-demo').show();
        }
        $(function() {
           $(".myselectTags").select2({
            tags: true,
            closeOnSelect : false,
            allowHtml: true,
            allowClear: true,
            placeholder : 'Select Section Name'
           });
        });
    </script>
    <script>
        CKEDITOR.replace( 'description' );
        CKEDITOR.replace( 'sub_description' );
        $(function() {
          Metis.formValidation();
        });

        function isNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        $('#popup-validation').submit(function(e){
            e.preventDefault();
            obj = $(this);
            resize.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (img) {
                $('#image_cr').val(img);
               obj.unbind('submit').submit();
            });
        });

        // image crop
        var resize = $('#upload-demo').croppie({
            enableExif: true,
            enableOrientation: true,    
            viewport: { // Default { width: 100, height: 100, type: 'square' } 
                width: 200,
                height: 200,
                type: 'square' //square
            },
            boundary: {
                width: 300,
                height: 300
            }
        });

        $('#image').on('change', function () { 
          var reader = new FileReader();
            reader.onload = function (e) {
              resize.croppie('bind',{
                url: e.target.result
              }).then(function(){
                console.log('jQuery bind complete');
              });
            }
            reader.readAsDataURL(this.files[0]);
        });
        // 
    </script>


@endsection