@extends('layouts.admin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
 
@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>{{ __('messages.Banner table') }}</h5>
                <a class="btn btn-primary pull-right" href="{{route('banners.create')}}" style="margin-top:4px;">{{ __('messages.Create Banner') }}</a>
            </header>
            <div id="collapse4" class="body">
              @include('message')
              
               <table id="user_datatable" class="table table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>{{ __('messages.Page Name') }}</th>
                        <th>{{ __('messages.Section Name') }}</th>
                        <th>{{ __('messages.Title') }}</th>
                        <th>{{ __('messages.Sub Title') }}</th>
                        <th>{{ __('messages.Image') }}</th>
                        <th>{{ __('messages.Order') }}</th>
                        <th>{{ __('messages.Status') }}</th>
                        <th>{{ __('messages.Action') }}</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
                  
            </div>
        </div>
    </div>
</div>
@endsection

@section('uniquescript')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>
<script type="text/javascript">

  function deleteRow(obj)
  {
    event.preventDefault(); // prevent form submit
    swal({
      title: "{{ __('messages.Are you sure?') }}",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {        
       obj.submit();

      } else {
         swal.close();
      }
    });
  }
  
  function changeStatus(id)
  {
    swal({
        title: "Are you sure?",
        type: "warning",
        showCancelButton: "No",
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
      },
        function(){
          jQuery.ajax({
          url: '{{route('banners.status.update')}}',
          type: 'POST',
          data:{banner_id:id},
          headers: {
          'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
          },
          success: function (response) {
            $("#status_"+id).html(response);
          }
          });
        }
    );
  }
    $(function() {
        var table = $('#user_datatable').DataTable({
           stateSave: true,
        processing: true,
        serverSide: true,
        order: [[0, "desc" ]],
        "ajax":{
        "url": '{!! route('banners.getdata') !!}',
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{csrf_token()}}"}
        },
        columns: [
        { data: 'page_name', name: 'page_name', orderable:true },
        { data: 'page_section_name', name: 'page_section_name', orderable:true },
        { data: 'title', name: 'title', orderable:true },
        { data: 'sub_title', name: 'sub_title', orderable:true },
        { data: 'image', name: 'image', orderable:true },
        { data: 'sort_order', name: 'sort_order', orderable:true },
        { data: 'status', name: 'status', orderable:true },
        { data: 'action', name: 'action', orderable:true  },
        ],
        "columnDefs": [
        { "searchable": false, "targets": 0 }
        ]
        , language: {
        searchPlaceholder: "{{ __('messages.Search by page name and title') }}"
        },
});
});  


</script>
@endsection

