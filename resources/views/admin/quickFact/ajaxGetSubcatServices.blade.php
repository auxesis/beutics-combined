@if($service_arr)
{{ Form::select('service_id', $service_arr,null,['class' => 'form-control validate[required]','placeholder'=>'Select Service','id'=>'services']) }}
@else
<h5 style="color:red;">Service not found</h5>
@endif

<script type="text/javascript">
	$("#services").change(function()
	{
	    var id = $(this).val();
	    jQuery.ajax({
	    url: '{{route('get-quickFact-sub-services')}}',
	    type: 'POST',
	    data:{service_id:id},
	    headers: {
	    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
	    },
	    success: function (response) {
	      $("#sub_service_div").html(response);
	      //console.log(response);
	    }
	    });
	      
	});

</script>