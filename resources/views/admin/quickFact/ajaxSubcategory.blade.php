@if($cat_arr)
{{ Form::select('subcategory_id', $cat_arr,null,['class' => 'form-control validate[required]','placeholder'=>'Select Sub Categories','id'=>'subcategory']) }}
@else
<h5 style="color:red;">Subcategory not found</h5>
@endif

<script type="text/javascript">
	$("#subcategory").change(function()
	{
	    var id = $(this).val();
	    jQuery.ajax({
	    url: '{{route('get-quickFact-services')}}',
	    type: 'POST',
	    data:{subcat_id:id},
	    headers: {
	    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
	    },
	    success: function (response) {
	      $("#service_div").html(response);
	      //console.log(response);
	    }
	    });
	      
	});

</script>