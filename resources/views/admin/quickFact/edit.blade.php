@extends('layouts.admin.admin')

@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/jquery.gritter/css/jquery.gritter.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/themes/default/css/uniform.default.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">

@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>{{$title}}</h5>
          

            </header>
             @include('message')
            <div id="collapse2" class="body">
                <!-- <form class="form-horizontal" id="popup-validation"> -->
                {!! Form::model($row,['route' => ['quickFact.update',$row->id], 'method' => 'PATCH', 'class' => 'form-horizontal','id' => 'popup-validation','enctype' => 'multipart/form-data']) !!}
                {{ csrf_field() }}
 
                    <div class="form-group">
                        {{Form::label('category_name', 'Category Name', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::select('category_id', $categories,null,['class' => 'form-control validate[required]','id'=>'category','placeholder'=>'Select Categories','onchange'=>'getSubcategory(this)']) }}
                        </div>
                    </div>

                     <div class="form-group">
                        {{Form::label('', 'Sub Category Name', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4" id="subcat_div">
                            {{ Form::select('subcategory_id', $subcat_arr,null,['class' => 'form-control validate[required]','placeholder'=>'Select Sub Categories','id'=>'subcategory']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('', 'Service Name', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4" id="service_div">
                            {{ Form::select('service_id', $service_arr,null,['class' => 'form-control validate[required]','placeholder'=>'Select Service', 'id'=>'services']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('', 'Sub Service Name', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4" id="sub_service_div">
                            {{ Form::select('subservice_id', $sub_service_arr,null,['class' => 'form-control','placeholder'=>'Select Sub Service', 'id'=>'subservices']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('title', 'Title', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('title',null, ['class' => 'form-control validate[required]']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('title_ar', 'Title(Arabic)', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('title_ar',null, ['class' => 'form-control validate[required]']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('description', 'Description', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::textarea('description',null, ['class' => 'form-control validate[required] summary-ckeditor','rows'=>'2']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('description_ar', 'Description(Arabic)', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::textarea('description_ar',null, ['class' => 'form-control validate[required] summary-ckeditor','rows'=>'2']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-4">Add Image</label>
                        <div class="col-lg-3">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                      @if($row->image!='')
                                      <img src="{{asset('sp_uploads/quick_facts/'.$row->image)}}" alt="...">
                                      @endif
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                    <div>
                                      <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="image"></span>
                                      <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                    </div>
                                </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-4">Banner Images</label>
                        <div class="col-lg-8">

                        <div class="row">
                            <div class="col-md-10">
                                <div class="row">
                                     <div class="col-md-6"><div class="input_fields_container">
                                       <input type="file" name="banner_path[]" accept= "image/*" /></div></div>
                                      <div class="col-md-6">
                                          <button class="btn btn-sm btn-primary add_more_button" style="margin-bottom:10px">Add More Images</button>
                                      </div>
                                </div>
                               
                            </div>
                        </div>
                        
                         <!-- banner div -->
                        <div id="more_image" style="display: none;">
                            <div class="row" style="margin-top: 10px;">
                            <div class="col-md-10">
                                <input type="file" name="banner_path[]"  accept= "image/*" />
                            </div>
                            <div  class="col-md-2">
                                <a href="#" class="remove_field" style="margin-bottom:10px;">Remove</a>
                            </div>
                        </div>
                        </div>
                        <!--  -->
                        @foreach($banner_images as $ban_img)
                        <div class="form-group disp-inline" >
                            <img src="{{asset('sp_uploads/quick_facts/'.$ban_img['banner_path'])}}" width="100" height="100" class="banner_img<?= $ban_img->id; ?>"/>
                            <a onclick="deleteBannerImage('<?= $ban_img->id; ?>');" class="banner_img<?= $ban_img->id; ?>">Delete</a>
                        </div>
                        @endforeach
                        </div>
                    </div>

                    <div class="form-actions no-margin-bottom">
                        <input type="submit" value="Submit" class="btn btn-primary">
                    </div>
                    <br><br><br><br>
                </form>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
@endsection
@section('uniquescript')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>

     <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/holder/2.4.1/holder.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/jquery.uniform.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>
    <script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>
       <script src="{{ url('/') }}/assets/lib/ckeditor/ckeditor.js"></script>
    <script>
      CKEDITOR.replace( 'description' );
      CKEDITOR.replace( 'description_ar' );
    </script>
    <script>
        $(function() {
          Metis.formValidation();
        });
    </script>

    <script type="text/javascript">

        // $(document).ready(function(){
        //     var obj = document.getElementById('category');
        //     getSubcategory(obj);
        // });


        function getSubcategory(category_id)
        {
            var id = category_id.value;
            jQuery.ajax({
            url: '{{route('get-quickFact-subcategory')}}',
            type: 'POST',
            data:{cat_id:id},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
              $("#subcat_div").html(response);
              //console.log(response);
            }
            });
                
        }
        

        function deleteBannerImage(id)
        {

          swal({
              title: "Are you sure?",
              type: "warning",
              showCancelButton: "No",
              confirmButtonClass: "btn-danger",
              confirmButtonText: "Yes",
              cancelButtonText: "No",
            },
              function(){
                jQuery.ajax({
                url: '{{route('quickfact.banner-image-delete')}}',
                type: 'POST',
                data:{banner_id:id},
                headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    
                    if(response == 'success'){
                        $(".banner_img"+id).hide();
                    }
                }
                });
              }
          );
        }

        $(document).ready(function() {
            var max_fields_limit = 5; //maximum input boxes allowed
            
         
            var x = 1; //initlal text box count
            $('.add_more_button').click(function(e){ //on add input button click
                e.preventDefault();
                $('.limit_error').remove();
                if(x < max_fields_limit){ //max input box allowed
                    x++; //text box increment
                    $('.input_fields_container').append($("#more_image").html()); //add input box
                }
                else
                {
                  $(this).after('<span class="limit_error" style="color:red;">You have reached the limit of images uploaded.</span>');
                }
            });
            
            $('.input_fields_container').on("click",".remove_field", function(e){ //user click on remove text
                e.preventDefault(); $(this).parent().parent('div').remove(); x--;
                  $('.limit_error').remove();
            })
        });

        
        
    </script>
@endsection