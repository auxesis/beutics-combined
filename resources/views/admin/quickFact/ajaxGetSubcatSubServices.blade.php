@if($sub_service_arr)
{{ Form::select('subservice_id', $sub_service_arr,null,['class' => 'form-control validate[required]','placeholder'=>'Select Sub Service','id'=>'subservices']) }}
@else
<h5 style="color:red;">Sub Service not found</h5>
@endif