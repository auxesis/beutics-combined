@extends('layouts.admin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/datepicker/jquery-ui.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css">
  <style>
  	#user_datatable_filter 
	{
	    display:none;
	}

  </style>


@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box table-responsive">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>{{$title}}</h5>
            </header>
            <div id="collapse4" class="body">
              @include('message')
                              

               <table id="user_datatable" class="display nowrap table table-hover table-striped table-bordered dataTable no-footer" style="width:100%;">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Order ID</th>
                        <th>Date</th>
                        <th>Booking Date</th>
                        <th>Customer</th>
                        <th>Store</th>
                        <th>Tier of the store</th>	
                        <th>Item Purchased</th>
                        <th>Buy Price (Total)</th>
                        <th>DP (Total)</th>
                        <th>Status</th>
                        <th>Gift Service</th>
                        <th>Gift To (customer name)</th>
                        <th>Gift To (mobile no)</th>
                        <th>Order Closed Date</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
                  
            </div>
        </div>
    </div>
</div>
@endsection

@section('uniquescript')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>
<script src="{{ url('/') }}/assets/lib/datepicker/jquery-ui.js"></script>
<script type="text/javascript">
  function callNative(){

    $("#user_datatable").DataTable().draw();
  }
  function loadStatusFilter()
  {
  	var sdate = $("#reg_sdate-custom-filter").val();
  	var edate = $("#reg_edate-custom-filter").val();
    jQuery("#user_datatable_filter").parent().parent().
       append('<form method="get" action="<?php echo route('export.order'); ?>"><input type="submit" value="Export Report" class="btn btn-primary" style="float: right;margin: -50px 21px;"><div id="user_datatable_reg_sdate" class="col-sm-12 col-md-2" ><label>Signup Start Date:</label><input type="text" name="reg_sdate" class="form-control" id="reg_sdate-custom-filter" onchange="callNative()" autocomplete="off"></div><div id="user_datatable_reg_edate" class="col-sm-12 col-md-2"><label>Signup End Date:</label><input type="text" name="reg_edate" class="form-control" id="reg_edate-custom-filter" onchange="callNative()" autocomplete="off"></div><br/><br/><form><br><br>');

    jQuery('#user_datatable_length').parent().closest("div.col-sm-12").removeClass("col-md-12");  
    jQuery('#user_datatable_length').parent().closest("div.col-sm-12").addClass("col-md-12");  
    jQuery('#user_datatable_filter').parent().closest("div.col-sm-12").removeClass("col-md-12");  
    jQuery('#user_datatable_filter').parent().closest("div.col-sm-12").addClass("col-md-12");

    jQuery('#reg_sdate-custom-filter').addClass("datepicker");
    jQuery('#reg_edate-custom-filter').addClass("datepicker");
    // jQuery( ".datepicker" ).datepicker({ 
    //     dateFormat: 'yy-mm-dd',
    //     changeYear: true,
    //     changeMonth: true,
    //     // minDate: 0,
    //     });

    jQuery( "#reg_sdate-custom-filter" ).datepicker({ 
        dateFormat: 'yy-mm-dd',
        changeYear: true,
        changeMonth: true,
        // minDate: 0,
        onSelect: function(dateText, inst){
             jQuery("#reg_edate-custom-filter").datepicker("option","minDate",
             jQuery("#reg_sdate-custom-filter").datepicker("getDate"));
          }
        });

       jQuery( "#reg_edate-custom-filter" ).datepicker({ 
        dateFormat: 'yy-mm-dd',
        changeYear: true,
        changeMonth: true,
        });
  }
  </script>
<script type="text/javascript"> 
    

    function deleteRow(obj)
      {
        event.preventDefault(); // prevent form submit
        swal({
          title: "Are you sure?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          cancelButtonText: "No",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm){
          if (isConfirm) {        
           obj.submit();

          } else {
             swal.close();
          }
        });
      }
  
    $(function() {
        var table = $('#user_datatable').DataTable({

            stateSave: true,
        processing: true,
        serverSide: true,

        order: [[0, "desc" ]],
        "ajax":{
        "url": '{!! route('rpt.order.getdata') !!}',
                "dataType": "json",
                "type": "POST",
                data: function(d) {
                    d._token= "{{csrf_token()}}";
                    d.reg_sdate = $('#reg_sdate-custom-filter').val();
                    d.reg_edate = $('#reg_edate-custom-filter').val();
                }
        },
        columns: [
        { data: 'sno', name: 'sno', orderable:true },
        { data: 'order_id', name: 'order_id', orderable:true },
        { data: 'created_at', name: 'created_at', orderable:true },
        { data: 'appointment_date', name: 'appointment_date', orderable:true },
        { data: 'user_id', name: 'user_id', orderable:true },
        { data: 'sp_id', name: 'sp_id', orderable:true },
        { data: 'tier_id', name: 'tier_id', orderable:true },
        { data: 'items', name: 'items', orderable:true },
        { data: 'original_price', name: 'original_price', orderable:true },
        { data: 'best_price', name: 'best_price', orderable:true },
        { data: 'status', name: 'status', orderable:true },
        { data: 'gift_status', name: 'gift_status', orderable:true },
        { data: 'gift_user_id', name: 'gift_user_id', orderable:true },
        { data: 'gift_mobile_no', name: 'gift_mobile_no', orderable:true },
        { data: 'completed_date', name: 'completed_date', orderable:true },
        ],
        "columnDefs": [
        { "searchable": false, "targets": 0 }
        ]
        , language: {
        searchPlaceholder: "Search by name"
        },
        initComplete:function()
        {
          loadStatusFilter();
        }
});
});  


</script>
@endsection

