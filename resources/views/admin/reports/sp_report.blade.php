@extends('layouts.admin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/datepicker/jquery-ui.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css">
  <style>
  	#user_datatable_filter 
	{
	    display:none;
	}

  </style>


@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box table-responsive">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>{{$title}}</h5>
            </header>
            <div id="collapse4" class="body">
              @include('message')
                     
                            

               <table id="user_datatable" class="display nowrap table table-hover table-striped table-bordered dataTable no-footer" style="width:100%;">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Category</th>
                        <th>Store Name</th>
                        <th>Tier in %</th>
                        <th>Location</th>
                        <th>Contact Name</th>	
                        <th>Store No</th> 
                        <th>Mobile No</th>
                        <th>Whatsapp No</th>
                        <th>Verified Status</th>
                        <th>Referral Code</th>
                        <th># of Referral Code</th>
                        <th>HS</th>
                        <th>MOV</th>
                        <th>Locations Served</th>
                        <th>Store Active Status</th>
                        <th>Review Received</th>
                        <th>VAT</th>
                        <th>Forum Activity</th>
                        <th>Store Service Count</th>
                        <th>HS Service Count</th>
                        <th>Store Offer Unique Numbers</th>
                        <th>HS Offer Unique Numbers</th>
                        <th>Sign Up Date & Time</th>
                        <!-- <th>Store Approval Date & Time</th> -->
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
                  
            </div>
        </div>
    </div>
</div>
@endsection

@section('uniquescript')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>
<script src="{{ url('/') }}/assets/lib/datepicker/jquery-ui.js"></script>
<script type="text/javascript">
  function callNative(){

    $("#user_datatable").DataTable().draw();
  }
  function loadStatusFilter()
  {
  	var sdate = $("#reg_sdate-custom-filter").val();
  	var edate = $("#reg_edate-custom-filter").val();
    jQuery("#user_datatable_filter").parent().parent().
      append('<form method="get" action="<?php echo route('export.sp'); ?>"><input type="submit" value="Export Report" class="btn btn-primary" style="float: right;margin: -50px 21px;"><div id="user_datatable_reg_sdate" class="col-sm-12 col-md-2" ><label>Signup Start Date:</label><input type="text" name="reg_sdate" class="form-control" id="reg_sdate-custom-filter" onchange="callNative()" autocomplete="off"></div><div id="user_datatable_reg_edate" class="col-sm-12 col-md-2"><label>Signup End Date:</label><input type="text" name="reg_edate" class="form-control" id="reg_edate-custom-filter" onchange="callNative()" autocomplete="off"></div><br/><br/><form><br><br>');

    jQuery('#user_datatable_length').parent().closest("div.col-sm-12").removeClass("col-md-12");  
    jQuery('#user_datatable_length').parent().closest("div.col-sm-12").addClass("col-md-12");  
    jQuery('#user_datatable_filter').parent().closest("div.col-sm-12").removeClass("col-md-12");  
    jQuery('#user_datatable_filter').parent().closest("div.col-sm-12").addClass("col-md-12");

    jQuery('#reg_sdate-custom-filter').addClass("datepicker");
    jQuery('#reg_edate-custom-filter').addClass("datepicker");
    // jQuery( ".datepicker" ).datepicker({ 
    //     dateFormat: 'yy-mm-dd',
    //     changeYear: true,
    //     changeMonth: true,
    //     // minDate: 0,
    //     });

    jQuery( "#reg_sdate-custom-filter" ).datepicker({ 
        dateFormat: 'yy-mm-dd',
        changeYear: true,
        changeMonth: true,
        // minDate: 0,
        onSelect: function(dateText, inst){
             jQuery("#reg_edate-custom-filter").datepicker("option","minDate",
             jQuery("#reg_sdate-custom-filter").datepicker("getDate"));
          }
        });

       jQuery( "#reg_edate-custom-filter" ).datepicker({ 
        dateFormat: 'yy-mm-dd',
        changeYear: true,
        changeMonth: true,
        });
  }
  </script>
<script type="text/javascript"> 
    

    function deleteRow(obj)
      {
        event.preventDefault(); // prevent form submit
        swal({
          title: "Are you sure?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          cancelButtonText: "No",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm){
          if (isConfirm) {        
           obj.submit();

          } else {
             swal.close();
          }
        });
      }
  
    $(function() {
        var table = $('#user_datatable').DataTable({

            stateSave: true,
        processing: true,
        serverSide: true,

        order: [[0, "desc" ]],
        "ajax":{
        "url": '{!! route('rpt.sp.getdata') !!}',
                "dataType": "json",
                "type": "POST",
                data: function(d) {
                    d._token= "{{csrf_token()}}";
                    d.reg_sdate = $('#reg_sdate-custom-filter').val();
                    d.reg_edate = $('#reg_edate-custom-filter').val();
                }
        },
        columns: [
        { data: 'sno', name: 'sno', orderable:false },
        { data: 'category_id', name: 'category_id', orderable:false },
        { data: 'store_name', name: 'store_name', orderable:false },
        { data: 'tier', name: 'tier', orderable:false },
        { data: 'location', name: 'location', orderable:false },
        { data: 'contact_name', name: 'contact_name', orderable:false },
        { data: 'store_no', name: 'store_no', orderable:false },
        { data: 'mobile_no', name: 'mobile_no', orderable:false },
        { data: 'whatsapp_no', name: 'whatsapp_no', orderable:false },
        { data: 'verified_status', name: 'total_refer_earning', orderable:false },
        { data: 'referral_code', name: 'referral_code', orderable:false },
        { data: 'referral_count', name: 'referral_count', orderable:false },
        { data: 'hs', name: 'hs', orderable:false },
        { data: 'mov', name: 'mov', orderable:false },
        { data: 'location_serve', name: 'location_serve', orderable:false },
        { data: 'store_active_status', name: 'store_active_status', orderable:false },
        { data: 'review_reviewd', name: 'review_reviewd', orderable:false },
        { data: 'vat', name: 'vat', orderable:false },
        { data: 'forum_activity', name: 'forum_activity', orderable:false },
        { data: 'store_service_count', name: 'store_service_count', orderable:false },
        { data: 'hs_service_count', name: 'hs_service_count', orderable:false },
        { data: 'store_offers', name: 'store_offers', orderable:false },
        { data: 'home_offers', name: 'home_offers', orderable:false },
        { data: 'sp_date_time', name: 'sp_date_time', orderable:false },
        // { data: 'approval_date_time', name: 'approval_date_time', orderable:false },
        // 
                
        // { data: 'referral_count', name: 'referral_count', orderable:true },
        ],
        "columnDefs": [
        { "searchable": false, "targets": 0 }
        ]
        , language: {
        searchPlaceholder: "Search by name"
        },
        initComplete:function()
        {
          loadStatusFilter();
        }
});
});  


</script>
@endsection

