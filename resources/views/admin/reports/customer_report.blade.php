@extends('layouts.admin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/datepicker/jquery-ui.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css">
  <style>
  	#user_datatable_filter 
	{
	    display:none;
	}

  </style>


@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box table-responsive">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>{{$title}}</h5>
            </header>
            <div id="collapse4" class="body">
              @include('message')
                     
                            

               <table id="user_datatable" class="display nowrap table table-hover table-striped table-bordered dataTable no-footer" style="width:100%;">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Name</th>
                        <th>Country Code</th>
                        <th>Contact No</th>
                        <th>Email</th>
                        <th>Date of Signup</th>	
                        <th>Time of Signup</th> 
                        <th># of referrals</th>
                        <th>Referral Type</th>
                        <th>Refer & Earning</th>
                        <th>Cashback Earning</th>
                        <th>Redeemed</th>
                        <th>My Cash</th>
                        <th>Total Forum <br/>Comments</th>
                        <th>Total Ecard <br/>Purchased</th>
                        <th>Gift Purchased</th>
                        <th>Total Reviews</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
                  
            </div>
        </div>
    </div>
</div>
@endsection

@section('uniquescript')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>
<script src="{{ url('/') }}/assets/lib/datepicker/jquery-ui.js"></script>
<script type="text/javascript">
  function callNative(){

    $("#user_datatable").DataTable().draw();
  }
  function loadStatusFilter()
  {
  	var sdate = $("#reg_sdate-custom-filter").val();
  	var edate = $("#reg_edate-custom-filter").val();
    jQuery("#user_datatable_filter").parent().parent().
      append('<form method="get" action="<?php echo route('export.customers'); ?>"><input type="submit" value="Export Report" class="btn btn-primary" style="float: right;margin: -50px 21px;"><div id="user_datatable_reg_sdate" class="col-sm-12 col-md-2" ><label>Signup Start Date:</label><input type="text" name="reg_sdate" class="form-control" id="reg_sdate-custom-filter" onchange="callNative()" autocomplete="off"></div><div id="user_datatable_reg_edate" class="col-sm-12 col-md-2"><label>Signup End Date:</label><input type="text" name="reg_edate" class="form-control" id="reg_edate-custom-filter" onchange="callNative()" autocomplete="off"></div><br/><br/><form><br><br>');

    jQuery('#user_datatable_length').parent().closest("div.col-sm-12").removeClass("col-md-12");  
    jQuery('#user_datatable_length').parent().closest("div.col-sm-12").addClass("col-md-12");  
    jQuery('#user_datatable_filter').parent().closest("div.col-sm-12").removeClass("col-md-12");  
    jQuery('#user_datatable_filter').parent().closest("div.col-sm-12").addClass("col-md-12");

    jQuery('#reg_sdate-custom-filter').addClass("datepicker");
    jQuery('#reg_edate-custom-filter').addClass("datepicker");
    // jQuery( ".datepicker" ).datepicker({ 
    //     dateFormat: 'yy-mm-dd',
    //     changeYear: true,
    //     changeMonth: true,
    //     // minDate: 0,
    //     });

    jQuery( "#reg_sdate-custom-filter" ).datepicker({ 
        dateFormat: 'yy-mm-dd',
        changeYear: true,
        changeMonth: true,
        // minDate: 0,
        onSelect: function(dateText, inst){
             jQuery("#reg_edate-custom-filter").datepicker("option","minDate",
             jQuery("#reg_sdate-custom-filter").datepicker("getDate"));
          }
        });

       jQuery( "#reg_edate-custom-filter" ).datepicker({ 
        dateFormat: 'yy-mm-dd',
        changeYear: true,
        changeMonth: true,
        });
  }
  </script>
<script type="text/javascript"> 
    

    function deleteRow(obj)
      {
        event.preventDefault(); // prevent form submit
        swal({
          title: "Are you sure?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          cancelButtonText: "No",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm){
          if (isConfirm) {        
           obj.submit();

          } else {
             swal.close();
          }
        });
      }
  
    $(function() {
        var table = $('#user_datatable').DataTable({

            stateSave: true,
        processing: true,
        serverSide: true,

        order: [[0, "desc" ]],
        "ajax":{
        "url": '{!! route('rpt.customer.getdata') !!}',
                "dataType": "json",
                "type": "POST",
                data: function(d) {
                    d._token= "{{csrf_token()}}";
                    d.reg_sdate = $('#reg_sdate-custom-filter').val();
                    d.reg_edate = $('#reg_edate-custom-filter').val();
                }
        },
        columns: [
        { data: 'sno', name: 'sno', orderable:false },
        { data: 'name', name: 'name', orderable:false },
        { data: 'country_code', name: 'country_code', orderable:false },
        { data: 'mobile_no', name: 'mobile_no', orderable:false },
        { data: 'email', name: 'email', orderable:false },
        { data: 'created_at', name: 'created_at', orderable:false },
        { data: 'created_time', name: 'created_time', orderable:false },
        { data: 'referral_count', name: 'referral_count', orderable:false },
        { data: 'referral_type', name: 'referral_type', orderable:false },
        { data: 'total_refer_earning', name: 'total_refer_earning', orderable:false },
        { data: 'total_cashback', name: 'total_cashback', orderable:false },
        { data: 'redemption', name: 'redemption', orderable:false },
        { data: 'my_cash', name: 'my_cash', orderable:false },
        { data: 'total_forum_comments', name: 'total_forum_comments', orderable:false },
        { data: 'ecount', name: 'ecount', orderable:false },
        { data: 'gifts', name: 'gifts', orderable:false },
        { data: 'reviewsCount', name: 'reviewsCount', orderable:false },
        // 
        
        
        
        
        
        
        // { data: 'referral_count', name: 'referral_count', orderable:true },
        ],
        "columnDefs": [
        { "searchable": false, "targets": 0 }
        ]
        , language: {
        searchPlaceholder: "Search by name"
        },
        initComplete:function()
        {
          loadStatusFilter();
        }
});
});  


</script>
@endsection

