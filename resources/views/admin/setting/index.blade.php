@extends('layouts.admin.admin')
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>Setting</h5>
                 <a class="btn btn-primary pull-right" href="{{route('setting.edit',$row->id)}}" style="margin-top:4px;">Update Setting</a>
            </header>
            <div id="collapse4" class="body">
              @include('message')
              
               <table  class="table table-bordered" >
                <tbody>
                    <tr>
                        <th width="50%">Tax Percentage</th>
                        <td width="50%"><?= $row['percentage']; ?></td>
                    </tr>
                    <tr>
                        <th width="50%">Referred Cashback</th>
                        <td width="50%"><?= $row['referred_cashback'].' AED'; ?></td>
                    </tr>
                    <tr>
                        <th width="50%">Contact Sync Cashback</th>
                        <td width="50%"><?= $row['contact_sync_cashback'].' AED'; ?></td>
                    </tr>
                    <tr>
                        <th width="50%">Appointment Date Selection </th>
                        <td width="50%"><?= $row['appointment_date_select']; ?></td>
                    </tr>
                    <tr>
                        <th width="50%">Gift Cancellation Days</th>
                        <td width="50%"><?= $row['gift_cancellation_days'].' Days'; ?></td>
                    </tr>
                    <tr>
                        <th width="50%">Order Expiration Days</th>
                        <td width="50%"><?= $row['order_expiration_days'].' Days'; ?></td>
                    </tr>

                </tbody>
               
            </table>
            <h3 align="center">App Versioning</h3>
            <table  class="table table-bordered" >
                <tbody>
                    <tr>
                        <th width="50%">Customer Android App Version</th>
                        <td width="50%"><?= $row['customer_android_app_version']; ?></td>
                    </tr>
                    <tr>
                        <th width="50%">Customer IOS App Version</th>
                        <td width="50%"><?= $row['customer_ios_app_version']; ?></td>
                    </tr>
                    <tr>
                        <th width="50%">Customer Android App Update Force</th>
                        <td width="50%"><?= $row['customer_android_app_force']=='1'?'Yes':'No'; ?></td>
                    </tr>

                     <tr>
                        <th width="50%">Customer IOS App Update Force</th>
                        <td width="50%"><?= $row['customer_ios_app_force']=='1'?'Yes':'No'; ?></td>
                    </tr>

                    <tr>
                        <th width="50%">SP Android App Version</th>
                        <td width="50%"><?= $row['sp_android_app_version']; ?></td>
                    </tr>
                    <tr>
                        <th width="50%">SP IOS App Version</th>
                        <td width="50%"><?= $row['sp_ios_app_version']; ?></td>
                    </tr>
                     <tr>
                        <th width="50%">SP Android App Update Force</th>
                        <td width="50%"><?= $row['sp_android_app_force']=='1'?'Yes':'No'; ?></td>
                    </tr>

                     <tr>
                        <th width="50%">SP IOS App Update Force</th>
                        <td width="50%"><?= $row['sp_ios_app_force']=='1'?'Yes':'No'; ?></td>
                    </tr>

                </tbody>
               
            </table>
                  
            </div>
        </div>
    </div>
</div>
@endsection

