@extends('layouts.admin.admin')

@section('uniquecss')

  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css">
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>{{$title}}</h5>
          

            </header>
             @include('message')
            <div id="collapse2" class="body">
                <!-- <form class="form-horizontal" id="popup-validation"> -->
                {!! Form::open(['route' => ['setting.update',$row->id], 'method' => 'PUT', 'class' => 'form-horizontal','id' => 'popup-validation','enctype' => 'multipart/form-data']) !!}
                {{ csrf_field() }}

                    <div class="form-group">
                        {{Form::label('name', 'Tax Percentage', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('percentage',round($row->percentage,2), ['class' => 'form-control validate[required]']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('name', 'Referred Cashback', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('referred_cashback',round($row->referred_cashback,2), ['class' => 'form-control validate[required]']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('contact_sync_cashback', 'Contact Sync Cashback', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('contact_sync_cashback',round($row->contact_sync_cashback,2), ['class' => 'form-control validate[required]']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('name', 'Appointment 
                        Date Selection', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('appointment_date_selection',$row->appointment_date_select, ['class' => 'form-control validate[required]']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('name', 'Gift Cancellation Days', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('gift_cancellation_days',$row->gift_cancellation_days, ['class' => 'form-control validate[required]']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('name', 'Order Expiration Days', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('order_expiration_days',$row->order_expiration_days, ['class' => 'form-control validate[required]']) }}
                        </div>
                    </div>
                    <h3 align="center">App Versioning</h3>
                    <div class="form-group">
                        {{Form::label('customer_android_app_version', 'Customer Android App Version', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('customer_android_app_version',$row->customer_android_app_version, ['class' => 'form-control validate[required]']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('customer_ios_app_version', 'Customer IOS App Version', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('customer_ios_app_version',$row->customer_ios_app_version, ['class' => 'form-control validate[required]']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        <?php
                        $is_yes = $row->customer_android_app_force ? true: false;
                        $is_no = $row->customer_android_app_force ? false: true;
                        ?>
                        {{Form::label('customer_android_app_force', 'Customer Android App Update Force', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::radio('customer_android_app_force', '1', $is_yes) }}Yes
                            {{ Form::radio('customer_android_app_force', '0', $is_no) }}No
                        </div>
                    </div>

                     <div class="form-group">
                         <?php
                        $is_yes = $row->customer_ios_app_force ? true: false;
                        $is_no = $row->customer_ios_app_force ? false: true;
                        ?>
                        {{Form::label('customer_ios_app_force', 'Customer IOS App Update Force', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::radio('customer_ios_app_force', '1', $is_yes) }}Yes
                            {{ Form::radio('customer_ios_app_force', '0', $is_no) }}No
                        </div>
                    </div>

                    <div class="form-group">

                        {{Form::label('sp_android_app_version', 'SP Android App Version', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('sp_android_app_version',$row->sp_android_app_version, ['class' => 'form-control validate[required]']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('sp_ios_app_version', 'SP IOS App Version', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('sp_ios_app_version',$row->sp_ios_app_version, ['class' => 'form-control validate[required]']) }}
                        </div>
                    </div>

                    <div class="form-group">
                         <?php
                        $is_yes = $row->sp_android_app_force ? true: false;
                        $is_no = $row->sp_android_app_force ? false: true;
                        ?>
                        {{Form::label('sp_android_app_force', 'SP Android App Update Force', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::radio('sp_android_app_force', '1',$is_yes) }}Yes
                            {{ Form::radio('sp_android_app_force', '0',$is_no) }}No
                        </div>
                    </div>

                     <div class="form-group">
                        <?php
                        $is_yes = $row->sp_ios_app_force ? true: false;
                        $is_no = $row->sp_ios_app_force ? false: true;
                        ?>
                        {{Form::label('sp_ios_app_force', 'SP IOS App Update Force', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                             {{ Form::radio('sp_ios_app_force', '1',$is_yes) }}Yes
                            {{ Form::radio('sp_ios_app_force', '0',$is_no) }}No
                        </div>
                    </div>


                    

                    <div class="form-actions no-margin-bottom">
                        <input type="submit" value="Submit" class="btn btn-primary">
                    </div>
                    <br><br><br><br>
                </form>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
@endsection
@section('uniquescript')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>

     <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/holder/2.4.1/holder.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/jquery.uniform.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>

    <script>
        $(function() {
          Metis.formValidation();
        });
    </script>


@endsection