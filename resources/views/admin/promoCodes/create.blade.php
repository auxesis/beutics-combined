@extends('layouts.admin.admin')

@section('uniquecss')


  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/datepicker/jquery-ui.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

  <style>
    .select2{
      width:350px !important;
    }
  </style>

@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>{{$title}}</h5>
          

            </header>
             @include('message')
            <div id="collapse2" class="body">
                <!-- <form class="form-horizontal" id="popup-validation"> -->
                {!! Form::open(['route' => 'promocode.store', 'method' => 'POST', 'class' => 'form-horizontal','id' => 'popup-validation','enctype' => 'multipart/form-data']) !!}
                {{ csrf_field() }}

                    <div class="form-group">
                        {{Form::label('promo_code_title', 'Promo Code Title', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('promo_code_title','', ['class' => 'form-control validate[required]']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('promo_code_title_ar', 'Promo Code Title(Arabic)', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('promo_code_title_ar','', ['class' => 'form-control validate[required]']) }}
                        </div>
                    </div>


                    <div class="form-group">
                        {{Form::label('description', 'Description', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::textarea('description','', ['class' => 'form-control validate[required]','rows'=>'2']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('description_ar', 'Description(Arabic)', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::textarea('description_ar','', ['class' => 'form-control validate[required]','rows'=>'2']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('minimum_order_amount', 'Minimum Order Amount', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('minimum_order_amount','', ['class' => 'form-control validate[required]','onkeypress'=>'return isNumber(event)']) }}
                        </div>
                    </div>


                    <div class="form-group">
                        {{Form::label('code_limit', 'Code Limit (per user)', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('code_limit','', ['class' => 'form-control validate[required]','onkeypress'=>'return isNumber(event)']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('from_date', 'From Date', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('from_date',null, ['class' => 'form-control validate[required]','id'=>'from_date_picker','autocomplete'=>'off']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('to_date', 'To Date', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('to_date',null, ['class' => 'form-control validate[required]','id'=>'to_date_picker','autocomplete'=>'off']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('promo_code_amount', 'Promo Code Amount', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            <input type="radio" name="promo_code_type" value="0" checked="checked"><label>Flat</label>
                            <input type="radio" name="promo_code_type" value="1"><label>Precentage</label>

                            {{ Form::text('promo_code_amount',null, ['class' => 'form-control validate[required]','autocomplete'=>'off','onkeypress'=>'return isNumber(event)']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('linked_store', 'Linked Store', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            <input type="radio" name="linked_store" class="linked_stores" value="0" checked="checked"><label>All Stores</label>
                            <input type="radio" name="linked_store" class="linked_stores" value="1"><label>Selected Store</label>
                            <div style="display:none;" id="select_store_div">
                                {{ Form::select('sp_id[]', $store_names,null,['class' => 'form-control validate[required] myselect']) }}
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('linked_customer', 'Linked Customer', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            <input type="radio" name="linked_customer" class="linked_customers" value="0" checked="checked"><label>All Customers</label>
                            <input type="radio" name="linked_customer" class="linked_customers" value="1"><label>Selected Customer</label>


                            <div style="display:none;" id="select_customer_div">
                                {{ Form::select('user_id[]', $customer_names,null,['class' => 'form-control validate[required] myselect']) }}
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('linked_level', 'Linked Level', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                           
                            <input type="radio" name="linked_level" class="linked_levels" value="2"><label>Category</label>

                            <input type="radio" name="linked_level" class="linked_levels" value="1"><label>Subcategory</label>

                             <input type="radio" name="linked_level" class="linked_levels" value="0"><label>Service</label>

                             <div style="display:none;" id="service_div">
                                {{ Form::select('service_id[]', $service_names,null,['class' => 'form-control validate[required] myselect']) }}
                            </div>

                            <div style="display:none;" id="subcategory_div">
                                {{ Form::select('subcategory_id[]', $subcategory_names,null,['class' => 'form-control validate[required] myselect']) }}
                            </div>

                             <div style="display:none;" id="category_div">
                                {{ Form::select('category_id[]', $category_names,null,['class' => 'form-control validate[required] myselect']) }}
                            </div>

                        </div>
                    </div>


                    <div class="form-actions no-margin-bottom">
                        <input type="submit" value="Submit" class="btn btn-primary">
                    </div>
                    <br><br><br><br>
                </form>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
@endsection
@section('uniquescript')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>

     <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/holder/2.4.1/holder.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/jquery.uniform.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>
    <script src="{{ url('/') }}/assets/lib/datepicker/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script>
        $(function() {
          Metis.formValidation();
          
          $(".myselect").select2({
                multiple: true,
            });
        });

        function isNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        $( function() {
            var a = new Date();
            var timeZoneOffset = +4*60;
            a.setMinutes(a.getMinutes() + a.getTimezoneOffset() + timeZoneOffset );
          $( "#from_date_picker" ).datepicker({ 
            // 
            dateFormat: 'yy-mm-dd',
            changeYear: true,
            changeMonth: true,
            minDate: 0,a,
            onSelect: function(dateText, inst){
                 $("#to_date_picker").datepicker("option","minDate",
                 $("#from_date_picker").datepicker("getDate"));
              }
            });

           $( "#to_date_picker" ).datepicker({ 
            dateFormat: 'yy-mm-dd',
            changeYear: true,
            changeMonth: true,a,
            });

           $(".linked_stores").change(function(){
                 var radioValue = $("input[name='linked_store']:checked").val();
                 if(radioValue == 1){
                    $("#select_store_div").css('display','block');
                 }else{
                    $("#select_store_div").css('display','none');
                 }
           });

           $(".linked_customers").change(function(){
                 var radioValue = $("input[name='linked_customer']:checked").val();
                 if(radioValue == 1){
                    $("#select_customer_div").css('display','block');
                 }else{
                    $("#select_customer_div").css('display','none');
                 }
           });

           $(".linked_levels").change(function(){
                 var radioValue = $("input[name='linked_level']:checked").val();
                 if(radioValue == 0){
                    $("#service_div").css('display','block');
                    $("#subcategory_div").css('display','none');
                    $("#category_div").css('display','none');
                 }
                 else if(radioValue == 1){
                    $("#subcategory_div").css('display','block');
                    $("#service_div").css('display','none');
                    $("#category_div").css('display','none');
                 }
                 else{
                    $("#category_div").css('display','block');
                    $("#subcategory_div").css('display','none');
                    $("#service_div").css('display','none');
                 }
           });

        } );
    </script>

@endsection