@extends('layouts.admin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
 
@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>Promo Code</h5>
                <a class="btn btn-primary pull-right" href="{{route('promocode.create')}}" style="margin-top:4px;">Create Promo Code</a>
            </header>
            <div id="collapse4" class="body">
              @include('message')
              
               <table id="user_datatable" class="table table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Title(Arabic)</th>
                        <th>From Date</th>
                        <th>To Date</th>
                        <th>Min. Order Amount</th>
                        <th>Code Limit</th>
                        <th>Applied On</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
                  
            </div>
        </div>
    </div>
</div>
@endsection

@section('uniquescript')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>
<script type="text/javascript">

  function deleteRow(obj)
  {
    event.preventDefault(); // prevent form submit
    swal({
      title: "Are you sure?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {        
       obj.submit();

      } else {
         swal.close();
      }
    });
  }

  function changeStatus(id)
    {
      swal({
          title: "Are you sure?",
          type: "warning",
          showCancelButton: "No",
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Yes",
          cancelButtonText: "No",
        },
          function(){
            jQuery.ajax({
            url: '{{route('promo.status.update')}}',
            type: 'POST',
            data:{promo_id:id},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
              $("#status_"+id).html(response);
            }
            });
          }
      );
    }
    
 
    $(function() {
        var table = $('#user_datatable').DataTable({
          stateSave: true,
        processing: true,
        serverSide: true,
        order: [[0, "desc" ]],
        "ajax":{
        "url": '{!! route('promocode.getdata') !!}',
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{csrf_token()}}"}
        },
        columns: [
        { data: 'promo_code_title', name: 'promo_code_title', orderable:true },
        { data: 'promo_code_title_ar', name: 'promo_code_title_ar', orderable:true },
        { data: 'from_date', name: 'from_date', orderable:true },
        { data: 'to_date', name: 'to_date', orderable:true },
        { data: 'minimum_order_amount', name: 'minimum_order_amount', orderable:true },
        { data: 'code_limit', name: 'code_limit', orderable:true },
        { data: 'flow', name: 'flow', orderable:true },
        { data: 'status', name: 'status', orderable:true },
        { data: 'action', name: 'action', orderable:true  },
        ],
        "columnDefs": [
        { "searchable": false, "targets": 0 }
        ]
        , language: {
        searchPlaceholder: "Search by title"
        },
});
});  


</script>
@endsection

