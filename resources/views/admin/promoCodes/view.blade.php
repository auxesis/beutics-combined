@extends('layouts.admin.admin')
@section('uniquecss')

<style type="text/css">
    table.owntable th {
        background-color: #337ab7;
        color: #fff;
    }
</style>

@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>View Promo Code Details</h5>

            </header>
            <div id="collapse2" class="body">
              <div class="row">
                 @include('message')
                <div class="col-sm-12 col-lg-12">

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Title</b></div>
                        <div class="col-lg-8"><?= $row->promo_code_title; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Title(Arabic)</b></div>
                        <div class="col-lg-8"><?= $row->promo_code_title_ar; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>From Date</b></div>
                        <div class="col-lg-8"><?= date('d-M-Y',strtotime($row->from_date)); ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>To Date</b></div>
                        <div class="col-lg-8"><?= date('d-M-Y',strtotime($row->to_date)); ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Code Limit</b></div>
                        <div class="col-lg-8"><?= $row->code_limit; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Minimum Order Amount</b></div>
                        <div class="col-lg-8"><?= $row->minimum_order_amount; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Promo Code Type</b></div>
                        <div class="col-lg-8">
                          <?php
                          if($row->promo_code_type == '0') {
                            echo "Flat";
                          }else{
                            echo "Percentage";
                          }
                          ?>
                        </div> 
                        <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Promo Code Amount</b></div>
                        <div class="col-lg-8"><?= round($row->promo_code_amount,2); ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Description</b></div>
                        <div class="col-lg-8"><?= $row->description; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Description(Arabic)</b></div>
                        <div class="col-lg-8"><?= $row->description_ar; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Linked Store</b></div>
                        <div class="col-lg-8">
                          <?php
                          if($row->linked_store == '0') {
                            echo "All";
                          }else{
                            foreach($row->getAssociatedServiceProviders as $cust){
                              $name[] = $cust->getAssociatedStorename->name;
                            }
                            echo implode(', ',$name);
                          }
                          ?>
                        </div> 
                        <div class="clearfix"></div>
                    </div>

                     <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Linked Customer</b></div>
                        <div class="col-lg-8">
                          <?php
                          if($row->linked_customer == '0') {
                            echo "All";
                          }else{
                            foreach($row->getAssociatedCustomers as $cust){
                              $name[] = $cust->getAssociatedUsername->name.' (<b>'.$cust->getAssociatedUsername->mobile_no.'</b>)';
                            }
                            echo implode(', ',$name);
                          }
                          ?>
                        </div> 
                        <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                      <?php 
                      $title = '';
                      $name = '';
                        if($row->linked_level == '0'){
                          $title = 'Services';
                          foreach($row->getAssociatedPromoServices as $row){
                            $sr_name[] = $row->getAssociatedPromoServiceName->name;
                          }
                          $names = implode(', ',$sr_name);

                        }elseif ($row->linked_level == '1') {
                          $title = 'Sub Categories';
                          foreach($row->getAssociatedSPromoSubcategories as $row){
                            $sr_name[] = $row->getAssociatedPromoSubcategoryName->category_name;
                          }
                          $names = implode(', ',$sr_name);
                        }else{
                          $title = 'Categories';
                          foreach($row->getAssociatedPromoCategories as $row){
                            $sr_name[] = $row->getAssociatedPromoCategoryName->category_name;
                          }
                          $names = implode(', ',$sr_name);
                        }
                      ?>
                        <div class="col-lg-4"><b>Linked {{$title}}</b></div>
                        <div class="col-lg-8">
                          <?php echo $names; ?>
                        </div> 
                        <div class="clearfix"></div>
                    </div>


                  <div class="clearfix"></div><br><br>
                </div>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>

@endsection