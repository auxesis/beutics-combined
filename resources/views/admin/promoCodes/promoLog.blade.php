@extends('layouts.admin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
 
@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>Promo Code Uses</h5>
               
            </header>
            <div id="collapse4" class="body">
              @include('message')
              
               <table id="user_datatable" class="table table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Order ID</th>
                        <th>Service Provider</th>
                        <th>Customer</th>
                        <th>Used On</th>
                    </tr>
                </thead>
                <tbody>
                  <?php if($data){ ?>
                  <?php foreach($data as $key => $value){ ?>
                    <tr>
                      <td><?php echo $key+1; ?></td>
                      <td><?php echo $value['booking_unique_id']; ?></td>
                      <td><?php echo $value['sp_name']; ?></td>
                      <td><?php echo $value['name']; ?></td>
                      <td><?php echo $value['created_at']; ?></td>
                    </tr>
                  <?php } ?>
                  <?php } else { ?>
                    <tr>
                      <td colspan="5">No data are found.</td>
                    </tr>
                  <?php } ?>
                </tbody>
            </table>
                  
            </div>
        </div>
    </div>
</div>
@endsection

@section('uniquescript')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>

@endsection

