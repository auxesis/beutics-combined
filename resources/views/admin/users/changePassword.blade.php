@extends('layouts.admin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css">
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>Change Password</h5>
                <!-- .toolbar -->
            <div class="toolbar">
              <nav style="padding: 8px;">
                  <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                      <i class="fa fa-minus"></i>
                  </a>
                  <a href="javascript:;" class="btn btn-default btn-xs full-box">
                      <i class="fa fa-expand"></i>
                  </a>
                  <a href="javascript:;" class="btn btn-danger btn-xs close-box">
                      <i class="fa fa-times"></i>
                  </a>
              </nav>
            </div>            <!-- /.toolbar -->

            </header>
            <div id="collapse2" class="body">
                <!-- <form class="form-horizontal" id="popup-validation"> -->
                {!! Form::open(['route' => 'users.update-password', 'method' => 'POST', 'class' => 'form-horizontal', 'id' => 'popup-validation']) !!}
                {{ csrf_field() }}

                    @include('message')

                    <input type="hidden" name="slug" value="{{$slug}}">
                   <!--  <div class="form-group">
                        <label class="control-label col-lg-4">Old Password</label>

                        <div class=" col-lg-4">
                            <input class="validate[required] form-control" type="password" name="old_password" id="old_password"/>
                        </div>
                    </div> -->

                    <div class="form-group">
                        <label class="control-label col-lg-4">New Password</label>

                        <div class=" col-lg-4">
                            <input class="validate[required] form-control" type="password" name="new_password" id="new_password"/>
                        </div>
                    </div>   

                    <div class="form-group">
                        <label class="control-label col-lg-4">Confirm Password</label>

                        <div class=" col-lg-4">
                            <input class="validate[required,equals[new_password]] form-control" type="password" name="confirm_password" id="confirm_password"/>
                                   
                        </div>
                    </div>  
              

                    <div class="form-actions no-margin-bottom">
                        <input type="submit" value="Submit" class="btn btn-primary">
                    </div>
                    <br><br><br><br>
                </form>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>

 

@endsection
@section('uniquescript')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>
    
    <script src="{{ url('/') }}/assets/lib/jquery-validation/jquery.validate.js"></script>

    
    <script>
        $(function() {
          Metis.formValidation();
        });
    </script>


@endsection