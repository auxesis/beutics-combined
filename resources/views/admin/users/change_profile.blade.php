@extends('layouts.admin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/jquery.gritter/css/jquery.gritter.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/themes/default/css/uniform.default.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css">
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>Edit Profile</h5>
                <!-- .toolbar -->
            <div class="toolbar">
              <nav style="padding: 8px;">
                  <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                      <i class="fa fa-minus"></i>
                  </a>
                  <a href="javascript:;" class="btn btn-default btn-xs full-box">
                      <i class="fa fa-expand"></i>
                  </a>
                  <a href="javascript:;" class="btn btn-danger btn-xs close-box">
                      <i class="fa fa-times"></i>
                  </a>
              </nav>
            </div>            <!-- /.toolbar -->

            </header>
            <div id="collapse2" class="body">
                <!-- <form class="form-horizontal" id="popup-validation"> -->
                {!! Form::open(['route' => 'admin.users.updateprofile', 'method' => 'POST', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
                {{ csrf_field() }}
                	<input type="hidden" name="id" value="<?= $row->id ?>">
                    <div class="form-group">
                        <label class="control-label col-lg-4">Name</label>
                        <div class="col-lg-4">
                            <input type="text" class="validate[required] form-control" id="req" name="name" value="<?= $row->name ?>">
                        </div>
                    </div>
                    
                    @if($errors->has('name'))
                    <div class="error">{{ $errors->first('name') }}</div>
                    @endif
                   
                    <div class="form-group">
                        <label class="control-label col-lg-4">Upload Image</label>
                        <div class="col-lg-2">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
              								<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
              								  <img src="{{url('/').'/'.$row->profile}}" alt="...">
              								</div>
              								<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"></div>
              								<div>
              								  <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="profile"></span>
              								  <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
              								</div>
              							</div>
                        </div>
                    </div>
                     @if($errors->has('profile'))
                    <div class="error">{{ $errors->first('profile') }}</div>
                    @endif

                    <div class="form-actions no-margin-bottom">
                        <input type="submit" value="Submit" class="btn btn-primary">
                    </div>
                    <br><br><br><br>
                </form>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
@endsection
@section('uniquescript')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>

     <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/holder/2.4.1/holder.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/jquery.uniform.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>

<!-- 
    <script src="{{ url('/') }}/assets/lib/plupload/js/plupload.full.min.js"></script>
    <script src="{{ url('/') }}/assets/lib/plupload/js/jquery.plupload.queue/jquery.plupload.queue.min.js"></script>
    <script src="{{ url('/') }}/assets/lib/jquery.gritter/js/jquery.gritter.min.js"></script>
    <script src="{{ url('/') }}/assets/lib/formwizard/js/jquery.form.wizard.js"></script>

    <script src="{{ url('/') }}/assets/lib/jquery-validation/jquery.validate.js"></script> -->
    <script>
        $(function() {
          Metis.formValidation();
        });
    </script>


@endsection