<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Login</title>
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ url('/') }}/assets/lib/bootstrap/css/bootstrap.css">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ url('/') }}/assets/lib/font-awesome/css/font-awesome.css">
    
    <!-- Metis core stylesheet -->
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/main.css">
    
    <style>
        .error{
            color:red;
        }
    </style>
</head>

<body class="login">

    <div class="form-signin">
        <div class="text-center">
            <img src="{{ url('/') }}/assets/img/logo.png" height="122px" alt="Metis Logo">
        </div>
        <hr>
    <div class="tab-content">
        <div id="login" class="tab-pane active">
                {!! Form::open(['route' => 'admin.make.login', 'method' => 'POST', 'class'=>'validate login100-form validate-form']) !!}
                {{ csrf_field() }}
                <p class="text-muted text-center">
                    Enter your username and password
                </p>
                {{ Form::text('username','', ['class' => 'form-control top', 'placeholder' => 'Username', 'required' => true]) }}

                {{ Form::password('password', array('placeholder' => 'Password', "class" => "form-control", 'required' => true)) }}

                 @if($errors->has('password'))
                <div class="error">{{ $errors->first('password') }}</div>
                @endif

                @if(Session::has('message'))
                    <div class="error"><center>{{Session::get('message')}}</center></div>
                @endif 

                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            </form>
        </div>
    </div>
     <br>
    <div class="text-center">
        <ul class="list-inline">
            <li><a href="{{route('front.forgotpassword')}}" class="text-muted">Forgot Password</a></li>
        </ul>
    </div>
  </div>

    <!--jQuery -->
    <script src="{{ url('/') }}/assets/lib/jquery/jquery.js"></script>

    <!--Bootstrap -->
    <script src="{{ url('/') }}/assets/lib/bootstrap/js/bootstrap.js"></script>
    <script src="{{ url('/') }}/assets/js/jquery.validate.min.js"></script>
    <script>$(".validate").validate();</script>
</body>

</html>
