@extends('layouts.admin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
 
@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>Customers</h5>
                <a class="btn btn-primary pull-right" href="{{route('users.downloadFile')}}" style="margin-top:4px;"><i class="fa fa-download"></i> Export All Customers</a> 
            </header>
            <div id="collapse4" class="body">
              @include('message')
              
               <table id="user_datatable" class="table table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Country Code</th>
                        <th>Mobile No</th>
                        <th>Referral</th>             
                        <th>Created At</th>
                        <th>Status</th>
                        <th style="width: 100px">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
                  
            </div>
        </div>
    </div>
</div>
@endsection

@section('uniquescript')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>
<script type="text/javascript">
  function callNative(){

    $("#user_datatable").DataTable().draw();
  }
  function loadStatusFilter()
  {

    jQuery("#user_datatable_filter").parent().parent().
      append('<div id="user_datatable_verfication_filter" class="col-sm-12 col-md-2"><label>Email Filter:</label><select name="columns[testtest]" class="form-control input-sm" id="custom-verfication-filter" onchange="callNative()"><option value="">All</option><option value="verified">Verified Email</option><option value="unverified">Unverified Email</option></select></div>');

    jQuery('#user_datatable_length').parent().closest("div.col-sm-4").removeClass("col-md-4");  
    jQuery('#user_datatable_length').parent().closest("div.col-sm-4").addClass("col-md-4");  

    jQuery('#user_datatable_filter').parent().closest("div.col-sm-4").removeClass("col-md-4");  
    jQuery('#user_datatable_filter').parent().closest("div.col-sm-4").addClass("col-md-4");

  
  }
  function deleteRow(obj)
  {
    event.preventDefault(); // prevent form submit
    swal({
      title: "Are you sure?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {        
       obj.submit();

      } else {
         swal.close();
      }
    });
  }
    
    function changeStatus(id)
    {
      swal({
          title: "Are you sure?",
          type: "warning",
          showCancelButton: "No",
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Yes",
          cancelButtonText: "No",
        },
          function(){
            jQuery.ajax({
            url: '{{route('admin.users.status.update')}}',
            type: 'POST',
            data:{user_id:id},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
              $("#status_"+id).html(response);
            }
            });
          }
      );
    }
    
    function changeArabicStatus(id)
    {
      swal({
          title: "Are you sure?",
          type: "warning",
          showCancelButton: "No",
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Yes",
          cancelButtonText: "No",
        },
          function(){
            jQuery.ajax({
            url: '{{route('admin.users.arabic.status.update')}}',
            type: 'POST',
            data:{user_id:id},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
              $("#ar_status_"+id).html(response);
            }
            });
          }
      );
    }
    $(function() {
        var table = $('#user_datatable').DataTable({
          stateSave: true,
        processing: true,
        serverSide: true,
        order: [[0, "desc" ]],
        "ajax":{
        "url": '{!! route('users.getdata') !!}',
                "dataType": "json",
                "type": "POST",
                data: function(d) {
                    d._token= "{{csrf_token()}}";
                    d.verfication_filter = $('#custom-verfication-filter').val();
                }
        },
        columns: [
        { data: 'name', name: 'name', orderable:true },
        { data: 'email', name: 'email', orderable:true  },
        { data: 'country_code', name: 'country_code', orderable:true  },
        { data: 'mobile_no', name: 'mobile_no', orderable:true  },
        { data: 'referral', name: 'referral', orderable:true  },
        { data: 'created_at', name: 'created_at', orderable:true  },
        { data: 'status', name: 'status', orderable:true  },
        { data: 'action', name: 'action', orderable:true  },
        ],
        "columnDefs": [
        { "searchable": false, "targets": 0 }
        ]
        , language: {
        searchPlaceholder: "Search by name and email"
        },
        initComplete:function(){

          loadStatusFilter();
        }
});
});  


</script>
@endsection

