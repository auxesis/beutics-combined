@extends('layouts.admin.admin')
@section('uniquecss')

  <style>
    #percent-sign {
        top: 8px;
        left: 126px;
        color: #555;
        position: absolute;
        z-index: 1;
    }
  </style>

@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>View Profile</h5>

            </header>
            <div id="collapse2" class="body">
                <div class="row">
                     @include('message')
                    <div class="col-sm-12 col-lg-12">
                        <div class="col-lg-12 border_details">
                            <div class="col-lg-8">
                                @if($row->image!='' && $row->image!=null)
                                <img src="<?= asset($row->image); ?>" width="150" height="150"/>
                                @else
                                <img src="<?= asset('images/defualt.jpeg'); ?>" width="150" height="150"/>
                                @endif
                                 <br/><br/>
                            </div> 

                            <div class="clearfix"></div>
                        </div>

                        <div class="col-lg-12 border_details">
                            <div class="col-lg-4"><b>Name</b></div>
                            <div class="col-lg-8"><?= $row->name; ?></div> <div class="clearfix"></div>
                        </div>
                        <div  class="col-lg-12 border_details">
                            <div class="col-lg-4"><b>Email</b></div>
                            <div class="col-lg-8"><?= $row->email; ?></div> <div class="clearfix"></div>
                        </div>
                        <div  class="col-lg-12 border_details">
                            <div class="col-lg-4"><b>Mobile No</b></div>
                            <div class="col-lg-8"><?= '+'.$row->country_code.' '.$row->mobile_no; ?></div> <div class="clearfix"></div>
                        </div>

                        <div  class="col-lg-12 border_details">
                            <div class="col-lg-4"><b>Gender</b></div>
                            <div class="col-lg-8"><?= $row->gender; ?></div> <div class="clearfix"></div>
                        </div>

                        <div  class="col-lg-12 border_details">
                            <div class="col-lg-4"><b>Date of Birth</b></div>
                            <div class="col-lg-8"><?= ($row->dob!='' && $row->dob!=null) ? date('Y-M-d',strtotime($row->dob)):''; ?></div> <div class="clearfix"></div>
                        </div>

                        <div  class="col-lg-12 border_details">
                            <div class="col-lg-4"><b>Registration Type</b></div>
                            <div class="col-lg-8"><?= $row->registration_type; ?></div> <div class="clearfix"></div>
                        </div>

                        <div  class="col-lg-12 border_details">
                            <div class="col-lg-4"><b>Address</b></div>
                            <div class="col-lg-8"><?= $row->address; ?></div> <div class="clearfix"></div>
                        </div>

               
                    </div>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>

@endsection
