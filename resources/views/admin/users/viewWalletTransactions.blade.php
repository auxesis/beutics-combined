@extends('layouts.admin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
 
@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>View Wallet Transactions</h5>
            </header>
            <div class="col-lg-12 text-center">
              <div class="col-lg-12 border_details">
                  <div class="col-lg-4"><b>Name</b></div>
                  <div class="col-lg-8"><?php echo $row->name; ?></div> <div class="clearfix"></div>
              </div>
              <div class="col-lg-12 border_details">
                  <div class="col-lg-4"><b>Mobile Number</b></div>
                  <div class="col-lg-8"><?php echo $row->mobile_no; ?></div> <div class="clearfix"></div>
              </div>
              <div class="col-lg-12 border_details">
                  <div class="col-lg-4"><b>Current Wallet Balance</b></div>
                  <div class="col-lg-8"><?php echo round($row->wallet_amount, 2).' AED'; ?></div> <div class="clearfix"></div>
              </div>
            </div>
            <div id="collapse4" class="body">
              @include('message')
              
               <table id="user_datatable" class="table table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Unique ID</th>
                        <th>Amount</th>
                        <th>Transaction Type</th>             
                        <th>Notes</th>
                        <th>Transaction Date & Time</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
                  
            </div>
        </div>
    </div>
</div>
@endsection

@section('uniquescript')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>
<script type="text/javascript">
 
    $(function() {
        var table = $('#user_datatable').DataTable({
          stateSave: true,
        processing: true,
        serverSide: true,
        order: [[0, "desc" ]],
        "ajax":{
        "url": '{!! route('users.getwalletdata') !!}',
                "dataType": "json",
                "type": "POST",
                data: function(d) {
                    d._token= "{{csrf_token()}}";
                    d.id = <?php echo $id ?>;
                }
        },
        columns: [
        { data: 's_no', name: 's_no', orderable:true },
        { data: 'booking_unique_id', name: 'booking_unique_id', orderable:true },
        { data: 'amount', name: 'amount', orderable:true  },
        { data: 'transaction_type', name: 'transaction_type', orderable:true  },
        { data: 'description', name: 'description', orderable:true  },
        { data: 'transaction_date_time', name: 'transaction_date_time', orderable:true  },
        ],
        "columnDefs": [
        { "searchable": false, "targets": 0 }
        ]
});
});  


</script>
<style>
#user_datatable_filter{display:none;}
</style>
@endsection

