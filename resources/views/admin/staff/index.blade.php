@extends('layouts.admin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
 
@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>Staff</h5>
                <a class="btn btn-primary pull-right" href="{{route('staff.create')}}" style="margin-top:4px;">Create Staff</a>
            </header>
            <div id="collapse4" class="body">
              @include('message')
              
               <table id="user_datatable" class="table table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Store Name</th>
                        <th>Staff Name</th>
                        <th>Nationality</th>
                        <th>Experience</th>
                        <th>Speciality</th>
                        <th>Provide Home Service</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
                  
            </div>
        </div>
    </div>
</div>
@endsection

@section('uniquescript')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>
<script type="text/javascript">

  function deleteRow(obj,asso)
  {
    event.preventDefault(); // prevent form submit
   
    var msg = "Are you sure?";
    if(asso!=''){
        var str = asso;
        var res = str.split("__");
        var msg = "You cannot delete this Staff because "+res+" tables are associated with it.";
                           
   
    }
    if(asso!=''){ 
      swal({
          title: msg,
          type: "warning",
          showCancelButton: true,
          showConfirmButton: false,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          cancelButtonText: "Ok",
          closeOnConfirm: false,
          closeOnCancel: false,

        },
        function(isConfirm){
          if (isConfirm) {        
           obj.submit();

          } else {
             swal.close();
          }
        });
    }
    else{
      swal({
          title: msg,
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          cancelButtonText: "No",
          closeOnConfirm: false,
          closeOnCancel: false,

        },
        function(isConfirm){
          if (isConfirm) {        
           obj.submit();

          } else {
             swal.close();
          }
        });
      }
  }
 
    $(function() {
        var table = $('#user_datatable').DataTable({
          stateSave: true,
        processing: true,
        serverSide: true,
        order: [[0, "desc" ]],
        "ajax":{
        "url": '{!! route('staff.getdata') !!}',
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{csrf_token()}}", spid: "{{$spid}}"}
        },
        columns: [
        { data: 'sp_id', name: 'sp_id', orderable:true },
        { data: 'staff_name', name: 'name', orderable:true },
        { data: 'nationality', name: 'nationality', orderable:true  },
        { data: 'experience', name: 'experience', orderable:true  },
        { data: 'speciality', name: 'speciality', orderable:true  },
        { data: 'is_provide_home_service', name: 'is_provide_home_service', orderable:true  },
        { data: 'action', name: 'action', orderable:true  },
        ],
        "columnDefs": [
        { "searchable": false, "targets": 0 }
        ]
        , language: {
        searchPlaceholder: "Search by name and store name"
        },
});
});  


</script>
@endsection

