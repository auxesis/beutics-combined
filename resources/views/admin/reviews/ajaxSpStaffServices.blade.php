
<?php

if(count($final_arr['staff_data'])>0 && count($final_arr['service_data'])>0){ ?>

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css">

<div class="col-md-6">
<h5 align="center">Review the Service Staff</h5>
<ul> 
    <li style="border-bottom:0px">
    <div class="row">
        <div class="col-md-6"><h5>Do you wish to endorse the staff.</h5></div>
        <div class="col-md-6" style="text-align:right">
            <input type="radio" class="staff_check" name="staff_enable" value="1" checked="checked"><label>Yes</label>
            <input type="radio" class="staff_check" name="staff_enable" value="0"><label>No</label>
        </div>
    </div>
            
            
            <input type="text" class="form-control" id="staff_search" placeholder="search staff" onkeyup="search(this,'staff')">
    
    </li>
<div id="staff_div"  class="hboxes" style="display:none;"> 
	@foreach($final_arr['staff_data'] as $staff_key => $staff)
              
        <li>
            <div class="row">
                <div class="col-md-6">
                    <input type="radio" name="staff_id" value="{{$staff_key}}" class="validate[required]" <?php if($staff_id == $staff_key) echo 'checked="checked"'; ?>>
                    <label><?php echo $staff;?></label>
                </div>
                <div class="col-md-6 text-right" id="staff_{{$staff_key}}"><span>

                    <?php if($staff_id == $staff_key && $selected_staff_like_dislike != ''){

                    ?>
                       <input type="radio" value="1" name="staff_like_dislike" class="validate[required]" <?php if($selected_staff_like_dislike == '1') echo 'checked="checked"'; ?> />
                       <label>Like</label> 
                       <input type="radio" value="0" name="staff_like_dislike" class="validate[required]" <?php if($selected_staff_like_dislike == '0') echo 'checked="checked"'; ?> />
                       <label>Dislike</label>
                    <?php
                    }
                    ?>
</span>

                </div>
            </div>    
        </li>
       
    @endforeach
    </div>
    </ul>
    </div>
<?php

$sp_sevices_data_arr = array();

if(isset($sp_sevices_data) && $sp_sevices_data != ''){
    $sp_sevices_data_arr = json_decode($sp_sevices_data);
}
?>

<div class="col-md-6">
<h5 align="center">Review the Service Availed</h5>
<ul> 
<li style="border-bottom:0px">
    <div class="row">
        <div class="col-md-6"> <h5>How do you rate the services</h5></div>
        <div class="col-md-6" style="text-align:right">
        <input type="radio" class="service_check" name="service_enable" value="1" checked="checked"><label>Yes</label>
            <input type="radio" class="service_check" name="service_enable" value="0"><label>No</label>
        </div>
    </div>
           
           
            <input type="text" class="form-control" id="staff_search" placeholder="search services" onkeyup="search(this,'service')">
        </span>
    </li>
    <div id="service_div" class="hboxes" style="display:none;"> 
    <?php foreach($final_arr['service_data'] as $service_key => $service){

       

            $checkbox = '';
            $radio = '';
                //echo '<pre>';
                //print_r($sp_sevices_data_arr);
                //echo '</pre>';
            if(count($sp_sevices_data_arr) > 0 ){
                
                $checkbox = '';
                $radio = '';

                foreach ($sp_sevices_data_arr as $value) {
                    
                    if($service_key == $value->service_id){
                        $checkbox = 1;
                        $radio = $value->like_dislike;
                    }


                }

            }
                



        ?>
                  
        <li>
        <div class="row"> 
        <div class="col-md-6">

            
            <input type="checkbox" name="service_id[]" value="{{$service_key}}" class="validate[required]" <?php if($checkbox != '') echo 'checked="checked"'; ?>>
            <label><?php echo $service;?></label>
        </div>
        <div class="col-md-6 text-right"><span>
            <?php
            if($radio != ''){
            ?>
            
                <input type="radio" value="1" name="service_like_dislike{{$service_key}}" <?php if($radio == '1') echo 'checked="checked"'; ?>><label>Like</label>
                <input type="radio" value="0" name="service_like_dislike{{$service_key}}" <?php if($radio == '0') echo 'checked="checked"'; ?>><label>Dislike</label>
            
            <?php
            }
            ?></span>
        </div>      
        
        </div>  
        </li>
       
    <?php } ?>
    </div>
    </ul> 
    </div>
    <script>
        $(document).ready(function(){
            var radioValue = $("input[name='staff_enable']:checked").val();
            if(radioValue == 1){
                $("#staff_div").css("display","block");
            }else{
                $("#staff_div").css("display","none");
            }
        });
        $(".staff_check").change(function(){
            var radioValue = $("input[name='staff_enable']:checked").val();
            var sp_id = $("#service_provider_id").val();
            if(radioValue == 1){
                var staff = $("#staff_div").html();
                if(staff == ""){
                    var res = search('','staff');
 
                    $("#staff_div").html(res);
                }
            }else{

                $("#staff_div").html("");
            }
        });
        $(document).ready(function(){
            var radioValue = $("input[name='service_enable']:checked").val();
            if(radioValue == 1){
                $("#service_div").css("display","block");
            }else{
                $("#service_div").css("display","none");
            }
        });
        $(".service_check").change(function(){
            var radioValue = $("input[name='service_enable']:checked").val();
            if(radioValue == 1){
                var service = $("#service_div").html();
                if(service == ""){
                    var res = search('','service');
 
                    $("#staff_div").html(res);
                }
                $("#service_div").css("display","block");
            }else{
                $("#service_div").html("");
            }
        });

        $(document).ready(function(){
            $('body #staff_div').on('click', 'input[name="staff_id"]', function() {
                var staff_id = $(this).val();
                $('.staff_id_radio').val(staff_id);
                $('.staff_like_dislike_radio').val('');

                $('body #staff_div .text-right').find('span').remove();
                $(this).closest('.row').find('.text-right').append('<span><input type="radio" value="1" name="staff_like_dislike" class="validate[required]" /><label>Like</label> <input type="radio" value="0" name="staff_like_dislike" class="validate[required]" /><label>Dislike</label></span>');

            });

            if($('.staff_id_radio').val()){
                $('body #staff_div .text-right').find('span').remove();
                <?php if($selected_staff_like_dislike){ ?>
                    $('#staff_'+$('.staff_id_radio').val()).closest('.row').find('.text-right').append('<span><input type="radio" value="1" name="staff_like_dislike" checked class="validate[required]" /><label>Like</label> <input type="radio" value="0" name="staff_like_dislike" class="validate[required]" /><label>Dislike</label></span>');
                <?php } else { ?>
                    $('#staff_'+$('.staff_id_radio').val()).closest('.row').find('.text-right').append('<span><input type="radio" value="1" name="staff_like_dislike"  class="validate[required]" /><label>Like</label> <input type="radio" checked value="0" name="staff_like_dislike" class="validate[required]" /><label>Dislike</label></span>');
                <?php } ?>
                
            }
            var ids = [];
            ids = $('.service_id_check').val().split(',');
            $('body #service_div').on('click', 'input[type="checkbox"]', function() {
                var service_id = $(this).val();

                if($(this).prop('checked') != true){
                    ids = jQuery.grep(ids, function(value) {
                      return value != service_id;
                    });
                } else {
                    ids.push(service_id);
                }                
                $('input[name="service_id_array[]"]').val(ids);

                $(this).closest('.row').find('.text-right').append('<span><input type="radio" value="1" name="service_like_dislike'+service_id+'" class="validate[required]" attr="'+service_id+'"><label>Like</label> <input type="radio" value="0" name="service_like_dislike'+service_id+'" class="validate[required]" attr="'+service_id+'" ><label>Dislike</label></span>');

                $('body #service_div input[type="checkbox"]').each(function(){
                    if($(this).prop('checked') != true){
                        $(this).closest('.row').find('.text-right').find('span').remove();
                    }
                });


            });

            $('body #staff_div').on('click', 'input[name="staff_like_dislike"]', function() {
                $('.staff_like_dislike_radio').val($(this).val());
           });

            $('body #service_div').on('click', 'input[type="radio"]', function() { 
                $('.last_row input[name="service_'+$(this).attr('attr')+'"]').remove();
                $('.last_row').append('<input type="hidden" value="'+$(this).val()+'" name="service_'+$(this).attr('attr')+'" id="service_like_'+$(this).attr('attr')+'" />');
           });

        });

        function search(keyword,search_name)
        {
            var id = $("#service_provider_id").val();
            var selected_staff_id = $(".staff_id_radio").val();
            var service_id_check = $(".service_id_check").val();
            
            // console.log('id',id);
            
            if(id == undefined || id == ''){
                id = $("#sp_id").val();    
            }

            var keyword = keyword.value;
            jQuery.ajax({
                url: '{{route('ajax-get-sp-searched-staff-service')}}',
                type: 'POST',
                data:{sp_id:id,search_keyword:keyword,search_name:search_name,selected_staff_id:selected_staff_id,service_id_check:service_id_check},
                headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                   
                   console.log(response);
                    if(search_name == 'staff'){
                        $("#staff_div").html(response);
                    }else{
                        $("#service_div").html(response);
                    }
                }
            });
        }



    </script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>
    <?php }else{ echo "nodata";} ?>
