<?php if($final_arr['search_name'] == 'staff'){ ?>
 
	@foreach($final_arr['staff_data'] as $staff_key => $staff)
              
        <li>
            <div class="row">
                <div class="col-md-6">
                    <?php if($final_arr['selected_staff_id'] == $staff_key){ ?>
                        <input type="radio" name="staff_id" checked value="{{$staff_key}}" class="validate[required]">
                    <?php } else { ?>
                        <input type="radio" name="staff_id" value="{{$staff_key}}" class="validate[required]">
                    <?php } ?>
                    
                    <?php echo $staff;?>
                </div>
                <div class="col-md-6 text-right" id="staff_{{$staff_key}}">
                </div>
            </div>    
        </li>
       
    @endforeach
  
<?php } else{ ?>

    @foreach($final_arr['service_data'] as $service_key => $service)
                  
        <li>
        <div class="row"> 
        <div class="col-md-6">
            <?php if(in_array($service_key, $final_arr['service_ids_check'])){ ?>
               <input type="checkbox" name="service_id[]" checked value="{{$service_key}}" class="validate[required]" id="service_to_ch_{{$service_key}}">
            <?php } else { ?>
                <input type="checkbox" name="service_id[]" value="{{$service_key}}" class="validate[required]" id="service_to_ch_{{$service_key}}">
            <?php } ?>
            
            <?php echo $service;?>
        </div>
        <div class="col-md-6 text-right"></div>      
        
        </div>  
        </li>
       
    @endforeach
    
    <?php } ?>
<script type="text/javascript">
<?php if(isset($final_arr['selected_staff_id'])){ ?>
if($('.staff_id_radio').val()){
    if($('.staff_like_dislike_radio').val() == '1'){
        $('#staff_'+$('.staff_id_radio').val()).closest('.row').find('.text-right').append('<span><input type="radio" value="1" name="staff_like_dislike" checked class="validate[required]" /><label>Like</label> <input type="radio" value="0" name="staff_like_dislike" class="validate[required]" /><label>Dislike</label></span>');
    } else if($('.staff_like_dislike_radio').val() == '0'){
         $('#staff_'+$('.staff_id_radio').val()).closest('.row').find('.text-right').append('<span><input type="radio" value="1" name="staff_like_dislike"  class="validate[required]" /><label>Like</label> <input type="radio" value="0" name="staff_like_dislike" checked class="validate[required]" /><label>Dislike</label></span>');
    } else {
         $('#staff_'+$('.staff_id_radio').val()).closest('.row').find('.text-right').append('<span><input type="radio" value="1" name="staff_like_dislike"  class="validate[required]" /><label>Like</label> <input type="radio" value="0" name="staff_like_dislike" class="validate[required]" /><label>Dislike</label></span>');
    } 
    
}
<?php } ?>

<?php if(isset($final_arr['service_data'])){
foreach($final_arr['service_data'] as $service_key => $service){
if(in_array($service_key, $final_arr['service_ids_check'])){ ?>
   if($('#service_like_<?= $service_key; ?>').val() == '1'){
        $('#service_to_ch_<?= $service_key; ?>').closest('.row').find('.text-right').append('<span><input type="radio" value="1" checked name="service_like_dislike'+<?= $service_key; ?>+'" class="validate[required]" attr="'+<?= $service_key; ?>+'"><label>Like</label> <input type="radio" value="0" name="service_like_dislike'+<?= $service_key; ?>+'" class="validate[required]" attr="'+<?= $service_key; ?>+'" ><label>Dislike</label></span>');
    } else if($('#service_like_<?= $service_key; ?>').val() == '0'){
         $('#service_to_ch_<?= $service_key; ?>').closest('.row').find('.text-right').append('<span><input type="radio" value="1" name="service_like_dislike'+<?= $service_key; ?>+'" class="validate[required]" attr="'+<?= $service_key; ?>+'"><label>Like</label> <input type="radio" value="0" name="service_like_dislike'+<?= $service_key; ?>+'" checked class="validate[required]" attr="'+<?= $service_key; ?>+'" ><label>Dislike</label></span>');
    } else {
       $('#service_to_ch_<?= $service_key; ?>').closest('.row').find('.text-right').append('<span><input type="radio" value="1" name="service_like_dislike'+<?= $service_key; ?>+'" class="validate[required]" attr="'+<?= $service_key; ?>+'"><label>Like</label> <input type="radio" value="0" name="service_like_dislike'+<?= $service_key; ?>+'" class="validate[required]" attr="'+<?= $service_key; ?>+'" ><label>Dislike</label></span>');
    }
<?php } } } ?>

</script>