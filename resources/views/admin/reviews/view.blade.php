@extends('layouts.admin.admin')
@section('uniquecss')

@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>My Reviews</h5>
                
            </header>
            <div id="collapse4" class="body">
				<div class="row">

					<div class="col-md-12">
						<h3>Questions and Rating</h3>
					    <?php $i=1; ?> 
						@foreach($re_ques_rating->getAssociatedReviewQuestions as $q_arr)
							<div class="col-lg-12 border_details">

	                            <div class="col-lg-4"><span>Q{{$i}}.</span>&nbsp;&nbsp;<b><?= $reviews_ques_arr[$q_arr['ques_id']]['name']; ?></b></div>

	                            <div class="col-lg-4"><b><?= $rating_arr[$q_arr['rating']]['name']; ?></b></div>

	                            <div class="col-lg-8"></div> <div class="clearfix"></div>
	                        </div>
	                     <?php $i++; ?>  
						@endforeach
					</div>
					
					<div class="col-lg-12 border_details">

                        <div class="col-lg-4"><b>Staff Name</b></div>
                        <div class="col-lg-8"><?= isset($re_ques_rating->getAssociatedStaffName->staff_name)?$re_ques_rating->getAssociatedStaffName->staff_name:'NA'; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Staff Like/Dislike</b></div>
                        <div class="col-lg-8">
                            <?php
                                if($re_ques_rating['staff_like_dislike'] == '1') {
                                    echo '<b>Liked</b>';
                                }elseif($re_ques_rating['staff_like_dislike'] == '0') {
                                    echo '<b>Disliked</b>';
                                }else{
                                    echo '<b>NA</b>';
                                }
                            ?>
                           </div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Services</b></div>
                        <div class="col-lg-8">
                        @foreach($re_ques_rating->getAssociatedReviewServices as $sr_arr)
	                        <div class="clearfix">
	                        	<?php

	                        		if($sr_arr['like_dislike'] == '1')
	                        		{
	                        			$sr_like_check = '<b>Liked</b>';
	                        		}else{
	                        			$sr_like_check = '<b>Disliked</b>';
	                        		}
	                        	?>
	                        	<?= $sr_arr->getAssociatedReviewService->getAssociatedService->name.' ('.$sr_like_check.')'?>
	                        </div>
                        @endforeach
                        </div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Beutics Rating</b></div>
                        <div class="col-lg-8"><?= $re_ques_rating['rate_beutics']; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Description</b></div>
                        <div class="col-lg-8"><?= $re_ques_rating['description']; ?></div> <div class="clearfix"></div>
                    </div>

				</div>

            </div>
        </div>
    </div>
</div>
@endsection


