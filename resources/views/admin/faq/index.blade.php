@extends('layouts.admin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
 
@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>FAQ</h5>
                <a class="btn btn-primary pull-right" href="{{route('faq.create')}}" style="margin-top:4px;">Create FAQ</a>
            </header>
            <div id="collapse4" class="body">
              @include('message')
              
               <table id="user_datatable" class="table table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Category Name</th>
                        <th>Question</th>
                        <th>Question(Arabic)</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
                  
            </div>
        </div>
    </div>
</div>
@endsection

@section('uniquescript')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>
<script type="text/javascript">
  function callNative(){

    $("#user_datatable").DataTable().draw();
  }
  function loadStatusFilter()
  {

    jQuery("#user_datatable_filter").parent().parent().
      append('<div id="user_datatable_category_filter" class="col-sm-12 col-md-2"><label>FAQ Categories:</label><select name="columns[testtest]" class="form-control input-sm" id="custom-filter" onchange="callNative()"><option value="">All Categories</option><?php foreach($categories as $key => $cat){ ?><option value="<?= $key?>"><?= $cat?></option><?php } ?></select></div>');
  }
  </script>

<script type="text/javascript">

  function deleteRow(obj)
  {
    event.preventDefault(); // prevent form submit
    swal({
      title: "Are you sure?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {        
       obj.submit();

      } else {
         swal.close();
      }
    });
  }
 
    $(function() {
        var table = $('#user_datatable').DataTable({
          stateSave: true,
        processing: true,
        serverSide: true,
        order: [[0, "desc" ]],
        "ajax":{
        "url": '{!! route('faq.getdata') !!}',
                "dataType": "json",
                "type": "POST",
                data: function(d) {
                    d._token= "{{csrf_token()}}";
                    d.category_filter = $('#custom-filter').val();
                }
        },
        columns: [
        { data: 'id', name: 'id', orderable:true },
        { data: 'name', name: 'name', orderable:true },
        { data: 'question', name: 'question', orderable:false },
        { data: 'question_ar', name: 'question_ar', orderable:false },
        { data: 'action', name: 'action', orderable:true  },
        ],
        "columnDefs": [
        { "searchable": false, "targets": 0 }
        ]
        , language: {
        searchPlaceholder: "Search by category and question"
        },
        initComplete:function()
        {
          loadStatusFilter();
        }
});
});  


</script>
@endsection

