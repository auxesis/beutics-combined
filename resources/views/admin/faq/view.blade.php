@extends('layouts.admin.admin')
@section('uniquecss')

<style type="text/css">
    table.owntable th {
        background-color: #337ab7;
        color: #fff;
    }
</style>

@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>View FAQ</h5>

            </header>
            <div id="collapse2" class="body">
              <div class="row">
                 @include('message')
                <div class="col-sm-12 col-lg-12">

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Category</b></div>
                        <div class="col-lg-8"><?= $row->name; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Question</b></div>
                        <div class="col-lg-8"><?= $row->question; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Question(Arabic)</b></div>
                        <div class="col-lg-8"><?= $row->question_ar; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Answer</b></div>
                        <div class="col-lg-8"><?= $row->answer; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Answer(Arabic)</b></div>
                        <div class="col-lg-8"><?= $row->answer_ar; ?></div> <div class="clearfix"></div>
                    </div>

                    <div class="col-lg-12 border_details">
                        <div class="col-lg-4"><b>Created At</b></div>
                        <div class="col-lg-8"><?= date('Y-m-d h:i A',strtotime($row->created_at)); ?></div> <div class="clearfix"></div>
                    </div>


                  <div class="clearfix"></div><br><br>
                </div>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>

@endsection