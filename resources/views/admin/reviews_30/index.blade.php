@extends('layouts.admin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
 
@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>Reviews</h5>
                <a class="btn btn-primary pull-right" href="{{route('reviews.create')}}" style="margin-top:4px;">Add Review</a>
            </header>
            <div id="collapse4" class="body">
              @include('message')
              
               <table id="user_datatable" class="table table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th width="20%">Service Provider</th>
                        <th width="20%">Customer Name</th>
                        <th width="35%">Review Comment</th>
                        <th width="10%">Overall Rating</th>
                        <th width="10%">Approved</th>
                        <th width="20%">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
                  
            </div>
        </div>
    </div>
</div>
@endsection
  
@section('uniquescript')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>

<script type="text/javascript">

  function callNative(){

    $("#user_datatable").DataTable().draw();
  }
  function loadStatusFilter()
  {

    jQuery("#user_datatable_filter").parent().parent().
      append('<div id="user_datatable_status_filter" class="col-sm-12 col-md-2"><label>Review Status:</label><select name="columns[testtest]" class="form-control input-sm" id="custom-filter" onchange="callNative()"><option value="">All Reviews</option><option value="pending">Pending</option><option value="approved">Approved</option></select></div><div id="user_datatable_review_type" class="col-sm-12 col-md-2"><label>Review Filter:</label><select name="columns[testtest]" class="form-control input-sm" id="custom-visibility-filter" onchange="callNative()"><option value="">All Reviews</option><option value="customer">Customer Reviews</option><option value="admin">Admin Reviews</option></select></div>');

    jQuery('#user_datatable_length').parent().closest("div.col-sm-12").removeClass("col-md-6");  
    jQuery('#user_datatable_length').parent().closest("div.col-sm-12").addClass("col-md-4");  

    jQuery('#user_datatable_filter').parent().closest("div.col-sm-12").removeClass("col-md-6");  
    jQuery('#user_datatable_filter').parent().closest("div.col-sm-12").addClass("col-md-4");

    jQuery('#user_datatable_status_filter').closest("div.col-sm-12").removeClass("col-md-6");  
    jQuery('#user_datatable_status_filter').closest("div.col-sm-12").addClass("col-md-4");
  }

  function deleteRow(obj)
  {
    event.preventDefault(); // prevent form submit
    swal({
      title: "Are you sure?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {        
       obj.submit();

      } else {
         swal.close();
      }
    });
  }

  function changeApproval(id)
    {
      swal({
          title: "Are you sure?",
          type: "warning",
          showCancelButton: "No",
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Yes",
          cancelButtonText: "No",
        },
          function(){
            jQuery.ajax({
            url: '{{route('review.status.update')}}',
            type: 'POST',
            data:{review_id:id},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
              $("#approvalStatus_"+id).html(response);
            }
            });
          }
      );
    }

    $(function() {
        var table = $('#user_datatable').DataTable({
          stateSave: true,
        processing: true,
        serverSide: true,
        order: [[0, "desc" ]],
        "ajax":{
        "url": '{!! route('reviews.getdata') !!}',
                "dataType": "json",
                "type": "POST",
                data: function(d) {
                    d._token= "{{csrf_token()}}";
                    d.status_filter = $('#custom-filter').val();
                    d.review_type = $('#custom-visibility-filter').val();
                }
        },
        columns: [
        { data: 'sp_id', name: 'sp_id', orderable:true },
        { data: 'customer_name', name: 'customer_name', orderable:true  },
        { data: 'description', name: 'description', orderable:true  },
        { data: 'overall_review', name: 'overall_review', orderable:true  },
        { data: 'is_approved', name: 'is_approved', orderable:true  },
        { data: 'action', name: 'action', orderable:true  },
        ],
        "columnDefs": [
        { "searchable": false, "targets": 0 }
        ]
        , language: {
        searchPlaceholder: "Search by name"
        },
        initComplete:function(){

          loadStatusFilter();
        }
});
});  


</script>
@endsection

