<?php if($final_arr['search_name'] == 'staff'){ ?>
 
	@foreach($final_arr['staff_data'] as $staff_key => $staff)
              
        <li>
            <div class="row">
                <div class="col-md-6">
                    <input type="radio" name="staff_id" value="{{$staff_key}}" class="validate[required]">
                    <?php echo $staff;?>
                </div>
                <div class="col-md-6 text-right" id="staff_{{$staff_key}}">
                </div>
            </div>    
        </li>
       
    @endforeach
  
<?php } else{ ?>

    @foreach($final_arr['service_data'] as $service_key => $service)
                  
        <li>
        <div class="row"> 
        <div class="col-md-6">
            <input type="checkbox" name="service_id[]" value="{{$service_key}}" class="validate[required]">
            <?php echo $service;?>
        </div>
        <div class="col-md-6 text-right">
            <!-- <input type="radio" value="1" name="service_like_dislike{{$service_key}}">Like
            <input type="radio" value="0" name="service_like_dislike{{$service_key}}">Dislike -->
        </div>      
        
        </div>  
        </li>
       
    @endforeach
    
    <?php } ?>
