@extends('layouts.admin.admin')

@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/jquery.gritter/css/jquery.gritter.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/themes/default/css/uniform.default.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />


@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>{{$title}}</h5>
          

            </header>
             @include('message')
            <div id="collapse2" class="body">
                <!-- <form class="form-horizontal" id="popup-validation"> -->
                {!! Form::model($row,['route' => ['reviews.update',$row->id], 'method' => 'PATCH', 'class' => 'form-horizontal','id' => 'popup-validation','enctype' => 'multipart/form-data']) !!}
                {{ csrf_field() }}

                    <div class="form-group">
                        {{Form::label('sp_id', 'Service Providers', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::select('sp_id', $service_providers,null,['class' => 'form-control validate[required] myselect','placeholder'=>'Select Service Provider','onchange'=>'getServiceProviderData(this)','id'=>'sp_id']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('customer_name', 'Customer Name', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('customer_name',null, ['class' => 'form-control validate[required]']) }}
                        </div>
                    </div>

                    
                    <ul class="emotions">
                    <?php $j=1; //print_r($rating_row[0]->ques_id);die;

                    //$row->staff_id
                    //dd($reviews_ques_arr);
                    ?>

                    @foreach($reviews_ques_arr as $ques_key => $ques)
                  
        
                        <li>
                            <h3><?php echo $ques['name'];?></h3>
                       
                        <ul>
                        <?php $i=1;?>
                        @foreach($rating_arr as $rate_key => $rating)
                            <li class="rating{{$i}}">
                                <h5>
                                    <?php 
                                     
                                    if($rating_row[$j-1]->rating == $i){
                                        $checked = true;
                                        
                                    }else{
                                        $checked = '';
                                         
                                    }

                                    ?>
                                    {{ Form::radio('question'.$j,$i,$checked,['class' => 'validate[required]']) }}<?php echo $rating['name'];?>
                                </h5>
                            </li>
                            <?php $i++; ?>
                        @endforeach
                        </ul>
                        </li>
                        <?php $j++; ?>
                    @endforeach

                    </ul>
                    <div id="sp_detail" class="row">

                    </div>
                    <div class="form-group txtarea row">
                        {{Form::label('description', 'Tell us more', ['class' => 'control-label col-lg-12'])}}
                        <div class="col-lg-12">
                            {{ Form::textarea('description',null, ['class' => 'form-control validate[required]','rows'=>'2']) }}
                        </div>
                    </div>

                    <div class="form-actions no-margin-bottom">
                        <input type="submit" value="Submit" class="btn btn-primary">
                    </div>
                    <br><br><br><br>
                </form>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->

    <input type="hidden" id="selected_staff_id" value="<?php echo $row->staff_id ?>">
    <input type="hidden" id="selected_staff_like_dislike" value="<?php echo $row->staff_like_dislike ?>">


    <?php foreach($review_sp_services_data as $k => $v){
        //dd($v->service_id);
        //like_dislike
    }

    ?>
    <input type="hidden" id="sp_sevices_data" value='<?php echo json_encode($review_sp_services_data);  ?>' />
</div>
@endsection
@section('uniquescript')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>

     <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/holder/2.4.1/holder.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/jquery.uniform.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script>
        $(function() {
          Metis.formValidation();
        });

        $(function() {
           $(".myselect").select2();
        });
    </script>

    <script>

        $(document).ready(function(){
            var spid = document.getElementById('sp_id');
            getServiceProviderData(spid);
        });

        function getServiceProviderData(sp_id)
        {
            var id = sp_id.value;
            var staff_id = $('#selected_staff_id').val();
            var sp_sevices_data = $('#sp_sevices_data').val();
            var selected_staff_like_dislike = $('#selected_staff_like_dislike').val();

            

            jQuery.ajax({
                url: '{{route('get-sp-staff-service')}}',
                type: 'POST',
                data:{sp_id:id,staff_id:staff_id,sp_sevices_data:sp_sevices_data,selected_staff_like_dislike:selected_staff_like_dislike},
                headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    if(response == 'nodata'){
                        response = "<p class='error'>Please add staff and services.</p>"
                    }
                  $("#sp_detail").html(response);
                  console.log(response);
                }
            });
        }
    </script>

@endsection