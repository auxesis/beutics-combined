@extends('layouts.admin.admin')
@section('uniquecss') 

<link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12 forumcmts">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>Forum Detail </h5>
                
            </header>
            <div id="collapse4" class="body">
              <?php if(!empty($categories)){ ?>
              <h4>Forum Categories</h4>
              <?php 
              $str = '';
              foreach($categories as $cat){ 
                $str .= $cat['category_name'].', ';
              } ?>
              <p><?php echo rtrim($str, ', ');?></p><br>
              <?php } ?>
              <h4>{{$forum['subject']}}</h4>
              <p><i>Requested By</i> {{$username}} &nbsp; &nbsp; <i>Posted On</i> {{ date('M d Y', strtotime($posted_on))}}</p>

              @include('message')
                    
               
                      @foreach($banners as $image)
                          @if($image['banner_path'])
                         
                            <div class="imgss">
                           <a href="#"
                              class="thumbnail"
                              data-image-id="" 
                              data-toggle="modal" 
                              data-title=""
                              data-image={{ asset('/public/sp_uploads/forum/'.$image['banner_path']) }}
                              data-target="#image-gallery"
                            >
                              <img src={{ asset('/public/sp_uploads/forum/'.$image['banner_path']) }} width="100%"/>
</a>
                            </div>
                          
                          @endif
                      @endforeach



                
                      <div class="row">
                      <div class="col-lg-12">
                      <p class="descp"><?= $forum['message']; ?></p>
                      </div></div>
                      
                     <h5>{{$responses}} Responses</h5>
                     
                      <div>
                        <ul class="fqclist">
                        @foreach($responses_list as $comment_item) 
                        <li>                           
                            <div class="fimages">
                              @if($comment_item['sp_image'])
                                <img src={{ asset('/public/sp_uploads/profile/'.$comment_item['sp_image']) }} width="50" />
                              @endif

                              @if($comment_item['user_image'])
                              <img src={{ asset('/public/'.$comment_item['user_image']) }} width="50" />
                              @endif

                              @if((!$comment_item['sp_image'] || $comment_item['sp_image'] =='') && (!$comment_item['user_image'] || $comment_item['user_image'] ==''))
                              
                              <img src={{ asset('/public/images/defualt.jpeg') }} width="50" />

                              @endif
                              </div>

                              <div class="fqlist-right">
                              <h4>
                                {{$comment_item['sp_username']}}{{$comment_item['username']}}                                
                                @if($comment_item['store_name'])
                                   <span>({{$comment_item['store_name']}})</span>
                                @endif
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <form method="post" action="{{route('forum.comment.delete',[$comment_item['id']])}}" accept-charset="UTF-8" style="display:inline" onsubmit="return deleteRow(this)"><input name="_method" value="DELETE" type="hidden">
                                {{csrf_field()}} <span><button data-toggle="tooltip"  title="Delete" type="submit" class="btn btn-danger btn-xs delete-action"><i class="fa fa-trash-o" aria-hidden="true"></i></button></span>
                               </form>

                              </h4>
                              
                              <p>{{ date('M d Y', strtotime($comment_item['date']))}}</p>
                            </div>                          
                            <p  class="descp">{{ $comment_item['message']}}</p>
                            
                            @if($comment_item['getAssociatedCommentBannerImages'])
                                 @foreach($comment_item['getAssociatedCommentBannerImages'] as $banner_img)   
                                 
                                 <div class="imgss">                               
                                 <img src={{asset('/public/sp_uploads/forum_reply_banners/'.$banner_img->banner_path)}}  width="100%" />
                                 </div>

              

                                 @endforeach
                            @endif                          

                           
                          </li>
                        @endforeach
                        </ul>
                      </div>
                    


            
                  
            </div>
        </div>
    </div>

    <div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="image-gallery-title"></h4>
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <img id="image-gallery-image" class="img-responsive" src="">
                    </div>
                    <div class="modal-footer galfooter">
                        <button type="button" class="btn btn-secondary previous" id="show-previous-image"><i class="fa fa-arrow-left"></i>
                        </button>

                        <button type="button" id="show-next-image" class="btn btn-secondary next"><i class="fa fa-arrow-right"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection
@section('uniquescript')
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>
<script>
let modalId = $('#image-gallery');

$(document)
  .ready(function () {

    loadGallery(true, 'a.thumbnail');

    //This function disables buttons when needed
    function disableButtons(counter_max, counter_current) {
      $('#show-previous-image, #show-next-image')
        .show();
      if (counter_max === counter_current) {
        $('#show-next-image')
          .hide();
      } else if (counter_current === 1) {
        $('#show-previous-image')
          .hide();
      }
    }

    /**
     *
     * @param setIDs        Sets IDs when DOM is loaded. If using a PHP counter, set to false.
     * @param setClickAttr  Sets the attribute for the click handler.
     */

    function loadGallery(setIDs, setClickAttr) {
      let current_image, selector, counter = 0;
      $('#show-next-image, #show-previous-image')
        .click(function () {
          if ($(this)
            .attr('id') === 'show-previous-image') {
            current_image--;
          } else {
            current_image++;
          }

          selector = $('[data-image-id="' + current_image + '"]');
          updateGallery(selector);
        });

      function updateGallery(selector) {
        let $sel = selector;
        current_image = $sel.data('image-id');
        $('#image-gallery-title').text($sel.data('title'));
        $('#image-gallery-image').attr('src', $sel.data('image'));       
        disableButtons(counter, $sel.data('image-id'));
      }

      if (setIDs == true) {
        $('[data-image-id]')
          .each(function () {
            counter++;
            $(this)
              .attr('data-image-id', counter);
          });
      }
      $(setClickAttr)
        .on('click', function () {
          updateGallery($(this));
        });
    }
  });

// build key actions
$(document)
  .keydown(function (e) {
    switch (e.which) {
      case 37: // left
        if ((modalId.data('bs.modal') || {})._isShown && $('#show-previous-image').is(":visible")) {
          $('#show-previous-image')
            .click();
        }
        break;

      case 39: // right
        if ((modalId.data('bs.modal') || {})._isShown && $('#show-next-image').is(":visible")) {
          $('#show-next-image')
            .click();
        }
        break;

      default:
        return; // exit this handler for other keys
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)
  });

  function deleteRow(obj)
  {
    event.preventDefault(); // prevent form submit
    swal({
      title: "Are you sure?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {        
       obj.submit();

      } else {
         swal.close();
      }
    });
  }


</script>
@endsection