@extends('layouts.admin.admin')

@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/jquery.gritter/css/jquery.gritter.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/themes/default/css/uniform.default.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css">
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>{{$title}}</h5>
          

            </header>
          
            <div id="collapse2" class="body">
               @include('message')
                <!-- <form class="form-horizontal" id="popup-validation"> -->
                {!! Form::open(['route' => 'tier.store', 'method' => 'POST', 'class' => 'form-horizontal','id' => 'popup-validation']) !!}
                {{ csrf_field() }}
                          
                    <div class="form-group">
                      {{Form::label('title', 'Title', ['class' => 'control-label col-lg-4'])}}
                      <div class="col-lg-4">
                          {{ Form::text('title','', ['class' => 'form-control validate[required]']) }}
                      </div>
                    </div>

                      <div class="form-group">
                         {{Form::label('commission', 'Commission', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('commission','', ['class' => 'form-control validate[required]']) }}
                        </div>
                      </div>
                      <div class="form-group">
                        {{Form::label('redeem', 'Redeem', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('redeem','', ['class' => 'form-control validate[required]']) }}
                        </div>
                      </div>

                      <div class="form-group">

                        {{Form::label('earning', 'Earning', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('earning','', ['class' => 'form-control validate[required]']) }}
                        </div>
                      </div>

                      <div class="form-actions no-margin-bottom">
                          <input type="submit" value="Submit" class="btn btn-primary" style="margin-left:360px;">
                      </div>
                      <br><br><br><br>
                </form>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
@endsection
@section('uniquescript')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>

     <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/holder/2.4.1/holder.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/jquery.uniform.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>

    <script>
        $(function() {
          Metis.formValidation();
        });
    </script>


@endsection