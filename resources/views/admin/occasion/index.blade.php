@extends('layouts.admin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
 
@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>Occasions</h5>
                <a class="btn btn-primary pull-right" href="{{route('occasion.create')}}" style="margin-top:4px;">Create Occasion</a>
            </header>
            <div id="collapse4" class="body">
              @include('message')
              
               <table id="user_datatable" class="table table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Name(Arabic)</th>
                        <th>Created At</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
                  
            </div>
        </div>
    </div>
</div>
@endsection

@section('uniquescript')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>
<script type="text/javascript">

  function deleteRow(obj,asso)
  {
    event.preventDefault(); // prevent form submit
   
    var msg = "Are you sure?";
    if(asso!=''){
        var str = asso;
        var res = str.split("__");
        var msg = "You cannot delete this Occasion because "+res+" are associated with it.";
                           
   
    }
    if(asso!=''){ 
      swal({
          title: msg,
          type: "warning",
          showCancelButton: true,
          showConfirmButton: false,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          cancelButtonText: "Ok",
          closeOnConfirm: false,
          closeOnCancel: false,

        },
        function(isConfirm){
          if (isConfirm) {        
           obj.submit();

          } else {
             swal.close();
          }
        });
    }
    else{
      swal({
          title: msg,
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          cancelButtonText: "No",
          closeOnConfirm: false,
          closeOnCancel: false,

        },
        function(isConfirm){
          if (isConfirm) {        
           obj.submit();

          } else {
             swal.close();
          }
        });
      }
  }
 
    $(function() {
        var table = $('#user_datatable').DataTable({
          stateSave: true,
        processing: true,
        serverSide: true,
        order: [[0, "desc" ]],
        "ajax":{
        "url": '{!! route('occasion.getdata') !!}',
                "dataType": "json",
                "type": "POST",
                "data":{ _token: "{{csrf_token()}}"}
        },
        columns: [
        { data: 'name', name: 'name', orderable:true },
        { data: 'name_ar', name: 'name_ar', orderable:true },
        { data: 'created', name: 'created', orderable:true },
        { data: 'action', name: 'action', orderable:true  },
        ],
        "columnDefs": [
        { "searchable": false, "targets": 0 }
        ]
        , language: {
        searchPlaceholder: "Search by name"
        },
});
});  


</script>
@endsection

