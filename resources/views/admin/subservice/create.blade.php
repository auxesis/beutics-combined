@extends('layouts.admin.admin')

@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css">
  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/jquery.gritter/css/jquery.gritter.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/themes/default/css/uniform.default.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.theme.min.css">
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/validationEngine.jquery.min.css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.min.css">
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <header class="dark">
                <div class="icons"><i class="fa fa-check"></i></div>
                <h5>{{$title}}</h5>
          

            </header>
             @include('message')
            <div id="collapse2" class="body">
                <!-- <form class="form-horizontal" id="popup-validation"> -->
                {!! Form::open(['route' => 'subservice.store', 'method' => 'POST', 'class' => 'form-horizontal','id' => 'popup-validation','enctype' => 'multipart/form-data']) !!}
                {{ csrf_field() }}
 
                    <div class="form-group">
                        {{Form::label('category_name', 'Category Name', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::select('cat_id', $categories,null,['class' => 'form-control validate[required]','id'=>'category','placeholder'=>'Select Categories']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('category_name', 'Sub Category Name', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4" id="subcat_div">
                            {{ Form::select('subcat_id', [],null,['class' => 'form-control validate[required] sub_category','placeholder'=>'Select Sub Categories']) }}
                        </div>
                    </div>
                    
                    <div class="form-group">
                        {{Form::label('parent_service_id', 'Service Name', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4" id="subcat_services_div">
                            {{ Form::select('parent_service_id', [],null,['class' => 'form-control validate[required]','placeholder'=>'Select Service Name','id'=>'parent_service_id']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{Form::label('name', 'Sub Service Name', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('name','', ['class' => 'form-control validate[required]', 'maxlength' => '50']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('name_ar', 'Sub Service Name(Arabic)', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('name_ar','', ['class' => 'form-control validate[required]', 'maxlength' => '50']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('price', 'Beutics Price', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::text('price','', ['class' => 'form-control']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('description', 'Description', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::textarea('description','', ['class' => 'form-control validate[required]','rows'=>'2', 'maxlength' => '1000']) }}
                        </div>
                    </div>

                     <div class="form-group">
                        {{Form::label('description_ar', 'Description(Arabic)', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                            {{ Form::textarea('description_ar','', ['class' => 'form-control validate[required]','rows'=>'2', 'maxlength' => '1000']) }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('time', 'Service Time', ['class' => 'control-label col-lg-4'])}}
                        <div class="input-group  col-lg-4">
                      
                            <!-- {{ Form::text('time',null, ['class' => 'form-control validate[required]','autocomplete'=>'off']) }} -->
                            <div class="hourbox">
                            <div class="hhbox control-label">Hours</div>
                            <div class="hhbox quantity">
                             <input type="number" name="serviceTimeHour" placeholder="Hours" value='0' min="0" max="23" step="1" class="validate[required]">
                             </div>
                           </div>
                           <div class="hourbox">
                            <div class="hhbox control-label">Minutes</div>
                             <div class="hhbox quantity">
                          <input type="number" name="serviceTimeMin" placeholder="Minutes" value='0' min="0" max="59" step="1" class="validate[required]">
                            </div>
                          </div>
                        </div>

                    </div>

                  <!--   <div class="form-group">
                        {{Form::label('time', 'Add On Services', ['class' => 'control-label col-lg-4'])}}
                        <div class="col-lg-4">
                           {{ Form::select('addon[]', $services,null,['class' => 'form-control myselect','data-live-search' => 'true','multiple']) }}
                        </div>
                    </div> -->
                    

                    <div class="form-actions no-margin-bottom">
                        <input type="submit" value="Submit" class="btn btn-primary">
                    </div>
                    <br><br><br><br>
                </form>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
@endsection
@section('uniquescript')
    <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/languages/jquery.validationEngine-en.min.js"></script>

     <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/holder/2.4.1/holder.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Uniform.js/2.1.2/jquery.uniform.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
    <script>
        $(function() {
          Metis.formValidation();
        });
    </script>
<script type="text/javascript">
    $(document).ready(function(){
            jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div></div>').insertAfter('.quantity input');
    jQuery('.quantity').each(function() {
      var spinner = jQuery(this),
        input = spinner.find('input[type="number"]'),
        btnUp = spinner.find('.quantity-up'),
        btnDown = spinner.find('.quantity-down'),
        min = input.attr('min'),
        max = input.attr('max');

      btnUp.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue >= max) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue + 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

      btnDown.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue <= min) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue - 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

    });
}); 
    </script>
    <script type="text/javascript">
        $("#category").change(function()
        {
            var id = $(this).val();
            
            jQuery.ajax({
            url: '{{route('new-subcategory.ajax')}}',
            type: 'POST',
            data:{cat_id:id},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
              $("#subcat_div").html(response);
            }
            });
              
        });

        $("body").on('change', '.sub_category', function()
        {
            var id = $(this).val();
            var cat_id = $("#category option:selected").val();
            
            jQuery.ajax({
            url: '{{route('subservices.ajax')}}',
            type: 'POST',
            data:{subcat_id:id,cat_id:cat_id},
            headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
              $("#subcat_services_div").html(response);
            }
            });
              
        });
    </script>
    <script type="text/javascript">
      $(".myselect").select2();
    </script>

     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.min.js"></script>

    <script type="text/javascript">
        $('.clockpicker').clockpicker({'donetext':'Done'});
    </script>


@endsection