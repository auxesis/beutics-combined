@extends('layouts.admin.admin')

@section('uniquecss')
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.5/fullcalendar.min.css">
  <style>
    a{
      color:black;
    }
  </style>
@endsection

@section('content')
@include('message')
  <div class="text">
    <ul class="stats_box">
      <a href="{{route('users.index')}}">
        <li>
            <div class="sparkline"><i class="fa fa-users" style="font-size:30px;"></i></div>
            <div class="stat_text">
                <strong><?= $user_count; ?></strong>Total Customers
            </div>
        </li>
      </a>
      <a href="{{route('sp.index')}}">
        <li>
            <div class="sparkline"><i class="fa fa-users" style="font-size:30px;"></i></div>
            <div class="stat_text">
                <strong><?= $sp_count; ?></strong>Total Service Providers
            </div>
        </li>
      </a>
      <a href="{{route('staff.index')}}">
        <li>
            <div class="sparkline"><i class="fa fa-user" style="font-size:30px;"></i></div>
            <div class="stat_text">
                <strong><?= $sp_staff; ?></strong>Total Service Providers Staff
            </div>
        </li>
      </a>
       <a href="{{route('sp.index')}}">
        <li>
            <div class="sparkline"><i class="fa fa-clock-o" style="font-size:30px;"></i></div>
            <div class="stat_text">
                <strong><?= $sp_pending_req; ?></strong>Total Pending SP Requests
            </div>
        </li>
      </a>
      <a href="{{route('sp.pending.offer')}}">
        <li>
            <div class="sparkline"><i class="fa fa-gift" style="font-size:30px;"></i></div>
            <div class="stat_text">
                <strong><?= $sp_pending_offer_req; ?></strong>Total Pending Offers
            </div>
        </li>
      </a>

      <a href="{{route('orders.new')}}">
        <li>
            <div class="sparkline"><i class="fa fa-shopping-cart" style="font-size:30px;"></i></div>
            <div class="stat_text">
                <strong><?= $new_bookings; ?></strong>Total Awaiting Orders
            </div>
        </li>
      </a>

      <a href="{{route('orders.confirm')}}">
        <li>
            <div class="sparkline"><i class="fa fa-shopping-cart" style="font-size:30px;"></i></div>
            <div class="stat_text">
                <strong><?= $confirmed_bookings; ?></strong>Total Confirmed Orders
            </div>
        </li>
      </a>

      <a href="{{route('orders.complete')}}">
        <li>
            <div class="sparkline"><i class="fa fa-shopping-cart" style="font-size:30px;"></i></div>
            <div class="stat_text">
                <strong><?= $completed_bookings; ?></strong>Total Completed Orders
            </div>
        </li>
      </a>

      <a href="{{route('orders.cancelled')}}">
        <li>
            <div class="sparkline"><i class="fa fa-shopping-cart" style="font-size:30px;"></i></div>
            <div class="stat_text">
                <strong><?= $cancelled_bookings; ?></strong>Total Cancelled Orders
            </div>
        </li>
      </a>
      <a href="{{route('reviews.index')}}">
        <li>
            <div class="sparkline"><i class="fa fa-star" style="font-size:30px;"></i></div>
            <div class="stat_text">
                <strong><?= $avg_review; ?></strong>Average Beutics Ratings
            </div>
        </li>
      </a>

       <a href="{{route('e-card.index')}}">
        <li>
            <div class="sparkline"><i class="fa fa-star" style="font-size:30px;"></i></div>
            <div class="stat_text">
                <strong><?= $total_ecards; ?></strong>Total E-cards
            </div>
        </li>
      </a>

    </ul>
</div>
<hr>
                       
@endsection

@section('uniquescript')
  <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.5/fullcalendar.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.18.4/js/jquery.tablesorter.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-sparklines/2.1.2/jquery.sparkline.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/flot/0.8.3/jquery.flot.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/flot/0.8.3/jquery.flot.selection.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/flot/0.8.3/jquery.flot.resize.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
<script>   
        // var ctx = $('#graph');
        // var keys = <?php //echo json_encode($keys);?>;
        // var values = <?php //echo json_encode($values);?>;
        // var graph = new Chart(ctx, {
        //     type: 'bar',
        //     data: {
        //         labels: keys,
        //         datasets: [{
        //             label: "Users",
        //             data: values,
        //             fill: false,
        //             borderColor: '#07C',
        //         }]
        //     }
        // });

        // function addData() {
        //     graph.data.labels.push('June')
        //     graph.data.datasets[0].data.push(6);
        //     graph.update();
        // }  
  </script>
  <script>
    $(function() {
      Metis.dashboard();
    });
</script>
@endsection
