@extends('layouts.admin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $('#addmore').click(function(){
      var count = parseInt($('.append_here').attr('data-count'));
      if(count <= 4){
        count++;
        $('.append_here').append('<input type="file" class="form-control" id="images'+count+'" name="image'+count+'" accept="image/*">');
        $('.append_here').attr('data-count',count);
      }
      else $('#addmore').remove();

      if(count == 5) $('#addmore').remove();
      
    });
  });
</script>
 
@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12 forumcmts">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>{{ __('messages.Session Faqs') }} - {{ ucfirst($user_data->name) }} </h5>
                
            </header>
            <div id="collapse4" class="body">
              @foreach($faq_responses as $faqs)
              <h4>Q. {{ $faqs['question'] }}</h4>
              <p><i>{{ __('messages.Posted On') }}</i> {{ date('M d Y h:i A', strtotime($faqs['created_at']))}}</p>
              <div class="row">
                <div class="col-lg-12">
                  <p class="descp">Ans. {{ $faqs['answer'] }}</p>
                </div>
              </div>
              <hr>
            @endforeach
            </div>
        </div>
    </div>
</div>
@endsection