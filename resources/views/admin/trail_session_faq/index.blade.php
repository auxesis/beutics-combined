@extends('layouts.admin.admin')
@section('uniquecss')

  <link rel="stylesheet" href="{{ url('/') }}/assets/lib/sweetalert/sweetalert.css">
 
@endsection
@section('content')
  <div class="row">
  <div class="col-lg-12">
        <div class="box">
            <header>
                <div class="icons"><i class="fa fa-table"></i></div>
                <h5>{{ __('messages.Responded Queries') }}</h5>
            </header>
            <div id="collapse4" class="body">
              @include('message')
               <table id="user_datatable" class="table table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th width="15%">Store Name</th>
                        <th width="15%">Session Name</th>
                        <th width="15%">Username</th>
                        <th width="10%">Email</th>
                        <th width="10%">Contact number</th>
                        <th width="15%">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
                  
            </div>
        </div>
    </div>
</div>
@endsection

@section('uniquescript')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/lib/sweetalert/sweetalert.js')}}"></script>
<script type="text/javascript">
  function callNative(){

    $("#user_datatable").DataTable().draw();
  }
  function loadStatusFilter()
  {

    jQuery("#user_datatable_filter").parent().parent().
      append('<div id="user_datatable_category_filter" class="col-sm-12 col-md-2"><label>Trail Sessions:</label><select name="columns[testtest]" class="form-control input-sm" id="custom-filter" onchange="callNative()"><option value="">All Sessions</option><?php foreach($session_list as $key => $name){ ?><option value="<?= $key?>"><?= $name?></option><?php } ?></select></div>');
  }
</script>

<script type="text/javascript">

  function deleteRow(obj)
  {
    event.preventDefault(); // prevent form submit
    swal({
      title: "Are you sure?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm) {        
       obj.submit();

      } else {
         swal.close();
      }
    });
  }

    $(function() {
        var table = $('#user_datatable').DataTable({
          stateSave: true,
        processing: true,
        serverSide: true,
        info : false,
        order: [[0, "desc" ]],
        "ajax":{
        "url": '{!! route('trail-session-faq.getdata') !!}',
                "dataType": "json",
                "type": "POST",
                data: function(d) {
                    d._token= "{{csrf_token()}}";
                    d.session_filter = $('#custom-filter').val();
                }
        },
        columns: [
        //{ data: 'id', name: 'id', orderable:true },
        { data: 'store_name', name: 'store_name', orderable:true },
        { data: 'session_name', name: 'session_name', orderable:true },
        { data: 'username', name: 'username', orderable:true },
        { data: 'email', name: 'email', orderable:true },
        { data: 'mobile_no', name: 'mobile_no', orderable:false },
        { data: 'action', name: 'action', orderable:true  },
        ],
        "columnDefs": [
        { "searchable": false, "targets": 0 }
        ]
        , language: {
        infoFiltered : "",
        searchPlaceholder: "Search by session, username, email, mobile"
        },
        initComplete:function()
        {
          loadStatusFilter();
        }
});
});  


</script>
@endsection

