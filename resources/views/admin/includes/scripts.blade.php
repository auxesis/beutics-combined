<script src="{{ url('/') }}/assets/lib/jquery/jquery.js"></script>

<!--Bootstrap -->
<script src="{{ url('/') }}/assets/lib/bootstrap/js/bootstrap.js"></script>
<!-- MetisMenu -->
<script src="{{ url('/') }}/assets/lib/metismenu/metisMenu.js"></script>
<!-- onoffcanvas -->
<script src="{{ url('/') }}/assets/lib/onoffcanvas/onoffcanvas.js"></script>
<!-- Screenfull -->
<script src="{{ url('/') }}/assets/lib/screenfull/screenfull.js"></script>


<!-- Metis core scripts -->
<script src="{{ url('/') }}/assets/js/core.js"></script>
<!-- Metis demo scripts -->
<script src="{{ url('/') }}/assets/js/app.js"></script>

<script>
    less = {
        env: "development",
        relativeUrls: false,
        rootpath: "/assets/"
    };
</script>
<script src="{{ url('/') }}/assets/js/style-switcher.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/2.7.1/less.js"></script>

@yield('uniquescript')

