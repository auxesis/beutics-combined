
<style type="text/css">
    .quantity {
  position: relative;
}

input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button
{
  -webkit-appearance: none;
  margin: 0;
}

input[type=number]
{
  -moz-appearance: textfield;
}
.hourbox .hhbox {
    display: inline-block;
    vertical-align: middle;
    padding: 0 5px;
}
.hourbox {
    float: left;
    /*margin-right: 30px;*/
}
.quantity input {
  width: 100px;
  height: 42px;
  line-height: 1.65;
  float: left;
  display: block;
  padding: 0;
  margin: 0;
  padding-left: 20px;
  border: 1px solid #eee;
}

.quantity input:focus {
  outline: 0;
}

.quantity-nav {
  float: left;
  position: relative;
  height: 42px;
}

.hourbox .control-label {
    font-weight: bold;
}

.quantity-button {
  position: relative;
  cursor: pointer;
  border-left: 1px solid #eee;
  width: 20px;
  text-align: center;
  color: #333;
  font-size: 18px;
  font-family: "Trebuchet MS", Helvetica, sans-serif !important;
  line-height: normal;
  -webkit-transform: translateX(-100%);
  transform: translateX(-100%);
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  -o-user-select: none;
  user-select: none;
}

.quantity-button.quantity-up {
  position: absolute;
  height: 50%;
  top: 0;
  border-bottom: 1px solid #eee;
}

.quantity-button.quantity-down {
  position: absolute;
  bottom: -1px;
  height: 50%;
}
    
</style>
<footer class="Footer bg-dark dker">
    <p>Copyright &copy; <?php echo date('Y') ?> Beutics All rights reserved. </p>
</footer>
<!-- /#footer -->
