<meta charset="UTF-8">
<!--IE Compatibility modes-->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!--Mobile first-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title><?= $title;?></title>

<meta name="description" content="Free Admin Template Based On Twitter Bootstrap 3.x">
<meta name="author" content="">
<link rel="shortcut icon" href="{{asset('assets/img/favicon.png')}}"/>
<meta name="msapplication-TileColor" content="#5bc0de" />
<meta name="msapplication-TileImage" content="{{ url('/') }}/assets/img/metis-tile.png" />

<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Bootstrap -->
<link rel="stylesheet" href="{{ url('/') }}/assets/lib/bootstrap/css/bootstrap.css">

<!-- Font Awesome -->
<link rel="stylesheet" href="{{ url('/') }}/assets/lib/font-awesome/css/font-awesome.css">

<!-- Metis core stylesheet -->
<link rel="stylesheet" href="{{ url('/') }}/assets/css/main.css">

<!-- metisMenu stylesheet -->
<link rel="stylesheet" href="{{ url('/') }}/assets/lib/metismenu/metisMenu.css">

<!-- onoffcanvas stylesheet -->
<link rel="stylesheet" href="{{ url('/') }}/assets/lib/onoffcanvas/onoffcanvas.css">

<!-- animate.css stylesheet -->
<link rel="stylesheet" href="{{ url('/') }}/assets/lib/animate.css/animate.css">

<!-- --------------------------- -->


<link rel="stylesheet" href="{{ url('/') }}/assets/css/style-switcher.css">
<link rel="stylesheet/less" type="text/css" href="{{ url('/') }}/assets/less/theme.less">

<style>
	.error{
		color:red;
	}
</style>

@yield('uniquecss')