<div id="left">
    <div class="media user-media bg-dark dker">
        <div class="user-media-toggleHover">
            <span class="fa fa-user"></span>
        </div>
        <?php
          $user_data = loginData(); 
          $name =  $user_data->name;
          $profile =  $user_data->profile;

          ?>
        <div class="user-wrapper bg-dark">
            <a class="user-link" href="">
                <!-- <img class="media-object img-thumbnail user-img" alt="User Picture" src="{{ url('/').'/'.$profile }}" width="70" height="70"> -->
                <img class="media-object img-thumbnail user-img" alt="User Picture" src="{{ url('/').'/' }}assets/img/user.gif" style="width:70px; height:70px;">
            </a>

            <div class="media-body">
                <h5 class="media-heading"> </h5>
                <ul class="list-unstyled user-info">
                    <li><a href="" style="color:white;">{{ucfirst($name)}}</a></li>
                    <li>Administrator<br>
                        <small>&nbsp;</small>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- #menu -->
    <ul id="menu" class="bg-blue dker" style="background-color:#333 !important;">
        <li class="nav-header">Menu</li>
        <li class="nav-divider"></li>
        <li class="">
          <a href="{{route('admin.dashboard')}}">
            <i class="fa fa-dashboard"></i><span class="link-title">&nbsp;Dashboard</span>
          </a>
        </li>
        <li class="">
            <a href="javascript:;">
              <i class="fa fa-users "></i>
              <span class="link-title">Users</span>
              <span class="fa arrow"></span>
            </a>
            <ul class="collapse" aria-expanded="false">

              <li class="@if(isset($module) && $module == 'users') active @endif" >
                <a href="{{route('users.index')}}">
                  <i class="fa fa-users"></i>&nbsp; Customers </a>
              </li>

              <li class="@if(isset($module) && $module == 'service-provider') active @endif" >
                <a href="{{route('sp.index')}}">
                    <i class="fa fa-users"></i>&nbsp; Service Provider </a>
              </li>        
              
            </ul>
        </li>

        <li class="">
            <a href="javascript:;">
              <i class="fa fa-building "></i>
              <span class="link-title">Masters</span>
              <span class="fa arrow"></span>
            </a>
            <ul class="collapse" aria-expanded="false">

              <li class="@if(isset($module) && $module == 'amenity') active @endif" >
                <a href="{{route('amenity.index')}}">
                  <i class="fa fa-bed"></i>&nbsp; Amenities </a>
              </li> 

              <li class="@if(isset($module) && $module == 'category') active @endif" >
                <a href="{{route('category.index')}}">
                  <i class="fa fa-list-alt"></i>&nbsp; Service Main Categories </a>
              </li>

              <li class="@if(isset($module) && $module == 'subcategory') active @endif" >
                <a href="{{route('subcategory.index')}}">
                  <i class="fa fa-list-alt"></i>&nbsp; Service Sub Categories </a>
              </li>

              <li class="@if(isset($module) && $module == 'service') active @endif" >
                <a href="{{route('service.index')}}">
                  <i class="fa fa-list-alt"></i>&nbsp; Service Items</a>
              </li> 

              <li class="@if(isset($module) && $module == 'offer') active @endif" >
                <a href="{{route('offer.index')}}">
                  <i class="fa fa-gift"></i>&nbsp; Offers Type</a>
              </li>

              <li class="@if(isset($module) && $module == 'spectacularOfferType') active @endif" >
                <a href="{{route('spectacularOffer.index')}}">
                  <i class="fa fa-gift"></i>&nbsp; Spectacular Offer Type</a>
              </li> 

              <li class="@if(isset($module) && $module == 'custompages') active @endif">
                <a href="{{route('custompages.index')}}">
                  <i class="fa fa-file"></i>&nbsp; Pages Manager </a>
              </li>  
              
            </ul>
        </li>
        <li class="">
            <a href="javascript:;">
              <i class="fa fa-building "></i>
              <span class="link-title">New Masters</span>
              <span class="fa arrow"></span>
            </a>
            <ul class="collapse" aria-expanded="false">

              <li class="@if(isset($module) && $module == 'fitness-type') active @endif" >
                <a href="{{route('fitness-type.index')}}">
                  <i class="fa fa-list-alt"></i>&nbsp; Fitness Type </a>
              </li>
              <li class="@if(isset($module) && $module == 'new-category') active @endif" >
                <a href="{{route('new-category.index')}}">
                  <i class="fa fa-list-alt"></i>&nbsp; Service Main Categories </a>
              </li>

              <li class="@if(isset($module) && $module == 'new-subcategory') active @endif" >
                <a href="{{route('new-subcategory.index')}}">
                  <i class="fa fa-list-alt"></i>&nbsp; Service Sub Categories </a>
              </li>

              <li class="@if(isset($module) && $module == 'service-attribute') active @endif" >
                <a href="{{route('service-attribute.index')}}">
                  <i class="fa fa-list-alt"></i>&nbsp; Service Attributes</a>
              </li>

              <li class="@if(isset($module) && $module == 'new-service') active @endif" >
                <a href="{{route('new-service.index')}}">
                  <i class="fa fa-list-alt"></i>&nbsp; Service Items</a>
              </li>

              <li class="@if(isset($module) && $module == 'subservice') active @endif" >
                <a href="{{route('subservice.index')}}">
                  <i class="fa fa-list-alt"></i>&nbsp; Sub Service Items</a>
              </li>
            </ul>
        </li>

        <li class="@if(isset($module) && $module == 'page-section') active @endif" >
          <a href="{{route('page-section.index')}}">
            <i class="fa fa-image"></i>&nbsp; Page Sections </a>
        </li>

        <li class="@if(isset($module) && $module == 'banners') active @endif" >
          <a href="{{route('banners.index')}}">
            <i class="fa fa-image"></i>&nbsp; Banners </a>
        </li>

        <li class="@if(isset($module) && $module == 'gallery') active @endif" >
          <a href="{{route('gallery.index')}}">
            <i class="fa fa-image"></i>&nbsp; Gallery </a>
        </li>

        <li class="@if(isset($module) && $module == 'staff') active @endif" >
          <a href="{{route('staff.index')}}">
            <i class="fa fa-user"></i>&nbsp; Staff </a>
        </li>

        <li class="@if(isset($module) && $module == 'product') active @endif" >
          <a href="{{route('product.index')}}">
            <i class="fa fa-product-hunt"></i>&nbsp; Product </a>
        </li>
        <li class="@if(isset($module) && $module == 'tag') active @endif" >
          <a href="{{route('tag.index')}}">
            <i class="fa fa-tag"></i>&nbsp; Tags </a>
        </li>
        <li class="">
            <a href="javascript:;">
              <i class="fa fa-gift "></i>
              <span class="link-title">Service Provider Offers</span>
              <span class="fa arrow"></span>
            </a>
            <ul class="collapse" aria-expanded="false">

              <li class="@if(isset($module) && $module == 'offerPending') active @endif" >
                <a href="{{route('sp.pending.offer')}}">
                  <i class="fa fa-gift"></i>&nbsp; Pending Offers </a>
              </li>

              <li class="@if(isset($module) && $module == 'offerApproved') active @endif" >
                <a href="{{route('sp.approved.offer')}}">
                  <i class="fa fa-gift"></i>&nbsp; Approved Offers </a>
              </li>

              <li class="@if(isset($module) && $module == 'offerExpired') active @endif" >
                <a href="{{route('sp.expired.offer')}}">
                  <i class="fa fa-gift"></i>&nbsp; Expired Offers </a>
              </li>
            </ul>
        </li>

        <li class="@if(isset($module) && $module == 'offerSpectacular') active @endif" >
          <a href="{{route('sp.spectacular.offer')}}">
            <i class="fa fa-gift"></i>&nbsp; Spectacular Offers </a>
        </li>

        <li class="@if(isset($module) && $module == 'tier') active @endif" >
          <a href="{{route('tier.index')}}">
            <i class="fa fa-users"></i>&nbsp; Tier Management </a>
        </li>

        <li class="@if(isset($module) && $module == 'quickfact') active @endif" >
          <a href="{{route('quickFact.index')}}">
            <i class="fa fa-sticky-note-o"></i>&nbsp; Quick Facts </a>
        </li>
         <li class="@if(isset($module) && $module == 'promocode') active @endif" >
            <a href="{{route('promocode.index')}}">
              <i class="fa fa-gift"></i>&nbsp; Promo Code</a>
          </li> 
        <li class="@if(isset($module) && $module == 'reviews') active @endif" >
          <a href="{{route('reviews.index')}}">
            <i class="fa fa-sticky-note-o"></i>&nbsp; Reviews </a>
        </li>

         <li class="@if(isset($module) && $module == 'forum') active @endif" >
          <a href="{{route('forum.index')}}">
            <i class="fa fa-sticky-note-o"></i>&nbsp; Forums </a>
        </li>

        <li class="">
            <a href="javascript:;">
              <i class="fa fa-shopping-cart"></i>
              <span class="link-title">Orders</span>
              <span class="fa arrow"></span>
            </a>
            <ul class="collapse" aria-expanded="false">
             
              <li class="@if(isset($module) && $module == 'orders') active @endif" >
                <a href="{{route('orders.new')}}">
                  <i class="fa fa-shopping-cart"></i>&nbsp; New Orders</a>
              </li> 

              <li class="@if(isset($module) && $module == 'orders') active @endif" >
                <a href="{{route('orders.confirm')}}">
                  <i class="fa fa-shopping-cart"></i>&nbsp; Confirmed Orders</a>
              </li> 
              <li class="@if(isset($module) && $module == 'orders') active @endif" >
                <a href="{{route('orders.complete')}}">
                  <i class="fa fa-shopping-cart"></i>&nbsp; Completed Orders</a>
              </li>    
              
              <li class="@if(isset($module) && $module == 'orders') active @endif" >
                <a href="{{route('orders.cancelled')}}">
                  <i class="fa fa-shopping-cart"></i>&nbsp; Cancelled Orders</a>
              </li>  
              <li class="@if(isset($module) && $module == 'aesthetic-orders') active @endif" >
                <a href="{{route('orders.aesthetic_orders')}}">
                  <i class="fa fa-shopping-cart"></i>&nbsp; Pending Aesthetic Orders </a>
              </li>
              <li class="@if(isset($module) && $module == 'gift-orders') active @endif" >
                <a href="{{route('orders.gift')}}">
                  <i class="fa fa-shopping-cart"></i>&nbsp; Pending Gifted Orders </a>
              </li>
            </ul>
        </li>
        
        <li class="">
            <a href="javascript:;">
              <i class="fa fa-ticket"></i>
              <span class="link-title">E-Cards</span>
              <span class="fa arrow"></span>
            </a>
            <ul class="collapse" aria-expanded="false">
             
              <li class="@if(isset($module) && $module == 'occasion') active @endif" >
                <a href="{{route('occasion.index')}}">
                  <i class="fa fa-viadeo"></i>&nbsp; Occasions </a>
              </li> 
              
              <li class="@if(isset($module) && $module == 'gift-theme') active @endif" >
                <a href="{{route('gift-theme.index')}}">
                  <i class="fa fa-ticket"></i>&nbsp; Themes </a>
              </li>
               <li class="@if(isset($module) && $module == 'e-card') active @endif" >
                <a href="{{route('e-card.index')}}">
                  <i class="fa fa-envelope"></i>&nbsp; E-Card Listing </a>
              </li>
            </ul>
        </li>

        
        <li class="">
            <a href="javascript:;">
              <i class="fa fa-question-circle"></i>
              <span class="link-title">FAQ</span>
              <span class="fa arrow"></span>
            </a>
            <ul class="collapse" aria-expanded="false">
             
              <li class="@if(isset($module) && $module == 'faq-category') active @endif" >
                <a href="{{route('faq-category.index')}}">
                  <i class="fa fa-tag"></i>&nbsp; Faq Categories </a>
              </li>
              <li class="@if(isset($module) && $module == 'faq') active @endif" >
                <a href="{{route('faq.index')}}">
                  <i class="fa fa-question-circle"></i>&nbsp; Questions & Answers </a>
              </li>
            </ul>
        </li>
        

        
        <li class="@if(isset($module) && $module == 'contact') active @endif" >
          <a href="{{route('contact.index')}}">
            <i class="fa fa-envelope"></i>&nbsp; Contact Inquiries </a>
        </li>
       
        <li class="">
            <a href="javascript:;">
              <i class="fa fa-money"></i>
              <span class="link-title">Settlements</span>
              <span class="fa arrow"></span>
            </a>
            <ul class="collapse" aria-expanded="false">
             
              <li class="@if(isset($module) && $module == 'unpaid-settlements') active @endif" >
                <a href="{{route('orders.sp_unpaid_settlements')}}">
                  <i class="fa fa-money"></i>&nbsp; Unpaid Settlements </a>
              </li>
              <li class="@if(isset($module) && $module == 'paid-settlements') active @endif" >
                <a href="{{route('orders.paid_settlements')}}">
                  <i class="fa fa-money"></i>&nbsp; Paid Settlements </a>
              </li>
              <li class="@if(isset($module) && $module == 'transaction-history') active @endif" >
                <a href="{{route('orders.transaction_history')}}">
                  <i class="fa fa-money"></i>&nbsp; Transaction History </a>
              </li>              
            </ul>
        </li>        

        <li class="">
            <a href="javascript:;">
              <i class="fa fa-question-circle"></i>
              <span class="link-title">Trail Session</span>
              <span class="fa arrow"></span>
            </a>
            <ul class="collapse" aria-expanded="false">
             
              <li class="@if(isset($module) && $module == 'trail-session') active @endif" >
                <a href="{{route('trail-session.index')}}">
                  <i class="fa fa-question-circle"></i>&nbsp; Trail Sessions</a>
              </li>
              <li class="@if(isset($module) && $module == 'trail-session-faq') active @endif" >
                <a href="{{route('trail-session-faq.index')}}">
                  <i class="fa fa-question-circle"></i>&nbsp; Questions & Answers </a>
              </li>
            </ul>
        </li>
        <li class="@if(isset($module) && $module == 'testimonial') active @endif" >
          <a href="{{route('testimonial.index')}}">
            <i class="fa fa-question-circle"></i>&nbsp; Testimonials </a>
        </li>
    </ul>
    <!-- /#menu -->
</div>
<!-- /#left -->