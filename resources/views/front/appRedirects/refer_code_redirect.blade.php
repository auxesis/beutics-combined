<!DOCTYPE html>
<html lang="en">
<head>
  <title>Beutics</title>

   <meta property="og:title" content="Beutics" />
  <meta property="og:type" content="website" />
  <meta property="og:image" content="{{asset('assets/img/scratch_card.png')}}">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="shortcut icon" href="{{asset('assets/img/favicon.png')}}"/>  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <script>

    console.log("navigator.userAgent", navigator.userAgent);

    if (navigator.userAgent.toLowerCase().indexOf("android") > -1) {

        window.location.href = "beutics://com.beutics/refercode/<?= $refer_code ?>";

         setTimeout(function () {

            window.location.href = 'https://play.google.com/store/apps/details?id=com.beutics';
        }, 2000);
        
    }
    if (navigator.userAgent.toLowerCase().indexOf("iphone") > -1 || navigator.userAgent.toLowerCase().indexOf("ipad") > -1) {
        window.location.href = "beuticsapp://refercode/<?= $refer_code?>";
        setTimeout(function () {
            window.location.replace("https://apps.apple.com/ae/app/beutics-beauty-home-services/id1483752013?ls=1");
        }, 2000);
        
    }


</script>

</head>
<body>

	<div class="container" style="margin-top:35px;">
		<div class="col-sm-12" style="background:#cce7d1;">
			<div style="border-bottom:1px solid #f7f7f7;padding-bottom:10px;">
	
			</br></br>
			<?php echo 'Beutics';?>
			</div>
		</div>
	</div>
  
</body>

</html>
