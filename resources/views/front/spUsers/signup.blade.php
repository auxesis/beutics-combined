<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Signup</title>
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ url('/') }}/assets/lib/bootstrap/css/bootstrap.css">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ url('/') }}/assets/lib/font-awesome/css/font-awesome.css">
    
    <!-- Metis core stylesheet -->
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/main.css">
    
    <style>
        .error{
            color:red;
        }

        .login .form-signin {
            max-width: 450px;
        }

    </style>
</head>

<body class="login">

    <div class="form-signin">
        <div class="text-center">
            <img src="{{ url('/') }}/assets/img/logo.png" height="100px" alt="Metis Logo">
        </div>
        <hr>

    <div class="tab-content">
        <div id="login" class="tab-pane active">
            <h4 align="center" style="color:#cc3937;">Become Service Provider</h4>
            <p align="center">Sign up to continue</p>
                @include('message')
                
                {!! Form::open(['route' => 'front.sp.make.signup', 'method' => 'POST', 'class'=>'validate login100-form validate-form']) !!}
                {{ csrf_field() }}
                
                <span class="hredio">{{ Form::radio('is_individual', '0' , true) }}Store</span>
                <span class="hredio">
                {{ Form::radio('is_individual', '1' , false) }}Individual</span>

                <div class="inner-addon left-addon">
                    {!! Form::select('category_id', $category, null, ['class' => 'form-control','placeholder'=>'Select Your Category']) !!}
                    @if($errors->has('category_id'))
                    <div class="error">{{ $errors->first('category_id') }}</div>
                    @endif
                </div>

                

                <div class="inner-addon left-addon">
                    <i class="fa fa-building"></i>
                    {{ Form::text('store_name','', ['class' => 'form-control top', 'placeholder' => 'Store Name', 'required' => true,'autocomplete'=>'off','maxlength'=>'50']) }}
                     @if($errors->has('store_name'))
                    <div class="error">{{ $errors->first('store_name') }}</div>
                    @endif
                </div>

                <div class="inner-addon left-addon hicon-number">
                    
                     <span class="hnumber"><i class="glyphicon glyphicon-phone"></i><span>+971</span></span>

                    {{ Form::text('store_number','', ['class' => 'form-control top', 'placeholder' => 'Store Number', 'required' => true,'autocomplete'=>'off']) }}
                    @if($errors->has('store_number'))
                    <div class="error">{{ $errors->first('store_number') }}</div>
                    @endif
                </div>

                <div class="inner-addon left-addon">
                    <i class="glyphicon glyphicon-user"></i>
                     {{ Form::text('name','', ['class' => 'form-control top', 'placeholder' => 'Your Name (Owner/Person-in-charge)', 'required' => true,'autocomplete'=>'off','maxlength'=>'50']) }}
                    @if($errors->has('name'))
                    <div class="error">{{ $errors->first('name') }}</div>
                    @endif
                </div>

               <div class="inner-addon left-addon">
                <i class="glyphicon glyphicon-map-marker"></i>

                  {!! Form::text('address', null, ['maxlength'=>'255','placeholder' => 'Enter your address', 'required'=>true, 'class'=>'form-control', 'id'=>'autocomplete']) !!} 

                  {!! Form::hidden('latitude', null, ['class'=>'form-control', 'id'=>'latitude']) !!}
                  {!! Form::hidden('longitude', null, ['class'=>'form-control', 'id'=>'longitude']) !!}

                  @if($errors->has('address'))
                  <div class="error">{{ $errors->first('address') }}</div>
                  @endif
                </div>

                
                <div class="inner-addon left-addon">
                    <i class="glyphicon glyphicon-user"></i>
                     {{ Form::text('landmark','', ['class' => 'form-control top', 'placeholder' => 'Landmark', 'required' => true,'autocomplete'=>'off']) }}
                     @if($errors->has('landmark'))
                    <div class="error">{{ $errors->first('landmark') }}</div>
                    @endif
                </div>
               
                <div class="inner-addon left-addon">
                    <i class="glyphicon glyphicon-envelope"></i>
                     {{ Form::text('email','', ['class' => 'form-control top', 'placeholder' => 'Email Address', 'required' => true,'autocomplete'=>'off']) }}
                     @if($errors->has('email'))
                    <div class="error">{{ $errors->first('email') }}</div>
                    @endif
                </div>
                <input type="hidden" name="country_code" value="971">

                <div class="inner-addon left-addon hicon-number">
                    <span class="hnumber"><i class="glyphicon glyphicon-phone"></i><span>+971</span></span>
                     {{ Form::text('mobile_no','', ['class' => 'form-control top', 'placeholder' => 'Mobile No', 'required' => true,'autocomplete'=>'off','id' => 'mobileno']) }}
                     @if($errors->has('mobile_no'))
                     <div class="error">{{ $errors->first('mobile_no') }}</div>
                     @endif
                </div>
                <div class="inner-addon left-addon hicon-number">
                     <input type="checkbox" id="sameas">   Same as a mobile number<br/>
                </div>
                <div class="inner-addon left-addon hicon-number">
                    <span class="hnumber"><i class="glyphicon glyphicon-phone"></i><span>+971</span></span>
                     {{ Form::text('whatsapp_no','', ['class' => 'form-control top', 'placeholder' => 'Whatsapp No', 'required' => true,'autocomplete'=>'off','id' => 'whatsappno']) }}
                     @if($errors->has('whatsapp_no'))
                     <div class="error">{{ $errors->first('whatsapp_no') }}</div>
                     @endif
                </div>

                <div class="inner-addon left-addon">
                    <i class="glyphicon glyphicon-lock"></i>
                     {{ Form::password('password', array('placeholder' => 'Password', "class" => "form-control top", 'required' => true,'autocomplete'=>'off')) }}
                    @if($errors->has('password'))
                    <div class="error">{{ $errors->first('password') }}</div>
                    @endif
                </div>
                
                <div class="inner-addon left-addon">
                    <i class="glyphicon glyphicon-lock"></i>
                     {{ Form::password('confirm_password', array('placeholder' => 'Confirm Password', "class" => "form-control", 'required' => true,'autocomplete'=>'off')) }}
                     @if($errors->has('confirm_password'))
                    <div class="error">{{ $errors->first('confirm_password') }}</div>
                    @endif
                </div>
                
                @if(Session::has('message'))
                    <div class="error"><center>{{Session::get('message')}}</center></div>
                @endif 

                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign up</button>
            </form>
        </div>
    </div>
    <br>
    <div class="text-center">
        <ul class="list-inline">
            <li><a href="{{route('spadmin.login')}}" class="text-muted">Already have an Account? Sign in here</a></li>
        </ul>
    </div>
  </div>

    <!--jQuery -->
    <script src="{{ url('/') }}/assets/lib/jquery/jquery.js"></script>

    <!--Bootstrap -->
    <script src="{{ url('/') }}/assets/lib/bootstrap/js/bootstrap.js"></script>
    <script src="{{ url('/') }}/assets/js/jquery.validate.min.js"></script>
    <script>$(".validate").validate();</script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCNVV0WGqduJq4EsX8_Y_s8L-hiZrHmrj4&libraries=places&callback=initialize"
        async defer></script>
    <script>
        //google map 
        $("#autocomplete").change(function(){
          $("#latitude").val('');
          $("#longitude").val('');
          
        });

        $("#sameas").click(function(){
            if($(this).is(':checked')){
                 var mobno = $("#mobileno").val();
                $("#whatsappno").val(mobno);
            }else{
                $("#whatsappno").val('');
            }
           
        });
        function initialize() 
        {
            var input = document.getElementById('autocomplete');
            var options = {};            
           var autocomplete = new google.maps.places.Autocomplete(input, options);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                    var place = autocomplete.getPlace();
                    var lat = place.geometry.location.lat();
                    var lng = place.geometry.location.lng();
                    $("#latitude").val(lat);
                    $("#longitude").val(lng); 
                });
        }
                     
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
</body>

</html>
