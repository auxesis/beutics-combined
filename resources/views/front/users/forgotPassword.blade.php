<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>{{$title}}</title>
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ url('/') }}/assets/lib/bootstrap/css/bootstrap.css">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ url('/') }}/assets/lib/font-awesome/css/font-awesome.css">
    
    <!-- Metis core stylesheet -->
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/main.css">
    
    <style>
        .error{
            color:red;
        }
    </style>
</head>

<body class="login">

    <div class="form-signin">
        <div class="text-center">
            <img src="{{ url('/') }}/assets/img/logo.png" height="122px" alt="Metis Logo">
        </div>
        <hr>
    <div class="tab-content">
        <div id="login" class="tab-pane active">
                {!! Form::open(['route' => 'front.make.forgotPassword', 'method' => 'POST', 'class'=>'validate login100-form validate-form']) !!}
                {{ csrf_field() }}
                <p class="text-muted text-center">
                    Forgot Password
                </p>
                {{ Form::text('email','', ['class' => 'form-control top', 'placeholder' => 'Your Email', 'required' => true]) }}

                @if(Session::has('message'))
                    <div class="error"><center>{{Session::get('message')}}</center></div>
                @endif 

                <br>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
            </form>
        </div>
    </div>
  </div>

    <!--jQuery -->
    <script src="{{ url('/') }}/assets/lib/jquery/jquery.js"></script>

    <!--Bootstrap -->
    <script src="{{ url('/') }}/assets/lib/bootstrap/js/bootstrap.js"></script>
    <script src="{{ url('/') }}/assets/js/jquery.validate.min.js"></script>
    <script>$(".validate").validate();</script>
</body>

</html>
