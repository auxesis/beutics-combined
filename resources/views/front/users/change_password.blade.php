<!DOCTYPE html>
<html lang="en">
<head>
  <title>Change Password</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
</head>
<body>

<div class="container">
<div class="col-sm-6 col-sm-offset-3">

<h3 class="text-uppercase">Update Password</h3>
 <form action="{!! route('front.user.update.forgot.password',['token'=>$token])!!}" method="post">
   {{ csrf_field() }}
    <div class="form-group">
      <label for="email">Password:</label>
       <input type="password" class="form-control" id="password" placeholder="Enter Password" name="password" required>
        @if ($errors->has('password'))
      <span class="help-block NewErr" style="margin-top:5px;">
         {{ $errors->first('password') }}
      </span>
      @endif
    </div>


    <div class="form-group">
      <label for="pwd">Confirm Password:</label>
       <input type="password" class="form-control" id="pwd" placeholder="Enter Confirm Password" name="confirm_password" required>

        @if ($errors->has('confirm_password'))
      <span class="help-block NewErr" style="margin-top:5px;">
        {{ $errors->first('confirm_password') }}
      </span>
      @endif
    </div>
     <button type="submit" class="btn btn-default btn-submit">Submit</button>
  </form>
  
   </div>
  </div>

 </body>

</html>



