<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>{{$title}}</title>
    
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ url('/') }}/assets/lib/bootstrap/css/bootstrap.css">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ url('/') }}/assets/lib/font-awesome/css/font-awesome.css">
    
    <!-- Metis core stylesheet -->
    <link rel="stylesheet" href="{{ url('/') }}/assets/css/main.css">
    
    <style>
        .error{
            color:red;
        }
    </style>
</head>

<body class="login">

    <div class="form-signin">
        <div class="text-center">
            <img src="{{ url('/') }}/assets/img/logo.png" height="122px" alt="Metis Logo">
        </div>
        <hr>
    <div class="tab-content">
        <div id="login" class="tab-pane active">
                {!! Form::open(['route' => 'update-forgot-password', 'method' => 'POST', 'class'=>'validate login100-form validate-form']) !!}
                {{ csrf_field() }}
               
                {{ Form::hidden('token', $token) }}

                {{ Form::password('password', array('placeholder' => 'New Password', "class" => "form-control", 'required' => true)) }}

                 @if($errors->has('password'))
                <div class="error">{{ $errors->first('password') }}</div>
                @endif

                {{ Form::password('confirm_password', array('placeholder' => 'Confirm Password', "class" => "form-control", 'required' => true)) }}

                 @if($errors->has('confirm_password'))
                <div class="error">{{ $errors->first('confirm_password') }}</div>
                @endif

                @if(Session::has('message'))
                    <div class="error"><center>{{Session::get('message')}}</center></div>
                @endif 

                <br>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Reset Password</button>
            </form>
        </div>
    </div>
  </div>

    <!--jQuery -->
    <script src="{{ url('/') }}/assets/lib/jquery/jquery.js"></script>

    <!--Bootstrap -->
    <script src="{{ url('/') }}/assets/lib/bootstrap/js/bootstrap.js"></script>
    <script src="{{ url('/') }}/assets/js/jquery.validate.min.js"></script>
    <script>$(".validate").validate();</script>
</body>

</html>
