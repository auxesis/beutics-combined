<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title>Untitled Document</title>
</head>
<style>
    table {
        border-collapse: collapse;
    }
    
    body {
        position: relative;
        color: #001028;
        background: #FFFFFF;
        font-family: Arial, sans-serif;
        font-size: 14px;
        font-family: Arial;
    }
    
    table th,
    td {
        padding: 5px 15px;
        color: #5D6975;
        /*white-space: nowrap;*/
        font-weight: normal;
    border: 0
    }

    table.middlePart th,
    table.middlePart td{
white-space: nowrap;
    }
</style>

<body>
    <?php 
      if(app()->getLocale() == 'en'){
        $na = 'NA';
        $booking_service_type_store = 'In store';
        $booking_service_type_home = 'At Home';
        $payment_type_paid = 'Paid';
        $payment_type_store = 'To pay at store';
      }else{
        $na = "لا";
        $booking_service_type_store = 'في المتجر';
        $booking_service_type_home = 'في البيت';
        $payment_type_paid = 'دفع';
        $payment_type_store = 'للدفع في المتجر';
        
      }
    ?>
    <div style="max-width: 1170px; margin: auto;border:1px solid">
    
    <table width="100%" style="margin-bottom: 20px">
  <tbody>
    <tr  bgcolor="#f2f2f2">
      <td cellpadding="0" width="80" align="left">
        <img height="70" src="<?= asset('assets/img/logo.png/'); ?>">
      </td>

      <td cellpadding="0">
        <h2 style="text-align: center;">
          <?php 
            if($final_data['status'] == '0' || $final_data['status'] == '1'){
            ?>
              {{__('messages.Your Booking Confirmation')}}
            <?php
            }else{
              ?>
              
              <span style='margin-left:-15%'>{{__('messages.Invoice')}}</span>
              <?php
            }
          ?>
          
        </h2>
      </td>
    </tr>
  </tbody>
</table>

        <table class="middlePart" border:co width="100%">
            <tbody>
                <tr>
                    <td width="40%"><b>{{__('messages.Order ID')}} :</b> <?= $final_data['booking_unique_id'];?></td>
                    <td width="20%" rowspan="5"><img width="90" src="<?= $final_data['sp_profile_image']; ?>"></td>
                    <td width="40%"><b>{{__('messages.Date of Booking')}} :</b> <?= date('M d, Y', strtotime($final_data['created_date']));?></td>
                </tr>

                <tr>
                    <td><b>{{__('messages.Appointment')}} :
                    </b> 
                    <?php 
                      $date = $final_data['appointment_date'];
                      $time = $final_data['appointment_time'];
                      $combinedDT = '';
                      if($date!='' && $date!=null && $time!=null && $time!=''){
                         $combinedDT = date('M d, Y h:i A', strtotime("$date $time"));
                       }else{
                        $combinedDT = $na;
                       }
                   
                    ?>
                    <?= $combinedDT;?>
                    </td>
                    <td><b>{{__('messages.Service Type')}} :</b> <?= $final_data['booking_service_type']=='shop'?$booking_service_type_store:$booking_service_type_home;?></td>
                </tr>
                <tr>
                    <td><b>{{__('messages.Service Staff')}} :</b> <?= $final_data['staff_name']!=''? $final_data['staff_name']:$na;?> </td>
                    <td><b>{{__('messages.Payment Status')}} :</b> <?= $final_data['payment_type']=='0'?$payment_type_paid:$payment_type_store;?> </td>
                </tr>
                <tr>
                    @if($final_data['is_gift'] == '1')
                      <td><b>{{__('messages.Gifted By')}}:</b> <?= $final_data['sender_name'];?> </td>
                    @else
                    <td><b>{{__('messages.Gifted By')}}:</b> {{$na}} </td>
                    @endif
                    <td><b> {{__('messages.Store')}} :</b> <?= $final_data['sp_store_name'];?></td>
                </tr>
                <tr>

                </tr>

            </tbody>
        </table>
        <hr/>
      <table width="100%">
        <tbody>
          <tr>
            <td ><b>{{__('messages.Services')}}</b> </td>
            <td></td>
            <td align="center"><b>{{__('messages.QTY')}}</b></td>
            <td></td>
            <td></td>
            <td></td>
            <td><b>{{__('messages.Total Amount (AED)')}}</b></td>
          </tr>
          <?php $total_price = 0;?>
          @foreach($final_data['booking_items'] as $items)

            <?php $total_price = $total_price + ($items['quantity'] * $items['best_price']) ?>
            <tr>
              <td>
                <?php 
                $off_sr1 = array();
                  if(array_key_exists("service_id",$items)){
                    echo $items['name'];
                  }else{
                    if(isset($items['services']))
                    {
                      foreach($items['services'] as $off_sr){
                        $off_sr1[] = '('.$off_sr['quantity'].') '.$off_sr['name'];
                      }
                    }
                    if($off_sr1){
                      echo '<b>'.$items['name'].'</b><br/>'.implode(', ',$off_sr1);
                    } else {
                      echo '<b>'.$items['name'].'</b>';
                    }
                    
                  }
                ?>
               
                  
                </td>
              <td></td>
              <td align="center"><?= $items['quantity'];?></td>
              <td></td>
              <td></td>
              <td></td>
              <td><?= $items['quantity'] * $items['best_price'];?></td>
            </tr>
          @endforeach
         
          <tr bgcolor="#f2f2f2">
            <td><strong>{{__('messages.Amount')}}</strong></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <?php $total_price = $total_price;?>
            <td><b><?= $total_price; ?></b></td>
          </tr>
        </tbody>
      </table>
      <hr/>

      <?php 
          if($final_data['status'] == '0' || $final_data['status'] == '1'){
      ?>
      <!--table width="100%">
        <tbody>
          <tr>
            <td width="30%">
              <b>Service Location:</b> 
              <?= $final_data['sp_address'].','.$final_data['sp_landmark'];?>
            </td>

            @if($final_data['payment_type']=='0')
            <td rowspan="3" width="70%" style="padding-top: 20px" valign="center"> with img
              <img width="100" src="<?= asset('assets/img/stamp.png/'); ?>"></td>
            @else
            <td width="70%" rowspan="3" style="padding-top: 20px" valign="center"> 
              <div style=" border:solid 1px red; width: 300px">&nbsp;</div></td>
            @endif
          </tr>
          <tr>
             <td width="30%">
              <b>Store Contact:</b> 
              <?= $final_data['sp_mobile_no'];?></td>
          </tr>
          <tr >
            <td width="30%" valign="top"> 
              <b>Beutics Contact</b> 
            +971 502595615</td>
          </tr>
        </tbody>
      </table-->


      <div style="margin: 5px 10px;   color: #5D6975; font-family: Arial, sans-serif; font-size: 14px; font-family: Arial;">
        <div style="width: 70.5%; display: inline-block;">
          <p>
              <b>{{__('messages.Service Location')}}:</b> 
              <?= $final_data['sp_address'].','.$final_data['sp_landmark'];?>
          </p>
          <p>
            <b>{{__('messages.Store Contact')}}:</b> 
              <?= $final_data['sp_mobile_no'];?>
          </p>
          <p>
             <b>{{__('messages.Beutics Contact')}}</b> 
              Ph: +971 585933202
          </p>
        </div>
        <div style="width: 28.5%; display: inline-block;">

            @if($final_data['payment_type']=='0')

              <img width="100" src="<?= asset('assets/img/stamp.png/'); ?>">

            @else
           
            @endif

        </div>
      </div>

      <?php
        }else{
      ?>
      <table width="100%">
        <tbody>
          <tr>
            <td width="70%"><b>{{__('messages.Tax')}}:</b> <?= $final_data['tax_amount'].' AED';?></td>
            <td rowspan="5" style="padding-top: 20px" valign="center"><img width="100" src="<?= asset('assets/img/stamp.png/'); ?>"></td>
          </tr>
          <tr>
            <td><b>{{__('messages.Promo Code Amount')}}:</b> <?= $final_data['promo_code_amount']!='' && $final_data['promo_code_amount']!= null ? $final_data['promo_code_amount'].' AED':$na;?></td>
          </tr>
          <tr>
            <td><b>{{__('messages.Beutics Rewards - Redeemed')}}:</b> <?= $final_data['redeemed_reward_points']!='' && $final_data['redeemed_reward_points']!= null ? $final_data['redeemed_reward_points'].' AED':$na;?></td>
          </tr>

          <tr>
            <td><b>{{__('messages.Paid Amount')}}:</b> <?= $final_data['paid_amount'].' AED';?></td>
          </tr>

          <tr>
            <td><b>{{__('messages.Total Cashback')}}:</b> <?= $final_data['total_cashback'].' AED';?></td>
          </tr>

         
        
         
        </tbody>
      </table>
      <?php
          
        }
      ?>
      

    </div>

</body>

</html>