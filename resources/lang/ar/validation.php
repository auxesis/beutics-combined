<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'يجب قبول :attribute.',
    'active_url'           => ':attribute ليس عنوان URL صالحًا..',
    'after'                => 'يجب أن يكون :attribute تاريخًا بعد :date.',
    'after_or_equal'       => 'يجب أن يكون :attribute تاريخًا بعد أو يساوي :date.',
    'alpha'                => 'قد يحتوي :attribute على أحرف فقط..',
    'alpha_dash'           => 'قد يحتوي :attribute فقط على أحرف وأرقام وشرطات.',
    'alpha_num'            => 'قد تحتوي :attribute فقط على حروف وأرقام.',
    'array'                => 'يجب أن يكون :attribute صفيفًا.',
    'before'               => 'يجب أن يكون :attribute تاريخًا قبل :date.',
    'before_or_equal'      => 'يجب أن يكون :attribute تاريخًا قبل أو يساوي :date.',
    'between'              => [
        'numeric' => 'يجب أن يكون :attribute بين :min و :max.',
        'file'    => 'يجب أن يكون :attribute بين :min و :max كيلو بايت.',
        'string'  => 'يجب أن يكون :attribute بين أحرف :min و :max.',
        'array'   => 'يجب أن يكون :attribute بين عناصر :min و :max.',
    ],
    'boolean'              => 'يجب أن يكون الحقل :attribute صحيحًا أو خاطئًا.',
    'confirmed'            => 'تأكيد :attribute غير متطابق.',
    'date'                 => ':attribute ليس تاريخًا صالحًا.',
    'date_format'          => ':attribute لا يطابق التنسيق :format.',
    'different'            => 'يجب أن يكون :attribute و :other مختلفين.',
    'digits'               => 'يجب أن يكون :attribute من الأرقام :digits.',
    'digits_between'       => 'يجب أن يكون :attribute بين رقم :min و :max.',
    'dimensions'           => ':attribute لها أبعاد صورة غير صالحة.',
    'distinct'             => 'يحتوي الحقل :attribute على قيمة مكررة.',
    'email'                => 'يجب أن يكون :attribute عنوان بريد إلكتروني صالحًا..',
    'exists'               => ':attribute المحدد غير صالح.',
    'file'                 => 'يجب أن يكون :attribute ملفًا.',
    'filled'               => 'يجب أن يكون للحقل :attribute قيمة',
    'image'                => 'يجب أن يكون :attribute صورة.',
    'in'                   => ':attribute المحدد غير صالح.',
    'in_array'             => 'الحقل :attribute غير موجود في :other.',
    'integer'              => 'يجب أن يكون :attribute عددًا صحيحًا.',
    'ip'                   => 'يجب أن يكون :attributes عنوان IP صالحًا.',
    'ipv4'                 => 'يجب أن يكون :attributes عنوان IPv4 صالحًا',
    'ipv6'                 => 'يجب أن يكون :attributes عنوان IPv6 صالحًا.',
    'json'                 => 'يجب أن يكون :attributes سلسلة JSON صالحة.',
    'max'                  => [
        'numeric' => 'قد لا يكون :attribute أكبر من :max.',
        'file'    => 'قد لا يكون :attribute أكبر من :max كيلو بايت.',
        'string'  => 'قد لا يكون :attribute أكبر من أحرف :max.',
        'array'   => 'قد لا يحتوي :attribute على أكثر من عناصر :max.',
    ],
    'mimes'                => 'يجب أن يكون :attribute ملفًا من النوع: :values.',
    'mimetypes'            => 'يجب أن يكون :attribute ملفًا من النوع: :values..',
    'min'                  => [
        'numeric' => 'يجب أن يكون :attribute على الأقل :min.',
        'file'    => 'يجب أن يكون :attribute على الأقل كيلو بايت :min.',
        'string'  => 'يجب أن يكون :attribute على الأقل من أحرف :min.',
        'array'   => 'يجب أن يحتوي :attribute على عناصر :min على الأقل.',
    ],
    'not_in'               => ':attribute المحدد غير صالح.',
    'numeric'              => 'يجب أن يكون :attribute رقمًا.',
    'present'              => 'يجب أن يكون الحقل :attribute موجودًا.',
    'regex'                => 'تنسيق :attribute غير صالح.',
    'required'             => 'حقل :attribute مطلوب.',
    'required_if'          => 'حقل :attribute مطلوب عندما يكون :other :value.',
    'required_unless'      => 'حقل :attribute مطلوب ما لم يكن :other في :values.',
    'required_with'        => 'حقل :attribute مطلوب عند وجود :values.',
    'required_with_all'    => 'حقل :attribute مطلوب عند وجود :values.',
    'required_without'     => 'حقل :attribute مطلوب عندما يكون :values غير موجود.',
    'required_without_all' => 'حقل :attribute مطلوب عند عدم وجود أي من :values.',
    'same'                 => 'يجب أن تطابق :attribute و :other.',
    'size'                 => [
        'numeric' => 'يجب أن يكون :attribute :size.',
        'file'    => 'يجب أن يكون :attribute كيلوبايت :size..',
        'string'  => 'يجب أن يكون :attribute أحرف :size.',
        'array'   => 'يجب أن يحتوي :attribute على عناصر :size.',
    ],
    'string'               => 'يجب أن يكون :attribute سلسلة.',
    'timezone'             => 'يجب أن يكون :attribute منطقة صالحة.',
    'unique'               => 'وقد اتخذت بالفعل :attribute.',
    'uploaded'             => 'فشل تحميل :attribute.',
    'url'                  => 'تنسيق :attribute غير صالح.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'staff_name' => 'اسم الموظفين',
        'nationality' => 'جنسية',
        'experience' => 'تجربة',
        'speciality' => 'تخصص',
        'certificates' => 'شهادات',
        'staff_image' => 'صورة الموظفين',
        'mov' => 'وسائل التحقق',
        'offer_id' => 'معرف العرض',
        'offer_details' => 'تفاصيل العرض',
        'gender' => 'جنس',
        'service_type' => 'نوع الخدمة',
        'total_price' => 'السعر الكلي',
        'discount' => 'خصم',
        'best_price' => 'افضل سعر',
        'expiry_date' => 'تاريخ الانتهاء',
        'offer_terms' => 'شروط العرض',
        'services' => 'خدمات',
        'order_id' => 'رقم التعريف الخاص بالطلب',
        'appointment_date' => 'تاريخ التعيين',
        'appointment_time' => 'وقت الموعد',
        'service_end_time' => 'وقت انتهاء الخدمة',
        'staff_id' => 'معرف الموظفين',
        'product_name' => 'اسم المنتج',
        'product_image' => 'صورة المنتج',
        'service_id' => 'معرف الخدمة',
        'country_code' => 'الرقم الدولي',
        'mobile_no' => 'رقم المحمول',
        'password' => 'كلمه السر',
        'banner_image' => 'صورة بانر',
        'old_password' => 'كلمة سر قديمة',
        'new_password' => 'كلمة مرور جديدة',
        'confirm_password' => 'تأكيد كلمة المرور',
        'store_name' => 'اسم المتجر',
        'store_number' => 'عدد مخزن',
        'name' => 'اسم',
        'address' => 'عنوان',
        'email' => 'البريد الإلكتروني',
        'whatsapp_no' => 'لا واتس اب',
        'longitude' => 'خط الطول',
        'latitude' => 'خط العرض',
        'landmark' => 'معلم معروف',
        'service_criteria' => 'معايير الخدمة',
        'amenity' => 'لطافة',
        'profile_image' => 'صورة الملف الشخصي',
        'opening_time_sunday' => 'وقت فتح الأحد',
        'opening_time_monday' => 'وقت فتح الاثنين',
        'opening_time_tuesday' => 'وقت فتح الثلاثاء',
        'opening_time_wednesday' => 'وقت فتح الأربعاء',
        'opening_time_thursday' => 'وقت فتح الخميس',
        'opening_time_saturday' => 'وقت الافتتاح السبت',

        'closing_time_sunday' => 'إغلاق وقت الأحد',
        'closing_time_monday' => 'وقت الإغلاق الاثنين',
        'closing_time_tuesday' => 'وقت الإغلاق الثلاثاء',
        'closing_time_wednesday' => 'وقت إغلاق الأربعاء',
        'closing_time_thursday' => 'إغلاق الوقت الخميس',
        'closing_time_friday' => 'إغلاق وقت الجمعة',
        'closing_time_saturday' => 'وقت الإغلاق السبت',
    ],

];
