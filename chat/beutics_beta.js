var express = require('express');
var multer  = require('multer');
var app = express();

var fs = require("fs");

// console.log('comingsss');
var options = {

     key: fs.readFileSync('/var/www/ssl-cert/private.key.txt'),
     cert: fs.readFileSync('/var/www/ssl-cert/b73e75205a2790db.crt'),
 ca: [fs.readFileSync('/var/www/ssl-cert/gd_bundle-g2-g1.crt', 'utf8')]
,     requestCertrequestCert: true
 };
 // console.log(options, 'hereis');



var http = require('https').Server(options,app);



// var http = require('https').Server(options,app);




// var http = require('http').Server(app);





var io = require('socket.io')(http, { pingInterval: 5000, pingTimeout: 3000, cookie: false });

var mysql = require('mysql');
var request = require('request');
var cron = require('node-cron');
var async = require("async");
var forEach = require('async-foreach').forEach;
var jsesc = require('jsesc');

app.use(express.static('upload_chat_imgs'));

// var port = process.env.PORT || 4243;
var port = 4243; 

var host_url = "https://app.beutics.com:"+port;
// var host_url = "http://192.168.0.154:"+port;

var host_base_url = "https://app.beutics.com/beta/api/";

var moment = require('moment');

var con = mysql.createConnection({
    host     : '127.0.0.1',
    user     : 'butics_user',
    password : 'SrH[2znZx#ajh&[(',
    database : 'beutics_beta_main',
    charset : 'utf8mb4',
    dateStrings:true
});

// var con = mysql.createConnection({
//     host     : '127.0.0.1',
//     user     : 'root',
//     password : '<nhzIndia@2018>',
//     database : 'beutics_production_live',
//     charset : 'utf8mb4',
//     dateStrings:true
// });

// var con = mysql.createConnection({
//     host     : '127.0.0.1',
//     user     : 'root',
//     password : 'root',
//     database : 'beutics',
//     charset : 'utf8mb4',
//     dateStrings:true
// });

var _url = host_url+"/";

/*mysql connect connection*/
con.connect(function(err) {
    if (err)
        return console.log(err);
        
    // console.log("Connected");

});


var sockets = {};
var socketsToUser = {};
var socketsFromUser = {};


app.get('/chat', function(req, res) {
    res.sendFile(__dirname + '/chat.html');
});


 /*socket update driver location*/
io.on('connection', function(socket) {

    if(!sockets[socket.handshake.query.user_id]){
        sockets[socket.handshake.query.user_id]=[];
    }

    sockets[socket.handshake.query.user_id].push(socket);

    var user_temp_id = socket.handshake.query.user_id;    

    if(socket.handshake.query.user_id_tbl!=undefined){

        var user_table = socket.handshake.query.user_id_tbl;

        // var sqlUpdate = "UPDATE "+user_table+" SET online=1 where id='" + socket.handshake.query.user_id + "'";
        // con.query(sqlUpdate, function(err, result) {
        // if (err)
        //     return console.log(err);            
            
        // });

        console.log('a user connected\n', socket.handshake.query.user_id);

        con.query('Select read_status, GROUP_CONCAT(id SEPARATOR ",") as read_status_id, CASE WHEN user_id = "'+ user_temp_id + '" THEN other_user_id ELSE user_id END AS result1 from chat where ( read_status="-1" OR read_status="0" ) and (other_user_id ="'+ user_temp_id + '" OR user_id ="'+ user_temp_id + '") group by result1', function (error, results, getMessageCallback) {
            if (error) console.log(error);


           if(results.length>0){

                
                var getMessageCallback = function(data, callback) {

                    // console.log('got data: '+data);

                    async.map(data, function (valmmm, callback) {

                        // console.log('got data valmmm: '+valmmm);

                        if(sockets[valmmm.result1]!=undefined){

                            con.query("update chat set read_status='0' where id IN ("+valmmm.read_status_id+") ");

                            var parts = valmmm.read_status_id.split(",");
                            var resultId = parts[parts.length - 1]; //

                            var rowschatQuerydata = {'msg_id': resultId, 'read_status': 0, 'user_id': valmmm.result1, 'other_user_id': user_temp_id};

                            // console.log('chat query id data', rowschatQuerydata);

                            for(var index in sockets[valmmm.result1]){
                                sockets[valmmm.result1][index].emit('read_message_update_response', rowschatQuerydata);

                                 // console.log('chat query id data inner', rowschatQuerydata);
                            }

                        }

                    }, callback);                    

                };

                getMessageCallback(results);

            }

        });

    }

    // get single chat history
    socket.on('get_message_history', function(user, cb) {

        console.log('a user request in history', user, 'here finish');

        if(user.user_id_tbl!=undefined){
            //user.other_user_id_tbl = 'admins'; // to be commented.

            var tprefix='U';
            if(user.user_id_tbl=="admins")
            {
                var tprefix='A';
            }else if(user.user_id_tbl=="sp_users")
            {
                var tprefix='SU';
            }

            var otprefix='U';
            if(user.other_user_id_tbl=="admins")
            {
                var otprefix='A';
            } else if(user.other_user_id_tbl=="sp_users")
            {
                var otprefix='SU';
            }

            var group_prefix = '';
            if((tprefix == 'U' && otprefix == 'A') || (tprefix == 'A' && otprefix == 'U')){
                group_prefix = 'AU';
            } else if((tprefix == 'SU' && otprefix == 'A') || (tprefix == 'A' && otprefix == 'SU')){
                group_prefix = 'AS';
            } else if((tprefix == 'SU' && otprefix == 'U') || (tprefix == 'U' && otprefix == 'SU')){
                group_prefix = 'SU';
            }

            

            var groupId = (user.user_id>user.other_user_id)?user.user_id+""+user.other_user_id+""+user.booking_id:user.other_user_id+""+user.user_id+""+user.booking_id;
            groupId = group_prefix+'-'+groupId;

            socket.chat_with = user.other_user_id;

            con.query("update chat set read_status=1 where group_id='" + groupId + "' && other_user_id ='" + user.user_id + "' ");

            // socketsToUser[socket.handshake.query.other_user_id] = socket;
            // socketsFromUser[socket.handshake.query.user_id] = socket;

            // console.log('groupId', groupId);

            con.query('Select id as last_chat_id from chat where group_id="' + groupId +'" order by id asc limit 1', function(err, rowss) {
            if (err) {

                    return err;

                } 
            else{

                    var total_unread_message=0;
                    

                    var ChatBlockQuery = "Select count(*) as is_chat_block from bookings WHERE id ='" + user.booking_id + "' AND (status ='2' OR status ='4' OR status ='3' OR status ='5')";

                    // console.log("ChatBlockQuery-------- : ", ChatBlockQuery);

                    con.query(ChatBlockQuery, function(err, rowsischatblockarr) {
                            if (err) console.log(err);

                            var is_chat_block = rowsischatblockarr[0].is_chat_block;

                            var CountTotalQuery = "Select count(*) as CountTotalUnRead from chat WHERE ( ( other_user_id ='" + user.user_id + "') AND ( read_status='0' OR read_status='-1' ))";

                            con.query(CountTotalQuery, function(err, rowscountarr) {
                                    if (err) console.log(err);

                                    var total_unread_message = rowscountarr[0].CountTotalUnRead;

                                    if(rowss.length>0)
                                        var last_chat_id = rowss[0].last_chat_id;
                                    else
                                        var last_chat_id = '';

                                    var table = ["chat"];

                                    var query = "Select count(*) as TotalCount from ?? WHERE ( group_id='" + groupId + "' )";

                                    query = mysql.format(query, table);
                                    con.query(query, function(err, rows) {

                                        if (err) {

                                            return err;

                                        } else {

                                            var totalCount = rows[0].TotalCount

                                            if (user.limit == '') {
                                                var LimitNum = 15;
                                            } else {
                                                var LimitNum = parseInt(user.limit);
                                            }

                                        }
                                        var user_id_tbl = 'sp_users';
                                        var other_user_id_tbl = "admins";

                                        //var condition_user = "AND ((user_id_tbl='"+user_id_tbl+"' AND other_user_id_tbl='"+other_user_id_tbl+"') OR (user_id_tbl='"+other_user_id_tbl+"' AND other_user_id_tbl='"+user_id_tbl+"'))";

                                        var startPage = parseInt(user.last_id);
                                        /*
                                        if(startPage >0)
                                        {
                                            if(user.type=='after')
                                            {   
                                                var Query = "select *,(Select count(*) from bookings WHERE id =chat.booking_id AND (status ='3' OR status ='5') ) as is_chat_block FROM  `chat`  WHERE id >'"+startPage+"' && group_id = '" + groupId + "' "+condition_user+" order by id desc";
                                            }
                                            else if(user.type=='before')
                                            {    
                                                var Query = "select *, (Select count(*) from bookings WHERE id =chat.booking_id AND (status ='3' OR status ='5') ) as is_chat_block FROM  `chat`  WHERE id <'"+startPage+"' && group_id = '" + groupId + "' "+condition_user+" order by id desc limit " + LimitNum;
                                            }
                                        }
                                        else
                                        {
                                            
                                            var Query = "select *, (Select count(*) from bookings WHERE id =chat.booking_id AND (status ='3' OR status ='5') ) as is_chat_block FROM  `chat` WHERE group_id = '" + groupId + "' "+condition_user+" order by id desc limit " + LimitNum;
                                        }
                                        */

                                        if(startPage >0) {
                                            if(user.type=='after')
                                            {   
                                                var Query = "select *,(Select count(*) from bookings WHERE id =chat.booking_id AND (status ='3' OR status ='5') ) as is_chat_block FROM  `chat`  WHERE id >'"+startPage+"' && group_id = '" + groupId + "' order by id desc";
                                            }
                                            else if(user.type=='before')
                                            {    
                                                var Query = "select *, (Select count(*) from bookings WHERE id =chat.booking_id AND (status ='3' OR status ='5') ) as is_chat_block FROM  `chat`  WHERE id <'"+startPage+"' && group_id = '" + groupId + "' order by id desc limit " + LimitNum;
                                            }
                                        }
                                        else
                                        {
                                            
                                            var Query = "select *, (Select count(*) from bookings WHERE id =chat.booking_id AND (status ='3' OR status ='5') ) as is_chat_block FROM  `chat` WHERE group_id = '" + groupId + "' order by id desc limit " + LimitNum;
                                        }

                                        // console.log("chat_query get_user_thread: ", Query);

                                        con.query(Query, function(err, rows) {
                                            if (err) console.log(err);
                                            
                                            if(rows.length>0)
                                                var newdataarr4= {'data':rows,'message' : 'User message history get successfully','status':'true','total_record' : totalCount,'total_unread_message':total_unread_message,'is_chat_block':is_chat_block, 'first_message_id': last_chat_id, 'type': user.type};
                                            else
                                                var newdataarr4= {'data':[], 'message' : 'No data found','status':'true','total_unread_message':total_unread_message,'is_chat_block':is_chat_block, 'total_record' : totalCount, 'first_message_id':'', 'type': user.type};

                                            console.log("get_message_history_response--------- : ", newdataarr4);

                                            // thread_response
                                            // socketsFromUser[user.user_id].emit('get_message_history_response', newdataarr4);

                                            if(sockets[tprefix+''+user.user_id]!=undefined){
                                                for(var index in sockets[tprefix+''+user.user_id]){
                                                    sockets[tprefix+''+user.user_id][index].emit('get_message_history_response', newdataarr4);
                                                }
                                                //sockets[user.user_id].emit('get_chat_thread_response', dataChat);
                                            }

                                            // io.emit('allChat', newdataarr4);

                                        });
                                    });

                            });

                    });


                }

            });

        }

    });

    // get all inbox chat list using user
    socket.on('get_inbox_list', function(user, cb) {

        // console.log('a get_inbox_list', user);

        if(user.user_id_tbl!=undefined){

            var tprefix='U';
            if(user.user_id_tbl=="admins")
            {
                var tprefix='A';
            }else if(user.user_id_tbl=="sp_users")
            {
                var tprefix='SU';
            }

            var user_table = (user.user_id_tbl=="users")?"users":"sp_users";
            var other_user_table =(user.user_id_tbl=="users")?"sp_users":"users";

            var query = "Select count(*) as TotalCount from ?? WHERE ( other_user_id ='" + user.user_id + "'  OR user_id ='" + user.user_id + "') ";

            var table = ["chat"];
            query = mysql.format(query, table);
            con.query(query, function(err, rows) {

                if (err) {
                    return err;
                } else {

                    var totalCount = rows[0].TotalCount;

                    if (user.start == '' && user.limit == '') {
                        var startNum = 0;
                        var LimitNum = 10;
                    } else {
                        var startNum = parseInt(user.start);
                        var LimitNum = parseInt(user.limit);
                    }
                }

                var total_unread_message=0;

                var CountTotalQuery = "Select count(*) as CountTotalUnRead from chat WHERE  ( ( other_user_id ='" + user.user_id + "') AND ( read_status='0' OR read_status='-1' ))";

                con.query(CountTotalQuery, function(err, rowscountarr) {
                    if (err) console.log(err);

                    var total_unread_message = rowscountarr[0].CountTotalUnRead;

                    if(user.start=='-1'){

                        if(user_table=="users")
                            var Query = "SELECT (Select count(*) from bookings WHERE id =c.booking_id AND (status ='3' OR status ='5') ) as is_chat_block, bookings.created_at as bookings_created_at, b1.status as booking_status, c.*, bookings.booking_unique_id, (Select count(*) from chat WHERE group_id =c.group_id AND other_user_id =c.other_user_id AND user_id =c.user_id AND (other_user_id =c.other_user_id AND other_user_id_tbl ='"+user_table+"') AND ( read_status='0' OR read_status='-1') ) as unread_message, (CASE WHEN (c.user_id_tbl='users') THEN u1.`name`  WHEN c.user_id_tbl='sp_users' THEN (SELECT store_name from sp_users WHERE id=c.user_id ) ELSE u1.`name` END ) AS user_name, (CASE WHEN c.user_id_tbl='users' THEN (SELECT image from users WHERE id=c.user_id )  WHEN c.user_id_tbl='sp_users' THEN (SELECT profile_image from sp_users WHERE id=c.user_id ) ELSE '' END ) AS user_image,  c.other_user_id_tbl as other_user_table, (CASE WHEN c.other_user_id_tbl='users' THEN (SELECT name from users WHERE id=c.other_user_id ) WHEN c.other_user_id_tbl='sp_users' THEN (SELECT store_name from sp_users WHERE id=c.other_user_id ) ELSE (SELECT name from admins WHERE id=c.other_user_id ) END ) as other_user_name, (CASE WHEN c.other_user_id_tbl='users' THEN (SELECT image from users WHERE id=c.other_user_id ) WHEN c.other_user_id_tbl='sp_users' THEN (SELECT profile_image from sp_users WHERE id=c.other_user_id ) ELSE 0 END ) as other_user_image FROM  `conversations` c LEFT JOIN bookings on c.booking_id=bookings.id LEFT JOIN users u1 ON c.user_id=u1.id INNER JOIN bookings b1 ON c.booking_id=b1.id WHERE ( c.other_user_id_tbl != 'admins' AND c.user_id_tbl != 'admins' ) AND ((c.user_id ='" + user.user_id + "' AND c.user_id_tbl='"+user_table+"')  OR (c.other_user_id ='" + user.user_id + "' AND c.other_user_id_tbl='"+user_table+"')) group by c.id order by c.updated_at desc";
                        else if(user_table=="sp_users")
                            var Query = "SELECT (Select count(*) from bookings WHERE id =c.booking_id AND (status ='3' OR status ='5') ) as is_chat_block, bookings.created_at as bookings_created_at, b1.status as booking_status, c.*, bookings.booking_unique_id, (Select count(*) from chat WHERE group_id =c.group_id AND other_user_id =c.other_user_id AND user_id =c.user_id AND (other_user_id =c.other_user_id AND other_user_id_tbl ='"+user_table+"') AND ( read_status='0' OR read_status='-1') ) as unread_message, (CASE WHEN (c.user_id_tbl='users') THEN u1.`name`  WHEN c.user_id_tbl='sp_users' THEN (SELECT store_name from sp_users WHERE id=c.user_id ) ELSE u1.`name` END ) AS user_name, (CASE WHEN c.user_id_tbl='users' THEN (SELECT image from users WHERE id=c.user_id )  WHEN c.user_id_tbl='sp_users' THEN (SELECT profile_image from sp_users WHERE id=c.user_id ) ELSE '' END ) AS user_image,  c.other_user_id_tbl as other_user_table, (CASE WHEN c.other_user_id_tbl='users' THEN (SELECT name from users WHERE id=c.other_user_id ) WHEN c.other_user_id_tbl='sp_users' THEN (SELECT store_name from sp_users WHERE id=c.other_user_id ) ELSE (SELECT name from admins WHERE id=c.other_user_id ) END ) as other_user_name, (CASE WHEN c.other_user_id_tbl='users' THEN (SELECT image from users WHERE id=c.other_user_id ) WHEN c.other_user_id_tbl='sp_users' THEN (SELECT profile_image from sp_users WHERE id=c.other_user_id ) ELSE 0 END ) as other_user_image FROM  `conversations` c LEFT JOIN bookings on c.booking_id=bookings.id LEFT JOIN users u1 ON c.user_id=u1.id INNER JOIN bookings b1 ON c.booking_id=b1.id WHERE ( c.other_user_id_tbl != 'admins' AND c.user_id_tbl != 'admins' ) AND ((c.user_id ='" + user.user_id + "' AND c.user_id_tbl='"+user_table+"')  OR (c.other_user_id ='" + user.user_id + "' AND c.other_user_id_tbl='"+user_table+"')) group by c.id order by c.updated_at desc";
                        else
                            var Query = "SELECT (Select count(*) from bookings WHERE id =c.booking_id AND (status ='3' OR status ='5') ) as is_chat_block, bookings.created_at as bookings_created_at, b1.status as booking_status, c.*, bookings.booking_unique_id, (Select count(*) from chat WHERE group_id =c.group_id AND other_user_id =c.other_user_id AND user_id =c.user_id AND (other_user_id =c.other_user_id AND other_user_id_tbl ='"+user_table+"') AND ( read_status='0' OR read_status='-1') ) as unread_message, (CASE WHEN (c.user_id_tbl='users') THEN u1.`name`  WHEN c.user_id_tbl='sp_users' THEN (SELECT store_name from sp_users WHERE id=c.user_id ) ELSE u1.`name` END ) AS user_name, (CASE WHEN c.user_id_tbl='users' THEN (SELECT image from users WHERE id=c.user_id )  WHEN c.user_id_tbl='sp_users' THEN (SELECT profile_image from sp_users WHERE id=c.user_id ) ELSE '' END ) AS user_image,  c.other_user_id_tbl as other_user_table, (CASE WHEN c.other_user_id_tbl='users' THEN (SELECT name from users WHERE id=c.other_user_id ) WHEN c.other_user_id_tbl='sp_users' THEN (SELECT store_name from sp_users WHERE id=c.other_user_id ) ELSE (SELECT name from admins WHERE id=c.other_user_id ) END ) as other_user_name, (CASE WHEN c.other_user_id_tbl='users' THEN (SELECT image from users WHERE id=c.other_user_id ) WHEN c.other_user_id_tbl='sp_users' THEN (SELECT profile_image from sp_users WHERE id=c.other_user_id ) ELSE 0 END ) as other_user_image FROM  `conversations` c LEFT JOIN bookings on c.booking_id=bookings.id LEFT JOIN "+user_table+" u1 ON c.user_id=u1.id INNER JOIN bookings b1 ON c.booking_id=b1.id WHERE ( c.other_user_id_tbl != 'admins' AND c.user_id_tbl != 'admins' ) AND ((c.user_id ='" + user.user_id + "' AND c.user_id_tbl='"+user_table+"')  OR (c.other_user_id ='" + user.user_id + "' AND c.other_user_id_tbl='"+user_table+"')) group by c.id order by c.updated_at desc";

                    }else
                    {
                        var startPage = (startNum==0)?0:Math.ceil(startNum * LimitNum);

                        var Query = "SELECT (Select count(*) from bookings WHERE id =c.booking_id AND (status ='3' OR status ='5') ) as is_chat_block, c.*, bookings.created_at as bookings_created_at, bookings.booking_unique_id, (Select count(*) from chat WHERE group_id =c.group_id AND other_user_id =c.other_user_id AND user_id =c.user_id AND (other_user_id =c.other_user_id AND other_user_id_tbl ='"+user_table+"') AND ( read_status='0' OR read_status='-1') ) as unread_message, (CASE WHEN c.user_id_tbl='users' THEN u1.`name`  WHEN c.user_id_tbl='sp_users' THEN (SELECT store_name from sp_users WHERE id=c.user_id ) ELSE u1.`name` END ) AS user_name, (CASE WHEN c.user_id_tbl='users' THEN (SELECT image from users WHERE id=c.user_id )  WHEN c.user_id_tbl='sp_users' THEN (SELECT profile_image from sp_users WHERE id=c.user_id ) ELSE '' END ) AS user_image,  c.other_user_id_tbl as other_user_table, (CASE WHEN c.other_user_id_tbl='users' THEN (SELECT name from users WHERE id=c.other_user_id ) WHEN c.other_user_id_tbl='sp_users' THEN (SELECT store_name from sp_users WHERE id=c.other_user_id ) ELSE (SELECT name from admins WHERE id=c.other_user_id ) END ) as other_user_name, (CASE WHEN c.other_user_id_tbl='users' THEN (SELECT image from users WHERE id=c.other_user_id ) WHEN c.other_user_id_tbl='sp_users' THEN (SELECT profile_image from sp_users WHERE id=c.other_user_id ) ELSE 0 END ) as other_user_image FROM  `conversations` c LEFT JOIN bookings on c.booking_id=bookings.id LEFT JOIN "+user_table+" u1 ON c.user_id=u1.id INNER JOIN bookings b1 ON c.booking_id=b1.id WHERE ( c.other_user_id_tbl != 'admins' AND c.user_id_tbl != 'admins' ) AND ((c.user_id ='" + user.user_id + "')  OR (c.other_user_id ='" + user.user_id + "')) group by c.id order by c.updated_at desc limit " + startPage + "," + LimitNum + " ";
                    }

                    // console.log("get_message_history chat_query : ", Query);

                    con.query(Query, function(err, rows) {
                        if (err) console.log(err);

                         // 
                        var InnerQuery = "SELECT (Select count(*) from bookings WHERE id =c.booking_id AND (status ='3' OR status ='5') ) as is_chat_block, c.*, (Select count(*) from chat WHERE group_id =c.group_id AND other_user_id =c.other_user_id AND user_id =c.user_id AND (other_user_id =c.other_user_id AND other_user_id_tbl ='"+user_table+"') AND  ( read_status='0' OR read_status='-1') ) as unread_message, u1.name AS user_name, u2.name AS other_user_name FROM `conversations` c LEFT JOIN "+user_table+" u1 ON c.user_id=u1.id LEFT JOIN admins u2 ON c.other_user_id=u2.id WHERE ( c.other_user_id_tbl = 'admins' OR c.user_id_tbl = 'admins' ) AND  ((c.user_id ='" + user.user_id + "' AND c.user_id_tbl='"+user_table+"') OR (c.other_user_id ='" + user.user_id + "' AND c.other_user_id_tbl='"+user_table+"')) order by c.updated_at desc ";

                        // // console.log("get_message_history chat_query InnerQuery: ", InnerQuery);

                        // console.log("get_message_history chat_query rows : ", rows);

                        con.query(InnerQuery, function(err, irows) {
                        if (err) console.log(err);                        

                            //sockets[user.user_id].emit('get_inbox_list_response', newdataarr);

                            if(irows.length<=0){

                                irows=[];
                            }

                            if(user.start=='-1')
                            {

                                if(rows.length>0){
                                    var newdataarr= {'admindata':irows,'data':rows,'message' : 'Message inbox list get successfully','status':'true','total_record' : totalCount, 'total_unread_message': total_unread_message};
                                }
                                else{
                                    var newdataarr= {'admindata':irows,'data':[], 'message' : 'No data found','status':'true','total_record' : totalCount, 'total_unread_message': total_unread_message};
                                }

                                // console.log("chat List : ", newdataarr);

                                for(var index in sockets[tprefix+''+user.user_id]){
                                    sockets[tprefix+''+user.user_id][index].emit('get_inbox_list_response', newdataarr);
                                }

                            }else{

                                var totalPage = Math.ceil(totalCount / LimitNum);

                                if(rows.length>0)
                                    var newdataarr= {'admindata':irows, 'data':rows,'message' : 'Message history get successfully','status':'true','total_record' : totalCount, "page":startNum, 'total_page':totalPage, 'total_unread_message': total_unread_message};
                                else
                                    var newdataarr= {'admindata':irows, 'data':[], 'message' : 'No data found','status':'true','total_record' : totalCount, "total_page":totalPage, 'page':startNum, 'total_unread_message': total_unread_message};

                                // console.log("chat List : ", newdataarr);

                                for(var index in sockets[tprefix+''+user.user_id]){
                                    sockets[tprefix+''+user.user_id][index].emit('get_inbox_list_response', newdataarr);
                                }

                            }
                            

                        });

                        // io.emit('get_inbox_list_response', rows);

                    });

                });

            });

        }

    });
    

    // send message
    socket.on('send_message', function(data, cb) {

        // console.log('Data request --- - ', data);

        if(data.user_id_tbl!=undefined && data.other_user_id_tbl!=undefined){

            var current_user_type='customer';
            var tprefix='U';
            if(data.user_id_tbl=="admins")
            {
                var tprefix='A';                

            }else if(data.user_id_tbl=="sp_users")
            {
                var tprefix='SU';                
            }

            var otprefix='U';
            if(data.other_user_id_tbl=="admins")
            {
                var otprefix='A';
                var current_user_type='Admin';

            }else if(data.other_user_id_tbl=="sp_users")
            {
                var otprefix='SU';
                var current_user_type='SpUser';
            }

            var group_prefix = '';
            if((tprefix == 'U' && otprefix == 'A') || (tprefix == 'A' && otprefix == 'U')){
                group_prefix = 'AU';
            } else if((tprefix == 'SU' && otprefix == 'A') || (tprefix == 'A' && otprefix == 'SU')){
                group_prefix = 'AS';
            } else if((tprefix == 'SU' && otprefix == 'U') || (tprefix == 'U' && otprefix == 'SU')){
                group_prefix = 'SU';
            }
            




            var user_table = data.user_id_tbl;
            var other_user_table =data.other_user_id_tbl;

            var created = moment().utc().format('YYYY-MM-DD HH:mm:ss');
            var groupId = (data.user_id>data.other_user_id)?data.user_id+""+data.other_user_id+""+data.booking_id:data.other_user_id+""+data.user_id+""+data.booking_id;
            groupId = group_prefix+'-'+groupId;
            
            con.query("SELECT * FROM `conversations` WHERE  group_id='" + groupId + "' AND ( (user_id='" + data.user_id + "' AND other_user_id='" + data.other_user_id + "') OR (user_id='" + data.other_user_id + "' AND other_user_id='" + data.user_id + "') )", function(err, res) {
                if (err) console.log(err);

                //// console.log('aa ',res.length);

                var created = moment().utc().format('YYYY-MM-DD HH:mm:ss');

                var str_message_type = data.message_type.charAt(0).toLowerCase() + data.message_type.slice(1);
                
                if (res.length <= 0) {

                    var conQuery = "insert conversations set user_id_tbl='" + data.user_id_tbl + "', other_user_id_tbl='" + data.other_user_id_tbl + "', booking_id='" + data.booking_id + "',  group_id='" + groupId + "', other_data='" + data.other_data + "', message_type='" + data.message_type + "', user_id='" + data.user_id + "', other_user_id='" + data.other_user_id + "', message='" + data.message + "',updated_at=" + con.escape(created) + "";
                
                } else {

                    var conQuery = "update conversations set user_id_tbl='" + data.user_id_tbl + "', other_user_id_tbl='" + data.other_user_id_tbl + "', booking_id='" + data.booking_id + "', other_data='" + data.other_data + "', message_type='" + data.message_type + "', user_id='" + data.user_id + "', other_user_id='" + data.other_user_id + "', message='" + data.message + "', deleted_by=0, updated_at=" + con.escape(created) + " where group_id='" + groupId + "' AND id=" + res[0].id;
                
                }
                

                con.query(conQuery, function(err, conId) {
                    if (err) console.log(err);

                    var insertedID = (res.length <= 0) ? conId.insertId : res[0].id;

                    con.query("Insert into chat(other_user_id_tbl, user_id_tbl, booking_id, group_id, other_user_id,user_id,message,conversation_id,message_type,user_time, other_data) values(?,?,?, ?,?,?,?,?,?,?,?)", [data.other_user_id_tbl, data.user_id_tbl, data.booking_id, groupId, data.other_user_id, data.user_id, data.message, insertedID, data.message_type,created, data.other_data], function(err, count) {
                    if (err){

                                // console.log("ERROR", err)
                                io.emit('chat_status', err);

                            } else {

                                data.id = count.insertId;

                                data.msg_id = count.insertId;

                                data.group_id = groupId;

                                // // console.log('hello data 11',"SELECT id, name AS user_name FROM "+user_table+" WHERE id='" + data.user_id + "'");

                                // CASE WHEN sur_name !='' THEN CONCAT(first_name,' ',sur_name) WHEN first_name !='' THEN first_name ELSE activated_matecode END AS user_name
                                if(user_table=="sp_users")                                        
                                    var user_select_query = "SELECT id, store_name AS user_name FROM "+user_table+" WHERE id='" + data.user_id + "'";
                                else
                                    var user_select_query = "SELECT id, name AS user_name FROM "+user_table+" WHERE id='" + data.user_id + "'";
                                                
                                con.query(user_select_query, function(err, fromResult) {
                                
                                    // console.log("users : ",fromResult,err);
                                    if(fromResult.length > 0){

                                        data['created_at'] = created;
                                        data['user_time'] = created;
                                        data['user_name'] = fromResult[0].user_name;

                                        var newdataarr3= {'data':data,'message' : 'Sender Data','status':'true'};
                                        
                                        // console.log('multi_user ', newdataarr3);

                                        // console.log('multi_user id', tprefix+''+data.user_id);

                                        for(var index in sockets[tprefix+''+data.user_id]){
                                            sockets[tprefix+''+data.user_id][index].emit('multi_user', newdataarr3);
                                        }

                                        // // console.log('hello data 12',"SELECT id, name AS other_user_name FROM "+other_user_table+" WHERE id='" + data.other_user_id + "'");
                                        // CASE WHEN sur_name !='' THEN CONCAT(first_name,' ',sur_name) WHEN first_name !='' THEN first_name ELSE activated_matecode END AS other_user_name
                                        con.query("SELECT id, name AS other_user_name FROM "+other_user_table+" WHERE id='" + data.other_user_id + "'", function(err, toResult) {
                                        if(toResult.length > 0){

                                                data['other_user_name'] = toResult[0].other_user_name;
                                                
                                                var newdataarr2= {'data':data,'message' : 'Receiver Data','status':'true'};
                                                
                                                // console.log('receive_message ', newdataarr2);

                                                // console.log('receive_message id', otprefix+''+data.other_user_id);

                                                if(sockets[otprefix+''+data.other_user_id]!=undefined){

                                                    con.query("update chat set read_status=0 where id IN(SELECT id from(SELECT c.id FROM  `chat` c  WHERE c.other_user_id ='" + data.other_user_id + "' and c.read_status='-1') as tmptable)");
                                                    
                                                    for(var index in sockets[otprefix+''+data.other_user_id]){
                                                        sockets[otprefix+''+data.other_user_id][index].emit('receive_message', newdataarr2);
                                                    }

                                                }

                                                var CountTotalQuery = "Select count(*) as CountTotalUnRead from (Select chat.* from chat inner join "+other_user_table+" as u1 on u1.id=chat.other_user_id WHERE ( ( chat.other_user_id ='" + data.other_user_id + "' AND chat. user_id_tbl ='"+user_table+"') AND ( chat.read_status='0' OR chat.read_status='-1' )) group by chat.id) as X";

                                                // console.log('CountTotalQuery', CountTotalQuery);

                                                con.query(CountTotalQuery, function(err, rowscountarr) {
                                                    if (err) console.log(err);

                                                    var total_unread_message = rowscountarr[0].CountTotalUnRead;

                                                    var newdataarr= {
                                                        'user_id': data.user_id,
                                                        'other_user_id': data.other_user_id,
                                                        'user_id_tbl': data.user_id_tbl,
                                                        'other_user_id_tbl': data.other_user_id_tbl,
                                                        'total_unread':total_unread_message,
                                                        'message' : 'Unread Data',
                                                        'status':'true',
                                                        'table_name':'tbn_'+user_table
                                                    };

                                                    for(var index in sockets[otprefix+''+data.other_user_id]){
                                                        sockets[otprefix+''+data.other_user_id][index].emit('admin_unread_count', newdataarr);
                                                    }

                                                });

                                                //// console.log('receive_message ', newdataarr2);

                                            }
                                        });

                                        var sqlUpdate = "SELECT online, unread_notifications from "+other_user_table+" WHERE online='0' AND id='" + data.other_user_id + "'";
                                        con.query(sqlUpdate, function(err, result) {
                                            // console.log("users : ",result,err);
                                            if(result.length > 0){

                                                var device_badges = result[0].unread_notifications + 1;
                                        
                                                var badge_update_query = "update "+other_user_table+" set unread_notifications=" + device_badges + " where id=" + data.other_user_id;
                                                con.query(badge_update_query, function (err, badge_update_result) {
                                                    if (err) { console.log(err); }
                                                    else{

                                                        var dataUserName = fromResult[0].user_name.charAt(0).toUpperCase() + fromResult[0].user_name.slice(1);

                                                        var notiMessage = dataUserName+": "+ encodeURI(data.message);

                                                        if(current_user_type !='Admin'){

                                                            console.log("Notification message : "+host_base_url+"send-chat-notification -  "+notiMessage);

                                                            // // console.log("Query string", 'user_table='+user_table+'&user_name='+dataUserName+'&current_user_type='+current_user_type+'&booking_id='+data.booking_id+'&user_id='+data.other_user_id+'&message='+notiMessage+'&group_id='+data.group_id+'&other_user_id='+data.user_id+'&chat_id='+data.msg_id);
                                                        
                                                            request(host_base_url+'send-chat-notification?user_table='+user_table+'&user_name='+dataUserName+'&current_user_type='+current_user_type+'&booking_id='+data.booking_id+'&user_id='+data.other_user_id+'&message='+notiMessage+'&group_id='+data.group_id+'&other_user_id='+data.user_id+'&chat_id='+data.msg_id, { json: true }, (err, res) => {
                                                              if (err) { console.log(err); }
                                                            });

                                                        }
                                                        

                                                    }

                                                });
                                            }
                                        });

                                    }

                                });
                                
                        }

                    });

                });

            });

        }

    });

    socket.on('read_message_update', function(user, cb) {

        console.log('Data read_message_update  request --- - ', user);
        if(user.other_user_id_tbl!=undefined){

            var otprefix='U';
            if(user.other_user_id_tbl=="admins")
            {
                var otprefix='A';
            }else if(user.other_user_id_tbl=="sp_users")
            {
                var otprefix='SU';
            }

            var str = (user.msg_id) ? String(user.msg_id) : '';

             console.log('Request length ',str);

            if(str.length>0){

                if(user.read_status!=undefined)
                    con.query("update chat set read_status='" + user.read_status + "' where id IN ("+str+") ");
                else
                    con.query("update chat set read_status='1' where id IN ("+str+") ");

                console.log('heee');
                if(sockets[otprefix+''+user.other_user_id]!=undefined){

                    //// console.log('read_message_update_response', user);
                    //sockets[user.other_user_id].emit('receive_message', newdataarr2);
                    
                    for(var index in sockets[otprefix+''+user.other_user_id]){
                        sockets[otprefix+''+user.other_user_id][index].emit('read_message_update_response', user);
                    }

                }else{

                    // console.log('read_message_update_response failed');

                }

            }

        }

    });

    // get unread count
    socket.on('get_unread_count', function(user, cb) {

        // console.log('Data request get_unread_count --- - ', user);

        if(user.user_id_tbl!=undefined){

            var tprefix='U';
            var group_prefix = 'U';
            var other_tbl_prefix = 'users';

            
            if(user.user_id_tbl=="admins") {
                var tprefix='A';
                group_prefix = 'A';
                other_tbl_prefix = 'admins';
            } else if(user.user_id_tbl=="sp_users")
            {
                var tprefix='SU';
                group_prefix = 'S';
                other_tbl_prefix = 'sp_users';
            }

            


            var user_table = (user.user_id_tbl=="users")?"users":"sp_users";

            var CountTotalQuery = "Select count(*) as CountTotalUnRead from (Select conversations.* from conversations inner join chat on conversations.id=chat.conversation_id inner join "+user_table+" as u1 on u1.id=conversations.other_user_id WHERE ( ( conversations.other_user_id ='" + user.user_id + "') AND ( chat.read_status='0' OR chat.read_status='-1' ) AND chat.group_id LIKE '%"+group_prefix+"%' AND chat.other_user_id_tbl='"+other_tbl_prefix+"') group by conversations.id) as X";

            console.log('count_query', CountTotalQuery);
            
            // var CountTotalQuery = "Select count(*) as CountTotalUnRead from chat inner join "+user_table+" as u1 on u1.id=chat.other_user_id WHERE ( ( chat.other_user_id ='" + user.user_id + "') AND ( chat.read_status='0' OR chat.read_status='-1' ))";

            // console.log('CountTotalQuery', CountTotalQuery);

            con.query(CountTotalQuery, function(err, rowscountarr) {
                if (err) console.log(err);

                var total_unread_message = rowscountarr[0].CountTotalUnRead;
                

                var newdataarr= {'total_unread':total_unread_message,'message' : 'Unread Data','status':'true'};
                console.log('get_unread_count_response', newdataarr);
                for(var index in sockets[tprefix+''+user.user_id]){
                    sockets[tprefix+''+user.user_id][index].emit('get_unread_count_response', newdataarr);
                }

            });

        }
            
    });

    socket.on('disconnect', function(arr) {

        var logoutid = socket.handshake.query.user_id;

        var otprefix = logoutid.replace(/[^a-zA-Z]+/g, '');

        // console.log("otprefix : "+otprefix);

        if(otprefix=="U")
        {
            var user_id_update = logoutid.replace(otprefix, '');

            console.log("users is OFFLINE update : "+user_id_update );

            var sqlUpdate = "UPDATE users SET online=0 where id='" + user_id_update + "'";

            con.query(sqlUpdate, function(err, result) {
            if (err)
                return console.log(err);
                // console.log("user is OFFLINE : "+user_id_update );
            });

        }else if(otprefix=="SU")
        {
            var user_id_update = logoutid.replace(otprefix, '');

            console.log("sp users is OFFLINE update : "+user_id_update );

            var sqlUpdate = "UPDATE sp_users SET online=0 where id='" + user_id_update + "'";
            con.query(sqlUpdate, function(err, result) {
            if (err)
                return console.log(err);
                // console.log("sp users is OFFLINE : "+user_id_update );
            });

        }

        // console.log('disconnect error region - ', arr);

        // console.log('disconnect user  ',socket.handshake.query.user_id);

        for(var data in sockets[socket.handshake.query.user_id]){
            if(socket.id==sockets[socket.handshake.query.user_id][data].id){

                // console.log('sockethandshakequeryuser_id ',socket.handshake.query.user_id);

                sockets[socket.handshake.query.user_id].splice(data, 1);

                // console.log('sockethandshakequeryuser_id data ',data);
                //delete sockets[socket.handshake.query.user_id][data];
            }
        }

    });

});

/*server connection*/
http.listen(port, function() {
    // console.log('listening on *:'+port);
});
