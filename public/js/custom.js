$(document).on("scroll", function(){
  if
    ($(document).scrollTop() > 86){
    $("#header_main").addClass("shrink");
  }
  else
  {
    $("#header_main").removeClass("shrink");
  }

  // if
  //   ($(document).scrollTop() > 150){
  //   $(".search_section_module").addClass("shrink_option");
  // }
  // else
  // {
  //   $(".search_section_module").removeClass("shrink_option");
  // }
});

//Menu On Hover
  
$('body').on('mouseenter mouseleave','.nav-item',function(e){
    if ($(window).width() > 750) {
      var _d=$(e.target).closest('.nav-item');_d.addClass('show');
      setTimeout(function(){
      _d[_d.is(':hover')?'addClass':'removeClass']('show');
      },1);
    }
});	

// Home Main banner
(function ($) {
$(function() {

var jQuerystatus = jQuery('.slick_custom_paginginfo');
  var jQueryslickElement = jQuery('.slideshow__slides');

  // jQueryslickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
  //     var i = (currentSlide ? currentSlide : 0) + 1;
  //     jQuerystatus.html('<span class="current_slide">' + i + '</span>' + '<span class="total_slide">' + slick.slideCount + '</span>');
  // });

jQuery(document).ready(function () {
  // Slick slider for prev/next thumbnails images
  $('.slideshow__slides').slick({
    dots: true,
    slidesToShow: 1,
    adaptiveHeight: false,
    autoplay: false
  });

  var jQuerystatus = jQuery('.slick_custom_paginginfo');
  var jQueryslickElement = jQuery('.slideshow__slides');

  jQueryslickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
      var i = (currentSlide ? currentSlide : 0) + 1;
      jQuerystatus.html('<span class="current_slide">' + i + '</span>' + '<span class="total_slide">' + slick.slideCount + '</span>');
  });
});

  setTimeout(function() {
    $('.slideshow__slides .slick-prev').prepend('<div class="prev-slick-img slick-thumb-nav"><img src="/prev.jpg" class="img-responsive"></div>');
    $('.slideshow__slides .slick-next').append('<div class="next-slick-img slick-thumb-nav"><img src="/next.jpg" class="img-responsive"></div>');
    get_prev_slick_img();
    get_next_slick_img();
  }, 500);

  $('.slideshow__slides').on('click', '.slick-prev', function() {
    get_prev_slick_img();
  });
  $('.slideshow__slides').on('click', '.slick-next', function() {
    get_next_slick_img();
  });
  $('.slideshow__slides').on('swipe', function(event, slick, direction) {
    if (direction == 'left') {
      get_prev_slick_img();
    }
    else {
      get_next_slick_img();
    }
  });
  $('.slideshow__slides .slick-dots').on('click', 'li button', function() {
    var li_no = $(this).parent('li').index();
    if ($(this).parent('li').index() > li_no) {
      get_prev_slick_img()
    }
    else {
      get_next_slick_img()
    }
  });
  function get_prev_slick_img() {
    // For prev img
    var prev_slick_img = $('.slideshow__slides .slick-current').prev('.slider-image').find('img').attr('src');
    $('.prev-slick-img img').attr('src', prev_slick_img);
    $('.prev-slick-img').css('background-image', 'url(' + prev_slick_img + ')');
    // For next img
    var prev_next_slick_img = $('.slideshow__slides .slick-current').next('.slider-image').find('img').attr('src');
    $('.next-slick-img img').attr('src', prev_next_slick_img);
    $('.next-slick-img').css('background-image', 'url(' + prev_next_slick_img + ')');
  }
  function get_next_slick_img() {
    // For next img
    var next_slick_img = $('.slideshow__slides .slick-current').next('.slider-image').find('img').attr('src');
    $('.next-slick-img img').attr('src', next_slick_img);
    $('.next-slick-img').css('background-image', 'url(' + next_slick_img + ')');
    // For prev img
    var next_prev_slick_img = $('.slideshow__slides .slick-current').prev('.slider-image').find('img').attr('src');
    $('.prev-slick-img img').attr('src', next_prev_slick_img);
    $('.prev-slick-img').css('background-image', 'url(' + next_prev_slick_img + ')');
  }
  // End
})
})(jQuery);

// Category sliders 

jQuery(document).ready(function () {

/* beauty_category Slider */
  jQuery('.beauty_category').slick({
      // slidesToShow: 4,
      infinite: true,
      cssEase: 'linear',
      slidesToScroll: 1,
      arrows: true,
      dots: false,
      autoplay: true,
      pauseOnHover: false,
      variableWidth: true,
      // fade: true,
      speed: 500,
      infinite: true,
      cssEase: 'ease',
      responsive: [
          {
            breakpoint: 1274,
            settings: {
              slidesToShow: 3,
           }
          },
           {
            breakpoint: 992,
            settings: {
              slidesToShow: 1,
              // centerMode: true,
              // autoplay: false,
              // centerPadding: '50px',
           }
          },
          {
            breakpoint: 800,
            settings: {
              slidesToShow: 1,
              // centerMode: true,
              // autoplay: false,
              // centerPadding: '50px',
           }
          },
           {
            breakpoint: 576,
            settings: {
              slidesToShow: 1,
              // centerMode: true,
              // centerPadding: '50px',
           }
          }
      ]
  });
  /* End beauty_category Slider */
  /* fitness_category Slider */
  jQuery('.fitness_category').slick({
      // slidesToShow: 4,
      infinite: true,
      cssEase: 'linear',
      slidesToScroll: 1,
      arrows: true,
      dots: false,
      autoplay: true,
      pauseOnHover: false,
      variableWidth: true,
      rtl: true,
      responsive: [
          {
            breakpoint: 1274,
            settings: {
              slidesToShow: 3,
           }
          },
           {
            breakpoint: 992,
            settings: {
              slidesToShow: 1,
              // centerMode: true,
              // autoplay: false,
              // centerPadding: '50px',
           }
          },
           {
            breakpoint: 576,
            settings: {
              slidesToShow: 1,
              // centerMode: true,
              // centerPadding: '50px',
           }
          }
      ]
  });
  /* End fitness_category Slider */
  
    /* wellness_category Slider */
  jQuery('.wellness_category').slick({
      // slidesToShow: 4,
      infinite: true,
      cssEase: 'linear',
      slidesToScroll: 1,
      arrows: true,
      dots: false,
      autoplay: true,
      pauseOnHover: false,
      variableWidth: true,
      responsive: [
          {
            breakpoint: 1274,
            settings: {
              slidesToShow: 3,
           }
          },
           {
            breakpoint: 992,
            settings: {
              slidesToShow: 2,
              // centerMode: true,
              // autoplay: false,
              // centerPadding: '50px',
           }
          },
           {
            breakpoint: 576,
            settings: {
              slidesToShow: 1,
              // centerMode: true,
              // centerPadding: '50px',
           }
          }
      ]
  });
  /* End wellness_category Slider */

    /* weight_loss_prog Slider */
    jQuery('.weight_loss_prog').slick({
      // slidesToShow: 4,
      infinite: true,
      cssEase: 'linear',
      slidesToScroll: 1,
      arrows: true,
      dots: false,
      // fade:true,
      autoplay: false,
      pauseOnHover: false,
      variableWidth: true,
      asNavFor: '.sld__content',
      responsive: [
          {
            breakpoint: 1274,
            settings: {
              slidesToShow: 3,
           }
          },
           {
            breakpoint: 992,
            settings: {
              slidesToShow: 2,
              centerMode: true,
              autoplay: false,
              centerPadding: '50px',
           }
          },
           {
            breakpoint: 576,
            settings: {
              slidesToShow: 1,
              centerMode: true,
              centerPadding: '50px',
           }
          }
      ]
  });
  $('.sld__content').slick({
slidesToShow: 1,
slidesToScroll: 1,
asNavFor: '.sld__images',
arrows: false,
dots: false,
infinite:true,
autoplay:true,
});
  /* End weight_loss_prog Slider */


    
  /* End weight_loss_prog Slider */

  


//   $(document).ready(function(){
//     length = 400;
//     cHtml = $(".content").html();
//     cText = $(".content").html().substr(0, length).trim();
//     $(".content").addClass("compressed").html(cText + "... <a href='#' class='exp'>More</a>");
//     window.handler = function()
//     {
//         $('.exp').click(function(){
//             if ($(".content").hasClass("compressed"))
//             {
//                 $(".content").html(cHtml + "<a href='#' class='exp'>Less</a>");
//                 $(".content").removeClass("compressed");
//                 handler();
//                 return false;
//             }
//             else
//             {
//                 $(".content").html(cText + "... <a href='#' class='exp'>More</a>");
//                 $(".content").addClass("compressed");
//                 handler();
//                 return false;
//             }
//         });
//     }
//     handler();
// });

//JS

$(document).ready( function () {
  var contentArray=[];
  var index="";
  var clickedIndex="";  
  var minimumLength=$('.read-more-less').attr('data-id');
  var initialContentLength=[];
  var initialContent=[];  
  var readMore="........<br/><hr/><span class='read-more'><span class='glyphicon glyphicon-plus-sign'></span>Read More</span>";
  var readLess=" <br/><hr/><span class='read-less'><span class='glyphicon glyphicon-minus-sign'></span>Read Less</span>";  
    $('.read-toggle').each(function(){
    index = $(this).attr('data-id');  
    contentArray[index] = $(this).html();
    initialContentLength[index] = $(this).html().length;
    if(initialContentLength[index]>minimumLength) {
      initialContent[index]=$(this).html().substr(0,minimumLength);
    }else {
      initialContent[index]=$(this).html();
    } 
      $(this).html(initialContent[index]+readMore);
    //console.log(initialContent[0]);  
      
    
  });
    $(document).on('click','.read-more',function(){
      $(this).fadeOut(500, function(){
      clickedIndex = $(this).parents('.read-toggle').attr('data-id');
      $(this).parents('.read-toggle').html(contentArray[clickedIndex]+readLess);  
      });
    });
   $(document).on('click','.read-less',function(){
      $(this).fadeOut(500, function(){
      clickedIndex = $(this).parents('.read-toggle').attr('data-id');
      $(this).parents('.read-toggle').html(initialContent[clickedIndex]+readMore);  
      });
    }); 
  });

// function myFunction() {
//   var dots = document.getElementById("dots");
//   var moreText = document.getElementById("more");
//   var btnText = document.getElementById("myBtn");

//   if (dots.style.display === "none") {
//     dots.style.display = "inline";
//     btnText.innerHTML = "Read more"; 
//     moreText.style.display = "none";
//   } else {
//     dots.style.display = "none";
//     btnText.innerHTML = "Read less"; 
//     moreText.style.display = "inline";
//   }
// }



  // Testimonial slider
  jQuery('.testimonial_slider').slick({
    dots: true,
    slidesToShow: 1,
    adaptiveHeight: false,
    arrows: false,
    autoplay: true,
    pauseOnHover: true,
    
  });

  // Blog slider
  jQuery('.blog_slider').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: true,
      dots: true,
      autoplay: true,
      variableWidth: true,
      pauseOnHover: true,
      responsive: [
          {
            breakpoint: 1274,
            settings: {
              slidesToShow: 3,
           }
          },
           {
            breakpoint: 992,
            settings: {
              slidesToShow: 1,
              
           }
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 1,
              variableWidth: false,
              
           }
          },
           {
            breakpoint: 576,
            settings: {
              slidesToShow: 1,
              centerMode: true,
           }
          }
      ]
  });

  //Langing page Blog slider
  jQuery('.landing_blog_slider').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: true,
      dots: false,
      autoplay: true,
      // variableWidth: true,
      pauseOnHover: true,
      responsive: [
          {
            breakpoint: 1274,
            settings: {
              slidesToShow: 3,
           }
          },
           {
            breakpoint: 992,
            settings: {
              slidesToShow: 2,
           }
          },
           {
            breakpoint: 576,
            settings: {
              slidesToShow: 1,
              centerMode: true,
           }
          }
      ]
  });

  //Langing page Offers Blog sliders

//   jQuery('.landing_blog_slider_all_category').slick({
//     slidesToShow: 3,
//     slidesToScroll: 1,
//     row:2,
//     infinite:true,
//     arrows: true,
//     dots: false,
//     autoplay: true,
//     // variableWidth: true,
//     pauseOnHover: true,
//     responsive: [
//         {
//           breakpoint: 1274,
//           settings: {
//             slidesToShow: 3,
//          }
//         },
//          {
//           breakpoint: 992,
//           settings: {
//             slidesToShow: 2,
//          }
//         },
//          {
//           breakpoint: 576,
//           settings: {
//             slidesToShow: 1,
//             centerMode: true,
//          }
//         }
//     ]
// });

(function ($) {
  
  $('.slick-slider-wrapper').slick({
      infinite: true,
      rows: 2,
      dots:false,
      slidesPerRow: 5,
      arrows: true,
      autoplay:true,
  });


}(jQuery));

  jQuery('.studio_offers_slider').slick({
      slidesToShow: 2,
      slidesToScroll: 1,
      arrows: false,
      dots: true,
      autoplay: true,
      // variableWidth: true,
      pauseOnHover: true,
      responsive: [
        {
          breakpoint: 1274,
          settings: {
            slidesToShow: 2,
         }
        },
         {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            
         }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            variableWidth: false,
            
         }
        },
         {
          breakpoint: 576,
          settings: {
            slidesToShow: 1,
            centerMode: true,
         }
        }
    ]
  });

  jQuery('.trainer_offers_slider').slick({
      slidesToShow: 2,
      slidesToScroll: 1,
      arrows: false,
      dots: true,
      autoplay: true,
      // variableWidth: true,
      pauseOnHover: true,
      responsive: [
          {
            breakpoint: 1274,
            settings: {
              slidesToShow: 2,
           }
          },
           {
            breakpoint: 992,
            settings: {
              slidesToShow: 2,
           }
          },
           {
            breakpoint: 576,
            settings: {
              slidesToShow: 1,
              // centerMode: true,
           }
          }
      ]
  });

  jQuery('.online_offers_slider').slick({
      slidesToShow: 2,
      slidesToScroll: 1,
      arrows: false,
      dots: true,
      autoplay: true,
      // variableWidth: true,
      pauseOnHover: true,
      responsive: [
          {
            breakpoint: 1274,
            settings: {
              slidesToShow: 2,
           }
          },
           {
            breakpoint: 992,
            settings: {
              slidesToShow: 2,
           }
          },
          {
            breakpoint:800,
            settings: {
              slidesToShow: 1,
           }
          },
           {
            breakpoint: 576,
            settings: {
              slidesToShow: 1,
              // centerMode: true,
           }
          }
      ]
  });

  // Listing fitness gallery

  jQuery('.product_image').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    // autoplay: true,
    asNavFor: '.product_image_thumb',
    
  });
  jQuery('.product_image_thumb').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.product_image',
    // dots: true,
    centerMode: true,
    arrows: false,
    // autoplay: true,
    focusOnSelect: true
  });

  // side_feature_blogs_slider

  jQuery('.side_feature_blogs_slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    autoplay: true
  });

  // Studio/Center Blog Masonry
  jQuery('.blogs_masonry').masonry();

 });

// add By Mukesh

$(document).ready(function() {
    $('.minus').click(function () {
      var $input = $(this).parent().find('input');
      var count = parseInt($input.val()) - 1;
      count = count < 1 ? 1 : count;
      $input.val(count);
      $input.change();
      return false;
    });
    $('.plus').click(function () {
      var $input = $(this).parent().find('input');
      $input.val(parseInt($input.val()) + 1);
      $input.change();
      return false;
    });
  });

$(document).ready(function() {
// Without changing default buttons
/*
$('.container').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 3
});
*/
// With custom buttons
var backButton = '<span class="slick-prev">&#8592;</span>';
var nextButton = '<span class="slick-next">&#8594;</span>'
$('.similar_gym_slider').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 2,
  prevArrow: backButton,
  nextArrow: nextButton,
   dots: true,
   responsive: [
    {
      breakpoint: 1274,
      settings: {
        slidesToShow: 3,
     }
    },
     {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
     }
    },
     {
      breakpoint: 576,
      settings: {
        slidesToShow: 1,
        
     }
    }
]

});
});

$(document).ready(function() { 
// Card's slider
var $carousel = $('.slider-for');

$carousel
  .slick({
     infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    adaptiveHeight: true,
    asNavFor: '.slider-nav',
    autoplay: true,
  })
  
$('.slider-nav').slick({
   infinite: true,
  slidesToShow: 4,
  slidesToScroll:1,
  asNavFor: '.slider-for',
  dots: false,
    autoplay: true,
  centerMode: false,
  focusOnSelect: true,
  variableWidth: true
});
});

$(document).ready(function() { 
$("#accordion").on("hide.bs.collapse show.bs.collapse", (e) => {
  $(e.target).prev().find("i:last-child").toggleClass("fa-chevron-down  fa-chevron-right");
});
});


$(document).ready(function(){
$('[data-toggle="tooltip"]').tooltip();   
});
$(document).ready(function(){
$('.product-carousel').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      nextArrow: '<i class="arrow right">',
  prevArrow: '<i class="arrow left">',
      dots: false,
      autoplay: true,
      infinite: true,
      variableWidth: false,
      pauseOnHover: true,
      cssEase: 'linear',
  responsive: [
    {
            breakpoint: 1274,
            settings: {
              slidesToShow: 1,
           }
          },
           {
            breakpoint: 992,
            settings: {
              slidesToShow: 2,
           }
          },
           {
            breakpoint: 576,
            settings: {
              slidesToShow: 1,
              centerMode: true,
           }
          }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});
});

$(document).ready(function() {

  $('.review-slider').slick({
      dots: true,
       customPaging: function(slider, i) { 
      return '<button class="tab">' + $(slider.$slides[i]).attr('title') + '<i class="fa fa-sort-asc"></i></button>';
  },
      autoplay: false,
      infinite: true,
      speed: 300,
      arrows:false,
      slidesToShow: 1,
      slidesToScroll: 1,
      responsive: [{
          breakpoint: 1024,
          settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              // centerMode: true,

          }

      }, {
          breakpoint: 800,
          settings: {
              slidesToShow: 1,
              slidesToScroll: 2,
              dots: false,
              infinite: true,

          }


      }, {
          breakpoint: 600,
          settings: {
              slidesToShow: 1,
              slidesToScroll: 2,
              dots: false,
              infinite: true,
              
          }
      }, {
          breakpoint: 480,
          settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              dots: false,
              infinite: true,
              autoplay: true,
              autoplaySpeed: 2000,
          }
      }]
  });


});

$(document).ready(function() {
$('.minus1').click(function () {
  var $input = $(this).parent().find('input');
  var count = parseInt($input.val()) - 1;
  count = count < 1 ? 1 : count;
  $input.val(count);
  $input.change();
  return false;
});
$('.plus1').click(function () {
  var $input = $(this).parent().find('input');
  $input.val(parseInt($input.val()) + 1);
  $input.change();
  return false;
});
});

$(document).ready(function() {
var $slider = $('.slider_checkout_page');
var backButton = '<span class="slick-prev prev_btn_image"> <i class="fas fa-long-arrow-alt-left"></i></span>';
var nextButton = '<span class="slick-next next_btn_image"> <i class="fas fa-long-arrow-alt-right"></i></span>'
// var $progressBar = $('.progress');
// var $progressBarLabel = $( '.slider__label' );

// $slider.on('beforeChange', function(event, slick, currentSlide, nextSlide) {   
//   var calc = ( (nextSlide) / (slick.slideCount-1) ) * 100;

$slider.slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  speed: 400,
  prevArrow: backButton,
  nextArrow: nextButton
});  
});

$(document).ready(function(){
$("#check-in").flatpickr({
  enableTime: false,
  disableMobile: true,
  dateFormat: "l, j F Y",
  minDate: "today",
  time_24hr: true,
  locale: "id"
});
});

$(document).ready(function() { 
$("#accordion1").on("hide.bs.collapse show.bs.collapse", (e) => {
  $(e.target).prev().find("i:last-child").toggleClass("fa-chevron-down  fa-chevron-right");
});
});

// Change option selected
const label = document.querySelector('.dropdown__filter-selected');
const options = Array.from(document.querySelectorAll('.dropdown__select-option'));

options.forEach(option => {
option.addEventListener('click', () => {
  label.textContent = option.textContent;
});
});

// Close dropdown onclick outside
document.addEventListener('click', e => {
const toggle = document.querySelector('.dropdown__switch');
const element = e.target;

if (element == toggle) return;

const isDropdownChild = element.closest('.dropdown__filter');

if (!isDropdownChild) {
  toggle.checked = false;
}
});


/*
Reference: http://jsfiddle.net/BB3JK/47/
*/

$('#mounth').each(function(){
var $this = $(this), numberOfOptions = $(this).children('option').length;

$this.addClass('select-hidden'); 
$this.wrap('<div class="select"></div>');
$this.after('<div class="select-styled"></div>');

var $styledSelect = $this.next('div.select-styled');
$styledSelect.text($this.children('option').eq(0).text());

var $list = $('<ul />', {
    'class': 'select-options'
}).insertAfter($styledSelect);

for (var i = 0; i < numberOfOptions; i++) {
    $('<li />', {
        text: $this.children('option').eq(i).text(),
        rel: $this.children('option').eq(i).val()
    }).appendTo($list);
}

var $listItems = $list.children('li');

$styledSelect.click(function(e) {
    e.stopPropagation();
    $('div.select-styled.active').not(this).each(function(){
        $(this).removeClass('active').next('ul.select-options').hide();
    });
    $(this).toggleClass('active').next('ul.select-options').toggle();
});

$listItems.click(function(e) {
    e.stopPropagation();
    $styledSelect.text($(this).text()).removeClass('active');
    $this.val($(this).attr('rel'));
    $list.hide();
    //console.log($this.val());
});

$(document).click(function() {
    $styledSelect.removeClass('active');
    $list.hide();
});

});

//Reference: 
//https://www.onextrapixel.com/2012/12/10/how-to-create-a-custom-file-input-with-jquery-css3-and-php/
;(function($) {

// Browser supports HTML5 multiple file?
var multipleSupport = typeof $('<input/>')[0].multiple !== 'undefined',
    isIE = /msie/i.test( navigator.userAgent );

$.fn.customFile = function() {

  return this.each(function() {

    var $file = $(this).addClass('custom-file-upload-hidden'), // the original file input
        $wrap = $('<div class="file-upload-wrapper">'),
        $input = $('<input type="text" class="file-upload-input" placeholder=" Upload"/>'),
        // Button that will be used in non-IE browsers
        $button = $('<button type="button" class="file-upload-button"> Photo <img src="assets/img/upload_white.png"/></button>'),
        // Hack for IE
        $label = $('<label class="file-upload-button" for="'+ $file[0].id +'">Select a File</label>');

    // Hide by shifting to the left so we
    // can still trigger events
    $file.css({
      position: 'absolute',
      left: '-9999px'
    });

    $wrap.insertAfter( $file )
      .append( $file, $input, ( isIE ? $label : $button ) );

    // Prevent focus
    $file.attr('tabIndex', -1);
    $button.attr('tabIndex', -1);

    $button.click(function () {
      $file.focus().click(); // Open dialog
    });

    $file.change(function() {

      var files = [], fileArr, filename;

      // If multiple is supported then extract
      // all filenames from the file array
      if ( multipleSupport ) {
        fileArr = $file[0].files;
        for ( var i = 0, len = fileArr.length; i < len; i++ ) {
          files.push( fileArr[i].name );
        }
        filename = files.join(', ');

      // If not supported then just take the value
      // and remove the path to just show the filename
      } else {
        filename = $file.val().split('\\').pop();
      }

      $input.val( filename ) // Set the value
        .attr('title', filename) // Show filename in title tootlip
        .focus(); // Regain focus

    });

    $input.on({
      blur: function() { $file.trigger('blur'); },
      keydown: function( e ) {
        if ( e.which === 13 ) { // Enter
          if ( !isIE ) { $file.trigger('click'); }
        } else if ( e.which === 8 || e.which === 46 ) { // Backspace & Del
          // On some browsers the value is read-only
          // with this trick we remove the old input and add
          // a clean clone with all the original events attached
          $file.replaceWith( $file = $file.clone( true ) );
          $file.trigger('change');
          $input.val('');
        } else if ( e.which === 9 ){ // TAB
          return;
        } else { // All other keys
          return false;
        }
      }
    });

  });

};

// Old browser fallback
if ( !multipleSupport ) {
  $( document ).on('change', 'input.customfile', function() {

    var $this = $(this),
        // Create a unique ID so we
        // can attach the label to the input
        uniqId = 'customfile_'+ (new Date()).getTime(),
        $wrap = $this.parent(),

        // Filter empty input
        $inputs = $wrap.siblings().find('.file-upload-input')
          .filter(function(){ return !this.value }),

        $file = $('<input type="file" id="'+ uniqId +'" name="'+ $this.attr('name') +'"/>');

    // 1ms timeout so it runs after all other events
    // that modify the value have triggered
    setTimeout(function() {
      // Add a new input
      if ( $this.val() ) {
        // Check for empty fields to prevent
        // creating new inputs when changing files
        if ( !$inputs.length ) {
          $wrap.after( $file );
          $file.customFile();
        }
      // Remove and reorganize inputs
      } else {
        $inputs.parent().remove();
        // Move the input so it's always last on the list
        $wrap.appendTo( $wrap.parent() );
        $wrap.find('input').focus();
      }
    }, 1);

  });
}

}(jQuery));

$('input[type=file]').customFile();

$('.togglefaq').click(function(e) {
e.preventDefault();
var notthis = $('.active').not(this);
notthis.find('.icon-minus').addClass('icon-plus').removeClass('icon-minus');
notthis.toggleClass('active').next('.faqanswer').slideToggle(300);
 $(this).toggleClass('active').next().slideToggle("fast");
$(this).children('i').toggleClass('fa-minus fa-plus ');
});

$('.togglefaq_side').click(function(e) {
  e.preventDefault();
  var notthis = $('.active').not(this);
  notthis.find('.icon-minus').addClass('icon-plus').removeClass('icon-minus');
  notthis.toggleClass('active').next('.faqanswer_side').slideToggle(300);
   $(this).toggleClass('active').next().slideToggle("fast");
  $(this).children('i').toggleClass('fa-minus fa-plus ');
  });


  function selectSth(elem) {
    let what=(elem?elem.innerText:'');
    let color=(elem?elem.style.backgroundColor:null);
    document.getElementById('current').innerText=what;
    document.getElementById('current').style.backgroundColor=(color || '');
    document.getElementById('suggestPos').blur();
    resetSel();
  }
  let currIndex=-1;
  function getLis() {
    return document.getElementById("suggestContent").getElementsByTagName("li");
  }
  function setSel() {
    let lis=getLis();
    for(let i=0; i<lis.length; i++) {
      lis[i].setAttribute("class", (lis[i].getAttribute("class") || "").replace(/sel/g, "")+(i===currIndex?" sel":""));
    }
  }
  function resetSel() {
    currIndex=-1;
    setSel();
  }
  function keyDown(event) {
    if ("ArrowDown"===event.key) {
      currIndex++;
      if (currIndex>2) {
        currIndex=0;
      }
      setSel();
    } else if ("ArrowUp"===event.key) {
      currIndex--;
      if (currIndex<0) {
        currIndex=2;
      }
      setSel();
    } else if ("Enter"===event.key) {
      selectSth(getLis()[currIndex]);
    } else if ("Backspace"===event.key) {
      selectSth(null);
    }
  }
  
  // auto-focus in gallery
  window.onload=()=>location.href.indexOf('fullcpgrid')>=0?document.getElementById('suggestPos').focus():null;

  $(".find_store").click(function(){
    if($(".main_search_wrapper").hasClass("tab_header")) {
        $(".main_search_wrapper").addClass("tab_header");
    } else {
        $(".main_search_wrapper").addClass("tab_header");
    }
    });

    // $(".find_store").click(function(){
    //   $(".main_search_wrapper").toggleClass("tab_header");
    // });


$("#heading-1-1").click(function(){
if($("#heading-1-1").hasClass("show_1")) {
    $("#heading-1-1").removeClass("show_1");
} else {
    $("#heading-1-1").addClass("show_1");
}
});

$("#heading-1-2").click(function(){
if($("#heading-1-2").hasClass("show_1")) {
    $("#heading-1-2").removeClass("show_1");
} else {
    $("#heading-1-2").addClass("show_1");
}
});

$("#heading-1-3").click(function(){
if($("#heading-1-3").hasClass("show_1")) {
    $("#heading-1-3").removeClass("show_1");
} else {
    $("#heading-1-3").addClass("show_1");
}
});
$("#heading-1-4").click(function(){
  if($("#heading-1-4").hasClass("show_1")) {
      $("#heading-1-4").removeClass("show_1");
  } else {
      $("#heading-1-4").addClass("show_1");
  }
  });
  $("#heading-1-5").click(function(){
    if($("#heading-1-5").hasClass("show_1")) {
        $("#heading-1-5").removeClass("show_1");
    } else {
        $("#heading-1-5").addClass("show_1");
    }
    });
    $("#heading-1-6").click(function(){
      if($("#heading-1-6").hasClass("show_1")) {
          $("#heading-1-6").removeClass("show_1");
      } else {
          $("#heading-1-6").addClass("show_1");
      }
      });
      $("#heading-1-7").click(function(){
        if($("#heading-1-7").hasClass("show_1")) {
            $("#heading-1-7").removeClass("show_1");
        } else {
            $("#heading-1-7").addClass("show_1");
        }
        });



// End Here


// New Sliders

(function ($) {
  
  $('.slick-slider-wrapper-1').slick({
      infinite: true,
      rows: 1,
      dots:false,
      slidesPerRow: 3,
      arrows: true,
      autoplay:true,
  });


}(jQuery));




$(document).ready(function(){
  
  $(".Modern-Slider").slick({
    autoplay:true,
    autoplaySpeed:10000,
    speed:400,
    slidesToShow:1,
    slidesToScroll:1,
    pauseOnHover:false,
    dots:true,
    pauseOnDotsHover:true,
    cssEase:'linear',
   // fade:true,
    draggable:false,
    prevArrow:'<button class="PrevArrow"></button>',
    nextArrow:'<button class="NextArrow"></button>', 
  });
  
})
