<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('locale/{locale}', function ($locale){
    Session::put('locale', $locale);
    return redirect()->back();
});

$this->group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'AdminLoggedIn'], function () {

    $this->get('/dashboard', 'PagesController@dashboard')->name('admin.dashboard');
    $this->get('/logout', 'AdminController@logout')->name('admin.logout');

    $this->get('/users', 'UserController@index')->name('users.index');
    $this->get('/users/download-file', 'UserController@download_file')->name('users.downloadFile');
    $this->post('/users/getdata', 'UserController@getData')->name('users.getdata');
    $this->post('/users/delete/{id}', 'UserController@delete')->name('users.delete');
    $this->any('/users/chat-history/{id}', 'UserController@userChatHistory')->name('users.userChatHistory');
    $this->any('/users/view-wallet-transactions/{id}', 'UserController@view_wallet_transactions')->name('users.view_wallet_transactions');
    $this->post('/users/getwalletdata', 'UserController@getWalletData')->name('users.getwalletdata');
    $this->get('/users/changePassword/{slug}', 'UserController@changePassword')->name('users.change-password');
    $this->post('/users/updatePassword', 'UserController@doUpdatePassword')->name('users.update-password');

    $this->get('/changeAdminPassword', 'AdminController@changeAdminPassword')->name('admin.change-password');
    $this->post('/updateAdminPassword', 'AdminController@updateAdminPassword')->name('admin.update-password');

    $this->post('/users/changeStatus', 'UserController@StatusUpdate')->name('admin.users.status.update');
    $this->post('/users/changeArabicStatus', 'UserController@arabicStatusUpdate')->name('admin.users.arabic.status.update');
    $this->get('/users/view/{slug}', 'UserController@view')->name('users.view');

    //------------------service provider --------------------
    $this->get('/service-provider', 'ServiceProviderController@index')->name('sp.index');
    $this->any('/service-provider/chat-history/{id}', 'ServiceProviderController@spuserChatHistory')->name('sp.spuserChatHistory');
    $this->post('/service-provider/getdata', 'ServiceProviderController@getData')->name('sp.getdata');

    $this->get('/service-provider/changePassword/{slug}', 'ServiceProviderController@changePassword')->name('sp.change-password');
    $this->post('/service-provider/updatePassword', 'ServiceProviderController@doUpdatePassword')->name('sp.update-password');

    $this->post('/service-provider/changeStatus', 'ServiceProviderController@StatusUpdate')->name('sp.users.status.update');
    $this->post('/service-provider/update-tax-status', 'ServiceProviderController@updateTaxVisibility')->name('sp.users.tax.visibility.update');
    
    $this->post('/service-provider/changeFeaturedStatus', 'ServiceProviderController@FeaturedStatusUpdate')->name('sp.users.featuredstatus.update');

    $this->post('/service-provider/changeApproval', 'ServiceProviderController@changeApproval')->name('sp.users.approval.update');

    $this->get('/service-provider/view/{slug}', 'ServiceProviderController@view')->name('sp.view');
    $this->get('/service-provider/edit/{slug}', 'ServiceProviderController@edit')->name('sp.edit');
    $this->post('/service-provider/update', 'ServiceProviderController@update')->name('sp.update');

    $this->post('/service-provider/approve', 'ServiceProviderController@approveUser')->name('sp.user.approve');

    $this->post('/service-provider/decline', 'ServiceProviderController@declineUser')->name('sp.user.decline');
    $this->post('/service-provider/delete-banner-image', 'ServiceProviderController@deleteBannerImage')->name('sp.banner-image-delete');

    $this->post('/service-provider/approval-filter', 'ServiceProviderController@userApprovalFilter')->name('sp.users.filter');
    $this->get('lifetime/{sp_id}', 'ServiceProviderController@showLifeTime')->name('sp.lifetime');
    $this->post('/delete-banner-image', 'ServiceProviderController@deleteBannerImage')->name('admin.banner-image-delete');
    $this->post('/update-banner-image', 'ServiceProviderController@updateGallery')->name('admin.update-gallery');

    $this->post('/service-provider/get-state-list', 'ServiceProviderController@getStateList')->name('sp.get-state-list');
    $this->post('/service-provider/get-city-list', 'ServiceProviderController@getCityList')->name('sp.get-city-list');
    //==================================================================New Category

    $this->get('new-category', 'NewCategoryController@index')->name('new-category.index');
    $this->post('new-category/getdata', 'NewCategoryController@getData')->name('new-category.getdata');

    $this->resource('new-subcategory', 'NewSubCategoryController');
    $this->post('new-subcategory/getdata', 'NewSubCategoryController@getData')->name('new-subcategory.getdata');
    $this->post('new-subcategory/ajax-subcategory', 'NewServiceController@ajaxSubcategory')->name('new-subcategory.ajax');

    $this->resource('new-service', 'NewServiceController');
    $this->post('new-service/getdata', 'NewServiceController@getData')->name('new-service.getdata');

    $this->resource('subservice', 'SubServiceController');
    $this->post('subservice/getdata', 'SubServiceController@getData')->name('subservice.getdata');

    $this->post('subservice/ajax-services', 'SubServiceController@ajaxSubcategoryservice')->name('subservices.ajax');

    $this->resource('service-attribute', 'ServiceAttributeController');
    $this->post('service-attribute/getdata', 'ServiceAttributeController@getData')->name('service-attribute.getdata');

    $this->resource('fitness-type', 'FitnessTypeController');
    $this->post('fitness-type/getdata', 'FitnessTypeController@getData')->name('fitness-type.getdata');
    
    //------------------------------------------------------------
    $this->resource('amenity', 'AmenityController');
    $this->post('amenity/getdata', 'AmenityController@getData')->name('amenity.getdata');

    $this->resource('tier', 'TierController');
    $this->post('tier/getdata', 'TierController@getData')->name('tier.getdata');

    $this->get('category', 'CategoryController@index')->name('category.index');
    $this->post('category/getdata', 'CategoryController@getData')->name('category.getdata');

    $this->resource('subcategory', 'SubCategoryController');
    $this->post('subcategory/getdata', 'SubCategoryController@getData')->name('subcategory.getdata');

    $this->post('subcategory/ajax-subcategory', 'ServiceController@ajaxSubcategory')->name('subcategory.ajax');

    $this->resource('service', 'ServiceController');
    $this->post('service/getdata', 'ServiceController@getData')->name('service.getdata');

    $this->resource('staff', 'StaffController');
    $this->post('staff/getdata', 'StaffController@getData')->name('staff.getdata');
    $this->post('staff/banner-delete', 'StaffController@deleteImageVideos')->name('staff.destroy-file');

    $this->resource('gallery', 'GalleryController');
    $this->post('gallery/getdata', 'GalleryController@getData')->name('gallery.getdata');

    $this->resource('banners', 'BannersController');
    $this->post('banners/getdata', 'BannersController@getData')->name('banners.getdata');
    $this->post('/banners/changeStatus', 'BannersController@StatusUpdate')->name('banners.status.update');

    $this->resource('product', 'ProductController');
    $this->post('product/getdata', 'ProductController@getData')->name('product.getdata');

    $this->resource('tag', 'TagController');
    $this->post('tag/getdata', 'TagController@getData')->name('tag.getdata');

    $this->resource('testimonial', 'TestimonialController');
    $this->post('testimonial/getdata', 'TestimonialController@getData')->name('testimonial.getdata');

    $this->resource('page-section', 'PageSectionController');
    $this->post('page-section/getdata', 'PageSectionController@getData')->name('page-section.getdata');
    $this->post('/page-section/changeStatus', 'PageSectionController@StatusUpdate')->name('page-section.status.update');

    $this->resource('occasion', 'OccasionController');
    $this->post('occasion/getdata', 'OccasionController@getData')->name('occasion.getdata');

    $this->resource('gift-theme', 'GiftThemeController');
    $this->post('gift-theme/getdata', 'GiftThemeController@getData')->name('gifttheme.getdata');
    $this->post('/gift-theme/makePublish', 'GiftThemeController@PublishUpdate')->name('gifttheme.publish.update');

    $this->resource('faq-category', 'FaqCategoryController');
    $this->post('faq-category/getdata', 'FaqCategoryController@getData')->name('faq-category.getdata');

    $this->resource('faq', 'FaqController');
    $this->post('faq/getdata', 'FaqController@getData')->name('faq.getdata');

    $this->resource('contact', 'ContactController');
    $this->post('contact/getdata', 'ContactController@getData')->name('contact.getdata');

    $this->resource('e-card', 'ECardController');
    $this->post('e-card/getdata', 'ECardController@getData')->name('e-card.getdata');
    $this->any('e-card/cancel', 'ECardController@cancel')->name('e-card.cancel');
    $this->get('cancelled-card-view/{id}', 'ECardController@cancelled_card_view')->name('e-card.cancelled_card_view');
    //------------------------offer routes--------------------
    $this->resource('offer', 'OfferController');
    $this->post('offer/getdata', 'OfferController@getData')->name('offer.getdata');

    $this->get('service-provider/pending/offer', 'SpOffersController@pendingOffers')->name('sp.pending.offer');

    $this->get('service-provider/approved/offer', 'SpOffersController@approvedOffers')->name('sp.approved.offer');

    $this->get('service-provider/expired/offer', 'SpOffersController@expiredOffers')->name('sp.expired.offer');

    $this->get('service-provider/spectacular/offer', 'SpOffersController@spectacularOffers')->name('sp.spectacular.offer');


    $this->post('sp/offer/getdata', 'SpOffersController@getData')->name('sp.offer.getdata');
    $this->get('sp/offer/edit/{id}/{name}', 'SpOffersController@edit')->name('sp.offer.edit');
    $this->get('sp/offer/nominate/{id}/{offer_status}', 'SpOffersController@nominateOffer')->name('sp.offer.nominate');
     $this->PATCH('sp/offer/update/{id}', 'SpOffersController@updateOffer')->name('sp.offer.update');

    $this->get('sp/offer/decline/{id}', 'SpOffersController@declineOffer')->name('sp.offer.decline');
    $this->get('sp/offer/view/{id}/{name}', 'SpOffersController@viewOffer')->name('sp.offer.view');
    $this->post('sp/offer/update/nominate/{id}', 'SpOffersController@updateNominateOffer')->name('sp.offer.update.nominate');
     $this->any('sp/offer/destroy/{id}', 'SpOffersController@destroy')->name('sp.offer.destroy');
     $this->post('sp/offer/changeStatus', 'SpOffersController@StatusUpdate')->name('sp.offer.status.update');
     $this->post('sp/offer/changeFeaturedStatus', 'SpOffersController@FeaturedStatusUpdate')->name('sp.offer.featuredstatus.update');

    $this->get('service-provider/services/{id}', 'SpServicesController@index')->name('sp.service.index');
    $this->get('service-provider/services/banner/{id}', 'ServiceProviderController@banner')->name('sp.service.banner');
    $this->post('sp/services/getdata', 'SpServicesController@getData')->name('sp.services.getdata');
    $this->get('sp/services/view/{id}', 'SpServicesController@view')->name('sp.services.view');

    //------------------------offer routes--------------------

   $this->resource('quickFact', 'QuickFactController');
   $this->post('quickFact/getdata', 'QuickFactController@getData')->name('quickFact.getdata');
   $this->post('get-quickFact-subcategory', 'QuickFactController@ajaxSubcategory')->name('get-quickFact-subcategory');
   $this->post('get-quickFact-services', 'QuickFactController@ajaxGetSubcatServices')->name('get-quickFact-services');
   $this->post('get-quickFact-sub-services', 'QuickFactController@ajaxGetSubcatSubServices')->name('get-quickFact-sub-services');

   $this->post('quickfact/banner-delete', 'QuickFactController@deleteBannerImage')->name('quickfact.banner-image-delete');

   $this->resource('reviews', 'ReviewsController');
   $this->post('reviews/getdata', 'ReviewsController@getData')->name('reviews.getdata');
   $this->post('reviews/get-sp-staff-service', 'ReviewsController@getSpStaffServices')->name('get-sp-staff-service');
   $this->post('reviews/ajax-get-sp-searched-staff-service', 'ReviewsController@ajaxGetSpSearchedStaffServices')->name('ajax-get-sp-searched-staff-service');
   $this->post('reviews/filter', 'ReviewsController@getCustomerReviewsFilter')->name('ajax.review.filter');

    $this->post('reviews/status', 'ReviewsController@StatusUpdate')->name('review.status.update');

    $this->get('reviews/show/overall/{sp_id}', 'ReviewsController@showOverallSpReviews')->name('sp.overall.reviews');
    
    $this->get('customer/forum', 'ForumController@index')->name('forum.index');
    $this->post('/forum/get-queries', 'ForumController@getData')->name('forum.queries');
    $this->any('/forum/delete-queries/{slug}', 'ForumController@deleteForumQueries')->name('forum.queries.delete');
    $this->any('/forum/show/{slug}', 'ForumController@viewQuery')->name('forum.queries.view');
    $this->any('/forum/delete/{slug}', 'ForumController@deleteForumComment')->name('forum.comment.delete');
    $this->any('/forum/approve-status/{slug}', 'ForumController@approveForumStatus')->name('forum.queries.approve-status');
    $this->any('/forum/reject-status/{slug}', 'ForumController@rejectForumStatus')->name('forum.queries.reject-status');

    $this->resource('spectacularOffer', 'SpectacularOfferTypeController');
    $this->post('spectacularOffer/getdata', 'SpectacularOfferTypeController@getData')->name('spectacularOffer.getdata');

    $this->resource('promocode', 'PromoCodeController');
    $this->post('promocode/getdata', 'PromoCodeController@getData')->name('promocode.getdata');
    $this->post('/promocode/changeStatus', 'PromoCodeController@StatusUpdate')->name('promo.status.update');
    $this->get('promocode/promo-log/{id}', 'PromoCodeController@promo_log')->name('promocode.promo-log');
    

    $this->get('settings', 'SettingController@index')->name('setting.index');
    $this->post('settings/getdata', 'SettingController@getData')->name('setting.getdata');
    $this->any('settings/edit/{id}', 'SettingController@edit')->name('setting.edit');
    $this->any('settings/update/{id}', 'SettingController@update')->name('setting.update');

    $this->get('gift-orders', 'OrdersController@giftOrders')->name('orders.gift');
    $this->post('gift-orders/getgiftdata', 'OrdersController@getGiftData')->name('new.orders.getgiftdata');
    $this->get('new-orders', 'OrdersController@newOrders')->name('orders.new');
    $this->post('new-orders/getdata', 'OrdersController@getData')->name('new.orders.getdata');
    $this->any('orders/view/{slug}/{flag?}', 'OrdersController@view')->name('orders.view');
    $this->get('confirm-orders', 'OrdersController@confirmOrders')->name('orders.confirm');
    $this->post('orders/getdataConfirm', 'OrdersController@getDataConfirm')->name('orders.getdataConfirm');
    $this->get('complete-orders', 'OrdersController@completeOrders')->name('orders.complete');
    $this->post('orders/getdataComplete', 'OrdersController@getDataComplete')->name('orders.getdataComplete');
    $this->post('orders/cancel', 'OrdersController@cancel')->name('orders.cancel');
    $this->get('cancelled-orders', 'OrdersController@cancelledOrders')->name('orders.cancelled');
    $this->post('orders/getcancelled', 'OrdersController@getCancelled')->name('orders.getcancelled');
    $this->get('cancelled-order-view/{id}/{flag?}', 'OrdersController@cancelled_order_view')->name('orders.cancelled_order_view');
    $this->get('/orders/order-chat-history/{id}/{flag?}', 'OrdersController@orderChatHistory')->name('orders.orderChatHistory');
    $this->get('/custompages', 'CustomPagesController@index')->name('custompages.index');
    $this->post('/custompages/getdata', 'CustomPagesController@getData')->name('custompages.getdata');
    $this->get('/custompages/edit/{slug}', 'CustomPagesController@edit')->name('custompages.edit');
    $this->post('/custompages/update', 'CustomPagesController@update')->name('custompages.update');
    

    $this->any('sp-unpaid-settlements', 'OrdersController@sp_unpaid_settlements')->name('orders.sp_unpaid_settlements');
    $this->post('orders/getDataSpUnpaid', 'OrdersController@getDataSpUnpaid')->name('orders.getDataSpUnpaid');
    $this->get('unpaid-settlements/{id}', 'OrdersController@unpaid_settlements')->name('orders.unpaid_settlements');
    $this->post('orders/getdataUnpaid', 'OrdersController@getDataUnpaid')->name('orders.getdataUnpaid');
    $this->any('confirm-settlements', 'OrdersController@confirm_settlements')->name('orders.confirm-settlements');
    $this->post('orders/save-settlements', 'OrdersController@save_settlements')->name('orders.save-settlements');
    $this->post('orders/offline-settlements', 'OrdersController@offline_settlements')->name('orders.offline-settlements');
    $this->get('paid-settlements', 'OrdersController@paid_settlements')->name('orders.paid_settlements');
    $this->post('orders/getdataPaid', 'OrdersController@getDataPaid')->name('orders.getdataPaid');
    $this->any('orders/view-settlement/{slug}/{flag?}', 'OrdersController@view_settlement')->name('orders.view-settlement');
    $this->get('transaction-history', 'OrdersController@transaction_history')->name('orders.transaction_history');
    $this->post('orders/getdataHistory', 'OrdersController@getDataHistory')->name('orders.getdataHistory');
    $this->get('orders/view-history/{id}', 'OrdersController@view_history')->name('orders.view_history');
    $this->get('aesthetic-orders', 'OrdersController@aesthetic_orders')->name('orders.aesthetic_orders');
    $this->post('orders/getaestheticData', 'OrdersController@getAestheticData')->name('orders.getaestheticData');
    $this->get('orders/view-aesthetic/{id}', 'OrdersController@view_aesthetic')->name('orders.view_aesthetic');
    $this->post('orders/save-aesthetic', 'OrdersController@save_aesthetic')->name('orders.save-aesthetic');
    $this->get('contact/view/{id}', 'ContactController@view')->name('contact.view');
    $this->post('contact/save-contact', 'ContactController@save_contact')->name('contact.save-contact');

    /*------------------------------ Store Trail Sessions Route ---------------------------------------------*/
    $this->resource('trail-session', 'TrailSessionController');
    $this->post('trail-session/getdata', 'TrailSessionController@getData')->name('trail-session.getdata');
    $this->resource('trail-session-faq', 'TrailSessionFaqController');
    $this->post('trail-session-faq/getdata', 'TrailSessionFaqController@getData')->name('trail-session-faq.getdata');
    $this->get('trail-session-faq/view/{spid}/{userid}/{sessionid}', 'TrailSessionFaqController@view')->name('trail-session-faq.view');
    $this->post('trail-session/delete-question/{slug}', 'TrailSessionController@deleteSessionQuestion')->name('trail-session.delete-question');
});

$this->group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'AdminBeforeLoggedIn'], function () {
    
    $this->get('/', 'AdminController@login')->name('admin.login');
    $this->get('/login', 'AdminController@login')->name('admin.login');
    $this->post('/login', 'AdminController@checklogin')->name('admin.make.login');
    
});

$this->group([ 'namespace' => 'Front'], function () {

    $this->get('/user-email-confirmation', 'UserController@userEmailConfirmation')->name('front.confirm.email');
    $this->get('/forgot-password', 'UserController@forgotPassword')->name('front.forgotpassword');
    $this->post('/forgot-password', 'UserController@checkForgotPasswordEmail')->name('front.make.forgotPassword');
    $this->any('/forgot-password-page/{token}', 'UserController@forgotPassPage')->name('front.forgot.page');
    $this->post('/update-forgot-password', 'UserController@updateForgotPassword')->name('update-forgot-password');

     //for service provider
     $this->get('sp/user-email-confirmation', 'UserController@spUserEmailConfirmation')->name('front.sp.confirm.email');
     $this->get('/forgotPassword', 'SpUserController@forgotPassword')->name('front.sp.forgotpassword');

    $this->post('/forgotPassword', 'SpUserController@checkForgotPasswordEmail')->name('front.sp.make.forgotPassword');
    $this->any('/forgotPassword-page/{token}', 'SpUserController@forgotPassPage')->name('front.sp.forgot.page');
    $this->post('/update-forgotPassword', 'SpUserController@updateForgotPassword')->name('sp.update-forgot-password');


     $this->get('/service-provider/signup', 'SpUserController@signup')->name('front.sp.signup');
     $this->post('/service-provider/signup', 'SpUserController@makeSignup')->name('front.sp.make.signup');
     $this->get('/service-provider/check-otp', 'SpUserController@checkOtp')->name('front.sp.check.otp');
     $this->post('/service-provider/registration', 'SpUserController@registerServiceProvider')->name('front.sp.registration');

     $this->get('pages/{slug}', 'CustomPageViewController@showPages')->name('show-custom-page');
     // $this->any('alert-page', 'CustomPageViewController@alertPage')->name('custom-alert-page');

     $this->any('payment/ipnlistener', 'PaytabController@ipnListener')->name('payment.ipn.listener');
     $this->any('payment/log', 'PaytabController@paymentLog')->name('payment.log');

      $this->get('generate-pdf/{orderid}', 'PdfGenerateController@orderInvoice')->name('generate-pdf');
      $this->get('app_redirect', 'CustomPageViewController@app_redirect')->name('app_redirect');
      $this->get('sp_app_redirect', 'CustomPageViewController@sp_app_redirect')->name('sp_app_redirect');
      $this->get('shopdetail/{sp_id}', 'CustomPageViewController@shopdetail_redirect')->name('shopdetail');
      $this->get('refercode/{refer_code}', 'CustomPageViewController@refercode_redirect')->name('refercode');
      $this->get('offerdetail/{offer_id}/{sp_id}', 'CustomPageViewController@offerdetail')->name('offerdetail');
      $this->get('/', 'CustomPageViewController@rootRedirect')->name('rootRedirect');

});


//routes for service provider admin

$this->group(['prefix' => 'service-provider', 'namespace' => 'SpAdmin', 'middleware' => 'SpAdminLoggedIn'], function () {

    $this->get('/', 'PagesController@dashboard')->name('spadmin.dashboard');
    $this->get('/dashboard', 'PagesController@dashboard')->name('spadmin.dashboard');
    $this->get('logoutt', 'UserController@logout')->name('spadmin.logout');

    $this->get('/changePassword', 'UserController@changePassword')->name('spuser.change-password');
    $this->post('/updatePassword', 'UserController@doUpdatePassword')->name('spuser.update-password');

    $this->post('/delete-banner-image', 'UserController@deleteBannerImage')->name('spuser.banner-image-delete');

    $this->get('/shop-timings', 'UserController@showShopTiming')->name('spuser.shop-timings');

    $this->post('/update-shop-timings', 'UserController@updateShopTimings')->name('spuser.update-shop-timings');

    $this->get('/edit-profile', 'UserController@edit')->name('spuser.edit-profile');
    $this->get('/gallery', 'UserController@gallery')->name('spuser.gallery');
    $this->post('/update', 'UserController@update')->name('spuser.update');
    $this->post('/update-gallery', 'UserController@updateGallery')->name('spuser.update-gallery');

    $this->resource('my-gallery', 'GalleryController');
    $this->post('my-gallery/getdata', 'GalleryController@getData')->name('my-gallery.getdata');

    $this->resource('staffs', 'StaffController');
    //$this->get('staffs', 'StaffController@index')->name('staffs.index');
    $this->post('staff/getdata', 'StaffController@getData')->name('staffs.getdata');
    $this->post('/delete-staff-image', 'StaffController@deleteImageVideos')->name('spuser.staff-destroy-file');

    $this->post('get-state-list', 'UserController@getStateList')->name('spuser.get-state-list');
    $this->post('get-city-list', 'UserController@getCityList')->name('spuser.get-city-list');

    $this->resource('products', 'ProductController');
    $this->post('product/getdata', 'ProductController@getData')->name('products.getdata');

    $this->get('/agreement', 'AgreementController@index')->name('spuser.agreement');
    $this->post('/agreement/home-service', 'AgreementController@addMov')->name('spuser.mov');

    $this->resource('services', 'ServiceController');
    $this->post('services/getdata', 'ServiceController@getData')->name('services.getdata');
    $this->post('ajax/getServices', 'ServiceController@ajaxGetSubcatServices')->name('ajax.get.services');

    $this->post('ajax/addServiceData', 'ServiceController@ajaxAddServiceData')->name('ajax.add.service.data');

    $this->post('ajax/check/homeService', 'ServiceController@checkProvideHomeService')->name('ajax.check.home.service');

    $this->post('service/active/homeService', 'ServiceController@activeHomeService')->name('home.service.active');

    /*======================================================================*/
    $this->resource('new-services', 'NewServiceController');
    $this->post('new-services/getdata', 'NewServiceController@getData')->name('new-services.getdata');
    $this->post('new-services/storemultiple', 'NewServiceController@storeMultiple')->name('new-services.storemultiple');
    $this->any('new-services/updatemultiple/{id}', 'NewServiceController@updateMultiple')->name('new-services.updatemultiple');
    $this->post('ajax/getNewServices', 'NewServiceController@ajaxGetSubcatServices')->name('ajax.get.newservices');
    $this->post('ajax/addNewServiceData', 'NewServiceController@ajaxAddServiceData')->name('ajax.add.newservice.data');
    $this->post('ajax/check/homeNewService', 'NewServiceController@checkProvideHomeService')->name('ajax.check.home.newservice');
    $this->post('service/active/homeNewService', 'NewServiceController@activeHomeService')->name('home.newservice.active');
    $this->post('ajax/getNewSubServices', 'NewServiceController@ajaxGetSubcatSubServices')->name('ajax.get.newsubservices');
    $this->post('ajax/getNewSubServicesInfo', 'NewServiceController@ajaxGetSubcatSubServicesInfo')->name('ajax.get.newsubservicesinfo');
    $this->post('ajax/getFitnessTypeService', 'NewServiceController@ajaxGetFitnessTypeServices')->name('ajax.get.newservicesfitnesstype');
    /*=======================================================================*/

    $this->post('ajax/change/mobile_no', 'UserController@ajaxChangeMobileNo')->name('ajax.change.mobileno');

    $this->post('ajax/check/otp', 'UserController@verifyOtp')->name('ajax.check.otp');

    $this->resource('offers', 'OffersController');
    $this->post('/my-offers/getdata', 'OffersController@getData')->name('offers.getdata');
    $this->post('/offers/get-services', 'OffersController@ajaxGetServices')->name('ajax.offers.get.services');
    $this->post('/offers/show-service-listing', 'OffersController@ajaxShowServiceListing')->name('ajax.offers.show.serviceListing');

    /*=======================================================================*/
    $this->resource('new-offers', 'NewOffersController');
    $this->post('/my-new-offers/getdata', 'NewOffersController@getData')->name('new-offers.getdata');
    $this->post('/new-offers/get-services', 'NewOffersController@ajaxGetServices')->name('ajax.new-offers.get.services');
    $this->post('/new-offers/show-service-listing', 'NewOffersController@ajaxShowServiceListing')->name('ajax.new-offers.show.serviceListing');

    /*=======================================================================*/

    $this->post('/resend-mail', 'UserController@resendMail')->name('spuser.resend.mail');
    $this->get('/forum/customer/queries', 'ForumController@customerQueriesIndex')->name('forum.customer.queries');

    $this->get('/forum/customer/responded-queries', 'ForumController@customerRespondedQueriesIndex')->name('forum.customer.responded.queries');

    $this->post('/forum/customer/get-queries', 'ForumController@customerGetQueries')->name('forum.customer.getqueries');

    $this->post('/forum/customer/get-responded', 'ForumController@customerGetResponded')->name('forum.customer.getresponded');

    $this->any('/forum/{slug}', 'ForumController@customerViewQuery')->name('forum.customer.queries.view');

    $this->get('my-reviews', 'ReviewController@index')->name('my-reviews');
    $this->post('my-reviews/filter', 'ReviewController@getCustomerReviewsFilter')->name('sp.ajax.review.filter');

    $this->get('/my-orders', 'OrdersController@index')->name('orders.index');
    $this->any('/my-orders/chat-history/{id}', 'OrdersController@myOrderChatHistory')->name('orders.myOrderChatHistory');
    $this->post('/my-orders/getdata', 'OrdersController@getData')->name('orders.getdata');

    $this->get('/my-confirmed-orders', 'OrdersController@confirmed_orders')->name('orders.confirmed_orders');
    $this->any('/my-confirmed-orders/chat-history/{id}', 'OrdersController@myConfirmOrderChatHistory')->name('orders.myConfirmOrderChatHistory');
    $this->post('/my-orders/getdataconfirmed', 'OrdersController@getDataConfirmed')->name('orders.getdataconfirmed');

    $this->get('/my-completed-orders', 'OrdersController@completed_orders')->name('orders.completed_orders');
    $this->post('/my-orders/getdatacompleted', 'OrdersController@getDataCompleted')->name('orders.getdatacompleted');

    $this->get('/my-orders/show-order-detail/{id}/{flag}', 'OrdersController@show_order')->name('orders.show_order');
    $this->get('/my-orders/reschedule-order/{id}', 'OrdersController@reschedule_order')->name('orders.reschedule_order');
    $this->post('ajax/gettime', 'OrdersController@ajaxGetTime')->name('ajax.get.time');
    $this->post('orders/reschedule', 'OrdersController@reschedule')->name('orders.reschedule');
    $this->get('/my-orders/completed-order-view/{id}', 'OrdersController@completed_order_view')->name('orders.completed_order_view');
    $this->post('orders/completed', 'OrdersController@completed')->name('orders.completed');
    $this->get('/my-orders/order-confirm-view/{id}', 'OrdersController@order_confirm_view')->name('orders.order_confirm_view');
    $this->post('orders/order_confirm', 'OrdersController@order_confirm')->name('orders.order_confirm');
    
    $this->get('/my-orders/add-order/{id?}/{date?}/{time?}', 'OrdersController@add_order')->name('orders.add_order');
    $this->post('orders/new_order', 'OrdersController@new_order')->name('orders.new_order');
    $this->get('cancelled-orders', 'OrdersController@cancelledOrders')->name('orders.cancelled_order');
    $this->post('orders/getdataCancelled', 'OrdersController@getDataCancelled')->name('orders.getdataCancelled');

    $this->get('staffs/leaves/{id}', 'StaffController@leaves')->name('staffs.leaves');
    $this->post('staffs/createLeave', 'StaffController@createLeave')->name('staffs.createLeave');
    $this->post('staffs/getleave', 'StaffController@getLeave')->name('staffs.getleave');
    $this->get('staffs/add_leave_form/{id}', 'StaffController@add_leave_form')->name('staffs.add_leave_form');
    $this->get('staffs/edit_leave_form/{id}', 'StaffController@edit_leave_form')->name('staffs.edit_leave_form');
    $this->post('staffs/editLeave', 'StaffController@editLeave')->name('staffs.editLeave');
    $this->any('staffs/destroy/{id}', 'StaffController@destroy_leave')->name('staffs.destroy_leave');
    $this->post('ajax/gettotime', 'StaffController@ajaxGetToTime')->name('ajax.get.totime');
    $this->post('ajax/getfromtime', 'StaffController@ajaxGetFromTime')->name('ajax.get.fromtime');

    $this->any('st/staff-calendar', 'StaffController@staffCalendar')->name('staffs.staff-calendar');

    $this->any('my-accounts', 'MyAccountController@index')->name('my-accounts');
    $this->get('get-sales/online', 'MyAccountController@onlineSales')->name('myaccount.online.sales');
    $this->get('get-sales/offline', 'MyAccountController@offlineSales')->name('myaccount.offline.sales');
    $this->get('get-sales/all', 'MyAccountController@allSales')->name('myaccount.all.sales');
    $this->post('getsales', 'MyAccountController@getData')->name('my-account.gets.sales');
    $this->post('get-online-data', 'MyAccountController@getOnlineData')->name('my-account.gets.onlinesales');
    $this->post('get-offline-data', 'MyAccountController@getOfflineData')->name('my-account.gets.offlinesales');
    $this->post('get-all-data', 'MyAccountController@getAllData')->name('my-account.gets.allsales');
    $this->get('get-sales/paid', 'MyAccountController@paidSales')->name('myaccount.paid.sales');
    $this->post('get-paid-data', 'MyAccountController@getPaidData')->name('my-account.gets.paidsales');

   
    $this->get('beutics-chat', 'UserController@beuticsChat')->name('spuser.beutics-chat');
    $this->any('get-chat', 'UserController@getChat')->name('spuser.get-chat');
    
});

$this->group(['prefix' => 'service-provider', 'namespace' => 'SpAdmin', 'middleware' => 'SpAdminBeforeLoggedIn'], function () {
    
    $this->get('/', 'UserController@login')->name('spadmin.login');
    $this->get('/login', 'UserController@login')->name('spadmin.login');
    $this->post('/login', 'UserController@checklogin')->name('spadmin.make.login');
    
});
//--------------------------------
//UPDATE sp_users SET store_url = REPLACE(store_url, '-', '_')
Auth::routes();
Route::get('/', 'HomeController@index')->name('/');
Route::get('offers', 'HomeController@offers')->name('offers');
Route::get('beauty', 'LandingController@index')->name('beauty');
Route::get('fitness', 'LandingController@index')->name('fitness');
Route::get('wellness', 'LandingController@index')->name('wellness');
Route::post('contact-form', 'LandingController@sendcontact')->name('contact-form');
Route::get('beauty-listing', 'ListingController@beautyindex')->name('beauty-listing');
Route::get('wellness-listing', 'ListingController@wellnessindex')->name('wellness-listing');
Route::get('fitness-listing', 'ListingController@fitnessindex')->name('fitness-listing');

Route::get('detail/{store_slug}', 'DetailsController@storeDetail');
Route::get('wellness/{store_slug}', 'DetailsController@storeDetail');
Route::get('fitness/{store_slug}', 'DetailsController@fitnessStoreDetail');




Route::get('instruct-details', 'DetailsController@idetailsindex')->name('instructdetails');
Route::get('about-details', 'DetailsController@adetailsindex')->name('aboutdetails');
Route::get('review-details', 'DetailsController@rdetailsindex')->name('reviewdetails');
Route::get('beauty-product', 'DetailsController@beautyproindex')->name('beautyproducts');
Route::get('beauty-in-store', 'DetailsController@beautystoreindex')->name('beautyinstore');
Route::get('/gift-acceptance', 'CronController@checkGiftAcceptance')->name('checkGiftAcceptance');

