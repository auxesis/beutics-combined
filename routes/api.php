<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Authorization, Content-Type");

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

Route::group(['namespace' => 'Api'], function () 
{
   
    // add nationality

    Route::post('add-nationality', 'UserController@addNationality');

    Route::get('test-notification','UserBookingsController@testNotification');
    // chat notification 
    Route::get('send-chat-notification', 'ChatController@sendNotification');

    //customer user managment routes 
    Route::post('register-user', 'UserController@registerUser');
    Route::post('send-signup-notification', 'UserController@sendSignupNotification');
    Route::get('get-country-list', 'UserController@getCountryList');    
    Route::post('login', 'UserController@login');
    Route::post('verify-otp', 'UserController@verificationApi');
    Route::post('register-otp', 'UserController@registerOtp'); 
    Route::post('social-check', 'UserController@checkSocial');
    Route::post('forgot-password', 'UserController@forgotPassword');
    Route::post('forgot-reset-password', 'UserController@forgotResetPassword');
    
     //Service Provider user managment routes
    Route::get('service-provider/get-country-list', 'ServiceProviderController@getCountryList');
    Route::get('service-provider/get-category-list', 'ServiceProviderController@getCategories');
    Route::get('service-provider/get-sub-category-list', 'ServiceProviderController@getSubCategories');       
    Route::get('service-provider/get-amenities', 'ServiceProviderController@getAmenities');  
    Route::post('service-provider/register-otp', 'ServiceProviderController@registerOtp');
    Route::post('service-provider/register-user', 'ServiceProviderController@registerUser');
    Route::post('service-provider/login', 'ServiceProviderController@login');  
    Route::post('service-provider/forgot-password', 'ServiceProviderController@forgotPassword');
    Route::post('service-provider/forgot-reset-password', 'ServiceProviderController@forgotResetPassword');
    Route::post('service-provider/verify-otp', 'ServiceProviderController@verificationApi');

    //cron
    Route::get('cron/gift-acceptance', 'UserBookingsController@checkGiftAcceptanceCron');
    Route::get('cron/send-gift-card', 'UserBookingsController@sendGiftCardCron');//for gift card send later
    Route::get('cron/order-completion', 'UserBookingsController@checkOrderCompletionCron');
    Route::get('cron/send-gift-notification', 'UserBookingsController@sendGiftCron');
    Route::get('cron/sp-offer-expire', 'SpOrdersController@sp_offer_expire');
    Route::get('cron/sp-enter-staff', 'SpOrdersController@sp_enter_staff');
    Route::get('cron/sp-enter-product', 'SpOrdersController@sp_enter_product');
    Route::get('cron/sp-enter-service', 'SpOrdersController@sp_enter_service');
    Route::get('cron/sp-enter-offer', 'SpOrdersController@sp_enter_offer');
    Route::get('cron/sp-enter-store-timing', 'SpOrdersController@sp_enter_store_timing');
    Route::get('cron/sp-enter-bank', 'SpOrdersController@sp_enter_bank');
    Route::get('cron/sp-close-service', 'SpOrdersController@sp_close_service');
    Route::get('cron/cs-promo-reminder', 'SpOrdersController@cs_promo_reminder');
    Route::get('cron/appointment-reminder-cron', 'UserBookingsController@appointmentReminderCron');
    Route::get('cron/app-confirm-reminder', 'SpOrdersController@appointmentConfirmReminder');
    Route::get('cron/mass-notification-cron', 'UserPagesController@mass_notification_cron');
    Route::get('cron/mass-promo-cron', 'UserPagesController@mass_promo_cron');
    // Route::get('cron/appointment-confirm-reminder-cron', 'SpOrdersController@appointmentConfirmReminder');
});

Route::group(['namespace' => 'Api', 'middleware' => 'jwt.auth'], function () 
{  
  //customer user managment routes
    Route::post('get-lat-long', 'UserController@getLatLong');
    Route::post('logout', 'UserController@logout');    
    Route::post('edit-profile', 'UserController@editProfile');
    Route::post('change-password', 'UserController@changePassword');
    Route::get('resend-mail', 'UserController@resendEmail'); 
    Route::post('edit-mobileno', 'UserController@updateMobile');
    Route::post('user-details', 'UserController@userDetails');
    Route::post('update-refer-code', 'UserController@updateReferCode');
    
    Route::get('get-main-categories', 'UserPagesController@getMainCategories');
    Route::get('get-sub-categories', 'UserPagesController@getSubCategories');
    Route::post('get-admin-quick-facts', 'UserPagesController@getQuickFactFromAdmin');
    Route::post('ask-expert', 'UserPagesController@askExpert');
    Route::post('get-forums', 'UserPagesController@getForum');
    Route::post('add-forum-comments', 'UserPagesController@addForumComment');
    Route::post('get-forum-comments', 'UserPagesController@getForumComment');
    Route::post('get-forum-detail', 'UserPagesController@getForumDetail');
    Route::post('comment-like-dislike', 'UserPagesController@commentLikeDislike');
    Route::post('contact-sync', 'UserPagesController@contactSync');
    Route::post('get-subcat-with-services', 'UserPagesController@getSubcategoryWithServices');
    Route::post('get-providers', 'UserPagesController@getProviders');
    Route::post('add-bookmark', 'UserPagesController@addBookmark');
    Route::post('show-bookmarks', 'UserPagesController@showBookmark');
    Route::post('service-provider-info', 'UserPagesController@getServiceProviderInfoTab');
    Route::post('service-provider-zoom-ins', 'UserPagesController@getServiceProviderZoomIns');
    Route::post('service-provider-all-services', 'UserPagesController@getServiceProviderAllServices');
    Route::post('get-customer-reviews','UserPagesController@getCustomerReviews');

    Route::post('get-voice-reviews','UserPagesController@getCombinedReviews');
    Route::post('search-suggestions','UserPagesController@searchServiceAndServiceProvider');
    Route::post('service-provider-all-home-services','UserPagesController@getAllHomeServices');
    Route::post('service-provider-choices','UserPagesController@getServiceProviderChoices');
    Route::post('get-offer-details','UserPagesController@getOfferDetails');
    Route::post('get-spectacular-offer-details','UserPagesController@getSpectacularOfferListing');
    Route::post('get-spectacular-offer-details-more','UserPagesController@getSpectacularOfferListingloadMore');
    Route::post('spectacular-offer-like','UserPagesController@spectacularOfferLike');
    Route::post('get-list-gallery','UserPagesController@getListGallery');


    //customer bookings routes

    Route::post('get-sp-staff-details','UserBookingsController@getSpStaffDetails');
    Route::post('get-tax-shoptimings','UserBookingsController@getStoreTaxDetails');
    Route::post('apply-promo-code','UserBookingsController@applyPromoCode');
    Route::post('booking','UserBookingsController@booking');
    Route::post('get-orders','UserBookingsController@orderListing');
    Route::post('order-summery','UserBookingsController@orderSummery');
    Route::post('add-reviews','UserBookingsController@addReviews');
    Route::post('my-wallet','UserBookingsController@myWallet');
    Route::post('my-earnings','UserBookingsController@earningsListing');
    Route::post('my-redemptions','UserBookingsController@redemptionListing');
    Route::post('refer-earning','UserBookingsController@referredEarningsListing');
    Route::post('get-staff-leaves','UserBookingsController@getStaffLeaves');
    Route::post('available-timeslots','UserBookingsController@getStaffTimeSlots');
    Route::post('friends-visited','UserBookingsController@friendsVisited');
    Route::post('applicable-promo-codes','UserBookingsController@showAppliedPromoCode');
    Route::post('download-order-image','UserBookingsController@downloadOrderImage');
    Route::post('check-gifted-user-existence','UserBookingsController@checkGiftedUserExistence');
    Route::post('add-card','UserBookingsController@addUserCard');
    Route::post('delete-card','UserBookingsController@deleteUserCard');
    Route::post('get-saved-cards','UserBookingsController@getUserCards');
    Route::post('my-cash-transactions','UserBookingsController@walletTransactions');
    Route::post('add-wallet-money','UserBookingsController@addMoneyToWallet');
    Route::post('total-net-savings','UserBookingsController@calculateTotalNetSaving');
    Route::post('get-occasions','UserBookingsController@getOccasions');
    Route::post('get-occasion-themes','UserBookingsController@getOccasionThemes');
    Route::post('get-faq-categories','UserBookingsController@getFaqCategories');
    Route::post('get-faqs','UserBookingsController@getFaqs');
    Route::post('contact-us','UserBookingsController@contactUs');
    Route::post('send-e-gift-card','UserBookingsController@sendGiftCard');
    Route::post('change-notification-setting','UserBookingsController@changeNotificationSetting');
    Route::post('low-balance-alert','UserBookingsController@lowBalanceAlert');
    Route::post('e-card-listing','UserBookingsController@eCardListing');
    Route::post('notification-listing','UserBookingsController@notificationsListing');
    Route::post('unread-notifications','UserBookingsController@checkUnreadNotification');
    Route::post('read-notifications','UserBookingsController@readNotifications');
    Route::post('app-version','UserBookingsController@updateAppVersion');
    Route::post('update-badge','UserBookingsController@updateUserBadge');
    Route::post('update-language','UserBookingsController@updateUserLanguage');
    Route::post('add-trail-session-faq','UserBookingsController@addTrailSessionFaq');
    Route::post('get-trail-session-faqs','UserBookingsController@getTrailSessionFaqs');

    // Route::get('test-notification','UserBookingsController@testNotification');


     //------------------------------------------
    //Service Provider user managment routes

    Route::post('service-provider/change-password', 'ServiceProviderController@changePassword');
    Route::get('service-provider/resend-mail', 'ServiceProviderController@resendEmail'); 
    Route::post('service-provider/logout', 'ServiceProviderController@logout');
    Route::post('service-provider/change-mobile', 'ServiceProviderController@updateMobile');
    Route::post('service-provider/user-details', 'ServiceProviderController@userDetails');
    Route::post('service-provider/edit-profile', 'ServiceProviderController@editProfile');
    Route::post('service-provider/update-shop-time', 'ServiceProviderController@shopTimings');
    Route::post('service-provider/delete-banner-image', 'ServiceProviderController@deleteBannerImage');

    Route::get('service-provider/view-agreement', 'ServiceProviderController@viewAgreement');            

    Route::post('service-provider/add-products', 'ServiceProviderPagesController@addProducts');
    Route::post('service-provider/get-products', 'ServiceProviderPagesController@getProducts');

    Route::post('service-provider/delete-product', 'ServiceProviderPagesController@deleteProduct');

    Route::post('service-provider/edit-product', 'ServiceProviderPagesController@editProduct');

    Route::post('service-provider/add-staff', 'ServiceProviderPagesController@addStaff');
    Route::post('service-provider/get-staff', 'ServiceProviderPagesController@getStaff');
    Route::post('service-provider/edit-staff', 'ServiceProviderPagesController@editStaff');
    Route::post('service-provider/delete-staff', 'ServiceProviderPagesController@deleteStaff');

    Route::post('service-provider/delete-staff-image', 'ServiceProviderPagesController@deleteStaffImage');

    Route::post('service-provider/staff-leave', 'ServiceProviderPagesController@staffLeave'); 

    Route::post('service-provider/edit-staff-leave', 'ServiceProviderPagesController@editStaffLeave'); 

    Route::post('service-provider/staff-home-service', 'ServiceProviderPagesController@staffHomeService');

    Route::get('service-provider/get-admin-service', 'ServiceProviderPagesController@serviceFromAdmin'); 

    Route::post('service-provider/add-service','ServiceProviderPagesController@addServices');
    Route::post('service-provider/edit-service','ServiceProviderPagesController@editServices');
    Route::get('service-provider/get-service','ServiceProviderPagesController@getServices');
    Route::post('service-provider/delete-service','ServiceProviderPagesController@deleteServices');                  

    Route::get('service-provider/get-offer-name','ServiceProviderPagesController@getMainOffers');

    Route::post('service-provider/offer-service-suggestion','ServiceProviderPagesController@OfferServiceSuggestion');

    Route::post('service-provider/add-offers','ServiceProviderPagesController@addOffers');

    Route::post('service-provider/get-offers','ServiceProviderPagesController@getOffers');
    Route::post('service-provider/edit-offers','ServiceProviderPagesController@editOffers');
    Route::post('service-provider/get-offer-details','ServiceProviderPagesController@getOfferDetails');


    Route::post('service-provider/delete-offers','ServiceProviderPagesController@deleteOffer');
    Route::post('service-provider/get-replied-forums','ServiceProviderPagesController@getRepliedForum');

    Route::post('service-provider/get-no-replied-forums','ServiceProviderPagesController@getNoRepliedForum');

    Route::post('service-provider/add-forum-reply','ServiceProviderPagesController@addForumReply');

    Route::post('service-provider/comment-like-dislike','ServiceProviderPagesController@commentLikeDislike');

    Route::post('service-provider/get-forum-comments','ServiceProviderPagesController@getForumComment');
    Route::post('service-provider/get-forum-detail','ServiceProviderPagesController@getForumDetail');

    Route::post('service-provider/get-customer-reviews','ServiceProviderPagesController@getCustomerReviews');

    Route::post('service-provider/get-voice-reviews','ServiceProviderPagesController@getCombinedReviews');

    Route::post('service-provider/get-order-listing','SpOrdersController@getOrderListing');

    Route::post('service-provider/order-summery','SpOrdersController@orderSummery');
    // Route::post('service-provider/search-orders','SpOrdersController@searchOrders');
    Route::post('service-provider/confirm-order','SpOrdersController@confirmOrder');
    Route::post('service-provider/complete-order','SpOrdersController@completeOrder');
    Route::post('service-provider/reschedule-order','SpOrdersController@rescheduleOrder');
    Route::post('service-provider/manual-appointment','SpOrdersController@addAppointment');
    Route::post('service-provider/staff-calendar','SpOrdersController@staffCalendar');
    Route::post('service-provider/get-dashboard-stats','SpOrdersController@getDashboardStats');
    Route::post('service-provider/my-account','SpOrdersController@myAccount');
    Route::post('service-provider/get-sales','SpOrdersController@getOnlineOfflineSales');
    Route::post('service-provider/change-notification-setting','SpOrdersController@changeNotificationSetting');
    Route::post('service-provider/notification-listing','SpOrdersController@notificationsListing');
    Route::post('service-provider/app-version','SpOrdersController@updateAppVersion');
    Route::post('service-provider/update-badge','SpOrdersController@updateUserBadge');
    Route::post('service-provider/update-language','SpOrdersController@updateUserLanguage');

    Route::post('service-provider/unread-notifications','SpOrdersController@checkUnreadNotification');
    Route::post('service-provider/read-notifications','SpOrdersController@readNotifications');

    // for gallery section
    Route::post('service-provider/add-tag','ServiceProviderPagesController@addTag');
    Route::get('service-provider/get-tags','ServiceProviderPagesController@getTags');

    Route::post('service-provider/add-media','ServiceProviderPagesController@addMedia');
    Route::post('service-provider/edit-media','ServiceProviderPagesController@editMedia');
    Route::post('service-provider/list-gallery','ServiceProviderPagesController@listGallery');
    Route::post('service-provider/delete-media','ServiceProviderPagesController@deleteMedia');

});

 

